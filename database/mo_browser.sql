/*
-- Query: SELECT * FROM mountain.mo_browser
LIMIT 0, 1000

-- Date: 2015-07-30 08:30
*/
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (1,'MSIE 2','InternetExplorer 2',1,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (2,'MSIE 3','InternetExplorer 3',2,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (3,'MSIE 4','InternetExplorer 4',3,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (4,'MSIE 5','InternetExplorer 5',4,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (5,'MSIE 6','InternetExplorer 6',5,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (6,'MSIE 7','InternetExplorer 7',6,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (7,'MSIE 8','InternetExplorer 8',7,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (8,'MSIE 9','InternetExplorer 9',8,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (9,'MSIE 10','InternetExplorer 10',9,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (10,'Trident/7.0','InternetExplorer 11',10,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (11,'MSIE','ETC InternetExplorer',11,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (12,'Firefox','FireFox',12,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (13,'Chrome','Chrome',13,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (14,'Safari','Safari',14,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (15,'Opera','Opera',15,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (16,'Lynx','Lynx',16,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (17,'LibWWW','LibWWW',17,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (18,'Konqueror','Konqueror',18,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (19,'Internet Ninja','Internet Ninja',19,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (20,'Download Ninja','Download Ninja',20,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (21,'WebCapture','WebCapture',21,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (22,'LTH','LTH Browser',22,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (23,'Gecko','Gecko compatible',23,'N');
INSERT INTO `mo_browser` (`bs_no`,`bs_cd`,`bs_nm`,`bs_seq`,`bs_fl`) VALUES (24,'Mozilla','Mozilla compatible',24,'N');

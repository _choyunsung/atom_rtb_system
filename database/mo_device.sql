/*
-- Query: SELECT * FROM mountain.mo_device
LIMIT 0, 1000

-- Date: 2015-07-30 08:52
*/
INSERT INTO `mo_device` (`dv_no`,`dv_cd`,`dv_nm`,`dv_seq`,`dv_fl`) VALUES (1,'PC','PC',1,'N');
INSERT INTO `mo_device` (`dv_no`,`dv_cd`,`dv_nm`,`dv_seq`,`dv_fl`) VALUES (2,'Mobile','Mobile',2,'N');
INSERT INTO `mo_device` (`dv_no`,`dv_cd`,`dv_nm`,`dv_seq`,`dv_fl`) VALUES (3,'Tablet','Tablet',3,'N');

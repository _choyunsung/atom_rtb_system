/*
-- Query: SELECT * FROM mountain.mo_os
LIMIT 0, 1000

-- Date: 2015-07-30 08:53
*/
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (1,'Windows CE','Windows CE',1,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (2,'Win98','Windows 98',2,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (3,'Windows 9x','Windows ME',3,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (4,'Windows me','Windows ME',4,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (5,'Windows 98','Windows 98',5,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (6,'Windows 95','Windows 95',6,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (7,'Windows NT 6.3','Windows 8.1',7,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (8,'Windows NT 6.2','Windows 8',8,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (9,'Windows NT 6.1','Windows 7',9,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (10,'Windows NT 6.0','Windows Vista',10,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (11,'Windows NT 5.2','Windows 2003/XP x64',11,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (12,'Windows NT 5.01','Windows 2000 SP1',12,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (13,'Windows NT 5.1','Windows XP',13,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (14,'Windows NT 5','Windows 2000',14,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (15,'Windows NT','Windows NT',15,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (16,'Macintosh','Macintosh',16,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (17,'Mac_PowerPC','Mac PowerPC',17,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (18,'Unix','Unix',18,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (19,'bsd','BSD',19,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (20,'Linux,Wget','Linux',20,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (21,'windows','ETC Windows',21,'N');
INSERT INTO `mo_os` (`os_no`,`os_cd`,`os_nm`,`os_seq`,`os_fl`) VALUES (22,'mac','ETC Mac',22,'N');

/*
-- Query: SELECT * FROM mountain.mo_category
-- Date: 2015-09-17 15:26
*/
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (1,'strAccountMgr','/account/account_master_view','계정관리',0,20,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (2,'strAuthorityMgr','/admin/authority_management','권한관리',37,82,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (3,'strPersnalInfoMgr','/account/account_master_view','개인정보관리',1,21,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (4,'strAdMgr','/campaign/campaign_list','광고관리',0,10,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (13,'strPaymentMgr','/cash/cash_charge_list','결제관리',0,30,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (14,'strReport','/report/report_operation','보고서',0,40,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (15,'strCampaign','/campaign/campaign_list','캠페인',4,11,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (16,'strAdvertiserMgr','/account/account_advertiser_list','광고주관리',1,22,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (17,'strAdGroup','/creative/creative_group_list','광고그룹',4,12,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (18,'strAd','/creative/creative_list','광고',4,13,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (19,'strCashMgr','/cash/cash_charge_list','캐쉬관리',13,31,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (20,'strReferencesMgr','/cash/tax_bill_list','증빙서류',13,32,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (21,'strOperationReport','/report/report_operation','광고운영 보고서',14,41,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (22,'strIntegratedReport','/report/report_integration','통합 보고서',14,44,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (23,'strBusinessReport','/report/report_business','업체 보고서',14,46,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (24,'strTargetingReport','/report/report_targeting','타겟팅 보고서',14,42,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (25,'strSalesReport','/report/report_sales','영업자 보고서',14,45,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (27,'strAccountSetting','/account/account_setting','환경설정',1,23,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (28,'strManager’sCash','/cash/manager_cash_summary','매니저캐쉬',13,33,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (29,'strAccountList','/account/account_list','가입목록',1,25,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (30,'strAdopWallet','/cash/cash_pay_management','ADOP지갑',0,50,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (31,'strCashPayMgr','/cash/cash_pay_management','캐쉬지급관리',30,51,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (32,'strAdCensor','/adcensor/ad_image','광고검수',0,60,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (33,'strImgAd','/adcensor/ad_image','이미지광고',32,61,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (34,'strCalMgr','/calculate/exchange_management','정산관리',0,70,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (35,'strExchangeMgr','/calculate/exchange_management','환율관리',34,71,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (37,'strAdmin','/admin/admin_account','관리자',0,80,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (38,'strAdminAccount','/admin/admin_account','관리자계정',37,81,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (40,'strMenuMgr','/admin/menu_management','메뉴관리',37,83,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (41,'strCustomerService','/board/member_notice_list','고객센터',0,90,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (42,'strMemberNotice','/board/member_notice_list','회원공지',41,91,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (43,'strAdminNotice','/board/admin_notice_list','운영자공지',41,92,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (44,'strQuestionList','/board/question_list','문의목록',41,93,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (45,'strTaxBillMgr','/calculate/tax_bill_list','세금계산서관리',34,72,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (46,'strRefundMgr','/cash/refund_request_list','환불관리',30,52,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (47,'strHome','/dashboard','대시보드',0,1,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (48,'strHome','/dashboard','대시보드',47,2,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (49,'strSalesHome','/dashboard/sales','영업자대시보드',47,2,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (50,'strFeeMgr','/calculate/fee_management','수수료관리',34,73,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (51,'strIntRev','/calculate/revenue_intergration','통합매출',34,74,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (52,'strBusinessRev','/calculate/revenue_business','업체매출',34,75,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (53,'strDepositMgr','/calculate/deposit_pre_list','예치금관리',34,76,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (54,'strNoPaidMgr','/calculate/no_paid_list','미수금관리',34,77,'N');
INSERT INTO `mo_category` (`cate_no`,`cate_nm`,`cate_url`,`cate_explan`,`cate_parent_no`,`cate_seq`,`cate_fl`) VALUES (55,'strMgrReport','/report/report_manager','매니저 보고서',14,43,'N');

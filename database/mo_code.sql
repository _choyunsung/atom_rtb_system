/*
-- Query: SELECT * FROM mountain.mo_code
-- Date: 2015-09-14 15:01
*/
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (1,'tel_no_sel','02',1,'02','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (2,'tel_no_sel','031',2,'031','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (3,'tel_no_sel','032',3,'032','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (4,'tel_no_sel','033',4,'033','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (5,'tel_no_sel','041',5,'041','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (6,'tel_no_sel','042',6,'042','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (7,'tel_no_sel','043',7,'043','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (8,'tel_no_sel','044',8,'044','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (9,'tel_no_sel','051',9,'051','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (10,'tel_no_sel','052',10,'052','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (11,'tel_no_sel','053',11,'053','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (12,'tel_no_sel','054',12,'054','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (13,'tel_no_sel','055',13,'055','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (14,'tel_no_sel','061',14,'061','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (15,'tel_no_sel','062',15,'062','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (16,'tel_no_sel','063',16,'063','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (17,'tel_no_sel','064',17,'064','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (18,'cell_no_sel','010',1,'010','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (19,'cell_no_sel','011',2,'011','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (20,'cell_no_sel','016',3,'016','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (21,'cell_no_sel','017',4,'017','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (22,'cell_no_sel','018',5,'018','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (23,'cell_no_sel','019',6,'019','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (24,'tel_no_sel','070',18,'070','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (25,'mail_host_sel','naver.com',1,'naver.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (26,'mail_host_sel','hanmail.net',2,'hanmail.net','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (27,'mail_host_sel','nate.com',3,'nate.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (28,'mail_host_sel','gmail.com',4,'gmail.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (29,'mail_host_sel','hotmail.com',5,'hotmail.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (30,'mail_host_sel','lycos.co.kr',6,'lycos.co.kr','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (31,'mail_host_sel','empal.com',7,'empal.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (32,'mail_host_sel','dreamwiz.com',8,'dreamwiz.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (33,'mail_host_sel','korea.com',9,'korea.com','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (34,'mail_host_sel','직접입력',10,'직접입력','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (35,'member_gb_sel','ADT',1,'strADT','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (36,'member_gb_sel','AGT',2,'strAGT','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (37,'member_gb_sel','LAB',3,'strLAB','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (38,'status','1',1,'strRun','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (39,'status','2',2,'strReady','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (40,'status','3',3,'strPause','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (41,'status','4',4,'strDone','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (42,'cre_gp_type','1',1,'strPc','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (43,'cre_gp_type','2',2,'strMobileWeb','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (44,'cre_gp_type','3',3,'strMobileApp','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (45,'evaluation','1',1,'strApproveWait','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (46,'evaluation','2',2,'strApprove','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (47,'evaluation','3',3,'strReject','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (48,'evaluation','4',4,'strDefer','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (49,'evaluation','5',5,'strApproveProgress','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (50,'cre_type','1',1,'strImage','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (51,'cre_type','2',2,'strFlash','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (52,'cre_type','3',3,'strText','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (53,'bank_name','1',1,'산업은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (54,'bank_name','2',2,'기업은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (55,'bank_name','3',3,'국민은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (56,'bank_name','4',4,'외환은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (57,'bank_name','5',5,'수협','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (58,'bank_name','6',6,'농협','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (59,'bank_name','7',7,'우리은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (60,'bank_name','8',8,'SC제일은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (61,'bank_name','9',9,'한국시티은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (62,'bank_name','10',10,'대구은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (63,'bank_name','11',11,'부산은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (64,'bank_name','12',12,'광주은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (65,'bank_name','13',13,'제주은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (66,'bank_name','14',14,'전북은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (67,'bank_name','15',15,'경남은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (68,'bank_name','16',16,'새마을금고','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (69,'bank_name','17',17,'신용협동조합','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (70,'bank_name','18',18,'상호저축은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (71,'bank_name','19',19,'HSBC','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (72,'bank_name','20',20,'우체국','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (73,'bank_name','21',21,'하나은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (74,'bank_name','22',22,'신한은행','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (75,'manager_status','1',1,'strRun','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (76,'manager_status','2',2,'strPause','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (77,'manager_status','3',3,'strDelete','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (78,'cash_type','1',1,'strVirtualBankAccount','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (79,'cash_type','2',2,'strCreditCard','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (80,'cash_type','3',3,'strAccountInfomation','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (81,'cash_type','4',4,'strCellPhone','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (82,'status','5',5,'strDailyBudgetLow','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (83,'status','6',6,'strCashLow','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (84,'question_fl','1',1,'strPay','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (85,'question_fl','2',2,'strError','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (86,'question_fl','3',3,'strReport','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (87,'question_fl','4',4,'strAdMgr','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (88,'question_fl','5',5,'strEtc','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (89,'question_fl','6',6,'strRefund','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (90,'question_fl','7',7,'strSecession','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (91,'question_st','1',1,'strNoAnswer','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (92,'question_st','2',2,'strTmpSave','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (93,'question_st','3',3,'strAnswerDone','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (94,'tax_publish_st','1',1,'strPublication','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (95,'tax_publish_st','2',2,'strWaitPublication','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (96,'tax_publish_st','3',3,'strNoPublication','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (97,'tax_publish_st','5',5,'strDonePublication','N');
INSERT INTO `mo_code` (`code_no`,`code_nm`,`code_key`,`code_order`,`code_desc`,`code_fl`) VALUES (98,'tax_publish_st','4',4,'strCarryOver','N');

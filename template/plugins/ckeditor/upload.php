<?php
	$up_url = "/image";
	$up_dir = "../../image";
	
	$funcNum = $_GET['CKEditorFuncNum'];
	$CKEditor = $_GET['CKEditor'];
	$langCode = $_GET['langCode'];
	
	if(isset($_FILES['upload']['tmp_name'])){
		$file_name = $_FILES['upload']['name'];
		$ext = strtolower(substr($file_name, (strrpos($file_name, '.') +1)));
		$file_name_md5 = md5($_FILES['upload']['name'].time()).".".$ext;
		$save_dir = sprintf('%s/%s', $up_dir, $file_name_md5);
		$save_url = sprintf('%s/%s', $up_url, $file_name_md5);
		if('jpg' != $ext && 'jpeg' != $ext && 'git' != $ext && 'png' != $ext){
			echo $file_name_md5;
			echo "이미지 파일만 가능 합니다.";
			return false;
		}
		
		
		if (move_uploaded_file($_FILES["upload"]["tmp_name"],$save_dir)){
			echo "<script>window.parent.CKEDITOR.tools.callFunction($funcNum, '$save_url', '업로드 완료');</script>";
		}
	}
?>
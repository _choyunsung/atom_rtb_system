<?

function get_rand ($start,$end,$count) {
    $temp = array();
    $c=0;
    while (1) { // 무한 반복
        $key = mt_rand($start,$end); // 지정 범위의 난수 구하기
        if (!$temp[$key]) { $c++; $temp[$key]='1'; } // 해당 배열값이 존재하는지 검사
        if ($c==$count) break; // 지저한 수만큼의 난수를 구했다면 종료
    }
    return array_keys($temp);
}

print_r(get_rand(0,100,6));
?>
<?php
class Service extends CI_Controller {


	public function __construct() {
		parent::__construct();

//        $_language = (!$this->session->userdata('site_lang'))?'korean':$this->session->userdata('site_lang');
//        $this->lang->load("word",$_language);
		$this->load->database();
		$this->load->helper(array('url','cookie','date'));
		$this->load->model("service_db");
		$this->load->library('user_agent');
	}

//    public function index(){
//
//    }

	function test()
	{
		$this->service_db->test();

	}

	function cookieGetUniqueViewerId($type){
		static $uniqueViewerId = null;
		if(!is_null($uniqueViewerId)) {
			return $uniqueViewerId;
		}

		if (isset($_COOKIE[$type])) {
			$uniqueViewerId = $_COOKIE[$type];
		} else{
			$uniqueViewerId = md5(uniqid('', true));  // Need to find a way to generate this...
		}
		return $uniqueViewerId;
	}

	function get_curl($url, $data, $conn_time_out=1) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $conn_time_out);
//         curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		$resData = curl_exec($ch);
		curl_close($ch);

		return $resData;
	}

 

	function post_curl($url, $data) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		//         curl_setopt($ch, CURLOPT_TIMEOUT, 1);
		$resData = curl_exec($ch);
		curl_close($ch);

		return $resData;
	}

	function c(){
		if(isset($_GET['e'])){$e = $_GET['e'];}else{$e = "";}; //광고정보
		if(isset($_GET['l'])){$l = $_GET['l'];}else{$l = "";}; //광고링크
		if(isset($_GET['r'])){$r = $_GET['r'];}else{$r = "";}; //SSP 클릭URL
		if(isset($_GET['a'])){$a = $_GET['a'];}else{$a = "";}; //agency_no
		if(isset($_GET['z'])){$z = $_GET['z'];}else{$z = "";}; //zone_no
		if(isset($_GET['i'])){$i = $_GET['i'];}else{$i = "";}; //상품코드
		if(isset($_GET['cate'])){$cate = $_GET['cate'];}else{$cate = "";}; //광고영역정보

  
		$cate_temp = urldecode($cate);
		$cate = explode(',',$cate_temp);

		$l = urldecode($l);

		$r = base64_decode($r);

		//어뷰징 방지를 위해 viewId를 생성한다.
		//echo "OAID:";
		$viewerID = $this->cookieGetUniqueViewerId("CUSER");

		if($e != ""){
			//광고정보추출
			$this->service_db->get_creative($e);
			//광고클릭로그쌓기
			//클릭카운트시킴!
			$data = array();
			$data['click_dt'] = date('Y-m-d H:i:s');
			$data['viewer_id'] = $_COOKIE['IUSER'];
			$data['cre_no'] = $e;
			$data['agency_no'] = $a;
			$data['zone_no'] = $z;
			$data['viewer_ip'] = $_SERVER['REMOTE_ADDR'];
			$data['viewer_ua'] = $_SERVER['HTTP_USER_AGENT'];
			$data['click_fl'] = "N";

			$data['adver_type'] = $_GET['adver_type'];
			$data['i'] = $i;

			$user_os = $this->user_os($_SERVER['HTTP_USER_AGENT']);
			$user_browser = $this->user_browser($_SERVER['HTTP_USER_AGENT']);
			$user_device = $this->user_device($_SERVER['HTTP_USER_AGENT']);
			$user_time = $this->user_time();

			$data['os'] = $user_os[0];
			$data['browser'] = $user_browser[0];
			$data['device'] = $user_device;
			$data['day'] = $user_time[0];
			$data['time'] = $user_time[1];

			$data['click_st'] = "";

			if(count($cate) > 0 && $cate[0] != ""){
				foreach($cate as $c){
					$data[$c] = "Y";
				}
			}

			if (isset($_COOKIE['CUSER'])) {
				//1분초과시 익스파이어 되는 쿠키생성
				$cookie = array(
								'name' => 'CUSER',
								'value' => $viewerID,
								'expire' => '60',
								'domain' => null,
								'path' => '/',
				);
				set_cookie($cookie);

				//쿠키가 만료되지 않으면 카운트 되지 않습니다.
				$data['click_fl'] = "Y";
				$data['click_st'] = "TIME";
			}else{
				//1분초과시 익스파이어 되는 쿠키생성
				$cookie = array(
					'name' => 'CUSER',
					'value' => $viewerID,
					'expire' => '60',
					'domain' => null,
					'path' => '/',
				);
				set_cookie($cookie);

				if ($this->agent->is_robot()){
					//로봇이면 카운트 되지 않습니다.
					$data['click_fl'] = "Y";
					$data['click_st'] = "ROBOT";
				}
			}

//            $this->service_db->insert_click_log($data);

		}
//
//        $data = array();
//
		if($r != ""){
			$this->get_curl($r, $data);
		}


		if($e != "" && isset($l) && $l != ""){
			gcurl(AUTION_CLICK_CHECK . urlencode($l).'&'.http_build_query($data), 1, 1);

//			if ($this->agent->is_browser('Safari'))
			if ($this->agent->is_mobile('iphone'))
			{
				echo '<script>window.location = "'.$l.'";</script>';
				die;
			}else{
				redirect($l,'location');
			}

		}

	}


	/*
	 * USER_AGENT에서 OS정보 추출
	 */
	function user_os($user_agent){
		//os 체크
		$OS = array(
			/* OS */
			array('Windows CE', 'Windows CE'),
			array('Win98', 'Windows 98'),
			array('Windows 9x', 'Windows ME'),
			array('Windows me', 'Windows ME'),
			array('Windows 98', 'Windows 98'),
			array('Windows 95', 'Windows 95'),
			array('Windows NT 6.3', 'Windows 8.1'),
			array('Windows NT 6.2', 'Windows 8'),
			array('Windows NT 6.1', 'Windows 7'),
			array('Windows NT 6.0', 'Windows Vista'),
			array('Windows NT 5.2', 'Windows 2003/XP x64'),
			array('Windows NT 5.01', 'Windows 2000 SP1'),
			array('Windows NT 5.1', 'Windows XP'),
			array('Windows NT 5', 'Windows 2000'),
			array('Windows NT', 'Windows NT'),
			array('Macintosh', 'Macintosh'),
			array('Mac_PowerPC', 'Mac PowerPC'),
			array('Unix', 'Unix'),
			array('bsd', 'BSD'),
			array('Linux', 'Linux'),
			array('Wget', 'Linux'),
			array('windows', 'ETC Windows'),
			array('mac', 'ETC Mac')
		);

		foreach($OS as $val){
			if(mb_eregi($val[0], $user_agent)){
				$os[0] = $val[0];
				$os[1] = $val[1];
				break;
			}
		}

		return $os;
	}

	/*
	 * USER_AGENT에서 BROWSER정보 추출
	 */
	function user_browser($user_agent){
		//browser 체크
		$BW = array(
			/* BROWSER */
			array('MSIE 2', 'InternetExplorer 2'),
			array('MSIE 3', 'InternetExplorer 3'),
			array('MSIE 4', 'InternetExplorer 4'),
			array('MSIE 5', 'InternetExplorer 5'),
			array('MSIE 6', 'InternetExplorer 6'),
			array('MSIE 7', 'InternetExplorer 7'),
			array('MSIE 8', 'InternetExplorer 8'),
			array('MSIE 9', 'InternetExplorer 9'),
			array('MSIE 10', 'InternetExplorer 10'),
			array('Trident/7.0', 'InternetExplorer 11'),
			array('MSIE', 'ETC InternetExplorer'),
			array('Firefox', 'FireFox'),
			array('Chrome', 'Chrome'),
			array('Safari', 'Safari'),
			array('Opera', 'Opera'),
			array('Lynx', 'Lynx'),
			array('LibWWW', 'LibWWW'),
			array('Konqueror', 'Konqueror'),
			array('Internet Ninja', 'Internet Ninja'),
			array('Download Ninja', 'Download Ninja'),
			array('WebCapture', 'WebCapture'),
			array('LTH', 'LTH Browser'),
			array('Gecko', 'Gecko compatible'),
			array('Mozilla', 'Mozilla compatible'),
			array('wget', 'Wget command')
		);

		foreach($BW as $val){
			if(mb_eregi($val[0], $user_agent)){
				$br[0] = $val[0];
				$br[1] = $val[1];
				break;
			}
		}

		return $br;
	}

	/*
	 * USER_AGENT에서 DEVICE정보 추출
	 */
	function user_device($user_agent){
		//device 체크
		$this->load->library('mobile_detect');
		$this->mobile_detect->setUserAgent($user_agent);
		$isMobile = $this->mobile_detect->isMobile();
		$isTablet = $this->mobile_detect->isTablet();

		if( $isMobile && !$isTablet )
			$dv = "Mobile";

		if( !$isMobile && $isTablet )
			$dv = "Tablet";

		if( !$isMobile && !$isTablet )
			$dv = "PC";

		return $dv;
	}

	/*
	 * Date_ymd에서 TIME정보 추출
	 */
	function user_time(){
		$timestamp = time();
		$time[0] = strtolower(date("D", $timestamp));
		$time[1] = date("G", $timestamp);
		return $time;
	}

	/*달러 -> 원화 로 변경*/
	function loc_price($input_date, $price){
		return $this->service_db->loc_price($input_date, $price);
	}

	function i(){
		if(isset($_GET['e'])){$e = $_GET['e'];}else{$e = "";}; //광고정보
		if(isset($_GET['a'])){$a = $_GET['a'];}else{$a = "";}; //매체아이정보
		if(isset($_GET['z'])){$z = $_GET['z'];}else{$z = "";}; //광고영역정보
		if(isset($_GET['cate'])){$cate = $_GET['cate'];}else{$cate = "";}; //광고영역정보
		if(isset($_GET['price'])){$price = $_GET['price'];}else{$price = "";}; //광고영역정보
		if(isset($_GET['fee'])){$fee = $_GET['fee'];}else{$fee = "";}; //광고영역정보

		$cate = urldecode($cate);
		$cate = json_decode($cate);

		$price = urldecode($price);
		$fee = urldecode($fee);

		if($e != ""){
			//광고정보추출
			//$cre = $this->service_db->get_creative($e);

			if (!isset($_COOKIE['IUSER'])) {
				//1년초과시 익스파이어 되는 쿠키생성
				$cookie = array(
					'name' => 'IUSER',
					'value' => $this->cookieGetUniqueViewerId("IUSER"),
					'expire' => '31104000',
					'domain' => null,
					'path' => '/',
				);
				set_cookie($cookie);
			}
/*
			if ($this->agent->is_robot()){
				//아이피가 로봇이면 카운트 되지 않습니다.
			}else{
				//임프레션카운트시킴!
				$data = array();
				$data['imp_dt'] = date('Y-m-d H:i:s');
				$data['viewer_id'] = $_COOKIE['IUSER'];
				$data['cre_no'] = $e;
				$data['viewer_ip'] = $_SERVER['REMOTE_ADDR'];
				$data['viewer_ua'] = $_SERVER['HTTP_USER_AGENT'];
				$this->service_db->insert_impression_log($data);
			}
*/
			//쿠키방식 - 시작
			$viewerID = $this->cookieGetUniqueViewerId("ADOP_".$e);

			$data = array();
			$data['imp_dt'] = date('Y-m-d H:i:s');
			$data['viewer_id'] = $_COOKIE['IUSER'];
			$data['cre_no'] = $e;
			$data['agency_no'] = $a;
			$data['zone_no'] = $z;
			$data['viewer_ip'] = $_SERVER['REMOTE_ADDR'];
			$data['viewer_ua'] = $_SERVER['HTTP_USER_AGENT'];

			$user_os = $this->user_os($_SERVER['HTTP_USER_AGENT']);
			$user_browser = $this->user_browser($_SERVER['HTTP_USER_AGENT']);
			$user_device = $this->user_device($_SERVER['HTTP_USER_AGENT']);
			$user_time = $this->user_time();

			$data['os'] = $user_os[0];
			$data['browser'] = $user_browser[0];
			$data['device'] = $user_device;
			$data['day'] = $user_time[0];
			$data['time'] = $user_time[1];

			$data['price'] = $price/1000;
			$data['loc_price'] = $this->loc_price(date('Y-m-d'), $price/1000);
			$data['fee'] = $fee;

			if(count($cate) > 0 && $cate[0] != ""){
				foreach($cate as $c){
					$data[$c] = "Y";
				}
			}

			$data['imp_fl'] = "N";

			if (isset($_COOKIE["ADOP_".$e])) {
				//쿠키가 만료되지 않으면 카운트 되지 않습니다.
				$data['imp_fl'] = "Y";
				$data['imp_st'] = "TIME";

				//3초초과시 익스파이어 되는 쿠키 새로 생성
				$cookie = array(
								'name' => "ADOP_".$e,
								'value' => $viewerID,
								'expire' => '3',
								'domain' => null,
								'path' => '/',
				);
				set_cookie($cookie);

			}else{
				//3초초과시 익스파이어 되는 쿠키생성
				$cookie = array(
					'name' => "ADOP_".$e,
					'value' => $viewerID,
					'expire' => '3',
					'domain' => null,
					'path' => '/',
				);
				set_cookie($cookie);

				if ($this->agent->is_robot()){
					//로봇이면 카운트 되지 않습니다.
					$data['imp_fl'] = "Y";
					$data['imp_st'] = "ROBOT";
				}
			}

			$this->service_db->insert_impression_log($data);
			//쿠키방식 - 끝
		}
	}

	public function bidding()
	{

		$jsonStr = file_get_contents("php://input");

//        $jsonStr = '{"id":193026,"at":"1","tmax":1200,"imp":[{"id":"fbbffb9ea9759a795c6bc097f540d3d8","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"300","h":"250","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]},"ptype":0}],"site":{"id":"1549","name":"rtb_top_300x250","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/RTB_0923.html","privacypolicy":"1","agency_id":"204"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_5) applewebkit\/537.36 (khtml, like gecko) chrome\/45.0.2454.101 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["AUCTION"],"cur":"USD"}';

		if (empty($jsonStr)) {
			echo '{"error" : "data empty","msg":"can not request data error"}';
			exit;
		}

		$jsonStr =  mb_convert_encoding($jsonStr, 'UTF-8');

		$req_arr = json_decode($jsonStr, TRUE);


//        if(in_array('AUCTION',$req_arr['bcate'])!==false)
			$this->bidding_auction($req_arr);
//        else
//            $this->bidding_images($req_arr);
	}

	public function bidding_images($req_arr)
	{

		if (!empty($req_arr)) {

			$reData['ssp_id'] = $req_arr['ssp_id'];
			$reData['id'] = ( $req_arr['id'] != '' ) ? trim($req_arr['id']) : '';
			$reData['imp'] = ( !empty($req_arr['imp']) ) ? json_encode($req_arr['imp']) : '';
			$reData['site'] = ( !empty($req_arr['site']) ) ? json_encode($req_arr['site']) : '';
			$reData['app'] = ( !empty($req_arr['app']) ) ? json_encode($req_arr['app']) : '';
			$reData['device'] = ( !empty($req_arr['device']) ) ? json_encode($req_arr['device']) : '';
			$reData['users'] = ( !empty($req_arr['user']) ) ? json_encode($req_arr['user']) : '';
			$reData['at'] = ( $req_arr['at'] != 0 || $req_arr['at'] != '' ) ? $req_arr['at'] : 0;
			$reData['tmax'] = ( !empty($req_arr['tmax']) ) ? $req_arr['tmax'] : 0;

			$reData['imp_bidfloor'] = $req_arr['imp'][0]['bidfloor'];
			$reData['imp_bidfloorcur'] = $req_arr['imp'][0]['bidfloorcur'];
			$reData['zone_no'] = ( !empty($req_arr['site']['id']) ) ? $req_arr['site']['id'] : '';
			$reData['agency_no'] = ( !empty($req_arr['site']['agency_id']) ) ? $req_arr['site']['agency_id'] : '';

			$reData['request_obj'] = json_encode($req_arr);
			$reData['reg_ymd'] = date('Y-m-d H:i:s');
			$reData['viewer_id'] = "";

			$this->service_db->insert_bid_request($reData);

			$reData['bcate'] = ( !empty($req_arr['bcate']) ) ? json_encode($req_arr['bcate']) : '';
			$reData['bidfloor'] = ( !empty($req_arr['imp'][0]['bidfloor']) ) ? json_encode($req_arr['imp'][0]['bidfloor']) : 0;
			$reData['w'] = ( !empty($req_arr['imp'][0]['banner']['w']) ) ? json_encode($req_arr['imp'][0]['banner']['w']) : 0;
			$reData['h'] = ( !empty($req_arr['imp'][0]['banner']['h']) ) ? json_encode($req_arr['imp'][0]['banner']['h']) : 0;
			$reData['impid'] = ( !empty($req_arr['imp'][0]['id']) ) ? json_encode($req_arr['imp'][0]['id']) : 0;

			$reData['request_page'] = $req_arr['site']['page'];
			$reData['zone_id'] = $req_arr['site']['id'];
		}

		$data = $this->service_db->insert_bid_response($reData);
		log_message('error','Res -> '.$data."\n\n\n");
		echo $data;
	}

	public function bidding_auction($req_arr)
	{

		if (!empty($req_arr)) {

			$reData['ssp_id'] = $req_arr['ssp_id'];
			$reData['id'] = ( $req_arr['id'] != '' ) ? trim($req_arr['id']) : '';
			$reData['imp'] = ( !empty($req_arr['imp']) ) ? json_encode($req_arr['imp']) : '';
			$reData['site'] = ( !empty($req_arr['site']) ) ? json_encode($req_arr['site']) : '';
			$reData['app'] = ( !empty($req_arr['app']) ) ? json_encode($req_arr['app']) : '';
			$reData['device'] = ( !empty($req_arr['device']) ) ? json_encode($req_arr['device']) : '';
			$reData['users'] = ( !empty($req_arr['user']) ) ? json_encode($req_arr['user']) : '';
			$reData['at'] = ( $req_arr['at'] != 0 || $req_arr['at'] != '' ) ? $req_arr['at'] : 0;
			$reData['tmax'] = ( !empty($req_arr['tmax']) ) ? $req_arr['tmax'] : 0;

			$reData['imp_bidfloor'] = $req_arr['imp'][0]['bidfloor'];
			$reData['imp_bidfloorcur'] = $req_arr['imp'][0]['bidfloorcur'];
			$reData['zone_no'] = ( !empty($req_arr['site']['id']) ) ? $req_arr['site']['id'] : '';
			$reData['agency_no'] = ( !empty($req_arr['site']['agency_id']) ) ? $req_arr['site']['agency_id'] : '';

			$reData['request_obj'] = json_encode($req_arr);
			$reData['reg_ymd'] = date('Y-m-d H:i:s');
			$reData['viewer_id'] = "";

//            $this->service_db->insert_bid_request($reData);

			$reData['bcate'] = ( !empty($req_arr['bcate']) ) ? json_encode($req_arr['bcate']) : '';
			$reData['bidfloor'] = ( !empty($req_arr['imp'][0]['bidfloor']) ) ? json_encode($req_arr['imp'][0]['bidfloor']) : 0;
			$reData['w'] = ( !empty($req_arr['imp'][0]['banner']['w']) ) ? json_encode($req_arr['imp'][0]['banner']['w']) : 0;
			$reData['h'] = ( !empty($req_arr['imp'][0]['banner']['h']) ) ? json_encode($req_arr['imp'][0]['banner']['h']) : 0;
			$reData['impid'] = ( !empty($req_arr['imp'][0]['id']) ) ? json_encode($req_arr['imp'][0]['id']) : 0;

			$reData['request_page'] = $req_arr['site']['loc'];
			$reData['zone_id'] = $req_arr['site']['id'];
		}

		$data = $this->service_db->insert_bid_response_auction($reData);

		echo $data;
	}


	function bidding_file_test(){
		$jsonStr = file_get_contents("php://input");

		if( empty($jsonStr) ){
			return "{'error' : 'data empty'}";
		}
	}

	function bidding_test(){


	/*
		$jsonStr = file_get_contents("php://input");

		if( empty($jsonStr) ){
			echo "{'error' : 'data empty'}";
			return;
		}

	*/
		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"300","h":"250","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8"}';
		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"300","h":"250","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (linux; android 4.4.2; shv-e330k build\/kot49h) applewebkit\/537.36 (khtml, like gecko) version\/4.0 chrome\/30.0.0.0 mobile safari\/537.36 naver(inapp; search; 340; 6.0.5)","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8"}';
		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"306","h":"100","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (linux; android 4.4.2; shv-e330k build\/kot49h) applewebkit\/537.36 (khtml, like gecko) version\/4.0 chrome\/30.0.0.0 mobile safari\/537.36 naver(inapp; search; 340; 6.0.5)","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB12","IAB24"]}';

		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (linux; android 4.4.2; shv-e330k build\/kot49h) applewebkit\/537.36 (khtml, like gecko) version\/4.0 chrome\/30.0.0.0 mobile safari\/537.36 naver(inapp; search; 340; 6.0.5)","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB12","IAB24"]}';


		//$jsonStr = '{"id":939,"at":"1","tmax":1200,"imp":[{"id":"5e0c733f7df7537ac331e2791e983a25","bidfloor":"0.0050","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"430","name":"www.82cook.com -  RTB\uc601\uc5ed","domain":"http:\/\/www.82cook.com","page":"http:\/\/portal.adop.co.kr\/adspace\/adspace_script_test_screen","privacypolicy":"1","ref":"http:\/\/portal.adop.co.kr\/adspace\/adspace_script_test","agency_id":"35"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_3) applewebkit\/537.36 (khtml, like gecko) chrome\/43.0.2357.134 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB22"]}';

		//$jsonStr = '{"id":1955,"at":"1","tmax":1200,"imp":[{"id":"01be55ec9b8641b6ef96e14ec7911df0","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"442","name":"adop.cc - \uae30\ubcf8\uc124\uc815","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/janey1.html","privacypolicy":"1","agency_id":"58"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/44.0.2403.130 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB20","IAB18"]}';
		//$jsonStr = '{"id":1975,"at":"1","tmax":1200,"imp":[{"id":"bcda14f78b1462101df49ed08cdff533","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"442","name":"adop.cc - \uae30\ubcf8\uc124\uc815","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/janey1.html","privacypolicy":"1","agency_id":"58"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/44.0.2403.130 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB20","IAB18"]}';
		//$jsonStr = '{"id":2604,"at":"1","tmax":1200,"imp":[{"id":"22d512aaed6fd9db663058980979d1fe","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"447","name":"\ud14c\uc2a4\ud2b8\uc601\uc5ed - 200*200","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/200*200.html","privacypolicy":"1","agency_id":"62"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (iphone; cpu iphone os 8_4_1 like mac os x) applewebkit\/600.1.4 (khtml, like gecko) mobile\/12h321 kakaotalk 5.1.0","ip":"223.33.165.34","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB17"]}';

		//$jsonStr = '{"id":190956,"at":"1","tmax":1200,"imp":[{"id":"2e9bc025256306da4fcdf852c9fb5990","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"336","h":"280","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]},"ptype":0}],"site":{"id":"1454","name":"\uc544\ud1b0\uc2e4\uc11c\ubc84\ud14c\uc2a4\ud2b8-336*280","domain":"http:\/\/adop.cc","privacypolicy":"1","agency_id":"201"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/45.0.2454.85 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":[a"IAB20"],"cur":"USD"}';
		//$jsonStr = '{"id":191016,"at":"1","tmax":1200,"imp":[{"id":"6f79ca6c2d7ebe329cdca8c1d699b45d","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"250","h":"250","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]},"ptype":0}],"site":{"id":"1452","name":"\uc544\ud1b0\uc2e4\uc11c\ubc84\ud14c\uc2a4\ud2b8-250*250","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/%EC%8B%A4%EC%84%9C%EB%B2%84_%EC%97%AC%ED%96%89_250*250.html","privacypolicy":"1","agency_id":"201"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/45.0.2454.85 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB20"],"cur":"USD"}';

		$jsonStr = '{"id":191049,"at":"1","tmax":1200,"imp":[{"id":"65de306e871b66b64eb6ea30fe7c6a6f","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"970","h":"90","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]},"ptype":0}],"site":{"id":"1456","name":"\uc544\ud1b0\uc2e4\uc11c\ubc84\ud14c\uc2a4\ud2b8-970*90","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/%EC%8B%A4%EC%84%9C%EB%B2%84_%EC%97%AC%ED%96%89_970*90.html","privacypolicy":"1","agency_id":"201"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/45.0.2454.85 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB20"],"cur":"USD"}';
		$req_arr = json_decode($jsonStr, TRUE);

		if (!empty($req_arr)) {

			$reData['ssp_id'] = $req_arr['ssp_id'];
			$reData['id'] = ( $req_arr['id'] != '' ) ? trim($req_arr['id']) : '';
			$reData['imp'] = ( !empty($req_arr['imp']) ) ? json_encode($req_arr['imp']) : '';
			$reData['site'] = ( !empty($req_arr['site']) ) ? json_encode($req_arr['site']) : '';
			$reData['app'] = ( !empty($req_arr['app']) ) ? json_encode($req_arr['app']) : '';
			$reData['device'] = ( !empty($req_arr['device']) ) ? json_encode($req_arr['device']) : '';
			$reData['users'] = ( !empty($req_arr['user']) ) ? json_encode($req_arr['user']) : '';
			$reData['at'] = ( $req_arr['at'] != 0 || $req_arr['at'] != '' ) ? $req_arr['at'] : 0;
			$reData['tmax'] = ( !empty($req_arr['tmax']) ) ? $req_arr['tmax'] : 0;

			$reData['imp_bidfloor'] = $req_arr['imp'][0]['bidfloor'];
			$reData['imp_bidfloorcur'] = $req_arr['imp'][0]['bidfloorcur'];
			$reData['zone_no'] = ( !empty($req_arr['site']['id']) ) ? $req_arr['site']['id'] : '';
			$reData['agency_no'] = ( !empty($req_arr['site']['agency_id']) ) ? $req_arr['site']['agency_id'] : '';

			$reData['request_obj'] = json_encode($req_arr);
			$reData['reg_ymd'] = date('Y-m-d H:i:s');
			$reData['viewer_id'] = "";

			$this->service_db->insert_bid_request($reData);

			$reData['bcate'] = ( !empty($req_arr['bcate']) ) ? json_encode($req_arr['bcate']) : '';
			$reData['bidfloor'] = ( !empty($req_arr['imp'][0]['bidfloor']) ) ? json_encode($req_arr['imp'][0]['bidfloor']) : 0;
			$reData['w'] = ( !empty($req_arr['imp'][0]['banner']['w']) ) ? json_encode($req_arr['imp'][0]['banner']['w']) : 0;
			$reData['h'] = ( !empty($req_arr['imp'][0]['banner']['h']) ) ? json_encode($req_arr['imp'][0]['banner']['h']) : 0;
			$reData['impid'] = ( !empty($req_arr['imp'][0]['id']) ) ? json_encode($req_arr['imp'][0]['id']) : 0;
		}

		$data = $this->service_db->insert_bid_response($reData);

		//echo $data;
		$this->output->enable_profiler(TRUE);
	}


	function bidding_fail(){


		/*
		 $jsonStr = file_get_contents("php://input");

		 if( empty($jsonStr) ){
		 return "{'error' : 'data empty'}";
		 }

		 */
		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"300","h":"250","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8"}';
		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"300","h":"250","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (linux; android 4.4.2; shv-e330k build\/kot49h) applewebkit\/537.36 (khtml, like gecko) version\/4.0 chrome\/30.0.0.0 mobile safari\/537.36 naver(inapp; search; 340; 6.0.5)","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8"}';
		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"306","h":"100","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (linux; android 4.4.2; shv-e330k build\/kot49h) applewebkit\/537.36 (khtml, like gecko) version\/4.0 chrome\/30.0.0.0 mobile safari\/537.36 naver(inapp; search; 340; 6.0.5)","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB12","IAB24"]}';

		//$jsonStr = '{"id":189997,"at":"1","tmax":1200,"imp":[{"id":"118412cd2b2fb79298b75d1e414df4b2","bidfloor":"0.0001","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,2],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"792","name":"\ubaa8\ubc14\uc77c_\ud558\ub2e8_300*250_RTB","domain":"http:\/\/m.iusm.co.kr\/","page":"http:\/\/m.iusm.co.kr\/news\/articleView.html?idxno=595906","privacypolicy":"1","ref":"http:\/\/www.iusm.co.kr\/news\/articleView.html?idxno=595906","agency_id":"54"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (linux; android 4.4.2; shv-e330k build\/kot49h) applewebkit\/537.36 (khtml, like gecko) version\/4.0 chrome\/30.0.0.0 mobile safari\/537.36 naver(inapp; search; 340; 6.0.5)","ip":"200.85.83.12","Os":"linux","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB12","IAB24"]}';


		//$jsonStr = '{"id":939,"at":"1","tmax":1200,"imp":[{"id":"5e0c733f7df7537ac331e2791e983a25","bidfloor":"0.0050","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"430","name":"www.82cook.com -  RTB\uc601\uc5ed","domain":"http:\/\/www.82cook.com","page":"http:\/\/portal.adop.co.kr\/adspace\/adspace_script_test_screen","privacypolicy":"1","ref":"http:\/\/portal.adop.co.kr\/adspace\/adspace_script_test","agency_id":"35"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_3) applewebkit\/537.36 (khtml, like gecko) chrome\/43.0.2357.134 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB22"]}';

		//$jsonStr = '{"id":1955,"at":"1","tmax":1200,"imp":[{"id":"01be55ec9b8641b6ef96e14ec7911df0","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"442","name":"adop.cc - \uae30\ubcf8\uc124\uc815","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/janey1.html","privacypolicy":"1","agency_id":"58"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/44.0.2403.130 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB20","IAB18"]}';
		//$jsonStr = '{"id":1975,"at":"1","tmax":1200,"imp":[{"id":"bcda14f78b1462101df49ed08cdff533","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"200","h":"200","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"442","name":"adop.cc - \uae30\ubcf8\uc124\uc815","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/janey1.html","privacypolicy":"1","agency_id":"58"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (macintosh; intel mac os x 10_10_4) applewebkit\/537.36 (khtml, like gecko) chrome\/44.0.2403.130 safari\/537.36","ip":"112.221.85.219","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB20","IAB18"]}';
		$jsonStr = '{"id":2604,"at":"1","tmax":1200,"imp":[{"id":"22d512aaed6fd9db663058980979d1fe","bidfloor":"0.0100","bidfloorcur":"USD","banner":{"w":"250","h":"280","pos":"1","btype":[1,3,4],"battr":[1,2,3,4,5,6,7,8,9,12,15,16]}}],"site":{"id":"447","name":"\ud14c\uc2a4\ud2b8\uc601\uc5ed - 200*200","domain":"http:\/\/adop.cc","page":"http:\/\/portal.adop.co.kr\/upload\/adtag\/200*200.html","privacypolicy":"1","agency_id":"62"},"user":{"id":"45asdf987656789adfad4678rew656789","buyeruid":"5df678asd8987656asdf78987654","geo":{"type":2}},"device":{"ua":"mozilla\/5.0 (iphone; cpu iphone os 8_4_1 like mac os x) applewebkit\/600.1.4 (khtml, like gecko) mobile\/12h321 kakaotalk 5.1.0","ip":"223.33.165.34","Os":"Unknown","Osv":"unknown","Js":"1","devicetype":"2","geo":{"type":2}},"ssp_id":"8","bcate":["IAB17"]}';
		$req_arr = json_decode($jsonStr, TRUE);

		if (!empty($req_arr)) {

			$reData['ssp_id'] = $req_arr['ssp_id'];
			$reData['id'] = ( $req_arr['id'] != '' ) ? trim($req_arr['id']) : '';
			$reData['imp'] = ( !empty($req_arr['imp']) ) ? json_encode($req_arr['imp']) : '';
			$reData['site'] = ( !empty($req_arr['site']) ) ? json_encode($req_arr['site']) : '';
			$reData['app'] = ( !empty($req_arr['app']) ) ? json_encode($req_arr['app']) : '';
			$reData['device'] = ( !empty($req_arr['device']) ) ? json_encode($req_arr['device']) : '';
			$reData['users'] = ( !empty($req_arr['user']) ) ? json_encode($req_arr['user']) : '';
			$reData['at'] = ( $req_arr['at'] != 0 || $req_arr['at'] != '' ) ? $req_arr['at'] : 0;
			$reData['tmax'] = ( !empty($req_arr['tmax']) ) ? $req_arr['tmax'] : 0;

			$reData['imp_bidfloor'] = $req_arr['imp'][0]['bidfloor'];
			$reData['imp_bidfloorcur'] = $req_arr['imp'][0]['bidfloorcur'];
			$reData['zone_no'] = ( !empty($req_arr['site']['id']) ) ? $req_arr['site']['id'] : '';
			$reData['agency_no'] = ( !empty($req_arr['site']['agency_id']) ) ? $req_arr['site']['agency_id'] : '';

			$reData['request_obj'] = json_encode($req_arr);
			$reData['reg_ymd'] = date('Y-m-d H:i:s');
			$reData['viewer_id'] = "";

			$this->service_db->insert_bid_request($reData);

			$reData['bcate'] = ( !empty($req_arr['bcate']) ) ? json_encode($req_arr['bcate']) : '';
			$reData['bidfloor'] = ( !empty($req_arr['imp'][0]['bidfloor']) ) ? json_encode($req_arr['imp'][0]['bidfloor']) : 0;
			$reData['w'] = ( !empty($req_arr['imp'][0]['banner']['w']) ) ? json_encode($req_arr['imp'][0]['banner']['w']) : 0;
			$reData['h'] = ( !empty($req_arr['imp'][0]['banner']['h']) ) ? json_encode($req_arr['imp'][0]['banner']['h']) : 0;
			$reData['impid'] = ( !empty($req_arr['imp'][0]['id']) ) ? json_encode($req_arr['imp'][0]['id']) : 0;
		}

		$data = $this->service_db->insert_bid_response($reData);

		echo $data;
	}
	//국제 통화 환율을 업데이트
	// URL/service/get_exchange/USD/KRW
	function get_exchange($from_currency = null, $to_currency = null){

		if(!isset($from_currency))
			return "";

		if(!isset($to_currency))
			return "";

		$date_str = "%Y-%m-%d %H:%i:%s";
		$date_str_search = "%Y-%m-%d";
		$time = time();

		$input_date = mdate($date_str, $time);
		$input_date_search = mdate($date_str_search, $time);

		$content = file_get_contents('https://www.google.com/finance/converter?a=1&from='.$from_currency.'&to='.$to_currency);
		$doc = new DOMDocument();
		@$doc->loadHTML($content);
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//*[@id="currency_converter_result"]/span')->item(0)->nodeValue;
		$exchange_rate = str_replace( $to_currency, '', $result);

		$data['from_code_key'] = $from_currency;
		$data['to_code_key'] = $to_currency;
		$data['exchange_rate'] = $exchange_rate;
		$data['input_dt'] = $input_date;
		$data['local_opt'] = "L";

		$isexchange = $this->service_db->get_exchange($from_currency, $to_currency, $input_date_search);

		if(!$isexchange){
			$this->service_db->insert_exchange($data);
			echo $exchange_rate;
		}else{
			$this->service_db->update_exchange($data);
			echo $exchange_rate;
		}
	}

	//국제 통화 환율을 업데이트
	// URL/service/update_bid_price
	function update_bid_price(){
		//$date_str = "%Y-%m-%d %H:%i:%s";
		$date_str_search = "%Y-%m-%d";
		$time = time();
		$input_date_search = mdate($date_str_search, $time);
		$this->service_db->update_bid_price($input_date_search);
		$this->service_db->update_bid_loc_price("KRW", $input_date_search);
	}

	//비딩 데이터를 정리 매일 0시 5분마다 실행
	//어제 데이터 시간별 정리
	/*
		-> impression_log 에 쌓여있는 모든 데이터 대상
		* impression_data 일자, 시간별 임프레션 수 업데이트 (차후 시간별 데이터 추출및 활용)
		-> click_log 에 쌓여있는 모든 데이터 대상
		* click_data 일자, 시간별 임프레션 수 업데이트 (차후 시간별 데이터 추출및 활용)
	*/

 
	function arrange_report_data(){
		if(isset($_GET['pw']) && $_GET['pw'] = '1234'){
			$this->service_db->arrange_report_data();
		}
	}

	function arrange_calculate_tax(){
		$this->service_db->arrange_calculate_tax();
	}

	/*
	 * 일일예산초과로 정지된 광고를 하루 초과후 진행으로 바꾸어줍니다.
	 * 캠페인/광고그룹/광고 -> 일예산초과(5) -> 진행(1)
	 */
	function active_daily_budget_low(){
		$this->service_db->active_daily_budget_low();
	}

	function init_data(){
		//return;
		$this->service_db->init_data();
	}

	function init_all_data(){
		//return;
		$this->service_db->init_all_data();
	}

	function alarm_confirm(){
		$alarm_no = $this->input->post('alarm_no');
		$this->service_db->alarm_confirm($alarm_no);
		return ture;
	}

	function alarm_test(){

		$alarm['mem_email'] = "dongha9878@naver.com";
		$alarm['email_alarm_c0_fl'] = "Y";

		$this->service_db->alarm(2, 1, $alarm);
		/*
		$alarm['mem_email'] = "sam@adop.co.kr";
		$alarm['email_alarm_c0_fl'] = "Y";

		$this->service_db->alarm(4, 1, $alarm);

		$alarm['mem_email'] = "seonu@adop.co.kr";
		$alarm['email_alarm_c0_fl'] = "Y";

		$this->service_db->alarm(5, 1, $alarm);

		$alarm['mem_email'] = "janey@adop.co.kr";
		$alarm['email_alarm_c0_fl'] = "Y";

		$this->service_db->alarm(6, 1, $alarm);
		*/
	}

	function arrange_revenue_data(){
		$this->service_db->arrange_revenue_data();
	}

	function arrange_data(){
		if(isset($_GET['pw']) && $_GET['pw'] = '1234'){

			$data = array();
			//적용할 URL
			$url = base_url()."service/arrange_apply";

			//적용
			$result = $this->post_curl($url, $data);

			$result = json_decode($result, TRUE);

			//적용이 끝나면 적용된 데이터만 삭제처리
			if($result['ok']){

				$result_ = $this->arrange_apply_complete($result);

				if($result_['ok']){
					$this->arrange_data_complete();
				}
			}

			echo "complete";
		}
	}

	function arrange_apply(){
		$result['ok'] = false;
		$result = $this->service_db->arrange_data();
		echo json_encode($result);
	}

	function arrange_apply_complete($data){
		$result = $this->service_db->arrange_apply_complete($data);
		return $result;
	}

	function arrange_data_complete(){
		$this->service_db->arrange_data_complete();
	}

	function arrange_agency_no(){
		$this->service_db->arrange_agency_no();
	}


}

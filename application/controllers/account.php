<?php
class Account extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
                'form',
                'url'
        ));
        $this->load->model("member_db");
        $this->load->model("menu_db");
        $this->load->model("code_db");
        $this->load->model("campaign_db");
        $this->load->model("creative_db");
        $this->load->model("account_db");
        $this->load->model("admanagement_db");
        
        $this->load->library('menu');

    }
    
    
    public function account_master_view(){
        $mem_no = $this->session->userdata('mem_no');
        $data['master_info'] = $this->account_db->select_master_info($mem_no);
        $data['mem_no'] = $mem_no;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_master_view'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_master_view', $data);
        $this->load->view('common/footer');
    }
    
    public function account_master_modify(){
        $mem_no = $this->session->userdata('mem_no');
        $data['master_info'] = $this->account_db->select_master_info($mem_no);
        $data['sel_bank_name'] =$this->code_db->sel_bank_code();
        $data['cell_no_list'] = $this->code_db->sel_cell_no_code();
        $data['tel_no_list'] = $this->code_db->sel_tel_no_code();
        $data['mem_no'] = $mem_no;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_master_view'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_master_modify', $data);
        $this->load->view('common/footer');
    }
    
    public function account_master_pwd_save() {
    
        $mem_no = $this->input->post('mem_no');
        $tmp_pwd = $this->input->post('tmp_pwd');
        $mem_pwd = $this->input->post('mem_pwd');
        $pwd_check = $this->account_db->check_master_pwd($mem_no, $tmp_pwd);
        if ($pwd_check == "ok"){
            $result = $this->account_db->update_master_pwd($mem_no, $mem_pwd);
            if ($result == "ok"){
                echo "OK";
            }else{
                echo "FALSE";
            }
        }else{
            echo "check_false";
        }
        exit;
    }
    
    public function master_info_modify_save(){
        $mem_no = $this->input->post('mem_no');
        $mem_post1 = $this->input->post('mem_post1');
        $mem_post2 = $this->input->post('mem_post2');
        $tax_mem_tel1 = $this->input->post('tax_mem_tel1');
        $tax_mem_tel2 = $this->input->post('tax_mem_tel2');
        $tax_mem_tel3 = $this->input->post('tax_mem_tel3');
        $tax_mem_cell1 = $this->input->post('tax_mem_cell1');
        $tax_mem_cell2 = $this->input->post('tax_mem_cell2');
        $tax_mem_cell3 = $this->input->post('tax_mem_cell3');
        
        $data['mem_com_ceo'] = $this->input->post('mem_com_ceo');
        $data['mem_com_type'] = $this->input->post('mem_com_type');
        $data['mem_com_item'] = $this->input->post('mem_com_item');
        $data['mem_post'] = $mem_post1."-".$mem_post2;
        $data['mem_addr'] = $this->input->post('mem_addr');
        $data['bank_nm'] = $this->input->post('bank_nm');
        $data['bank_no'] = $this->input->post('bank_no');
        $data['bank_mem_nm'] = $this->input->post('bank_mem_nm');
        $data['tax_mem_nm'] = $this->input->post('tax_mem_nm');
        $data['tax_mem_email'] = $this->input->post('tax_mem_email');
        if ($tax_mem_tel1 != ""){
            $data['tax_mem_tel'] = $tax_mem_tel1."-".$tax_mem_tel2."-".$tax_mem_tel3;
        }
        if ($tax_mem_cell1 != ""){
            $data['tax_mem_cell'] = $tax_mem_cell1."-".$tax_mem_cell2."-".$tax_mem_cell3;
        }
        $data['sms_alarm_c10_fl'] = $this->input->post('sms_alarm_c10_fl');
        $data['sms_alarm_d10_fl'] = $this->input->post('sms_alarm_d10_fl');
        $data['sms_alarm_c0_fl'] = $this->input->post('sms_alarm_c0_fl');
        $data['email_alarm_c10_fl'] = $this->input->post('email_alarm_c10_fl');
        $data['email_alarm_d10_fl'] = $this->input->post('email_alarm_d10_fl');
        $data['email_alarm_c0_fl'] = $this->input->post('email_alarm_c0_fl');
        
        $result = $this->account_db->master_info_modify_save($mem_no, $data);
        redirect('/account/account_master_view', 'refresh');
    }
    
    public function account_manager_list(){
        $mem_no = $this->session->flashdata('master_no');
        if ($mem_no == ""){
            $mem_no = $this->session->userdata('mem_no');
        }
        $data['mem_no'] = $mem_no;
        $data['manager_list'] = $this->account_db->select_account_manager_list($mem_no);
        $data['sel_manager_status'] = $this->code_db->sel_manager_status_code();

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_master_view'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_manager_list', $data);
        $this->load->view('common/footer');
    }
    
    public function manager_add(){
        $data['mem_id'] = $this->input->post('mem_id');
        $data['mem_pwd'] = md5($this->input->post('mem_pwd'));
        $data['mem_cont'] = $this->input->post('mem_cont');
        $data['mem_type'] = $this->input->post('mem_type');
        $data['mem_active_st'] = '1';
        $data['mem_ymd'] = date('Y-m-d H:i:s');
        $data['sms_alarm_c10_fl'] = 'Y';
        $data['sms_alarm_d10_fl'] = 'Y';
        $data['sms_alarm_c0_fl'] = 'Y';
        $data['email_alarm_c10_fl'] = 'Y';
        $data['email_alarm_d10_fl'] = 'Y';
        $data['email_alarm_c0_fl'] = 'Y';

        $master_mem_no = $this->input->post('master_mem_no');
        $master_info = $this->account_db->select_master_info($master_mem_no);
        $data['mem_com_nm'] = $master_info['mem_com_nm'];
        $data['mem_com_no'] = $master_info['mem_com_no'];
        $data['mem_com_ceo'] = $master_info['mem_com_ceo'];
        $data['mem_com_type'] = $master_info['mem_com_type'];
        $data['mem_com_item'] = $master_info['mem_com_item'];
        $data['mem_com_site_nm'] = $master_info['mem_com_site_nm'];
        $data['tax_mem_nm'] = $master_info['tax_mem_nm'];
        $data['tax_mem_email'] = $master_info['tax_mem_email'];
        $data['tax_mem_cell'] = $master_info['tax_mem_cell'];
        $data['group_nm'] = $master_info['group_nm'];
        $data['group_no'] = $master_info['group_no'];
        $data['mem_pay_later'] = $master_info['mem_pay_later'];
        $data['agency_no'] = $master_mem_no;
        
        //차후 매니저 이메일 등록
        $data['mem_email'] = $master_info['mem_email'];
        $data['mem_nm'] = $master_info['mem_nm']." manager";
        $data['mem_post'] = $master_info['mem_post'];
        $data['mem_addr'] = $master_info['mem_addr'];
        $data['mem_tel'] = $master_info['mem_tel'];
        $data['mem_cell'] = $master_info['mem_cell'];
        $data['mem_com_url'] = $master_info['mem_com_url'];
        $data['admin_fl'] = $master_info['admin_fl'];
        $data['fee_fl'] = $master_info['fee_fl'];
        $data['mem_com_fee'] = $master_info['mem_com_fee'];
        
        $data['role'] = $master_info['role'];
        
        $manager_mem_no = $this->account_db->insert_manager_account($data, $master_mem_no);
        //첫등록시 매니저 권한으로 권한설정
        if($data['role']  == "agency"){
            $this->menu_db->insert_category_member(AGENCY_MANAGER_GROUP_NUMBER, $manager_mem_no);
        }else if($data['role']  == "lab"){
            $this->menu_db->insert_category_member(LAB_MANAGER_GROUP_NUMBER, $manager_mem_no);
        }else{
            $this->menu_db->insert_category_member(ADVER_MANAGER_GROUP_NUMBER, $manager_mem_no);
        }
        
        if ($manager_mem_no > 0){
            echo "ok";
        }else{
            echo "false";
        }
    }
    
    public function account_manager_modify(){
        $mem_no = $this->input->post('manager_no');
        $master_mem_no = $this->input->post('master_no');
        $data['manager_info'] = $this->account_db->select_account_manager_info($mem_no);
        $data['mem_no'] = $mem_no;
        $data['master_mem_no'] = $master_mem_no;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_master_view'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_manager_modify', $data);
        $this->load->view('common/footer');
        
    }
    
    public function sel_manager_status_change(){
        $mem_no = $this->input->post('mem_no');
        $master_no = $this->input->post('master_no');
        $status_key = $this->input->post('status_key');
        if ($mem_no == "all"){
            $mem_no_arr = $this->input->post('sel_manager_no');
            $data['mem_no_arr'] = implode(",", $mem_no_arr);
        }else{
            $data['mem_no_arr'] = $mem_no;
        }
        $data['status_key'] = $status_key;
    
        $result = $this->account_db->manager_status_change($data);
        $this->session->set_flashdata('master_no', $master_no);
        redirect('/account/account_manager_list/', 'refresh');
    
    }
    
    public function account_manager_view(){
        
        $mem_no = $this->session->userdata('mem_no');
        $data['manager_info'] = $this->account_db->select_account_manager_info($mem_no);
        $data['mem_no'] = $mem_no;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_master_view'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_manager_view', $data);
        $this->load->view('common/footer');
    
    }
    
    public function manager_info_modify_save(){
        $mem_no = $this->input->post('mem_no');
        $master_mem_no = $this->input->post('master_mem_no');
        $data['mem_nm'] = $this->input->post('mem_nm');
        $data['mem_email'] = $this->input->post('mem_email');
        $data['mem_tel'] = $this->input->post('mem_tel');
        $data['mem_cell'] = $this->input->post('mem_cell');
        $data['mem_cont'] = $this->input->post('mem_cont');
        $data['sms_alarm_c10_fl'] = $this->input->post('sms_alarm_c10_fl');
        $data['sms_alarm_d10_fl'] = $this->input->post('sms_alarm_d10_fl');
        $data['sms_alarm_c0_fl'] = $this->input->post('sms_alarm_c0_fl');
        $data['email_alarm_c10_fl'] = $this->input->post('email_alarm_c10_fl');
        $data['email_alarm_d10_fl'] = $this->input->post('email_alarm_d10_fl');
        $data['email_alarm_c0_fl'] = $this->input->post('email_alarm_c0_fl');
        
        $result = $this->account_db->update_manager_info($data, $mem_no);
        if ($master_mem_no == ""){
            redirect('/account/account_manager_view/', 'refresh');
        }else{
            $this->session->set_flashdata('master_no', $master_mem_no);
            redirect('/account/account_manager_list/', 'refresh');
        }
    }
    
    
    public function account_login_list(){
        $mem_no = $this->session->userdata('mem_no');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        
        $data['total_rows'] = $this->account_db->select_login_list_count($mem_no, $from_date, $to_date);
        $this->load->library('pagination');
        $config['base_url'] = '/account/account_login_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        $data['login_list'] = $this->account_db->select_account_login_list($mem_no, $from_date, $to_date, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_master_view'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_login_list', $data);
        $this->load->view('common/footer');
    }
    
    public function account_setting(){
        $mem_no = $this->session->userdata('mem_no');
        $data['account_setting'] = $this->account_db->select_master_info($mem_no);
        $data['sel_country'] = $this->code_db->sel_country_code();
        $data['sel_timezone'] = $this->code_db->sel_timezone_code();
        $data['mem_no'] = $mem_no;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_setting'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_setting', $data);
        $this->load->view('common/footer');
    }
    
    public function account_setting_save(){
        $data['mem_cur'] = $this->input->post('mem_cur');
//         $data['mem_timezone'] = $this->input->post('mem_timezone');
        $data['mem_lang'] = $this->input->post('mem_lang');
        $mem_no = $this->input->post('mem_no');
        
        $result=$this->account_db->master_info_modify_save($mem_no, $data);
        if ($result == "ok"){
            if ($data['mem_lang'] == 'korean'){
                $this->session->set_userdata('site_lang', 'korean');
            }elseif ($data['mem_lang'] == 'english'){
                $this->session->set_userdata('site_lang', 'english');
            }
            redirect('/account/account_setting', 'refresh');
        }
    }
    
    public function account_advertiser_list(){
        $mem_no = $this->session->userdata('mem_no');
        $data['mem_no'] = $mem_no;
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 10;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->account_db->select_advertiser_list_count($mem_no);
        $this->load->library('pagination');
        $config['base_url'] = '/account/account_advertiser_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
        $data['adver_list'] = $this->account_db->select_account_advertiser_list($mem_no, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_advertiser_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_advertiser_list', $data);
        $this->load->view('common/footer');
    }
    
    public function account_advertiser_modify(){
        $master_no = $this->session->userdata('mem_no');
        $adver_no = $this->input->post('adver_no');
        $data['adver_info'] = $this->account_db->select_master_info($adver_no);
        $data['mem_no'] = $adver_no;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_advertiser_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_advertiser_modify', $data);
        $this->load->view('common/footer');
    }
    
    
    public function advertiser_modify_save(){
        $data['mem_com_nm'] = $this->input->post('mem_com_nm');
        $data['mem_com_url'] = $this->input->post('mem_com_url');
        $data['mem_nm'] = $this->input->post('mem_nm');
        $data['mem_email'] = $this->input->post('mem_email');
        $data['mem_tel'] = $this->input->post('mem_tel');
        $data['mem_cell'] = $this->input->post('mem_cell');
        $data['mem_com_no'] = $this->input->post('mem_com_no');
        $data['mem_addr'] = $this->input->post('mem_addr');
        $mem_no = $this->input->post('mem_no');
        
        $result = $this->account_db->advertiser_modify_save($mem_no, $data);
        if ($result == "ok"){
            redirect('/account/account_advertiser_list', 'refresh');
        }
        
    }
    
    public function new_advertiser_save(){
        $data['mem_id'] = "adver";
        $data['mem_gb'] = "ADT";
        $data['mem_com_nm'] = $this->input->post('mem_com_nm');
        $data['mem_com_url'] = $this->input->post('mem_com_url');
        $data['mem_nm'] = $this->input->post('mem_nm');
        $data['mem_tel'] = $this->input->post('mem_tel');
        $data['mem_cell'] = $this->input->post('mem_cell');
        $data['mem_email'] = $this->input->post('mem_email');
        $data['mem_com_no'] = $this->input->post('mem_com_no');
        $data['mem_addr'] = $this->input->post('mem_addr');
        $data['mem_ymd'] = date('Y-m-d');
        $adver_no = $this->campaign_db->insert_new_advertiser($data);
        $group['agency_no'] = $this->input->post('mem_no');
        $group['adver_no'] = $adver_no;
        if ($adver_no > 0){
            $mem_group_no = $this->campaign_db->insert_new_member_group($group);
        }
    
        if ($mem_group_no > 0){
            echo "ok";
        }
    }

    public function account_list(){
        $agency_yn = $this->input->post('agency_yn');
        $lab_yn = $this->input->post('lab_yn');
        $ind_yn = $this->input->post('ind_yn');
        $adver_yn = $this->input->post('adver_yn');
        $mem_active_st = $this->input->post('mem_active_st');
        $master_yn = $this->input->post('master_yn');
        $manager_yn = $this->input->post('manager_yn');
        $mem_com_nm = $this->input->post('mem_com_nm');
        $mem_id = $this->input->post('mem_id');
        $last_connect = $this->input->post('last_connect');
        $sales_no = $this->input->post('sales_no');
        $fromto_date = $this->input->post('fromto_date');
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];

        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        
        $condition = array (
                "master_yn" => $master_yn,
                "mem_com_nm" => $mem_com_nm,
                "manager_yn" => $manager_yn,
                "mem_id" => $mem_id,
                "mem_active_st" => $mem_active_st,
                "last_connect" => $last_connect,
                "fromto_date" => $fromto_date,
                "from_date" => $from_date,
                "to_date" => $to_date,
                "agency_yn" => $agency_yn,
                "lab_yn" => $lab_yn,
                "ind_yn" => $ind_yn,
                "adver_yn" => $adver_yn,
                "sales_no" => $sales_no
        );
        
        $data['cond'] = $condition;
        $data['total_rows'] = $this->account_db->select_account_list_count($condition);
        
        $this->load->library('pagination');
        $config['base_url'] = '/account/account_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if ($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        $data['account_summary'] = $this->account_db->select_account_list_summary();
        $data['account_list'] = $this->account_db->select_account_list($condition, $data['per_page'], $data['page_num']);

        $data['sales_list'] = $this->account_db->select_sales_list();
        if (count($data['account_list']) > 0){
            $data['page_links'] = $this->pagination->create_links();
        }
        
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/account/account_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_list', $data);
        $this->load->view('common/footer');
    }

    public function account_sales_modify(){
        $data['sales_no'] = $this->input->post('sales_no');
        $data['mem_no'] = $this->input->post('mem_no');

        $result = $this->account_db->account_sales_modify($data);

        if ($result == "ok"){
            $http_referer = $this->input->server('HTTP_REFERER');
            $from = isset($http_referer) ? $http_referer : '/account/account_list';
            redirect($from, 'refresh');
        }
    }

    public function account_active_modify(){
        $data['active_no'] = $this->input->post('active_no');
        $data['mem_no'] = $this->input->post('mem_no');

        $result = $this->account_db->account_active_modify($data);

        if ($result == "ok"){
            $http_referer = $this->input->server('HTTP_REFERER');
            $from = isset($http_referer) ? $http_referer : '/account/account_list';
            redirect($from, 'refresh');
        }
    }
    
    public function account_type_modify(){
        $data['type'] = $this->input->post('type');
        $data['mem_no'] = $this->input->post('mem_no');
        $result = $this->account_db->account_mem_type_modify($data);
        
        if ($result == "ok"){
            $http_referer = $this->input->server('HTTP_REFERER');
            $from = isset($http_referer) ? $http_referer : '/account/account_list';
            redirect($from, 'refresh');
        }
    }
    
    public function account_info_detail(){
        $mem_no = $this->input->post('mem_no');
        $data['detail_info'] = $this->account_db->select_account_info($mem_no);
        
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/account/account_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header', $menu_data);
        $this->load->view('account/account_info_view', $data);
        $this->load->view('common/footer');
    }
}
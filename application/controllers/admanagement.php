<?php
class Admanagement extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->database();
        $this->load->model("admanagement_db");
  
        $this->load->library('menu');
    }

    public function index()
    {
    }

    function update_content_category() {
        $this->admanagement_db->update_content_category();
    }

    function targeting_add_form() {
        $data['cre_gp_no'] = $this->input->get('cre_gp_no');
        $data['cam_no'] = $this->input->get('cam_no');
        $data['os'] = $this->admanagement_db->get_os();
        $data['browser'] = $this->admanagement_db->get_browser();
        $data['device'] = $this->admanagement_db->get_device();
        $data['category'] = $this->admanagement_db->get_content_category();
        $mem_no = $this->session->userdata('mem_no');

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_group_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);

        $data['write_fl'] = $auth[0]['write_fl'];
        $this->load->view("creative/targeting_add_form", $data);
        
        $this->load->view('common/footer');
    }

    function targeting_add() {
        $data = array();
        $cre_gp_no = $this->input->post('cre_gp_no');
        $cam_no = $this->input->post('cam_no');
        //광고그룹아이디 
        $data['cre_gp_no'] = $cre_gp_no;
        //시간대 제한 옵션이 있을 경우
        $option_time = $this->input->post('option_time');
        $comparison_time = $this->input->post('comparison_time');
        $data_time = $this->input->post('data_time');

        $option_os = $this->input->post('option_os');
        $comparison_os = $this->input->post('comparison_os');
        $data_os = $this->input->post('data_os');

        $option_browser = $this->input->post('option_browser');
        $comparison_browser = $this->input->post('comparison_browser');
        $data_browser = $this->input->post('data_browser');

        $option_device = $this->input->post('option_device');
        $comparison_device = $this->input->post('comparison_device');
        $data_device = $this->input->post('data_device');

        $option_category = $this->input->post('option_category');
        $comparison_category = $this->input->post('comparison_category');
        $data_category = $this->input->post('data_category');
        if($option_time){
            if($data_time != ""){
                $data['time']['tgt_type'] = 'time';
                if($comparison_time){
                    $data['time']['tgt_comparison'] = '!~';
                }else{
                    $data['time']['tgt_comparison'] = '=~';
                }
                $data['time']['tgt_data'] = $data_time;
            }
        }

        if($option_os){
            if($data_os != ""){
                $data['os']['tgt_type'] = 'os';
                if($comparison_os){
                    $data['os']['tgt_comparison'] = '!~';
                }else{
                    $data['os']['tgt_comparison'] = '=~';
                }
                $data['os']['tgt_data'] = $data_os;
            }
        }

        if($option_browser){
            if($data_browser != ""){
                $data['browser']['tgt_type'] = 'browser';
                if($comparison_browser){
                    $data['browser']['tgt_comparison'] = '!~';
                }else{
                    $data['browser']['tgt_comparison'] = '=~';
                }
                $data['browser']['tgt_data'] = $data_browser;
            }
        }

        if($option_device){
            if($data_device != ""){
                $data['device']['tgt_type'] = 'device';
                if($comparison_device){
                    $data['device']['tgt_comparison'] = '!~';
                }else{
                    $data['device']['tgt_comparison'] = '=~';
                }
                $data['device']['tgt_data'] = $data_device;
            }
        }
        if($option_category){
            if($data_category != ""){
                $data['category']['tgt_type'] = 'category';
                if($comparison_category){
                    $data['category']['tgt_comparison'] = '!~';
                }else{
                    $data['category']['tgt_comparison'] = '=~';
                }
                $data['category']['tgt_data'] = $data_category;
            }
        }
        if(isset($data)){
            $this->admanagement_db->insert_targeting($data);
        }
        ?>
        <script type="text/javascript">
            location.href="/creative/creative_add_form/?cre_gp_no=<?php echo $cre_gp_no?>&cam_no=<?php echo $cam_no?>";
        </script>
        <?php 
    }

    function targeting_modify_form() {
        
        $mem_no = $this->session->userdata('mem_no');

        $data['os'] = $this->admanagement_db->get_os();
        $data['browser'] = $this->admanagement_db->get_browser();
        $data['device'] = $this->admanagement_db->get_device();
        $data['category'] = $this->admanagement_db->get_content_category();

        $cam_no = $this->input->get('cam_no');
        //광고그룹아이디 :
        $data['cre_gp_no'] = $this->input->get('cre_gp_no');
        $data['cam_no'] = $cam_no;
        
        $data['targeting'] = $this->admanagement_db->get_targeting($data['cre_gp_no']);
        

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_group_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝

        $this->load->view('common/header', $menu_data);

        $this->load->view("creative/targeting_modify_form", $data);
        
        $this->load->view('common/footer');
    }

    function modify_targeting() {
        $data = array();
        $cam_no=$this->input->post('cam_no');
        //광고그룹아이디 : 
        $data['cre_gp_no'] = $this->input->post('cre_gp_no');
        $data['cam_no'] = $cam_no;
        //시간대 제한 옵션이 있을 경우
        $option_time = $this->input->post('option_time');
        $comparison_time = $this->input->post('comparison_time');
        $data_time = $this->input->post('data_time');

        $option_os = $this->input->post('option_os');
        $comparison_os = $this->input->post('comparison_os');
        $data_os = $this->input->post('data_os');

        $option_browser = $this->input->post('option_browser');
        $comparison_browser = $this->input->post('comparison_browser');
        $data_browser = $this->input->post('data_browser');

        $option_device = $this->input->post('option_device');
        $comparison_device = $this->input->post('comparison_device');
        $data_device = $this->input->post('data_device');

        $option_category = $this->input->post('option_category');
        $comparison_category = $this->input->post('comparison_category');
        $data_category = $this->input->post('data_category');

        if($option_time){
            if($data_time != ""){
                $data['time']['tgt_type'] = 'time';
                if($comparison_time){
                    $data['time']['tgt_comparison'] = '!~';
                }else{
                    $data['time']['tgt_comparison'] = '=~';
                }
                $data['time']['tgt_data'] = $data_time;
            }
        }

        if($option_os){
            if($data_os != ""){
                $data['os']['tgt_type'] = 'os';
                if($comparison_os){
                    $data['os']['tgt_comparison'] = '!~';
                }else{
                    $data['os']['tgt_comparison'] = '=~';
                }
                $data['os']['tgt_data'] = $data_os;
            }
        }

        if($option_browser){
            if($data_browser != ""){
                $data['browser']['tgt_type'] = 'browser';
                if($comparison_browser){
                    $data['browser']['tgt_comparison'] = '!~';
                }else{
                    $data['browser']['tgt_comparison'] = '=~';
                }
                $data['browser']['tgt_data'] = $data_browser;
            }
        }

        if($option_device){
            if($data_device != ""){
                $data['device']['tgt_type'] = 'device';
                if($comparison_device){
                    $data['device']['tgt_comparison'] = '!~';
                }else{
                    $data['device']['tgt_comparison'] = '=~';
                }
                $data['device']['tgt_data'] = $data_device;
            }
        }


        if($option_category){
            if($data_category != ""){
                $data['category']['tgt_type'] = 'category';
                if($comparison_category){
                    $data['category']['tgt_comparison'] = '!~';
                }else{
                    $data['category']['tgt_comparison'] = '=~';
                }
                $data['category']['tgt_data'] = $data_category;
            }
        }

        $this->admanagement_db->update_targeting($data);
        ?>
        <script type="text/javascript">
            location.href="/creative/creative_group_list?cam_no=<?php echo $cam_no?>";
        </script>
        <?php 
    }

}

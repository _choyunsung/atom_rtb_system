<?php
class Excel extends CI_Controller {

    public function __construct() {

        parent::__construct();

        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('array');
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
                'form',
                'url'
        ));
        $this->load->model("campaign_db");
        $this->load->model("creative_db");
        $this->load->model("account_db");
        $this->load->model("cash_db");
       
    }
    public function cam_excel_down() {
    
        $mem_no = $this->session->userdata('mem_no');
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $cam_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cam_status[] = "0";
        }
        if ($run == "Y"){
            $cam_status[] = "1";
        }
        if ($ready == "Y"){
            $cam_status[] = "2";
        }
        if ($pause == "Y"){
            $cam_status[] = "3";
            $cam_status[] = "5"; //일예산부족
            $cam_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cam_status[] = "4";
        }
    
        $data['all'] = $all;
        $data['run'] = $run;
        $data['ready'] = $ready;
        $data['pause'] = $pause;
        $data['done'] = $done;
    
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $condition = array (
                "mem_no" => $mem_no,
                "cam_status" => $cam_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
    
        $data['cam_list'] = $this->campaign_db->select_campaign_list($condition);
    
    
        $str_column = 'no,cam_nm,adver_nm,daily_budget,cam_status_desc,cam_ymd,imp,clk,ctr';
        $str_column_header = 'no,캠페인 이름, 광고주명, 일허용예산, 캠페인상태, 캠페인 등록일, 노출수, 클릭수, CTR';
    
    
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
//         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('캠페인 관리');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '캠페인 관리');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:I1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['cam_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "cam_ymd") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                } else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                }else if (strcmp($sel, "cam_status_desc") == 0) {
                    $col = lang($r[$sel]) ;
                } else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '캠페인관리.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    public function cre_gp_excel_down() {
    
        $mem_no = $this->session->userdata('mem_no');
        $cam_no = $this->input->post('cam_no');
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $fromto_date = $this->input->post('fromto_date');
        
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $cre_gp_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cre_gp_status[] = "0";
        }
        if ($run == "Y"){
            $cre_gp_status[] = "1";
        }
        if ($ready == "Y"){
            $cre_gp_status[] = "2";
        }
        if ($pause == "Y"){
            $cre_gp_status[] = "3";
            $cre_gp_status[] = "5"; //일예산부족
            $cre_gp_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cre_gp_status[] = "4";
        }
        $data['all'] = $all;
        $data['run'] = $run;
        $data['ready'] = $ready;
        $data['pause'] = $pause;
        $data['done'] = $done;
        $data['cam_no'] = $cam_no;
        
        
        $condition = array (
                "mem_no" => $mem_no,
                "cam_no" => $cam_no,
                "cre_gp_status" => $cre_gp_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
    
        $data['cre_gp_list'] = $this->creative_db->select_creative_group_list($condition);
        
        $str_column = 'no,cre_gp_nm,cam_nm,start_ymd,end_ymd,cre_gp_status_desc,daily_budget,bid_loc_price,bid_cur,imp,clk,ctr';
        $str_column_header = 'no,광고그룹 이름,캠페인 이름,시작일,종료일,광고그룹 상태,일허용예산,대표입찰가,통화단위,노출수,클릭수,CTR';
        
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
        //         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('광고그룹 관리');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '광고그룹 관리');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:I1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['cre_gp_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "start_ymd") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "end_ymd") == 0) {
                    if ($r[$sel] == "" || $r[$sel] == "0000-00-00" ){
                        $col = "한도없음";
                    }
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                }else if (strcmp($sel, "cre_gp_status_desc") == 0) {
                    $col = lang($r[$sel]) ;
                } else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '광고그룹관리.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    public function cre_excel_down() {
    
        $cre_gp_no = $this->input->post('cre_gp_no');
        $mem_no = $this->session->userdata('mem_no');
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $fromto_date = $this->input->post('fromto_date');

        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $cre_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cre_status[] = "0";
        }
        if ($run == "Y"){
            $cre_status[] = "1";
        }
        if ($ready == "Y"){
            $cre_status[] = "2";
        }
        if ($pause == "Y"){
            $cre_status[] = "3";
            $cre_status[] = "5"; //일예산부족
            $cre_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cre_status[] = "4";
        }

        $data['all'] = $all;
        $data['run'] = $run;
        $data['ready'] = $ready;
        $data['pause'] = $pause;
        $data['done'] = $done;
        $data['cre_gp_no'] = $cre_gp_no;

        $condition = array (
                "mem_no" => $mem_no,
                "cre_gp_no" => $cre_gp_no,
                "cre_status" => $cre_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
    
        $data['cre_list'] = $this->creative_db->select_creative_list($condition);
    
        $str_column = 'no,cre_nm,cre_gp_nm,cam_nm,adver_nm,cre_status_desc,cre_evaluation_desc,cre_gp_type_desc,imp,clk,ctr';
        $str_column_header = 'no,광고 이름,광고그룹 이름,캠페인 이름,광고주 이름,광고 상태,심사,타입,노출수,클릭수,CTR';
    
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
        //         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('광고 관리');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '광고 관리');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:I1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['cre_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "cre_gp_type_desc") == 0) {
                    $col = lang($r[$sel]) ;
                }else if (strcmp($sel, "cre_evaluation_desc") == 0) {
                    $col = lang($r[$sel]) ;
                }else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                }else if (strcmp($sel, "cre_status_desc") == 0) {
                    $col = lang($r[$sel]) ;
                } else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '광고관리.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    public function manager_excel_down() {
    
        $mem_no = $this->session->userdata('mem_no');
        
        $data['manager_list'] = $this->account_db->select_account_manager_list($mem_no);
    
    
        $str_column = 'no,mem_id,manager_st,mem_cont,mem_ymd';
        $str_column_header = 'no,아이디,상태,설명,등록일';
    
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
        //         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('매니저 리스트');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '매니저 리스트');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:E1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['manager_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "manager_st") == 0) {
                    $col = lang($r[$sel]) ;
                }else if (strcmp($sel, "mem_ymd") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                } else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '매니저리스트.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    public function cash_charge_excel_down() {
        $mem_no = $this->input->post('mem_no');
        if ($mem_no == "" ){
            $mem_no = $this->session->userdata('mem_no');
        }
        $kind = $this->input->post('kind');
        $fromto_date = $this->input->post('fromto_date');
        
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $mem_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        $data['cash_charge_list'] = $this->cash_db->select_cash_charge_list($condition);
    
    
        $str_column = 'no,charge_req_dt,charge_dt,charge_cash,cash_type,charge_way_desc,charge_st';
        $str_column_header = 'no,충전신청 일자,충전완료 일자,충전금액,캐시종류,충전방법,충전상태';
    
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
        //         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('캐시충전 리스트');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '캐시충전 리스트');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:G1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['cash_charge_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "cash_type") == 0) {
                    if ($r[$sel] == "C"){
                        $col = lang('strMountain')." ".lang('strCash');
                    }else{
                        $col = lang('strEvent')." ".lang('strCash');
                    }
                }else if (strcmp($sel, "charge_way_desc") == 0) {
                    $col = lang($r[$sel]) ;
                }else if (strcmp($sel, "charge_req_dt") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "charge_dt") == 0) {
                    if ($r[$sel] == ""){
                        $col = "-";
                    }else{
                        $col = substr($r[$sel], 0, 10) ;
                    }
                }else if (strcmp($sel, "charge_st") == 0) {
                    if ($r[$sel] == "R"){ 
                        $col = lang('strNeedToDeposit');
                    }else{
                        $col = lang('strCashComplete');
                    }
                }else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                } else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '캐시충전리스트.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    public function cash_history_excel_down() {
    
        $mem_no = $this->input->post('mem_no');
        if ($mem_no == "" ){
            $mem_no = $this->session->userdata('mem_no');
        }
        $kind = $this->input->post('kind');
        $fromto_date = $this->input->post('fromto_date');
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $condition = array (
                "mem_no" => $mem_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
    
        $data['cash_history_list'] = $this->cash_db->select_cash_history_list($condition);
    
    
        $str_column = 'no,cash_dt,mo_cash,event_cash,sum_cash';
        $str_column_header = 'no,사용일자,마운틴 캐쉬 사용금액,이벤트 캐쉬 사용금액,총사용금액';
    
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
        //         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('캐시사용 리스트');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '캐시사용 리스트');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:E1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['cash_history_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "cash_dt") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                } else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '캐시사용리스트.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    public function cash_refund_excel_down() {
        $mem_no = $this->input->post('mem_no');
        if ($mem_no == "" ){
            $mem_no = $this->session->userdata('mem_no');
        }
        $kind = $this->input->post('kind');
        $fromto_date = $this->input->post('fromto_date');
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $condition = array (
                "mem_no" => $mem_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
    
        $data['cash_history_list'] = $this->cash_db->select_cash_refund_list($condition);
    
    
        $str_column = 'no,refund_req_dt,refund_st,refund_way_desc,refund_dt,refund_cash';
        $str_column_header = 'no,환불신청일자,환불신청결과,환불방법,환불지급일자,환불지급금액';
    
        // 엑셀 컬럼용 배열 생성
        $col_title = explode(",", $str_column);
        $abc = $this->excel_column_alpha(count($col_title));
        $col_header = explode(",", $str_column_header);
        ini_set('memory_limit', -1); // 메모리제한을 없엔다.
         
        // PHPExcel 라이브러리 로드
        //         require_once '../mountain/application/libraries/PHPExcel/PHPExcel.php'; // 라이브러리 파일
        require_once '../mountain/application/libraries/PHPExcel/Classes/PHPExcel.php'; // 라이브러리 파일
        //require_once '../libraries/IOFactory.php'; // 라이브러리 파일
         
        // PHPExcel 객체생성
        $obj_php_excel = new PHPExcel();
        // 워크시트에서 1번째는 활성화
        $obj_php_excel->setActiveSheetIndex(0);
    
        // 워크시트 이름 지정
        $obj_php_excel->getActiveSheet()->setTitle('캐시사용 리스트');
        // 엑셀 헤더 정의
        $obj_php_excel->getActiveSheet()->setCellValue('A1', '캐시사용 리스트');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $obj_php_excel->getActiveSheet()->mergeCells('A1:E1');
        $obj_php_excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    
        // 엑셀 컬럼헤더 타이틀을 생성한다.
        $col_cnt = 0;
        foreach ($col_header as $header) {
            $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . "3", $header);
            $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . "3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $obj_php_excel->getActiveSheet()->getColumnDimension($abc[$col_cnt])->setAutoSize(true);
            $col_cnt++;
        }
    
        $row_num = 1;
    
        // 셀렉트된 결과를 레코드 단위로 처리
        foreach ($data['cash_history_list'] as $row) {
    
            $col_cnt = 0;
            // 컬럼단위로 엑셀의 셀을 생성한다.
            foreach ($col_title as $sel) {
                $r = (array)$row;
                $align = 'left';
                $is_number = false;
                $format_code = "#,##0";
                if (strcmp($sel, "refund_dt") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "refund_req_dt") == 0) {
                    $col = substr($r[$sel], 0, 10) ;
                }else if (strcmp($sel, "no") == 0) {
                    $col = $row_num;
                    $align = 'center';
                }else if (strcmp($sel, "refund_st") == 0){
                    if ($r[$sel] == "R"){
                        $col = lang('strCashWait');
                    }else if ($r[$sel] == "C"){
                        $col = lang('strRefundComplete');
                    }else{
                        $col = lang('strCashRejected');
                    }
                }else if (strcmp($sel, "refund_way_desc") == 0){
                    $col = lang($r[$sel]);
                }else {
                    $col = $r[$sel];
                }
                $row_cnt = $row_num + 3;
    
                // 데이터 입력
                if ($is_number) {
                    $align = 'right';
                    $obj_php_excel->getActiveSheet()->setCellValueExplicit($abc[$col_cnt] . $row_cnt, $col, PHPExcel_Cell_DataType::TYPE_NUMERIC);
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getNumberFormat()->setFormatCode($format_code);
                } else {
                    $obj_php_excel->getActiveSheet()->setCellValue($abc[$col_cnt] . $row_cnt, $col);
                }
    
                if (strcmp($align, "right") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                } elseif (strcmp($align, "center") == 0) {
                    $obj_php_excel->getActiveSheet()->getStyle($abc[$col_cnt] . $row_cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                }
                $col_cnt++;
            }
            $row_num++;
        }
    
        // 파일명은 euc-kr로 변환해야함.
        $file_name = iconv('UTF-8', 'EUC-KR', '캐시사용리스트.xls'); // 엑셀 파일 이름
        header("Content-Type: application/vnd.ms-excel"); // mime 타입
        header("Content-Disposition: attachment;filename='".$file_name."'"); // 브라우저에서 받을 파일 이름
        header("Cache-Control: max-age=0"); // no cache
    
    
        // Excel5 포맷으로 저장 엑셀 2007 포맷으로 저장하고 싶은 경우 'Excel2007'로 변경합니다.
        $obj_writer = PHPExcel_IOFactory::createWriter($obj_php_excel, 'Excel5');
        // 서버에 파일을 쓰지 않고 바로 다운로드 받습니다.
        $obj_writer->save('php://output');
    
    }
    
    protected function excel_column_alpha($count) {
    
        $abc = array (
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
        );
        $cell = $count;
        $char = 26;
    
        for($i = 0; $i < $cell; $i++) {
            if ($i < $char)
                $ret[] = $abc[$i];
            else {
                $idx1 = (int)($i - $char) / $char;
                $idx2 = ($i - $char) % $char;
                $ret[] = $abc[$idx1] . $abc[$idx2];
            }
        }
    
        return $ret;
    }
    
    public function read_excel_form()
    {
        
        if($_FILES['cre_excel_auction']['error']!=0) { //옥션 엑셀 파일
            return $_FILES['cre_excel_auction']['error'];
        }
        else {
            if(isset($_FILES['cre_excel_auction']['error'])) { //옥션 엑셀 파일
                $inputFileName = $_FILES['cre_excel_auction']['tmp_name'];
            }
        }
        
        require_once $_SERVER['DOCUMENT_ROOT'].'/application/libraries/PHPExcel/Classes/PHPExcel.php';

        $objPHPExcel = new PHPExcel();
        $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
        $objReader->setReadDataOnly(true);
        $objExcel = $objReader->load($inputFileName);
        $objExcel->setActiveSheetIndex(0);
        $objWorksheet = $objExcel->getActiveSheet();

        $rowIterator = $objWorksheet->getRowIterator();

        $_return_array = array();
        $o=0;
        
        
        foreach ($rowIterator as $keys => $row)
        {
            
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            foreach ($cellIterator as $key => $cell ) { // 해당 열의 모든 셀에 대해서
                $_return_array_temp[$key] = iconv("UTF-8", "EUC-KR",$cell->getValue());                    
            }
            $_return_array[$o] = array_filter($_return_array_temp);
            $o++;

        }
        
        $_RETURN['html']='<colgroup>
        <col width="100px" />
        <col width="*" />
        </colgroup>';
        foreach(array_filter($_return_array) as $key => $val )
        {
            $_RETURN['html'] .= vsprintf('<tr>
            <td>%s</td><td>%s</td><td>%s</td>
            </tr>',array(
                $val[0],
                $val[1],
                $val[2],
            ));
            $_RETUNARRAY[$key] = array($val[0],$val[1],$val[2]);
        }
        $_RETURN['data'] = json_encode($_RETUNARRAY);
        echo json_encode($_RETURN,true); 
        
    }

    public function read_excel_form_cheetah()
    {

        if($_FILES['cre_excel_cheetah']['error']!=0) { //치타 엑셀 파일
            return $_FILES['cre_excel_cheetah']['error'];
        }
        else {
            if(isset($_FILES['cre_excel_cheetah']['error'])) { //치타 엑셀 파일
                $inputFileName = $_FILES['cre_excel_cheetah']['tmp_name'];
            }
        }

        require_once $_SERVER['DOCUMENT_ROOT'].'/application/libraries/PHPExcel/Classes/PHPExcel.php';

        $objPHPExcel = new PHPExcel();
        $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
        $objReader->setReadDataOnly(true);
        $objExcel = $objReader->load($inputFileName);
        $objExcel->setActiveSheetIndex(0);
        $objWorksheet = $objExcel->getActiveSheet();

        $rowIterator = $objWorksheet->getRowIterator();

        $_return_array = array();
        $o=0;


        foreach ($rowIterator as $keys => $row)
        {

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);
            foreach ($cellIterator as $key => $cell ) { // 해당 열의 모든 셀에 대해서
                $_return_array_temp[$key] = iconv("UTF-8", "EUC-KR",$cell->getValue());
            }
            $_return_array[$o] = array_filter($_return_array_temp);
            $o++;

        }

        $_RETURN['html']='<colgroup>
        <col width="100px" />
        <col width="*" />
        </colgroup>';
        foreach(array_filter($_return_array) as $key => $val )
        {
            $_RETURN['html'] .= vsprintf('<tr>
            <td>%s</td><td>%s</td><td>%s</td>
            </tr>',array(
                $val[0],
                $val[1],
                $val[2],
            ));
            $_RETUNARRAY[$key] = array($val[0],$val[1],$val[2]);
        }
        $_RETURN['data'] = json_encode($_RETUNARRAY);
        echo json_encode($_RETURN,true);

    }
    
}
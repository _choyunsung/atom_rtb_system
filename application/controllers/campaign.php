<?php 

class Campaign extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('array');
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
                'form',
                'url'
        ));
        $this->load->model("member_db");
        $this->load->model("code_db");
        $this->load->model("campaign_db");
        $this->load->model("account_db");

        //페이징관련 라이브러리
        $this->load->library('pagination');
        
        $this->load->library('menu');
    }
    
    public function campaign_list(){
        $mem_no = $this->session->userdata('mem_no');
        $cam_no = $this->input->get_post('cam_no');
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $cam_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cam_status[] = "0";
        }
        if ($run == "Y"){
            $cam_status[] = "1";
        }
        if ($ready == "Y"){
            $cam_status[] = "2";
        }
        if ($pause == "Y"){
            $cam_status[] = "3";
            $cam_status[] = "5"; //일예산부족
            $cam_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cam_status[] = "4";
        }
        if (element(0,$cam_status) == ""){
            $cam_status[] = "1";
            $run = "Y";
        }
        
        $data['all'] = $all;
        $data['run'] = $run;
        $data['ready'] = $ready;
        $data['pause'] = $pause;
        $data['done'] = $done;
        $data['sel_status'] = $this->code_db->sel_status_code();
        
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $mem_no,
                "cam_no" => $cam_no,
                "cam_status" => $cam_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
/*페이징처리*/
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->campaign_db->select_campaign_count($condition);
        
        $this->load->library('pagination');
        $config['base_url'] = "/campaign/campaign_list/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);

        $data['campaign_list'] = $this->campaign_db->select_campaign_list($condition, $data['per_page'], $data['page_num']);
        $data['sum_campaign_list'] = $this->campaign_db->select_sum_campaign_list($condition);
        $data['page_links'] = $this->pagination->create_links();
/*페이징처리*/
        $data['campaign_list_count'] = $this->campaign_db->campaign_list_count($condition);
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/campaign/campaign_list'
        );

        $menu_data = $this->menu->menu_info($params);
        
//        var_dump($menu_data);
        //메뉴 라이브러리 - 끝

        $this->load->view('common/header', $menu_data);
        $this->load->view('campaign/campaign_list', $data);
        $this->load->view('common/footer');
    }
    
    public function campaign_modify_form(){
        $cam_no = $this->input->get("cam_no");
        $mem_no = $this->session->userdata('mem_no');
        $data['advertiser_list'] = $this->campaign_db->sel_advertiser($mem_no);
        $data['campaign_info'] = $this->campaign_db->select_campaign_info($cam_no);

        $mem = $this->account_db->select_master_info($mem_no);
        $data['group_no'] = $mem['group_no'];
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/campaign/campaign_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('campaign/campaign_modify_form', $data);
        $this->load->view('common/footer');
    }
    
    public function modify_campaign_save(){
        $cam_no = $this->input->post('cam_no');
        $data['cam_nm'] = $this->input->post('campaign_nm');
        $data['daily_budget'] = $this->input->post('daily_budget');
        $data['cam_ymd'] = date('Y-m-d');
        $result = $this->campaign_db->update_modify_campaign($data, $cam_no);
        if ($result == "ok"){
            echo "ok";
        }
    }
    
    public function campaign_chart(){
        $mem_no = $this->input->post('mem_no');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $all= $this->input->post('all');
        $run= $this->input->post('run');
        $ready= $this->input->post('ready');
        $pause= $this->input->post('pause');
        $done= $this->input->post('done');
        $cam_status=array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cam_status[] = "0";
        }
        if ($run == "Y"){
            $cam_status[] = "1";
        }
        if ($ready == "Y"){
            $cam_status[] = "2";
        }
        if ($pause == "Y"){
            $cam_status[] = "3";
            $cam_status[] = "5"; //일예산부족
            $cam_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cam_status[] = "4";
        }
        
        $condition = array (
                "mem_no" => $mem_no,
                "cam_status" => $cam_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['chart_data'] = $this->campaign_db->select_campaign_chart_data($condition); 
        $this->load->view('creative/creative_management_chart', $data);
    }
    
    public function campaign_add_form(){
        $mem_no = $this->session->userdata('mem_no');
        $data['advertiser_list'] = $this->campaign_db->sel_advertiser($mem_no);

        $mem = $this->account_db->select_master_info($mem_no);
        $data['group_no'] = $mem['group_no'];
        $data['fee_fl'] = $mem->fee_fl;
    /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/campaign/campaign_list';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/campaign/campaign_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('campaign/campaign_add_form', $data);
        $this->load->view('common/footer');
    }
    
    public function advertiser_add_form(){
        $data['mem_no'] = $this->input->post("mem_no");
        $this->load->view('campaign/advertiser_add_form',$data);
    }
    
    public function new_advertiser_save(){
        $data['mem_id'] = "adver".time();
        $data['mem_gb'] = "ADT";
        $data['mem_com_nm'] = $this->input->post('mem_com_nm');
        $data['mem_nm'] = $this->input->post('mem_nm');
        $data['mem_tel'] = $this->input->post('mem_tel');
        $data['mem_email'] = $this->input->post('mem_email');
        $data['mem_cont'] = $this->input->post('mem_cont');
        $data['mem_ymd'] = date('Y-m-d');
        $adver_no = $this->campaign_db->insert_new_advertiser($data);
        
        $logged_in = $this->session->userdata('logged_in');
        
        if($logged_in['mem_type'] == "master"){
            $group['agency_no'] = $this->input->post('mem_no');
            $group['adver_no'] = $adver_no;
        }else if($logged_in['mem_type'] == "manager"){
            $group['agency_no'] = $this->campaign_db->select_master_no($this->input->post('mem_no'));
            $group['adver_no'] = $adver_no;
            $group['manager_no'] = $this->input->post('mem_no');
        }
        
        if ($adver_no > 0){
            $mem_group_no = $this->campaign_db->insert_new_member_group($group);
        }
        
        if ($mem_group_no > 0){
           echo "ok";
        }
    }
    
    public function campaign_save(){
        $data['cam_nm'] = $this->input->post('campaign_nm');
        $data['adver_no'] = $this->input->post('adver_no');
        $data['daily_budget'] = $this->input->post('daily_budget');
        $data['cam_status'] = "1";
        $data['cam_ymd'] = date('Y-m-d H:i:s');
        $cam_no = $this->campaign_db->insert_campaign($data);
        $data['cam_no'] = $cam_no;

        if ($cam_no > 0){
            echo $cam_no;
        }else{
            echo "false";
        }
    }
    
    public function sel_campaign_status_change(){
        $mem_no = $this->input->post('mem_no');
        $status_key = $this->input->post('status_key');
        $cam_no = $this->input->post('cam_no');
        if ($cam_no == "all"){
            $cam_no_arr = $this->input->post('sel_campaign');
            $data['cam_no_arr'] = implode(",", $cam_no_arr);
        }else{
            $data['cam_no_arr'] = $cam_no;
        }
        $data['status_key'] = $status_key;
        
        $result = $this->campaign_db->campaign_status_change($data);
        
        redirect('/campaign/campaign_list', 'refresh');
        
    }
    
    public function sel_campaign_delete(){
        $mem_no = $this->input->post('mem_no');
        $cam_no = $this->input->post('cam_no');
        if ($cam_no == "all"){
            $cam_no_arr = $this->input->post('sel_campaign');
            $data['cam_no_arr'] = implode(",",$cam_no_arr);
        }else{
            $data['cam_no_arr'] = $cam_no;
        }
        $data['status_key'] = 'Y';
    
        $result = $this->campaign_db->campaign_status_change($data);
    
        redirect('/campaign/campaign_list', 'refresh');
    
    }
    
    public function new_campaign_copy(){
        $mem_no = $this->input->post('mem_no');
        $cam_no_arr = $this->input->post('sel_campaign');
        $data['advertiser_list'] = $this->campaign_db->sel_advertiser($mem_no);
        $data['cam_no_arr'] = implode(",",$cam_no_arr);
        $data['campaign_info'] = $this->campaign_db->select_campaign_info($data['cam_no_arr']);
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/campaign/campaign_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('campaign/campaign_copy', $data);
        $this->load->view('common/footer');
    }
    
    public function campaign_nm_check(){
        $campaign_nm = $this->input->post('campaign_nm');
        $mem_no = $this->input->post('mem_no');
        $result = $this->campaign_db->nm_check($campaign_nm, $mem_no);
        echo $result;
        exit();
    }

    public function modify_campaign_nm_check(){
        $campaign_nm = $this->input->post('campaign_nm');
        $mem_no = $this->input->post('mem_no');
        $cam_no = $this->input->post('cam_no');
        $result = $this->campaign_db->nm_check($campaign_nm, $mem_no, $cam_no);
        echo $result;
        exit();
    }
    
    public function campaign_name_modi_save(){
        $data['cam_nm'] = $this->input->post('cam_nm');
        $cam_no = $this->input->post('cam_no');
        $result = $this->campaign_db->update_modify_campaign($data, $cam_no);
        if ($result == "ok"){
            echo "ok";
        }
    }
    
}

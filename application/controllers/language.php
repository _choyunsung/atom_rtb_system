<?php
/**
 * Created by PhpStorm.
 * User: adop
 * Date: 2014. 6. 19.
 * Time: 오후 1:11
 */
class Language extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("member_db");
        $this->load->helper('url');
        $this->load->library('session');
    }

    public function switch_language($language = "") {
        $CI =& get_instance();
        $language = ($language != "") ? $language : $this->config['language'];

        $userdata = $this->session->userdata("logged_in");
        $input_data['mem_lang'] = $language;
        $where = "mem_no = '".$userdata['mem_no']."'";
        $this->member_db->modify_member_row($input_data, $where);
        $this->session->set_userdata('site_lang', $language);
        $http_referer = $this->input->server('HTTP_REFERER');
        $from = isset($http_referer) ? $http_referer : $CI->config->base_url();
        redirect($from, 'refresh');
    }
}
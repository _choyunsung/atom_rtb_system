<?php
class Auction extends CI_Controller
{
    public $_PDATA;

    public function __construct()
    {
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));

        $this->load->library(array('session','email','form_validation', 'pagination', 'menu'));
        $this->load->helper(array('array','date','url','security','cookie','form', 'file'));
        $this->load->model(array("member_db","account_db","code_db","campaign_db", "auction_db"));

        $this->load->database();

        $this->_PDATA = $this->input->post(null,true);



        //아마존 cdn -- 시작
        $this->load->library('AWS/s3');

        // Bucket Name
        $this->bucket = "www.ads_optima.com";
        //         if (!class_exists('S3'))require_once('s3.php');

        //AWS access info
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJPQVCELP2JMKWLXQ');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');

        //instantiate the class
//        $this->s3->setAuth(awsAccessKey, awsSecretKey);
//        $this->s3->putBucket($this->bucket);
        $s3 = new S3("awsAccessKey", "awsSecretKey");

        //아마존 cdn -- 끝

    }

    public function items()
    {

        //메뉴 라이브러리 - 시작
        $params = array(
            'url' => '/auction/items'
        );

        $menu_data = $this->menu->menu_info($params);
        $data = $this->auction_db->get_auction_items();

        $this->load->view('common/header', $menu_data);
        $this->load->view('auction/auction_list', $data);
        $this->load->view('common/footer');

    }

    public function items_add_form()
    {

        //메뉴 라이브러리 - 시작
        $params = array(
            'url' => '/auction/auction_from'
        );

        if ($_POST['items_date']!="") {
            $data = $this->auction_db->get_auction_item_detail();
        }else{
            $data[] = '';
        }

        if(count($data['list']) > 0) {

            $item_order = 0;
            foreach ($data['list'] as $key => $row) {
                if ($item_order < $row['item_order']) {
                    $item_order = $row['item_order'];
                }
            }

            foreach ($data['list'] as $val) {
                $_SORT[(int)$val['item_order']] = $val;
            }

            ksort($_SORT);

            for ($i = 0; $i <= $item_order; $i++) {
                if ($_SORT[$i]) {
                    $_RSROT[$i] = $_SORT[$i];
                } else {
                    $_RSROT[$i] = '';
                    $_RSROT[$i]['item_skip'] = 'Y';
                }
            }

            $data['list'] = $_RSROT;
        }

        $menu_data = $this->menu->menu_info($params);
        $this->load->view('common/header', $menu_data);
        $this->load->view('auction/auction_form', $data);
        $this->load->view('common/footer');
    }

    public function items_add_save() {
        if ($_POST['items_idx'] != ""){
            $data = $this->auction_db->update_auction_items();
        }else {
            $data = $this->auction_db->insert_auction_items();
        }
        if ($data > 0){
            $this->auction_items();
            redirect("/auction/items", 'refresh');
        }

    }

    public function auction_cache_items()
    {




    }

    public function auction_items() {

        $date = $_POST['daterange'];
//        $date = "2016-02-17";
//        $filePath = "/Users/jung/Documents/workspace_m/mountain/application/cache/";
        $fileName = "adop_items_".$date.".json";
        $s3path = "atom/allkill_item/item_list/";

        $data = multiarray_post($this->_PDATA);

        foreach ($data as $key => $row){
            if ($row['item_name'] != "" && $row['item_skip'] == 'N') {
                $url = "http://cdn.ads-optima.com/atom/allkill_item/".$_POST['daterange']."/img_product".($key+1)."_%s.jpg";
                $json_data[$key]['item_name'][] = $row['item_name'];
                $json_data[$key]['item_info'][] = $row['item_info'];
                $json_data[$key]['item_price'][] = $row['item_price'];
                $json_data[$key]['item_link'][] = trim($row['item_link']);
                $json_data[$key]['item_mlink'][] = trim($row['item_link']);
                $json_data[$key]['item_image'][] = $url;

            } elseif($row['item_skip'] == 'Y') {

                $url = "http://cdn.ads-optima.com/atom/allkill_item/".$_POST['daterange']."/img_product".($key+1)."_%s.jpg";
                $json_data[$key]['item_name'][] = "";
                $json_data[$key]['item_info'][] = "";
                $json_data[$key]['item_price'][] = "";
                $json_data[$key]['item_link'][] = "";
                $json_data[$key]['item_mlink'][] = "";
                $json_data[$key]['item_image'][] = "";
            }
        }

        //파일을 s3에 바로 작성
        $upload_string = json_encode($json_data,JSON_UNESCAPED_UNICODE);
        $this->s3->putObjectString($upload_string, $this->bucket , $s3path.$fileName, S3::ACL_PUBLIC_READ);

        //파일을 ec2에 생성 후 업로드 할때
//        write_file($filePath.$fileName, json_encode($json_data,JSON_UNESCAPED_UNICODE));
//        $this->s3->putObjectFile($filePath.$fileName, $this->bucket , $s3path.$fileName, S3::ACL_PUBLIC_READ);

    }
}
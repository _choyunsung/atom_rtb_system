<?php
class Dashboard extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
            'form',
            'url'
        ));
        $this->load->model("campaign_db");
        $this->load->model("creative_db");
        $this->load->model("board_db");
        $this->load->model("cash_db");
        $this->load->model("menu_db");
        $this->load->library('menu');
    }

    public function index(){
        $mem_no = $this->session->userdata('mem_no');
        
        $cam_no = $this->input->post('cam_no');
        $cam_nm = $this->input->post('cam_nm');
        $data['sel_campaign'] = $this->campaign_db->sel_campaign($mem_no);
        $data['creative_summary'] = $this->creative_db->select_creative_summary($mem_no);
        $data['mem_no'] = $mem_no;
        $data['cam_no'] = $cam_no;
        $data['cam_nm'] = $cam_nm;
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/dashboard';
        $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
*/
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/dashboard'
        );
        $menu_data = $this->menu->menu_info($params);
               
        //메뉴 라이브러리 - 끝
        
        if($this->agent->is_mobile()){
            $this->load->view('common/m_header');
            $this->load->view('common/m_lmenu');
            $this->load->view('common/m_dashboard', $data);
            $this->load->view('common/m_footer');
        }else{
            $this->load->view('common/header_no_menu', $menu_data);
            $this->load->view('common/dashboard', $data);
            $this->load->view('common/footer');
        }
        
    }
    
    public function sales(){
        $mem_no = $this->session->userdata('mem_no');
        
        $cam_no = $this->input->post('cam_no');
        $cam_nm = $this->input->post('cam_nm');
        $data['sel_campaign'] = $this -> campaign_db -> sel_campaign($mem_no);
        $data['creative_summary'] = $this->creative_db->select_creative_summary($mem_no);
        $data['mem_no'] = $mem_no;
        $data['cam_no'] = $cam_no;
        $data['cam_nm'] = $cam_nm;
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/dashboard';
        $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
*/
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/dashboard/sales'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        if($this->agent->is_mobile()){
            $this->load->view('common/m_header');
            $this->load->view('common/m_lmenu');
            $this->load->view('common/m_dashboard', $data);
            $this->load->view('common/m_footer');
        }else{
            $this->load->view('common/header_no_menu', $menu_data);
            $this->load->view('common/dashboard_sales', $data);
            $this->load->view('common/footer');
        }
    }
    
    public function dashboard_cash_summary(){
        $mem_no = $this->input->post('mem_no');
        $data['cash_summary'] = $this->cash_db->select_dashboard_cash_summary($mem_no);
        $this->load->view('common/dashboard_cash_summary', $data);
    }
    
    public function dashboard_dday_cre_group(){
        $mem_no = $this->input->post('mem_no');
        $data['cre_gp_d_day'] = $this->creative_db->select_end_date_creative_group($mem_no);
        $this->load->view('common/dashboard_dday_cre_group', $data);
    }
    
    public function dashboard_all_cre_summary(){
        $mem_no = $this->input->post('mem_no');
        $data['creative_summary'] = $this->creative_db->select_creative_summary($mem_no);
        $data['creative_all_summary'] = $this->creative_db->select_creative_all_summary($mem_no);
        $this->load->view('common/dashboard_all_cre_summary', $data);
    }
    
    public function dashboard_all_cre_evaluation(){
        $mem_no = $this->input->post('mem_no');
        $data['all_cre_evaluation'] = $this->creative_db->select_all_cre_evaluation($mem_no);
        $this->load->view('common/dashboard_all_cre_evaluation', $data);
    }
    
    public function dashboard_campaign_chart(){
        $mem_no = $this->input->post('mem_no');
        $cam_no = $this->input->post('cam_no');
        $from_date = date("Y-m-d", strtotime("-7 day"));
        $to_date = date("Y-m-d");
        
        $condition = array (
                "mem_no" => $mem_no,
                "cam_no" =>$cam_no,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['chart_data'] = $this->campaign_db->select_dashboard_campaign_chart_data($condition);
        $this->load->view('common/dashboard_campaign_chart', $data);
    }
    
    public function dashboard_campaign_summary(){
        $mem_no = $this->session->userdata('mem_no');
        $cam_no = $this->input->post('cam_no');
        $from_date = date("Y-m-d", strtotime("-6 day"));
        $to_date = date("Y-m-d");
        $before_from_date = date("Y-m-d", strtotime("-13 day"));
        $before_to_date = date("Y-m-d", strtotime("-6 day"));
        $condition = array (
                "mem_no" => $mem_no,
                "cam_no" =>$cam_no,
                "from_date" => $from_date,
                "to_date" => $to_date,
                "before_from_date" => $before_from_date,
                "before_to_date" => $before_to_date,
        );
        $data['summary_data'] = $this->campaign_db->select_dashboard_campaign_summary_data($condition);
        $this->load->view('common/dashboard_campaign_summary', $data);
    }

    public function dashboard_all_notice(){
        $group_no = $this->session->userdata('group_no');
        $condition = array (
            "group_no" => $group_no
        );
        $data['notice_data'] = $this->board_db->select_all_notice($condition);
        $this->load->view('common/dashboard_all_notice', $data);
    }


    public function temp(){
        $this->load->view('common/header_no_menu');
        $this->load->view('common/dashboard_temp');
        $this->load->view('common/footer');
    }


}

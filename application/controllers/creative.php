<?php
class Creative extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('pagination');

        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array ('form', 'url', 'array'));

        $this->load->model("member_db");
        $this->load->model("code_db");
        $this->load->model("campaign_db");
        $this->load->model("creative_db");
        $this->load->model("admanagement_db");
        $this->load->model("account_db");
        
        //아마존 cdn -- 시작
        $this->load->library('AWS/s3');
        
        // Bucket Name
        $this->bucket = "www.ads_optima.com";
        //         if (!class_exists('S3'))require_once('s3.php');
         
        //AWS access info
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJPQVCELP2JMKWLXQ');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
         
        //instantiate the class
        $this->s3->setAuth(awsAccessKey, awsSecretKey);
        $this->s3->putBucket($this->bucket);
        //아마존 cdn -- 끝
        
        $this->load->library('menu');
        
    }
    
    public function creative_group_list(){
        $mem_no = $this->session->userdata('mem_no');
        $cam_no = $this->session->flashdata('cam_no');
        if ($cam_no == ""){
            $cam_no = $this->input->get_post('cam_no');
        }
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $cre_gp_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cre_gp_status[] = "0";
        }
        if ($run == "Y"){
            $cre_gp_status[] = "1";
        }
        if ($ready == "Y"){
            $cre_gp_status[] = "2";
        }
        if ($pause == "Y"){
            $cre_gp_status[] = "3";
            $cre_gp_status[] = "5"; //일예산부족
            $cre_gp_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cre_gp_status[] = "4";
        }
        if (element(0, $cre_gp_status) == ""){
            $cre_gp_status[] = "1";
            $run = "Y";
        }
        $data['all'] = $all;
        $data['run'] = $run;
        $data['ready'] = $ready;
        $data['pause'] = $pause;
        $data['done'] = $done;
        $data['cam_no'] = $cam_no;
        $data['sel_status'] = $this->code_db->sel_status_code();
        
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $condition = array (
                "mem_no" => $mem_no,
                "cam_no" => $cam_no,
                "cre_gp_status" => $cre_gp_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['total_rows'] = $this->creative_db->select_creative_group_count($condition);
        $this->load->library('pagination');
        $config['base_url'] = '/creative/creative_group_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        $data['creative_group_list_count'] = $this->creative_db->creative_group_list_count($condition);
        $data['creative_group_list'] = $this->creative_db->select_creative_group_list($condition, $data['per_page'], $data['page_num']);
        $data['sum_creative_group_list'] = $this->creative_db->select_sum_creative_group_list($condition);
        $data['page_links'] = $this->pagination->create_links();
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_group_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_group_list', $data);
        $this->load->view('common/footer');
    }
    
    public function creative_group_chart(){
        $mem_no = $this->input->post('mem_no');
        $cam_no = $this->input->post('cam_no');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $cre_gp_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cre_gp_status[] = "0";
        }
        if ($run == "Y"){
            $cre_gp_status[] = "1";
        }
        if ($ready == "Y"){
            $cre_gp_status[] = "2";
        }
        if ($pause == "Y"){
            $cre_gp_status[] = "3";
            $cre_gp_status[] = "5"; //일예산부족
            $cre_gp_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cre_gp_status[] = "4";
        }
    
        $condition = array (
                "mem_no" => $mem_no,
                "cam_no" => $cam_no,
                "cre_gp_status" => $cre_gp_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['chart_data'] = $this->creative_db->select_creative_group_chart_data($condition);
        $this->load->view('creative/creative_management_chart', $data);
    }
    
    public function new_creative_group_copy(){
        $tmp_cre_gp_no = $this->input->post('sel_creative_group');
        $cre_gp_no = $tmp_cre_gp_no[0];
        $data['creative_group_info'] = $this->creative_db->select_creative_group_info($cre_gp_no);
        
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/creative/creative_group_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_group_copy_form', $data);
        $this->load->view('common/footer');
    }
    
    /* 광고리스트 페이지  */
    public function creative_list(){
        $cre_gp_no = $this->session->flashdata('cre_gp_no');
        if ($cre_gp_no == ""){
            $cre_gp_no = $this->input->post('cre_gp_no');
        }
        $mem_no = $this->session->userdata('mem_no');
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $cre_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cre_status[] = "0";
        }
        if ($run == "Y"){
            $cre_status[] = "1";
        }
        if ($ready == "Y"){
            $cre_status[] = "2";
        }
        if ($pause == "Y"){
            $cre_status[] = "3";
            $cre_status[] = "5"; //일예산부족
            $cre_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cre_status[] = "4";
        }
        if (element(0, $cre_status) == ""){
            $cre_status[] = "1";
            $run = "Y";
        }
        $data['all'] = $all;
        $data['run'] = $run;
        $data['ready'] = $ready;
        $data['pause'] = $pause;
        $data['done'] = $done;
        $data['cre_gp_no'] = $cre_gp_no;
        $data['sel_status'] = $this->code_db->sel_status_code();
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $condition = array (
                "mem_no" => $mem_no,
                "cre_gp_no" => $cre_gp_no,
                "cre_status" => $cre_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['total_rows'] = $this->creative_db->select_creative_count($condition);
        $this->load->library('pagination');
        $config['base_url'] = '/creative/creative_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        $data['creative_list_count'] = $this->creative_db->creative_list_count($condition);
        
        $data['creative_list'] = $this->creative_db->select_creative_list($condition, $data['per_page'], $data['page_num']);
        
        $data['sum_creative_list'] = $this->creative_db->select_sum_creative_list($condition);
        
        $data['page_links'] = $this->pagination->create_links();
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_list', $data);
        $this->load->view('common/footer');
    }
    
    public function creative_chart(){
        $mem_no = $this->input->post('mem_no');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $all = $this->input->post('all');
        $run = $this->input->post('run');
        $ready = $this->input->post('ready');
        $pause = $this->input->post('pause');
        $done = $this->input->post('done');
        $cre_status = array();
        if ($all.$run.$ready.$pause.$done == ""){
            $run = "Y";
        }
        if ($all == "Y"){
            $cre_status[] = "0";
        }
        if ($run == "Y"){
            $cre_status[] = "1";
        }
        if ($ready == "Y"){
            $cre_status[] = "2";
        }
        if ($pause == "Y"){
            $cre_status[] = "3";
            $cre_status[] = "5"; //일예산부족
            $cre_status[] = "6"; //잔액부족
        }
        if ($done == "Y"){
            $cre_status[] = "4";
        }
    
        $condition = array (
                "mem_no" => $mem_no,
                "cre_gp_no" => $cre_gp_no,
                "cre_status" => $cre_status,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['chart_data'] = $this->creative_db->select_creative_chart_data($condition);
        $this->load->view('creative/creative_management_chart', $data);
    }
    
    public function new_creative_copy(){
        $tmp_cre_no = $this->input->post('sel_creative');
        $cre_no = $tmp_cre_no[0];
        $data['creative_info'] = $this->creative_db->select_creative_info($cre_no);
        if ($data['creative_info']['cre_type'] == 4){
            $data['auction_info'] = $this->creative_db->select_ad_info($cre_no);
        }
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/creative/creative_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_copy_form', $data);
        $this->load->view('common/footer');
    }
    
    public function creative_detail(){
        $cre_no = $this->input->get('cre_no');
        $cre_gp_no = $this->input->get('cre_gp_no');
        $mem_no = $this->session->userdata('mem_no');
        $fromto_date = $this->input->get('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d", strtotime("-1 day"));
        }
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $mem_no,
                "cre_gp_no" => $cre_gp_no,
                "cre_no" => $cre_no,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['fromto_date'] = $fromto_date;
        $data['cre_gp_no'] = $cre_gp_no;
        $data['cre_no'] = $cre_no;
        $data['sel_status'] = $this->code_db->sel_status_code();
        $data['creative_detail'] = $this->creative_db->select_creative_detail_view($condition);
        $data['sum_creative_detail'] = $this->creative_db->select_sum_creative_detail($condition);
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_detail', $data);
        $this->load->view('common/footer');
    }
    
    public function creative_detail_chart(){
        $mem_no = $this->input->post('mem_no');
        $cre_no = $this->input->post('cre_no');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d", strtotime("-1 day"));
        }
        $data['fromto_date']= $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $condition = array (
                "mem_no" => $mem_no,
                "cre_no" => $cre_no,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        $data['chart_data'] = $this->creative_db->select_creative_detail_chart_data($condition);
        $this->load->view('creative/creative_management_chart', $data);
    }

    public function creative_group_add_form(){
        $cam_no = $this->input->get('cam_no');
        $adver_no = $this->input->get('adver_no');
        $mem_no = $this->session->userdata('mem_no');
        
        $data['cam_no'] = $cam_no;
        $data['adver_no'] = $adver_no;
        $data['campaign_info'] = $this->campaign_db->select_campaign_info($cam_no);
        $mem = $this->account_db->select_master_info($this->session->userdata('mem_no'));
        $data['group_no'] = $mem['group_no'];

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_group_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝

        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_group_add_form', $data);
        $this->load->view('common/footer');
    }
    
    public function creative_group_save(){
        $data['start_ymd']          =   $this->input->post('start_ymd');
        $data['end_ymd']            =   $this->input->post('end_ymd');
        $data['cam_no']             =   $this->input->post('cam_no');
        $data['cre_gp_nm']          =   $this->input->post('cre_gp_nm');
        $data['daily_budget']       =   $this->input->post('daily_budget');
        $data['bid_loc_price']      =   $this->input->post('bid_loc_price');
        $data['bid_price']          =   $this->input->post('bid_price');
        $data['bid_fee']            =   $this->input->post('bid_fee');
        $data['bid_type']           =   $this->input->post('bid_type');
        $data['bid_cur']            =   $this->input->post('bid_cur');
        $data['cre_gp_type']        =   $this->input->post('cre_gp_type');
        $data['cre_gp_status']      =   '1';
        $data['cre_gp_ymd']         =   date('Y-m-d H:i:s');

        if ($data['bid_cur'] == "USD"){
            $data['bid_price'] = $data['bid_loc_price'];
        }
        $cre_gp_no = $this->creative_db->insert_creative_group($data);
        $data['cre_gp_no']          =   $cre_gp_no;

        $this->update_bid_price($cre_gp_no);

        if ($cre_gp_no > 0){
            echo $cre_gp_no;
        }else{
            echo "false";
        }
    }
    
    public function creative_group_modify_form(){
        $cre_gp_no = $this->input->get('cre_gp_no');
        $mem_no = $this->session->userdata('mem_no');
        $data['creative_group_info'] = $this->creative_db->select_creative_group_info($cre_gp_no);
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/creative/creative_group_list';
        $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_group_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('creative/creative_group_modify_form', $data);
        $this->load->view('common/footer');
    }
    
    public function modify_creative_group_save(){
        $cre_gp_no = $this->input->post('cre_gp_no');
        $data['start_ymd'] = $this->input->post('start_ymd');
        $data['end_ymd'] = $this->input->post('end_ymd');
        $data['cre_gp_nm'] = $this->input->post('cre_gp_nm');
        $data['daily_budget'] = $this->input->post('daily_budget');
        $data['bid_loc_price'] = $this->input->post('bid_loc_price');
        $data['bid_fee'] = $this->input->post('bid_fee');
        $data['bid_type'] = $this->input->post('bid_type');
        $data['cre_gp_type'] = $this->input->post('cre_gp_type');
        $data['bid_cur']  = $this->input->post('bid_cur');
//         $data['cre_gp_status'] = '1';
        $data['cre_gp_ymd'] = date('Y-m-d H:i:s');
        
        if ($data['bid_cur'] == "USD"){
            $data['bid_price'] = $data['bid_loc_price'];
        }
        
        $result = $this->creative_db->update_modify_creative_group($data, $cre_gp_no);

        $this->update_bid_price($cre_gp_no);

        if ($result=="ok"){
            echo "ok";
        }
    }

    public function creative_add_form($cre_gp_no = null, $cam_no = null){

        if ($cre_gp_no == ""){
            $cre_gp_no = $this->input->get('cre_gp_no');
        }
        if ($cam_no == ""){
            $cam_no = $this->input->get('cam_no');
        }

        $data['adver_no'] = $this->input->post('adver_no');
        $data['cam_no'] = $cam_no;
        $data['cre_gp_no'] = $cre_gp_no;
        $data['creative_group_info'] = $this->creative_db->select_creative_group_info($cre_gp_no);
        $mem_no = $this->session->userdata('mem_no');
        
        
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/creative/creative_list';
        $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx=0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('creative/creative_add_form',$data);
        $this->load->view('common/footer');
    }

    function creative_image_file(){
        $width = $this->input->post('cre_width');
        $height = $this->input->post('cre_height');
        $data['original_file_nm'] = '';
        
        $original_file_nm = $this->file_upload($this, 'cre_link_file', $width, $height);
        /*
        if (isset($original_file_nm['error'])) {
            echo 'false';
        } else if (isset($original_file_nm['result'])) {
            $data['original_file_nm'] = $original_file_nm['result']['file_name'];
            echo $data['original_file_nm'];
        }
        */
        if($original_file_nm != null){
            echo $original_file_nm;
//             $data['original_file_nm'] = $original_file_nm;
//             echo $data['original_file_nm'];
        }else{
            echo 'false';
        }
    }

    public function new_creative_save(){
        $mem_no = $this->session->userdata('mem_no');

        $data['cre_gp_no'] = $this->input->post('cre_gp_no');
        $data['cre_nm'] = $this->input->post('cre_nm');
        $data['cre_type'] = $this->input->post('cre_type');
        $data['cre_link'] = $this->input->post('cre_link');
        $data['cre_width'] = $this->input->post('cre_width');
        $data['cre_height'] = $this->input->post('cre_height');
        $data['cre_cont'] = $this->input->post('cre_cont');
        $data['cre_img_link'] = $this->input->post('cre_img_link');
        
        $data['cre_ymd'] = date('Y-m-d H:i:s');
        $data['cre_status'] = "1";
        $data['cre_evaluation'] = "1";
        $data['mem_no'] = $mem_no;
        
        /* 옥션 BNCODE 데이터 수집 */
        $data['cre_bncode_site'] = $this->input->post('cre_bncode_site');
        $data['cre_width_action'] = $this->input->post('cre_width_action');
        $data['cre_height_action'] = $this->input->post('cre_height_action');
        $data['cre_auction_type'] = $this->input->post('cre_action_type');

        /* 치타 */
        $data['cre_zoneid_site'] = $this->input->post('cre_zoneid_site');
        $data['cre_width_cheetah'] = $this->input->post('cre_width_cheetah');
        $data['cre_height_cheetah'] = $this->input->post('cre_height_cheetah');

        $cre_no = $this->creative_db->insert_new_creative($data);

        $data['cre_no'] = $cre_no;

        if ($cre_no > 0){
            echo $cre_no;
        }else{
            echo "false";
        }
    }


    public function creative_modify_form(){
        $cre_no = $this->input->get('cre_no');
        $mem_no = $this->session->userdata('mem_no');
        $data['creative_info'] = $this->creative_db->select_creative_info($cre_no);

        if ($data['creative_info']['cre_type'] == 4){ //옥션
            $data['auction_info'] = $this->creative_db->select_ad_info($cre_no);
        }
        elseif($data['creative_info']['cre_type'] == 5) { //치타
            $data['cheetah_info'] = $this->creative_db->select_ad_info($cre_no);
        }


        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/creative/creative_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header', $menu_data);
        $this->load->view('creative/creative_modify_form', $data);
        $this->load->view('common/footer');
    }
    
    public function creative_modify_save(){
        $cre_no= $this->input->post('cre_no');
        $data['cre_gp_no'] = $this->input->post('cre_gp_no');
        $data['cre_nm'] = $this->input->post('cre_nm');
        $data['cre_type'] = $this->input->post('cre_type');
        $data['cre_link'] = $this->input->post('cre_link');
        $data['cre_width'] = $this->input->post('cre_width');
        $data['cre_height'] = $this->input->post('cre_height');
        $data['cre_cont'] = $this->input->post('cre_cont');
        $data['cre_img_link'] = $this->input->post('cre_img_link');
        $data['cre_ymd'] = date('Y-m-d H:i:s');
        $data['cre_link'] = $this->input->post('cre_link');
        $data['cre_status'] = $this->input->post('cre_status');
        $data['cre_evaluation'] = $this->input->post('cre_evaluation');
        $ori_cre_img_link = $this->input->post('ori_cre_img_link');
        $ori_cre_link = $this->input->post('ori_cre_link');
        $ori_cre_cont = $this->input->post('ori_cre_cont');
        $cre_img_link_fl = 'N';
        $cre_link_fl = 'N';
        $cre_cont_fl = 'N';

        /* 옥션 BNCODE 데이터 수집 */
        $data['cre_bncode_site'] = $this->input->post('cre_bncode_site');
        $data['cre_width_action'] = $this->input->post('cre_width_action');
        $data['cre_height_action'] = $this->input->post('cre_height_action');
        $data['cre_auction_type'] = $this->input->post('cre_action_type');

        /* 치타 */
        $data['cre_width_cheetah'] = $this->input->post('cre_width_cheetah');
        $data['cre_height_cheetah'] = $this->input->post('cre_height_cheetah');
        $data['cre_zoneid_site'] = $this->input->post('cre_zoneid_site');

        if ($data['cre_img_link'] != $ori_cre_img_link){
            $cre_img_link_fl = 'Y';
        }
        
        if ($data['cre_link'] != $ori_cre_link){
            $cre_link_fl = 'Y';
        }
        
        if ($data['cre_cont'] != $ori_cre_cont){
            $cre_cont_fl = 'Y';
        }
        
        if ($cre_img_link_fl == 'Y' || $cre_link_fl == 'Y' || $cre_cont_fl == 'Y'){
            $data['cre_evaluation'] = '1';
        }
        $result = $this->creative_db->update_modify_creative($data, $cre_no);
        
        if ($result == "ok"){
            echo "ok";
        }
    }
    
    public function sel_creative_group_status_change(){
        $mem_no = $this->input->post('mem_no');
        $status_key = $this->input->post('status_key');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $cam_no = $this->input->post('cam_no');
        if ($cre_gp_no == "all"){
            $cre_gp_no_arr = $this->input->post('sel_creative_group');
            $data['cre_gp_no_arr'] = implode(",", $cre_gp_no_arr);
        }else{
            $data['cre_gp_no_arr'] = $cre_gp_no;
        }
        $data['status_key'] = $status_key;
    
        $this->creative_db->creative_group_status_change($data);
        $this->session->set_flashdata('cam_no', $cam_no);
        redirect('/creative/creative_group_list/', 'refresh');
    
    }
    
    public function sel_creative_group_delete(){
        $cam_no = $this->input->post('cam_no');
        $cre_gp_no = $this->input->post('cre_gp_no');
        if ($cre_gp_no == "all"){
            $cre_gp_no_arr = $this->input->post('sel_creative_group');
            $data['cre_gp_no_arr'] = implode(",", $cre_gp_no_arr);
        }else{
            $data['cre_gp_no_arr'] = $cre_gp_no;
        }
        $data['status_key'] = 'Y';
    
        $result = $this->creative_db->creative_group_status_change($data);
        $this->session->set_flashdata('cam_no', $cam_no);
        redirect('/creative/creative_group_list/', 'refresh');
    
    }
    
    public function sel_creative_status_change(){
        
        $mem_no = $this->input->post('mem_no');
        $status_key = $this->input->post('status_key');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $cre_no = $this->input->post('cre_no');
        if ($cre_no == "all"){
            $cre_no_arr = $this->input->post('sel_creative');
            $data['cre_no_arr'] = implode(",", $cre_no_arr);
        }else{
            $data['cre_no_arr']= $cre_no;
        }
        $data['status_key']= $status_key;
    
        $this->creative_db->creative_status_change($data);
        $this->session->set_flashdata('cre_gp_no', $cre_gp_no);
        redirect('/creative/creative_list/', 'refresh');
    
    }
    
    public function sel_creative_delete(){
        $mem_no = $this->input->post('mem_no');
        $cre_no = $this->input->post('cre_no');
        $cre_gp_no = $this->input->post('cre_gp_no');
        if ($cre_no == "all"){
            $cre_no_arr = $this->input->post('sel_creative');
            $data['cre_no_arr'] = implode(",", $cre_no_arr);
        }else{
            $data['cre_no_arr'] = $cre_no;
        }
        $data['status_key'] = 'Y';
    
        $result= $this->creative_db->creative_status_change($data);
        $this->session->set_flashdata('cre_gp_no', $cre_gp_no);
        redirect('/creative/creative_list/', 'refresh');
    
    }
    
    function creative_group_tar_add() {
        $data['cre_gp_no'] = $this->input->post('cre_gp_no');
        $data['os'] = $this->admanagement_db->get_os();
        $data['browser'] = $this->admanagement_db->get_browser();
        $data['device'] = $this->admanagement_db->get_device();
        $data['category'] = $this->admanagement_db->get_content_category();
        $this->load->view('creative/targeting_add_form', $data);
    }
    
    public function creative_group_name_modi_save(){
        $data['cre_gp_nm'] = $this->input->post('cre_gp_nm');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $result = $this->creative_db->update_modify_creative_group($data, $cre_gp_no);
        if ($result == "ok"){
            echo "ok";
        }
    }
    
    public function creative_group_bid_price_modi_save(){
        $bid_price = $this->input->post('bid_price');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $bid_cur = $this->input->post('bid_cur');

        if ($bid_cur == "USD"){
            $data['bid_price'] = $bid_price;
        }elseif ($bid_cur == "KRW"){
            $data['bid_loc_price'] = $bid_price;
        }

        $result = $this->creative_db->update_modify_creative_group($data, $cre_gp_no);
        if ($result == "ok"){
            $this->update_bid_price($cre_gp_no);
            echo "ok";
        }
    }
    
    
    public function make_directory($path) {
    
        $this->load->helper('directory');
        
        if (!is_dir($path)) {
            //mkdir($path);
            //$path = "/img/no_image.png";
        }
       
        return $path;
    }
    
    public function file_upload($parent, $field_name, $width, $height){
        /*
        $UPLOADDIR = "/upload/cre_img_link/";
        //$UPLOADDIR = "/Users/adop/upload/";
        // 저장 path 확장 ( 업체코드에 대한 폴더가 없을 경우 생성한다. )
                
        $save_path = $this->make_directory($UPLOADDIR);
        
        
        $config['upload_path'] = $save_path;        
        //$config['max_size'] = '20240'; // 파일 사이즈 제한 1024KB
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['overwrite'] = FALSE;

        $parent->load->library('upload', $config);
        if (!$parent->upload->do_upload($field_name)) {
            $data = array (
                    'error' => 'false'
            );
            return $data;
        } else {
            $result = $this->upload->data();
            if ($result['image_width' ] != $width || $result['image_height'] != $height){
                unlink($result['full_path']);
                $data = array (
                        'error' => 'false'
                );
            }else{
                $data['result'] = $result;
            }

            return $data;
        }
        
        */
        
        $mem_no = $this->session->userdata('mem_no');;
        list ($img_width, $img_height) = getimagesize($_FILES[$field_name]['tmp_name']);
        if ($img_width != $width || $img_height != $height){
            return null;
        }
        
        // http://cdn.ads-optima.com/ad_img/e855df614051d10ec945870ba951183b.jpg
        $ext = substr(strrchr($_FILES[$field_name]['name'],"."), 1);
        $ext = strtolower($ext);
        if ($ext == "gif" || $ext == "jpg" || $ext == "png" || $ext == "jpeg"){
            $fileName = $mem_no."/".md5($_FILES[$field_name]['name'].time()). "." . $ext;
            $filePath = $_FILES[$field_name]['tmp_name'];
            $this->s3->putObject($this->s3->inputFile($filePath, false), $this->bucket."/ad_img", $fileName, S3::ACL_PUBLIC_READ, array(), array('Content-Type' => 'image/'.$ext));
            return $fileName;
        }else{
            return null;
        }
        
    }
    
    public function creative_group_nm_check(){
        $cre_gp_nm = $this->input->post('cre_gp_nm');
        $cam_no = $this->input->post('cam_no');
        $result = $this->creative_db->cre_gp_nm_check($cre_gp_nm, $cam_no);
        echo $result;
        exit();
    }

    public function modify_creative_group_nm_check(){
        $cre_gp_nm = $this->input->post('cre_gp_nm');
        $cam_no = $this->input->post('cam_no');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $result = $this->creative_db->cre_gp_nm_check($cre_gp_nm, $cam_no, $cre_gp_no);
        echo $result;
        exit();
    }
    
    public function creative_nm_check(){
        $cre_nm = $this->input->post('cre_nm');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $result = $this->creative_db->cre_nm_check($cre_nm, $cre_gp_no);
        echo $result;
        exit();
    }

    public function modify_creative_nm_check(){
        $cre_nm = $this->input->post('cre_nm');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $cam_no = $this->input->post('cam_no');
        $result = $this->creative_db->cre_nm_check($cre_nm, $cre_gp_no, $cam_no);
        echo $result;
        exit();
    }

    function update_bid_price($cre_gp_no){
        //$date_str = "%Y-%m-%d %H:%i:%s";
        $date_str_search = "%Y-%m-%d";
        $time = time();
        $input_date_search = mdate($date_str_search, $time);
        $this->creative_db->update_bid_price($input_date_search, $cre_gp_no);
        $this->creative_db->update_bid_loc_price("KRW", $input_date_search, $cre_gp_no);
    }
        
}

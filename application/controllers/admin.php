<?php
class Admin extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $this->load->helper(array (
            'form',
            'url'
        ));

        $this->load->model("code_db");
        $this->load->model("menu_db");
        $this->load->model("admin_db");
        $this->load->model("campaign_db");
    
        $this->load->library('menu');
    }

    public function index()
    {
        $this->load->view('common/header');
        $this->load->view("admin/authority");
        $this->load->view('common/footer');
    }

    function category_add() {
        $cate['cate_explan'] = $this->input->post('cate_explan');
        $cate['cate_nm'] = $this->input->post('cate_nm');
        $cate['cate_url'] = $this->input->post('cate_url');
        $cate['cate_parent_no'] = $this->input->post('cate_parent_no');
        $cate['cate_seq'] = $this->input->post('cate_seq');
        $cate['cate_fl'] = $this->input->post('cate_fl');
        $this->menu_db->insert_category_info($cate);
        echo "<script type='text/javascript'>";
        echo "  alert('등록완료');";
        echo "  location.href='/admin/menu_management';";
        echo "</script>";
    }

    function category_modify() {
        $cate_no = $this->input->post('cate_no');
        $where = "cate_no = ".$cate_no;
        $cate['cate_explan'] = $this->input->post('cate_explan');
        $cate['cate_nm'] = $this->input->post('cate_nm');
        $cate['cate_url'] = $this->input->post('cate_url');
        $cate['cate_parent_no'] = $this->input->post('cate_parent_no');
        $cate['cate_seq'] = $this->input->post('cate_seq');
        $cate['cate_fl'] = $this->input->post('cate_fl');
        $this->menu_db->update_category_info($cate, $where, $cate_no);
        echo "<script type='text/javascript'>";
        echo "  alert('수정완료');";
        echo "  location.href='/admin/menu_management';";
        echo "</script>";
    }

    function authority_management() {

        $mem_no = $this->session->userdata('mem_no');
        
        $mem_nm = $this->input->post('mem_nm');
        $group_no = $this->input->post('group_no');
        $group_nm = $this->input->post('group_nm');
        $data['group_nm'] = $group_nm;
        $condition = array (
            "mem_nm" => $mem_nm,
            "group_no" => $group_no
        );

        $data['condition'] = $condition;

        $per_page = $this->input->post('per_page');

        if($per_page == ""){
            $data['per_page'] = 10;
        }else{
            $data['per_page'] = $per_page;
        }

        $data['total_rows'] = $this->admin_db->select_members_count($condition);

        $this->load->library('pagination');
        $config['base_url'] = '/admin/authority_management';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);

        $data['select_category_template'] = $this->menu_db->select_category_template();

        $data['members_list'] = $this->admin_db->select_members_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();

        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = "/admin/authority_management";
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url']=$url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/admin/authority_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝

        $this->load->view('common/header', $menu_data);
        if (count($auth) > 0) {
            $data['write_fl'] = $auth[0]['write_fl'];
            $this->load->view('admin/authority_management', $data);
        } else {
            //임시
            $data['write_fl'] = $auth[0]['write_fl'];
            $this->load->view("admin/authority_management", $data);
        }
        $this->load->view('common/footer');
    }

    function authority_template($group_no) {
        $data['cate_rows'] = $this->menu_db->get_category_template_info($group_no);
        $data['all_cate_rows'] = $this->menu_db->get_category_list();
        $data['temp_group_nm'] = $data['cate_rows']['0']['group_nm'];
        $data['admin_fl'] = $data['cate_rows']['0']['admin_fl'];
        $data['fee_fl'] = $data['cate_rows']['0']['fee_fl'];
        $data['role'] = $data['cate_rows']['0']['role'];
        $this->load->view("admin/authority_detail", $data);
    }

    function authority_add_temp() {
        $group_nm = $this->input->post('group_nm');
        $read_fl = $this->input->post('read_fl_list');
        $write_fl = $this->input->post('write_fl_list');
        $admin_fl = $this->input->post('admin_fl');
        $fee_fl = $this->input->post('fee_fl');
        $role = $this->input->post('role');

        $group_no = $this->menu_db->insert_category_template($role, $fee_fl, $admin_fl, $read_fl, $write_fl, $group_nm);

        if ($group_no > 0) {
            echo "<script type='text/javascript'>";
            echo "  alert('등록완료');";
            echo "  location.href='/admin/authority_management';";
            echo "</script>";
        }
    }

    function authority_modify_temp() {
        $mem_no = $this->input->post('mem_no');
        $group_no = $this->input->post('group_no');
        $group_nm = $this->input->post('group_nm');
        $read_fl = $this->input->post('read_fl_list');
        $write_fl = $this->input->post('write_fl_list');
        $admin_fl = $this->input->post('admin_fl');
        $fee_fl = $this->input->post('fee_fl');
        $role = $this->input->post('role');
        $mem_com_fee = $this->input->post('mem_com_fee');

        if (isset($mem_no) && strcmp($mem_no, '') != 0 && $mem_no != 0) {
            $ret = $this->menu_db->update_category_member($mem_com_fee, $read_fl, $write_fl, $mem_no);
            if ($ret == true) {
                echo "<script type='text/javascript'>";
                echo "  alert('수정완료');";
                echo "  location.href='/admin/authority_management';";
                echo "</script>";
                exit;
            }
        } else if (isset($group_no) && strcmp($group_no, '') != 0 && $group_no != 0) {
            $ret = $this->menu_db->update_category_template($role, $fee_fl, $admin_fl, $read_fl, $write_fl, $group_no, $group_nm);
            if ($ret == true) {
                echo "<script type='text/javascript'>";
                echo "  alert('수정완료');";
                echo "  location.href='/admin/authority_management';";
                echo "</script>";
                exit;
            }
        }
    }

    function authority_delete_temp() {
        $group_no = $this->input->post('group_no');
        $ret = $this->menu_db->delete_category_template($group_no);
        if ($ret)
            echo 1;
    }

    function authority_member($mem_no) {
        $data['cate_rows'] = $this->menu_db->get_category_member($mem_no);
        $mem = $this->admin_db->get_member($mem_no);
        $data['admin_fl'] = $mem[0]['admin_fl'];
        $data['fee_fl'] = $mem[0]['fee_fl'];
        $data['role'] = $mem[0]['role'];
        $data['mem_com_fee'] = $mem[0]['mem_com_fee'];
        $data['mem_no'] = $mem_no;
        $data['mem_nm'] = $mem[0]['mem_nm'];
        $data['group_no'] = $mem[0]['group_no'];
        $data['select_category_template'] = $this->menu_db->select_category_template();
        $this->load->view("admin/authority_detail", $data);
    }

    function authority_apply() {
        $mem_no = $this->input->post('mem_no');
        $group_no = $this->input->post('group_no');
        $$mem_no = explode(",", $mem_no);
        foreach ($$mem_no as $mn) {
            $ret = $this->menu_db->insert_category_member($group_no, $mn);
        }
        if ($ret)
            echo 1;
    }

    function authority_apply_cate_temp() {
        $mem_no = $this->input->post('mem_no');
        $group_no = $this->input->post('group_no');
        $mem_com_fee = $this->input->post('mem_com_fee');
        if (isset($mem_no) && strcmp($mem_no, '') != 0) {
            $ret = $this->menu_db->insert_category_member($group_no, $mem_no);
            if ($ret == true) {
                echo "<script type='text/javascript'>";
                echo "  alert('수정완료');";
                echo "  location.href='/admin/authority_management';";
                echo "</script>";
                exit;
            }
        }
    }

    function admin_account(){

        $data['mem_no'] = $this->session->userdata('mem_no');

        $condition = array(
            'admin_nm' => $this->input->post("cond_admin_nm"),
            'group_no' => $this->input->post("cond_group_no")
        );

        $data['cond_admin_no'] = $this->input->post("cond_admin_no");
        $data['cond_group_no'] = $this->input->post("cond_group_no");

        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 25;
        }else{
            $data['per_page'] = $per_page;
        }

        $data['total_rows'] = $this->admin_db->select_admin_account_list_count($condition);

        $this->load->library('pagination');
        $config['base_url'] = '/adcensor/ad_image';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);

        $data["admin_group_list"] = $this->admin_db->select_admin_group_list();

        $data['exchange_list'] = $this->admin_db->select_admin_account_list($condition, $data['per_page'], $data['page_num']);


        $data['page_links'] = $this->pagination->create_links();
/*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/admin/admin_account';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($data['mem_no']);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
*/
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/admin/admin_account'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('admin/admin_account', $data);
        $this->load->view('common/footer');
    }

    function admin_account_add_form(){
        $data['tel_no_list']= $this->code_db->sel_tel_no_code();
        $data["admin_group_list"] = $this->admin_db->select_admin_group_list();
        $this->load->view('admin/admin_account_add_form', $data);
    }

    function admin_account_add(){

        $data['mem_com_nm'] = "애드오피";
        $data['mem_type'] = "master";
        $data['admin_fl'] = "Y";
        $data['mem_gb'] = "IND";
        $data['mem_nm'] = $this->input->post("mem_nm");
        $data['mem_nick_nm'] = $this->input->post("mem_nick_nm");
        $data['mem_tel'] = $this->input->post("sel_mem_tel")."-".$this->input->post("mem_tel1")."-".$this->input->post("mem_tel2");
        $data['mem_id'] = $this->input->post("mem_id");
        $data['mem_pwd'] = md5($this->input->post("mem_pwd"));
        $data['mem_ymd'] = date("Y-m-d H:i:s");

        $mem_no = $this->admin_db->insert_admin_account($data);

        $group_no = $this->input->post("group_no");

        //첫등록시 광고주로 권한설정
        //광고주그룹아이디 : group_no
        $this->menu_db->insert_category_member($group_no, $mem_no);

        //광고주 자신을 광고주 그룹에 넣는다.
        $group['agency_no'] = $mem_no;
        $group['adver_no'] = $mem_no;
        $ret = $this->campaign_db->insert_new_member_group($group);

        if($ret){
            echo "ok";
        }else{
            echo "false";
        }
    }

    function admin_account_modify_form(){

        $data['mem_fl'] = $this->input->post("mem_fl");
        $data['mem_no'] = $this->input->post("mem_no");
        $data['group_no'] = $this->input->post("group_no");
        $data['mem_nm'] = $this->input->post("mem_nm");
        $data['mem_nick_nm'] = $this->input->post("mem_nick_nm");
        $data['mem_id'] = $this->input->post("mem_id");
        $mem_tel = $this->input->post("mem_tel");

        $mem_tel = explode("-", $mem_tel);
        $data['mem_tel_0'] = $mem_tel[0];
        $data['mem_tel_1'] = $mem_tel[1];
        $data['mem_tel_2'] = $mem_tel[2];

        $data['tel_no_list'] = $this->code_db->sel_tel_no_code();
        $data["admin_group_list"] = $this->admin_db->select_admin_group_list();
        $this->load->view('admin/admin_account_modify_form', $data);
    }

    function admin_account_modify(){

        $data['mem_fl'] = $this->input->post("mem_fl");
        $data['mem_no'] = $this->input->post("mem_no");
        $data['mem_nm'] = $this->input->post("mem_nm");
        $data['mem_nick_nm'] = $this->input->post("mem_nick_nm");
        $data['mem_tel'] = $this->input->post("sel_mem_tel")."-".$this->input->post("mem_tel1")."-".$this->input->post("mem_tel2");

        if($this->input->post("mem_pwd") != ""){
            $data['mem_pwd'] = md5($this->input->post("mem_pwd"));
        }

        $mem_no = $this->admin_db->update_admin_account($data);

        $group_no = $this->input->post("group_no");

        //첫등록시 광고주로 권한설정
        //광고주그룹아이디 : group_no
        $ret = $this->menu_db->insert_category_member($group_no, $mem_no);

        if($ret){
            echo "ok";
        }else{
            echo "false";
        }
    }

    function menu_management(){

        $mem_no = $this->session->userdata('mem_no');
        $data['cate_rows'] = $this->menu_db->get_all_category_list();
/*
        $main_cates = $this->menu_db->get_main_category();
        $url = "/admin/menu_management";
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url']=$url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
*/
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/admin/menu_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);

        if (count($auth) > 0) {
            $data['write_fl'] = $auth[0]['write_fl'];
            $this->load->view('admin/menu_management', $data);
        } else {
            //임시
            $data['write_fl'] = $auth[0]['write_fl'];
            $this->load->view("admin/menu_management", $data);
        }

        $this->load->view('common/footer');
    }


}

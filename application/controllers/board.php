<?php
class Board extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $this->load->helper(array (
                'form',
                'url',
                'array'
        ));
        $this->load->model("member_db");
        $this->load->model("menu_db");
        $this->load->model("code_db");
        $this->load->model("board_db");
        $this->load->model("account_db");
        $this->load->model("cash_db");
        
        //아마존 cdn -- 시작
        $this->load->library('AWS/s3');
        
        // Bucket Name
        $this->bucket = "www.ads_optima.com";
        //         if (!class_exists('S3'))require_once('s3.php');
         
        //AWS access info
        if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJPQVCELP2JMKWLXQ');
        if (!defined('awsSecretKey')) define('awsSecretKey', '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');
         
        //instantiate the class
        $this->s3->setAuth(awsAccessKey, awsSecretKey);
        $this->s3->putBucket($this->bucket);
        //아마존 cdn -- 끝
        
        
        $this->load->library('menu');
        
    }
    
    public function admin_notice_list(){
        $mem_no = $this->session->userdata('mem_no');
        $search_txt = $this->input->post('search_txt');
        $search_type = $this->input->post('search_type');
        
        $data['search_txt'] = $search_txt;
        $data['search_type'] = $search_type;
        
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $condition = array (
                "mem_no" => $mem_no,
                "search_txt" => $search_txt,
                "search_type" => $search_type
        );
        $data['total_rows'] = $this->board_db->select_admin_notice_count($condition);
        $config['base_url'] = '/board/admin_notice_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        $data['no_read_notice_cnt'] = $this->board_db->select_no_read_admin_notice($condition);
        $data['notice_list'] = $this->board_db->select_admin_notice_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        
        $params = array(
                        'url' => '/board/admin_notice_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('board/admin_notice_list', $data);
        $this->load->view('common/footer');
    } 
    
    public function admin_notice_detail_view(){
        
        $mem_no = $this->session->userdata('mem_no');
        $notice_no = $this->input->get('notice_no');
        if ($notice_no == ""){
            $notice_no = $this->input->post('notice_no');
        }
    
        $data['pre_notice_no'] = $this->board_db->select_pre_notice_no('A',$notice_no);
        $data['next_notice_no'] = $this->board_db->select_next_notice_no('A',$notice_no);
        $this->board_db->insert_board_read($mem_no, $notice_no, 'N');
        $this->board_db->update_notice_read_cnt($notice_no);
        $data['notice_detail'] = $this->board_db->select_notice_detail_view($notice_no);
        $data['reply_list'] = $this->board_db->select_notice_reply_list($notice_no);
        $data['notice_attach'] = $this->board_db->select_notice_attach_list($notice_no);
        
        $params = array(
                        'url' => '/board/admin_notice_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        //         $this->load->view('board/notice_list', $data);
        $this->load->view('board/admin_notice_detail_view', $data);
        $this->load->view('common/footer');
    }
    
    public function member_notice_list(){
        $mem_no = $this->session->userdata('mem_no');
        $search_txt = $this->input->post('search_txt');
        $search_type = $this->input->post('search_type');
    
        $data['search_txt'] = $search_txt;
        $data['search_type'] = $search_type;
    
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $condition = array (
                "mem_no" => $mem_no,
                "search_txt" => $search_txt,
                "search_type" => $search_type
                
        );
        $data['total_rows'] = $this->board_db->select_member_notice_count($condition);
        $config['base_url'] = '/board/member_notice_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
        $data['no_read_notice_cnt'] = $this->board_db->select_no_read_member_notice($condition);
        $data['notice_list'] = $this->board_db->select_member_notice_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
    
        $params = array(
                        'url' => '/board/member_notice_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('board/member_notice_list', $data);
        $this->load->view('common/footer');
    }
    
    public function member_notice_detail_view(){
        $mem_no = $this->session->userdata('mem_no');
        $notice_no = $this->input->post('notice_no');
        
        $data['pre_notice_no'] = $this->board_db->select_pre_notice_no('M',$notice_no);
        $data['next_notice_no'] = $this->board_db->select_next_notice_no('M',$notice_no);
        $this->board_db->insert_board_read($mem_no, $notice_no, 'N');
        $this->board_db->update_notice_read_cnt($notice_no);
        $data['notice_detail'] = $this->board_db->select_notice_detail_view($notice_no);
        $data['notice_attach'] = $this->board_db->select_notice_attach_list($notice_no);
    
       $params = array(
                        'url' => '/board/member_notice_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('board/member_notice_detail_view', $data);
        $this->load->view('common/footer');
    }
    
    public function notice_add_form(){
        $params = array(
                        'url' => '/board/admin_notice_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('board/notice_add_form');
        $this->load->view('common/footer');
    }
    
    public function notice_add() {
        $data['mem_no'] = $this->session->userdata("mem_no");
        $data['notice_nm'] = $this->input->post('notice_nm');
        $data['notice_cont'] = $this->input->post('notice_cont');
        $data['date_ymd'] = date("Y-m-d");
        $data['notice_fl'] = $this->input->post('notice_fl');
    
        $ret = $this->board_db->insert_notice($data);
        if ($ret > 0) {
            for($i=1; $i<=10; $i++ ){
                $file['attach_file_nm'] = '';
                $file['board_no']=$ret;
                $file['board_fl']='N';
                $file['order_no']=$i;
                $data['upload'.$i.'_yn'] = $this->input->post('upload'.$i.'_yn');
                 
                if ($data['upload'.$i.'_yn'] == 'Y'){
                    $original_file_nm1 = $this->file_upload($this, 'attach_file_nm'.$i);
                     
                    if ($original_file_nm1 != ""){
                        $file['attach_file_nm'] = $original_file_nm1;
                        $this->board_db->insert_notice_file($file);
                    }
                }
            }
            if ($data['notice_fl'] == "A"){
                redirect("/board/admin_notice_list", 'refresh');
            }else{
                redirect("/board/member_notice_list", 'refresh');
            }
        }
    }

    public function notice_modify_form() {
        $data['mem_no'] = $this->session->userdata("mem_no");
        $notice_no = $this->input->post('notice_no');
        $data['notice_detail'] = $this->board_db->select_notice_detail_view($notice_no);
        $data['notice_attach'] = $this->board_db->select_notice_attach_list($notice_no);

        $params = array(
            'url' => '/board/member_notice_list'
        );
        $menu_data = $this->menu->menu_info($params);

        $this->load->view('common/header', $menu_data);
        $this->load->view('board/notice_modify_form', $data);
        $this->load->view('common/footer');
    }

    public function notice_modify_save() {
        $data['mem_no'] = $this->session->userdata("mem_no");
        $data['notice_nm'] = $this->input->post('notice_nm');
        $data['notice_cont'] = $this->input->post('notice_cont');
        $data['notice_fl'] = $this->input->post('notice_fl');
        $notice_no = $this->input->post('notice_no');

        $this->board_db->update_notice($data, $notice_no);
        for($i=1; $i<=10; $i++ ){
            $file['attach_file_nm'] = '';
            $file['board_no']=$notice_no;
            $file['board_fl']='N';
            $file['order_no']=$i;
            $data['upload'.$i.'_yn'] = $this->input->post('upload'.$i.'_yn');

            if ($data['upload'.$i.'_yn'] == 'Y'){
                $original_file_nm1 = $this->file_upload($this, 'attach_file_nm'.$i);

                if ($original_file_nm1 != ""){
                    $file['attach_file_nm'] = $original_file_nm1;
                    $this->board_db->insert_notice_file($file);
                }
            }
            if ($data['notice_fl'] == "A"){
                redirect("/board/admin_notice_list", 'refresh');
            }else{
                redirect("/board/member_notice_list", 'refresh');
            }
        }
    }
    
    public function notice_reply1_save(){
        $data['notice_no'] = $this->input->post('notice_no');
        $data['mem_no'] = $this->input->post('mem_no');
        $data['reply_cont'] = $this->input->post('reply_cont');
        $data['reply_depth'] = $this->input->post('reply_depth');
        $data['parent_reply_no'] = $this->input->post('parent_reply_no');
        $data['date_ymd'] = date('Y-m-d H:i:s');
        
        $ret = $this->board_db->insert_reply($data);
        if($this->agent->is_mobile()) {
            redirect("/board/m_notice_detail_view/?notice_no=".$data['notice_no'], 'refresh');
        }else{
            redirect("/board/admin_notice_detail_view/?notice_no=" . $data['notice_no'], 'refresh');
        }
        
    }
    
    public function notice_reply1_modify_save(){
        $reply_no = $this->input->post('reply_no');
        $notice_no = $this->input->post('notice_no');
        $reply_depth = "1";
        $data['reply_cont'] = $this->input->post('reply_cont');
        $data['date_ymd'] = date('Y-m-d H:i:s');
    
        $ret = $this->board_db->update_reply1($data, $reply_no);

        if($this->agent->is_mobile()) {
            redirect("/board/m_notice_detail_view/?notice_no=".$notice_no, 'refresh');
        }else{
            redirect("/board/admin_notice_detail_view/?notice_no=" .$notice_no, 'refresh');
        }
    }
    
    public function notice_reply1_delete(){
        $reply_no = $this->input->post('reply_no');
        $notice_no = $this->input->post('notice_no');
        $data['reply_fl'] = 'Y';
        $ret = $this->board_db->delete_reply1($data, $reply_no);
    
        if($this->agent->is_mobile()) {
            redirect("/board/m_notice_detail_view/?notice_no=".$notice_no, 'refresh');
        }else{
            redirect("/board/admin_notice_detail_view/?notice_no=" .$notice_no, 'refresh');
        }
    
    }
    
    public function question_list(){
        $mem_no = $this->session->userdata('mem_no');
        $question_fl = $this->input->post('question_fl');
        $no_answer = $this->input->post('no_answer');
        $tmp_save = $this->input->post('tmp_save');
        $answer_done = $this->input->post('answer_done');
        $mem_id = $this->input->post('mem_id');
        $mem_nm = $this->input->post('mem_nm');
        $manager_nm = $this->input->post('manager_nm');
        $question_nm = $this->input->post('question_nm');
        $fromto_date = $this->input->post('fromto_date');
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $search_txt = $this->input->post('search_txt');
        $search_type = $this->input->post('search_type');
        
        $data['search_txt'] = $search_txt;
        $data['search_type'] = $search_type;
    
        $per_page = $this->input->post('per_page');
        if ($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $condition = array (
                "mem_no" => $mem_no,
                "question_fl" => $question_fl,
                "no_answer" => $no_answer,
                "tmp_save" => $tmp_save,
                "answer_done" => $answer_done,
                "mem_id" => $mem_id,
                "mem_nm" => $mem_nm,
                "manager_nm" => $manager_nm,
                "question_nm" => $question_nm,
                "from_date" => $from_date,
                "to_date" => $to_date,
                "fromto_date" => $fromto_date,
                "search_txt" => $search_txt,
                "search_type" => $search_type,
        );
        $data['cond'] =$condition;
        $data['total_rows'] = $this->board_db->select_question_count($condition);
        $config['base_url'] = '/board/question_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if ($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
        $data['question_list'] = $this->board_db->select_question_list($condition, $data['per_page'], $data['page_num']);
        $data['question_summary'] = $this->board_db->select_question_summary();
        $data['sel_question_fl'] = $this->code_db->sel_question_fl();
        $data['page_links'] = $this->pagination->create_links();
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/board/question_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        if ($this->session->userdata('admin_fl') == "Y"){
            $this->load->view('board/question_list', $data);
        }else{
            $this->load->view('board/member_question_list', $data);
        }
        $this->load->view('common/footer');
    }
    
    public function question_detail_view(){
    
        $mem_no = $this->session->userdata('mem_no');
        $question_no = $this->input->get('question_no');
        if ($question_no == ""){
            $question_no = $this->input->post('question_no');
        }
    
        $this->board_db->insert_board_read($mem_no, $question_no, 'Q');
        $data['question_detail'] = $this->board_db->select_question_detail_view($question_no);
        $data['reply_list'] = $this->board_db->select_notice_reply_list($question_no);
        $data['question_attach'] = $this->board_db->select_question_attach_list($question_no);
        
        $params = array(
                        'url' => '/board/question_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        if ($this->session->userdata('admin_fl') == "Y"){
            $this->load->view('board/question_detail_view', $data);
        }else{ 
            $this->load->view('board/member_question_detail_view', $data);
        }
        $this->load->view('common/footer');
    }
    
    public function question_modify_form(){
        $mem_no = $this->session->userdata('mem_no');
        $question_no = $this->input->post('question_no');
        $data['question_detail'] = $this->board_db->select_question_detail_view($question_no);
        $data['question_attach'] = $this->board_db->select_question_attach_list($question_no);
        $data['sel_question_fl'] = $this->code_db->sel_question_fl();
        
        $params = array(
                'url' => '/board/question_list'
        );
        $menu_data = $this->menu->menu_info($params);
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('board/member_question_modify_form', $data);
        $this->load->view('common/footer');
    }
    
    public function question_add_form(){
        $mem_no = $this->session->userdata('mem_no');
        $logged_in = $this->session->userdata('logged_in');

        $data['mem_pay_later'] = $logged_in['mem_pay_later'];
        $data['mem_info'] = $this->account_db->select_account_manager_info($mem_no);
        $data['mem_no'] = $mem_no;
        $data['sel_question_fl'] = $this->code_db->sel_question_fl();
        
        $params = array(
                        'url' => '/board/question_list'
        );
        $menu_data = $this->menu->menu_info($params);
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('board/question_add_form', $data);
        $this->load->view('common/footer');
    }
    
    public function question_add() {
        $data['mem_no'] = $this->input->post("mem_no");
        $data['mem_nm'] = $this->input->post("mem_nm");
        $data['mem_tel'] = $this->input->post("mem_tel");
        $data['question_fl'] = $this->input->post("question_fl");
        $data['mem_email'] = $this->input->post("mem_email");
        $data['adver_id'] = $this->input->post("adver_id");
        $data['mem_com_url'] = $this->input->post("mem_com_url");
        $data['question_nm'] = $this->input->post('question_nm');
        $data['question_cont'] = $this->input->post('question_cont');
        $data['date_ymd'] = date("Y-m-d");
        $data['question_st'] = "1";
        
        $ret = $this->board_db->insert_question($data);
        if ($data['question_fl'] == "6"){
            $ret2 = $this->cash_db->insert_refund_request($ret);
        }
        if ($ret > 0) {
            for ($i = 1; $i <= 10; $i++ ){
                $file['attach_file_nm'] = '';
                $file['board_no']=$ret;
                $file['board_fl']='Q';
                $file['order_no']=$i;
                $data['upload'.$i.'_yn'] = $this->input->post('upload'.$i.'_yn');
                 
                if ($data['upload'.$i.'_yn']=='Y'){
                    $original_file_nm1 = $this->file_upload($this, 'attach_file_nm'.$i);
                     
                    if ($original_file_nm1 != ""){
                        $file['attach_file_nm'] = $original_file_nm1;
                        $this->board_db->insert_notice_file($file);
                    }
                }
            }
            redirect("/board/question_list", 'refresh');
        }
    }
    
    public function question_modify_save(){
        $data['mem_tel'] = $this->input->post("mem_tel");
        $data['question_fl'] = $this->input->post("question_fl");
        $data['mem_email'] = $this->input->post("mem_email");
        $data['adver_id'] = $this->input->post("adver_id");
        $data['mem_com_url'] = $this->input->post("mem_com_url");
        $data['question_nm'] = $this->input->post('question_nm');
        $data['question_cont'] = $this->input->post('question_cont');
        $data['date_ymd'] = date("Y-m-d");
        $question_no = $this->input->post('question_no');
        
        $ret = $this->board_db->update_question($data, $question_no);
        if ($ret > 0) {
            for($i=1; $i<=10; $i++ ){
                $ori_attach_file_nm = $this->input->post('ori_attach_file_nm'.$i);
                $attach_file_nm = $this->input->post('attach_file_nm'.$i);
                if ($ori_attach_file_nm != $attach_file_nm){
                    $file['attach_file_nm'] = '';
                    $file['board_no'] = $ret;
                    $file['board_fl'] = 'Q';
                    $file['order_no'] = $i;
                    $data['upload'.$i.'_yn'] = $this->input->post('upload'.$i.'_yn');
                     
                    if ($data['upload'.$i.'_yn'] == 'Y'){
                        $original_file_nm1 = $this->file_upload($this, 'attach_file_nm'.$i);
                         
                        if (isset($original_file_nm1['error'])) {
                        } else if (isset($original_file_nm1['result'])) {
                            $file['attach_file_nm'] = $original_file_nm1['result']['file_name'];
                        }
                        if( $file['attach_file_nm'] != ''){
                            $this->board_db->insert_notice_file($file);
                        }
                    }
                }
            }
            redirect("/board/question_list", 'refresh');
        }
    }
    
    public function answer_save(){
        $answer_no = $this->input->post('answer_no');
        $data['question_no'] = $this->input->post('question_no');
        $data['mem_no'] = $this->input->post('mem_no');
        $data['answer_cont'] = $this->input->post('answer_cont');
        $data['date_ymd'] = date('Y-m-d');
        $udt_data['question_st'] = $this->input->post('question_st');
        $udt_data['contact_mem_no'] = $this->input->post('mem_no');
        if ($answer_no == ""){
            $ret = $this->board_db->insert_answer($data);
        }else{
            $ret = $this->board_db->update_answer($data, $answer_no);
        }
        if ($ret>0){
            $this->board_db->update_question($udt_data, $data['question_no']);
        }
        redirect("/board/question_detail_view/?question_no=".$data['question_no'], 'refresh');
    
    }
    
    public function question_delete(){
        $question_no = $this->input->post('question_no');
        $data['del_fl'] = 'Y';
        $this->board_db->update_question($data, $question_no);
        redirect("/board/question_list", 'refresh');
    
    }
    
    public function make_directory($path) {
    
        $this->load->helper('directory');
    
        if (!is_dir($path)) {
//             mkdir($path);
            //$path = "/img/no_image.png";
        }
         
        return $path;
    }
    
    public function file_upload($parent, $field_name){

        $mem_no = $this->session->userdata('mem_no');;
        $ext = substr(strrchr($_FILES[$field_name]['name'],"."), 1);
        $ext = strtolower($ext);
        // http://cdn.ads-optima.com/ad_img/e855df614051d10ec945870ba951183b.jpg
        $fileName = $mem_no."/".md5($_FILES[$field_name]['name'].time()). "." . $ext;
        $filePath = $_FILES[$field_name]['tmp_name'];
        $this->s3->putObject($this->s3->inputFile($filePath, false), $this->bucket."/board_file", $fileName, S3::ACL_PUBLIC_READ, array(), array('Content-Type' => 'image/'.$ext));
        return $fileName;
    }
    public function img_download(){
        $file_name = $this->input->post('file_name');
        $file = file_get_contents("/upload/cre_img_link/$file_name");
        force_download($file_name, $file);
    }

    public function m_notice_detail_view(){
        $mem_no = $this->input->post('mem_no');

        $notice_no = $this->input->get('notice_no');
        if ($notice_no == ""){
            $notice_no = $this->input->post('notice_no');
        }
        $notice_info = $this->board_db->select_notice_detail_view($notice_no);
        $notice_fl = $notice_info['notice_fl'];

        $data['pre_notice_no'] = $this->board_db->select_pre_notice_no($notice_fl,$notice_no);
        $data['next_notice_no'] = $this->board_db->select_next_notice_no($notice_fl,$notice_no);
        $this->board_db->insert_board_read($mem_no, $notice_no, 'N');
        $this->board_db->update_notice_read_cnt($notice_no);
        $data['notice_detail'] = $this->board_db->select_notice_detail_view($notice_no);
        $data['reply_list'] = $this->board_db->select_notice_reply_list($notice_no);
        $data['notice_attach'] = $this->board_db->select_notice_attach_list($notice_no);
        $data['notice_fl'] = $notice_fl;
        if ($notice_fl == "M"){
            $params = array(
                'url' => '/board/member_notice_list'
            );
        }else{
            $params = array(
                'url' => '/board/admin_notice_list'
            );
        }

        $menu_data = $this->menu->menu_info($params);

        $this->load->view('common/m_header', $menu_data);
        $this->load->view('board/m_notice_detail_view', $data);
        $this->load->view('common/m_footer');
    }


    public function test(){
        $data['test'] = $this->board_db->test_db();
        print_r($data['test'][0]);

    }
        
}
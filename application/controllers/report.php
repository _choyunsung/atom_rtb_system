<?php
class Report extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->load->library('session');
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
                'form',
                'url'
        ));

        $this->load->model("report_db");
        $this->load->library('menu');
    }

    public function report_operation(){

        $mem_no = $this->session->userdata('mem_no');

        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');

        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }

        $data['fromto_date'] = $fromto_date;
        $data['range'] = $range;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];

        $data['report_member_list'] = $this->report_db->select_report_member_list($mem_no, $from_date, $to_date);

        //메뉴 라이브러리 - 시작
        $params = array(
            'url' => '/report/report_operation'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header',$menu_data);
        $this->load->view('report/report_operation', $data);
        $this->load->view('common/footer');
    }

    public function report_operation_detail(){

        $data_type = $this->input->post('data_type');
        $data_key = $this->input->post('data_key');
        $data_name = $this->input->post('data_name');

        //$data_type = "member";
        //$data_key = "54";
        //$data_name = "광고주";

        if($data_type == "campaign"){
        	$data['mem_com_nm'] = $this->report_db->select_report_detail_mem_nm($data_type, $data_key);	
        }
        
        if($data_type == "creative_group"){
        	$data['mem_com_nm'] = $this->report_db->select_report_detail_mem_nm($data_type, $data_key);
        	$data['cam_nm'] = $this->report_db->select_report_detail_cam_nm($data_type, $data_key);
        }
        
        if($data_type == "creative"){
        	$data['mem_com_nm'] = $this->report_db->select_report_detail_mem_nm($data_type, $data_key);
        	$data['cam_nm'] = $this->report_db->select_report_detail_cam_nm($data_type, $data_key);
        	$data['cre_gp_nm'] = $this->report_db->select_report_detail_cre_gp_nm($data_type, $data_key);
        }
        
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_ctr'] = $this->input->post('option_ctr');
        $data['option_ppc'] = $this->input->post('option_ppc');
        $data['option_ppi'] = $this->input->post('option_ppi');
        $data['option_price'] = $this->input->post('option_price');

        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_ctr'] == null){
            $data['option_ctr'] = "N";
        }
        if($data['option_ppc'] == null){
            $data['option_ppc'] = "N";
        }
        if($data['option_ppi'] == null){
            $data['option_ppi'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
        //$data_type = "member";
        //$data_key = "54";

        $data['data_type'] = $data_type;
        $data['data_key'] = $data_key;
        $data['data_name'] = $data_name;

        $fromto_date = $this->input->post('daterange');

        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }

        $data['fromto_date'] = $fromto_date;

        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];

        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        
        if($per_page==""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }

        $data['total_rows'] = $this->report_db->select_report_count($data_type, $data_key, $from_date, $to_date);

        $this->load->library('pagination');
        $config['base_url'] = "/report/report_operation_detail/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);


        $data['report_list'] = $this->report_db->select_report_list($data_type, $data_key, $from_date, $to_date, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        /*페이징처리*/

        $data['report_info'] =$this->report_db->select_report_info($data_type, $data_key);

        $this->load->view('report/report_operation_detail', $data);
    }
    
    public function m_report_operation(){
  
        $mem_no = $this->session->userdata('mem_no');
        $range = $this->input->post('range');
        if ($range == ""){
            $range == "last7";
        }
        
        if ($range == "today"){
            $fromto_date = date("Y-m-d");
        }elseif ($range == "yesterday"){
            $fromto_date = date("Y-m-d", strtotime("-1 day"));
        }elseif ($range == "last7"){
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif ($range == "last30"){
            $fromto_date = date("Y-m-d", strtotime("-30 day")) . " ~ " . date("Y-m-d");
        }elseif ($range == "thismonth"){
            $fromto_date = date("Y-m-01") . " ~ " . date("Y-m-t") ;
        }elseif ($range == "lastmonth"){
            $fromto_date = date("Y-m-01", strtotime("-1 month")) . " ~ " . date("Y-m-t", strtotime("-1 month"));
        }else{
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }
        
        $data['fromto_date'] = $fromto_date;
        $data['range'] = $range;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        if (strpos($fromto_date, '~')){
            $to_date = $temp_date[1];
        }else{
            $to_date = '';
        }
        $data['from_date'] = $from_date;
        $data['to_date'] = $to_date;
        $data['mem_no'] = $mem_no;
        $condition = array (
                "mem_no" => $mem_no,
                "range" => $range,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        $data['report_member_list'] = $this->report_db->select_mobile_report_member_list($condition);
    /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/report/report_operation';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url']=$url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
     */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/report/report_operation'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/m_header');
        $this->load->view('common/m_lmenu');
        $this->load->view('report/m_report_operation', $data);
        $this->load->view('common/m_footer');
    }
    
    public function m_report_operation_detail(){
    
        $data_type = $this->input->post('data_type');
        $data_key = $this->input->post('data_key');
        $data_name = $this->input->post('data_name');

        if($data_type == "campaign"){
            $data['mem_com_nm'] = $this->report_db->select_report_detail_mem_nm($data_type, $data_key);
        }
    
        if($data_type == "creative_group"){
            $data['mem_com_nm'] = $this->report_db->select_report_detail_mem_nm($data_type, $data_key);
            $data['cam_nm'] = $this->report_db->select_report_detail_cam_nm($data_type, $data_key);
        }
    
        if($data_type == "creative"){
            $data['mem_com_nm'] = $this->report_db->select_report_detail_mem_nm($data_type, $data_key);
            $data['cam_nm'] = $this->report_db->select_report_detail_cam_nm($data_type, $data_key);
            $data['cre_gp_nm'] = $this->report_db->select_report_detail_cre_gp_nm($data_type, $data_key);
        }
    
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_price'] = $this->input->post('option_price');
    
        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
        //$data_type = "member";
        //$data_key = "54";
    
        $data['data_type'] = $data_type;
        $data['data_key'] = $data_key;
        $data['data_name'] = $data_name;
    
        $fromto_date = $this->input->post('daterange');
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        if($per_page==""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
    
        $data['total_rows'] = $this->report_db->select_report_count($data_type, $data_key, $from_date, $to_date);
    
        $this->load->library('pagination');
        $config['base_url'] = "/report/report_operation_detail/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
    
        $data['report_list'] = $this->report_db->select_report_list($data_type, $data_key, $from_date, $to_date, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        /*페이징처리*/
    
        $data['report_info'] =$this->report_db->select_report_info($data_type, $data_key);
    
        $this->load->view('report/m_report_operation_detail', $data);
    }
    
    
    public function report_targeting(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['range'] = $range;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $targeting = $this->input->post('targeting');
        $adver_no = $this->input->post('adver_no');
        $cam_no= $this->input->post('cam_no');
        $cre_gp_no = $this->input->post('cre_gp_no');
        $cre_no = $this->input->post('cre_no');
        
        $data['targeting'] = $targeting;
        $data['adver_no'] = $adver_no;
        $data['cam_no'] = $cam_no;
        $data['cre_gp_no'] = $cre_gp_no;
        $data['cre_no'] = $cre_no;
        
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/report/report_targeting'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header',$menu_data);
        $this->load->view('report/report_targeting', $data);
        $this->load->view('common/footer');
    }
    
    public function report_targeting_detail(){
    
        $data_type = $this->input->post('data_type');
        $data_key = $this->input->post('data_key');
        $targeting = $this->input->post('targeting');
            
        //$data_type = "advertiser";
        //$data_key = "2";
        //$targeting = "os";

        $data['data_type'] = $data_type;
        $data['data_key'] = $data_key;
        $data['targeting'] = $targeting;
    
        $fromto_date = $this->input->post('daterange');
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $data['max_os'] = $this->report_db->select_report_targeting_max_imp('os', $data_type, $data_key, $from_date, $to_date);
        $data['max_browser'] = $this->report_db->select_report_targeting_max_imp('browser', $data_type, $data_key, $from_date, $to_date);
        $data['max_device'] = $this->report_db->select_report_targeting_max_imp('device', $data_type, $data_key, $from_date, $to_date);
        $data['max_time'] = $this->report_db->select_report_targeting_max_imp('time', $data_type, $data_key, $from_date, $to_date);
        $data['max_category'] = $this->report_db->select_report_targeting_max_imp('category', $data_type, $data_key, $from_date, $to_date);
          
        if($targeting != null){
            /*페이징처리*/
            $per_page = $this->input->post('per_page');
            if($per_page==""){
                $data['per_page'] = 100;
            }else{
                $data['per_page'] = $per_page;
            }
        
            $data['total_rows'] = $this->report_db->select_report_targeting_count($targeting, $data_type, $data_key, $from_date, $to_date);
        
            $this->load->library('pagination');
            $config['base_url'] = "/report/report_targeting_detail/";
            $config['total_rows'] = $data['total_rows'];
            $config['per_page'] = $data['per_page'];
            $config['use_page_numbers'] = TRUE;
            $config['uri_segment'] = 3;
            $this->pagination->initialize($config);
        
            if($this->uri->segment(3) > 0)
                $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
            else
                $data['page_num'] = $this->uri->segment(3);
        
        
            $data['report_list'] = $this->report_db->select_report_targeting_list($targeting, $data_type, $data_key, $from_date, $to_date, $data['per_page'], $data['page_num']);
            $data['page_links'] = $this->pagination->create_links();
            /*페이징처리*/
        }
        $this->load->view('report/report_targeting_detail', $data);
    }
    
    public function targeting_search_form(){
        
        $mem_no = $this->session->userdata('mem_no');
        
        $advertiser_list = $this->report_db->select_advertiser_list($mem_no);
        
        $targeting = $this->input->post('targeting');
        
        $adver_no = $this->input->post('adver_no');
        
        $campaign_list = $this->report_db->select_campaign_list($adver_no);
        
        $cam_no = $this->input->post('cam_no');
        
        $creative_group_list = $this->report_db->select_creative_group_list($cam_no);
        
        $cre_gp_no = $this->input->post('cre_gp_no');
        
        $creative_list = $this->report_db->select_creative_list($cre_gp_no);
        
        $cre_no = $this->input->post('cre_no');
        
        $data = array(
            'targeting' => $targeting,   
            'adver_no' => $adver_no,
            'cam_no' => $cam_no,
            'cre_gp_no' => $cre_gp_no,
            'cre_no' => $cre_no,
            'advertiser_list' => $advertiser_list,
            'campaign_list' => $campaign_list,
            'creative_group_list' => $creative_group_list,
            'creative_list' => $creative_list
        );
        
        $this->load->view('report/targeting_search_form', $data);
    }
    
    public function report_integration(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['range'] = $range;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/report/report_integration'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('report/report_integration', $data);
        $this->load->view('common/footer');
    }
    
    public function report_integration_detail(){
        
        $mem_no = $this->session->userdata('mem_no');
        
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_ctr'] = $this->input->post('option_ctr');
        $data['option_ppc'] = $this->input->post('option_ppc');
        $data['option_ppi'] = $this->input->post('option_ppi');
        $data['option_price'] = $this->input->post('option_price');
        
        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_ctr'] == null){
            $data['option_ctr'] = "N";
        }
        if($data['option_ppc'] == null){
            $data['option_ppc'] = "N";
        }
        if($data['option_ppi'] == null){
            $data['option_ppi'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
        
        $data['daterange'] = $this->input->post('daterange');
        
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];

        $term = $this->input->post('term');
        $data['term'] = $term;
        
        $data['report_list'] = $this->report_db->select_report_integration_list($mem_no, $term, $from_date, $to_date);
  
        $this->load->view('report/report_integration_detail', $data);
    }
    
    public function report_business(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['daterange'] = $data['fromto_date'];
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/report/report_business'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('report/report_business', $data);
        $this->load->view('common/footer');
    }
    
    public function report_business_detail(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['datatype'] = $this->input->post('datatype');
        $data['datakey'] = $this->input->post('datakey');
        
        if( $data['datatype'] == null){
            $data['datatype'] = "role";
        }
        
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_ctr'] = $this->input->post('option_ctr');
        $data['option_ppc'] = $this->input->post('option_ppc');
        $data['option_ppi'] = $this->input->post('option_ppi');
        $data['option_price'] = $this->input->post('option_price');
    
        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_ctr'] == null){
            $data['option_ctr'] = "N";
        }
        if($data['option_ppc'] == null){
            $data['option_ppc'] = "N";
        }
        if($data['option_ppi'] == null){
            $data['option_ppi'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
        
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
        
        $data['report_list'] = $this->report_db->select_report_business_list($data, $from_date, $to_date);
    
        $this->load->view('report/report_business_detail', $data);
    }
   
    public function report_sales(){

        $mem_no = $this->session->userdata('mem_no');
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['daterange'] = $data['fromto_date'];
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/report/report_sales'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('report/report_sales', $data);
        $this->load->view('common/footer');
    }
    
    public function report_sales_detail(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['datatype'] = $this->input->post('datatype');
        $data['datakey'] = $this->input->post('datakey');
    
        if( $data['datatype'] == null){
            $data['datatype'] = "role";
        }
    
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_ctr'] = $this->input->post('option_ctr');
        $data['option_ppc'] = $this->input->post('option_ppc');
        $data['option_ppi'] = $this->input->post('option_ppi');
        $data['option_price'] = $this->input->post('option_price');
    
        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_ctr'] == null){
            $data['option_ctr'] = "N";
        }
        if($data['option_ppc'] == null){
            $data['option_ppc'] = "N";
        }
        if($data['option_ppi'] == null){
            $data['option_ppi'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['report_list'] = $this->report_db->select_report_sales_list($data, $from_date, $to_date);
    
        $this->load->view('report/report_sales_detail', $data);
    }
    

    
    function report_daily_form(){
        $mem_no = $this->session->userdata('mem_no');
        
        $datatype = $this->input->post('datatype');
        $data['datatype'] = $datatype;
        $datakey = $this->input->post('datakey');
        $data['datakey'] = $datakey;
        
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_ctr'] = $this->input->post('option_ctr');
        $data['option_ppc'] = $this->input->post('option_ppc');
        $data['option_ppi'] = $this->input->post('option_ppi');
        $data['option_price'] = $this->input->post('option_price');
        
        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_ctr'] == null){
            $data['option_ctr'] = "N";
        }
        if($data['option_ppc'] == null){
            $data['option_ppc'] = "N";
        }
        if($data['option_ppi'] == null){
            $data['option_ppi'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
        
        $data['daterange'] = $this->input->post('daterange');
        
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];

        $term = $this->input->post('term');
        $data['term'] = $term;
        
        $data['report_list'] = $this->report_db->select_report_daily_list($data, $term, $from_date, $to_date);
        
        $data['sales_nm'] = $data['report_list'][0]['sales_nm'];
        $data['group_role'] = $data['report_list'][0]['group_role'];
        $data['group_nm'] = $data['report_list'][0]['group_nm'];
        $data['mem_com_nm'] = $data['report_list'][0]['mem_com_nm'];
        $data['mem_type'] = $data['report_list'][0]['mem_type'];
        
        
        $this->load->view('report/report_daily_form', $data);
    }
    

    public function report_manager(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['daterange'] = $data['fromto_date'];
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/report/report_manager'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('report/report_manager', $data);
        $this->load->view('common/footer');
    }
    
    public function report_manager_detail(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['datatype'] = $this->input->post('datatype');
        $data['datakey'] = $this->input->post('datakey');
    
        if( $data['datatype'] == null){
            $data['datatype'] = "role";
        }
    
        $data['option_imp'] = $this->input->post('option_imp');
        $data['option_click'] = $this->input->post('option_click');
        $data['option_ctr'] = $this->input->post('option_ctr');
        $data['option_ppc'] = $this->input->post('option_ppc');
        $data['option_ppi'] = $this->input->post('option_ppi');
        $data['option_price'] = $this->input->post('option_price');
    
        if($data['option_imp'] == null){
            $data['option_imp'] = "N";
        }
        if($data['option_click'] == null){
            $data['option_click'] = "N";
        }
        if($data['option_ctr'] == null){
            $data['option_ctr'] = "N";
        }
        if($data['option_ppc'] == null){
            $data['option_ppc'] = "N";
        }
        if($data['option_ppi'] == null){
            $data['option_ppi'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['report_list'] = $this->report_db->select_report_business_list($data, $from_date, $to_date);
    
        $this->load->view('report/report_manager_detail', $data);
    }
    
}
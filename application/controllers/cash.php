<?php
class Cash extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $this->load->helper(array (
                'form',
                'url'
        ));
        $this->load->model("member_db");
        $this->load->model("code_db");
        $this->load->model("cash_db");
        $this->load->model("account_db");
        $this->load->model("calculate_db");
        $this->load->model("admanagement_db");
        $this->load->model("board_db");
        $this->load->library('menu');
    }
    
    public function cash_info_chart(){
        $mem_no = $this->input->post('mem_no');
        $data['use_daily_cash'] = $this->cash_db->select_use_daily_cash($mem_no);
        $this->load->view('cash/cash_info_chart',$data);
    }
    
    public function m_cash_charge(){
        $mem_no = $this->session->userdata('mem_no');
        $data['mem_cash_info'] = $this->cash_db->select_member_cash_info($mem_no);
        
        $this->load->view('common/m_header');
        $this->load->view('common/m_lmenu');
        $this->load->view('cash/m_cash_charge', $data);
        $this->load->view('common/m_footer');
    }
    
    public function cash_charge_list(){
        $mem_no = $this->session->userdata('mem_no');
        $kind = $this->input->post('kind');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
        
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        
        $condition = array (
                "mem_no" => $mem_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        $data['total_rows'] = $this->cash_db->select_cash_charge_count($condition);
        
        $this->load->library('pagination');
        $config['base_url'] = "/cash/cash_charge_list/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['member_info'] = $this->account_db->select_master_info($mem_no);
        $data['report_info'] = $this->cash_db->select_report_info($mem_no);
        $data['cash_charge_list'] = $this->cash_db->select_cash_charge_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        $data['mem_no'] = $mem_no;
        $data['kind'] = $kind;
        $data['range'] = $range;
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/cash_charge_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/cash_charge_list',$data);
        $this->load->view('common/footer');
    }
    
    public function cash_history_list(){
        $mem_no = $this->session->userdata('mem_no');
        $kind = $this->input->post('kind');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $mem_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_cash_history_count($condition);
    
        $this->load->library('pagination');
        $config['base_url'] = "/cash/cash_charge_list/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['member_info'] = $this->account_db->select_master_info($mem_no);
        $data['report_info'] = $this->cash_db->select_report_info($mem_no);
        $data['cash_history_list'] = $this->cash_db->select_cash_history_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        $data['mem_no'] = $mem_no;
        $data['kind'] = $kind;
        $data['range'] = $range;
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/cash_charge_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/cash_history_list',$data);
        $this->load->view('common/footer');
    }
    
    public function cash_refund_list(){
        $mem_no = $this->session->userdata('mem_no');
        $kind = $this->input->post('kind');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $mem_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_cash_refund_count($condition);
    
        $this->load->library('pagination');
        $config['base_url'] = "/cash/cash_charge_list/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['member_info'] = $this->account_db->select_master_info($mem_no);
        $data['report_info'] = $this->cash_db->select_report_info($mem_no);
        $data['cash_refund_list'] = $this->cash_db->select_cash_refund_list($condition, $data['per_page'], $data['page_num']);
        //select_refund_request_list
        $data['page_links'] = $this->pagination->create_links();
        $data['mem_no'] = $mem_no;
        $data['kind'] = $kind;
        $data['range'] = $range;
        
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/cash_charge_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/cash_refund_list',$data);
        $this->load->view('common/footer');
    }
    
    public function tax_bill_list(){
        $mem_no = $this->session->userdata('mem_no');
        $order_col = $this->input->post('order_col');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');

        if ($fromto_date == null && $range== null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range!= null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
        
        $data['fromto_date'] = $fromto_date;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        if($order_col == ""){
            $order_col = "calculate_tax_dt";
        }
        /*페이징처리*/
        $per_page=$this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_tax_bill_count($mem_no, $from_date, $to_date);
    
        $this->load->library('pagination');
        $config['base_url'] = "/cash/tax_bill_list";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['forecast_amount'] = $this->cash_db->select_forecast_amount($mem_no);
        $data['member_info'] = $this->account_db->select_master_info($mem_no);
        $data['tax_bill_list'] = $this->cash_db->select_tax_bill_list($mem_no, $order_col, $data['per_page'], $data['page_num'], $from_date, $to_date);
        $data['page_links'] = $this->pagination->create_links();
        $data['mem_no'] = $mem_no;
        $data['order_col'] = $order_col;
        $data['range'] = $range;

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/tax_bill_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/tax_bill_list',$data);
        $this->load->view('common/footer');
    }
    
    public function request_republish_tax_bill() {
        $data['mem_no'] = $this->input->post("mem_no");
        $date_ym = $this->input->post("date_ym");
        $question_info = $this->account_db->select_master_info($data['mem_no']);
        
        $data['mem_nm'] = $question_info['mem_nm'];
        $data['mem_tel'] = $question_info['mem_tel'];
        $data['question_fl'] = '5';
        $data['mem_email'] = $question_info['mem_email'];
        $data['mem_com_url'] = $question_info['mem_com_url'];
        $data['question_nm'] = $question_info['mem_com_nm'].'('.$question_info['mem_id'].")의 ".$date_ym."정산서 재발행 요청";
        $data['question_cont'] = $question_info['mem_com_nm'].'('.$question_info['mem_id'].")의 ".$date_ym."정산서 재발행을 요청하였습니다.";
        $data['date_ymd'] = date("Y-m-d");
        $data['question_st'] = "1";
    
        $ret = $this->board_db->insert_question($data);
        if ($ret > 0){
            echo 'ok';
        }else{
            echo 'false';
        }
    }
    
    public function detail_tax_bill_view(){
        $mem_no = $this->input->post('mem_no');
        $date_ym = $this->input->post('date_ym');
        
        $data['date_ym'] = $date_ym; 
        $data['member_info'] = $this->account_db->select_master_info($mem_no);
        if ($this->session->userdata('mem_type') == 'master'){
            $data['taxbill_list'] = $this->calculate_db->select_tax_bill_detail_list($mem_no, $date_ym);
        }else{
            $data['taxbill_list'] = $this->cash_db->select_manager_tax_bill_detail($mem_no, $date_ym);
        }
        $this->load->view('cash/tax_bill_detail_view', $data);
    }
    
    public function manager_cash_summary(){
        $master_no = $this->session->userdata('mem_no');
        
        $manager_arr = $this->cash_db->select_manager_list($master_no);
        $data['manager_cash_info'] = $this->cash_db->select_manager_cash_info($master_no);
        $data['manager_cash_list'] = $this->cash_db->select_manager_cash_list($master_no);
        $data['report_info'] = $this->cash_db->select_manager_report_info($manager_arr);
        $data['manager_arr'] = $manager_arr;
        $data['master_no'] = $master_no;
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/manager_cash_summary'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/manager_cash_summary',$data);
        $this->load->view('common/footer');
    }
    
    public function manager_cash_info_chart(){
        $master_no = $this->input->post('mem_no');
        $manager_arr = $this->cash_db->select_manager_list($master_no);
        $data['use_daily_cash'] = $this->cash_db->select_manager_use_daily_cash($manager_arr);
        $this->load->view('cash/cash_info_chart',$data);
    }
    
    public function manager_cash_charge_list(){
        $mem_no = $this->session->userdata('mem_no');
        $manager_no = $this->input->post('manager_no');
        $master_no = $this->input->post('master_no');
        $kind = $this->input->post('kind');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null && $range== null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range!= null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $manager_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_cash_charge_count($condition);
    
        $this->load->library('pagination');
        $config['base_url'] = "/cash/manager_cash_summary";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['manager_info']=$this->account_db->select_master_info($manager_no);
        $data['manager_cash_charge_list']=$this->cash_db->select_cash_charge_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        $data['master_no'] = $master_no;
        $data['manager_no'] = $manager_no;
        $data['kind'] = $kind;
        $data['range'] = $range;
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/cash/manager_cash_summary';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/manager_cash_summary'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/manager_cash_charge_list',$data);
        $this->load->view('common/footer');
    }
    
    public function manager_cash_history_list(){
        $mem_no = $this->session->userdata('mem_no');
        $manager_no = $this->input->post('manager_no');
        $master_no = $this->input->post('master_no');
        $kind = $this->input->post('kind');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d", strtotime("-1 day"));
        }elseif($fromto_date != null && $range!= null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d", strtotime("-1 day"));
        }
        $data['fromto_date']=$fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $manager_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        /*페이징처리*/
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_cash_history_count($condition);
    
        $this->load->library('pagination');
        $config['base_url'] = "/cash/manager_cash_summary/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['manager_info'] = $this->account_db->select_master_info($manager_no);
        $data['manager_cash_history_list'] = $this->cash_db->select_cash_history_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        $data['master_no'] = $master_no;
        $data['manager_no'] = $manager_no;
        $data['kind'] = $kind;
        $data['range'] = $range;
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/cash/manager_cash_summary';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/manager_cash_summary'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/manager_cash_history_list',$data);
        $this->load->view('common/footer');
    }
    
    public function manager_cash_refund_list(){
        $mem_no = $this->session->userdata('mem_no');
        $manager_no = $this->input->post('manager_no');
        $master_no = $this->input->post('master_no');
        $kind = $this->input->post('kind');
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_no" => $manager_no,
                "kind" => $kind,
                "from_date" => $from_date,
                "to_date" => $to_date
        );
        
        /*페이징처리*/
        $per_page=$this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_cash_refund_count($condition);
    
        $this->load->library('pagination');
        $config['base_url'] = "/cash/manager_cash_summary/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['manager_info'] = $this->account_db->select_master_info($manager_no);
        $data['manager_cash_refund_list'] = $this->cash_db->select_cash_refund_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        $data['master_no'] = $master_no;
        $data['manager_no'] = $manager_no;
        $data['kind'] = $kind;
        $data['range'] = $range;
        /*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/cash/manager_cash_summary';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($mem_no);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
        */
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/manager_cash_summary'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/manager_cash_refund_list',$data);
        $this->load->view('common/footer');
    }
    
    public function cash_charge_step1(){
        $mem_no = $this->input->post('mem_no');
        $charge_type = $this->input->post('charge_type');
        $data['mem_cash_info'] = $this->cash_db->select_member_cash_info($mem_no);
        
        $data['select_mem_list'] = $this->cash_db->select_member_list();

        $data['charge_type'] = $charge_type;
        $this->load->view('cash/cash_charge_step1',$data);
    }
    
    public function cash_charge_step2(){
        
        if($this->input->post('mem_no') == null){
            $data['mem_no'] = $this->session->userdata('mem_no');
        }else{
            $data['mem_no'] = $this->input->post('mem_no');
        }
        
        $data['cash'] = $this->input->post('cash');
        $data['charge_cash'] = $this->input->post('charge_cash');
        $data['charge_money'] = $this->input->post('charge_money');
        $data['cash_type'] = $this->input->post('cash_type');
        $data['charge_way'] = $this->input->post('charge_way');
        $data['charge_st'] = $this->input->post('charge_st');
        $data['origin_cash'] = $this->input->post('origin_cash');
        $data['after_charge'] = $this->input->post('after_charge');
        $data['charge_req_dt'] = date('Y-m-d');
        $data['charge_limit_dt'] = date("Y-m-d", strtotime("+3 day"));
        $this->load->view('cash/cash_charge_step2',$data);
    }
    
    public function cash_charge_save(){
        
        //$data['mem_no'] = $this->session->userdata('mem_no');
        
        $data['mem_no'] = $this->input->post('mem_no');
        
        $data['charge_cash'] = $this->input->post('charge_cash');
        
        $data['cash_type'] = $this->input->post('cash_type');
        $data['charge_way'] = $this->input->post('charge_way');
        
        $data['charge_money'] = $this->input->post('charge_money');
        
        $data['charge_st'] = $this->input->post('charge_st');
        
        //이벤트 캐쉬 지급일 경우
        if($data['charge_way'] == 6){
            $data['cash_type'] = "E";
            $data['charge_st'] = "C";
            $result = $this->cash_db->update_event_cash_pay($data);
            
            $this->load->model("service_db");
            $this->service_db->active_cash_low($data['mem_no']);
        }
        
        $data['charge_nm'] = $this->input->post('charge_nm');
        //$data['charge_way'] = '1';
        $data['charge_req_dt'] = $this->input->post('charge_req_dt');
        $data['charge_limit_dt'] = $this->input->post('charge_limit_dt');
        
        $result = $this->cash_db->insert_cash_charge_info($data);
        if($result > 0){
            $this->load->view('cash/cash_charge_step3', $data);
        }else{
            echo "false";
        }
    }
    
    public function m_cash_charge_save(){
        $data['mem_no'] = $this->session->userdata('mem_no');
        $data['charge_cash'] = $this->input->post('charge_cash');
        $data['charge_money'] = $this->input->post('charge_money');
        $data['cash_type'] = $this->input->post('cash_type');
        $data['cash_way'] = $this->input->post('cash_way');
        $data['charge_st'] = $this->input->post('charge_st');
        $data['charge_nm'] = $this->input->post('charge_nm');
        $data['charge_way'] = '1';
        $data['charge_req_dt'] = date('Y-m-d');
        $data['charge_limit_dt'] = date("Y-m-d", strtotime("+2 day"));
    
        $result = $this->cash_db->insert_cash_charge_info($data);
        if($result > 0){
            echo "true";
        }else{
            echo "false";
        }
    }

    public function cash_pay_management(){

        $mem_no = $this->session->userdata('mem_no');

        $data['mem_no'] = $mem_no;

        $condition = array(
            'charge_nm' => $this->input->post("cond_charge_nm"),
            'charge_st' => $this->input->post("cond_charge_st"),
            'charge_way' => 1
        );

        $data['cond_charge_nm'] = $this->input->post("cond_charge_nm");
        $data['cond_charge_st'] = $this->input->post("cond_charge_st");

        $per_page=$this->input->post('per_page');
        if($per_page==""){
            $data['per_page'] = 25;
        }else{
            $data['per_page'] = $per_page;
        }

        $data['total_rows'] = $this->cash_db->select_cash_list_count($condition);
        $data['kind_cnt'] = $this->cash_db->select_cash_kind_count();
        
        $this->load->library('pagination');
        $config['base_url'] = '/cash/cash_pay_management';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        $data['cash_list'] = $this->cash_db->select_cash_list($condition, $data['per_page'], $data['page_num']);

        $data['page_links'] = $this->pagination->create_links();

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/cash_pay_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('cash/cash_pay_management', $data);
        $this->load->view('common/footer');
    }
    
    public function cash_pay_later_management(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['mem_no'] = $mem_no;
    
        $condition = array(
                        'charge_nm' => $this->input->post("cond_charge_nm"),
                        'charge_st' => $this->input->post("cond_charge_st"),
                        'charge_way' => 5
        );
    
        $data['cond_charge_nm'] = $this->input->post("cond_charge_nm");
        $data['cond_charge_st'] = $this->input->post("cond_charge_st");
    
        $per_page=$this->input->post('per_page');
        if($per_page==""){
            $data['per_page'] = 25;
        }else{
            $data['per_page'] = $per_page;
        }
    
        $data['total_rows'] = $this->cash_db->select_cash_list_count($condition);
        $data['kind_cnt'] = $this->cash_db->select_cash_kind_count();
        
        $this->load->library('pagination');
        $config['base_url'] = '/cash/cash_pay_later_management';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
        $data['cash_list'] = $this->cash_db->select_cash_list($condition, $data['per_page'], $data['page_num']);
    
        $data['page_links'] = $this->pagination->create_links();

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/cash_pay_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('cash/cash_pay_later_management', $data);
        $this->load->view('common/footer');
    }
    
    public function cash_pay_event_management(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['mem_no'] = $mem_no;
    
        $condition = array(
                        'charge_nm' => $this->input->post("cond_charge_nm"),
                        'charge_st' => $this->input->post("cond_charge_st"),
                        'charge_way' => 6
        );
    
        $data['cond_charge_nm'] = $this->input->post("cond_charge_nm");
        $data['cond_charge_st'] = $this->input->post("cond_charge_st");
    
        $per_page=$this->input->post('per_page');
        if($per_page==""){
            $data['per_page'] = 25;
        }else{
            $data['per_page'] = $per_page;
        }
    
        $data['total_rows'] = $this->cash_db->select_event_cash_list_count($condition);
        $data['kind_cnt'] = $this->cash_db->select_cash_kind_count();
        
        $this->load->library('pagination');
        $config['base_url'] = '/cash/cash_pay_event_management';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
        $data['cash_list'] = $this->cash_db->select_event_cash_list($condition, $data['per_page'], $data['page_num']);
    
        $data['page_links'] = $this->pagination->create_links();
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/cash/cash_pay_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('cash/cash_pay_event_management', $data);
        $this->load->view('common/footer');
        
        //$this->output->enable_profiler(TRUE);
    }

    public function cash_pay_form(){
        $data['admin_no'] = $this->input->post("admin_no");
        $data['charge_no'] = $this->input->post("charge_no");
        $data['charge_st'] = $this->input->post("charge_st");
        $data['charge_way'] = $this->input->post("charge_way");
        $data['cash_info'] = $this->cash_db->select_cash_info($data);

        $this->load->view('cash/cash_pay_form', $data);
    }

    public function cash_pay(){

        $data['mem_no'] = $this->input->post("mem_no");
        $data['charge_cash'] = $this->input->post("charge_cash");
        $data['charge_no'] = $this->input->post("charge_no");
        $data['charge_st'] = $this->input->post("charge_st");
        $data['charge_way'] = $this->input->post("charge_way");
        $data['admin_no'] = $this->input->post("admin_no");
        $data['charge_confirm_dt'] = $this->input->post("charge_confirm_dt");
        $data['charge_dt'] = date("Y-m-d H:i:s");
 
        $result = $this->cash_db->update_cash_pay($data);
        
        $this->load->model("service_db");
        $this->service_db->active_cash_low($data['mem_no']);
        
        if($result){
            echo "ok";
        }else{
            echo "false";
        }
    }

    public function cash_revoke(){
        $data['charge_no'] = $this->input->post("charge_no");
        $data['admin_no'] = $this->input->post("admin_no");
        $data['mem_no'] = $this->input->post("mem_no");
        $data['charge_cash'] = $this->input->post("charge_cash");
        $data['charge_way'] = $this->input->post("charge_way");
        
        $result = $this->cash_db->update_cash_revoke($data);

        if($result){
            $http_referer = $this->input->server('HTTP_REFERER');
            $from = isset($http_referer) ? $http_referer : '/cash/cash_revoke';
            redirect($from, 'refresh');
        }
    }

    public function cash_pay_modify (){

        $data['charge_no'] = $this->input->post("charge_no");
        $data['charge_confirm_dt'] = $this->input->post("charge_confirm_dt");
        $result = $this->cash_db->update_charge_confirm_dt($data);
        if($result){
            echo "ok";
        }else{
            echo "false";
        }
    }
    
    public function refund_request_list(){
        $mem_type = $this->input->post('mem_type');
        $mem_com_nm = $this->input->post('mem_com_nm');
        $mem_id = $this->input->post('mem_id');
        $manager_yn = $this->input->post('manager_yn');
        $master_yn = $this->input->post('master_yn');
        $fromto_date = $this->input->post('fromto_date');;
        $cond_refund_st = $this->input->post('cond_refund_st');
        
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
        
        $condition = array (
                "mem_type" => $mem_type,
                "mem_com_nm" => $mem_com_nm,
                "mem_id" => $mem_id,
                "fromto_date" => $fromto_date,
                "from_date" =>$from_date,
                "to_date" =>$to_date,
                "master_yn" => $master_yn,
                "manager_yn" => $manager_yn,
                "cond_refund_st" => $cond_refund_st
        );
        
        $data['cond'] = $condition;
        
        /*페이징처리*/
        $per_page=$this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        $data['total_rows'] = $this->cash_db->select_refund_request_count($condition);
        
        $this->load->library('pagination');
        $config['base_url'] = "/cash/refund_request_list/";
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if($this->uri->segment(3) > 0)
            $data['page_num'] = ($this->uri->segment(3) + 0)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        $data['request_summary'] = $this->cash_db->select_refund_request_summary();
        $data['request_list'] = $this->cash_db->select_refund_request_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();
        
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/cash/refund_request_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/refund_request_list',$data);
        $this->load->view('common/footer');
    }
    
    public function refund_request_view(){
        $request_no = $this->input->post('request_no');
        $data['refund_type'] = $this->input->post('type');
        
        $condition = array (
                "request_no" => $request_no
        );
        
        $data['request_view'] = $this->cash_db->select_refund_request_view($condition);
        $data['sel_bank_code'] = $this->code_db->sel_bank_code();
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/cash/refund_request_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header',$menu_data);
        $this->load->view('cash/refund_request_view',$data);
        $this->load->view('common/footer');
    }
    
    public function refund_request_save(){
        $request_no = $this->input->post('request_no');
        $request_mem_no = $this->input->post('request_mem_no');
        $data['refund_cont'] = $this->input->post('refund_cont');
        $data['refund_st'] = $this->input->post('type');
        $data['refund_type'] = '1';
        $data['refund_amount'] = $this->input->post('refund_amount');
        $data['refund_bank'] = $this->input->post('refund_bank');
        $data['refund_bank_no'] = $this->input->post('refund_bank_no');
        $data['refund_bank_no_nm'] = $this->input->post('refund_bank_no_nm');
        $data['refund_reject_cont'] = $this->input->post('refund_reject_cont');
        $data['refund_reject_type'] = $this->input->post('refund_reject_type');
        $data['contact_no'] = $this->session->userdata('mem_no');
        if ($data['refund_st'] == "3"){
            $data['refund_ymd'] = date('Y-m-d');
        }
        
        $this->cash_db->update_refund_request($data, $request_no, $request_mem_no);
        
        redirect('/cash/refund_request_list', 'refresh');
    }
    
}
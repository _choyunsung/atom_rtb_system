<?php
class Calculate extends CI_Controller {


    public function __construct() {
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
            'form',
            'url'
        ));

        $this->load->model("calculate_db");     
        $this->load->library('menu');

    }

    function exchange_management(){

        $data['mem_no'] = $this->session->userdata('mem_no');

        $condition = array(
            'from_code_key' => $this->input->post("cond_from_code_key"),
            'term' => $this->input->post("cond_term")
        );

        $data['cond_from_code_key'] = $this->input->post("cond_from_code_key");
        $data['cond_term'] = $this->input->post("cond_term");

        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }

        $data['total_rows'] = $this->calculate_db->select_exchange_list_count($condition);

        $this->load->library('pagination');
        $config['base_url'] = '/calculate/exchange_management';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);


        $data['exchange_list'] = $this->calculate_db->select_exchange_list($condition, $data['per_page'], $data['page_num']);


        $data['page_links'] = $this->pagination->create_links();
/*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/calculate/exchange_management';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($data['mem_no']);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
*/
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/calculate/exchange_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('calculate/exchange_management', $data);
        $this->load->view('common/footer');

    }

    function fee_management(){

        $data['mem_no'] = $this->session->userdata('mem_no');

        $fromto_date = $this->input->post('fromto_date');


        $data['fromto_date'] = $fromto_date;

        $temp_date = explode(' ~ ', $fromto_date);

        $from_date = $temp_date[0];

        $to_date = $temp_date[1];
        
        if($this->input->post("role") == null){
            $role = "all";
        }else{
            $role = $this->input->post("role");
        }

        $condition = array(
            'role' => $role,
            'mem_com_nm' => $this->input->post("mem_com_nm"),
            'sales_nm' => $this->input->post("sales_nm"),
            'fromto_date' => $this->input->post("fromto_date"),
            "from_date" => $from_date,
            "to_date" => $to_date
        );

        $data['role'] = $condition['role'];
        $data['mem_com_nm'] = $condition['mem_com_nm'];
        $data['sales_nm'] = $condition['sales_nm'];

        $per_page = $this->input->post('per_page');

        if($per_page == ""){

            $data['per_page'] = 100;

        }else{

            $data['per_page'] = $per_page;

        }

        $data['total_rows'] = $this->calculate_db->select_fee_list_count($condition);
        $this->load->library('pagination');
        $config['base_url'] = '/calculate/fee_management';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);

        if( $data['page_num'] == null){
             $data['page_num'] = 0;
        }

        $data['fee_list'] = $this->calculate_db->select_fee_list($condition, $data['per_page'], $data['page_num']);
        $data['page_links'] = $this->pagination->create_links();

        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/calculate/fee_management'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        $this->load->view('common/header', $menu_data);
        $this->load->view('calculate/fee_management', $data);
        $this->load->view('common/footer');

    }
    
    function fee_modify(){
        $mem_no = $this->input->post("mem_no");
        $mem_com_fee = $this->input->post("mem_com_fee");
        return $this->calculate_db->modify_fee($mem_no, $mem_com_fee);
    }
    
    public function tax_bill_list(){
    
        $mem_no = $this->session->userdata('mem_no');
        $adver_yn = $this->input->post('adver_yn');
        $agency_yn = $this->input->post('agency_yn');
        $rep_yn = $this->input->post('rep_yn');
        $publication_st = $this->input->post('publication_st');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        
        if ($year.$month == ""){
            $year = date('Y');
            $month = date('m');
        }
        if (strlen($month) == 1){
            $month = "0".$month;
        }
    
        $condition = array(
                'mem_no' => $mem_no,
                'adver_yn' => $adver_yn,
                'agency_yn' => $agency_yn,
                'rep_yn' => $rep_yn,
                'publication_st' => $publication_st,
                'year' => $year,
                'month' => $month
        );
    
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
    
        $data['total_rows'] = $this->calculate_db->select_tax_bill_count($condition);
        $data['cond'] = $condition;
        $this->load->library('pagination');
        $config['base_url'] = '/calculate/tax_bill_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
    
        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
    
    
        $data['taxbill_list'] = $this->calculate_db->select_tax_bill_list($condition, $data['per_page'], $data['page_num']);
        $data['taxbill_summary'] = $this->calculate_db->select_tax_bill_summary($condition);
    
        $data['page_links'] = $this->pagination->create_links();
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/calculate/tax_bill_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('calculate/tax_bill_list', $data);
        $this->load->view('common/footer');
    
    }
    
    public function tax_bill_view(){
        $mem_no = $this->input->get('mem_no');
        $date_ym = $this->input->get('date_ym');
        
        if ($mem_no == ""){
            $mem_no = $this->input->post('mem_no');
        }
        if ($date_ym == ""){
            $date_ym = $this->input->post('date_ym');
        }
        
        $data['date_ym'] = $date_ym;
        $data['agency_no'] = $mem_no;
        $data['taxbill_view'] = $this->calculate_db->select_tax_bill_view($mem_no, $date_ym);
        $data['taxbill_list'] = $this->calculate_db->select_tax_bill_detail_list($mem_no, $date_ym);
        $data['publish_list'] = $this->calculate_db->select_tax_bill_publish_list($mem_no, $date_ym);
        
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/calculate/tax_bill_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header', $menu_data);
        $this->load->view('calculate/tax_bill_view', $data);
        $this->load->view('common/footer');
    
    }
    
    public function publish_tax_bill(){
        $data['mem_no'] = $this->input->post('mem_no');
        $data['publish_mem_no'] = $this->input->post('publish_mem_no');
        $data['publish_mem_nm'] = $this->input->post('publish_mem_nm');
        $data['loc_price'] = $this->input->post('publish_price');
        $data['tax_loc_price'] = $this->input->post('publish_tax');
        $data['result_loc_price'] = $this->input->post('publish_result');
        $data['calculate_tax_dt'] = $this->input->post('write_ymd');
        $data['publish_dt'] = $this->input->post('publish_dt');
        $data['publish_fl'] = '4';
        $data['date_ym']= $this->input->post('date_ym');
        $tax_mem_email = $this->input->post('tax_mem_email');
        $history_no = $this->input->post('history_no');
        $chk_cal_tax_no = $this->input->post('chk_cal_tax_no');
        $date_ym = $this->input->post('date_ym');
        $mem_pay_later = $this->input->post('mem_pay_later');
        
        if ($history_no == ""){
            $history_no = $this->calculate_db->insert_calculate_tax_history($data);
        }else{
            $this->calculate_db->update_calculate_tax_history($data, $history_no);
        }
        
        if ($history_no > 0){
            $this->calculate_db->update_publish_calculate_tax($chk_cal_tax_no, $history_no, $date_ym, $data['mem_no']);
        }
        
        if ($mem_pay_later == 'Y'){
            $this->calculate_db->insert_receivable($data);
        }
        
        $this->tax_bill_send_mail($data['mem_no'], $date_ym, $tax_mem_email);
        
    }
    
    public function tax_bill_send_mail($mem_no, $date_ym, $tax_mem_email){
        $mem_no = $this->input->get('mem_no');
        $date_ym = $this->input->get('date_ym');
        if ($mem_no == ""){
            $mem_no = $this->input->post('mem_no');
        }
        if ($date_ym == ""){
            $date_ym = $this->input->post('date_ym');
        }
        
        $data['taxbill_view'] = $this->calculate_db->select_tax_bill_view($mem_no, $date_ym);
        $data['publish_list'] = $this->calculate_db->select_tax_bill_publish_list($mem_no, $date_ym);
        
        $config['mailtype']  = "html";
        $config['charset']   = "UTF-8";
        $config['protocol']  = "smtp";
        $config['smtp_host'] = "ssl://smtp.googlemail.com";
        $config['smtp_port'] = 465;
        $config['smtp_user'] = "adsense@adop.co.kr";
        $config['smtp_pass'] = "asdfqaz135%";
        $this->email->initialize($config);
        // 메일 수신 체크 데이터 생성
        $this->email->set_newline("\r\n");
        $this->email->clear();
        $this->email->from('adsense@adop.co.kr', '애드오피 운영');
        $this->email->to($tax_mem_email);
        $this->email->subject('ATOM '.$date_ym.'정산서');
        $msg = $this->load->view('calculate/tax_bill_mail_form',$data, true);
        $this->email->message($msg);
        $ret = $this->email->send();
        if ($ret){
            echo "<script>alert('정산서가 발송되었습니다.'); </script>";
            echo "<script>location.href='/calculate/tax_bill_list'; </script>";
        }else{
            echo "<script>alert('정산서 발송을 실패했습니다.'); </script>";
            echo "<script>location.href='/calculate/tax_bill_list'; </script>";
        }
    }
    
    public function tax_bill_temp_save(){
        $data['mem_no'] = $this->input->post('mem_no');
        $data['price'] = $this->input->post('publish_price');
        $data['tax_price'] = $this->input->post('publish_tax');
        $data['result_price'] = $this->input->post('publish_result');
        $data['calculate_tax_dt'] = $this->input->post('write_ymd');
        $data['publish_dt'] = $this->input->post('publish_dt');
        $data['date_ym'] = $this->input->post('date_ym');
        $data['publish_fl'] = '2';
        $history_no = $this->input->post('history_no');
        $chk_cal_tax_no = $this->input->post('chk_cal_tax_no');
        $date_ym = $this->input->post('date_ym');
    
        if ($history_no == ""){
            $history_no = $this->calculate_db->insert_calculate_tax_history($data);
        }else{
            $this->calculate_db->update_calculate_tax_history($data, $history_no);
        }
    
        if (count($chk_cal_tax_no) > 0){
            $this->calculate_db->update_temp_calculate_tax($chk_cal_tax_no, $history_no);
        }
    
        redirect('/calculate/tax_bill_list', 'refresh');
    }
    
    public function tax_bill_detail_modify_save(){
        $calculate_tax_no = $this->input->post('calculate_tax_no');
        $data['price'] = $this->input->post('price');
        $data['tax_price'] = $this->input->post('tax_price');
        $data['result_price'] = $this->input->post('result_price');
        $data['date_ym'] = $this->input->post('date_ym');
        $data['tax_cont'] = $this->input->post('tax_cont');
        
        $ret = $this->calculate_db->update_tax_bill_detail($data, $calculate_tax_no);
        if ($ret > 0){
            echo "ok";
        }else{
            echo "false";
        }
    }
    
    public function tax_bill_detail_add(){
        $data['price'] = $this->input->post('price');
        $data['tax_price'] = $this->input->post('tax_price');
        $data['result_price'] = $this->input->post('result_price');
        $data['date_ym'] = $this->input->post('date_ym');
        $data['tax_cont'] = $this->input->post('tax_cont');
        $data['mem_no'] = $this->input->post('mem_no');
    
        $ret = $this->calculate_db->insert_tax_bill_detail($data);
        if ($ret > 0){
            echo "ok";
        }else{
            echo "false";
        }
    }
    
    public function tax_bill_carry_over(){
        $history_no = $this->input->post('history_no');
        $mem_no = $this->input->post('mem_no');
        $data['publish_fl'] = '4';
        $data['mem_no'] = $mem_no;
        $data['date_ym'] = date('Y-m');
        $data['calculate_tax_dt'] = date('Y-m-d');
        $data['publish_dt'] = date('Y-m-d');
        $data['publish_mem_no'] = $this->session->userdata('mem_no');
        $data['publish_mem_nm'] = $this->session->userdata('mem_nm');
        
        
        if ($history_no != ""){
            $this->calculate_db->update_calculate_tax_history($data, $history_no);
        }else{
            $this->calculate_db->insert_calculate_tax_history($data);
        }
        
        $this->calculate_db->update_calculate_tax_carry_over($mem_no);
        
        echo "ok";
    }
    
    public function tax_bill_detail_forward(){
        $calculate_tax_no = $this->input->post('calculate_tax_no');
        $data['publish_fl'] = $this->input->post('publish_fl');
        $data['forward_fl'] = $this->input->post('forward_fl');
        $data['history_no'] = $this->input->post('history_no');
        $data['date_ym'] = $this->input->post('date_ym');
        $data['agency_no'] = $this->input->post('agency_no');
        
        $this->calculate_db->update_tax_bill_detail($data, $calculate_tax_no);
    
        echo "ok";
    }
    
    function revenue_intergration(){
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['range'] = $range;
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/calculate/revenue_intergration'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('calculate/revenue_intergration', $data);
        $this->load->view('common/footer');
    }
    
    function revenue_intergration_detail(){
        $mem_no = $this->session->userdata('mem_no');
    
        $data['option_pre_deposit'] = $this->input->post('option_pre_deposit');
        $data['option_deposit'] = $this->input->post('option_deposit');
        $data['option_price'] = $this->input->post('option_price');
        $data['option_fee_price'] = $this->input->post('option_fee_price');
        $data['option_refund'] = $this->input->post('option_refund');
        $data['option_revenue'] = $this->input->post('option_revenue');
    
        if($data['option_pre_deposit'] == null){
            $data['option_pre_deposit'] = "N";
        }
        if($data['option_deposit'] == null){
            $data['option_deposit'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "N";
        }
        if($data['option_fee_price'] == null){
            $data['option_fee_price'] = "N";
        }
        if($data['option_refund'] == null){
            $data['option_refund'] = "N";
        }
        if($data['option_revenue'] == null){
            $data['option_revenue'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['revenue_list'] = $this->calculate_db->select_revenue_intergration_list($mem_no, $term, $from_date, $to_date);
    
        $this->load->view('calculate/revenue_intergration_detail', $data);
    }
        
    public function revenue_business(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['daterange'] = $data['fromto_date'];
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/calculate/revenue_business'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('calculate/revenue_business', $data);
        $this->load->view('common/footer');
    }
    
    public function revenue_business_detail(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['datatype'] = $this->input->post('datatype');
        $data['datakey'] = $this->input->post('datakey');
    
        if( $data['datatype'] == null){
            $data['datatype'] = "role";
        }
    
        $data['option_pre_deposit'] = $this->input->post('option_pre_deposit');
        $data['option_deposit'] = $this->input->post('option_deposit');
        $data['option_price'] = $this->input->post('option_price');
        $data['option_fee_price'] = $this->input->post('option_fee_price');
        $data['option_refund'] = $this->input->post('option_refund');
        $data['option_revenue'] = $this->input->post('option_revenue');
    
        if($data['option_pre_deposit'] == null){
            $data['option_pre_deposit'] = "N";
        }
        if($data['option_deposit'] == null){
            $data['option_deposit'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "N";
        }
        if($data['option_fee_price'] == null){
            $data['option_fee_price'] = "N";
        }
        if($data['option_refund'] == null){
            $data['option_refund'] = "N";
        }
        if($data['option_revenue'] == null){
            $data['option_revenue'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['revenue_list'] = $this->calculate_db->select_revenue_business_list($data, $from_date, $to_date);
    
        $this->load->view('calculate/revenue_business_detail', $data);
    }
    
    function revenue_daily_form(){
        $mem_no = $this->session->userdata('mem_no');
    
        $datatype = $this->input->post('datatype');
        $data['datatype'] = $datatype;
        $datakey = $this->input->post('datakey');
        $data['datakey'] = $datakey;
    
        $data['option_pre_deposit'] = $this->input->post('option_pre_deposit');
        $data['option_deposit'] = $this->input->post('option_deposit');
        $data['option_price'] = $this->input->post('option_price');
        $data['option_fee_price'] = $this->input->post('option_fee_price');
        $data['option_refund'] = $this->input->post('option_refund');
        $data['option_revenue'] = $this->input->post('option_revenue');
    
        if($data['option_pre_deposit'] == null){
            $data['option_pre_deposit'] = "N";
        }
        if($data['option_deposit'] == null){
            $data['option_deposit'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "N";
        }
        if($data['option_fee_price'] == null){
            $data['option_fee_price'] = "N";
        }
        if($data['option_refund'] == null){
            $data['option_refund'] = "N";
        }
        if($data['option_revenue'] == null){
            $data['option_revenue'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['revenue_list'] = $this->calculate_db->select_revenue_daily_list($data, $term, $from_date, $to_date);
    
        $data['group_role'] = $data['revenue_list'][0]['group_role'];
        $data['group_nm'] = $data['revenue_list'][0]['group_nm'];
        $data['mem_com_nm'] = $data['revenue_list'][0]['mem_com_nm'];
    
        $this->load->view('calculate/revenue_daily_form', $data);
    }

    function deposit_pre_list(){
        $mem_com_nm = $this->input->post('mem_com_nm');
        $publish_fl = $this->input->post('publish_fl');
        if ($publish_fl == ""){
            $publish_fl = "all";
        }
        $type = $this->input->post('type');
        if ($type == ""){
            $type = "all";
        }
        $charge_way = $this->input->post('charge_way');
        if ($charge_way == ""){
            $charge_way = "all";
        }
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $condition = array(
                'type' => $type,
                'mem_com_nm' => $mem_com_nm,
                'charge_way' => $charge_way,
                'fromto_date' => $fromto_date,
                'to_date' => $to_date,
                'from_date' => $from_date,
                'publish_fl' => $publish_fl,
        );
    
        $data['fromto_date'] = $fromto_date;
        $data['range'] = $range;
        $data['cond'] = $condition;
        $data['deposit_summary'] = $this->calculate_db->select_deposit_summary();
        $data['deposit_list'] = $this->calculate_db->select_pre_deposit_list($condition);
    
    
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/calculate/deposit_pre_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('calculate/deposit_pre_list', $data);
        $this->load->view('common/footer');
    }
    
    function no_paid_list(){
        
        $type = $this->input->post('type');
        $mem_com_nm = $this->input->post('mem_com_nm');
        
        $condition = array(
                'type' => $type,
                'mem_com_nm' => $mem_com_nm
        );
        
        $per_page = $this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 100;
        }else{
            $data['per_page'] = $per_page;
        }
        
        $data['total_rows'] = $this->calculate_db->select_no_paid_list_count($condition);
        $data['cond'] = $condition;
        $this->load->library('pagination');
        $config['base_url'] = '/calculate/no_paid_list';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        
        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);
        
        
        $data['nopaid_list'] = $this->calculate_db->select_no_paid_list($condition);
        
        $data['page_links'] = $this->pagination->create_links();
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/calculate/no_paid_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $data['cond'] = $condition;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                'url' => '/calculate/no_paid_list'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('calculate/no_paid_list', $data);
        $this->load->view('common/footer');
    }
    
    public function update_receivable_detail(){
        $data['mem_no'] = $this->input->post('mem_no');
        $data['paid_amount'] = $this->input->post('paid_amount');
        $data['cont'] = $this->input->post('paid_cont');
        $data['no_paid_amount'] = $this->input->post('no_paid_amount');
        $detail_fl = $this->input->post('detail_fl');
        $data['insert_ymd'] = $this->input->post('insert_ymd');
        
        $this->calculate_db->insert_receivable_detail($data, $detail_fl);
        echo "ok";
    }
    
    public function no_paid_detail_list(){
        $mem_no = $this->input->post('mem_no');
        $data['loc_price_lt'] = $this->calculate_db->select_sum_loc_price_lt($mem_no);
        $data['deposit'] = $this->calculate_db->select_sum_deposit_price($mem_no);
        //$data['nopaid_summary']=$this->calculate_db->select_no_paid_detail_summry($mem_no);
        $data['nopaid_list'] = $this->calculate_db->select_no_paid_detail_list($mem_no);
        $this->load->view("/calculate/no_paid_detail_list", $data);
    }
}

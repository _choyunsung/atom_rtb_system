<?php 
class Login extends CI_Controller {
    
    public function __construct() {
    
        parent::__construct();
        $this->load->database();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->model("member_db");
        $this->load->model("menu_db");
    }
	
	public function index($data)
	{
	
        if($this->session->userdata('logged_in')){
            $mem_no = $this->session->userdata('mem_no');
            
            //유저권한의 첫번째 카테고리 
            $first_url = $this->menu_db->get_first_category();  
            if($first_url != ""){
                redirect($first_url, 'refresh');
            }else{
                $this->logout_process();
            }
        }else{ 
            if($this->agent->is_mobile()){
                $this->load->view('login/m_login', $data);
            }else{
                $this->load->view('login/login', $data);
            }
        }
	}
	
	public function switch_language($language = "") {
	
	    $language = ($language != "") ? $language : $this->config['language'];
	    $this->session->set_userdata('site_lang', $language);
	    $from = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url();
	    redirect($from, 'refresh');
	}
	
	public function login_process() {
	    $pass = $_POST['mem_pwd'];
	    $data['id'] = $_POST['mem_id_login'];
	    $data['pass'] = md5($pass);
	    $result= $this->member_db->select_login_info($data);
        $mem_no = $result['mem_no'];
        $mem_id = $result['mem_id'];
        $mem_nm = $result['mem_nm'];
        $mem_com_nm = $result['mem_com_nm'];
        $group_no = $result['group_no'];
        $mem_gb = $result['mem_gb'];
        $admin_fl = $result['admin_fl'];
        $mem_type = $result['mem_type'];
        $mem_pay_later = $result['mem_pay_later'];
        $mem_fl = $result['mem_fl'];
        // 로그인 시점
        $format = 'DATE_W3C';
        $time = time();
        $time_stamp = standard_date($format, $time);
        $md5_str = do_hash($data['id'] . $time_stamp, 'MD5');
        $login_data = array (
                'mem_fl' => $mem_fl,
                'mem_no' => $mem_no,
                'mem_nm' => $mem_nm,
                'mem_id' => $mem_id,
                'mem_com_nm' => $mem_com_nm,
                'mem_gb' => $mem_gb,
                'group_no' => $group_no,
                'admin_fl' => $admin_fl,
                'mem_type' => $mem_type,
                'mem_pay_later' => $mem_pay_later,
                'ip_addr' => $_SERVER["REMOTE_ADDR"]
        );

        $this->session->set_userdata('logged_in', $login_data);
        if ($result['mem_lang'] == "english"){
            $lang = "english";
        }else{
            $lang = "korean";
        }
        $this->session->set_userdata('mem_fl', $mem_fl);
        $this->session->set_userdata('mem_type', $mem_type);
        $this->session->set_userdata('site_lang', $lang);
        $this->session->set_userdata($login_data);
	    
        $cookie = array (
                'name' => 'ADOP_USER',
                'value' => $data['id'],
                'expire' => '86500'
        );

        $this->input->set_cookie($cookie);
        $this->member_db->insert_login_log($login_data);
        redirect("/", 'refresh');
	    
	}

    public function logout_process(){
        session_start();
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('site_lang');
        session_destroy();
        redirect('/', 'refresh');
    }

    function authenticate(){
        if (!$this->session->userdata('logged_in')){
            redirect('/', 'refresh');
        }
    }

	public function login_check(){
	    $pass = md5($_POST['mem_pwd']);
	    $id = $_POST['mem_id_login'];

        $this->db->select("mem_id");
        $this->db->from("mo_members");
        $this->db->where("mem_id", $id);
        $this->db->where("mem_pwd", $pass);
        $this->db->where("mem_fl", "N");
        $this->db->where("mem_active_st !=", "3");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            $this->login_process();
        }else{
            $data['login_err'] = "err";
            $this->index($data);
        }
	}
	
}

<?php 
class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->helper(array (
                'form',
                'url'
        ));
        $this->load->model("member_db");
        $this->load->model("code_db");
        $this->load->model("menu_db");
        $this->load->model("campaign_db");
        $this->load->library('menu');
    }
    
    public function id_check() {
        $id = $this->input->post('mem_id');
        $result = $this->member_db->id_check($id);
        echo $result;
        exit();
    }
    
    public function cert_no_check() {
        $id = $this->input->post('cert_no');
        $email = $this->input->post('mem_email');
        $kind = $this->input->post('kind');
        $condition = array (
                "no" => $id,
                "email" => $email,
                "kind" => $kind
        );
        $result = $this->member_db->cert_check($condition);
        echo $result;
        exit();
    }
    
    public function reg_no_check() {
        $reg_no = $this->input->post('com_reg_no');
        $result = $this->member_db->reg_no_check($reg_no);
        echo $result;
        exit();
    }
    
    public function member_join_step1(){
        $this->load->view("login/member_join_step1");
    }
    
    public function member_join_step2(){
        $data['email_host_list']=$this->code_db->sel_email_host_code();
        $data['member_gb_list']=$this->code_db->sel_member_gb_code();
        $this->load->view("login/member_join_step2", $data);
    }
    
    public function member_join_step3(){
        $id = $this->input->post('cert_no');
        $email = $this->input->post('mem_email');
        $kind = $this->input->post('kind');
        $email_tmp= explode('@',$email);
        $data['mem_com_nm'] = $this->input->post('mem_com_nm');
        $data['mem_com_no'] = $this->input->post('mem_com_no');
        $data['sel_mem_type'] = $this->input->post('mem_type');
        $data['email']=$email_tmp[0];
        $data['email_host']=$email_tmp[1];
        $condition = array (
                "no" => $id,
                "email" => $email,
                "kind" => $kind
        );
        $result = $this->member_db->cert_check($condition);
        $data['cell_no_list']= $this->code_db->sel_cell_no_code();
        $data['tel_no_list']= $this->code_db->sel_tel_no_code();
        if($result == "false"){
            echo "false";
        }else{
            if($data['sel_mem_type']=="company"){
                $this->load->view("login/member_join_step3_1", $data);
            }else{
                $this->load->view("login/member_join_step3_2", $data);
            }
        }
    }
    
    public function member_join_step4() {
        $data['mem_id'] = $this->input->post('mem_id');
        $data['mem_pwd'] = md5($this->input->post('mem_pwd'));
        $data['mem_com_nm'] = $this->input->post('mem_com_nm');
        $data['mem_nm'] = $this->input->post('mem_nm');
        $data['mem_com_no'] = $this->input->post('mem_com_no');
        $data['mem_com_site_nm'] = $this->input->post('mem_com_site_nm');
        $data['mem_com_url'] = $this->input->post('mem_com_url');
        $data['mem_tel'] = $this->input->post('mem_tel');
        $data['mem_cell'] = $this->input->post('mem_cell');
        $data['mem_email'] = $this->input->post('mem_email');
        $data['mem_post'] = $this->input->post('mem_post');
        $data['mem_addr'] = $this->input->post('mem_addr');
        $data['mem_ymd'] = date('Y-m-d H:i:s');
        $data['mem_lang'] = "korean";
        $data['mem_type'] = "master";
        $data['mem_cont'] = "마스터";
        $data['mem_fl'] = 'N';
        $data['sms_alarm_c10_fl'] = 'Y';
        $data['sms_alarm_d10_fl'] = 'Y';
        $data['sms_alarm_c0_fl'] = 'Y';
        $data['email_alarm_c10_fl'] = 'Y';
        $data['email_alarm_d10_fl'] = 'Y';
        $data['email_alarm_c0_fl'] = 'Y';
        
        $mem_type = $this->input->post('mem_type');
        if($mem_type != "individual"){
            $data['mem_gb'] = 'ADT';
        }else{
            $data['mem_gb'] = 'IND';
        }
        $result = $this->member_db->insert_member_info($data);
        $data['mem_no'] = $result;
        //첫등록시 광고주로 권한설정
        //광고주그룹아이디 : 10
        
        if($mem_type == 'individual'){
            $this->menu_db->insert_category_member(IND_GROUP_NUMBER, $data['mem_no']);
        }else{
            $this->menu_db->insert_category_member(ADVER_GROUP_NUMBER, $data['mem_no']);
        }
        
        //광고주 자신을 광고주 그룹에 넣는다.
        $group['agency_no'] = $data['mem_no'];
        $group['adver_no'] = $data['mem_no'];
        $this->campaign_db->insert_new_member_group($group);

        if($mem_type == "individual"){
            echo "finish";
        }else{
            $this->load->view("login/member_join_step4", $data);
        }
    }

    public function member_join_cancel(){
        $mem_no = $this->input->post('mem_no');
        $this->member_db->delete_membre_info($mem_no);


    }
    
    public function member_join_step5() {
        $mem_no = $this->input->post('mem_no');
        $data['mem_com_ceo'] = $this->input->post('mem_com_ceo');
        $data['mem_com_type'] = $this->input->post('mem_com_type');
        $data['mem_com_item'] = $this->input->post('mem_com_item');
        $data['mem_post'] = $this->input->post('mem_post');
        $data['mem_addr'] = $this->input->post('mem_address');
        $result = $this->member_db->update_company_info($data, $mem_no);
        echo $result;
    }
    
    public function member_id_find_step1(){
        $data['email_host_list']=$this->code_db->sel_email_host_code();
        $this->load->view("login/member_id_find_step1", $data);
    }
    
    public function member_id_find_step2(){
        $data['mem_com_nm']= $this->input->post('mem_com_nm');
        $data['mem_com_no']= $this->input->post('mem_com_no');
        $ret['list'] = $this->member_db->get_find_member_step1($data);
        if($ret['list']=="false"){
            echo "false";
        }else{
            $this->load->view("login/member_id_find_step2", $ret);
        }
    }
    
    public function member_id_find_step3(){
        $data['cert_no']= $this->input->post('cert_no');
        $data['mem_email']= $this->input->post('mem_email');
        $ret['list']= $this->member_db->get_find_full_id($data);
        if($ret['list']=='false'){
            echo "false";
        }else{
            $this->load->view("login/member_id_find_step3", $ret);
        }
    }

    public function member_pwd_find_step1(){
        $this->load->view("login/member_pwd_find_step1");
    }

    public function member_pwd_find_step2() {
        
        $data['mem_id']= $this->input->post('mem_id');
        $data['mem_com_no']= $this->input->post('mem_com_no');
        $data['mem_type']= $this->input->post('mem_type');
        $ret['list'] = $this->member_db->get_find_pwd_step1($data);
        if(count($ret['list'])>0){
            $this->load->view("login/member_pwd_find_step2",$ret);
        }else{
            echo "false";
        }
    }
    
    public function find_pwd_email_send(){
        $target_email=$this->input->post('mem_email');
        $kind=$this->input->post('kind');
        //인증번호 난수생성
        $cert_no=mt_rand(100000, 999999);
        $data['cert_no']=$cert_no;
        $data['kind']=$kind;
        //이메일 중복체크
        $dup_email="true";
        if($kind=="join"){
            $dup_email = $this->member_db->check_email($target_email);
        } 
        if($dup_email=="true"){
            $this->member_db->update_member_cert($target_email,$cert_no,$kind);
            $config['mailtype']  = "html";
            $config['charset']   = "UTF-8";
            $config['protocol']  = "smtp";
            $config['smtp_host'] = "ssl://smtp.googlemail.com";
            $config['smtp_port'] = 465;
            $config['smtp_user'] = "adsense@adop.co.kr";
            $config['smtp_pass'] = "asdfqaz135%";
            $this->email->initialize($config);
            // 메일 수신 체크 데이터 생성
            $this->email->set_newline("\r\n");
            $this->email->clear();
            $this->email->from('adsense@adop.co.kr', '애드오피 운영');
            $this->email->to($target_email);
            //$this->email->to('justin@adop.co.kr');
            if($kind=="join"){
                $this->email->subject('ATOM 회원가입 인증번호 안내');
                $msg = $this->load->view('login/member_cert_mail_form',$data, true);
                $this->email->message($msg);
            }else if($kind=="pwd"){
                $this->email->subject('ATOM 비밀번호 찾기 인증번호 안내');
                $msg = $this->load->view('login/member_cert_mail_form',$data, true);
                $this->email->message($msg);
            }else{
                $this->email->subject('ATOM 아이디 찾기 인증번호 안내');
                $msg = $this->load->view('login/member_cert_mail_form',$data, true);
                $this->email->message($msg);
                //$this->email->message('비밀번호 찾기 인증번호는 ' .$cert_no. '입니다.');
            }
            $this->email->send();
            echo "OK";
        }else{
            echo "false";
        }
    }
    
    public function find_pwd_save() {
        $mem_pwd= $this->input->post('mem_pwd');
        $mem_email= $this->input->post('mem_email');
        $result = $this->member_db->update_member_pwd($mem_email,$mem_pwd);
        if($result=="ok"){
            echo "OK";
        }else{
            echo "FALSE";
        }
        exit;
    }
    
	public function test_page(){
	    $this->load->view('empty');
	    $this->load->view('common/footer');
	}

	
	public function test(){
	    $this->load->view('login/member_cert_mail_form');
	}
}

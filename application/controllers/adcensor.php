<?php
class Adcensor extends CI_Controller {

    public function __construct() {
    
        parent::__construct();
        $this->lang->load("word", $this->session->userdata('site_lang'));
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->helper('date');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $this->load->helper(array (
                'form',
                'url'
        ));

        $this->load->model("adcensor_db");
        $this->load->library('menu');
    }

    function ad_image(){

        $data['mem_no'] = $this->session->userdata('mem_no');

        $condition = array(
            'cre_evaluation' => $this->input->post("cond_cre_evaluation")
        );

        $data['cond_cre_evaluation'] = $this->input->post("cond_cre_evaluation");


        $per_page=$this->input->post('per_page');
        if($per_page == ""){
            $data['per_page'] = 10;
        }else{
            $data['per_page'] = $per_page;
        }

        $data['total_rows'] = $this->adcensor_db->select_adcensor_list_count($condition);

        $this->load->library('pagination');
        $config['base_url'] = '/adcensor/ad_image';
        $config['total_rows'] = $data['total_rows'];
        $config['per_page'] = $data['per_page'];
        $config['use_page_numbers'] = TRUE;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        if($this->uri->segment(3) > 0)
            $data['page_num'] = $this->uri->segment(3)*$config['per_page'] - $config['per_page'];
        else
            $data['page_num'] = $this->uri->segment(3);


        $data['adcensor_list'] = $this->adcensor_db->select_adcensor_list($condition, $data['per_page'], $data['page_num']);


        $data['page_links'] = $this->pagination->create_links();
/*
        $main_cates = $this->menu_db->get_main_category();
        $url = '/adcensor/ad_image';
        $auth = $this->menu_db->get_auth_category($url);
        $menu_data['url'] = $url;
        $menu_data['sub_menu'] = $this->menu_db->get_sub_menu($url);
        $menu_data['main_menu'] = $this->menu_db->get_main_menu($url);
        $menu_data['cash_info'] = $this->account_db->select_master_info($data['mem_no']);
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }
*/
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/adcensor/ad_image'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
        
        $this->load->view('common/header', $menu_data);
        $this->load->view('adcensor/ad_image', $data);
        $this->load->view('common/footer');
    }

    function adcensor_evaluation(){
        $cre_no = $this->input->post("cre_no");
        $admin_no = $this->input->post("admin_no");
        $adver_no = $this->input->post("adver_no");
        $cre_evaluation = $this->input->post("cre_evaluation");


        $cre_no = explode(",", $cre_no);
        foreach ($cre_no as $cn) {
            $data['cre_no'] = $cn;
            $data['admin_no'] = $admin_no;
            $data['cre_evaluation'] = $cre_evaluation;
            $result = $this->adcensor_db->update_adcensor_evaluation($data);
        }

        if($result){
            $http_referer = $this->input->server('HTTP_REFERER');
            $from = isset($http_referer) ? $http_referer : '/adcensor/ad_image';
            redirect($from, 'refresh');
        }
    }

}
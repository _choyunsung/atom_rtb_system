<?php

function tmplogs($log_txts)
{

	$log_dir = "/tmp";
	$log_file = @fopen('/tmp/logs.log', "a+");
	$_datas = @fread($log_file,filesize($log_file));

	$log_txt = (is_array($log_txts))?json_encode($log_txts):$log_txts;

	@fwrite($log_file, $_datas.$_SERVER['REMOTE_ADDR'].' - ['.date('Y-m-d H:i:s').'] - '.$log_txt."\r\n");
	@fclose($log_file);
}

### 파일 디렉토리 전부 만들기
function mkdirp($path)
{
	$file_path = pathinfo($path);
	if(!$file_path['extension'])
	   $_file_path =  explode('/',$path);
	else
	   $_file_path =  explode('/',$file_path['dirname']);

	unset($_file_path[0]);
	$_path_array = array_values($_file_path);

	$_tmp_path = '';
	foreach($_path_array as $key => $val )
	{
		$_tmp_path .= '/'.$val;
		mkdir($_tmp_path,0777);

	}

}

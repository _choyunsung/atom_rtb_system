<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * params_array
 * input value 에서 배열에 지정한 값들만 리턴
 * */
function params_array($array=array())
{
	if(!is_array($array))
		show_404('params_array must be array type');

	$CI = &get_instance();
	$CI->load->library('user_agent','input');
	$type = $CI->input->server('REQUEST_METHOD');


	foreach($array as $key => $val )
	{
		$_return[$val] =  $CI->input->{$type}($val,true);
	}

	return $_return;
}

function pcurl($url, $data) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	$resData = curl_exec($ch);
	curl_close($ch);

	return $resData;
}

function gcurl($url,$con_time_out = 1,$time_out = 2) {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $con_time_out);
		curl_setopt($ch, CURLOPT_TIMEOUT, $time_out);
		$resData = curl_exec($ch);
		curl_close($ch);
		return $resData;
}

### 암호화
function encryt($plain_text)
{
	return base64_encode(openssl_encrypt(sprintf('%s####%s',$plain_text,microtime(true) * 100) , "aes-256-cbc", ENCRYTIONKEY , true, str_repeat(chr(0), 16)));
}

### 복호화 
function decryt($base64_text)
{
	$_first_temp =  openssl_decrypt(base64_decode($base64_text), "aes-256-cbc", ENCRYTIONKEY , true, str_repeat(chr(0), 16));
	$_string = explode('####',$_first_temp);

	if(is_numeric($_string[1]) !== true )
	{
		return $base64_text;
	}

	return  $_string[0];
}


function array_select($array,$select)
{
	$_keys = explode(',',$select);
	if(is_array($_keys))
	{
	foreach($array as $keys => $vals )
	{
		$vals_temp = '';
		foreach($vals as $key => $val )
		{
			if(in_array($key,$_keys))
				$vals_temp[$key] = $val;
		}
		$_return[$keys] = $vals_temp;
	}
	return $_return;
	}else return false;
}

function get_rand ($start,$end,$count) {
	$temp = array();
	$c=0;
	while (1) { // 무한 반복
		$key = mt_rand($start,$end); // 지정 범위의 난수 구하기
		if (!$temp[$key]) { $c++; $temp[$key]='1'; } // 해당 배열값이 존재하는지 검사
		if ($c==$count) break; // 지저한 수만큼의 난수를 구했다면 종료
	}
	return array_keys($temp);
}

function file_force_contents($filename, $data, $flags = 0){
	if(!is_dir(dirname($filename)))
		mkdir(dirname($filename).'/', 0777, TRUE);
	return file_put_contents($filename, $data,$flags);
}

/**
 * SAVE LOCAL CACHE DATA
 */
function save_cache_datta($path, $data, $type='json')
{
	try {
		file_force_contents($path, $data);
	}catch(Exception $e)
	{

	}

}



function get_cache_data($path,$type='json')
{

	if(file_exists($path))
	{
		$_contents = file_get_contents($path);
		var_dump($_contents);
		switch($type)
		{
			case "json":return json_decode($_contents,true);break;
			case "serializable":return serializable($_contents);break;
		}

	}else
		return false;

}

function multiarray_post($_post)
{
	foreach (array_keys($_post) as $item => $values)
	{

		foreach ($_post[$values] as $key => $val )
		{
            $_RETURN[$key][$values] = $val;
//            $_RETURN[$key]['files'] = $_FILES[$key];
        }
	}
	return $_RETURN;
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Menu {

    public function __construct() {
        //parent::__construct();
    }

    public function menu_info($params){
        
        $CI =& get_instance();
        
        $CI->load->library('session');
        $CI->load->helper('url');
        $CI->load->database();
        $CI->load->model('menu_db');
        $CI->load->model('account_db');
        $CI->load->model('service_db');

        $mem_no = $CI->session->userdata('mem_no');
        $mem_fl = $CI->session->userdata('mem_fl');
        
        $main_cates = $CI->menu_db->get_main_category();
        $auth = $CI->menu_db->get_auth_category($params['url']);
        $menu_data['url'] = $params['url'];
        
        $menu_data['main_menu'] = $CI->menu_db->get_main_menu($params['url']);
        $menu_data['sub_menu'] = $CI->menu_db->get_sub_menu($params['url']);
        $menu_data['cash_info'] = $CI->account_db->select_master_info($mem_no);
        $menu_data['alarm_info'] = $CI->service_db->select_alarm_info($mem_no);
        
        if (isset($main_cates)) {
            $idx = 0;
            foreach ($main_cates as $main_cate) {
                $menu_data['rows'][$idx]['cate_no'] = $main_cate->cate_no;
                $menu_data['rows'][$idx]['cate_nm'] = $main_cate->cate_nm;
                $menu_data['rows'][$idx]['cate_url'] = $main_cate->cate_url;
                $idx++;
            }
        }

        if($menu_data['cash_info']['mem_id']=='auction')
        {
            array_push( $menu_data["rows"],array("cate_no" => "18",
                    "cate_nm" => "strAuctionItems",
                    "cate_url" => "/auction/items")
            );
        }

        if($mem_fl == "Y"){
            redirect("/access");
        }

        if(count($auth) == 0){
                        
            return $menu_data;
            //redirect("/access");
        }else{
            return $menu_data;
        }
    }   
}
<?php

class AuctionSessionApi {

    // Declare Needed Variables
    private $action = "http://www.auction.co.kr/ServiceInterfaces/RequestApplicationTicket";
    //private $serverUrl = "https://api.auction.co.kr/APIv1/SecurityService.asmx";
    private $serverUrl = "https://apitest.auction.co.kr/APIv1/SecurityService.asmx";
    //private $serverUrl = "https://api.auction.co.kr/APIv1/Securityservice.asmx";
    private $devID;
    private $appID;
    private $appPWD;


    function __construct($_session){
        
        $this->devID = $_session['devID'];
        $this->appID = $_session['appID'];
        $this->appPWD = $_session['appPWD'];
    }

    public function doService(){

        // Set Request SOAP Message
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>';
        $requestXmlBody .= '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
        $requestXmlBody .= '<soap:Body>';
        $requestXmlBody .= '<RequestApplicationTicket xmlns="http://www.auction.co.kr/ServiceInterfaces">';
        $requestXmlBody .= '<req DevID="'.$this->devID.'" AppID="'.$this->appID.'" AppPassword="'.$this->appPWD.'" />';
        $requestXmlBody .= '</RequestApplicationTicket>';
        $requestXmlBody .= '</soap:Body>';
        $requestXmlBody .= '</soap:Envelope>';

        // Load the XML Document to Print Request SOAP
        $requestDoc = new DomDocument();
        $requestDoc->loadXML($requestXmlBody);

//        // Print Request SOAP
//        echo "<PRE>";
//        echo "<STRONG>* REQUEST SOAP</STRONG><BR>";
//        echo htmlentities ($requestDoc->saveXML());
//        echo "</PRE>";

        //Create a new auction session with all details pulled in from included auctionSession.php
        $session = new AuctionCommon($this->serverUrl, $this->action);

        //send the request and get response
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        
        // Process Response
        return $this->processResponse ($responseXml);
    }

    private function processResponse($responseXml){
        if(stristr($responseXml, 'HTTP 404') || $responseXml == '') {
            die('<P>Error sending request');
        } else {
            //Xml string is parsed and creates a DOM Document object
            $responseDoc = new DomDocument();
            $responseDoc->loadXML($responseXml);

            // Print Response SOAP
//            echo "<PRE>";
//            echo "<STRONG>* RESPONSE SOAP</STRONG><BR>";
//            echo "<BR>".iconv("UTF-8", "EUC-KR", urldecode (htmlentities ($responseDoc->saveXML(), ENT_NOQUOTES, "UTF-8")) );
//            echo "</PRE>";

            // Error
            $eleFaultcode = $responseDoc->getElementsByTagName('faultcode')->item(0);
            $eleFaultstring = $responseDoc->getElementsByTagName('faultstring')->item(0);

            if (empty ($eleFaultcode)){
                //Process Response
                $eleAuthenticationTicket = $responseDoc->getElementsByTagName('RequestApplicationTicketResult')->item(0);

                if ($eleAuthenticationTicket != null){
                    return $eleAuthenticationTicket->nodeValue;
                }
            }else{
                $this->processError($eleFaultcode, $eleFaultstring);
            }
        }

        return "";
    }

    private function processError($eleFaultcode, $eleFaultstring){
        if ($eleFaultcode != null) echo "faultcode : ".iconv("UTF-8", "EUC-KR", urldecode (htmlentities ($eleFaultcode->nodeValue, ENT_NOQUOTES, "UTF-8")))."<BR>";
        if ($eleFaultstring != null) echo "faultstring : ".iconv("UTF-8", "EUC-KR", urldecode (htmlentities ($eleFaultstring->nodeValue, ENT_NOQUOTES, "UTF-8")))."<BR>";
    }
    
    
}
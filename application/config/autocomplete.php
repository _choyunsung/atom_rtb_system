<?php
// help IDE(s) support Codeigniter 2.0
/**
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Benchmark $benchmark
 * @property CI_Calendar $calendar
 * @property CI_Cart $cart
 * @property CI_Config $config
 * @property CI_Controller $controller
 * @property CI_Email $email
 * @property CI_Encrypt $encrypt
 * @property CI_Exceptions $exceptions
 * @property CI_Form_validation $form_validation
 * @property CI_Ftp $ftp
 * @property CI_Hooks $hooks
 * @property CI_Image_lib $image_lib
 * @property CI_Input $input
 * @property CI_Language $language
 * @property CI_Loader $load
 * @property CI_Log $log
 * @property CI_Model $model
 * @property CI_Output $output
 * @property CI_Pagination $pagination
 * @property CI_Parser $parser
 * @property CI_Profiler $profiler
 * @property CI_Router $router
 * @property CI_Session $session
 * @property CI_Sha1 $sha1
 * @property CI_Table $table
 * @property CI_Trackback $trackback
 * @property CI_Typography $typography
 * @property CI_Unit_test $unit_test
 * @property CI_Upload $upload
 * @property CI_URI $uri
 * @property CI_User_agent $user_agent
 * @property CI_Validation $validation
 * @property CI_Xmlrpc $xmlrpc
 * @property CI_Xmlrpcs $xmlrpcs
 * @property CI_Zip $zip
 *
 * #### MODLE ####
 * @property account_db $account_db
 * @property adcensor_db $adcensor_db
 * @property admanagement_db $admanagement_db
 * @property admin_db $admin_db
 * @property board_db $board_db
 * @property calculate_db $calculate_db
 * @property campaign_db $campaign_db
 * @property cash_db $cash_db
 * @property code_db $code_db
 * @property creative_db $creative_db
 * @property member_db $member_db
 * @property menu_db $menu_db
 * @property report_db $report_db
 * @property service_db $service_db
 *
 *
 * ### libraries ####
 * @property adopcommon $adopcommon
 * @property auctionApi $auctionapi
 * @property auctioncommon $auctioncommon
 * @property s3 $s3
 * @property phpexcel $phpexcel
 * @property menu $menu
 * @property mobile_detect $mobile_detect
 *
 *
 */
class CI_Controller {};
class MY_Controller extends CI_Controller {};
/**
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Config $config
 * @property CI_Loader $load
 * @property CI_Session $session
 */
class CI_Model {};
/* End of file autocomplete.php */
/* Location: ./application/config/autocomplete.php */
/**
 * @property CI_Session $session
 */

?>
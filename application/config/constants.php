<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('IMAGE_BASE_URL',		                'http://cdn.ads-optima.com/ad_img/');
define('BOARD_IMAGE_BASE_URL',		                'http://cdn.ads-optima.com/board_file/');
/* End of file constants.php */
/* Location: ./application/config/constants.php */


define('IND_GROUP_NUMBER', 2);
define('ADVER_GROUP_NUMBER', 2);
define('ADVER_MANAGER_GROUP_NUMBER', 5);
define('AGENCY_MANAGER_GROUP_NUMBER', 6);
define('LAB_MANAGER_GROUP_NUMBER', 7);


define('ENCRYTIONKEY','doemdhvldkxhatltmxpa');

if($_SERVER['REMOTE_ADDR']=='112.221.85.219' || $_SERVER['REMOTE_ADDR']=='::1')
    define('DEVEL_ADMIN',true);
else
    define('DEVEL_ADMIN',false);


define('ADOP_LOGO','http://cdn.ads-optima.com/adop/logo_ico_adop.png');
define('AUCTION_LOGO_ICON','http://cdn.ads-optima.com/atom/logo_auction.png');
define('AUCTION_LOGO_ALLKILL_ICON','http://cdn.ads-optima.com/atom/logo_allkill.png');
define('AUCTION_LOGO_ALLKILL_W_ICON','http://cdn.ads-optima.com/atom/logo_auction_w.png');

define('AUCTION_USER_ID','auction');

define('AUTION_CLICK_CHECK','http://log.ads-optima.com/auction/click?l=');
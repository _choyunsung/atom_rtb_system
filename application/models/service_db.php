<?php
class Service_Db extends CI_Model {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	public function test()
	{
		$_DATA = '';
		$_loc = $_GET['type'];

		switch($_loc)
		{
			case "1":$_urls = 'adhtml/auction/auction_200x200_1';break;
			case "2":$_urls = 'adhtml/auction/auction_300x250_1';break;
			case "3":$_urls = 'adhtml/auction/auction_320x50_1';break;
			default:
				$_urls = 'adhtml/auction/auction_320x100_1';
		}
		$adm = $this->load->view($_urls,$_DATA,true);
		echo $adm;
	}


	function select_agency_no($mem_no){
		$this->db->select("mo_members_group.agency_no");
		$this->db->where('mo_members_group.adver_no', $mem_no);
		$this->db->from('mo_members_group');
		$this->db->join('mo_members', 'mo_members.mem_no = mo_members_group.adver_no');
		$query = $this->db->get();

		if( $query->num_rows() > 0 ){
			$row = $query->row();
			$agency_no = $row->agency_no;

			return $agency_no;
		}else{
			return $mem_no;
		}
	}

	function insert_bid_request($data){
		$this->db->insert('mo_bid_request', $data);
	}

	function insert_bid_response($data){

		$excepted_cre_gp = array();
		//매체의 카테고리 정보
		$bcate = json_decode($data['bcate']);
		//targeting
		//os, browser, device
		$device = json_decode($data['device'], TRUE);
		$user_agent = $device['ua'];

		//os와 browser 체크
		$OS = array(
			/* OS */
			array('Windows CE', 'Windows CE'),
			array('Win98', 'Windows 98'),
			array('Windows 9x', 'Windows ME'),
			array('Windows me', 'Windows ME'),
			array('Windows 98', 'Windows 98'),
			array('Windows 95', 'Windows 95'),
			array('Windows NT 6.3', 'Windows 8.1'),
			array('Windows NT 6.2', 'Windows 8'),
			array('Windows NT 6.1', 'Windows 7'),
			array('Windows NT 6.0', 'Windows Vista'),
			array('Windows NT 5.2', 'Windows 2003/XP x64'),
			array('Windows NT 5.01', 'Windows 2000 SP1'),
			array('Windows NT 5.1', 'Windows XP'),
			array('Windows NT 5', 'Windows 2000'),
			array('Windows NT', 'Windows NT'),
			array('Macintosh', 'Macintosh'),
			array('Mac_PowerPC', 'Mac PowerPC'),
			array('Unix', 'Unix'),
			array('bsd', 'BSD'),
			array('Linux', 'Linux'),
			array('Wget', 'Linux'),
			array('windows', 'ETC Windows'),
			array('mac', 'ETC Mac'),
		);

		$BW = array(
			/* BROWSER */
			array('MSIE 2', 'InternetExplorer 2'),
			array('MSIE 3', 'InternetExplorer 3'),
			array('MSIE 4', 'InternetExplorer 4'),
			array('MSIE 5', 'InternetExplorer 5'),
			array('MSIE 6', 'InternetExplorer 6'),
			array('MSIE 7', 'InternetExplorer 7'),
			array('MSIE 8', 'InternetExplorer 8'),
			array('MSIE 9', 'InternetExplorer 9'),
			array('MSIE 10', 'InternetExplorer 10'),
			array('Trident/7.0', 'InternetExplorer 11'),
			array('MSIE', 'ETC InternetExplorer'),
			array('Firefox', 'FireFox'),
			array('Chrome', 'Chrome'),
			array('Safari', 'Safari'),
			array('Opera', 'Opera'),
			array('Lynx', 'Lynx'),
			array('LibWWW', 'LibWWW'),
			array('Konqueror', 'Konqueror'),
			array('Internet Ninja', 'Internet Ninja'),
			array('Download Ninja', 'Download Ninja'),
			array('WebCapture', 'WebCapture'),
			array('LTH', 'LTH Browser'),
			array('Gecko', 'Gecko compatible'),
			array('Mozilla', 'Mozilla compatible'),
			array('wget', 'Wget command'),
		);

		foreach($OS as $val){
			if(mb_eregi($val[0], $user_agent)){
				$os = $val[0];
				break;
			}
		}

		foreach($BW as $val){
			if(mb_eregi($val[0], $user_agent)){
				$br = $val[0];
				break;
			}
		}

		//device 체크
		$this->load->library('mobile_detect');
		$this->mobile_detect->setUserAgent($user_agent);
		$isMobile = $this->mobile_detect->isMobile();
		$isTablet = $this->mobile_detect->isTablet();

		if( $isMobile && !$isTablet )
			$dv = "Mobile";

		if( !$isMobile && $isTablet )
			$dv = "Tablet";

		if( !$isMobile && !$isTablet )
			$dv = "PC";

		//시간과 요일 체크
		$time = $this->time_now();

		$this->db->select("cre_gp_no");

		//제외되는 os 광고그룹...
		$this->db->where("(tgt_type = 'os' AND tgt_fl = 'N' AND tgt_comparison = '!~'  AND FIND_IN_SET('".$os."', tgt_data))");
		$this->db->or_where("(tgt_type = 'os' AND tgt_fl = 'N' AND tgt_comparison = '=~'  AND NOT FIND_IN_SET('".$os."', tgt_data))");

		//제외되는 browser 광고그룹...
		$this->db->or_where("(tgt_type = 'browser' AND tgt_fl = 'N' AND tgt_comparison = '!~'  AND FIND_IN_SET('".$br."', tgt_data))");
		$this->db->or_where("(tgt_type = 'browser' AND tgt_fl = 'N' AND tgt_comparison = '=~'  AND NOT FIND_IN_SET('".$br."', tgt_data))");

		//제외되는 device 광고그룹...
		$this->db->or_where("(tgt_type = 'device' AND tgt_fl = 'N' AND tgt_comparison = '!~'  AND FIND_IN_SET('".$dv."', tgt_data))");
		$this->db->or_where("(tgt_type = 'device' AND tgt_fl = 'N' AND tgt_comparison = '=~'  AND NOT FIND_IN_SET('".$dv."', tgt_data))");

		//제외되는 시간과 요일 광고그룹...
		$this->db->or_where("(tgt_type = 'time' AND tgt_fl = 'N' AND tgt_comparison = '!~'  AND FIND_IN_SET('".$time."', tgt_data))");
		$this->db->or_where("(tgt_type = 'time' AND tgt_fl = 'N' AND tgt_comparison = '=~'  AND NOT FIND_IN_SET('".$time."', tgt_data))");

		//제외되는 카테고리 광고그룹...
		if(count($bcate) > 0){
			foreach($bcate as $bc){
				$this->db->or_where("(tgt_type = 'category' AND tgt_fl = 'N' AND tgt_comparison = '!~'  AND FIND_IN_SET('".$bc."', tgt_data))");
			}
		}

		if(count($bcate) > 0){
			$where = "";
			foreach($bcate as $key=>$bc){
				if($key == 0){
					$and = "";
				}else{
					$and = " AND ";
				}
				$where .= $and."(tgt_type = 'category' AND tgt_fl = 'N' AND tgt_comparison = '=~'  AND NOT FIND_IN_SET('".$bc."', tgt_data))";
			}
			$this->db->or_where($where);
		}


		$query = $this->db->get('mo_targeting');

		if ( $query->num_rows() > 0 ) {

			$excepted_cre_gp = $query->result_array();
		}

		$i = count($excepted_cre_gp);

		$ecg_list = "";

		foreach($excepted_cre_gp as $key=>$ecg){
			if($key+1 == $i){
				$ecg_list .= $ecg['cre_gp_no'];
			}else{
				$ecg_list .= $ecg['cre_gp_no'].",";
			}
		}

		$ecg_arr = explode(",", $ecg_list);
		$ecg_arr = array_unique($ecg_arr);
		$ecg_list = implode(",", $ecg_arr);

		//비딩 최소값
		$bidfloor = $data['bidfloor'];
		$bidfloor= str_replace( "\"","", $bidfloor );

		$this->db->select("*");
		$this->db->where('start_ymd <= ', date('Y-m-d'));
		$this->db->where('bid_price >= ', $bidfloor);
		$this->db->where('cre_gp_status', '1');
		$this->db->where('cre_gp_fl', 'N');
		if($ecg_list != null){
			$where = "cre_gp_no NOT IN (".$ecg_list.")";
			$this->db->where($where);
		}
		$query = $this->db->get('mo_creative_group');



		$cre_gp_temp = array();
		if ( $query->num_rows() > 0 )
			$cre_gp_temp = $query->result_array();

		//조건에 맞는 광고그룹 구하기
		$cre_gp = array();
		foreach($cre_gp_temp as $cgt){
			if($cgt['end_ymd'] == "" || $cgt['end_ymd'] == "0000-00-00 00:00:00"){
				$cre_gp[] = $cgt['cre_gp_no'];
			}else{
				if(date("Y-m-d") > $cgt['end_ymd']){
					//종료일이 지나면 업데이트 완료로 업데이트
					$data = array(
						'cre_gp_status' => "5"
					);
					$this->db->where('cre_gp_no', $cgt['cre_gp_no']);
					$this->db->update('mo_creative_group', $data);
				}else{
					$cre_gp[] = $cgt['cre_gp_no'];
				}
			}
		}

		$cg_list = "";
		$i = count($cre_gp);

		foreach($cre_gp as $key=>$cg){
			if($key+1 == $i){
				$cg_list .= $cg;
			}else{
				$cg_list .= $cg.",";
			}
		}


		//가로길이
		$width = $data['w'];
		$width = str_replace( "\"","", $width );

		//세로길이
		$height = $data['h'];
		$height = str_replace( "\"","", $height );


		//bid_price 최대값 구하기
		$this->db->select("max(mo_creative_group.bid_price) AS bid_price");
		$this->db->where('cre_status', '1'); //승인된 광고만
		$this->db->where('cre_evaluation', '2'); //승인된 광고
		$this->db->where('cre_fl', 'N');
		$this->db->where('cre_width', $width);
		$this->db->where('cre_height', $height);

		$where = "mo_creative_group.cre_gp_status  = '1'";
		$this->db->where($where);

		if($cg_list != ""){
			$where = "mo_creative.cre_gp_no IN (".$cg_list.")";
			$this->db->where($where);
		}else{
			return '{"error" : "data empty" , "message" : "no matching group" }';
		}

		$this->db->from('mo_creative');
		$this->db->join('mo_creative_group', 'mo_creative_group.cre_gp_no = mo_creative.cre_gp_no');
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ) {
			$cre = $query->first_row('array');
		}

		$max_bid_price = $cre['bid_price'];

		//조건에 맞는 광고 구하기
		$this->db->select("*");
		$this->db->where('cre_status', '1');
		//$this->db->where('cam_status', '1');
		$this->db->where('cre_fl', 'N');
		$this->db->where('cre_width', $width);
		$this->db->where('cre_height', $height);
		$this->db->where('cre_evaluation', '2');
		$where = "mo_creative_group.bid_price  = '".$max_bid_price."'";

		$this->db->where($where);

		if($cg_list != ""){
			$where = "mo_creative.cre_gp_no IN (".$cg_list.")";
			$this->db->where($where);
		}else{
			return '{"error" : "data empty", "message" : "no matching group"}';
		}

		$this->db->order_by("cre_no", "random");
		$this->db->limit(1);
		$this->db->from('mo_creative');
		$this->db->join('mo_creative_group', 'mo_creative_group.cre_gp_no = mo_creative.cre_gp_no');
		$this->db->join('mo_campaign', 'mo_creative_group.cam_no = mo_campaign.cam_no');

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){

			$cre = $query->result_array();
			$resdata['zone_no'] = $data['zone_no'];
			$resdata['agency_no'] = $data['agency_no'];
			$resdata['adver_no'] = $cre[0]['adver_no'];
			$resdata['cre_no'] = $cre[0]['cre_no'];
			$resdata['id'] = $data['id'];
			$resdata['impid'] = str_replace( "\"", "", $data['impid'] );
			$resdata['seatbid'] = $data['id'];
			$resdata['seatbid_bid_price'] = $cre[0]['bid_price'];
			$resdata['cur'] = "USD";
			$resdata['seatid_bid_nurl'] = "";
			$resdata['response_obj'] = json_encode($data);
			$resdata['reg_ymd'] = date('Y-m-d H:i:s');

//            $this->db->insert('mo_bid_response', $resdata);

			$this->db->select("mo_members.mem_com_fee");
			$this->db->where('mo_members_group.agency_no', $cre[0]['adver_no']);
			$this->db->from('mo_members');
			$this->db->join('mo_members_group', 'mo_members.mem_no = mo_members_group.adver_no');
			$query = $this->db->get();
			$row = $query->row();
			$fee = $row->mem_com_fee;

			//$cre = $query->result_array();
			$base_url = $this->config->config['base_url'];

			/* 광고 형태 변경 (옵션 분류) - steven */
//            $cre[0]['cre_type'] = 4;
			$_DATA['base_url'] = $base_url;
			$_DATA['cre'] = $cre;
			$_DATA['data'] = $data;
			$_DATA['category'] = join(',',$bcate);
			$_DATA['fee'] = $fee;

//            $cre[0]['cre_type'] =4;
			switch($cre[0]['cre_type'])
			{
				case 4:

					$this->load->library('auction/auctionApi');
					$_request_url_parser = $this->auctionapi->request_domain_auction($data['request_page']);

					$_Qry = sprintf("SELECT bn_code,adver_type FROM mo_agency_code WHERE  site_domain = '%s' and id_no = '%d' and  zone_id = '%d' ",$_request_url_parser['host'],$resdata['cre_no'],$data['zone_id']);

					$_Result = $this->db->query($_Qry);


					if($_Result->num_rows() > 0 )
					{
					$_Row = $_Result->first_row('array');
					$_BNCODE = $_Row['bn_code'];
					$_DATA['BNCODE'] = $_BNCODE;

					$_DATA['auction'] = $this->auctionapi->auction_adver($data['request_page'],$_BNCODE);


					$_load_page = vsprintf('adhtml/auction/auction_%dx%d_%d',
						array(
							$cre[0]['cre_width'],
							$cre[0]['cre_height'],
							1
						)
					);

					$adm = $this->load->view($_load_page,$_DATA,true);

					$adm = ($adm)?$adm:'';
					}else{
						$adm ='';
					}
				break;
				case 1:
				default:

					$adm = $this->load->view('adhtml/adop/image_advertise',$_DATA,true);
			}



			$res_arr['id'] = $data['id'];
			$res_arr['seatbid'][0]['bid'][0]['id'] = $data['id'];
			$res_arr['seatbid'][0]['bid'][0]['impid'] = str_replace( "\"","", $data['impid'] );
			$res_arr['seatbid'][0]['bid'][0]['price'] = $cre[0]['bid_price'];
			$res_arr['seatbid'][0]['bid'][0]['nurl'] = "";
			$res_arr['seatbid'][0]['bid'][0]['adm'] = $adm;
			$res_arr['cur'] = "USD";

			$res = json_encode($res_arr);
			return $res;
		}else{
			return '{"error" : "data empty", "msg": "no data selected.00020"}';
		}
	}


	function insert_bid_response_auction($data){


		$excepted_cre_gp = array();
		//매체의 카테고리 정보
		$bcate = json_decode($data['bcate']);
		//targeting
		//os, browser, device
		$device = json_decode($data['device'], TRUE);
		$user_agent = $device['ua'];

		$this->load->library('auction/auctionApi');
		$_request_url_parser = $this->auctionapi->request_domain_auction($data['request_page']);

        /* 옥션과 올킬 분기 처리 하는 플래그
        $_keyword_search = true; // 옥션 광고
         */
//        $_keyword_search = true;
		### 인벤 옥션 광고 처리
//            $_load_page_path = 'adhtml/auction/auction_inven2';
		//옥션 이미지 + 상품명광고 진행시
//		$_custom_code = 'adhtml/auction/auction_%dx%d_2';
		//옥션 통이미지 광고 진행시
		$_custom_code = 'adhtml/auction/auction_%dx%d_3';

//        $_add_qury = sprintf("and site_domain = '%s'",$_request_url_parser['host']);

		$this->db->cache_on();
		$_qry = sprintf("SELECT * FROM mo_creative as a LEFT JOIN mo_agency_code as b ON a.cre_no = b.id_no WHERE b.status='Y' AND b.zone_id = '%d' ",$data['zone_id']);

		$result = $this->db->query($_qry);

		if($result->num_rows() > 0)
		{
			$row = $result->first_row('array');

			$resdata['zone_no'] = $data['zone_no'];
			$resdata['agency_no'] = $data['agency_no'];
			$resdata['adver_no'] = $row['adver_no'];
			$resdata['cre_no'] = $row['cre_no'];
			$resdata['id'] = $data['id'];
			$resdata['impid'] = str_replace( "\"", "", $data['impid'] );
			$resdata['seatbid'] = $data['id'];
			$resdata['seatbid_bid_price'] = $row['bid_price'];
			$resdata['cur'] = "USD";
			$resdata['seatid_bid_nurl'] = "";
			$resdata['response_obj'] = json_encode($data);
			$resdata['reg_ymd'] = date('Y-m-d H:i:s');
//            $this->db->insert('mo_bid_response', $resdata);

			$this->db->cache_on();
			$this->db->select("mo_members.mem_com_fee");
			$this->db->where('mo_members_group.agency_no', $row['cre_gp_no']);
			$this->db->from('mo_members');
			$this->db->join('mo_members_group', 'mo_members.mem_no = mo_members_group.adver_no');
			$query = $this->db->get();
			$rows = $query->row();
			$fee = $rows->mem_com_fee;

			//$cre = $query->result_array();
			$base_url = $this->config->config['base_url'];

			/* 광고 형태 변경 (옵션 분류) - steven */
//            $cre[0]['cre_type'] = 4;
			$_DATA['base_url'] = $base_url;
			$_DATA['data'] = $resdata;
			$_DATA['category'] = join(',',$bcate);
			$_DATA['fee'] = $fee;
			$_DATA['cre'] = $resdata;
			$_DATA['BNCODE'] = $row['bn_code'];

			$_DATA['req_site'] = $_request_url_parser['host'];

//			원본
//			$_DATA['auction'] = $this->auctionapi->auction_adver_new($data['request_page'],$row['bn_code'],$_keyword_search);

			$_DATA['auction'] = $this->auctionapi->auction_adver_new($data['request_page'],$row['bn_code'],$row['cre_width'],$row['cre_height'],$_keyword_search);

			if ($_DATA['auction'] == 'empty_data') {
				return '{"error" : "data empty", "msg": "no data selected.0010"}';
			}

			$_COLOR['auction'][0] = array('text' => '#f93a5c', 'background' => '#ffdfe4');
			$_COLOR['auction'][1] = array('text' => '#818181', 'background' => '#e0e0e2');
			$_COLOR['auction'][2] = array('text' => '#3d5ec5', 'background' => '#cae5ff');
			$_COLOR['auction'][3] = array('text' => '#0ca392', 'background' => '#cdefd6');
			$_COLOR['auction'][4] = array('text' => '#cc5bf5', 'background' => '#eed6f6');
			$_COLOR['auction'][5] = array('text' => '#fa791e', 'background' => '#ffd3b4');
			$_COLOR['auction'][6] = array('text' => '#26adca', 'background' => '#c3ebeb');
			$_COLOR['auction'][7] = array('text' => '#cea910', 'background' => '#ffefb1');

			$_COLOR['allkill'] = array('text'=>'#e43f2f','background'=>'#f9c542');

			$_AUCTION_IMG['32050'] = array('logo' => 'http://cdn.ads-optima.com/atom/auction/logo_symbol.png', 'auction' => 'http://cdn.ads-optima.com/atom/auction/logo_txt_kr38x11.png', 'allkill' => 'http://cdn.ads-optima.com/atom/auction/logo_allkill47x9.png');
			$_AUCTION_IMG['320100'] = array('logo' => 'http://cdn.ads-optima.com/atom/auction/logo_symbol.png', 'auction' => 'http://cdn.ads-optima.com/atom/auction/logo_txt_kr38x11.png', 'allkill' => 'http://cdn.ads-optima.com/atom/auction/logo_allkill47x9.png');
			$_AUCTION_IMG['200200'] = array('logo' => 'http://cdn.ads-optima.com/atom/auction/logo_symbol79x56.png', 'auction' => 'http://cdn.ads-optima.com/atom/auction/logo_txt_kr.png', 'allkill' => 'http://cdn.ads-optima.com/atom/auction/logo_allkill47x9.png');
			$_AUCTION_IMG['300250'] = array('logo' => 'http://cdn.ads-optima.com/atom/auction/logo_symbol85x61.png', 'auction' => 'http://cdn.ads-optima.com/atom/auction/logo_txt_kr73x22.png', 'allkill' => 'http://cdn.ads-optima.com/atom/auction/logo_allkill101x20.png');

			if(AUCTION_SERVICE=='allkill')
				$_DATA['auction_color'] = $_COLOR['allkill'];
			else
				$_DATA['auction_color'] = $_COLOR[AUCTION_SERVICE][get_rand(0,count($_COLOR[AUCTION_SERVICE])-1,1)[0]];

			$_size_codes = $row['cre_width'].$row['cre_height'];

			$_DATA['auction_images'] = $_AUCTION_IMG[$_size_codes];


			if(empty(array_filter($_DATA['auction'])))
			{
				$adm = '';

			}else{

				if(empty($_custom_code))
					$_custom_code = 'adhtml/auction/auction_%dx%d_%d';


				$_load_page = vsprintf($_custom_code,
						array(
								$row['cre_width'],
								$row['cre_height'],
								1
						)
				);
				if (isset($_load_page_path))
					$_load_page = $_load_page_path;

				$adm = $this->load->view($_load_page,$_DATA,true);

			}

			$res_arr['id'] = $data['id'];
			$res_arr['seatbid'][0]['bid'][0]['id'] = $data['id'];
			$res_arr['seatbid'][0]['bid'][0]['impid'] = str_replace( "\"","", $data['impid'] );
			$res_arr['seatbid'][0]['bid'][0]['price'] = "0.1";
			$res_arr['seatbid'][0]['bid'][0]['nurl'] = "";
			$res_arr['seatbid'][0]['bid'][0]['adm'] = $adm;
			$res_arr['seatbid'][0]['bid'][0]['width'] = $row['cre_width'];
			$res_arr['seatbid'][0]['bid'][0]['height'] = $row['cre_height'];
			$res_arr['cur'] = "USD";

//			if($_SERVER['REMOTE_ADDR'] == '::1') {
//				$res = $adm;
//			}
//			else {
				$res = json_encode($res_arr);
//			}

			return $res;
		}else{
			return '{"error" : "data empty", "msg": "no data selected.0010"}';
		}
	}

	//현재시간
	function time_now(){
		$timestamp = time();
		$day = strtolower(date("D", $timestamp));
		$time = date("G", $timestamp);
		return $day.$time;
	}

	function get_creative($cre_no){
		$this->db->select("*");
		$this->db->where('cre_no', $cre_no);
		$this->db->where('cre_status', '1');
		$this->db->where('cre_fl', 'N');
		$this->db->where('cre_evaluation', '1');
		$this->db->from('mo_creative');
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$cre = $query->result_array();
			return $cre;
		}
	}

	function insert_click_log($data){

		$this->db->insert('mo_click_log', $data);

		if($data["click_fl"] == "N"){
			$this->db->insert('mo_click_log_cal', $data);
		}
	}

	function insert_impression_log($data){
		//어뷰징처리
		//최근것에서 3초이내인것은 안쌓임
		//데이터베이스방식은 아닌거 같음...
		/*
		$this->db->select("*");
		$this->db->where('cre_no', $data['cre_no']);
		$this->db->where('viewer_id', $data['viewer_id']);
		$this->db->where('viewer_ip', $data['viewer_ip']);
		$this->db->where('viewer_ua', $data['viewer_ua']);
		$this->db->from('mo_impression_log_cal');
		$this->db->order_by('imp_dt', 'desc');
		$this->db->limit(1);
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$imp_log_cal = $query->result_array();

			//들어온 데이터에서 3초 초과되는 데이터만 계산로그에 쌓음
			$term = round(strtotime(date('Y-m-d H:i:s')) - strtotime($imp_log_cal[0]['imp_dt']));

			if($term > 3){
				$this->db->insert('mo_impression_log', $data);
				$this->db->insert('mo_impression_log_cal', $data);
			}else{
				//3초미만 데이터는 어뷰징데이터로 간주
				//어뷰징 데이터
				$data["imp_fl"] = "Y";
				$data["term"] = $term;
				$this->db->insert('mo_impression_log', $data);
			}
		}else{
			$this->db->insert('mo_impression_log', $data);
			$this->db->insert('mo_impression_log_cal', $data);
		}
		*/

		$this->db->insert('mo_impression_log', $data);

		if($data["imp_fl"] == "N"){
			$this->db->insert('mo_impression_log_cal', $data);
		}
	}

	/**
	 * 통화별 일별 환율 데이터 insert
	 */
	function insert_exchange($data){
		$sql = $this->db->insert_string('mo_exchange', $data);
		$this->db->query($sql);
		$ret = $this->db->insert_id();
		return $ret;
	}

	function update_exchange($data){

		$data['exchange_rate'] = $this->get_exchange_($data['from_code_key'], $data['to_code_key']);

		$this->db->where('from_code_key', $data['from_code_key']);
		$this->db->where('to_code_key', $data['to_code_key']);
		$this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", date("Y-m-d", strtotime($data['input_dt'])));
		$this->db->update('mo_exchange', $data);

		return true;
	}

	/**
	 * 선택된 일자에 대한 환율 데이터가 있는지 유무
	 */
	function get_exchange($from_code_key, $to_code_key, $input_date){
		$this->db->select("*");
		$this->db->where('from_code_key', $from_code_key);
		$this->db->where('to_code_key', $to_code_key);
		$this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
		$this->db->from('mo_exchange');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*차후 다른통화로 업데이트 */
	function loc_price($input_date , $price){
		$this->db->select("*");
		$this->db->where('from_code_key', "USD");
		$this->db->where('to_code_key', "KRW");
		$this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
		$this->db->from('mo_exchange');
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			foreach ($query->result() as $row){
				$exchange_rate = $row->exchange_rate;
				return $price * $exchange_rate;
			}
		}
	}


	//국제 통화 환율을 업데이트
	// URL/service/get_exchange/USD/KRW
	function get_exchange_($from_currency = null, $to_currency = null){

		if(!isset($from_currency))
			return "";

		if(!isset($to_currency))
			return "";

		$content = file_get_contents('https://www.google.com/finance/converter?a=1&from='.$from_currency.'&to='.$to_currency);
		$doc = new DOMDocument();
		@$doc->loadHTML($content);
		$xpath = new DOMXpath($doc);
		$result = $xpath->query('//*[@id="currency_converter_result"]/span')->item(0)->nodeValue;
		$exchange_rate = str_replace( $to_currency, '', $result);

		return $exchange_rate;
	}

	function update_bid_price($input_date){

		$this->db->select("*");
		$this->db->where("cre_gp_fl", "N");
		$this->db->where("bid_cur !=", "USD");
		$this->db->from('mo_creative_group');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$cre_gp = $query->result_array();
			foreach($cre_gp as $cg){
				$bid_cur = $cg['bid_cur'];
				$this->db->select("exchange_rate");
				$this->db->where("from_code_key", "USD");
				$this->db->where("to_code_key", $bid_cur);
				$this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
				$this->db->from('mo_exchange');
				$this->db->limit(1);
				$query = $this->db->get();
				foreach ($query->result() as $row){
					$exchange_rate = $row->exchange_rate;
				}

				if(!isset($exchange_rate) || $exchange_rate == ""){
					$exchange_rate = $this->get_exchange_("USD", $bid_cur);
				}

				if($cg['bid_loc_price'] > 0) {
					$bid_price = $cg['bid_loc_price'] / $exchange_rate;
				}else{
					$bid_price = 0;
				}

				$data = array(
					'bid_price' => $bid_price
				);

				$this->db->where('cre_gp_no', $cg['cre_gp_no']);
				$this->db->update('mo_creative_group', $data);
			}
		}
	}

	function update_bid_loc_price($currency, $input_date){

		$this->db->select("*");
		$this->db->where("cre_gp_fl", "N");
		$this->db->where("bid_cur", "USD");
		$this->db->from('mo_creative_group');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$cre_gp = $query->result_array();
			foreach($cre_gp as $cg){
				$bid_cur = $currency;
				$this->db->select("exchange_rate");
				$this->db->where("from_code_key", "USD");
				$this->db->where("to_code_key", $bid_cur);
				$this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
				$this->db->from('mo_exchange');
				$this->db->limit(1);
				$query = $this->db->get();
				foreach ($query->result() as $row){
					$exchange_rate = $row->exchange_rate;
				}

				if(!isset($exchange_rate) || $exchange_rate == ""){
					$exchange_rate = $this->get_exchange_("USD", $bid_cur);
				}

				if($cg['bid_price'] > 0) {
					$bid_loc_price = $cg['bid_price'] * $exchange_rate;
				}else{
					$bid_loc_price = 0;
				}

				$data = array(
					'bid_loc_price' => $bid_loc_price
				);

				$this->db->where('cre_gp_no', $cg['cre_gp_no']);
				$this->db->update('mo_creative_group', $data);
			}
		}
	}


	function arrange_data(){

		$imp_no = $this->arrange_sale_info_imp();
		$click_no = $this->arrange_sale_info_click();

		/*타겟팅 OS정보*/
		$this->arrange_report_os($imp_no, $click_no);

		/*타겟팅 BROWSER정보*/
		$this->arrange_report_browser($imp_no, $click_no);

		/*타겟팅 DEVICE정보*/
		$this->arrange_report_device($imp_no, $click_no);

		/*타겟팅 CATEGORY정보*/
		$this->arrange_report_category(1, $imp_no, $click_no);
		$this->arrange_report_category(3, $imp_no, $click_no);
		$this->arrange_report_category(4, $imp_no, $click_no);
		$this->arrange_report_category(5, $imp_no, $click_no);
		$this->arrange_report_category(6, $imp_no, $click_no);
		$this->arrange_report_category(7, $imp_no, $click_no);
		$this->arrange_report_category(8, $imp_no, $click_no);
		$this->arrange_report_category(9, $imp_no, $click_no);
		$this->arrange_report_category(10, $imp_no, $click_no);
		$this->arrange_report_category(12, $imp_no, $click_no);
		$this->arrange_report_category(13, $imp_no, $click_no);
		$this->arrange_report_category(16, $imp_no, $click_no);
		$this->arrange_report_category(17, $imp_no, $click_no);
		$this->arrange_report_category(18, $imp_no, $click_no);
		$this->arrange_report_category(19, $imp_no, $click_no);
		$this->arrange_report_category(20, $imp_no, $click_no);
		$this->arrange_report_category(22, $imp_no, $click_no);
		$this->arrange_report_category(24, $imp_no, $click_no);

		/*타겟팅 TIME정보*/
		$this->arrange_report_time($imp_no, $click_no);

		//sale_info_cal에 쌓여있는 apply_fl이 'N'인 데이터 처리
		$this->arrange_members_cash($imp_no);

		$result['imp_no'] = $imp_no;
		$result['click_no'] = $click_no;
		$result['ok'] = true;

		return $result;
	}

	function arrange_apply_complete($result){

   
		$imp_no = $result['imp_no'];
		$click_no = $result['click_no'];

		//적용 플래그 값 변경
		$data = array(
						'apply_fl' => 'Y'
		);
		if(count($imp_no) > 0){
			$imp_no_ = implode($imp_no, ',');
			$this->db->where("imp_no IN (".$imp_no_.")");
			$this->db->update('mo_impression_log_cal', $data);
		}

		if(count($click_no) > 0){
			$click_no_ = implode($click_no, ',');
			$this->db->where("click_no IN (".$click_no_.")");
			$this->db->update('mo_click_log_cal', $data);
		}


		$result_['ok'] = true;

		return $result_;
	}

	//적용된 데이터를 주기적으로 비워준다.
	//데이터가 많이 쌓이고 처리되는 부분이므로 꼭 비워주어야 함.
	function arrange_data_complete(){
		/* 노출수 impression_log_cal 삭제 */
		//$this->db->truncate('mo_impression_log_cal');
		$this->db->where('apply_fl', 'Y');
		$this->db->delete('mo_impression_log_cal');
		/* 클릭수 click_log_cal 삭제 */
		$this->db->where('apply_fl', 'Y');
		$this->db->delete('mo_click_log_cal');
		//$this->db->truncate('mo_click_log_cal');
		/* sale_info_cal 삭제 */
		$this->db->where('apply_fl', 'Y');
		$this->db->delete('mo_sale_info_cal');
		//$this->db->truncate('mo_sale_info_cal');
	}

	function arrange_sale_info_imp(){

		/* 노출수 업데이트 - 시작 */
		// 적용 안된 데이터만 가지고 처리 (2015-09-02 수정)
		$imp_no = array();

		$this->db->select("*");
		$this->db->from('mo_impression_log_cal');
		$this->db->where('apply_fl', "N");
		$this->db->order_by("imp_dt", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){

			$impression_log_cal = $query->result_array();

			//광고별 노출수
			//광고별 노출비용
			foreach($impression_log_cal as $imp){

				$imp_no[] = $imp['imp_no'];

				$this->db->select("cre_gp_no");
				$this->db->where("cre_no", $imp["cre_no"]);
				$this->db->from('mo_creative');
				$cre_query = $this->db->get();
				$cre_row = $cre_query->row();
				$cre_gp_no = $cre_row->cre_gp_no;

				$this->db->select("cam_no, daily_budget, bid_price, bid_loc_price, bid_cur");
				$this->db->where("cre_gp_no", $cre_gp_no);
				$this->db->from('mo_creative_group');
				$cre_gp_query = $this->db->get();
				$cre_gp_row = $cre_gp_query->row();
				$cam_no = $cre_gp_row->cam_no;
				$cre_gp_daily_budget = $cre_gp_row->daily_budget;
				$bid_price = $cre_gp_row->bid_price;
				$bid_loc_price = $cre_gp_row->bid_loc_price; //지금은 무조건 한국 원화로... 차후 국가별

				$this->db->select("adver_no, daily_budget");
				$this->db->where("cam_no", $cam_no);
				$this->db->from('mo_campaign');
				$cam_query = $this->db->get();
				$cam_row = $cam_query->row();
				$adver_no = $cam_row->adver_no;
				$cam_daily_budget = $cam_row->daily_budget;

				$mem_no = $this->select_agency_no($adver_no);
				$this->db->select("mem_cash, mem_event_cash, email_alarm_c10_fl, mem_email");
				$this->db->where("mem_no", $adver_no);
				$this->db->from('mo_members');

				$mem_query = $this->db->get();
				$mem_row = $mem_query->row();
				$mem_cash = $mem_row->mem_cash;
				$mem_event_cash = $mem_row->mem_event_cash;
				$mem_total_cash = $mem_cash + $mem_event_cash;
				$alarm['email_alarm_c10_fl'] = $mem_row->email_alarm_c10_fl;
				$alarm['mem_email'] = $mem_row->mem_email;

				$this->db->select("*");
				$this->db->where("date_ymd", substr($imp["imp_dt"], 0, 10));
				$this->db->where("cre_no", $imp["cre_no"]);
				$this->db->from('mo_sale_info');
				$query = $this->db->get();

				$mem_com_fee = $imp["fee"];

				if ( $query->num_rows() > 0 ){
					//mo_sale_info 해당 날짜에 광고가 존재하면 수치 업데이트
					$sale_info = $query->result_array();
					foreach($sale_info as $sale){
						//$total_loc_price = $sale["total_loc_price"] + ($bid_loc_price / 1000);
						//$total_price = $sale["total_price"] + ($bid_price / 1000);
						$total_loc_price = $sale["total_loc_price"] + $imp['loc_price'];
						$total_price = $sale["total_price"] + $imp['price'];

						if($imp['loc_price'] >= $mem_total_cash || $mem_total_cash - $imp['loc_price'] < 10000){

							//10000원미만일때
							if($mem_total_cash - $imp['loc_price'] < 10000){
								$this->alarm($adver_no, '5', $alarm);
							}

							//캐쉬부족

							//맴버에 속한 광고그룹및 캠페인 정지
							$this->db->select("*");
							$this->db->where("adver_no", $adver_no);
							$this->db->where("cam_fl", "N");
							$this->db->where("cam_status", "1"); //진행중
							$this->db->from('mo_campaign');
							$query = $this->db->get();
							if ( $query->num_rows() > 0 ){
								$cam = $query->result_array();
								foreach($cam as $c){
									$data = array(
										'cam_status' => 6 //잔액부족
									);
									$this->db->where("cam_no", $c["cam_no"]);
									$this->db->update('mo_campaign', $data);

									//이벤트로그 - 시작
									$data = array(
										'mem_no' => $adver_no,
										'event_type' => "system",
										'event_subject' => "캠페인 잔액부족",
										'event_cont' => "캠페인[".$c["cam_no"]."] 잔액부족",
										'event_dt' => date('Y-m-d H:i:s')
									);
									$sql = $this->db->insert_string("mo_event_log", $data);
									$this->db->query($sql);
									//이벤트로그 - 끝

									$this->db->select("*");
									$this->db->where("cam_no", $c["cam_no"]);
									$this->db->where("cre_gp_fl", "N");
									$this->db->where("cre_gp_status", "1"); //진행중
									$this->db->from('mo_creative_group');
									$query = $this->db->get();
									if ( $query->num_rows() > 0 ){
										$cre_gp = $query->result_array();
										foreach($cre_gp as $cg){
											$data = array(
												'cre_gp_status' => 6 //잔액부족
											);
											$this->db->where("cre_gp_no", $cg["cre_gp_no"]);
											$this->db->update('mo_creative_group', $data);

											//이벤트로그 - 시작
											$data = array(
												'mem_no' => $adver_no,
												'event_type' => "system",
												'event_subject' => "광고그룹 잔액부족",
												'event_cont' => "광고그룹[". $cg["cre_gp_no"]."] 잔액부족",
												'event_dt' => date('Y-m-d H:i:s')
											);
											$sql = $this->db->insert_string("mo_event_log", $data);
											$this->db->query($sql);
											//이벤트로그 - 끝

											$this->db->select("*");
											$this->db->where("cre_gp_no", $cg["cre_gp_no"]);
											$this->db->where("cre_fl", "N");
											$this->db->where("cre_status", "1"); //진행중
											$this->db->from('mo_creative');
											$query = $this->db->get();
											if ( $query->num_rows() > 0 ){
												$cre_arr = $query->result_array();
												foreach($cre_arr as $cre){
													$data = array(
															'cre_status' => 6 //잔액부족
													);
													$this->db->where("cre_no", $cre["cre_no"]);
													$this->db->update('mo_creative', $data);

													//이벤트로그 - 시작
													$data = array(
															'mem_no' => $adver_no,
															'event_type' => "system",
															'event_subject' => "광고 잔액부족",
															'event_cont' => "광고[". $cre["cre_no"]."] 잔액부족",
															'event_dt' => date('Y-m-d H:i:s')
													);
													$sql = $this->db->insert_string("mo_event_log", $data);
													$this->db->query($sql);
													//이벤트로그 - 끝

												}
											}
										}
									}
								}
							}
						}

						$this->db->select("sum(total_loc_price) total_loc_price");
						$this->db->where("date_ymd", substr($imp["imp_dt"], 0, 10));
						$this->db->where("cam_no", $sale['cam_no']);
						$this->db->from('mo_sale_info');
						$this->db->group_by("cam_no");
						$query = $this->db->get();

						if ( $query->num_rows() > 0 ){
							$cam_total_loc_price = $query->result_array();
						}

						//캠페인 예산 남은 것
						$leftover_cam_daily_budget = $cam_daily_budget - $cam_total_loc_price[0]['total_loc_price'];

						//총 예산의 10%
						$cam_daily_budget_10 = $cam_daily_budget/10;

						//남은 캠페인 예산이 총예산의 10% 보다 적게 된다면 알람
						if($leftover_cam_daily_budget < $cam_daily_budget_10 && $cam_daily_budget > 0){
							$this->alarm($adver_no, '2', $alarm);
						}

						if($cam_total_loc_price[0]['total_loc_price'] +  $imp['loc_price'] >= $cam_daily_budget && $cam_daily_budget > 0){
							//캠페인 일예산 초과
							//캠페인 정지
							$data = array(
								'cam_status' => 5 //일예산초과
							);
							$this->db->where("cam_no", $cam_no);
							$this->db->update('mo_campaign', $data);

							//이벤트로그 - 시작
							$data = array(
									'mem_no' => $adver_no,
									'event_type' => "system",
									'event_subject' => "캠페인 일예산초과",
									'event_cont' => "캠페인[". $cam_no."] 일예산초과",
									'event_dt' => date('Y-m-d H:i:s')
							);
							$sql = $this->db->insert_string("mo_event_log", $data);
							$this->db->query($sql);
							//이벤트로그 - 끝

							//캠페인에 속한 광고 그룹정지
							$this->db->select("*");
							$this->db->where("cam_no", $cam_no);
							$this->db->where("cre_gp_fl", "N");
							$this->db->where("cre_gp_status", "1"); //진행중
							$this->db->from('mo_creative_group');
							$query = $this->db->get();
							if ( $query->num_rows() > 0 ){
								$cre_gp = $query->result_array();
								foreach($cre_gp as $cg){
									$data = array(
										'cre_gp_status' => 5 //일예산초과
									);
									$this->db->where("cre_gp_no", $cg["cre_gp_no"]);
									$this->db->update('mo_creative_group', $data);

									//이벤트로그 - 시작
									$data = array(
										'mem_no' => $adver_no,
										'event_type' => "system",
										'event_subject' => "캠페인 일예산초과",
										'event_cont' => "캠페인 일예산초과 광고그룹[". $cg["cre_gp_no"]."] 정지",
										'event_dt' => date('Y-m-d H:i:s')
									);
									$sql = $this->db->insert_string("mo_event_log", $data);
									$this->db->query($sql);
									//이벤트로그 - 끝

									//광고그룹에 속한 광고정지
									$this->db->select("*");
									$this->db->where("cre_gp_no", $cg["cre_gp_no"]);
									$this->db->where("cre_fl", "N");
									$this->db->where("cre_status", "1"); //진행중
									$this->db->from('mo_creative');
									$query = $this->db->get();
									if ( $query->num_rows() > 0 ){
										$cre_arr = $query->result_array();
										foreach($cre_arr as $cre){
											$data = array(
													'cre_status' => 5 //일예산초과
											);
											$this->db->where("cre_no", $cre["cre_no"]);
											$this->db->update('mo_creative', $data);

											//이벤트로그 - 시작
											$data = array(
													'mem_no' => $adver_no,
													'event_type' => "system",
													'event_subject' => "캠페인 일예산초과",
													'event_cont' => "캠페인 일예산초과 광고[". $cre["cre_no"]."] 정지",
													'event_dt' => date('Y-m-d H:i:s')
											);
											$sql = $this->db->insert_string("mo_event_log", $data);
											$this->db->query($sql);
											//이벤트로그 - 끝
										}
									}
								}
							}
							//이벤트로그
						}

						$this->db->select("sum(total_loc_price) total_loc_price");
						$this->db->where("date_ymd", substr($imp["imp_dt"], 0, 10));
						$this->db->where("cre_gp_no", $sale['cre_gp_no']);
						$this->db->from('mo_sale_info');
						$this->db->group_by("cre_gp_no");
						$query = $this->db->get();

						if ( $query->num_rows() > 0 ){
							$cre_gp_total_loc_price = $query->result_array();
						}

						//광고그룹 예산 남은 것 (보류)
						//$leftover_cre_gp_daily_budget = $cre_gp_daily_budget - $cre_gp_total_loc_price[0]['total_loc_price'];

						//총 예산의 10% (보류)
						//$cre_gp_daily_budget_10 = $cre_gp_daily_budget/10;

						//남은 광고그룹 예산이 총예산의 10% 보다 적게 된다면 알람 (보류)
						//if($leftover_cre_gp_daily_budget < $cre_gp_daily_budget_10 && $cre_gp_daily_budget > 0){
						//    $this->alarm($mem_no, '3');
						//}

						if($cre_gp_total_loc_price[0]['total_loc_price'] +  $imp['loc_price'] >= $cre_gp_daily_budget && $cre_gp_daily_budget > 0){
							//광고그룹 일예산 초과
							//광고그룹 정지
							$data = array(
								'cre_gp_status' => 5 //일예산초과
							);
							$this->db->where("cre_gp_no", $cre_gp_no);
							$this->db->update('mo_creative_group', $data);

							//이벤트로그 - 시작
							$data = array(
								'mem_no' => $adver_no,
								'event_type' => "system",
								'event_subject' => "광고그룹 일예산초과",
								'event_cont' => "광고그룹[". $cre_gp_no."] 일예산초과",
								'event_dt' => date('Y-m-d H:i:s')
							);
							$sql = $this->db->insert_string("mo_event_log", $data);
							$this->db->query($sql);
							//이벤트로그 - 끝

							//광고그룹에 속한 광고정지
							$this->db->select("*");
							$this->db->where("cre_gp_no", $cre_gp_no);
							$this->db->where("cre_fl", "N");
							$this->db->where("cre_status", "1"); //진행중
							$this->db->from('mo_creative');
							$query = $this->db->get();
							if ( $query->num_rows() > 0 ){
								$cre_arr = $query->result_array();
								foreach($cre_arr as $cre){
									$data = array(
										'cre_status' => 5 //일예산초과
									);
									$this->db->where("cre_no", $cre["cre_no"]);
									$this->db->update('mo_creative', $data);

									//이벤트로그 - 시작
									$data = array(
											'mem_no' => $adver_no,
											'event_type' => "system",
											'event_subject' => "광고그룹 일예산초과",
											'event_cont' => "광고그룹 일예산초과 광고[". $cre["cre_no"]."] 정지",
											'event_dt' => date('Y-m-d H:i:s')
									);
									$sql = $this->db->insert_string("mo_event_log", $data);
									$this->db->query($sql);
									//이벤트로그 - 끝
								}
							}
						}

						$data = array(
										'imp_cnt' => $sale["imp_cnt"] + 1,
										'mem_com_fee' => $mem_com_fee,
										'total_price' => $total_price,
										'total_loc_price' => $total_loc_price
						);

						$this->db->where("date_ymd", $sale["date_ymd"]);
						$this->db->where("cre_no", $sale["cre_no"]);

						$this->db->update('mo_sale_info', $data);

					}

				}else{

					//mo_sale_info 해당 날짜에 광고가 존재하지 않는다면 추가
					$data = array(
						'date_ymd' => substr($imp["imp_dt"], 0, 10),
						'mem_no' => $adver_no,
						'cam_no' => $cam_no,
						'cre_gp_no' => $cre_gp_no,
						'cre_no' => $imp["cre_no"],
						'mem_cash' => $mem_cash,
						'mem_event_cash' => $mem_event_cash,
						'cre_gp_daily_budget' => $cre_gp_daily_budget,
						'cam_daily_budget' => $cam_daily_budget,
						//'total_price' => $bid_price / 1000,
						//'total_loc_price' => $bid_loc_price /1000,
						'total_price' => $imp['price'],
						'total_loc_price' => $imp['loc_price'],
						'mem_com_fee' => $mem_com_fee,
						'imp_cnt' => 1
					);

					$sql = $this->db->insert_string("mo_sale_info", $data);
					$this->db->query($sql);

				}

				// sale_info_cal 처리 시작
				$this->db->select("*");
				$this->db->where("date_ymd", substr($imp["imp_dt"], 0, 10));
				$this->db->where("cre_no", $imp["cre_no"]);
				$this->db->from('mo_sale_info_cal');
				$query = $this->db->get();

				if ( $query->num_rows() > 0 ){
					//mo_sale_info_cal 해당 날짜에 광고가 존재하면 수치 업데이트
					$sale_info_cal = $query->result_array();
					foreach($sale_info_cal as $sale){
						//$total_loc_price = $sale["total_loc_price"] + ($bid_loc_price / 1000);
						//$total_price = $sale["total_price"] + ($bid_price / 1000);

						$total_loc_price = $sale["total_loc_price"] + $imp['loc_price'];
						$total_price = $sale["total_price"] + $imp['price'];

						$data = array(
										'imp_cnt' => $sale["imp_cnt"] + 1,
										'mem_com_fee' => $mem_com_fee,
										'total_price' => $total_price,
										'total_loc_price' => $total_loc_price
						);

						$this->db->where("date_ymd", $sale["date_ymd"]);
						$this->db->where("cre_no", $sale["cre_no"]);

						$this->db->update('mo_sale_info_cal', $data);

					}
				}else{
					$data = array(
									'date_ymd' => substr($imp["imp_dt"], 0, 10),
									'mem_no' => $adver_no,
									'cam_no' => $cam_no,
									'cre_gp_no' => $cre_gp_no,
									'cre_no' => $imp["cre_no"],
									'mem_cash' => $mem_cash,
									'mem_event_cash' => $mem_event_cash,
									'cre_gp_daily_budget' => $cre_gp_daily_budget,
									'cam_daily_budget' => $cam_daily_budget,
									//'total_price' => $bid_price / 1000,
									//'total_loc_price' => $bid_loc_price /1000,
									'total_price' => $imp['price'],
									'total_loc_price' => $imp['loc_price'],
									'mem_com_fee' => $mem_com_fee,
									'imp_cnt' => 1
					);

					$sql = $this->db->insert_string("mo_sale_info_cal", $data);
					$this->db->query($sql);

				}
				// sale_info_cal 처리 끝
			}
		}
		/* 노출수 업데이트 - 끝 */

		//처리된 아이디값
		return $imp_no;
	}

	function arrange_sale_info_click(){
		/* 클릭수 업데이트 - 시작 */
		// 적용 안된 데이터만 가지고 처리 (2015-09-02 수정)
		$click_no = array();

		$this->db->select("*");
		$this->db->from('mo_click_log_cal');
		$this->db->where('apply_fl', 'N');
		$this->db->order_by("click_dt", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){

			$click_log_cal = $query->result_array();

			foreach($click_log_cal as $click){

				$click_no[] = $click['click_no'];

				$this->db->select("*");
				$this->db->where("date_ymd", substr($click["click_dt"], 0, 10));
				$this->db->where("cre_no", $click["cre_no"]);
				$this->db->from('mo_sale_info');
				$query = $this->db->get();
				if ( $query->num_rows() > 0 ){
					//mo_sale_info 해당 날짜에 광고가 존재하면 수치 업데이트
					$sale_info = $query->result_array();
					foreach($sale_info as $sale){
						$data = array(
							'click_cnt' => $sale["click_cnt"] + 1
						);

						$this->db->where("date_ymd", $sale["date_ymd"]);
						$this->db->where("cre_no", $sale["cre_no"]);
						$this->db->update('mo_sale_info', $data);

						$data = array(
							'click_cnt' => $sale["click_cnt"] + 1
						);

						$this->db->where("date_ymd", $sale["date_ymd"]);
						$this->db->where("cre_no", $sale["cre_no"]);
						$this->db->update('mo_sale_info_cal', $data);
					}
				}else{
					//mo_sale_info 해당 날짜에 광고가 존재하지 않는다면 추가
					$cre_sql = "SELECT cre_gp_no FROM mo_creative WHERE cre_no = '".$click["cre_no"]."'";
					$cre_query = $this->db->query($cre_sql);
					$cre_row = $cre_query->row();
					$cre_gp_no = $cre_row->cre_gp_no;

					$cre_gp_sql = "SELECT cam_no, daily_budget FROM mo_creative_group WHERE cre_gp_no = '".$cre_gp_no."'";
					$cre_gp_query = $this->db->query($cre_gp_sql);
					$cre_gp_row = $cre_gp_query->row();
					$cam_no = $cre_gp_row->cam_no;
					$cre_gp_daily_budget = $cre_gp_row->daily_budget;

					$cam_sql = "SELECT adver_no, daily_budget FROM mo_campaign WHERE cam_no = '".$cam_no."'";
					$cam_query = $this->db->query($cam_sql);
					$cam_row = $cam_query->row();
					$adver_no = $cam_row->adver_no;
					$cam_daily_budget = $cam_row->daily_budget;

					$mem_no = $this->select_agency_no($adver_no);

					$mem_sql = "SELECT mem_cash, mem_event_cash FROM mo_members WHERE mem_no = '".$mem_no."'";
					$mem_query = $this->db->query($mem_sql);
					$mem_row = $mem_query->row();
					$mem_cash = $mem_row->mem_cash;
					$mem_event_cash = $mem_row->mem_event_cash;

					//mo_sale_info 해당 날짜에 광고가 존재하지 않는다면 추가
					$data = array(
						'date_ymd' => substr($click["click_dt"], 0, 10),
						'mem_no' => $mem_no,
						'cam_no' => $cam_no,
						'cre_gp_no' => $cre_gp_no,
						'cre_no' => $click["cre_no"],
						'mem_cash' => $mem_cash,
						'mem_event_cash' => $mem_event_cash,
						'cre_gp_daily_budget' => $cre_gp_daily_budget,
						'cam_daily_budget' => $cam_daily_budget,
						'click_cnt' => 1
					);
					$sql = $this->db->insert_string("mo_sale_info", $data);
					$this->db->query($sql);

					$data = array(
						'date_ymd' => substr($click["click_dt"], 0, 10),
						'mem_no' => $mem_no,
						'cam_no' => $cam_no,
						'cre_gp_no' => $cre_gp_no,
						'cre_no' => $click["cre_no"],
						'mem_cash' => $mem_cash,
						'mem_event_cash' => $mem_event_cash,
						'cre_gp_daily_budget' => $cre_gp_daily_budget,
						'cam_daily_budget' => $cam_daily_budget,
						'click_cnt' => 1
					);
					$sql = $this->db->insert_string("mo_sale_info_cal", $data);
					$this->db->query($sql);


				}
			}
		}
		/* 클릭수 업데이트 - 끝 */

		return $click_no;

	}

	function arrange_members_cash(){

		/* 노출수 업데이트 - 시작 */
		$this->db->select("sale_no, date_ymd, mem_no, total_loc_price, total_price");
		$this->db->from('mo_sale_info_cal');
		$this->db->where('apply_fl', 'N');

		//$this->db->group_by('mem_no');
		$query = $this->db->get();

		$mem_cash_a = 0;
		$mem_event_cash_a = 0;

		$sale_no = array();
		if ( $query->num_rows() > 0 ){
			$sale_info_cal = $query->result_array();

			foreach($sale_info_cal as $sale){

				$sale_no[] = $sale['sale_no'];

				$mem_sql = "SELECT * FROM mo_members WHERE mem_no = '".$sale['mem_no']."'";
				$mem_query = $this->db->query($mem_sql);
				$mem_row = $mem_query->row();
				$mem_cash = $mem_row->mem_cash;
				$mem_event_cash = $mem_row->mem_event_cash;
				$mem_pay_later = $mem_row->mem_pay_later;
				$mem_group_no = $mem_row->group_no;
				$mem_group_nm = $mem_row->group_nm;
				$mem_group_role = $mem_row->role;
				$mem_com_nm = $mem_row->mem_com_nm;
				$agency_no = $mem_row->agency_no;
				$mem_id = $mem_row->mem_id;
				$mem_type = $mem_row->mem_type;

				//수수료
				$mem_com_fee = $mem_row->mem_com_fee;
				$fee_fl = $mem_row->fee_fl;
				//사용캐쉬
				$mem_total_loc_price = $sale['total_loc_price'];
				$mem_total_price = $sale['total_price'];

				$fee_fl = $mem_row->fee_fl;
				$alarm['email_alarm_c0_fl'] = $mem_row->email_alarm_c0_fl;
				$alarm['email_alarm_d10_fl'] = $mem_row->email_alarm_d10_fl;
				$alarm['mem_email'] = $mem_row->mem_email;

				//차감된 캐쉬 정보 - 시작
				if($mem_cash >= $sale['total_loc_price']){

					$mem_cash_a = $mem_cash - $sale['total_loc_price'];

					if($fee_fl == "Y"){
						//대행사/랩
						if($mem_cash_a + $mem_event_cash < 100000){
							$this->alarm($sale['mem_no'], '4', $alarm);
						}
					}else{
						//광고주
						if($mem_cash_a + $mem_event_cash < 10000){
							$this->alarm($sale['mem_no'], '3', $alarm);
						}
					}

					$mem_event_cash_a = $mem_event_cash;
					$left_cash = 0;
				}else{
					//캐쉬부족으로 인한 이벤트캐쉬 소진


					//여기서 이벤트 캐쉬 소진
					if(($mem_cash + $mem_event_cash) >= $sale['total_loc_price']){

						//실 캐쉬 차감후 남은 잔액 ( = 이벤트 캐쉬 차감 금액 )
						$left_cash = $sale['total_loc_price'] - $mem_cash;

						$mem_event_cash_a = $mem_event_cash - $left_cash;

						$mem_cash_a = 0;
						//$mem_cash_a = $mem_cash - $sale['total_loc_price'];

						if($fee_fl == "Y"){
							//대행사/랩
							if($mem_cash_a + $mem_event_cash < 100000){
								$this->alarm($sale['mem_no'], '4', $alarm);
							}
						}else{
							//광고주
							if($mem_cash_a + $mem_event_cash < 10000){
								$this->alarm($sale['mem_no'], '3', $alarm);
							}
						}

					}else{

						//캐쉬 소진 알람 시작
						$this->alarm($sale['mem_no'], '1', $alarm);
						//캐쉬 소진 알람 끝

						// 이벤트 캐쉬 차감 금액
						$left_cash = $mem_event_cash;
						$mem_cash_a = 0;
						//$mem_cash_a = $mem_cash - $sale['total_loc_price'];

						$mem_event_cash_a = 0;

						if($fee_fl == "Y"){
							//대행사/랩
							if($mem_cash_a + $mem_event_cash < 100000){
								$this->alarm($sale['mem_no'], '4', $alarm);
							}
						}else{
							//광고주
							if($mem_cash_a + $mem_event_cash < 10000){
								$this->alarm($sale['mem_no'], '3', $alarm);
							}
						}
					}
				}
				//차감된 캐쉬 정보 - 끝


				//정산 테이블
				$this->db->select("*");
				//$this->db->from('mo_revenue_price');
				$this->db->from('mo_revenue');
				$this->db->where("mem_no", $sale['mem_no']);
				$this->db->where("date_ymd", date('Y-m-d'));
				$query = $this->db->get();

				if ($query->num_rows() > 0) {
					$revenue_price = $query->result_array();

					foreach($revenue_price as $rp){
						$price = $rp['price'];
						$loc_price = $rp['loc_price'];
						$loc_event_price = $rp['loc_event_price'];
						$price_lt = $rp['price_lt'];
						$loc_price_lt = $rp['loc_price_lt'];
						$fee_price = $rp['fee_price'];
						$fee_price_lt = $rp['fee_price_lt'];
						$loc_fee_price = $rp['loc_fee_price'];
						$mem_group_no = $rp['group_no'];
						$loc_fee_price_lt = $rp['loc_fee_price_lt'];
					}

					if($mem_pay_later == "Y"){
						$data = array(
										'loc_price_lt' => $loc_price_lt + $sale['total_loc_price'] - $left_cash,
										'price_lt' => $price_lt + $sale['total_price'],
										'loc_event_price' => $loc_event_price + $left_cash
						);

						if($fee_fl == "Y"){
							$data['fee_price_lt'] = $fee_price_lt + (($sale['total_price'] - $left_cash) * $mem_com_fee / 100);
							$data['loc_fee_price_lt'] = $loc_fee_price_lt + (($sale['total_loc_price'] - $left_cash) * $mem_com_fee / 100);
						}

						$data['agency_no'] = $agency_no;
						$data['mem_id'] = $mem_id;
						$data['mem_type'] = $mem_type;
						$data['group_no'] = $mem_group_no;
						$data['group_nm'] = $mem_group_nm;
						$data['group_role'] = $mem_group_role;
						$data['mem_pay_later'] = $mem_pay_later;
						$data['mem_com_nm'] = $mem_com_nm;

					}else{
						$data = array(
										'loc_price' => $loc_price + $sale['total_loc_price'] - $left_cash,
										'price' => $price + $sale['total_price'],
										'loc_event_price' => $loc_event_price + $left_cash
						);

						if($fee_fl == "Y"){
							$data['fee_price'] = $fee_price + (($sale['total_price'] - $left_cash) * $mem_com_fee / 100);
							$data['loc_fee_price'] = $loc_fee_price + (($sale['total_loc_price'] - $left_cash) * $mem_com_fee / 100);
						}
						$data['agency_no'] = $agency_no;
						$data['mem_id'] = $mem_id;
						$data['mem_type'] = $mem_type;
						$data['group_no'] = $mem_group_no;
						$data['group_nm'] = $mem_group_nm;
						$data['group_role'] = $mem_group_role;
						$data['mem_pay_later'] = $mem_pay_later;
						$data['mem_com_nm'] = $mem_com_nm;
					}

					$this->db->where('mem_no', $sale['mem_no']);
					$this->db->where('date_ymd', date('Y-m-d'));
					//$this->db->update('mo_revenue_price', $data);
					$this->db->update('mo_revenue', $data);

				}else{
					if($mem_pay_later == "Y"){
						$data = array(
										'mem_no' => $sale['mem_no'],
										'date_ymd' => date("Y-m-d"),
										'loc_price_lt' => $sale['total_loc_price'] - $left_cash,
										'price_lt' => $sale['total_price'],
										'loc_event_price' => $left_cash
						);

						if($fee_fl == "Y"){
							$data['fee_price_lt'] = ($sale['total_price'] - $left_cash) * $mem_com_fee / 100;
							$data['loc_fee_price_lt'] = ($sale['total_loc_price'] - $left_cash) * $mem_com_fee / 100;
						}
						$data['agency_no'] = $agency_no;
						$data['mem_id'] = $mem_id;
						$data['mem_type'] = $mem_type;
						$data['group_no'] = $mem_group_no;
						$data['group_nm'] = $mem_group_nm;
						$data['group_role'] = $mem_group_role;
						$data['mem_pay_later'] = $mem_pay_later;
						$data['mem_com_nm'] = $mem_com_nm;

					}else{
						$data = array(
										'mem_no' => $sale['mem_no'],
										'date_ymd' => date("Y-m-d"),
										'loc_price' => $sale['total_loc_price'] - $left_cash,
										'price' => $sale['total_price'],
										'loc_event_price' => $left_cash
						);

						if($fee_fl == "Y"){
							$data['fee_price'] = ($sale['total_price'] - $left_cash) * $mem_com_fee / 100;
							$data['loc_fee_price'] = ($sale['total_loc_price'] - $left_cash) * $mem_com_fee / 100;
						}
						$data['agency_no'] = $agency_no;
						$data['mem_id'] = $mem_id;
						$data['mem_type'] = $mem_type;
						$data['group_no'] = $mem_group_no;
						$data['group_nm'] = $mem_group_nm;
						$data['group_role'] = $mem_group_role;
						$data['mem_pay_later'] = $mem_pay_later;
						$data['mem_com_nm'] = $mem_com_nm;
					}
					//$query = $this->db->insert_string('mo_revenue_price', $data);
					$query = $this->db->insert_string('mo_revenue', $data);
					$this->db->query($query);
				}
				//정산 테이블 - 끝




				$data = array(
					'mem_cash' => $mem_cash_a,
					'mem_event_cash' => $mem_event_cash_a,
				);

				$this->db->where("mem_no", $sale['mem_no']);
				$this->db->update('mo_members', $data);

				//캐쉬 사용 내역 남기기 -- 시작 --
				$this->db->select("*");
				$this->db->from('mo_cash_history');
				$this->db->where("mem_no", $sale['mem_no']);
				$this->db->where("cash_dt", $sale['date_ymd']);
				$query = $this->db->get();
				//해당 날짜에 내역이 있다면 업데이트
				if ( $query->num_rows() > 0 ){
					$cash_history = $query->result_array();
					foreach($cash_history as $ch){
						if($ch['cash_type'] == "C"){
							$data = array(
								'used_cash' => $ch['used_cash'] + ($mem_cash - $mem_cash_a)
							);
							$this->db->where("cash_no", $ch['cash_no']);
							$this->db->update('mo_cash_history', $data);
						}elseif($ch['cash_type'] == "E"){
							$data = array(
								'used_cash' => $ch['used_cash'] + ($mem_event_cash - $mem_event_cash_a)
							);
							$this->db->where("cash_no", $ch['cash_no']);
							$this->db->update('mo_cash_history', $data);
						}
					}
				}else{
					$data = array(
						'used_cash' => $mem_cash - $mem_cash_a,
						'mem_no' => $sale['mem_no'],
						'cash_type' => 'C',
						'cash_dt' => $sale['date_ymd']
					);
					$sql = $this->db->insert_string("mo_cash_history", $data);
					$this->db->query($sql);

					$data = array(
							'used_cash' => $mem_event_cash - $mem_event_cash_a,
							'mem_no' => $sale['mem_no'],
							'cash_type' => 'E',
							'cash_dt' => $sale['date_ymd']
					);
					$sql = $this->db->insert_string("mo_cash_history", $data);
					$this->db->query($sql);
				}
				//캐쉬 사용 내역 남기기 -- 끝 --

				$data = array(
					'mem_no' => $sale['mem_no'],
					'mem_cash_b' => $mem_cash,
					'mem_event_cash_b' => $mem_event_cash,
					'mem_cash_a' => $mem_cash_a,
					'mem_event_cash_a' => $mem_event_cash_a,
					'bid_loc_price' => $sale['total_loc_price'],
					'cash_log_dt' => date('Y-m-d H:i:s')
				);
				$sql = $this->db->insert_string("mo_members_cash_log", $data);
				$this->db->query($sql);


			}
		}

		if(count($sale_no) > 0){
			$sale_no_ = implode($sale_no, ",");
			//적용 플래그 값 변경
			$data = array(
							'apply_fl' => 'Y'
			);

			$this->db->where("sale_no IN (".$sale_no_.")");
			$this->db->update('mo_sale_info_cal', $data);
		}

	}

	//타겟팅 os 리포팅
	function arrange_report_os($imp_no, $click_no){

		if(count($imp_no) > 0){
			$imp_no = implode($imp_no, ",");

			//OS별로 노출수 카운트
			$sql = "SELECT SUBSTR(imp_dt, 1, 10) imp_dt, os, cre_no, COUNT(*) imp_cnt, SUM(price) total_price, SUM(loc_price) total_loc_price";
			$sql .= " FROM mo_impression_log_cal";
			$sql .= " WHERE imp_no IN (".$imp_no.")";
			$sql .= " GROUP BY SUBSTR(imp_dt, 1, 10), os, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$imp_info = $query->result_array();
				foreach($imp_info as $imp){
					$this->db->select("*");
					$this->db->from('mo_report_os');
					$this->db->where("date_ymd", $imp['imp_dt']);
					$this->db->where("os", $imp['os']);
					$this->db->where("cre_no", $imp['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$imp_cnt = $row->imp_cnt + $imp['imp_cnt'];
						$click_cnt = $row->click_cnt;
						$loc_price = $row->loc_price + $imp['total_loc_price'];
						$price = $row->price + $imp['total_price'];
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;

						if($click_cnt > 0){
							$loc_ppc = $loc_price / $click_cnt;
							$ppc = $price / $click_cnt;
						}else{
							$loc_ppc = 0;
							$ppc = 0;
						}

						$data = array(
							'imp_cnt' => $imp_cnt,
							'click_cnt' => $click_cnt,
							'price' => $price,
							'loc_price' => $loc_price,
							'ctr' => $ctr,
							'loc_ppi' => $loc_ppi,
							'ppi' => $ppi,
							'loc_ppc' => $loc_ppc,
							'ppc' => $ppc
						);
						$this->db->where("date_ymd", $imp['imp_dt']);
						$this->db->where("os", $imp['os']);
						$this->db->where("cre_no", $imp['cre_no']);
						$this->db->update('mo_report_os', $data);
						//print_r($this->db->last_query());
						//exit;
					//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						$data = array(
							'date_ymd' => $imp['imp_dt'],
							'cre_no' => $imp['cre_no'],
							'os' => $imp['os'],
							'imp_cnt' => $imp['imp_cnt'],
							'click_cnt' => 0,
							'price' => $imp['total_price'],
							'loc_price' => $imp['total_loc_price'],
							'ctr' => 0,
							'loc_ppi' => $imp['total_loc_price'] / $imp['imp_cnt'],
							'ppi' => $imp['total_price'] / $imp['imp_cnt'],
							'loc_ppc' => 0,
							'ppc' => 0
						);
						$sql = $this->db->insert_string("mo_report_os", $data);
						$this->db->query($sql);
						//print_r($this->db->last_query());
						//exit;
					}
				}
			}
		}

		//OS별로 클릭수 카운트
		if(count($click_no) > 0){
			$click_no = implode($click_no, ",");

			$sql = "SELECT SUBSTR(click_dt, 1, 10) click_dt, cre_no, os, COUNT(*) click_cnt";
			$sql .= " FROM mo_click_log_cal";
			$sql .= " WHERE click_no IN (".$click_no.")";
			$sql .= " GROUP BY SUBSTR(click_dt, 1, 10), os, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$click_info = $query->result_array();
				foreach($click_info as $click){
					$this->db->select("*");
					$this->db->from('mo_report_os');
					$this->db->where("date_ymd", $click['click_dt']);
					$this->db->where("os", $click['os']);
					$this->db->where("cre_no", $click['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$click_cnt = $row->click_cnt + $click['click_cnt'];
						$imp_cnt = $row->imp_cnt;
						$loc_price = $row->loc_price;
						$price = $row->price;
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;
						$loc_ppc = $loc_price / $click_cnt;
						$ppc = $price / $click_cnt;
						$data = array(
							'imp_cnt' => $imp_cnt,
							'click_cnt' => $click_cnt,
							'ctr' => $ctr,
							'loc_ppi' => $loc_ppi,
							'ppi' => $ppi,
							'loc_ppc' => $loc_ppc,
							'ppc' => $ppc
						);
						$this->db->where("date_ymd", $click['click_dt']);
						$this->db->where("os", $click['os']);
						$this->db->where("cre_no", $click['cre_no']);
						$this->db->update('mo_report_os', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						//노출없이 클릭이 없음
					}
				}
			}
		}
	}

	//타겟팅 browser 리포팅
	function arrange_report_browser($imp_no, $click_no){

		if(count($imp_no) > 0){
			$imp_no = implode($imp_no, ",");
			//BROWSER별로 노출수 카운트
			$sql = "SELECT SUBSTR(imp_dt, 1, 10) imp_dt, browser, cre_no, COUNT(*) imp_cnt, SUM(price) total_price, SUM(loc_price) total_loc_price";
			$sql .= " FROM mo_impression_log_cal";
			$sql .= " WHERE imp_no IN (".$imp_no.")";
			$sql .= " GROUP BY SUBSTR(imp_dt, 1, 10), browser, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$imp_info = $query->result_array();
				foreach($imp_info as $imp){
					$this->db->select("*");
					$this->db->from('mo_report_browser');
					$this->db->where("date_ymd", $imp['imp_dt']);
					$this->db->where("browser", $imp['browser']);
					$this->db->where("cre_no", $imp['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$imp_cnt = $row->imp_cnt + $imp['imp_cnt'];
						$click_cnt = $row->click_cnt;
						$loc_price = $row->loc_price + $imp['total_loc_price'];
						$price = $row->price + $imp['total_price'];
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;

						if($click_cnt > 0){
							$loc_ppc = $loc_price / $click_cnt;
							$ppc = $price / $click_cnt;
						}else{
							$loc_ppc = 0;
							$ppc = 0;
						}

						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'price' => $price,
										'loc_price' => $loc_price,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $imp['imp_dt']);
						$this->db->where("browser", $imp['browser']);
						$this->db->where("cre_no", $imp['cre_no']);
						$this->db->update('mo_report_browser', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						$data = array(
										'date_ymd' => $imp['imp_dt'],
										'cre_no' => $imp['cre_no'],
										'browser' => $imp['browser'],
										'imp_cnt' => $imp['imp_cnt'],
										'click_cnt' => 0,
										'price' => $imp['total_price'],
										'loc_price' => $imp['total_loc_price'],
										'ctr' => 0,
										'loc_ppi' => $imp['total_loc_price'] / $imp['imp_cnt'],
										'ppi' => $imp['total_price'] / $imp['imp_cnt'],
										'loc_ppc' => 0,
										'ppc' => 0
						);
						$sql = $this->db->insert_string("mo_report_browser", $data);
						$this->db->query($sql);
					}
				}
			}
		}

		//BROWSER별로 클릭수 카운트
		if(count($click_no) > 0){
			$click_no = implode($click_no, ",");
			$sql = "SELECT SUBSTR(click_dt, 1, 10) click_dt, cre_no, browser, COUNT(*) click_cnt";
			$sql .= " FROM mo_click_log_cal";
			$sql .= " WHERE click_no IN (".$click_no.")";
			$sql .= " GROUP BY SUBSTR(click_dt, 1, 10), browser, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$click_info = $query->result_array();
				foreach($click_info as $click){
					$this->db->select("*");
					$this->db->from('mo_report_browser');
					$this->db->where("date_ymd", $click['click_dt']);
					$this->db->where("browser", $click['browser']);
					$this->db->where("cre_no", $click['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$click_cnt = $row->click_cnt + $click['click_cnt'];
						$imp_cnt = $row->imp_cnt;
						$loc_price = $row->loc_price;
						$price = $row->price;
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;
						$loc_ppc = $loc_price / $click_cnt;
						$ppc = $price / $click_cnt;
						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $click['click_dt']);
						$this->db->where("browser", $click['browser']);
						$this->db->where("cre_no", $click['cre_no']);
						$this->db->update('mo_report_browser', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						//노출없이 클릭이 없음
					}
				}
			}
		}
	}

	//타겟팅 device 리포팅
	function arrange_report_device($imp_no, $click_no){
		//DEVICE별로 노출수 카운트
		if(count($imp_no) > 0){
			$imp_no = implode($imp_no, ",");
			$sql = "SELECT SUBSTR(imp_dt, 1, 10) imp_dt, device, cre_no, COUNT(*) imp_cnt, SUM(price) total_price, SUM(loc_price) total_loc_price";
			$sql .= " FROM mo_impression_log_cal";
			$sql .= " WHERE imp_no IN (".$imp_no.")";
			$sql .= " GROUP BY SUBSTR(imp_dt, 1, 10), device, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$imp_info = $query->result_array();
				foreach($imp_info as $imp){
					$this->db->select("*");
					$this->db->from('mo_report_device');
					$this->db->where("date_ymd", $imp['imp_dt']);
					$this->db->where("device", $imp['device']);
					$this->db->where("cre_no", $imp['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$imp_cnt = $row->imp_cnt + $imp['imp_cnt'];
						$click_cnt = $row->click_cnt;
						$loc_price = $row->loc_price + $imp['total_loc_price'];
						$price = $row->price + $imp['total_price'];
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;

						if($click_cnt > 0){
							$loc_ppc = $loc_price / $click_cnt;
							$ppc = $price / $click_cnt;
						}else{
							$loc_ppc = 0;
							$ppc = 0;
						}

						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'price' => $price,
										'loc_price' => $loc_price,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $imp['imp_dt']);
						$this->db->where("device", $imp['device']);
						$this->db->where("cre_no", $imp['cre_no']);
						$this->db->update('mo_report_device', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						$data = array(
										'date_ymd' => $imp['imp_dt'],
										'cre_no' => $imp['cre_no'],
										'device' => $imp['device'],
										'imp_cnt' => $imp['imp_cnt'],
										'click_cnt' => 0,
										'price' => $imp['total_price'],
										'loc_price' => $imp['total_loc_price'],
										'ctr' => 0,
										'loc_ppi' => $imp['total_loc_price'] / $imp['imp_cnt'],
										'ppi' => $imp['total_price'] / $imp['imp_cnt'],
										'loc_ppc' => 0,
										'ppc' => 0
						);
						$sql = $this->db->insert_string("mo_report_device", $data);
						$this->db->query($sql);
					}
				}
			}
		}

		//DEVICE별로 클릭수 카운트
		if(count($click_no) > 0){
			$click_no = implode($click_no, ",");
			$sql = "SELECT SUBSTR(click_dt, 1, 10) click_dt, cre_no, device, COUNT(*) click_cnt";
			$sql .= " FROM mo_click_log_cal";
			$sql .= " WHERE click_no IN (".$click_no.")";
			$sql .= " GROUP BY SUBSTR(click_dt, 1, 10), device, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$click_info = $query->result_array();
				foreach($click_info as $click){
					$this->db->select("*");
					$this->db->from('mo_report_device');
					$this->db->where("date_ymd", $click['click_dt']);
					$this->db->where("device", $click['device']);
					$this->db->where("cre_no", $click['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$click_cnt = $row->click_cnt + $click['click_cnt'];
						$imp_cnt = $row->imp_cnt;
						$loc_price = $row->loc_price;
						$price = $row->price;
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;
						$loc_ppc = $loc_price / $click_cnt;
						$ppc = $price / $click_cnt;
						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $click['click_dt']);
						$this->db->where("device", $click['device']);
						$this->db->where("cre_no", $click['cre_no']);
						$this->db->update('mo_report_device', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						//노출없이 클릭이 없음
					}
				}
			}
		}
	}


	//타겟팅 catogory 리포팅
	function arrange_report_category($i, $imp_no, $click_no){
		//Category별로 노출수 카운트 (IAB1)
		if(count($imp_no) > 0){
			$imp_no = implode($imp_no, ",");
			$sql = "SELECT SUBSTR(imp_dt, 1, 10) imp_dt, cre_no, COUNT(*) imp_cnt, SUM(price) total_price, SUM(loc_price) total_loc_price";
			$sql .= " FROM mo_impression_log_cal WHERE IAB".$i." = 'Y'";
			$sql .= " AND imp_no IN (".$imp_no.")";
			$sql .= " GROUP BY SUBSTR(imp_dt, 1, 10), IAB".$i.", cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$imp_info = $query->result_array();
				foreach($imp_info as $imp){
					$this->db->select("*");
					$this->db->from('mo_report_category');
					$this->db->where("date_ymd", $imp['imp_dt']);
					$this->db->where("category", "IAB".$i);
					$this->db->where("cre_no", $imp['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$imp_cnt = $row->imp_cnt + $imp['imp_cnt'];
						$click_cnt = $row->click_cnt;
						$loc_price = $row->loc_price + $imp['total_loc_price'];
						$price = $row->price + $imp['total_price'];
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;

						if($click_cnt > 0){
							$loc_ppc = $loc_price / $click_cnt;
							$ppc = $price / $click_cnt;
						}else{
							$loc_ppc = 0;
							$ppc = 0;
						}

						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'price' => $price,
										'loc_price' => $loc_price,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $imp['imp_dt']);
						$this->db->where("category", "IAB".$i);
						$this->db->where("cre_no", $imp['cre_no']);
						$this->db->update('mo_report_category', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						$data = array(
										'date_ymd' => $imp['imp_dt'],
										'cre_no' => $imp['cre_no'],
										'category' => "IAB".$i,
										'imp_cnt' => $imp['imp_cnt'],
										'click_cnt' => 0,
										'price' => $imp['total_price'],
										'loc_price' => $imp['total_loc_price'],
										'ctr' => 0,
										'loc_ppi' => $imp['total_loc_price'] / $imp['imp_cnt'],
										'ppi' => $imp['total_price'] / $imp['imp_cnt'],
										'loc_ppc' => 0,
										'ppc' => 0
						);
						$sql = $this->db->insert_string("mo_report_category", $data);
						$this->db->query($sql);
					}
				}
			}
		}

		//DEVICE별로 클릭수 카운트
		if(count($click_no) > 0){
			$click_no = implode($click_no, ",");
			$sql = "SELECT SUBSTR(click_dt, 1, 10) click_dt, cre_no, COUNT(*) click_cnt";
			$sql .= " FROM mo_click_log_cal WHERE IAB".$i." = 'Y'";
			$sql .= " AND click_no IN (".$click_no.")";
			$sql .= " GROUP BY SUBSTR(click_dt, 1, 10), IAB".$i.", cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$click_info = $query->result_array();
				foreach($click_info as $click){
					$this->db->select("*");
					$this->db->from('mo_report_category');
					$this->db->where("date_ymd", $click['click_dt']);
					$this->db->where("category", "IAB".$i);
					$this->db->where("cre_no", $click['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$click_cnt = $row->click_cnt + $click['click_cnt'];
						$imp_cnt = $row->imp_cnt;
						$loc_price = $row->loc_price;
						$price = $row->price;
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;
						$loc_ppc = $loc_price / $click_cnt;
						$ppc = $price / $click_cnt;
						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $click['click_dt']);
						$this->db->where("category", "IAB".$i);
						$this->db->where("cre_no", $click['cre_no']);
						$this->db->update('mo_report_category', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						//노출없이 클릭이 없음
					}
				}
			}
		}
	}

	//타겟팅 time 리포팅
	function arrange_report_time($imp_no, $click_no){
		//TIME별로 노출수 카운트
		if(count($imp_no) > 0){
			$imp_no = implode($imp_no, ",");
			$sql = "SELECT SUBSTR(imp_dt, 1, 10) imp_dt, day, time, cre_no, COUNT(*) imp_cnt, SUM(price) total_price, SUM(loc_price) total_loc_price";
			$sql .= " FROM mo_impression_log_cal";
			$sql .= " WHERE imp_no IN (".$imp_no.")";
			$sql .= " GROUP BY SUBSTR(imp_dt, 1, 10), day, time, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$imp_info = $query->result_array();
				foreach($imp_info as $imp){
					$this->db->select("*");
					$this->db->from('mo_report_time');
					$this->db->where("date_ymd", $imp['imp_dt']);
					$this->db->where("day", $imp['day']);
					$this->db->where("time", $imp['time']);
					$this->db->where("cre_no", $imp['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$imp_cnt = $row->imp_cnt + $imp['imp_cnt'];
						$click_cnt = $row->click_cnt;
						$loc_price = $row->loc_price + $imp['total_loc_price'];
						$price = $row->price + $imp['total_price'];
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;

						if($click_cnt > 0){
							$loc_ppc = $loc_price / $click_cnt;
							$ppc = $price / $click_cnt;
						}else{
							$loc_ppc = 0;
							$ppc = 0;
						}

						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'price' => $price,
										'loc_price' => $loc_price,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $imp['imp_dt']);
						$this->db->where("day", $imp['day']);
						$this->db->where("time", $imp['time']);
						$this->db->where("cre_no", $imp['cre_no']);
						$this->db->update('mo_report_time', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						if($imp['day'] == 'mon'){
							$day_seq = 1;
						}else if($imp['day'] == 'tue'){
							$day_seq = 2;
						}else if($imp['day'] == 'wed'){
							$day_seq = 3;
						}else if($imp['day'] == 'thu'){
							$day_seq = 4;
						}else if($imp['day'] == 'fri'){
							$day_seq = 5;
						}else if($imp['day'] == 'sat'){
							$day_seq = 6;
						}else if($imp['day'] == 'sun'){
							$day_seq = 7;
						}

						$data = array(
										'date_ymd' => $imp['imp_dt'],
										'cre_no' => $imp['cre_no'],
										'day' => $imp['day'],
										'day_seq' => $day_seq,
										'time' => $imp['time'],
										'imp_cnt' => $imp['imp_cnt'],
										'click_cnt' => 0,
										'price' => $imp['total_price'],
										'loc_price' => $imp['total_loc_price'],
										'ctr' => 0,
										'loc_ppi' => $imp['total_loc_price'] / $imp['imp_cnt'],
										'ppi' => $imp['total_price'] / $imp['imp_cnt'],
										'loc_ppc' => 0,
										'ppc' => 0
						);
						$sql = $this->db->insert_string("mo_report_time", $data);
						$this->db->query($sql);
					}
				}
			}
		}
		//TIME별로 클릭수 카운트
		if(count($click_no) > 0){
			$click_no = implode($click_no, ",");
			$sql = "SELECT SUBSTR(click_dt, 1, 10) click_dt, cre_no, day, time, COUNT(*) click_cnt";
			$sql .= " FROM mo_click_log_cal";
			$sql .= " WHERE click_no IN (".$click_no.")";
			$sql .= " GROUP BY SUBSTR(click_dt, 1, 10), device, cre_no";

			$query = $this->db->query($sql);

			if ( $query->num_rows() > 0 ){
				$click_info = $query->result_array();
				foreach($click_info as $click){
					$this->db->select("*");
					$this->db->from('mo_report_time');
					$this->db->where("date_ymd", $click['click_dt']);
					$this->db->where("day", $click['day']);
					$this->db->where("time", $click['time']);
					$this->db->where("cre_no", $click['cre_no']);
					$query = $this->db->get();
					//해당날짜에 데이터가 있다면 업데이트
					if ( $query->num_rows() > 0 ){
						$row = $query->row();
						$click_cnt = $row->click_cnt + $click['click_cnt'];
						$imp_cnt = $row->imp_cnt;
						$loc_price = $row->loc_price;
						$price = $row->price;
						$ctr = $click_cnt / $imp_cnt * 100;
						$loc_ppi = $loc_price / $imp_cnt;
						$ppi = $price / $imp_cnt;
						$loc_ppc = $loc_price / $click_cnt;
						$ppc = $price / $click_cnt;
						$data = array(
										'imp_cnt' => $imp_cnt,
										'click_cnt' => $click_cnt,
										'ctr' => $ctr,
										'loc_ppi' => $loc_ppi,
										'ppi' => $ppi,
										'loc_ppc' => $loc_ppc,
										'ppc' => $ppc
						);
						$this->db->where("date_ymd", $click['click_dt']);
						$this->db->where("day", $click['day']);
						$this->db->where("time", $click['time']);
						$this->db->where("cre_no", $click['cre_no']);
						$this->db->update('mo_report_time', $data);
						//해당날짜에 데이터가 있다면 첫 인서트
					}else{
						//노출없이 클릭이 없음
					}
				}
			}
		}
	}


	//하루에 한번 업데이트시 ( 1시간에 한번씩으로 하려면 mo_sale_info_cal 로 계산 )
	function arrange_calculate_tax_temp(){
		$this->db->empty_table("mo_calculate_tax");
		$this->db->select("mem_no, DATE_FORMAT(date_ymd, '%Y-%m') AS month, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(total_loc_price) total_loc_price, sum(total_price) total_price", FALSE);
		$this->db->from('mo_sale_info');


		//$this->db->where('month', date('Y-m', strtotime('-1 month')));
		$this->db->where('month', date('Y-m'));

		$this->db->group_by("mem_no, month");
		$this->db->order_by("month", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$sale_info = $query->result_array();

			foreach($sale_info as $sale){
				$this->db->select("mem_com_nm, mem_com_no, role");
				$this->db->from('mo_members');
				$this->db->where("mem_no", $sale['mem_no']);
				$query = $this->db->get();
				$row = $query->row();
				$mem_com_nm = $row->mem_com_nm;
				$mem_com_no = $row->mem_com_no;
				$role = $row->role;

				$tax_price = $sale['total_price'] / 10;
				$tax_loc_price = $sale['total_loc_price'] / 10;

				$result_price = $sale['total_price'] + $tax_price;
				$result_loc_price = $sale['total_loc_price'] + $tax_loc_price;

				$data = array(
					'date_ym' => $sale['month'],
					'mem_no' => $sale['mem_no'],
					'mem_com_nm' => $mem_com_nm,
					'role' => $role,
					'mem_com_no' => $mem_com_no,
					'loc_price' => $sale['total_loc_price'],
					'price' => $sale['total_price'],
					'tax_loc_price' => $tax_loc_price,
					'tax_price' => $tax_price,
					'result_loc_price' => $result_loc_price,
					'result_price' => $result_price
				);

				$this->db->select("*");
				$this->db->from('mo_calculate_tax');
				$this->db->where("mem_no", $sale['mem_no']);
				$this->db->where("date_ym", $sale['month']);
				$query = $this->db->get();
				if($query->num_rows() == 0){
					$sql = $this->db->insert_string("mo_calculate_tax", $data);
					$this->db->query($sql);
				}
			}
		}
	}

	//하루에 한번 업데이트시 ( 1시간에 한번씩으로 하려면 mo_sale_info_cal 로 계산 )
	function arrange_report_data(){

		$this->db->empty_table("mo_report_member");
		$this->db->empty_table("mo_report_campaign");
		$this->db->empty_table("mo_report_creative_group");
		$this->db->empty_table("mo_report_creative");
		//광고주 일별 리포트
		$this->db->select("date_ymd, mem_no, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(total_loc_price) total_loc_price, sum(total_price) total_price");
		$this->db->from('mo_sale_info');
		$this->db->group_by("mem_no, date_ymd");
		$this->db->order_by("date_ymd", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$sale_info = $query->result_array();
			foreach($sale_info as $sale){

				$this->db->select("mem_type, mem_id");
				$this->db->from('mo_members');
				$this->db->where("mo_members.mem_no", $sale['mem_no']);
				$query = $this->db->get();
				$row = $query->row();
				$mem_type = $row->mem_type;
				$mem_id = $row->mem_id;

				if($mem_type == "master"){
					$this->db->select("mo_members.mem_com_nm, mo_members.role, mo_members_group.agency_no, mo_members_group.sales_no");
					$this->db->from('mo_members');
					$this->db->join('mo_members_group', 'mo_members_group.adver_no = mo_members.mem_no');
					$this->db->where("mo_members.mem_no", $sale['mem_no']);
					$query = $this->db->get();
					$row = $query->row();
					$mem_com_nm = $row->mem_com_nm;
					$group_no = $row->agency_no;
					$sales_no = $row->sales_no;
					$role = $row->role;
				}else if($mem_type == "manager"){
					$this->db->select("mo_members.mem_com_nm, mo_members.role, mo_members_group.manager_no, mo_members_group.agency_no, mo_members_group.sales_no");
					$this->db->from('mo_members');
					$this->db->join('mo_members_group', 'mo_members_group.manager_no = mo_members.mem_no');
					$this->db->where("mo_members.mem_no", $sale['mem_no']);
					$query = $this->db->get();
					$row = $query->row();
					$mem_com_nm = $row->mem_com_nm;
					$group_no = $row->agency_no;
					$sales_no = $row->sales_no;
					$role = $row->role;
				}

				//그룹role
				$this->db->select("role, mem_com_nm");
				$this->db->from('mo_members');
				$this->db->where("mem_no", $group_no);
				$query = $this->db->get();
				$row = $query->row();
				$group_role = $row->role;
				$group_nm = $row->mem_com_nm;

				//영업자 정보
				$this->db->select("mem_nm");
				$this->db->from('mo_members');
				$this->db->where("mem_no", $sales_no);
				$query = $this->db->get();
				$row = $query->row();
				$sales_nm = $row->mem_nm;

				//ctr = 클릭수 / 노출수 * 100
				//평균 ppc = 총광고비 / 클릭수
				//평균 ppi = 총광고비 / 노출수
				if($sale['imp_cnt'] > 0){
					$ctr = $sale['click_cnt'] / $sale['imp_cnt'] * 100;
					$loc_ppi = $sale['total_loc_price'] / $sale['imp_cnt'];
					$ppi = $sale['total_price'] / $sale['imp_cnt'];
				}else{
					$ctr = 0;
					$loc_ppi = 0;
					$ppi = 0;
				}

				if($sale['click_cnt'] > 0){
					$loc_ppc = $sale['total_loc_price'] / $sale['click_cnt'];
					$ppc = $sale['total_price'] / $sale['click_cnt'];
				}else{
					$loc_ppc = 0;
					$ppc = 0;
				}

				$data = array(
					'date_ymd' => $sale['date_ymd'],
					'mem_no' => $sale['mem_no'],
					'mem_type' => $mem_type,
					'mem_id' => $mem_id,
					'sales_no' => $sales_no,
					'sales_nm' => $sales_nm,
					'group_no' => $group_no,
					'group_role' => $group_role,
					'group_nm' => $group_nm,
					'mem_com_nm' => $mem_com_nm,
					'role' => $role,
					'imp_cnt' => $sale['imp_cnt'],
					'click_cnt' => $sale['click_cnt'],
					'loc_price' => $sale['total_loc_price'],
					'price' => $sale['total_price'],
					'ctr' => $ctr,
					'loc_ppc' => $loc_ppc,
					'ppc' => $ppc,
					'loc_ppi' => $loc_ppi,
					'ppi' => $ppi
				);

				$sql = $this->db->insert_string("mo_report_member", $data);
				$this->db->query($sql);
			}
		}
		//캠페인 일별 리포트
		$this->db->select("date_ymd, mem_no, cam_no, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(total_loc_price) total_loc_price, sum(total_price) total_price");
		$this->db->from('mo_sale_info');
		$this->db->group_by("cam_no, date_ymd");
		$this->db->order_by("date_ymd", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$sale_info = $query->result_array();
			foreach($sale_info as $sale){
				$this->db->select("cam_nm");
				$this->db->from('mo_campaign');
				$this->db->where("cam_no", $sale['cam_no']);
				$query = $this->db->get();
				$row = $query->row();
				$cam_nm = $row->cam_nm;

				//ctr = 클릭수 / 노출수 * 100
				//평균 ppc = 총광고비 / 클릭수
				//평균 ppi = 총광고비 / 노출수
				if($sale['imp_cnt'] > 0){
					$ctr = $sale['click_cnt'] / $sale['imp_cnt'] * 100;
					$loc_ppi = $sale['total_loc_price'] / $sale['imp_cnt'];
					$ppi = $sale['total_price'] / $sale['imp_cnt'];
				}else{
					$ctr = 0;
					$loc_ppi = 0;
					$ppi = 0;
				}

				if($sale['click_cnt'] > 0){
					$loc_ppc = $sale['total_loc_price'] / $sale['click_cnt'];
					$ppc = $sale['total_price'] / $sale['click_cnt'];
				}else{
					$loc_ppc = 0;
					$ppc = 0;
				}

				$data = array(
					'date_ymd' => $sale['date_ymd'],
					'mem_no' => $sale['mem_no'],
					'cam_no' => $sale['cam_no'],
					'cam_nm' => $cam_nm,
					'imp_cnt' => $sale['imp_cnt'],
					'click_cnt' => $sale['click_cnt'],
					'loc_price' => $sale['total_loc_price'],
					'price' => $sale['total_price'],
					'ctr' => $ctr,
					'loc_ppc' => $loc_ppc,
					'ppc' => $ppc,
					'loc_ppi' => $loc_ppi,
					'ppi' => $ppi
				);

				$sql = $this->db->insert_string("mo_report_campaign", $data);
				$this->db->query($sql);
			}
		}
		//광고그룹 일별 리포트
		$this->db->select("date_ymd, mem_no, cam_no, cre_gp_no, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(total_loc_price) total_loc_price, sum(total_price) total_price");
		$this->db->from('mo_sale_info');
		$this->db->group_by("cre_gp_no, date_ymd");
		$this->db->order_by("date_ymd", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$sale_info = $query->result_array();
			foreach($sale_info as $sale){
				$this->db->select("cre_gp_nm");
				$this->db->from('mo_creative_group');
				$this->db->where("cre_gp_no", $sale['cre_gp_no']);
				$query = $this->db->get();
				$row = $query->row();
				$cre_gp_nm = $row->cre_gp_nm;

				//ctr = 클릭수 / 노출수 * 100
				//평균 ppc = 총광고비 / 클릭수
				//평균 ppi = 총광고비 / 노출수
				if($sale['imp_cnt'] > 0){
					$ctr = $sale['click_cnt'] / $sale['imp_cnt'] * 100;
					$loc_ppi = $sale['total_loc_price'] / $sale['imp_cnt'];
					$ppi = $sale['total_price'] / $sale['imp_cnt'];
				}else{
					$ctr = 0;
					$loc_ppi = 0;
					$ppi = 0;
				}

				if($sale['click_cnt'] > 0){
					$loc_ppc = $sale['total_loc_price'] / $sale['click_cnt'];
					$ppc = $sale['total_price'] / $sale['click_cnt'];
				}else{
					$loc_ppc = 0;
					$ppc = 0;
				}

				$data = array(
					'date_ymd' => $sale['date_ymd'],
					'mem_no' => $sale['mem_no'],
					'cam_no' => $sale['cam_no'],
					'cre_gp_no' => $sale['cre_gp_no'],
					'cre_gp_nm' => $cre_gp_nm,
					'imp_cnt' => $sale['imp_cnt'],
					'click_cnt' => $sale['click_cnt'],
					'loc_price' => $sale['total_loc_price'],
					'price' => $sale['total_price'],
					'ctr' => $ctr,
					'loc_ppc' => $loc_ppc,
					'ppc' => $ppc,
					'loc_ppi' => $loc_ppi,
					'ppi' => $ppi
				);

				$sql = $this->db->insert_string("mo_report_creative_group", $data);
				$this->db->query($sql);
			}
		}
		//광고 일별 리포트
		$this->db->select("date_ymd, mem_no, cre_gp_no, cre_no, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(total_loc_price) total_loc_price, sum(total_price) total_price");
		$this->db->from('mo_sale_info');
		$this->db->group_by("cre_no, date_ymd");
		$this->db->order_by("date_ymd", "asc");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$sale_info = $query->result_array();
			foreach($sale_info as $sale){
				$this->db->select("cre_nm");
				$this->db->from('mo_creative');
				$this->db->where("cre_no", $sale['cre_no']);
				$query = $this->db->get();
				$row = $query->row();
				$cre_nm = $row->cre_nm;

				//ctr = 클릭수 / 노출수 * 100
				//평균 ppc = 총광고비 / 클릭수
				//평균 ppi = 총광고비 / 노출수
				if($sale['imp_cnt'] > 0){
					$ctr = $sale['click_cnt'] / $sale['imp_cnt'] * 100;
					$loc_ppi = $sale['total_loc_price'] / $sale['imp_cnt'];
					$ppi = $sale['total_price'] / $sale['imp_cnt'];
				}else{
					$ctr = 0;
					$loc_ppi = 0;
					$ppi = 0;
				}

				if($sale['click_cnt'] > 0){
					$loc_ppc = $sale['total_loc_price'] / $sale['click_cnt'];
					$ppc = $sale['total_price'] / $sale['click_cnt'];
				}else{
					$loc_ppc = 0;
					$ppc = 0;
				}

				$data = array(
					'date_ymd' => $sale['date_ymd'],
					'mem_no' => $sale['mem_no'],
					'cre_gp_no' => $sale['cre_gp_no'],
					'cre_no' => $sale['cre_no'],
					'cre_nm' => $cre_nm,
					'imp_cnt' => $sale['imp_cnt'],
					'click_cnt' => $sale['click_cnt'],
					'loc_price' => $sale['total_loc_price'],
					'price' => $sale['total_price'],
					'ctr' => $ctr,
					'loc_ppc' => $loc_ppc,
					'ppc' => $ppc,
					'loc_ppi' => $loc_ppi,
					'ppi' => $ppi
				);

				$sql = $this->db->insert_string("mo_report_creative", $data);
				$this->db->query($sql);
			}
		}
	}

	function active_daily_budget_low(){
		$this->db->select("*");
		$this->db->from('mo_campaign');
		$this->db->where("cam_status", "5"); //일예산초과일때
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$campaign_info = $query->result_array();

			$i = count($campaign_info);

			$cam_no_list = "";

			foreach($campaign_info as $key=>$campaign){
				if($key+1 == $i){
					$cam_no_list .= $campaign['cam_no'];
				}else{
					$cam_no_list .= $campaign['cam_no'].",";
				}
			}

			$data = array(
				'cam_status' => "1",
			);

			if($cam_no_list != null){
				$where = "cam_no IN (".$cam_no_list.")";
				$this->db->where($where);
			}

			$this->db->update('mo_campaign', $data);
		}


		$this->db->select("*");
		$this->db->from('mo_creative_group');
		$this->db->where("cre_gp_status", "5"); //일예산초과일때
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$creative_group_info = $query->result_array();

			$i = count($creative_group_info);

			$cre_gp_no_list = "";

			foreach($creative_group_info as $key=>$creative_group){
				if($key+1 == $i){
					$cre_gp_no_list .= $creative_group['cre_gp_no'];
				}else{
					$cre_gp_no_list .= $creative_group['cre_gp_no'].",";
				}
			}

			$data = array(
							'cre_gp_status' => "1",
			);

			if($cre_gp_no_list != null){
				$where = "cre_gp_no IN (".$cre_gp_no_list.")";
				$this->db->where($where);
			}

			$this->db->update('mo_creative_group', $data);
		}


		$this->db->select("*");
		$this->db->from('mo_creative');
		$this->db->where("cre_status", "5"); //일예산초과일때
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$creative_info = $query->result_array();

			$i = count($creative_info);

			$cre_no_list = "";

			foreach($creative_info as $key=>$creative){
				if($key+1 == $i){
					$cre_no_list .= $creative['cre_no'];
				}else{
					$cre_no_list .= $creative['cre_no'].",";
				}
			}

			$data = array(
							'cre_status' => "1",
			);

			if($cre_no_list != null){
				$where = "cre_no IN (".$cre_no_list.")";
				$this->db->where($where);
			}

			$this->db->update('mo_creative', $data);
		}
	}

	function active_cash_low($mem_no){
		$this->db->select("*");
		$this->db->from('mo_campaign');
		$this->db->where("cam_status", "6"); //잔액부족일때
		$this->db->where("adver_no", $mem_no);
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$campaign_info = $query->result_array();

			$i = count($campaign_info);

			$cam_no_list = "";

			foreach($campaign_info as $key=>$campaign){
				if($key+1 == $i){
					$cam_no_list .= $campaign['cam_no'];
				}else{
					$cam_no_list .= $campaign['cam_no'].",";
				}
			}

			$data = array(
							'cam_status' => "1",
			);

			if($cam_no_list != null){
				$where = "cam_no IN (".$cam_no_list.")";
				$this->db->where($where);
			}

			$this->db->update('mo_campaign', $data);
		}

		$this->db->select("*");
		$this->db->from('mo_creative_group');
		$this->db->join("mo_campaign", "mo_campaign.cam_no = mo_creative_group.cam_no");
		$this->db->where("mo_creative_group.cre_gp_status", "6"); //잔액부족일때
		$this->db->where("mo_campaign.adver_no", $mem_no);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$creative_group_info = $query->result_array();

			$i = count($creative_group_info);

			$cre_gp_no_list = "";

			foreach($creative_group_info as $key=>$creative_group){
				if($key+1 == $i){
					$cre_gp_no_list .= $creative_group['cre_gp_no'];
				}else{
					$cre_gp_no_list .= $creative_group['cre_gp_no'].",";
				}
			}

			$data = array(
							'cre_gp_status' => "1",
			);

			if($cre_gp_no_list != null){
				$where = "cre_gp_no IN (".$cre_gp_no_list.")";
				$this->db->where($where);
			}

			$this->db->update('mo_creative_group', $data);
		}


		$this->db->select("*");
		$this->db->from('mo_creative');
		$this->db->join("mo_creative_group", "mo_creative_group.cre_gp_no = mo_creative.cre_gp_no");
		$this->db->join("mo_campaign", "mo_campaign.cam_no = mo_creative_group.cam_no");
		$this->db->where("mo_creative.cre_status", "6"); //잔액부족일때
		$this->db->where("mo_campaign.adver_no", $mem_no);

		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$creative_info = $query->result_array();

			$i = count($creative_info);

			$cre_no_list = "";

			foreach($creative_info as $key=>$creative){
				if($key+1 == $i){
					$cre_no_list .= $creative['cre_no'];
				}else{
					$cre_no_list .= $creative['cre_no'].",";
				}
			}

			$data = array(
							'cre_status' => "1",
			);

			if($cre_no_list != null){
				$where = "cre_no IN (".$cre_no_list.")";
				$this->db->where($where);
			}

			$this->db->update('mo_creative', $data);
		}

	}

	//개별 데이터 초기화
	function init_data(){
		if(isset($_GET['pw']) && $_GET['pw'] = '1234'){
			$this->db->truncate("mo_alarm");
			$this->db->truncate("mo_bid_request");
			$this->db->truncate("mo_bid_response");
			$this->db->truncate("mo_board_attach");
			$this->db->truncate("mo_board_read");
			$this->db->truncate("mo_calculate_tax");
			$this->db->truncate("mo_calculate_tax_history");
			$this->db->truncate("mo_cash_charge");
			$this->db->truncate("mo_cash_history");
			$this->db->truncate("mo_cash_revoke_log");
			$this->db->truncate("mo_click_data");
			$this->db->truncate("mo_click_log");
			$this->db->truncate("mo_click_log_cal");
			$this->db->truncate("mo_event_log");
			$this->db->truncate("mo_impression_data");
			$this->db->truncate("mo_impression_log");
			$this->db->truncate("mo_impression_log_cal");
			$this->db->truncate("mo_members_cash_log");
			$this->db->truncate("mo_notice");
			$this->db->truncate("mo_notice_reply");
			$this->db->truncate("mo_question");
			$this->db->truncate("mo_question_answer");
			$this->db->truncate("mo_receivable");
			$this->db->truncate("mo_refund_request");
			$this->db->truncate("mo_report_browser");
			$this->db->truncate("mo_report_campaign");
			$this->db->truncate("mo_report_category");
			$this->db->truncate("mo_report_creative");
			$this->db->truncate("mo_report_creative_group");
			$this->db->truncate("mo_report_device");
			$this->db->truncate("mo_report_member");
			$this->db->truncate("mo_report_os");
			$this->db->truncate("mo_report_time");
			$this->db->truncate("mo_revenue");
			$this->db->truncate("mo_receivable");
			$this->db->truncate("mo_receivable_detail");
			//$this->db->truncate("mo_revenue_deposit");
			//$this->db->truncate("mo_revenue_price");
			//$this->db->truncate("mo_revenue_refund");
			$this->db->truncate("mo_sale_info");
			$this->db->truncate("mo_sale_info_cal");
			$this->db->truncate("mo_tax");

			$data = array(
							'mem_cash' => "0",
							'mem_event_cash' => "0",
							'mem_pay_later' => "N",
			);

			$this->db->update('mo_members', $data);

		}
	}

	function init_all_data(){
		//비딩정보만 삭제, 금액 리셋
		if(isset($_GET['pw']) && $_GET['pw'] = '1234'){
			$this->db->truncate("mo_alarm");
			$this->db->truncate("mo_bid_request");
			$this->db->truncate("mo_bid_response");
			$this->db->truncate("mo_board_attach");
			$this->db->truncate("mo_board_read");
			$this->db->truncate("mo_calculate_tax");
			$this->db->truncate("mo_calculate_tax_history");
			$this->db->truncate("mo_campaign");
			$this->db->truncate("mo_cash_charge");
			$this->db->truncate("mo_cash_history");
			$this->db->truncate("mo_cash_revoke_log");
			$this->db->truncate("mo_category_member");
			$this->db->truncate("mo_cert");
			$this->db->truncate("mo_click_data");
			$this->db->truncate("mo_click_log");
			$this->db->truncate("mo_click_log_cal");
			$this->db->truncate("mo_creative");
			$this->db->truncate("mo_creative_group");
			$this->db->truncate("mo_event_log");
			$this->db->truncate("mo_impression_data");
			$this->db->truncate("mo_impression_log");
			$this->db->truncate("mo_impression_log_cal");
			$this->db->truncate("mo_login_log");
			$this->db->truncate("mo_members");
			$this->db->truncate("mo_members_cash_log");
			$this->db->truncate("mo_members_group");
			$this->db->truncate("mo_notice");
			$this->db->truncate("mo_notice_reply");
			$this->db->truncate("mo_question");
			$this->db->truncate("mo_question_answer");
			$this->db->truncate("mo_receivable");
			$this->db->truncate("mo_refund_request");
			$this->db->truncate("mo_report_browser");
			$this->db->truncate("mo_report_campaign");
			$this->db->truncate("mo_report_category");
			$this->db->truncate("mo_report_creative");
			$this->db->truncate("mo_report_creative_group");
			$this->db->truncate("mo_report_device");
			$this->db->truncate("mo_report_member");
			$this->db->truncate("mo_report_os");
			$this->db->truncate("mo_report_time");
			$this->db->truncate("mo_revenue");
			$this->db->truncate("mo_receivable");
			$this->db->truncate("mo_receivable_detail");
			//$this->db->truncate("mo_revenue_deposit");
			//$this->db->truncate("mo_revenue_price");
			//$this->db->truncate("mo_revenue_refund");
			$this->db->truncate("mo_sale_info");
			$this->db->truncate("mo_sale_info_cal");
			$this->db->truncate("mo_tax");
			$this->db->truncate("mo_targeting");
		}
	}

	function alarm($mem_no, $type, $alarm){
		//알람 -- 시작
		$this->db->select("*");
		$this->db->from('mo_alarm');
		$this->db->where('mem_no', $mem_no);
		$this->db->where('alarm_type', $type);
		$query = $this->db->get();

		if($type == '1'){
			$alarm_nm = "캐쉬 소진";
			$alarm_cont = "strAlarmContent1";
		}else if($type == '2'){
			$alarm_nm = "캠페인 일 허용예산부족";
			$alarm_cont = "strAlarmContent2";
		}else if($type == '3'){
			$alarm_nm = "광고주 캐쉬 최소금액 미만";
			$alarm_cont = "strAlarmContent3";
		}else if($type == '4'){
			$alarm_nm = "대행사/랩사 캐쉬 최소금액 미만";
			$alarm_cont = "strAlarmContent4";
		}else if($type == '5'){
			$alarm_nm = "캐쉬 최소금액 미만";
			$alarm_cont = "strAlarmContent3";
		}

		if ( $query->num_rows() > 0 ){
			//같은 타입의 알람이 1일 지나지 않았을 경우만 업데이트
			$alarm_info = $query->result_array();

			$date = $alarm_info['0']['date_ymd'];
			$date = strtotime($date);
			// 1일 후
			$date_1day_time = strtotime('+1 day', $date);
			$date_1day = date('Y-m-d H:i:s', $$date_1day_time);

			//현재시간이 알람이 간지 1일이 지난 시간이면 다시 알림
			if(date('Y-m-d H:i:s') > $date_1day){
				$data = array(
					'alarm_nm' => $alarm_nm,
					'alarm_cont' => $alarm_cont,
					'alarm_confirm_fl' => "N",
					'alarm_fl' => "N",
					'date_ymd' => date("Y-m-d H:i:s")
				);
				$this->db->where("alarm_type", $type);
				$this->db->where("mem_no", $mem_no);
				$this->db->update("mo_alarm", $data);


				if($type == 1 && $alarm['email_alarm_c0_fl'] == "Y"){
				   // 메일 발송
					$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
				}

				if($type == 2 && $alarm['email_alarm_c10_fl'] == "Y"){
					// 메일 발송
					$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
				}

				if($type == 3 && $alarm['email_alarm_d10_fl'] == "Y"){
					// 메일 발송
					$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
				}

				if($type == 4 && $alarm['email_alarm_d10_fl'] == "Y"){
					// 메일 발송
					$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
				}

				if($type == 5){
					// 메일 발송
					$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
				}
			}
		}else{
			$data = array(
				'mem_no' => $mem_no,
				'alarm_type' => $type,
				'alarm_nm' => $alarm_nm,
				'alarm_cont' => $alarm_cont,
				'date_ymd' => date("Y-m-d H:i:s")
			);
			$sql = $this->db->insert_string("mo_alarm", $data);
			$this->db->query($sql);

			if($type == 1 && $alarm['email_alarm_c0_fl'] == "Y"){
				// 메일 발송
				$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
			}

			if($type == 2 && $alarm['email_alarm_c10_fl'] == "Y"){
				// 메일 발송
				$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
			}

			if($type == 3 && $alarm['email_alarm_d10_fl'] == "Y"){
				// 메일 발송
				$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
			}

			if($type == 4 && $alarm['email_alarm_d10_fl'] == "Y"){
				// 메일 발송
				$this->send_email($alarm['mem_email'], $alarm_nm, $alarm_cont);
			}
		}
		//알람 -- 끝

	}


	function select_alarm_info($mem_no){

		$alarm_info = array();

		$this->db->select("*");
		$this->db->from('mo_alarm');
		$this->db->where('mem_no', $mem_no);
		$this->db->where('alarm_confirm_fl', 'N');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$alarm_info = $query->result_array();
		}

		return $alarm_info;
	}

	function alarm_confirm($alarm_no){
		$this->db->where('alarm_no', $alarm_no);
		$data = array(
			'alarm_confirm_fl' => 'Y'
		);
		$this->db->update('mo_alarm', $data);
	}

	function send_email($target_email, $subject ,$content){

		$this->load->library('email');

		$config['mailtype']  = "html";
		$config['charset']   = "UTF-8";
		$config['protocol']  = "smtp";
		$config['smtp_host'] = "ssl://smtp.googlemail.com";
		$config['smtp_port'] = 465;
		$config['smtp_user'] = "adsense@adop.co.kr";
		$config['smtp_pass'] = "asdfqaz135%";
		$this->email->initialize($config);
		// 메일 수신 체크 데이터 생성
		$this->email->set_newline("\r\n");
		$this->email->clear();
		$this->email->from('adsense@adop.co.kr', '애드오피 운영');
		$this->email->to($target_email);
		//$this->email->to('justin@adop.co.kr');

		$data = array(
		  'content' => $content
		);
		$this->email->subject('ATOM '.$subject.' 안내메일');
		$msg = $this->load->view('common/member_alarm_mail_form', $data, true);
		$this->email->message($msg);

		$this->email->send();
	}

	function arrange_agency_no(){

		$this->db->select("*");
		$this->db->from('mo_members');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$mem_info = $query->result_array();

			foreach($mem_info as $mem){
				$this->db->select("*");
				$this->db->from('mo_members_group');
				$this->db->where('manager_no', $mem['mem_no']);
				$query = $this->db->get();
				$mem_row = $query->row();

				if($mem_row->agency_no > 0){
					$data = array(
						'agency_no'=>$mem_row->agency_no
					);
				}else{
					$data = array(
						'agency_no'=>$mem['mem_no']
					);
				}

				$where = " mem_no = '".$mem['mem_no']."' ";
				$udt_query = $this->db->update_string('mo_members', $data, $where);
				$this->db->query($udt_query);
			}
		}

	}


	//수익데이터 업데이트 (전날 데이터 업데이트 - 그룹번호, 그룹명, role, 잔여예치금 (전달기준 예치금 - (매출 + 수수료 + 환불)), 실매출 )
	function arrange_revenue_data(){
		//$d1_day = date("Y-m-d", strtotime("-1 days"));

		$d1_day = date("Y-m-d");

		//전날 데이터 셀렉트
		$this->db->select("*");
		$this->db->from('mo_revenue');
		$this->db->where("date_ymd <= '".$d1_day."'");
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$revenue_info = $query->result_array();

			foreach($revenue_info as $rev){

				$this->db->select("*");
				$this->db->from('mo_members');
				$this->db->where('mem_no', $rev['mem_no']);
				$query = $this->db->get();
				$mem_row = $query->row();
				//그룹번호, 그룹명, role, 회사명 업데이트
				$this->db->where('mem_no', $rev['mem_no']);
				$data = array(
						'agency_no'=>$mem_row->agency_no,
						'mem_id'=>$mem_row->mem_id,
						'mem_type'=>$mem_row->mem_type,
						'group_no'=>$mem_row->group_no,
						'group_nm'=>$mem_row->group_nm,
						'group_role'=>$mem_row->role,
						'mem_com_nm'=>$mem_row->mem_com_nm,
						'agency_no'=>$mem_row->agency_no,
						'mem_id'=>$mem_row->mem_id,
						'mem_pay_later'=>$mem_row->mem_pay_later,
						'mem_type'=>$mem_row->mem_type
				);

				$this->db->update('mo_revenue', $data);

				//수익 계산 ( 매출 - 수수료 )
				$revenue = $rev['price'] - $rev['fee_price'];
				$loc_revenue = $rev['loc_price'] - $rev['loc_fee_price'];
				$revenue_lt = $rev['price_lt'] - $rev['fee_price_lt'];
				$loc_revenue_lt = $rev['loc_price_lt'] - $rev['loc_fee_price_lt'];

				$this->db->where('revenue_no', $rev['revenue_no']);
				$data = array(
					'revenue'=>$revenue,
					'loc_revenue'=>$loc_revenue,
					'revenue_lt'=>$revenue_lt,
					'loc_revenue_lt'=>$loc_revenue_lt,
					'pre_deposit' => $rev['deposit'] - ($rev['price'] + $rev['refund']),
					'loc_pre_deposit' => $rev['loc_deposit'] - ($rev['loc_price'] + $rev['loc_refund']),
					'pre_deposit_lt' => $rev['deposit_lt'] - ($rev['price_lt'] + $rev['refund_lt']),
					'loc_pre_deposit_lt' => $rev['loc_deposit_lt'] - ($rev['loc_price_lt'] + $rev['loc_refund_lt'])
				);
				$this->db->update('mo_revenue', $data);


			}

		}

	}
 
	//세금계산서 업데이트
	function arrange_calculate_tax(){
		//$m1_day = date("Y-m", strtotime("-1 month"));

		$m1_day = date("Y-m");

		//한달전 데이터 셀렉트
		$this->db->select("date_ymd, mo_members.mem_type, mo_members.mem_id, mo_members.mem_no, mo_members.mem_com_nm, mo_members.role, mo_members.group_nm, mo_members.mem_com_no, mo_members.mem_pay_later");
		$this->db->select("SUM(pre_deposit) AS pre_deposit ");
		$this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
		$this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
		$this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
		$this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
		$this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
		$this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
		$this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
		$this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
		$this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
		$this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%Y-%m') AS month", FALSE);
		$this->db->from('mo_revenue');
		$this->db->join('mo_members', 'mo_revenue.mem_no = mo_members.mem_no');
		$this->db->where("DATE_FORMAT(date_ymd,'%Y-%m')", $m1_day);
		$this->db->group_by("month, mem_no");
		$query = $this->db->get();

		if ( $query->num_rows() > 0 ){
			$revenue_info = $query->result_array();

			foreach($revenue_info as $rev){

				$this->db->select("mo_members_group.agency_no");
				$this->db->from('mo_members');

				if($rev['mem_type']  == "manager"){
					$this->db->join('mo_members_group', 'mo_members_group.manager_no = mo_members.mem_no');
					$this->db->where("mo_members_group.manager_no", $rev['mem_no']);
				}else if($rev['mem_type']  == "master"){
					$this->db->join('mo_members_group', 'mo_members_group.adver_no = mo_members.mem_no');
					$this->db->where("mo_members_group.adver_no", $rev['mem_no']);
				}
				$query = $this->db->get();
				$mem_row = $query->row();
				$agency_no = $mem_row->agency_no;

				$this->db->select("*");
				$this->db->from('mo_calculate_tax');
				$this->db->where("date_ym", $rev['month']);
				$this->db->where("mem_no", $rev['mem_no']);
				$query = $this->db->get();

				if($query->num_rows() == 0){

					 if($rev['mem_pay_later'] == "Y"){
						$data = array(
										'date_ym' => $rev['month'],
										'mem_no' => $rev['mem_no'],
										'mem_id' => $rev['mem_id'],
										'mem_type' => $rev['mem_type'],
										'agency_no' => $agency_no,
										'mem_com_nm' => $rev['mem_com_nm'],
										'mem_com_no' => $rev['mem_com_no'],
										'price' => $rev['price_lt'],
										'loc_price' => $rev['loc_price_lt'],
										'tax_price' => $rev['price_lt'] / 10,
										'tax_loc_price' => $rev['loc_price_lt'] / 10,
										'result_price' => $rev['price_lt'] + ($rev['price_lt'] / 10),
										'result_loc_price' => $rev['loc_price_lt'] + ($rev['loc_price_lt'] / 10),
										'mem_pay_later' => $rev['mem_pay_later'],
										'role' => $rev['role']
						);
					}else{
						$data = array(
										'date_ym' => $rev['month'],
										'mem_no' => $rev['mem_no'],
										'mem_id' => $rev['mem_id'],
										'mem_type' => $rev['mem_type'],
										'agency_no' => $agency_no,
										'mem_com_nm' => $rev['mem_com_nm'],
										'mem_com_no' => $rev['mem_com_no'],
										'price' => $rev['price'],
										'loc_price' => $rev['loc_price'],
										'tax_price' => $rev['price'] / 10,
										'tax_loc_price' => $rev['loc_price'] / 10,
										'result_price' => $rev['price'] + ($rev['price'] / 10),
										'result_loc_price' => $rev['loc_price'] + ($rev['loc_price'] / 10),
										'mem_pay_later' => $rev['mem_pay_later'],
										'role' => $rev['role']
						);
					}
					$sql = $this->db->insert_string("mo_calculate_tax", $data);
					$this->db->query($sql);

				}else{

					if($rev['mem_pay_later'] == "Y"){
						$data = array(
										'date_ym' => $rev['month'],
										'mem_no' => $rev['mem_no'],
										'mem_id' => $rev['mem_id'],
										'mem_type' => $rev['mem_type'],
										'agency_no' => $agency_no,
										'mem_com_nm' => $rev['mem_com_nm'],
										'mem_com_no' => $rev['mem_com_no'],
										'price' => $rev['price_lt'],
										'loc_price' => $rev['loc_price_lt'],
										'tax_price' => $rev['price_lt'] / 10,
										'tax_loc_price' => $rev['loc_price_lt'] / 10,
										'result_price' => $rev['price_lt'] + ($rev['price_lt'] / 10),
										'result_loc_price' => $rev['loc_price_lt'] + ($rev['loc_price_lt'] / 10),
										'mem_pay_later' => $rev['mem_pay_later'],
										'role' => $rev['role']
						);
					}else{
						$data = array(
										'date_ym' => $rev['month'],
										'mem_no' => $rev['mem_no'],
										'mem_id' => $rev['mem_id'],
										'mem_type' => $rev['mem_type'],
										'agency_no' => $agency_no,
										'mem_com_nm' => $rev['mem_com_nm'],
										'mem_com_no' => $rev['mem_com_no'],
										'price' => $rev['price'],
										'loc_price' => $rev['loc_price'],
										'tax_price' => $rev['price'] / 10,
										'tax_loc_price' => $rev['loc_price'] / 10,
										'result_price' => $rev['price'] + ($rev['price'] / 10),
										'result_loc_price' => $rev['loc_price'] + ($rev['loc_price'] / 10),
										'mem_pay_later' => $rev['mem_pay_later'],
										'role' => $rev['role']
						);
					}
					$this->db->where("date_ym", $rev['month']);
					$this->db->where("mem_no", $rev['mem_no']);
					$this->db->update("mo_calculate_tax", $data);

				}
			}
		}
	}
}
?>
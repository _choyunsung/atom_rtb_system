<?php
class Admin_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function select_members_count($condition) {
        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where('mem_fl', "N");

        if ($condition["mem_nm"] != "") {
            $this->db->like('mem_nm', $condition["mem_nm"]);
            $this->db->or_like('mem_id', $condition["mem_nm"]);
        }

        if ($condition["group_no"] != "") {
            $this->db->where('group_no', $condition["group_no"]);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }

    public function select_members_list($condition, $per_page, $page_num) {

        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where('mem_fl', "N");

        if ($condition["mem_nm"] != "") {
            $this->db->like('mem_nm', $condition["mem_nm"]);
            $this->db->or_like('mem_id', $condition["mem_nm"]);
        }

        if ($condition["group_no"] != "") {
            $this->db->where('group_no', $condition["group_no"]);
        }

        $this->db->order_by('mem_no', 'DESC');
        $this->db->limit($per_page, $page_num);

        $query = $this->db->get();

        //print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }

    }

    function get_member($mem_no) {
        $sql = "SELECT * FROM mountain.mo_members WHERE mem_no = '".$mem_no."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function select_admin_account_list($condition, $per_page, $page_num){

        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("admin_fl = 'Y'");

        if($condition['admin_nm'] != ""){
            $this->db->like("mem_nm", $condition['admin_nm']);
        }

        if($condition['group_no'] != ""){
            $this->db->where("group_no", $condition['group_no']);
        }

        $this->db->order_by('mem_no', 'DESC');
        $this->db->limit($per_page, $page_num);

        $query = $this->db->get();

        //print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    public function select_admin_account_list_count($condition){

        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("admin_fl = 'Y'");

        if($condition['admin_nm'] != ""){
            $this->db->like("mem_nm", $condition['admin_nm']);
        }

        if($condition['group_no'] != ""){
            $this->db->where("group_no", $condition['group_no']);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }

    public function select_admin_group_list(){

        $this->db->select("group_no, group_nm");
        $this->db->from('mo_category_template');
        $this->db->where("admin_fl = 'Y'");
        $this->db->group_by('group_no');
        $this->db->order_by('group_no', 'ASC');

        $query = $this->db->get();

        //print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    public function insert_admin_account($data){
        $sql = $this->db->insert_string("mo_members", $data);
        $this->db->query($sql);
        return $this->db->insert_id();
    }

    public function update_admin_account($data){

        $this->db->where("mem_no", $data["mem_no"]);
        $this->db->update('mo_members', $data);

    }

}
?>
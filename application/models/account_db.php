<?php
class Account_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function select_master_info($mem_no){
        $sql = "SELECT
                    A.*,
                    B.*
                FROM
                    mountain.mo_members A LEFT JOIN
                    (SELECT * FROM mountain.mo_timezone ) B ON (B.tz_no = A.mem_timezone)
                WHERE
                    A.mem_no = '".$mem_no."'; ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
            return $row[0];
        }
        
    }
    
    public function update_master_pwd($mem_no,$mem_pwd){
        $mem_pwd = md5($mem_pwd);
        $sql = "UPDATE mountain.mo_members SET mem_pwd='$mem_pwd' WHERE mem_no='$mem_no' ";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return "ok";
        }else{
            return "FALSE";
        }
    }
    
    function check_master_pwd($mem_no,$tmp_pwd){
        $mem_pwd = md5($tmp_pwd);
        $sql = " SELECT mem_no FROM mountain.mo_members WHERE mem_pwd='$mem_pwd' AND mem_no='$mem_no' ";
        $result= $this->db->query($sql);
        if ($result->num_rows() > 0) {
            return "ok";
        }else{
            return "false";
        }
    }
    
    public function master_info_modify_save($mem_no,$data){
        $where = " mem_no='$mem_no' ";
        $udt_query = $this->db->update_string('mountain.mo_members', $data, $where);
        $query = $this->db->query($udt_query);
        return "ok";
    }
    
    public function select_account_manager_list($mem_no){

        $manager_list = array();
        $sql = " SELECT manager_no FROM mountain.mo_members_group WHERE agency_no=$mem_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if($row['manager_no']!=""){
                    $manager_list[] = $row['manager_no'];
                }
            }
        
            if (count($manager_list) > 0){
                $manager_list = implode(',', $manager_list);
                $sql = " SELECT *, (SELECT code_desc FROM mountain.mo_code WHERE code_nm='manager_status' AND code_key=A.mem_active_st) AS manager_st ";
                $sql.= " FROM mountain.mo_members A WHERE mem_no IN ($manager_list) ";
                $query = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        $result[] = $row;
                    }
                    return $result;
                }
            }
        }
    }
    
    public function insert_manager_account($data,$master_mem_no){
        $ist_query = $this->db->insert_string('mountain.mo_members', $data);
        $query = $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        
        $sql = " INSERT INTO mountain.mo_members_group ";
        $sql.= " (agency_no, manager_no) ";
        $sql.= " VALUES ";
        $sql.= " ('$master_mem_no', '$ret') ";
        $this->db->query($sql);

        if($ret != ""){
            return $ret;
        }else{
            return "FALSE";
        }
    }
    
    public function select_account_manager_info($mem_no){
        $sql = " SELECT * FROM mountain.mo_members WHERE mem_no='$mem_no' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row[0];
        }
    }
    
    function manager_status_change($data){
        $sql = " UPDATE mountain.mo_members SET ";
        $sql.= " mem_active_st= '".$data['status_key']."' ";
        $sql.= " WHERE mem_no IN (".$data['mem_no_arr'].") ";
        $query = $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return "ok";
        }else{
            return "false";
        }
    }
    
    function update_manager_info($data, $mem_no){
        $where = " mem_no = $mem_no ";
        $udt_query = $this->db->update_string('mountain.mo_members', $data, $where);
        $this->db->query($udt_query);
        return "ok";
    }
    
    function select_login_list_count($mem_no, $from_date, $to_date){
        $manager_list[]=$mem_no;
        $sql = " SELECT manager_no FROM mountain.mo_members_group WHERE agency_no=$mem_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if($row['manager_no']!=""){
                    $manager_list[] = $row['manager_no'];
                }
            }
        }
        
        if ($manager_list[0] != ""){
            $manager_list = implode(',', $manager_list);
            $sql = " SELECT *, (SELECT mem_id FROM mountain.mo_members WHERE mem_no=A.mem_no) ";
            $sql.= " FROM mountain.mo_login_log A ";
            $sql.= " WHERE mem_no IN ($manager_list) AND login_dt BETWEEN '".$from_date."' AND '".$to_date."' ";
            
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            }else{
                return 0;
            }
        }
    } 
    
    function select_account_login_list($mem_no,  $from_date, $to_date, $per_page, $page_num){
        $manager_list[]=$mem_no;
        $sql = " SELECT manager_no FROM mountain.mo_members_group WHERE agency_no='$mem_no' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if($row['manager_no']!=""){
                    $manager_list[] = $row['manager_no'];
                }
            }
        }
        
        if ($manager_list[0] != ""){
            $manager_list = implode(',', $manager_list);
            $sql = " SELECT *, ";
            $sql.= " (SELECT mem_id FROM mountain.mo_members WHERE mem_no=A.mem_no) AS mem_id, ";
            $sql.= " (SELECT group_nm FROM mountain.mo_members WHERE mem_no=A.mem_no) AS group_nm ";
            $sql.= " FROM mountain.mo_login_log A ";
            $sql.= " WHERE mem_no IN ($manager_list) AND DATE_FORMAT(login_dt,'%Y-%m-%d') BETWEEN '".$from_date."' AND '".$to_date."' ";
            $sql.= " ORDER BY login_dt DESC ";
            if($page_num == null || $page_num == ""){
                $page_num = 0;
            }
            
            $sql.= " LIMIT ".$page_num.",".$per_page;
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $result[] = $row;
                }
                return $result;
            }
        }
    }
    
    function select_advertiser_list_count($mem_no){
        $sql = " SELECT adver_no FROM mountain.mo_members_group WHERE agency_no=$mem_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if($row['adver_no']!=""){
                    $manager_list[] = $row['adver_no'];
                }
            }
        }
        
        if ($manager_list[0] != ""){
            $manager_list = implode(',',$manager_list);
            $sql = " SELECT *, (SELECT mem_nm FROM mountain.mo_members WHERE mem_no=A.mem_no) ";
            $sql.= " FROM mountain.mo_members A WHERE mem_no IN ($manager_list) ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            }else{
                return 0;
            }
        }
    }
    
    function select_account_advertiser_list($mem_no, $per_page, $page_num){
        $sql = " SELECT adver_no FROM mountain.mo_members_group WHERE agency_no=$mem_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if($row['adver_no']!=""){
                    $manager_list[] = $row['adver_no'];
                }
            }
        }
    
        if ($manager_list[0] != ""){
            $manager_list = implode(',', $manager_list);
            $sql = " SELECT *, (SELECT mem_nm FROM mountain.mo_members WHERE mem_no=A.mem_no) ";
            $sql.= " FROM mountain.mo_members A WHERE mem_no IN ($manager_list) ";
            
            if($page_num == null || $page_num == ""){
                $page_num = 0;
            }
            
            $sql.= " LIMIT ".$page_num.",".$per_page;
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $result[] = $row;
                }
                return $result;
            }
        }
    }
    
    public function advertiser_modify_save($mem_no, $data){
        $where = " mem_no = '".$mem_no."' ";
        $udt_query = $this->db->update_string('mountain.mo_members', $data, $where);
        $this->db->query($udt_query);
        return "ok";
    }

    public function select_account_list_count($cond){
        $sql = " SELECT A.* ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members A LEFT JOIN ";
        $sql.= " mountain.mo_members_group B ON (B.adver_no = A.mem_no) ";
        $sql.= " WHERE 1=1 ";
        if ($cond['agency_yn'] == "Y"){
            $sql.= " AND role = 'agency' ";
        }elseif ($cond['lab_yn'] == "Y"){
            $sql.= " AND role = 'rep' ";
        }elseif ($cond['adver_yn'] == "Y"){
            $sql.= " AND role = 'adver' ";
        }elseif ($cond['ind_yn'] == "Y"){
            $sql.= " AND role = 'ind' ";
        }
        if ($cond['master_yn'] == "on"){
            $sql.= " AND mem_type = 'master' ";
        }
        if ($cond['manager_yn'] == "on"){
            $sql.= " AND mem_type = 'manager' ";
        }
        if ($cond['mem_com_nm'] != ""){
            $sql.= " AND mem_com_nm LIKE '%".$cond['mem_com_nm']."%' ";
        }
        if ($cond['mem_id'] != ""){
            $sql.= " AND mem_id LIKE '%".$cond['mem_id']."%' ";
        }
        if ($cond["fromto_date"] != ""){
            $sql.= " AND mem_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        }
        if ($cond["last_connect"] == "year"){
            $from_date = date("Y-m-d", strtotime("-1 year"));
            $sql.= " AND mem_last_login <= '".$from_date."'";
        }elseif ($cond["last_connect"] == "6month"){
            $from_date = date("Y-m-d", strtotime("-6 month"));
            $sql.= " AND mem_last_login <= '".$from_date."'";
        }
        
        if ($cond['mem_active_st'][0] >= 1){
            $mem_active_st = implode(',', $cond['mem_active_st']);
            $sql.= " AND mem_active_st IN ($mem_active_st)";
        }
        
        if ($cond['sales_no'] != ""){
            $sql.= " AND B.sales_no = '".$cond['sales_no']."' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row;
        }else{
            return array();
        }
    }

    public function select_account_list($cond, $per_page, $page_num){
        
        $sql = " SELECT B.*, (SELECT mem_nm FROM mountain.mo_members WHERE mem_no=A.sales_no) AS sales_nm ";
        $sql.= " FROM ";
        $sql.= " (SELECT IF(adver_no IS NULL, manager_no, adver_no) AS mem_no, sales_no FROM ";
        $sql.= " mountain.mo_members_group) A lEFT JOIN ";
        $sql.= " mountain.mo_members B ";
        $sql.= " ON B.mem_no = A.mem_no ";
        $sql.= " WHERE 1=1 ";
//         $sql = " SELECT A.*, (SELECT mem_nm FROM mountain.mo_members WHERE mem_no=B.sales_no) AS sales_nm ";
//         $sql.= " FROM mountain.mo_members A LEFT JOIN ";
//         $sql.= " mountain.mo_members_group B ON (B.adver_no = A.mem_no ) ";
//         $sql.= " WHERE 1=1 ";
        if ($cond['agency_yn'] == "Y"){
            $sql.= " AND role = 'agency' ";
        }elseif ($cond['lab_yn'] == "Y"){
            $sql.= " AND role = 'rep' ";
        }elseif ($cond['adver_yn'] == "Y"){
            $sql.= " AND role = 'adver' ";
        }elseif ($cond['ind_yn'] == "Y"){
            $sql.= " AND role = 'ind' ";
        }
        if ($cond['master_yn'] == "on"){
            $sql.= " AND mem_type = 'master' ";
        }
        if ($cond['manager_yn'] == "on"){
            $sql.= " AND mem_type = 'manager' ";
        }
        if ($cond['mem_com_nm'] != ""){
            $sql.= " AND mem_com_nm LIKE '%".$cond['mem_com_nm']."%' ";
        }
        if ($cond['mem_id'] != ""){
            $sql.= " AND mem_id = '".$cond['mem_id']."' ";
        }
        if ($cond["fromto_date"] != ""){
            $sql.= " AND mem_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        }
        if ($cond["last_connect"] == "year"){
            $from_date = date("Y-m-d", strtotime("-1 year"));
            $sql.= " AND mem_last_login <= '".$from_date."'";
        }elseif ($cond["last_connect"] == "6month"){
            $from_date = date("Y-m-d", strtotime("-6 month"));
            $sql.= " AND mem_last_login <= '".$from_date."'";
        }
        
        if ($cond['mem_active_st'][0] >= 1){
            $mem_active_st = implode(',', $cond['mem_active_st']);
            $sql.= " AND mem_active_st IN ($mem_active_st)";
        }
        
        if ($cond['sales_no'] != ""){
            $sql.= " AND A.sales_no = '".$cond['sales_no']."' ";
        }
        $sql.= " ORDER BY B.mem_no DESC";
        if ($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    public function select_account_list_summary(){
        $sql = " SELECT ";
        $sql.= " COUNT(1) AS all_cnt, ";
        $sql.= " IFNULL(SUM(IF(left(mem_ymd, 10) = date_format(now(),'%Y-%m-%d'), 1, 0)),0) AS today_cnt, ";
        $sql.= " IFNULL(SUM(IF(role = 'adver', 1, 0)),0) AS adver_cnt, ";
        $sql.= " IFNULL(SUM(IF(role = 'ind', 1, 0)),0) AS individual_cnt, ";
        $sql.= " IFNULL(SUM(IF(role = 'lab', 1, 0)),0) AS lab_cnt, ";
        $sql.= " IFNULL(SUM(IF(role = 'agency', 1, 0)),0) AS agency_cnt ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members ";
        $sql.= " WHERE admin_fl != 'Y' AND mem_fl = 'N' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
        
    }
    

    public function select_sales_list(){
        $this->db->select("*");
        $this->db->from('mo_members');
        //영업자 그룹 번호
        $this->db->where("role = 'sales'");
        $this->db->order_by('mem_no', 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    public function account_sales_modify($condition){
        $data['sales_no'] = $condition['sales_no'];
        $where = "agency_no = '".$condition['mem_no']."'";
        $query = $this->db->update_string('mo_members_group', $data, $where);
        $this->db->query($query);
        
        
        return "ok";
    }

    public function account_active_modify($condition){
        $data['mem_active_st'] = $condition['active_no'];
        
        //회원탈퇴할 경우 - 모든 캠페인, 광고그룹, 광고 삭제
        if($data['mem_active_st'] == '4'){
            $data['mem_fl'] = "Y";
        }
        
        $where = "mem_no = '".$condition['mem_no']."'";
        $query = $this->db->update_string('mo_members', $data, $where);
        $this->db->query($query);
        
        if($data['mem_active_st'] == '4'){
            $this->db->select("*");
            $this->db->from('mo_campaign');
            $this->db->where("adver_no", $condition['mem_no']);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $campaign = $query->result_array();
                foreach($campaign as $cam){
                    $where = "cam_no ='".$cam['cam_no']."'";
                    $data = array(
                                    'cam_fl' => 'Y'
                    );
                    $query = $this->db->update_string('mo_campaign', $data, $where);
                    $this->db->query($query);
                }
        
                $this->db->select("*");
                $this->db->from('mo_creative_group');
                $this->db->where("cam_no", $cam['cam_no']);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $creative_group = $query->result_array();
                    foreach($creative_group as $cre_gp){
                        $where = "cre_gp_no ='".$cre_gp['cre_gp_no']."'";
                        $data = array(
                                        'cre_gp_fl' => 'Y'
                        );
                        $query = $this->db->update_string('mo_creative_group', $data, $where);
                        $this->db->query($query);
        
                        $this->db->select("*");
                        $this->db->from('mo_creative');
                        $this->db->where("cre_gp_no", $cre_gp['cre_gp_no']);
                        $query = $this->db->get();
                        if ($query->num_rows() > 0) {
                            $creative = $query->result_array();
        
                            foreach($creative as $cre){
                                $where = "cre_no ='".$cre['cre_no']."'";
                                $data = array(
                                                'cre_fl' => 'Y'
                                );
                                $query = $this->db->update_string('mo_creative', $data, $where);
                                $this->db->query($query);
                            }
                        }
                    }
                }
            }
        }
        
        return "ok";
    }
    
    public function account_mem_type_modify($condition){
        
        $data['mem_type'] = $condition['type'];
         
        $where = "mem_no = '".$condition['mem_no']."'";
        $query = $this->db->update_string('mo_members', $data, $where);
        $this->db->query($query);

        return "ok";
    }
    
    public function select_account_info($mem_no){
    
        $sql = " SELECT A.*, (SELECT mem_nm FROM mo_members WHERE B.sales_no = mem_no) AS sales_nm ";
        $sql.= " FROM mountain.mo_members A LEFT JOIN ";
        $sql.= " mountain.mo_members_group B ON (B.adver_no = A.mem_no ) ";
        $sql.= " WHERE A.mem_no = '$mem_no' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
             $row = $query->result_array();
            return $row[0];
        }else{
            return array();
        }
    }
}
<?php
class Board_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function select_member_notice_list($cond, $per_page = null, $page_num = null) {
        $sql = " SELECT A.* ";
        $sql.= " FROM mountain.mo_notice A ";
        $sql.= " WHERE A.notice_fl = 'M' ";
        
        if ($cond['search_type'] == "sub"){
            $sql.= " AND notice_nm LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "cont"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "all"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' OR notice_nm LIKE '%".$cond['search_txt']."%' ";
        }
        
        $sql.= " ORDER BY notice_no DESC ";
        
        if ($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_notice_reply_list($notice_no){
        $sql = " SELECT A.*, B.mem_nm ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_notice_reply A LEFT JOIN ";
        $sql.= " mountain.mo_members B ON (A.mem_no = B.mem_no) ";
        $sql.= " WHERE A.notice_no = '$notice_no' AND A.reply_fl = 'N' ORDER BY A.parent_reply_no, A.reply_depth, A.reply_no "; 
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_admin_notice_list($cond, $per_page = null, $page_num = null) {
        $sql = " SELECT A.*, (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = A.mem_no) AS writer_nm ";
        $sql.= " FROM mountain.mo_notice A ";
        $sql.= " WHERE A.notice_fl = 'A' ";
        
        if ($cond['search_type'] == "sub"){
            $sql.= " AND notice_nm LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "cont"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "all"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' OR notice_nm LIKE '%".$cond['search_txt']."%' ";
        }
    
        $sql.= " ORDER BY notice_no DESC ";
    
        if ($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    
    }
    
    public function select_notice_detail_view($notice_no){
        $sql = " SELECT A.*, (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = A.mem_no) AS writer_nm ";
        $sql.= " FROM mountain.mo_notice A ";
        $sql.= " WHERE A.notice_no = '$notice_no' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function select_notice_attach_list($notice_no){
        $sql = " SELECT A.* ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_board_attach A  ";
        $sql.= " WHERE A.board_no = '$notice_no' AND A.board_fl = 'N' ORDER BY A.order_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_question_attach_list($question_no){
        $sql = " SELECT A.* ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_board_attach A  ";
        $sql.= " WHERE A.board_no = '$question_no' AND A.board_fl = 'Q' ORDER BY A.order_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_pre_notice_no($type, $notice_no){
        $sql = " SELECT notice_no FROM mountain.mo_notice where notice_fl = '$type' ";
        $sql.= " AND notice_no < '$notice_no' ORDER BY notice_no DESC LIMIT 1 ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
                
    }
    
    public function select_next_notice_no($type, $notice_no){
        $sql = " SELECT notice_no FROM mountain.mo_notice where notice_fl = '$type' ";
        $sql.= " AND notice_no > '$notice_no' ORDER BY notice_no LIMIT 1 ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function select_member_notice_count($cond) {
        $sql = " SELECT count(notice_no) AS cnt FROM mountain.mo_notice ";
        $sql.= " WHERE notice_fl = 'M' ";
        if ($cond['search_type'] == "sub"){
            $sql.= " AND notice_nm LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "cont"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "all"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' OR notice_nm LIKE '%".$cond['search_txt']."%' ";
        }
        $result = $this->db->query($sql);
        $row = $result->num_rows();
        return $row;
    }
    
    public function select_admin_notice_count($cond) {
        $sql = " SELECT count(notice_no) AS cnt FROM mountain.mo_notice ";
        $sql.= " WHERE notice_fl = 'A' ";
        if ($cond['search_type'] == "sub"){
            $sql.= " AND notice_nm LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "cont"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "all"){
            $sql.= " AND notice_cont LIKE '%".$cond['search_txt']."%' OR notice_nm LIKE '%".$cond['search_txt']."%' ";
        }
        $result = $this->db->query($sql);
        $row = $result->num_rows();
        return $row;
    }
    
    public function select_no_read_admin_notice($condition) {
        $sql = " SELECT * FROM mountain.mo_notice ";
        $sql.= " WHERE notice_fl = 'A' ";
        $result = $this->db->query($sql);
        $row = $result->row();
        return $row;
    }
    
    public function select_no_read_member_notice($condition) {
        $sql = " SELECT * FROM mountain.mo_notice ";
        $sql.= " WHERE notice_fl = 'M' ";
        $result = $this->db->query($sql);
        $row = $result->row();
        return $row;
    }
    
    public function insert_notice($data){
        $sql = $this->db->insert_string('mountain.mo_notice', $data);
        $query = $this->db->query($sql);
        $ret = $this->db->insert_id();
        return $ret;
    }

    public function update_notice($data, $notice_no){
        $where = ' notice_no = '.$notice_no;
        $sql = $this->db->update_string('mountain.mo_notice', $data, $where);
        $query = $this->db->query($sql);
    }
    
    public function insert_notice_file($data) {
        
        $board_no=$data['board_no'];
        $order_no=$data['order_no'];
        $sel_sql = "SELECT attach_no FROM mountain.mo_board_attach WHERE board_no='$board_no' AND order_no='$order_no' ";
        $sel_query = $this->db->query($sel_sql);
        if ($sel_query->num_rows() > 0) {
            $up_sql = " UPDATE mountain.mo_board_attach SET attach_file_nm='".$data['attach_file_nm']."' ";
            $up_sql.= " WHERE board_no='$board_no' AND order_no='$order_no' ";
            $query = $this->db->query($up_sql);
        }else{
            $sql = $this->db->insert_string('mountain.mo_board_attach', $data);
            $query = $this->db->query($sql);
        }
    }
    
    public function update_notice_read_cnt($notice_no){
        $udt_query = " UPDATE mountain.mo_notice SET read_no = read_no + 1 WHERE  notice_no = '$notice_no' ";
        $this->db->query($udt_query);
    }
    
    public function insert_board_read($mem_no, $board_no, $board_fl){
        $dup_sql = " SELECT mem_no FROM mountain.mo_board_read WHERE mem_no = '$mem_no' AND board_no = '$board_no' AND board_fl = '$board_fl' ";
        $query = $this->db->query($dup_sql);
        if ($query->num_rows() == 0) {
            $date_ymd = date('Y-m-d');
            $sql = " INSERT INTO mountain.mo_board_read (board_no, board_fl, mem_no, date_ymd) values ('$board_no','$board_fl','$mem_no','$date_ymd') ";
            $this->db->query($sql);
        }
    }
    
    public function insert_reply($data){
        $sql = $this->db->insert_string('mountain.mo_notice_reply', $data);
        $this->db->query($sql);
        $ret = $this->db->insert_id();
        
        if ($ret > 0){
            if ($data['reply_depth'] == "1"){
                $sql = " UPDATE mountain.mo_notice_reply SET parent_reply_no = '$ret' WHERE reply_no = '$ret' ";
                $this->db->query($sql);
            }
        }
        return $ret;
    }
    
    public function update_reply1($data, $reply_no){
        $where = " reply_no = '$reply_no' ";
        $sql = $this->db->update_string('mountain.mo_notice_reply', $data, $where);
        $this->db->query($sql);
    }
    
    public function delete_reply1($data, $reply_no){
        $where = " reply_no = '$reply_no' ";
        $sql = $this->db->update_string('mountain.mo_notice_reply', $data, $where);
        $this->db->query($sql);
    }
    
    public function select_question_list($cond, $per_page = null, $page_num = null) {
        $sql = " SELECT A.*, B.*, C.mem_id, C.mem_com_nm ";
        $sql.= " , (SELECT code_desc FROM mountain.mo_code WHERE code_nm = 'question_fl' AND code_key = A.question_fl) AS question_fl_desc ";
        $sql.= " , (SELECT code_desc FROM mountain.mo_code WHERE code_nm = 'question_st' AND code_key = A.question_st) AS question_st_desc ";
        $sql.= " , (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = A.contact_mem_no ) AS contact_nm ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A ";
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_board_attach B ON (A.question_no = B.board_no AND B.board_fl = 'Q') LEFT JOIN ";
        $sql.= " (SELECT mem_no, mem_id, mem_com_nm FROM mountain.mo_members ) C ON ( C.mem_no = A.mem_no) ";
        $sql.= " WHERE A.del_fl = 'N' ";
        
        //권한에 따라 모든문의를 보여줄지 본인것만 보여줄지
        if ($this->session->userdata('admin_fl') != "Y" ){
            $sql.= " AND A.mem_no = '".$cond['mem_no']."'";
        }
        
        //회원일경우 검색 플래그에 따라
        if ($cond['search_type'] == "sub"){
            $sql.= " AND question_nm LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "cont"){
            $sql.= " AND question_cont LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "all"){
            $sql.= " AND question_cont LIKE '%".$cond['search_txt']."%' OR question_nm LIKE '%".$cond['search_txt']."%' ";
        }
        
        //관리자의 문의 페이지에서 검색
        if ($cond['mem_id'] != ""){
            $sql.= " AND C.mem_id LIKE '%".$cond['mem_id']."%' ";
        }
        if ($cond['question_fl'] != ""){
            $sql.= " AND A.question_fl = '".$cond['question_fl']."' ";
        }
        if ($cond['no_answer'] == "on"){
            $sql.= " AND A.question_st = '1' ";
        }
        if ($cond['tmp_save'] == "on"){
            $sql.= " AND A.question_st = '2' ";
        }
        if ($cond['answer_done'] == "on"){
            $sql.= " AND A.question_st = '3' ";
        }
        
        if ($cond['manager_nm'] != ""){
            $sql.= " AND C.mem_id LIKE '%".$cond['mem_id']."%' ";
        }
        if ($cond['question_nm'] != ""){
            $sql.= " AND A.question_nm LIKE '%".$cond['question_nm']."%' ";
        }
        if ($cond['fromto_date'] != ""){
            $sql.= " AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."'  ";
        }
        
        $sql.= " ORDER BY question_no DESC ";
    
        if ($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_question_count($cond) {
        $sql = " SELECT count(question_no) AS cnt FROM mountain.mo_question ";
        if ($cond['search_type'] == "sub"){
            $sql.= " WHERE question_nm LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "cont"){
            $sql.= " WHERE question_cont LIKE '%".$cond['search_txt']."%' ";
        }elseif ($cond['search_type'] == "all"){
            $sql.= " WHERE question_cont LIKE '%".$cond['search_txt']."%' OR question_nm LIKE '%".$cond['search_txt']."%' ";
        }
        $result = $this->db->query($sql);
        $row = $result->row();
        return $row;
    }
    
    public function select_question_summary($cond) {
        $sql = " SELECT "; 
        $sql.= " COUNT(1) AS all_cnt, "; 
        $sql.= " IFNULL(SUM(IF(left(date_ymd, 10) = date_format(now(),'%Y-%m-%d'), 1, 0)),0) AS today_cnt, ";
        $sql.= " IFNULL(SUM(IF(question_st = 1, 1, 0)),0) AS noanswer_cnt, ";
        $sql.= " IFNULL(SUM(IF(question_st = 2, 1, 0)),0) AS answering_cnt, ";
        $sql.= " IFNULL(SUM(IF(question_st = 3, 1, 0)),0) AS done_cnt ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function select_question_detail_view($question_no){
        
        $sql = " SELECT A.*, B.*, C.*, A.mem_no AS writer_no, A.question_no AS question_no ";
        $sql.= " , A.date_ymd AS question_ymd, C.date_ymd AS answer_ymd ";
        $sql.= " , (SELECT code_desc FROM mountain.mo_code WHERE code_nm = 'question_fl' AND code_key = A.question_fl) AS question_fl_desc ";
        $sql.= " , (SELECT code_desc FROM mountain.mo_code WHERE code_nm = 'question_st' AND code_key = A.question_st) AS question_st_desc ";
        $sql.= " , (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = A.contact_mem_no ) AS contact_nm ";
        $sql.= " , A.question_no AS origin_qeustion_no";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A ";
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_board_attach B ON (A.question_no = B.board_no AND B.board_fl = 'Q') ";
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_question_answer C ON (A.question_no = C.question_no) ";
        $sql.= " WHERE A.question_no = '$question_no' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function insert_answer($data){
        $sql = $this->db->insert_string('mountain.mo_question_answer', $data);
        $query = $this->db->query($sql);
        $ret = $this->db->insert_id();
    
        return $ret;
    }
    
    public function update_answer($data, $answer_no){
        $where = " answer_no = '$answer_no' ";
        $sql = $this->db->update_string('mountain.mo_question_answer', $data, $where);
        $this->db->query($sql);
        $ret = $this->db->affected_rows();
        return $ret;
    }
    
    public function update_question($data, $question_no){
        $where = " question_no = '$question_no' ";
        $sql = $this->db->update_string('mountain.mo_question', $data, $where);
        $this->db->query($sql);
        return $question_no;
    }
    
    public function insert_question($data){
        $sql = $this->db->insert_string('mountain.mo_question', $data);
        $this->db->query($sql);
        $ret = $this->db->insert_id();
    
        return $ret;
    }

    public function select_all_notice($cond){
        $sql = " SELECT * FROM mountain.mo_notice ";
        $sql.= " WHERE del_fl = 'N' ";
        if ($cond['group_no'] != "1"){
            $sql.= " AND notice_fl = 'M' ";
        }
        $sql.= " ORDER BY notice_no DESC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }

    }


    public function test_db(){

        $sql = " SELECT * FROM mountain.mo_notice ";
        $sql.= " WHERE del_fl = 'N' ";
        $this->adopcommon->query($sql);
        $query = $this->db->query($sql);
        //$row = $query->result_array();
        return $query;
        //$this->db = $this->adopcommon;
    }
    
    
}

    
   
<?php
class Auction_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();

    }

    public function get_auction_items(){

        $start = $this->input->post('start');
        $cnt = $this->input->post('cnt');

        if($start == null) {
            $start = 0;
        }
        if($cnt == null) {
            $cnt = 10;
        }

        $page_start = $start * $cnt;

        $sql_cnt = " SELECT COUNT(items_idx) AS cnt FROM mo_auction_items";
        $query_cnt = $this->db->query($sql_cnt);

        if($query_cnt->num_rows() > 0) {
            $cnt_data = $query_cnt->row();
            $list_cnt = $cnt_data->cnt;
        }


        $sql = " SELECT * FROM mo_auction_items ORDER BY items_date DESC LIMIT $page_start, $cnt";
        $query = $this->db->query($sql);
        $result['list'] = $query->result_array();
        $result['total_cnt'] = $list_cnt;
        $result['start'] = $start;
        $result['page_cnt'] = $cnt;

        return $result;

    }

    public function get_auction_item_detail(){

        $item_date = $_POST['items_date'];
        $sql = " SELECT * FROM mo_auction_item_detail WHERE item_date = '$item_date' AND item_fl = 'N' ORDER BY item_order";
        $query = $this->db->query($sql);
        $idx_sql = " SELECT items_idx FROM mo_auction_items WHERE items_date = '$item_date' ";
        $idx_query = $this->db->query($idx_sql);
        $result['list'] = $query->result_array();
        $result['date'] = $item_date;
        $result['idx'] = $idx_query->result_array();
        return $result;

    }

    function insert_auction_items(){

        $insert_items['items_cnt'] = 0;
        $insert_detail['item_date'] = $_POST['daterange'];
        $insert_items['items_date'] = $_POST['daterange'];

        for($i=0; $i<10; $i++){

            if ($_POST['item_name'][$i] != "" && $_POST['item_skip'][$i] == 'N') {
                $insert_items['items_cnt']++;
                $insert_detail['item_name'] = $_POST['item_name'][$i];
                $insert_detail['item_info'] = $_POST['item_info'][$i];
                $insert_detail['item_price'] = $_POST['item_price'][$i];
                $insert_detail['item_link'] = $_POST['item_link'][$i];
                $insert_detail['item_order'] = $i;

                $this->insert_auction_item_query($insert_detail);

            }
        }

        $items_sql = $this->db->insert_string('mo_auction_items', $insert_items);
        $this->db->query($items_sql);
        return $this->db->insert_id();

    }

    function update_auction_items(){

        $update_items['items_cnt'] = 0;
        $items_date = $_POST['daterange'];

        for($i=0; $i<10; $i++) {
            $update_detail="";
            $where = " item_date = '$items_date' AND item_idx = '".$_POST['item_idx'][$i]."' ";

            if ($_POST['item_name'][$i] != "" && $_POST['item_skip'][$i] == 'N') { //상품명이 공백이 아니고, 스킵 체크가 안되있을 때
                $update_items['items_cnt']++;
                $update_detail['item_idx'] = $_POST['item_idx'][$i];
                $update_detail['item_name'] = $_POST['item_name'][$i];
                $update_detail['item_info'] = $_POST['item_info'][$i];
                $update_detail['item_price'] = $_POST['item_price'][$i];
                $update_detail['item_link'] = $_POST['item_link'][$i];
                $update_detail['item_order'] = $i;
                $update_detail['item_fl'] = "N";

                if($_POST['item_idx'][$i] != "") {
                    $where = " item_date = '$items_date' AND item_idx = '".$_POST['item_idx'][$i]."' ";
                    $this->update_auction_item_query($update_detail, $where);
                } else {
                    $update_detail['item_date'] = $items_date;
                    $this->insert_auction_item_query($update_detail);
                }
            }
            elseif($_POST['item_skip'][$i] == 'Y') { //스킵 체크가 되있을 때
                $update_detail['item_fl'] = "Y";
                $where = " item_date = '$items_date' AND item_idx = '".$_POST['item_idx'][$i]."' ";
                $this->update_auction_item_query($update_detail, $where);
            }
            else {
                $update_detail['item_fl'] = "Y";
                $where = " item_date = '$items_date' AND item_idx = '".$_POST['item_idx'][$i]."' ";
                $this->update_auction_item_query($update_detail, $where);
            }
        }

        $items_where = " items_date = '$items_date' ";
        $items_sql = $this->db->update_string('mo_auction_items', $update_items, $items_where);
        $this->db->query($items_sql);
        return "100";
    }

    function update_auction_item_query($data, $where){
        $sql = $this->db->update_string('mo_auction_item_detail', $data, $where);
        $this->db->query($sql);
        return true;
    }

    function insert_auction_item_query($data){
        $sql = $this->db->insert_string('mo_auction_item_detail', $data);
        $this->db->query($sql);
        return true;

    }

}

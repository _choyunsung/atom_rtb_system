<?php
class Code_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function sel_tel_no_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='tel_no_sel' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_cell_no_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='cell_no_sel' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_email_host_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='mail_host_sel' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_member_gb_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='member_gb_sel' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_status_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='status' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_bank_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='bank_name' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_manager_status_code(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='manager_status' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_country_code(){
        $sql = " SELECT * FROM mountain.mo_country WHERE ct_fl='N' ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_timezone_code(){
        $sql = " SELECT * FROM mountain.mo_timezone  ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
    function sel_question_fl(){
        $sql = " SELECT * FROM mountain.mo_code WHERE code_nm='question_fl' AND code_fl='N' ORDER BY code_order ASC ";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $row) {
            $result[] = $row;
        }
        return $result;
    }
    
}
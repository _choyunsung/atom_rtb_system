<?php
class Creative_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->db->cache_delete_all();
    }

    function select_creative_group_list($cond, $per_page = null, $page_num = null){
        $sql = " SELECT C.*, D.imp, D.clk, D.ctr, B.cam_nm, ";
        $sql.= " (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no = B.adver_no ) AS adver_nm, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='status' AND code_key=C.cre_gp_status) AS cre_gp_status_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_gp_type' AND code_key=C.cre_gp_type) AS cre_gp_type ";
        $sql.= " FROM ";
        $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no = '".$cond['mem_no']."') A LEFT JOIN ";
        $sql.= " mountain.mo_campaign B ON (B.adver_no = A.mem_no) LEFT JOIN ";
        $sql.= " mountain.mo_creative_group C ON (C.cam_no = B.cam_no) LEFT JOIN ";
        $sql.= " ( SELECT cre_gp_no, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL(AVG(ctr),0) AS ctr ";
        $sql.= " FROM mountain.mo_report_creative_group WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' GROUP BY cre_gp_no) D ";
        $sql.= " ON (D.cre_gp_no = C.cre_gp_no) ";
        $sql.= " WHERE C.cre_gp_fl = 'N' ";
        
        if ($cond['cam_no'] != ""){
            $sql.= " AND C.cam_no = '".$cond['cam_no']."' ";
        }
        if (isset($cond['cre_gp_status'])){
            if ($cond['cre_gp_status'][0]!=0){
                $cre_gp_status=implode(',',$cond['cre_gp_status']);
                $sql.= " AND C.cre_gp_status IN ($cre_gp_status) ";
            }
        }
        if ($page_num == null || $page_num == ""){
            $page_num = 0;
        }

        $sql.= " ORDER BY C.cre_gp_ymd DESC ";
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function select_sum_creative_group_list($cond){
        $sql = " SELECT 'strAll' AS row_nm, SUM(A.imp_cnt) AS imp, SUM(A.click_cnt) AS clk, (SUM(A.click_cnt) / SUM(A.imp_cnt) * 100) AS ctr, SUM(A.price) AS price "; 
        $sql.= " FROM mountain.mo_report_creative_group A, "; 
        $sql.= " (SELECT A.cam_no "; 
        $sql.= " FROM mountain.mo_campaign A, mountain.mo_members B "; 
        $sql.= " WHERE B.mem_no = A.adver_no and B.mem_no = ".$cond['mem_no'].") B ";
        $sql.= " WHERE A.cam_no = B.cam_no ";
        if($cond['cam_no']!=""){
            $sql.="     AND A.cam_no='".$cond['cam_no']."' ";
        }
        $sql.= " AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
            return $row[0];
        }
    }
    
    function select_creative_group_count($cond){
        $sql = " SELECT A.*, ";
        $sql.= " B.cam_nm, (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no = B.adver_no ) AS adver_nm, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='status' AND code_key=A.cre_gp_status) AS cre_gp_status_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_gp_type' AND code_key=A.cre_gp_type) AS cre_gp_type ";
        $sql.= " FROM "; 
        $sql.= " mountain.mo_creative_group A LEFT JOIN ";
        $sql.= " mountain.mo_campaign B ON (A.cam_no=B.cam_no), ";
        $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no = '".$cond['mem_no']."') C WHERE C.mem_no=B.adver_no ";
        $sql.= " AND cre_gp_fl='N' ";
        if($cond['cam_no']!=""){
            $sql.= " AND A.cam_no = '".$cond['cam_no']."' ";
        }
        if(isset($cond['cre_gp_status'])){
            if($cond['cre_gp_status'][0]!=0){
                $cre_gp_status=implode(',',$cond['cre_gp_status']);
                $sql.= " AND A.cre_gp_status IN ($cre_gp_status) ";
            }
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    function creative_group_list_count($cond){
        
        $sql = " SELECT COUNT(1) AS all_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_gp_status='1' THEN 1 ELSE 0 end),0) AS run_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_gp_status='2' THEN 1 ELSE 0 end),0) AS ready_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_gp_status='3' OR cre_gp_status = '5' OR cre_gp_status = '6' THEN 1 ELSE 0 end),0) AS pause_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_gp_status='4' THEN 1 ELSE 0 end),0) AS done_cnt ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_creative_group A ";
        if($cond['cam_no']!=""){
            $sql.= " WHERE cam_no = '".$cond['cam_no']."' ";
            $sql.= " AND A.cre_gp_fl='N' ";
        }else{
            $sql.= " LEFT JOIN ";
            $sql.= " mountain.mo_campaign B ON (A.cam_no = B.cam_no), ";
            $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no = '".$cond['mem_no']."') C WHERE C.mem_no = B.adver_no ";
            $sql.= " AND A.cre_gp_fl='N' ";
        }
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    function select_creative_group_chart_data($cond){
        $sql = " SELECT D.date_ymd, SUM(D.imp) AS imp, SUM(D.clk) AS clk, AVG(D.ctr) AS ctr ";
        $sql.= " FROM (SELECT * FROM mountain.mo_members WHERE mem_no = '".$cond['mem_no']."') A "; 
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_campaign B ON (A.mem_no = B.adver_no) "; 
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_creative_group C ON (B.cam_no = C.cam_no) "; 
        $sql.= " LEFT JOIN ";
        $sql.= " ( SELECT cam_no, cre_gp_no, date_ymd, IFNULL(imp_cnt,0) AS imp, IFNULL(click_cnt,0) AS clk, IFNULL(ctr,0) AS ctr "; 
        $sql.= " FROM mountain.mo_report_creative_group WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."'  ) "; 
        $sql.= " D ON (C.cre_gp_no = D.cre_gp_no) ";
        $sql.= " WHERE C.cre_gp_fl='N' AND D.date_ymd != '' ";
        if($cond['cam_no']!=""){
            $sql.= " AND C.cam_no = '".$cond['cam_no']."' ";
        }
        if(isset($cond['cre_gp_status'])){
            if($cond['cre_gp_status'][0] != 0){
                $cre_gp_status = implode(',',$cond['cre_gp_status']);
                $sql.= " AND C.cre_gp_status IN ($cre_gp_status) ";
            }
        }
        $sql.= " GROUP BY D.date_ymd ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function select_creative_list($cond, $per_page = null, $page_num = null){
        
        $sql = " SELECT D.*, A.cam_no, B.cre_gp_nm,  E.imp, E.clk, E.ctr, ";
        $sql.= " A.cam_nm, (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no = A.adver_no ) AS adver_nm, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='status' AND code_key=D.cre_status) AS cre_status_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='evaluation' AND code_key=D.cre_evaluation) AS cre_evaluation_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_type' AND code_key=D.cre_type) AS cre_gp_type_desc ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_campaign A LEFT JOIN ";
        $sql.= " mountain.mo_creative_group B ON (A.cam_no=B.cam_no) LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no='".$cond['mem_no']."') C ON (C.mem_no=A.adver_no) LEFT JOIN ";
        $sql.= " mountain.mo_creative D ON (B.cre_gp_no=D.cre_gp_no) LEFT JOIN ";
        $sql.= " ( SELECT cre_no, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL(AVG(ctr),0) AS ctr ";
        $sql.= " FROM mountain.mo_report_creative ";
        $sql.= " WHERE ";
        $sql.= " date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        $sql.= " GROUP BY cre_no) E ON (D.cre_no=E.cre_no) ";
        $sql.= " WHERE C.mem_no=A.adver_no ";
        $sql.= " AND D.cre_fl='N'";
        
        if($cond['cre_gp_no']!=""){
            $sql.= " AND D.cre_gp_no= '".$cond['cre_gp_no']."' ";
        }
        if(isset($cond['cre_status'])){
            if($cond['cre_status'][0]!=0){
                $cre_status=implode(',',$cond['cre_status']);
                $sql.= " AND D.cre_status IN ($cre_status) ";
            }
        }
       
        if($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        
        
        $sql .= ' order by D.cre_no desc';

        $query = $this->db->query($sql);

        $_select_create_data = $this->select_creative_auction();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $key => $row) {
                $result[$key] = $row;
                if($row['cre_type']=='4')
                    $result[$key]['cre_auction_type'] = $_select_create_data[$row['cre_no']]['adver_type'];
            }
            return $result;
        }
    }
    
    public function select_creative_auction()
    {
        $Qry = "SELECT DISTINCT id_no, adver_type FROM mo_agency_code";
        $query = $this->db->query($Qry);
        $row = $query->result_array();
        
        foreach($row as $key=> $val )
        {
            $_RETURN[$val['id_no']] = $val;
        }
        
        return $_RETURN;
    }
    
    function select_creative_chart_data($cond){
        $sql = " SELECT C.date_ymd, SUM(C.imp) AS imp, SUM(C.clk) AS clk, (SUM(C.imp) / SUM(C.clk) * 100) AS ctr ";
        $sql.= " FROM ";
        $sql.= " ( SELECT B.cre_gp_no, A.adver_no ";
        $sql.= " FROM mountain.mo_campaign A, mountain.mo_creative_group B, mo_members C ";
        $sql.= " WHERE C.mem_no=A.adver_no AND A.cam_no=B.cam_no  AND C.mem_no = '".$cond['mem_no']."') A ";
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_creative B ON (A.cre_gp_no= B.cre_gp_no) ";
        $sql.= " LEFT JOIN ";
        $sql.= " ( SELECT cre_no, date_ymd, IFNULL(imp_cnt,0) AS imp, IFNULL(click_cnt,0) AS clk, IFNULL(ctr,0) AS ctr ";
        $sql.= " FROM mountain.mo_report_creative WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ) C ON (B.cre_no = C.cre_no) ";
        $sql.= " WHERE B.cre_fl='N' AND C.date_ymd != '' ";
        if($cond['cre_gp_no']!=""){
            $sql.= " AND A.cre_gp_no = '".$cond['cre_gp_no']."' ";
        }
        if(isset($cond['cre_status'])){
            if($cond['cre_status'][0]!=0){
                $cre_status=implode(',',$cond['cre_status']);
                $sql.= " AND B.cre_status IN ($cre_status) ";
            }
        }
        $sql.= " GROUP BY C.date_ymd ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function select_sum_creative_list($cond){
        $sql = " SELECT 'strAll' AS row_nm, SUM(A.imp_cnt) AS imp, SUM(A.click_cnt) AS clk, (SUM(A.click_cnt) / SUM(A.imp_cnt) * 100) AS ctr, SUM(A.price) AS price ";
        $sql.= " FROM mountain.mo_report_creative A, ";
        $sql.= " (SELECT D.cre_no, C.cre_gp_no ";
        $sql.= " FROM mountain.mo_campaign A, mountain.mo_members B, mountain.mo_creative_group C, mountain.mo_creative D ";
        $sql.= " WHERE B.mem_no = A.adver_no AND A.cam_no=C.cam_no AND C.cre_gp_no=D.cre_gp_no and B.mem_no ='".$cond['mem_no']."' ";
        if($cond['cre_gp_no']!=""){
            $sql.=" AND C.cre_gp_no='".$cond['cre_gp_no']."' ";
        }
        $sql.= " ) B ";
        $sql.= " WHERE A.cre_no = B.cre_no AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            
            $row = $result->result_array();
            return $row[0];
        }
    }
    
    function select_creative_count($cond){
    $sql = " SELECT D.*, A.cre_gp_nm, ";
        $sql.= " B.cam_nm, (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no=B.adver_no ) AS adver_nm, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='status' AND code_key=D.cre_status) AS cre_status_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='evaluation' AND code_key=D.cre_evaluation) AS cre_evaluation_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_type' AND code_key=D.cre_type) AS cre_type_desc ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_creative_group A LEFT JOIN ";
        $sql.= " mountain.mo_campaign B ON (A.cam_no=B.cam_no) LEFT JOIN ";
        $sql.= " mountain.mo_creative D ON (A.cre_gp_no = D.cre_gp_no), ";
        $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no='".$cond['mem_no']."') C WHERE C.mem_no=B.adver_no";
        $sql.= " AND D.cre_fl='N' ";
        if($cond['cre_gp_no']!=""){
            $sql.= " AND D.cre_gp_no= '".$cond['cre_gp_no']."' ";
        }
        if(isset($cond['cre_status'])){
            if($cond['cre_status'][0]!=0){
                $cre_status=implode(',',$cond['cre_status']);
                $sql.= " AND D.cre_status IN ($cre_status) ";
            }
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    function select_creative_detail_view($cond){
        $sql = " SELECT D.*, B.cre_gp_nm, E.imp, E.clk, E.ctr, B.bid_loc_price, B.bid_price, B.daily_budget,";
        $sql.= " A.cam_nm, (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no=A.adver_no ) AS adver_nm, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='status' AND code_key=D.cre_status) AS cre_status_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='evaluation' AND code_key=D.cre_evaluation) AS cre_evaluation_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_gp_type' AND code_key=D.cre_type) AS cre_gp_type_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_type' AND code_key=D.cre_type) AS cre_type_desc ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_campaign A LEFT JOIN ";
        $sql.= " mountain.mo_creative_group B ON (A.cam_no=B.cam_no) LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no='".$cond['mem_no']."') C ON (C.mem_no=A.adver_no) LEFT JOIN ";
        $sql.= " mountain.mo_creative D ON (B.cre_gp_no=D.cre_gp_no) LEFT JOIN ";
        $sql.= " ( SELECT cre_no, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL(AVG(ctr),0) AS ctr ";
        $sql.= " FROM mountain.mo_report_creative ";
        $sql.= " WHERE ";
        $sql.= " date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        $sql.= " GROUP BY cre_no) E ON (D.cre_no=E.cre_no) ";
        $sql.= " WHERE C.mem_no=A.adver_no ";
        $sql.= " AND D.cre_fl='N' AND D.cre_no= '".$cond['cre_no']."' ";
        $query = $this->db->query($sql);
        
        $_auction_data = $this->select_creative_auction();
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            $row[0]['auction'] = $_auction_data[$row->cre_no]['adver_type'];
            return $row[0];
        }
    }
    
    function select_sum_creative_detail($cond){
        $sql = " SELECT 'strAll' AS row_nm, SUM(A.imp_cnt) AS imp, SUM(A.click_cnt) AS clk, (SUM(A.click_cnt) / SUM(A.imp_cnt) * 100) AS ctr, SUM(A.price) AS price ";
        $sql.= " FROM mountain.mo_report_creative A ";
        $sql.= " LEFT JOIN mountain.mo_creative B ON (A.cre_no = B.cre_no) ";
        $sql.= " WHERE B.cre_no = '".$cond['cre_no']."' ";
        $sql.= " AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
    
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
    
            $row = $result->result_array();
            return $row[0];
        }
    }
    
    function select_creative_detail_chart_data($cond){
        $sql = " SELECT B.date_ymd, SUM(B.imp) AS imp, SUM(B.clk) AS clk, (SUM(B.imp) / SUM(B.clk) * 100) AS ctr ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_creative A ";
        $sql.= " LEFT JOIN ";
        $sql.= " ( SELECT cre_no, date_ymd, IFNULL(imp_cnt,0) AS imp, IFNULL(click_cnt,0) AS clk, IFNULL(ctr,0) AS ctr ";
        $sql.= " FROM mountain.mo_report_creative WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ) B ON (A.cre_no = B.cre_no) ";
        $sql.= " WHERE B.date_ymd != '' ";
        $sql.= " AND A.cre_no = '".$cond['cre_no']."' ";
        $sql.= " GROUP BY B.date_ymd ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function creative_list_count($cond){
    
        $sql = " SELECT COUNT(1) AS all_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_status='1' THEN 1 ELSE 0 END),0) AS run_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_status='2' THEN 1 ELSE 0 END),0) AS ready_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_status='3' OR cre_status='5' OR cre_status='6' THEN 1 ELSE 0 END),0) AS pause_cnt, ";
        $sql.= " IFNULL(SUM(CASE WHEN cre_status='4' THEN 1 ELSE 0 END),0) AS done_cnt ";
        $sql.= " FROM mountain.mo_creative D ";
        if($cond['cre_gp_no']!=""){
            $sql.= " WHERE D.cre_fl='N' AND cre_gp_no='".$cond['cre_gp_no']."' ";
            
        }else{
            $sql.= " LEFT JOIN mountain.mo_creative_group A ON (A.cre_gp_no=D.cre_gp_no) LEFT JOIN ";
            $sql.= " mountain.mo_campaign B ON (A.cam_no=B.cam_no), ";
            $sql.= " (SELECT * FROM mountain.mo_members WHERE mem_no='".$cond['mem_no']."') C WHERE C.mem_no=B.adver_no ";
            $sql.= " AND D.cre_fl='N' ";
        }
        
        
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    function select_creative_group_info($cre_gp_no){
        $sql = " SELECT A.*, LEFT(A.start_ymd,10) AS start_ymd, LEFT(A.end_ymd,10) AS end_ymd, B.cam_nm, B.daily_budget AS cam_daily_budget, ";
        $sql.= " (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no = B.adver_no) AS adver_nm, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='status' AND code_key = A.cre_gp_status) AS cre_gp_status_desc, ";
        $sql.= " (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cre_gp_type' AND code_key = A.cre_gp_type) AS cre_gp_type_desc, ";
        $sql.= " (SELECT COUNT(1) FROM mountain.mo_targeting WHERE cre_gp_no = '".$cre_gp_no."') AS targeting_yn ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_creative_group A, mountain.mo_campaign B";
        $sql.= " WHERE A.cam_no = B.cam_no AND A.cre_gp_no = '".$cre_gp_no."'";
        
        $result = $this->db->query($sql);
        
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
           
            return $row[0];
        }
    }
    
    function select_creative_info($cre_no){
        $sql = " SELECT A.*, B.*, C.*, ";
        $sql.= " (SELECT mem_com_nm FROM mountain.mo_members WHERE mem_no=B.adver_no ) AS adver_nm ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_creative A, mountain.mo_campaign B , mountain.mo_creative_group C ";
        $sql.= " WHERE B.cam_no=C.cam_no  AND A.cre_gp_no=C.cre_gp_no AND A.cre_no='$cre_no' ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
            return $row[0];
        }
    }

    function select_ad_info($cre_no){
        $sql = " SELECT * ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_agency_code ";
        $sql.= " WHERE id_no = '$cre_no' ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
            return $row;
        }
    }
    
    function insert_creative_group($data){
        $query = $this->db->insert_string('mountain.mo_creative_group', $data);
        $this->db->query($query);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    function insert_new_creative($data){
        
        $_creative_insert = elements(
            array('cre_gp_no','cre_nm','cre_type','cre_link','cre_width','cre_height','cre_cont','cre_img_link','cre_ymd','cre_status','cre_evaluation','mem_no'),
            $data
        );

        if($data['cre_type']=='4') {
            $_creative_insert['cre_width'] = $data['cre_width_action'];
            $_creative_insert['cre_height'] = $data['cre_height_action'];
        }
        elseif($data['cre_type']=='5') { //치타
            $_creative_insert['cre_width'] = $data['cre_width_cheetah'];
            $_creative_insert['cre_height'] = $data['cre_height_cheetah'];
        }

        $ist_query = $this->db->insert_string('mountain.mo_creative', $_creative_insert);

        $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        
        if($data['cre_bncode_site']) {
            $data['id_no'] = $ret;
            $this->insert_new_creative_auction_bncode($data);
        }
        elseif($data['cre_zoneid_site']) {
            $data['id_no'] = $ret;
            $this->insert_new_creative_cheetah_zoneid($data);
        }
        
        return $ret;
    }
    
    public function insert_new_creative_auction_bncode($data) {
        $_SQry = sprintf("SELECT * FROM mo_agency_code WHERE id_no = '%d'",$data['id_no']);
        $result = $this->db->query($_SQry);
        
        if($result->num_rows() > 0) {
            $this->db->query(sprintf("DELETE FROM mo_agency_code WHERE id_no = '%d'", $data['id_no']));
        }

        $_bncode_array = json_decode($data['cre_bncode_site'],true);
        
        foreach($_bncode_array as $key => $val ) {
            $Qry[$key] = sprintf('("%d", "%s", "%d", "%s", "%s", "%s")', $data['id_no'], $val[0], $data['cre_auction_type'], $val[1], $val[2], date('Y-m-d H:i:s'));
        }

        $_Qry = "INSERT INTO mo_agency_code (id_no, bn_code, adver_type, site_domain, zone_id, regdate) values ".join(',', $Qry);
        return $this->db->query($_Qry);
    }

    public function insert_new_creative_cheetah_zoneid($data) {

        $_SQry = sprintf("SELECT * FROM mo_agency_code WHERE id_no = '%d'", $data['id_no']);
        $result = $this->db->query($_SQry);

        if($result->num_rows() > 0) {
            $this->db->query(sprintf("DELETE FROM mo_agency_code WHERE id_no = '%d'",$data['id_no']));
        }

        $_zoneid_array = json_decode($data['cre_zoneid_site'], true);

        foreach($_zoneid_array as $key => $val) {

            $Qry[$key] = sprintf('("%d", "%s", "%d", "%s", "%s", "%s")', $data['id_no'], $val[0], $data['cre_type'], $val[1], $val[0], date('Y-m-d H:i:s'));
        }

        $_Qry = "INSERT INTO mo_agency_code (id_no, bn_code, adver_type, site_domain, zone_id, regdate) values ".join(',', $Qry);

        return $this->db->query($_Qry);
    }
    
    function creative_group_status_change($data){
    
        $sql = " UPDATE mountain.mo_creative_group SET ";
        if($data['status_key']=='Y'){
            $sql.= " cre_gp_fl= '".$data['status_key']."' ";
        }else{
            $sql.= " cre_gp_status= '".$data['status_key']."' ";
        }
        $sql.= " WHERE cre_gp_no IN (".$data['cre_gp_no_arr'].") ";
        $query = $this->db->query($sql);
    
        if($data['status_key']=='Y'){
            $sql = " UPDATE mountain.mo_creative SET ";
            $sql.= " cre_fl= 'Y' ";
            $sql.= " WHERE cre_gp_no IN (".$data['cre_gp_no_arr'].") ";
            $query= $this->db->query($sql);
        }else{
            $sql = " UPDATE mountain.mo_creative SET ";
            $sql.= " cre_status= '".$data['status_key']."' ";
            $sql.= " WHERE cre_gp_no IN (".$data['cre_gp_no_arr'].") ";
            $query= $this->db->query($sql);
        }
        
        if ($this->db->affected_rows() > 0) {
            return "ok";
        }else{
            return "false";
        }
    }
    
    function creative_status_change($data){
    
        $sql = " UPDATE mountain.mo_creative SET ";
        if($data['status_key']=='Y'){
            $sql.= " cre_fl= '".$data['status_key']."' ";
        }else{
            $sql.= " cre_status= '".$data['status_key']."' ";
        }
        $sql.= " WHERE cre_no IN (".$data['cre_no_arr'].") ";
        $query = $this->db->query($sql);
    
        
        if ($this->db->affected_rows() > 0) {
            return "ok";
        }else{
            return "false";
        }
    }
    
    function update_modify_creative($data, $cre_no){

        $_creative_update = elements(
            array('cre_nm','cre_type','cre_link','cre_width','cre_height','cre_cont','cre_img_link','cre_ymd','cre_status','cre_evaluation'),
            $data
        );

        if($data['cre_type']=='4') {
            $_creative_update['cre_width'] = $data['cre_width_action'];
            $_creative_update['cre_height'] = $data['cre_height_action'];
        }
        elseif($data['cre_type']=='5') { //치타
            $_creative_update['cre_width'] = $data['cre_width_cheetah'];
            $_creative_update['cre_height'] = $data['cre_height_cheetah'];
        }

        if($data['cre_bncode_site']) {
            $data['id_no'] = $cre_no;
            $this->insert_new_creative_auction_bncode($data);
        }
        elseif($data['cre_zoneid_site']) {
            $data['id_no'] = $cre_no;
            $this->insert_new_creative_cheetah_zoneid($data);
        }

        $where = " cre_no = $cre_no ";
        $udt_query = $this->db->update_string('mountain.mo_creative', $_creative_update, $where);
        $this->db->query($udt_query);
        echo "ok";
    }
    
    function update_modify_creative_group($data, $cre_gp_no){
        $where = " cre_gp_no = $cre_gp_no ";
        $udt_query = $this->db->update_string('mountain.mo_creative_group', $data, $where);
        $this->db->query($udt_query);
        echo "ok";
    }

    function update_bid_price($input_date, $cre_gp_no){

        $this->db->select("*");
        $this->db->where("cre_gp_fl", "N");
        $this->db->where("bid_cur !=", "USD");
        $this->db->where("cre_gp_no", $cre_gp_no);
        $this->db->from('mo_creative_group');
        $query = $this->db->get();
        if ( $query->num_rows() > 0 ){
            $cre_gp = $query->result_array();
            foreach($cre_gp as $cg){
                $bid_cur = $cg['bid_cur'];
                $this->db->select("exchange_rate");
                $this->db->where("from_code_key", "USD");
                $this->db->where("to_code_key", $bid_cur);
                $this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
                $this->db->from('mo_exchange');
                $this->db->limit(1);
                $query = $this->db->get();
                foreach ($query->result() as $row){
                    $exchange_rate = $row->exchange_rate;
                }

                if(!isset($exchange_rate) || $exchange_rate == ""){
                    $exchange_rate = $this->get_exchange_("USD", $bid_cur);
                }

                if($cg['bid_loc_price'] > 0) {
                    $bid_price = $cg['bid_loc_price'] / $exchange_rate;
                }else{
                    $bid_price = 0;
                }

                $data = array(
                    'bid_price' => $bid_price
                );

                $this->db->where('cre_gp_no', $cg['cre_gp_no']);
                $this->db->update('mo_creative_group', $data);
            }
        }
    }

    function update_bid_loc_price($cur, $input_date, $cre_gp_no){

        $this->db->select("*");
        $this->db->where("cre_gp_fl", "N");
        $this->db->where("bid_cur", "USD");
        $this->db->where("cre_gp_no", $cre_gp_no);
        $this->db->from('mo_creative_group');
        $query = $this->db->get();
        if ( $query->num_rows() > 0 ){
            $cre_gp = $query->result_array();
            foreach($cre_gp as $cg){
                $bid_cur = $cur; //일단 원화로 계산
                $this->db->select("exchange_rate");
                $this->db->where("from_code_key", "USD");
                $this->db->where("to_code_key", $bid_cur);
                $this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
                $this->db->from('mo_exchange');
                $this->db->limit(1);
                $query = $this->db->get();
                foreach ($query->result() as $row){
                    $exchange_rate = $row->exchange_rate;
                }

                if(!isset($exchange_rate) || $exchange_rate == ""){
                    $exchange_rate = $this->get_exchange_("USD", $bid_cur);
                }

                if($cg['bid_price'] > 0) {
                    $bid_loc_price = $cg['bid_price'] * $exchange_rate;
                }else{
                    $bid_loc_price = 0;
                }

                $data = array(
                    'bid_loc_price' => round($bid_loc_price)
                );

                $this->db->where('cre_gp_no', $cg['cre_gp_no']);
                $this->db->update('mo_creative_group', $data);
            }
        }
    }

    //국제 통화 환율을 업데이트
    // URL/service/get_exchange/USD/KRW
    function get_exchange_($from_currency = null, $to_currency = null){

        if(!isset($from_currency))
            return "";

        if(!isset($to_currency))
            return "";

        $content = file_get_contents('https://www.google.com/finance/converter?a=1&from='.$from_currency.'&to='.$to_currency);
        $doc = new DOMDocument();
        @$doc->loadHTML($content);
        $xpath = new DOMXpath($doc);
        $result = $xpath->query('//*[@id="currency_converter_result"]/span')->item(0)->nodeValue;
        $exchange_rate = str_replace( $to_currency, '', $result);

        return $exchange_rate;
    }
    
    //대시보드 종료예정 광고그룹
    function select_end_date_creative_group($mem_no){
        $sql = " SELECT cre_gp_nm, start_ymd, end_ymd, (TO_DAYS(end_ymd) - TO_DAYS(now())) AS d_day ";
        $sql.= " FROM "; 
        $sql.= " (SELECT cam_no FROM mountain.mo_campaign A, mountain.mo_members B ";
        $sql.= " WHERE A.adver_no = B.mem_no AND B.mem_no='".$mem_no."') A, ";
        $sql.= " mountain.mo_creative_group B ";
        $sql.= " WHERE A.cam_no = B.cam_no AND end_ymd > now() AND B.cre_gp_fl = 'N' ";
        $sql.= " ORDER BY end_ymd LIMIT 5";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }else{
            return array();
        }
    }
    
    //대시보드 광고관리 현황
    function select_creative_all_summary($mem_no){
        $sql = " SELECT IFNULL(A.cam_cnt, 0) AS cam_cnt, IFNULL(B.cre_gp_cnt, 0) AS cre_gp_cnt, IFNULL(C.cre_cnt, 0) AS cre_cnt ";
        $sql.= " FROM ";
        $sql.= " (SELECT COUNT(cam_no) AS cam_cnt, cam_status FROM mo_campaign A, mo_members B ";
        $sql.= " WHERE A.adver_no=B.mem_no AND B.mem_no='".$mem_no."' AND A.cam_fl = 'N') A, ";
        $sql.= " (SELECT COUNT(cre_gp_no) AS cre_gp_cnt, cre_gp_status FROM mo_campaign A, mo_members B, mo_creative_group C ";
        $sql.= " WHERE A.adver_no=B.mem_no AND A.cam_no=C.cam_no AND B.mem_no='".$mem_no."' AND C.cre_gp_fl = 'N') B, ";
        $sql.= " (SELECT COUNT(cre_no) AS cre_cnt, cre_status FROm mo_campaign A, mo_members B, mo_creative_group C, mo_creative D ";
        $sql.= " WHERE A.adver_no=B.mem_no AND A.cam_no=C.cam_no AND C.cre_gp_no = D.cre_gp_no AND B.mem_no='".$mem_no."' AND D.cre_fl = 'N') C ";
        $result = $this->db->query($sql);
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
            return $row[0];
        }
    }
    
    function select_all_cre_evaluation($mem_no){
        $sql = " SELECT A.code_key, B.cre_evaluation from ";
        $sql.= " (SELECT code_key, code_desc from mo_code where code_nm = 'cre_type') A left join ";
        $sql.= " ( ";
        $sql.= " SELECT cre_no,D.cre_evaluation, D.cre_type from ";
        $sql.= " (SELECT mem_no FROM mountain.mo_members WHERE mem_no = '".$mem_no."') A, ";
        $sql.= " (SELECT cam_no, adver_no FROM mountain.mo_campaign) B, ";
        $sql.= " (SELECT cam_no, cre_gp_no FROM mountain.mo_creative_group) C, ";
        $sql.= " (SELECT cre_gp_no, cre_no, cre_evaluation, cre_type FROM mountain.mo_creative ) D ";
        $sql.= " WHERE A.mem_no = B.adver_no AND B.cam_no = C.cam_no AND C.cre_gp_no = D.cre_gp_no) B ";
        $sql.= " ON A.code_key = B.cre_type ";
        $sql.= " WHERE B.cre_evaluation !='' ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }else{
            return array();
        }
    }
    
    function select_creative_summary($mem_no){
        $sql = " SELECT A.code_key, A.code_desc, IFNULL(B.cam_cnt, 0) AS cam_cnt, IFNULL(C.cre_gp_cnt, 0) AS cre_gp_cnt, IFNULL(D.cre_cnt, 0) AS cre_cnt ";
        $sql.= " FROM ";
        $sql.= " (SELECT code_key, code_desc FROM mo_code WHERE code_nm='status' AND code_key<5) A LEFT JOIN ";
        $sql.= " (SELECT COUNT(cam_no) AS cam_cnt, cam_status FROM mo_campaign A, mo_members B ";
        $sql.= " WHERE A.adver_no=B.mem_no AND B.mem_no='".$mem_no."' group by cam_status) B ON A.code_key = B.cam_status LEFT JOIN ";
        $sql.= " (SELECT COUNT(cre_gp_no) AS cre_gp_cnt, cre_gp_status FROM mo_campaign A, mo_members B, mo_creative_group C ";
        $sql.= " WHERE A.adver_no=B.mem_no AND A.cam_no=C.cam_no AND B.mem_no='".$mem_no."' GROUP BY cre_gp_status) C ON A.code_key = C.cre_gp_status LEFT JOIN ";
        $sql.= " (SELECT COUNT(cre_no) AS cre_cnt, cre_status FROm mo_campaign A, mo_members B, mo_creative_group C, mo_creative D ";
        $sql.= " WHERE A.adver_no=B.mem_no AND A.cam_no=C.cam_no AND C.cre_gp_no = D.cre_gp_no AND B.mem_no='".$mem_no."' GROUP BY cre_status ) D ON A.code_key = D.cre_status ";
    
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }else{
            return array();
        }
    }
    
    function cre_gp_nm_check($cre_gp_nm, $cam_no, $cre_gp_no = NULL){
    
        if ((strlen($cre_gp_nm) == 0 )|| ($cre_gp_nm == '')) {
            return "none";
        } else {
            $sql = "SELECT
                        cre_gp_no
                    FROM
                        mountain.mo_creative_group
                    WHERE
                        cam_no = '".$cam_no."' AND cre_gp_nm = '".$cre_gp_nm."' AND cre_gp_fl = 'N' ";
            if ($cre_gp_no != ""){
                $sql.= " AND cre_gp_no != '$cre_gp_no' ";
            }
            $query = $this->db->query($sql);
    
            if ($query->num_rows() > 0) {
                return "false";
            } else {
                return "true";
            }
        }
    }
    
    function cre_nm_check($cre_nm, $cre_gp_no, $cre_no){
    
        if ((strlen($cre_nm) == 0 )|| ($cre_nm == '')) {
            return "none";
        } else {
            $sql = "SELECT
                        cre_no
                    FROM
                        mountain.mo_creative
                    WHERE
                        cre_nm = '".$cre_nm."' AND cre_gp_no = '".$cre_gp_no."' AND cre_fl = 'N' ";
            if ($cre_no != ""){
                $sql.= " AND cre_no != '$cre_no' ";
            }
            $query = $this->db->query($sql);
    
            if ($query->num_rows() > 0) {
                return "false";
            } else {
                return "true";
            }
        }
    }
    
}
<?php
class Member_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }


    function id_check($id) {
    
        if ((strlen($id) == 0 )|| ($id == '')) {
            return "none";
        } else if (strlen($id) < 3) {
            return "short";
        } else if (strlen($id) > 15) {
            return "long";
        } else {
            $sql = "SELECT mem_id FROM mountain.mo_members WHERE mem_id = '" . $id . "'";
            $query = $this->db->query($sql);
    
            if ($query->num_rows() > 0) {
                return "false";
            } else {
                return "true";
            }
        }
    }
    
    function reg_no_check($reg_no) {
        
        $sql = "SELECT mem_id FROM mountain.mo_members WHERE mem_com_no = '" . $reg_no . "'";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return "false";
        } else {
            return "true";
        }
    }
    
    function cert_check($cond) {
        if($cond['kind'] == "join"){
            $sql = " SELECT cert_no FROM mountain.mo_cert ";
            $sql.= " WHERE cert_email = '".$cond['email']."' ";
            $sql.= " AND cert_key = '".$cond['no']."' AND cert_no != '' ";
        }else{
            $sql = "SELECT mem_id FROM mountain.mo_members ";
            if($cond['kind'] == "pwd"){
                $sql.= " WHERE mem_pwd = '".$cond['no']."' ";
            }else{
                $sql.= " WHERE mem_cert_no = '".$cond['no']."' ";
            }
            $sql.= " AND mem_email='".$cond['email']."' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            if ($cond['kind'] == "join"){
                $del_sql = " DELETE FROM mountain.mo_cert WHERE cert_email = '".$cond['email']."' AND cert_key = '".$cond['no']."' ";
                $del_query = $this->db->query($del_sql);
            }
            return "true";
        } else {
            return "false";
        }
        
    }
    
    function check_email($email) {
        $sql = " SELECT mem_id FROM mountain.mo_members ";
        $sql.= " WHERE mem_email='$email' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return "false";
        } else {
            $check_sql = " SELECT cert_no FROM mountain.mo_cert ";
            $check_sql.= " WHERE cert_email='$email' ";
            $check_query = $this->db->query($check_sql);
            if ($check_query->num_rows() > 0) {
                $del_sql = " DELETE FROM mountain.mo_cert WHERE cert_email='$email' ";
                $this->db->query($del_sql);
            }
            return "true";
        }
    }
    
    public function insert_member_info($data){
        $ist_query = $this->db->insert_string('mountain.mo_members', $data);
        $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    public function update_company_info($data,$mem_no){
        $where = " mem_no='$mem_no'";
        $udt_query = $this->db->update_string('mountain.mo_members', $data, $where);
        $this->db->query($udt_query);
        return "true";
    }
    
    public function select_login_info($data){
        $qry = " SELECT * ";
        $qry .= " FROM mountain.mo_members ";
        $qry .= "  WHERE mem_id = '".$data['id']."' AND mem_pwd = '".$data['pass']."' ;";
        $result = $this->db->query($qry);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function insert_login_log($login_data){
        $sql = " INSERT INTO mountain.mo_login_log ";
        $sql.= " (mem_no, login_dt, login_ip) ";
        $sql.= " VALUES ";
        $sql.= " ('".$login_data['mem_no']."', '".date("Y-m-d H:i:s")."', '".$login_data['ip_addr']."') ";
        $this->db->query($sql);

        $data = array(
            'mem_last_login' => date("Y-m-d H:i:s")
        );

        $this->db->where('mem_no', $login_data['mem_no']);
        $this->db->update('mo_members', $data);
    }

    public function get_find_member_step1($data){
        $sql = " SELECT mem_id, LEFT(mem_ymd,10) AS mem_ymd, mem_email, mem_no FROM mountain.mo_members ";
        $sql.= " WHERE mem_com_nm = '".$data['mem_com_nm']."' AND mem_com_no = '".$data['mem_com_no']."' AND mem_fl='N' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
        }else{
            $result="false";
        }
        return $result;
    }
    
    public function get_find_full_id($data){
        $sql = "SELECT mem_id, LEFT(mem_ymd,10) AS mem_ymd FROM mountain.mo_members WHERE mem_cert_no = '".$data['cert_no']."' AND mem_email = '".$data['mem_email']."' AND mem_fl='N' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row[0];
        }else{
            return 'false';
        }
    }
    
    public function get_find_pwd_step1($data){
        $sql = "SELECT mem_email  FROM mountain.mo_members ";
        $sql.= " WHERE mem_id='".$data['mem_id']."' ";
        if ($data['mem_type'] == "company"){ 
            if ($data['mem_com_no']!="--"){
                $sql.= " AND mem_com_no = '".$data['mem_com_no']."' AND mem_fl='N' ";
            }
        }
        $query = $this->db->query($sql);
        $row = $query->result_array();
        return $row[0];
        
    }
    
    public function update_member_cert($mem_email,$cert_no,$kind){
        $cert_dt=date('Y-m-d H:i:s');
        if($kind=="join"){
            $sql = " INSERT INTO mountain.mo_cert (cert_key, cert_email, cert_ymd) VALUES ('$cert_no', '$mem_email', '$cert_dt') ";
        }else{
            $sql = "UPDATE mountain.mo_members SET ";
            if($kind=="pwd"){
                $sql.= " mem_pwd='$cert_no' ";
            }else{
                $sql.= " mem_cert_no='$cert_no' ";
            }
            $sql.= " WHERE mem_email='$mem_email' ";
        }
        $this->db->query($sql);
    }
    
    public function update_member_pwd($mem_email,$mem_pwd){
        $mem_pwd=md5($mem_pwd);
        $sql = "UPDATE mountain.mo_members SET mem_pwd='$mem_pwd' WHERE mem_email='$mem_email' ";
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            return "ok";
        }else{
            return "FALSE";
        }
    }

    public function modify_member_row($data, $where) {
        $sql = $this->db->update_string('mo_members', $data, $where);
        $ret = $this->db->query($sql);
        return $ret;
    }

    public function delete_membre_info($mem_no){
        $sql = " DELETE FROM mountain.mo_members WHERE mem_no = '$mem_no' ";
        $this->db->query($sql);

        $cate_sql = " DELETE FROM mountain.mo_category_member WHERE mem_no = '$mem_no' ";
        $this->db->query($cate_sql);

        $group_sql = " DELETE FROM mountain.mo_members_group WHERE agency_no = '$mem_no' AND adver_no = '$mem_no' ";
        $this->db->query($group_sql);
    }
    
}

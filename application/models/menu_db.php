<?php
class Menu_Db extends CI_Model {

    public function __construct() {
        parent::__construct();
    }
    
    function get_first_category(){
        
        $mem_no = $this->session->userdata('mem_no');
        
        $this->db->select("mo_category.cate_no");
        $this->db->where('mo_category_member.mem_no', $mem_no);
        $this->db->where('mo_category.cate_parent_no', '0');
        $this->db->where('mo_category.cate_fl', 'N');
        $this->db->from('mo_category');
        $this->db->join('mo_category_member', 'mo_category_member.cate_no = mo_category.cate_no');
        $this->db->order_by("mo_category.cate_seq", "ASC");
        $this->db->limit("1");
        
        $query = $this->db->get();
        
        if ( $query->num_rows() > 0 ){
            //$main_menu = $query->result_array();
            
            foreach ($query->result() as $row) {
                $this->db->select("mo_category.cate_url");
                $this->db->where('mo_category.cate_parent_no', $row->cate_no);
                $this->db->where('mo_category_member.mem_no', $mem_no);
                $this->db->where('mo_category.cate_fl', 'N');
                $this->db->where('mo_category_member.read_fl', 'Y');
                $this->db->from('mo_category');
                $this->db->join('mo_category_member', 'mo_category_member.cate_no = mo_category.cate_no');
                $this->db->order_by("mo_category.cate_seq", "ASC");
                $this->db->limit("1");
                $query = $this->db->get();
                
                //print_r($this->db->last_query());
                $cate_url = $query->result_array();
            }

            return $cate_url[0]['cate_url'];
        }
    }

    function get_main_category() {

        $mem_no = $this->session->userdata('mem_no');

        $sql = "SELECT A.* FROM mountain.mo_category A LEFT JOIN mountain.mo_category_member B ON A.cate_no = B.cate_no WHERE B.mem_no = '" . $mem_no . "' AND A.cate_parent_no = '0' AND A.cate_fl = 'N' AND B.read_fl = 'N' ORDER BY A.cate_seq ASC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {

                $sub_sql = "SELECT * FROM mountain.mo_category_member A LEFT JOIN mountain.mo_category B ON A.cate_no = B.cate_no WHERE A.cate_parent_no = '".$row->cate_no."' and A.read_fl = 'Y' AND A.mem_no = '".$mem_no."' order by B.cate_seq asc";
                $sub_query = $this->db->query($sub_sql);

                if($sub_query->num_rows() > 0){

                    foreach ($sub_query->result_array() as $key=>$sub_row) {
                        if($key == 0){
                            $cate_url = $sub_row['cate_url'];
                        }
                    }
                    $row->cate_url = $cate_url;
                    $data[] = $row;
                }
            }
        }

        return $data;
    }

    function get_sub_category($cate_parent_no) {

        $mem_no = $this->session->userdata('mem_no');

        $sql = "SELECT A.* FROM mountain.mo_category A LEFT JOIN mountain.mo_category_member B ON A.cate_no = B.cate_no WHERE B.mem_no = '".$mem_no."' AND A.cate_parent_no = '".$cate_parent_no."' AND A.cate_fl = 'N' AND B.read_fl = 'Y' ORDER BY A.cate_seq ASC";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    
    function get_sub_menu($cate_url) {
        
        $mem_no = $this->session->userdata('mem_no');
        
        $sql = " SELECT A.* ";
        $sql.= " FROM mountain.mo_category A LEFT JOIN mountain.mo_category_member B ON A.cate_no = B.cate_no ";
        $sql.= " WHERE B.mem_no = '".$mem_no."' AND A.cate_parent_no=(SELECT cate_parent_no FROM mountain.mo_category WHERE cate_url = '".$cate_url."' AND cate_parent_no != '0') ";
        $sql.= " AND A.cate_fl = 'N' AND B.read_fl = 'Y' ORDER BY A.cate_seq ASC";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    
    function get_main_menu($cate_url) {

        $sql = " SELECT * ";
        $sql.= " FROM mountain.mo_category ";
        $sql.= " WHERE cate_no = ( SELECT cate_parent_no FROM mountain.mo_category WHERE cate_url='".$cate_url."' AND cate_parent_no != '0') ORDER BY cate_seq ASC  ";
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    
    function get_url_category($url) {

        $sql = "SELECT * FROM mountain.mo_category WHERE cate_url = '".$url."' ORDER BY cate_seq ASC ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
           $result = $this->db->query($sql);
            $row = $result->row();
            return $row;
        }
    }

    function get_auth_category($url) {

        $mem_no = $this->session->userdata('mem_no');

        $sql = "SELECT A.*, B.read_fl, B.write_fl ";
        $sql.= " FROM mountain.mo_category A LEFT JOIN mountain.mo_category_member B ON A.cate_no = B.cate_no ";
        $sql.= " WHERE B.mem_no = '".$mem_no."' AND B.cate_no = A.cate_no AND A.cate_fl = 'N' AND B.read_fl = 'Y' AND A.cate_url = '".$url."' ORDER BY A.cate_seq ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function get_category_template_info($group_no) {
        $sql = " SELECT A.cate_parent_no, A.cate_no, A.cate_nm, B.cate_temp_no, B.read_fl, B.write_fl, B.group_nm, B.admin_fl, B.fee_fl, B.role FROM mountain.mo_category A LEFT JOIN mountain.mo_category_template B ON A.cate_no = B.cate_no ";
        $sql.= " WHERE A.cate_fl = 'N' AND B.group_no = '".$group_no."' ORDER BY A.cate_seq ASC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function get_category_list() {
        $sql = "select * FROM mountain.mo_category where cate_fl = 'N' order by cate_seq asc";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function get_all_category_list() {
        $sql = "select * FROM mountain.mo_category order by cate_seq asc";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function get_category_template_name($group_no) {
        $sql = " SELECT group_nm FROM mountain.mo_category_template WHERE group_no = '".$group_no."' LIMIT 1 ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data->group_nm;
        }
    }

    function insert_category_template($role, $fee_fl, $admin_fl, $read_fl, $write_fl, $temp_group_nm) {

        $sql = "SELECT MAX(group_no) AS group_no FROM mo_category_template";
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row();
            $group_no = $data->group_no;
        }

        $group_no = $group_no + 1;

        if ($group_no > 0) {
            $sql = "SELECT cate_no, cate_parent_no, cate_fl FROM mountain.mo_category ORDER BY cate_no ASC";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $sql = "INSERT INTO mountain.mo_category_template( role, fee_fl, admin_fl, group_no, group_nm, cate_no, cate_parent_no, read_fl, write_fl, del_fl) VALUES ('" . $role . "','" . $fee_fl . "','" . $admin_fl . "','" . $group_no . "', '" . $temp_group_nm . "', '" . $row['cate_no'] . "', '" . $row['cate_parent_no'] . "', 'N', 'N', '" . $row['cate_fl'] . "')";
                    $this->db->query($sql);
                }
            }
            if($read_fl != ""){
                $sql = "UPDATE mountain.mo_category_template SET read_fl='Y' WHERE cate_no IN (".$read_fl.") AND group_no = '".$group_no."'";
                $this->db->query($sql);
            }
            if($write_fl != ""){
                $sql = "UPDATE mountain.mo_category_template SET write_fl='Y' WHERE cate_no IN (".$write_fl.") AND group_no = '".$group_no."'";
                $this->db->query($sql);
            }
            return true;
        }
    }

    function update_category_template($role, $fee_fl, $admin_fl, $read_fl, $write_fl, $group_no, $group_nm) {

        $sql = "UPDATE mountain.mo_category_template SET read_fl='N' WHERE cate_no NOT IN (".$read_fl.") AND group_no = '".$group_no."' AND cate_parent_no > 0";
        $this->db->query($sql);

        $sql = "UPDATE mountain.mo_category_template SET write_fl='N' WHERE cate_no NOT IN (".$write_fl.") AND group_no = '".$group_no."' AND cate_parent_no > 0";
        $this->db->query($sql);

        $sql = "UPDATE mountain.mo_category_template SET read_fl='Y' WHERE cate_no IN (".$read_fl.") AND group_no = '".$group_no."'";
        $this->db->query($sql);

        $sql = "UPDATE mountain.mo_category_template SET write_fl='Y' WHERE cate_no IN (".$write_fl.") AND group_no = '".$group_no."'";
        $this->db->query($sql);

        $sql = "UPDATE mountain.mo_category_template SET group_nm ='".$group_nm."', admin_fl = '".$admin_fl."', role = '".$role."', fee_fl = '".$fee_fl."'  WHERE group_no = '".$group_no."'";
        $this->db->query($sql);

        $sql = "UPDATE mountain.mo_members SET group_nm ='".$group_nm."', admin_fl = '".$admin_fl."', role = '".$role."', fee_fl = '".$fee_fl."'  WHERE group_no = '".$group_no."'";
        $this->db->query($sql);

        $sql = "SELECT mem_no FROM mountain.mo_members WHERE group_no='".$group_no."' AND mem_fl = 'N' ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $sel_mem_no[] = $row;
            }
        }

        $sql="SELECT cate_no, cate_parent_no, read_fl, write_fl, del_fl FROM mountain.mo_category_template WHERE group_no='".$group_no."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $sel_template[]=$row;
            }
        }
        if(isset($sel_mem_no) && isset($sel_template)){
            foreach ($sel_template as $temp){
                $cate_no = $temp['cate_no'];
                $cate_parent_no = $temp['cate_parent_no'];
                $read_fl = $temp['read_fl'];
                $write_fl = $temp['write_fl'];
                $del_fl = $temp['del_fl'];
                foreach ($sel_mem_no as $mem){
                    $mem_no = $mem['mem_no'];
                    $sql = "UPDATE mountain.mo_category_member SET read_fl='".$read_fl."', write_fl='".$write_fl."', del_fl='".$del_fl."' ";
                    $sql.= " WHERE cate_no = '".$cate_no."' AND cate_parent_no = '".$cate_parent_no."' AND mem_no = '".$mem_no."'";
                    $this->db->query($sql);
                }
            }
        }

        return true;
    }

    function delete_category_template($group_no) {
        $sql = "SELECT * FROM mountain.mo_category_template WHERE group_no = '".$group_no."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $query = "DELETE FROM mountain.mo_category_template WHERE group_no = '".$group_no."'";
            $this->db->query($query);
        }
        return true;
    }

    function select_category_template() {
        $sql = "SELECT group_no, group_nm FROM mountain.mo_category_template GROUP BY group_no ORDER BY group_no ASC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function get_category_member($mem_no) {
        $sql = "SELECT A.cate_parent_no, A.cate_nm, A.cate_no, B.cate_mem_no, B.read_fl, B.write_fl FROM mountain.mo_category A LEFT JOIN mountain.mo_category_member B ON A.cate_no = B.cate_no WHERE A.cate_fl = 'N' AND B.mem_no = '". $mem_no ."' ORDER BY A.cate_seq ASC";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function insert_category_member($group_no, $mem_no) {

        $sql = "DELETE FROM mountain.mo_category_member WHERE mem_no = '".$mem_no."'";
        $ret = $this->db->query($sql);
        if ($ret == true) {
            $sql = "SELECT role, fee_fl, admin_fl, group_no, group_nm, cate_no, cate_parent_no, read_fl, write_fl, del_fl FROM mountain.mo_category_template WHERE group_no = '".$group_no."' ORDER BY cate_temp_no ASC";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $sql = "INSERT INTO mountain.mo_category_member( cate_no, cate_parent_no, mem_no, read_fl, write_fl, del_fl) VALUES ('".$row['cate_no']."', '".$row['cate_parent_no']."', '".$mem_no. "', '".$row['read_fl']."', '".$row['write_fl']."', '".$row['del_fl']."')";
                    $this->db->query($sql);
                    $admin_fl = $row['admin_fl'];
                    $fee_fl = $row['fee_fl'];
                    $role = $row['role'];
                    $group_no = $row['group_no'];
                    $group_nm = $row['group_nm'];
                }
                $sql = "UPDATE mountain.mo_members SET admin_fl = '".$admin_fl."', fee_fl = '".$fee_fl."', role = '".$role."', group_no = '".$group_no."', group_nm = '".$group_nm."' WHERE mem_no = '".$mem_no."'";
                $this->db->query($sql);
            }
            return true;
        }
    }

    function update_category_member($mem_com_fee, $read_fl, $write_fl, $mem_no) {

        if ($read_fl == '') {
            $sql = "UPDATE mountain.mo_category_member SET read_fl='N' WHERE mem_no = '".$mem_no."' AND cate_parent_no > 0";
            $this->db->query($sql);
            $sql = "UPDATE mountain.mo_category_member SET read_fl='N' WHERE mem_no = '".$mem_no."'";
            $this->db->query($sql);
        } else {
            $sql = "UPDATE mountain.mo_category_member SET read_fl='N' WHERE cate_no NOT IN (".$read_fl.") AND mem_no = '".$mem_no."' AND cate_parent_no > 0";
            $this->db->query($sql);
            $sql = "UPDATE mountain.mo_category_member SET read_fl='Y' WHERE cate_no IN (".$read_fl.") AND mem_no = '".$mem_no."'";
            $this->db->query($sql);
        }

        if ($write_fl == '') {
            $sql = "UPDATE mountain.mo_category_member SET write_fl='N' WHERE mem_no = '".$mem_no."' AND cate_parent_no > 0";
            $this->db->query($sql);
            $sql = "UPDATE mountain.mo_category_member SET write_fl='N' WHERE mem_no = '".$mem_no."'";
            $this->db->query($sql);
        } else {
            $sql = "UPDATE mountain.mo_category_member SET write_fl='N' WHERE cate_no NOT IN (".$write_fl.") AND mem_no = '".$mem_no."' AND cate_parent_no > 0";
            $this->db->query($sql);
            $sql = "UPDATE mountain.mo_category_member SET write_fl='Y' WHERE cate_no IN (".$write_fl.") AND mem_no = '".$mem_no."'";
            $this->db->query($sql);
        }

        if($mem_com_fee > 0){
            $sql = "UPDATE mountain.mo_members SET mem_com_fee='".$mem_com_fee."' WHERE mem_no = '".$mem_no."'";
            $this->db->query($sql);
        }

        return true;
    }

    function insert_category_info($data) {
        $cate_parent_no = $data['cate_parent_no'];
        $cate_fl = $data['cate_fl'];
        $cate_nm = $data['cate_nm'];

        $sql = $this->db->insert_string('mountain.mo_category', $data);
        $this->db->query($sql);
        $insert_id = $this->db->insert_id();

        $cate_no = "SELECT cate_no FROM mountain.mo_category WHERE cate_nm = '".$cate_nm."'";
        $query = $this->db->query($cate_no);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $cate_no = $row->cate_no;
            }
        }
        $sql = "SELECT group_no, group_nm FROM mountain.mo_category_template GROUP BY group_no";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach($query->result_array() as $row){
                $cate_temp[] = $row;
            }
            foreach($cate_temp as $temp){
                $group_no = $temp['group_no'];
                $group_nm = $temp['group_nm'];
                $sql = " INSERT INTO mountain.mo_category_template(group_no, group_nm, cate_no, cate_parent_no, read_fl, write_fl, del_fl) ";
                $sql.= " VALUES ";
                $sql.= " ('".$group_no."', '".$group_nm."', '".$cate_no."', '".$cate_parent_no."', 'N', 'N', '".$cate_fl."')";
                $this->db->query($sql);
            }
        }
        $sql = "SELECT mem_no FROM mountain.mo_category_member GROUP BY mem_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $cate_mem[] = $row;
            }
            foreach ($cate_mem as $mem) {
                $mem_no = $mem['mem_no'];
                $sql = " INSERT INTO mountain.mo_category_member (cate_no, cate_parent_no, mem_no, read_fl, write_fl, del_fl) ";
                $sql.= " VALUES ";
                $sql.= " ('".$cate_no."', '".$cate_parent_no."', '".$mem_no."', 'N', 'N', '".$cate_fl."')";
                $this->db->query($sql);
            }
        }
        return $insert_id;
    }

    function update_category_info($data, $where, $cate_no) {
        $cate_parent_no = $data['cate_parent_no'];
        $cate_fl = $data['cate_fl'];

        $sql = " UPDATE mountain.mo_category_member SET cate_parent_no = '".$cate_parent_no."', del_fl = '".$cate_fl."' WHERE cate_no ='".$cate_no."' ";
        $this->db->query($sql);

        $sql = " UPDATE mountain.mo_category_template SET cate_parent_no = '".$cate_parent_no."', del_fl = '".$cate_fl."' WHERE cate_no ='".$cate_no."' ";
        $this->db->query($sql);

        $sql = $this->db->update_string('mountain.mo_category', $data, $where);

        $query = $this->db->query($sql);
        return $query;
    }
}
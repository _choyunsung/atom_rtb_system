<?php
class Report_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /*
    public function select_report_info($mem_no){
        $sql = " SELECT 'strDailyAverageUseCash' AS kind , AVG(B.sum_cash) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history A, ";
        $sql.= " (SELECT SUM(userd_cash) AS sum_cash FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND cash_dt between DATE_FORMAT(now()- interval 7 day , '%Y-%m-%d') AND DATE_FORMAT(now(), '%Y-%m-%d') ";
        $sql.= " GROUP BY cash_dt) B ";
        $sql.= " UNION ALL ";
        $sql.= " SELECT 'strYesterDayUseCash', IFNULL(SUM(userd_cash),0) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND  cash_dt = DATE_FORMAT(now()- interval 1 day , '%Y-%m-%d') ";
        $sql.= " UNION ALL ";
        $sql.= " SELECT 'strTodayUseCash', IFNULL(SUM(userd_cash),0) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND cash_dt = DATE_FORMAT(now(), '%Y-%m-%d') ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    */

    function select_report_count($data_type, $data_key, $from_date, $to_date){
        $this->db->select("*");

        if($data_type == "member"){
            $this->db->from('mo_report_member');
            $this->db->where('mem_no', $data_key);
        }elseif($data_type == "campaign"){
            $this->db->from('mo_report_campaign');
            $this->db->where('cam_no', $data_key);
        }elseif($data_type == "creative_group"){
            $this->db->from('mo_report_creative_group');
            $this->db->where('cre_gp_no', $data_key);
        }elseif($data_type == "creative"){
            $this->db->from('mo_report_creative');
            $this->db->where('cre_no', $data_key);
        }
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }


    function select_report_list($data_type, $data_key, $from_date, $to_date, $per_page, $page_num){

        $this->db->select("A.date_ymd, sum(A.imp_cnt) imp_cnt, sum(A.click_cnt) click_cnt, sum(A.loc_price) loc_price, sum(A.price) price");

        if($data_type == "member"){
            $this->db->from('mo_report_member A');
            //$this->db->join('mo_members B', 'A.mem_no = B.mem_no');
            $this->db->where('A.mem_no', $data_key);

        }elseif($data_type == "campaign"){
            //$this->db->select("cam_nm, daily_budget");
            $this->db->from('mo_report_campaign A');
            //$this->db->join('mo_campaign B', 'A.cam_no = B.cam_no');
            $this->db->where('A.cam_no', $data_key);

        }elseif($data_type == "creative_group"){
            //$this->db->select("cre_gp_nm, bid_type, bid_price, bid_loc_price, daily_budgey");
            $this->db->from('mo_report_creative_group A');
            //$this->db->join('mo_creative_group B', 'A.cre_gp_no = B.cre_gp_no');
            $this->db->where('A.cre_gp_no', $data_key);

        }elseif($data_type == "creative"){
            //$this->db->select("cre_nm, cre_width, cre_height");
            $this->db->from('mo_report_creative A');
            //$this->db->join('mo_creative B', 'A.cre_no = B.cre_no');
            $this->db->where('A.cre_no', $data_key);
        }
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
        $this->db->group_by("date_ymd");
        $this->db->limit($per_page, $page_num);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_report_integration_list($mem_no, $term, $from_date, $to_date){
    
        //멤버아이디 밑에 광고주들의 총 광고들
        /*
        $this->db->select("adver_no");
        $this->db->from('mo_members_group');
        $this->db->where('agency_no', $mem_no);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $adver_no_temp = $query->result_array();      
            
            foreach($adver_no_temp as $c){
                $adver_no[] = $c['adver_no'];
            }
        }
        */
        $this->db->select("date_ymd, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(loc_price) loc_price, sum(price) price");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_report_member');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
        
        if($term == "day"){
            $this->db->group_by("day");
        }else if($term == "week"){
            $this->db->group_by("week");
        }else if($term == "month"){
            $this->db->group_by("month");
        }
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_report_daily_list($data, $term, $from_date, $to_date){
    
        if($data['datatype'] == 'group'){
            $this->db->select("sales_nm, group_role, group_nm, mem_com_nm, role, date_ymd, imp_cnt, click_cnt, loc_price, price, mem_type");
        }else{
            $this->db->select("sales_nm, group_role, group_nm, mem_com_nm, role, date_ymd, sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(loc_price) loc_price, sum(price) price");
        }
        
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        
        $this->db->from('mo_report_member');
        //$this->db->where_in('mem_no', $adver_no);
        
        if($data['datatype'] == "role"){
            $this->db->where('group_role', $data['datakey']);
        }else if($data['datatype'] == "role_kind"){
            $this->db->where('group_no', $data['datakey']);
        }else if($data['datatype'] == "sales"){
            $this->db->where('sales_nm', $data['datakey']);
        }else if($data['datatype'] == "sales_kind"){     
            $this->db->where('group_no', $data['datakey']);
        }else if($data['datatype'] == "group"){
            $this->db->where('mem_no', $data['datakey']);
        }
        
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($term == "day"){
            $this->db->group_by("day");
        }else if($term == "week"){
            $this->db->group_by("week");
        }else if($term == "month"){
            $this->db->group_by("month");
        }
    
        $query = $this->db->get();
        
        //print_r($this->db->last_query());
        
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_report_business_list($data, $from_date, $to_date){
    
        if($data['datatype'] == "time"){
            $this->db->select("date_ymd");
        }else if($data['datatype'] == "role"){
            $this->db->select("group_role role");
        }else if($data['datatype'] == "role_kind"){
            $this->db->select("MIN(mem_no) mem_no, group_nm mem_com_nm, group_role role");
        }else if($data['datatype'] == "group"){
            $this->db->select("mem_id, mem_no, mem_com_nm, mem_type");
        }
        
        if($data['datatype'] == "group"){        
            $this->db->select("sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(loc_price) loc_price, sum(price) price");
            $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        }else{
           $this->db->select("sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(loc_price) loc_price, sum(price) price");
            $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        }
        
        $this->db->from('mo_report_member');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);


        if($data['datatype'] == "time"){
            if($term == "day"){
                $this->db->group_by("day");
            }else if($term == "week"){
                $this->db->group_by("week");
            }else if($term == "month"){
                $this->db->group_by("month");
            }
        }else if($data['datatype'] == "role"){
            $this->db->group_by("group_role");     
        }else if($data['datatype'] == "role_kind"){   
            $this->db->where('group_role', $data['datakey']);
            $this->db->group_by("group_no");
        }else if($data['datatype'] == "group"){
            $this->db->where('group_no', $data['datakey']);
            $this->db->group_by("mem_no");
        }
        
        
        if($data['datatype'] == "role" || $data['datatype'] == "role_kind"){
            $this->db->order_by("price", "desc");
        }
    
        $query = $this->db->get();
        
        //print_r($this->db->last_query());
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_report_sales_list($data, $from_date, $to_date){
   
        if($data['datatype'] == "time"){
            $this->db->select("date_ymd");
        }else if($data['datatype'] == "sales"){
            $this->db->select("sales_no, sales_nm");
        }else if($data['datatype'] == "sales_kind"){
            $this->db->select("mem_no, group_nm mem_com_nm, group_role role");
        }
     
        $this->db->select("sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(loc_price) loc_price, sum(price) price");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_report_member');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
    
        if($data['datatype'] == "time"){
            if($term == "day"){
                $this->db->group_by("day");
            }else if($term == "week"){
                $this->db->group_by("week");
            }else if($term == "month"){
                $this->db->group_by("month");
            }
        }else if($data['datatype'] == "sales"){
            $this->db->group_by("sales_no");
        }else if($data['datatype'] == "sales_kind"){
            $this->db->where('sales_nm', $data['datakey']);
            $this->db->group_by("group_no");
        }
    
    
        if($data['datatype'] == "sales" || $data['datatype'] == "sales_kind"){
            $this->db->order_by("price", "desc");
        }
    
        //print_r($this->db->last_query());
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_report_info($data_type, $data_key){

        $this->db->select("*");

        if($data_type == "member"){
            $this->db->from('mo_members');
            $this->db->where('mem_no', $data_key);

        }elseif($data_type == "campaign"){
            $this->db->from('mo_campaign');
            $this->db->where('cam_no', $data_key);

        }elseif($data_type == "creative_group"){
            $this->db->from('mo_creative_group');
            $this->db->where('cre_gp_no', $data_key);

        }elseif($data_type == "creative"){
            $this->db->from('mo_creative');
            $this->db->where('cre_no', $data_key);
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_adver_list($mem_no){
        $this->db->select("B.*");
        $this->db->from('mo_members_group A');
        $this->db->join('mo_members B', 'A.agency_no = B.mem_no');
        $this->db->where('B.mem_no', $mem_no);
        $query = $this->db->get();
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    function select_report_member_list($mem_no, $from_date, $to_date){
        
        $logged_in = $this->session->userdata('logged_in');
 
        $this->db->select("A.mem_no, A.mem_com_nm, sum(A.imp_cnt) imp_cnt, sum(A.click_cnt) click_cnt, sum(A.loc_price) loc_price, sum(A.price) price, B.mem_com_nm mem_com_nm_, B.mem_active_st");
        $this->db->from('mo_report_member A');
        $this->db->join('mo_members B', 'A.mem_no = B.mem_no');
        
        if($logged_in['mem_type'] == "master"){
            $this->db->join('mo_members_group C', 'C.adver_no = B.mem_no');
            $this->db->where('C.agency_no', $mem_no);
        }else if($logged_in['mem_type'] == "manager"){
            $this->db->where('B.mem_no', $mem_no);   
        }
        
        $this->db->where('A.date_ymd >=', $from_date);
        $this->db->where('A.date_ymd <=', $to_date);
        $this->db->group_by("A.mem_no");
        $this->db->order_by("B.mem_com_nm", "asc");
        //$this->db->limit($per_page.",".$page_num);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    function select_report_campaign_view($mem_no, $from_date, $to_date){
        $this->db->select("A.cam_no, B.cam_fl, A.cam_nm, sum(A.imp_cnt) imp_cnt, sum(A.click_cnt) click_cnt, sum(A.loc_price) loc_price, sum(A.price) price, B.cam_nm cam_nm_, B.cam_status");
        $this->db->from('mo_report_campaign A');
        $this->db->join('mo_campaign B', 'A.cam_no = B.cam_no');
        $this->db->where('A.mem_no', $mem_no);
        $this->db->where('A.date_ymd >=', $from_date);
        $this->db->where('A.date_ymd <=', $to_date);
        $this->db->group_by("A.cam_no");
        $this->db->order_by("B.cam_nm", "asc");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    function select_report_creative_group_view($cam_no, $from_date, $to_date){
        $this->db->select("A.cam_no, B.cre_gp_fl, A.cre_gp_no, A.cre_gp_nm, sum(A.imp_cnt) imp_cnt, sum(A.click_cnt) click_cnt, sum(A.loc_price) loc_price, sum(A.price) price, B.cre_gp_nm cre_gp_nm_, B.cre_gp_status");
        $this->db->from('mo_report_creative_group A');
        $this->db->join('mo_creative_group B', 'A.cre_gp_no = B.cre_gp_no');
        $this->db->where('A.cam_no', $cam_no);
        $this->db->where('A.date_ymd >=', $from_date);
        $this->db->where('A.date_ymd <=', $to_date);
        $this->db->group_by("A.cre_gp_no");
        $this->db->order_by("B.cre_gp_nm", "asc");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    function select_report_creative_view($cre_gp_no, $from_date, $to_date){
        $this->db->select("A.cre_gp_no, B.cre_fl, A.cre_no, A.cre_nm, sum(A.imp_cnt) imp_cnt, sum(A.click_cnt) click_cnt, sum(A.loc_price) loc_price, sum(A.price) price, B.cre_nm cre_nm_, B.cre_status");
        $this->db->from('mo_report_creative A');
        $this->db->join('mo_creative B', 'A.cre_no = B.cre_no');
        $this->db->where('A.cre_gp_no', $cre_gp_no);
        $this->db->where('A.date_ymd >=', $from_date);
        $this->db->where('A.date_ymd <=', $to_date);
        $this->db->group_by("A.cre_no");
        $this->db->order_by("B.cre_nm", "asc");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    function select_report_detail_mem_nm($data_type, $data_key){
    	if($data_type == "campaign"){
    		$this->db->select("B.mem_com_nm");
    		$this->db->from("mo_campaign A");
    		$this->db->join('mo_members B', 'A.adver_no = B.mem_no');
    		$this->db->where('A.cam_no', $data_key);
    		$query = $this->db->get();
    		$row = $query->row();
    		return $row->mem_com_nm;
    	}else if($data_type == "creative_group"){
    		$this->db->select("B.mem_com_nm");
    		$this->db->from("mo_campaign A");
    		$this->db->join('mo_members B', 'A.adver_no = B.mem_no');
    		$this->db->join('mo_creative_group C', 'C.cam_no = A.cam_no');
    		$this->db->where('C.cre_gp_no', $data_key);
    		$query = $this->db->get();
    		$row = $query->row();
    		return $row->mem_com_nm;
    	}else if($data_type == "creative"){
    		$this->db->select("B.mem_com_nm");
    		$this->db->from("mo_campaign A");
    		$this->db->join('mo_members B', 'A.adver_no = B.mem_no');
    		$this->db->join('mo_creative_group C', 'C.cam_no = A.cam_no');
    		$this->db->join('mo_creative D', 'C.cre_gp_no = D.cre_gp_no');
    		$this->db->where('D.cre_no', $data_key);
    		$query = $this->db->get();
    		$row = $query->result_array();
    		return $row[0]['mem_com_nm'];
    	}
    }
    
    function select_report_detail_cam_nm($data_type, $data_key){
    	if($data_type == "creative_group"){
    		$this->db->select("A.cam_nm");
    		$this->db->from("mo_campaign A");
    		$this->db->join('mo_creative_group B', 'B.cam_no = A.cam_no');
    		$this->db->where('B.cre_gp_no', $data_key);
    		$query = $this->db->get();
    		$row = $query->row();
    		return $row->cam_nm;
    	}else if($data_type == "creative"){
    		$this->db->select("A.cam_nm");
    		$this->db->from("mo_campaign A");
    		$this->db->join('mo_creative_group B', 'B.cam_no = A.cam_no');
    		$this->db->join('mo_creative C', 'C.cre_gp_no = B.cre_gp_no');
    		$this->db->where('C.cre_no', $data_key);
    		$query = $this->db->get();
    		$row = $query->result_array();
    		return $row[0]['cam_nm'];
    	}
    }
    
    function select_report_detail_cre_gp_nm($data_type, $data_key){
    	$this->db->select("A.cre_gp_nm");
    	$this->db->from("mo_creative_group A");
    	$this->db->join('mo_creative B', 'B.cre_gp_no = A.cre_gp_no');
    	$this->db->where('B.cre_no', $data_key);
    	$query = $this->db->get();
    	$row = $query->result_array();
    	return $row[0]['cre_gp_nm'];
    }
    
    function select_mobile_report_member_list($cond){
        $this->db->select("A.mem_no, A.mem_com_nm, sum(A.imp_cnt) imp_cnt, sum(A.click_cnt) click_cnt, sum(A.loc_price) loc_price, sum(A.price) price, B.mem_com_nm mem_com_nm_, B.mem_active_st");
        $this->db->from('mo_report_member A');
        $this->db->join('mo_members B', 'A.mem_no = B.mem_no');
        $this->db->join('mo_members_group C', 'C.adver_no = B.mem_no');
        $this->db->where('C.agency_no', $cond['mem_no']);
        if ($cond['range'] == "today" || $cond['range'] == "yesterday"){
        $this->db->where('A.date_ymd =', $cond['from_date']);
        }else{
            $this->db->where('A.date_ymd >=', $cond['from_date']);
            $this->db->where('A.date_ymd <=', $cond['to_date']);
        }
        $this->db->group_by("A.mem_no");
        $this->db->order_by("B.mem_com_nm", "asc");
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    //광고주 리스트
    function select_advertiser_list($mem_no){
        
        $logged_in = $this->session->userdata('logged_in');
        
        $this->db->select("A.mem_no, A.mem_com_nm");
        $this->db->from('mo_members A');
        
        if($logged_in['mem_type'] == "master"){
            $this->db->join('mo_members_group B', 'B.adver_no = A.mem_no');
            $this->db->where('B.agency_no', $mem_no);
            $this->db->group_by("A.mem_no");
            $this->db->order_by("A.mem_com_nm", "asc");
        }else if($logged_in['mem_type'] == "manager"){
            $this->db->where('A.mem_no', $mem_no);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_campaign_list($adver_no){
        $this->db->select("cam_no, cam_nm");
        $this->db->from('mo_campaign');
        $this->db->where('adver_no', $adver_no);
        $this->db->order_by("cam_nm", "asc");
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_creative_group_list($cam_no){
        $this->db->select("cre_gp_no, cre_gp_nm");
        $this->db->from('mo_creative_group');
        $this->db->where('cam_no', $cam_no);
        $this->db->order_by("cre_gp_nm", "asc");
        $query = $this->db->get();
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_creative_list($cre_gp_no){
        $this->db->select("cre_no, cre_nm");
        $this->db->from('mo_creative');
        $this->db->where('cre_gp_no', $cre_gp_no);
        $this->db->order_by("cre_nm", "asc");
        $query = $this->db->get();
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_report_targeting_count($targeting, $data_type, $data_key, $from_date, $to_date){
        if($data_key == ""){
            $data_key = $this->session->userdata('mem_no');
        }
        
        if($data_type == "targeting"){
            //멤버아이디 밑에 광고주들의 총 광고들
            $this->db->select("adver_no");
            $this->db->from('mo_members_group');
            $this->db->where('agency_no', $data_key);
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                $adver_no_temp = $query->result_array();      
                
                foreach($adver_no_temp as $c){
                    $adver_no[] = $c['adver_no'];
                }
                
                $this->db->select("cam_no");
                $this->db->from('mo_campaign');
                $this->db->where_in('adver_no', $adver_no);
                $query = $this->db->get();
                
                if ($query->num_rows() > 0) {
                    $cam_no_temp = $query->result_array();
                    
                    foreach($cam_no_temp as $c){
                        $cam_no[] = $c['cam_no'];
                    }
                    
                    $this->db->select("cre_gp_no");
                    $this->db->from('mo_creative_group');
                    $this->db->where_in('cam_no', $cam_no);
                    $query = $this->db->get();
                    
                    if ($query->num_rows() > 0) {
                        $cre_gp_no_temp = $query->result_array();
                    
                        foreach($cre_gp_no_temp as $c){
                            $cre_gp_no[] = $c['cre_gp_no'];
                        }
                        
                        $this->db->select("cre_no");
                        $this->db->from('mo_creative');
                        $this->db->where_in('cre_gp_no', $cre_gp_no);
                        $query = $this->db->get();
                        
                        if ($query->num_rows() > 0) {
                            $cre_no_temp = $query->result_array();
                            
                            foreach($cre_no_temp as $c){
                                $cre_no[] = $c['cre_no'];
                            }
                        }
                    }
                } 
            }
        }elseif($data_type == "advertiser"){
            //해당 광고주 하위 광고만
            $this->db->select("cam_no");
            $this->db->from('mo_campaign');
            $this->db->where('adver_no', $data_key);
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                $cam_no_temp = $query->result_array();
                
                foreach($cam_no_temp as $c){
                    $cam_no[] = $c[cam_no];
                }

                $this->db->select("cre_gp_no");
                $this->db->from('mo_creative_group');
                $this->db->where_in('cam_no', $cam_no);
                $query = $this->db->get();
                //print_r($this->db->last_query());
            
                if ($query->num_rows() > 0) {
                    $cre_gp_no_temp = $query->result_array();
            
                    foreach($cre_gp_no_temp as $c){
                        $cre_gp_no[] = $c['cre_gp_no'];
                    }
                    
                    $this->db->select("cre_no");
                    $this->db->from('mo_creative');
                    $this->db->where_in('cre_gp_no', $cre_gp_no);
                    $query = $this->db->get();
            
                    if ($query->num_rows() > 0) {
                        $cre_no_temp = $query->result_array();
                        
                        foreach($cre_no_temp as $c){
                            $cre_no[] = $c['cre_no'];
                        }
                        
                    }
                }
            }
            
        }elseif($data_type == "campaign"){
            
            $this->db->select("cre_gp_no");
            $this->db->from('mo_creative_group');
            $this->db->where('cam_no', $data_key);
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                $cre_gp_no_temp = $query->result_array();
            
                foreach($cre_gp_no_temp as $c){
                    $cre_gp_no[] = $c['cre_gp_no'];
                }
                
                $this->db->select("cre_no");
                $this->db->from('mo_creative');
                $this->db->where_in('cre_gp_no', $cre_gp_no);
                $query = $this->db->get();
            
                if ($query->num_rows() > 0) {
                    $cre_no_temp = $query->result_array();
                    
                    foreach($cre_no_temp as $c){
                        $cre_no[] = $c['cre_no'];
                    }
                }
            }
           
        }elseif($data_type == "creative_group"){
            
            $this->db->select("cre_no");
            $this->db->from('mo_creative');
            $this->db->where('cre_gp_no', $data_key);
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                $cre_no_temp = $query->result_array();
                
                foreach($cre_no_temp as $c){
                    $cre_no[] = $c['cre_no'];
                }
            }
              
        }elseif($data_type == "creative"){
            $cre_no = $data_key;
        }
        
        
        $this->db->select("*");
        
        if($targeting == "time"){
            $this->db->from('mo_report_time');
        }elseif($targeting == "os"){
            $this->db->from('mo_report_os');
        }elseif($targeting == "browser"){
            $this->db->from('mo_report_browser');
        }elseif($targeting == "device"){
            $this->db->from('mo_report_device');
        }elseif($targeting == "category"){
            $this->db->from('mo_report_category');
        }
        
        $this->db->where_in('cre_no', $cre_no);
        
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
        
        if($targeting == "time"){
            $this->db->group_by("day");
            $this->db->group_by("time");
        }elseif($targeting == "os"){
            $this->db->group_by("os");
        }elseif($targeting == "browser"){
            $this->db->group_by("browser");
        }elseif($targeting == "device"){
            $this->db->group_by("device");
        }elseif($targeting == "category"){
            $this->db->group_by("category");
        }
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    
    function select_report_targeting_list($targeting, $data_type, $data_key, $from_date, $to_date, $per_page, $page_num){
        if($data_key == ""){
            $data_key = $this->session->userdata('mem_no');
        }    

        if($data_type == "targeting"){
            //멤버아이디 밑에 광고주들의 총 광고들
            $this->db->select("adver_no");
            $this->db->from('mo_members_group');
            $this->db->where('agency_no', $data_key);
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $adver_no_temp = $query->result_array();
                
                foreach($adver_no_temp as $c){
                    $adver_no[] = $c['adver_no'];
                }
                
                $this->db->select("cam_no");
                $this->db->from('mo_campaign');
                $this->db->where_in('adver_no', $adver_no);
                $query = $this->db->get();
        
                if ($query->num_rows() > 0) {
                    $cam_no_temp = $query->result_array();
        
                    foreach($cam_no_temp as $c){         
                        $cam_no[] = $c['cam_no'];  
                    }
 
                    $this->db->select("cre_gp_no");
                    $this->db->from('mo_creative_group');
                    $this->db->where_in('cam_no', $cam_no);
                    $query = $this->db->get();
        
                    if ($query->num_rows() > 0) {
                        $cre_gp_no_temp = $query->result_array();
                        
                        foreach($cre_gp_no_temp as $c){
                            $cre_gp_no[] = $c['cre_gp_no'];
                        }
                        
                        $this->db->select("cre_no");
                        $this->db->from('mo_creative');
                        $this->db->where_in('cre_gp_no', $cre_gp_no);
                        $query = $this->db->get();
        
                        if ($query->num_rows() > 0) {
                            $cre_no_temp = $query->result_array();
                            
                            foreach($cre_no_temp as $c){
                                $cre_no[] = $c['cre_no'];
                            }
                        }
                    }
                }
            }

        }elseif($data_type == "advertiser"){
            //해당 광고주 하위 광고만

            $this->db->select("cam_no");
            $this->db->from('mo_campaign');
            $this->db->where('adver_no', $data_key);
            $query = $this->db->get();
        
            if ($query->num_rows() > 0) {
                $cam_no_temp = $query->result_array();
        
                foreach($cam_no_temp as $c){
                    $cam_no[] = $c['cam_no'];
                }
                
                
                $this->db->select("cre_gp_no");
                $this->db->from('mo_creative_group');
                $this->db->where_in('cam_no', $cam_no);
                $query = $this->db->get();
        
                if ($query->num_rows() > 0) {
                    $cre_gp_no_temp = $query->result_array();
        
                    foreach($cre_gp_no_temp as $c){
                        $cre_gp_no[] = $c['cre_gp_no'];
                    }
                    
                    $this->db->select("cre_no");
                    $this->db->from('mo_creative');
                    $this->db->where_in('cre_gp_no', $cre_gp_no);
                    $query = $this->db->get();
        
                    if ($query->num_rows() > 0) {
                        $cre_no_temp = $query->result_array();
                        
                        foreach($cre_no_temp as $c){
                            $cre_no[] = $c['cre_no'];
                        } 
                    }
                }
            }

        }elseif($data_type == "campaign"){
        
            $this->db->select("cre_gp_no");
            $this->db->from('mo_creative_group');
            $this->db->where('cam_no', $data_key);
            $query = $this->db->get();
        
            if ($query->num_rows() > 0) {
                $cre_gp_no_temp = $query->result_array();
        
                foreach($cre_gp_no_temp as $c){
                    $cre_gp_no[] = $c['cre_gp_no'];
                }
                
                $this->db->select("cre_no");
                $this->db->from('mo_creative');
                $this->db->where_in('cre_gp_no', $cre_gp_no);
                $query = $this->db->get();
        
                if ($query->num_rows() > 0) {
                    $cre_no_temp = $query->result_array();
                    
                    foreach($cre_no_temp as $c){
                        $cre_no[] = $c['cre_no'];        
                    }
                }
            }

        }elseif($data_type == "creative_group"){
        
            $this->db->select("cre_no");
            $this->db->from('mo_creative');
            $this->db->where('cre_gp_no', $data_key);
            $query = $this->db->get();
        
            if ($query->num_rows() > 0) {
                $cre_no_temp = $query->result_array();
                
                foreach($cre_no_temp as $c){ 
                    $cre_no[] = $c['cre_no'];        
                }
            }

        }elseif($data_type == "creative"){
            $cre_no = $data_key;
        }
        
        if($targeting == "time"){
            $this->db->select("day, time");
        }elseif($targeting == "os"){
            $this->db->select("os");
        }elseif($targeting == "browser"){
            $this->db->select("browser");
        }elseif($targeting == "device"){
            $this->db->select("device");
        }elseif($targeting == "category"){
            $this->db->select("(SELECT cont_cate_nm from mo_content_category where cont_cate_cd = category) category");
        }
        
        $this->db->select("sum(imp_cnt) imp_cnt, sum(click_cnt) click_cnt, sum(loc_price) loc_price, sum(price) price");
        
        if(count($cre_no) > 0){
            if($targeting == "time"){
                $this->db->from('mo_report_time');
            }elseif($targeting == "os"){
                $this->db->from('mo_report_os');
            }elseif($targeting == "browser"){
                $this->db->from('mo_report_browser');
            }elseif($targeting == "device"){
                $this->db->from('mo_report_device');
            }elseif($targeting == "category"){
                $this->db->from('mo_report_category');
            }
            
            $this->db->where_in('cre_no', $cre_no);
            
            $this->db->where('date_ymd >=', $from_date);
            $this->db->where('date_ymd <=', $to_date);
            
            if($targeting == "time"){
                $this->db->group_by("day");
                $this->db->group_by("time");
            }elseif($targeting == "os"){
                $this->db->group_by("os");
            }elseif($targeting == "browser"){
                $this->db->group_by("browser");
            }elseif($targeting == "device"){
                $this->db->group_by("device");
            }elseif($targeting == "category"){
                $this->db->group_by("category");
            }
            
            if($targeting == "time"){
                $this->db->order_by("day_seq", 'asc');
                $this->db->order_by("time", 'asc');
            }
            
            $this->db->limit($per_page, $page_num);
            
            $query = $this->db->get();
            //print_r($this->db->last_query());
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }else{
                return array();
            }
        }else{
            return array();  
        }
    }
    
    function select_report_targeting_max_imp($targeting, $data_type, $data_key, $from_date, $to_date){
    
        if($data_key == ""){
            $data_key = $this->session->userdata('mem_no');
        }
        
        if($data_type == "targeting"){
            //멤버아이디 밑에 광고주들의 총 광고들
            $this->db->select("adver_no");
            $this->db->from('mo_members_group');
            $this->db->where('agency_no', $data_key);
            $query = $this->db->get();
    
            if ($query->num_rows() > 0) {
                $adver_no_temp = $query->result_array();
    
                foreach($adver_no_temp as $c){
                    $adver_no[] = $c['adver_no'];
                }
    
                $this->db->select("cam_no");
                $this->db->from('mo_campaign');
                $this->db->where_in('adver_no', $adver_no);
                $query = $this->db->get();
    
                if ($query->num_rows() > 0) {
                    $cam_no_temp = $query->result_array();
    
                    foreach($cam_no_temp as $c){
                        $cam_no[] = $c['cam_no'];
                    }
    
                    $this->db->select("cre_gp_no");
                    $this->db->from('mo_creative_group');
                    $this->db->where_in('cam_no', $cam_no);
                    $query = $this->db->get();
    
                    if ($query->num_rows() > 0) {
                        $cre_gp_no_temp = $query->result_array();
    
                        foreach($cre_gp_no_temp as $c){
                            $cre_gp_no[] = $c['cre_gp_no'];
                        }
    
                        $this->db->select("cre_no");
                        $this->db->from('mo_creative');
                        $this->db->where_in('cre_gp_no', $cre_gp_no);
                        $query = $this->db->get();
    
                        if ($query->num_rows() > 0) {
                            $cre_no_temp = $query->result_array();
    
                            foreach($cre_no_temp as $c){
                                $cre_no[] = $c['cre_no'];
                            }
                        }
                    }
                }
            }
    
        }elseif($data_type == "advertiser"){
            //해당 광고주 하위 광고만
     
            $this->db->select("cam_no");
            $this->db->from('mo_campaign');
            $this->db->where('adver_no', $data_key);
            $query = $this->db->get();
    
            if ($query->num_rows() > 0) {
                $cam_no_temp = $query->result_array();
                
                foreach($cam_no_temp as $c){
                    $cam_no[] = $c['cam_no'];
                }
    
                $this->db->select("cre_gp_no");
                $this->db->from('mo_creative_group');
                $this->db->where_in('cam_no', $cam_no);
                $query = $this->db->get();
    
                if ($query->num_rows() > 0) {
                    $cre_gp_no_temp = $query->result_array();
    
                    foreach($cre_gp_no_temp as $c){
                        $cre_gp_no[] = $c['cre_gp_no'];
                    }
    
                    $this->db->select("cre_no");
                    $this->db->from('mo_creative');
                    $this->db->where_in('cre_gp_no', $cre_gp_no);
                    $query = $this->db->get();
    
                    if ($query->num_rows() > 0) {
                        $cre_no_temp = $query->result_array();
    
                        foreach($cre_no_temp as $c){
                            $cre_no[] = $c['cre_no'];
                        }
                    }
                }
            }
    
        }elseif($data_type == "campaign"){
    
            $this->db->select("cre_gp_no");
            $this->db->from('mo_creative_group');
            $this->db->where('cam_no', $data_key);
            $query = $this->db->get();
    
            if ($query->num_rows() > 0) {
                $cre_gp_no_temp = $query->result_array();
    
                foreach($cre_gp_no_temp as $c){
                    $cre_gp_no[] = $c['cre_gp_no'];
                }
    
                $this->db->select("cre_no");
                $this->db->from('mo_creative');
                $this->db->where_in('cre_gp_no', $cre_gp_no);
                $query = $this->db->get();
    
                if ($query->num_rows() > 0) {
                    $cre_no_temp = $query->result_array();
    
                    foreach($cre_no_temp as $c){
                        $cre_no[] = $c['cre_no'];
                    }
                }
            }
    
        }elseif($data_type == "creative_group"){
    
            $this->db->select("cre_no");
            $this->db->from('mo_creative');
            $this->db->where('cre_gp_no', $data_key);
            $query = $this->db->get();
    
            if ($query->num_rows() > 0) {
                $cre_no_temp = $query->result_array();
    
                foreach($cre_no_temp as $c){
                    $cre_no[] = $c['cre_no'];
                }
            }
            
        }elseif($data_type == "creative"){
            $cre_no = $data_key;
        }

        if(count($cre_no) > 0){

            if($targeting == "time"){
                $this->db->select("day, time");
            }elseif($targeting == "os"){
                $this->db->select("os");
            }elseif($targeting == "browser"){
                $this->db->select("browser");
            }elseif($targeting == "device"){
                $this->db->select("device");
            }elseif($targeting == "category"){
                $this->db->select("(SELECT cont_cate_nm from mo_content_category where cont_cate_cd = category) category");
            }
        
            $this->db->select("sum(imp_cnt) imp_cnt");
        
            if($targeting == "time"){
                $this->db->from('mo_report_time');
            }elseif($targeting == "os"){
                $this->db->from('mo_report_os');
            }elseif($targeting == "browser"){
                $this->db->from('mo_report_browser');
            }elseif($targeting == "device"){
                $this->db->from('mo_report_device');
            }elseif($targeting == "category"){
                $this->db->from('mo_report_category');
            }
    
            $this->db->where_in('cre_no', $cre_no);
        
            $this->db->where('date_ymd >=', $from_date);
            $this->db->where('date_ymd <=', $to_date);
        
            if($targeting == "time"){
                $this->db->group_by("day");
                $this->db->group_by("time");
            }elseif($targeting == "os"){
                $this->db->group_by("os");
            }elseif($targeting == "browser"){
                $this->db->group_by("browser");
            }elseif($targeting == "device"){
                $this->db->group_by("device");
            }elseif($targeting == "category"){
                $this->db->group_by("category");
            }
    
            $this->db->order_by("imp_cnt", 'desc');
            
            $this->db->limit('1');
        
            $query = $this->db->get();
            
            //print_r($this->db->last_query());
            
            if ($query->num_rows() > 0) {
                return $query->result_array();
            }else{
                return array();
            }
        }else{
            return array();
        }
    }

}

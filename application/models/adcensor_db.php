<?php
class Adcensor_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function select_adcensor_list($condition, $per_page, $page_num){

        $this->db->select("A.*, B.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE A.admin_no = mem_no) AS admin_nm");
        $this->db->from('mo_creative A');


        if($condition['cre_evaluation'] != null){
            $this->db->where("A.cre_evaluation = '".$condition['cre_evaluation']."'");
        }
        $this->db->where("A.cre_fl = 'N'");
        $this->db->join('mo_members B', 'B.mem_no = A.mem_no');
        $this->db->order_by('A.cre_no', 'desc');
        $this->db->limit($per_page, $page_num);

        $query = $this->db->get();

        //print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    public function select_adcensor_list_count($condition){
        $this->db->select("A.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE A.admin_no = mem_no) AS admin_nm");
        $this->db->from('mo_creative A');

        if($condition['cre_evaluation'] != null){
            $this->db->where("A.cre_evaluation = '".$condition['cre_evaluation']."'");
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }


    public function update_adcensor_evaluation($evaluation_data){

        $data = array(
                        'cre_evaluation' => $evaluation_data['cre_evaluation'],
                        'admin_no' => $evaluation_data['admin_no'],
                        'evaluation_dt' => date("Y-m-d H:i:s")
        );
        
        $this->db->where('cre_no', $evaluation_data['cre_no']);
        $this->db->update('mo_creative', $data);

        return true;
    }
}

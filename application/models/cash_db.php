<?php
class Cash_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    public function select_report_info($mem_no){
        $sql = " SELECT 'strDailyAverage' AS kind , AVG(B.sum_cash) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history A, ";
        $sql.= " (SELECT SUM(used_cash) AS sum_cash FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND cash_dt between DATE_FORMAT(now()- interval 7 day , '%Y-%m-%d') AND DATE_FORMAT(now(), '%Y-%m-%d') ";
        $sql.= " GROUP BY cash_dt) B ";
        $sql.= " UNION ALL ";
        $sql.= " SELECT 'strYesterdayCash', IFNULL(SUM(used_cash),0) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND  cash_dt = DATE_FORMAT(now()- interval 1 day , '%Y-%m-%d') ";
        $sql.= " UNION ALL ";
        $sql.= " SELECT 'strTodayCash', IFNULL(SUM(used_cash),0) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND cash_dt = DATE_FORMAT(now(), '%Y-%m-%d') ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_dashboard_cash_summary($mem_no){
        $sql = " SELECT A.mem_cash, A.mem_event_cash, IFNULL(AVG(B.sum_cash), 0) AS daily_avg, IFNULL(C.today_cash, 0) AS today_cash, IFNULL(D.yesterday_cash, 0) AS yesterday_cash ";
        $sql.= " FROM ";
        $sql.= " (SELECT mem_no, mem_event_cash, mem_cash FROM mountain.mo_members WHERE mem_no = '$mem_no') A LEFT JOIN";
        $sql.= " (SELECT mem_no, IFNULL(SUM(used_cash),0) AS sum_cash FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' AND cash_dt between DATE_FORMAT(now()- interval 7 day , '%Y-%m-%d') AND DATE_FORMAT(now(), '%Y-%m-%d')) ";
        $sql.= " B ON (B.mem_no = A.mem_no) LEFT JOIN ";
        $sql.= " (SELECT mem_no, IFNULL(SUM(used_cash),0) AS today_cash FROM mountain.mo_cash_history WHERE mem_no = '$mem_no' AND cash_dt = DATE_FORMAT(now(), '%Y-%m-%d')) ";
        $sql.= " C ON (C.mem_no = A.mem_no) LEFT JOIN ";
        $sql.= " (SELECT mem_no, IFNULL(SUM(used_cash),0) AS yesterday_cash FROM mountain.mo_cash_history WHERE mem_no = '$mem_no' AND cash_dt = DATE_FORMAT(now()- interval 1 day , '%Y-%m-%d')) D ON (D.mem_no = A.mem_no) ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
        
    }
    
    public function select_use_daily_cash($mem_no){
        $sql = " SELECT DATE_FORMAT(cash_dt,'%Y-%m-%d') AS cash_dt, SUM(used_cash) AS used_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $sql.= " AND cash_dt between DATE_FORMAT(now()- interval 7 day , '%Y-%m-%d') AND DATE_FORMAT(now(), '%Y-%m-%d') ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_member_cash_info($mem_no){
        $sql = " SELECT * FROM mountain.mo_members ";
        $sql.= " WHERE mem_no = '$mem_no' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }    
    
    public function insert_cash_charge_info($data){
        $query = $this->db->insert_string('mountain.mo_cash_charge', $data);
        $this->db->query($query);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    public function select_cash_charge_list($cond, $per_page = null, $page_num = null){
        $sql = " SELECT A.*, (SELECT code_desc FROM mountain.mo_code WHERE code_nm='cash_type' AND code_key=A.charge_way) AS charge_way_desc ";
        $sql.= " FROM mountain.mo_cash_charge A ";
        $sql.= " WHERE A.mem_no='".$cond['mem_no']."' AND A.charge_fl='N' AND charge_req_dt BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        if ($cond['kind'] == "C" || $cond['kind'] == "E"){
            $sql.= " AND cash_type = '".$cond['kind']."' ";
        }elseif ($cond['kind'] == "1" || $cond['kind'] == "2"){
            $sql.= " AND charge_way = '".$cond['kind']."' ";
        }
        
        if ($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_cash_charge_count($cond){
        $sql = " SELECT A.charge_no ";
        $sql.= " FROM mountain.mo_cash_charge A ";
        $sql.= " WHERE A.mem_no='".$cond['mem_no']."' AND A.charge_fl='N' AND charge_req_dt BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        if ($cond['kind'] == "C" || $cond['kind'] == "E"){
            $sql.= " AND cash_type = '".$cond['kind']."' ";
        }elseif ($cond['kind'] == "1" || $cond['kind'] == "2"){
            $sql.= " AND charge_way = '".$cond['kind']."' ";
        }
    
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_cash_history_list($cond, $per_page = null, $page_num = null){
        if ($cond['kind'] == null){
            $sql = " SELECT  A.cash_dt, B.sum_cash AS event_cash, C.sum_cash AS mo_cash ";
        }elseif ($cond['kind'] == "E"){
            $sql = " SELECT  A.cash_dt, B.sum_cash AS event_cash, '0' AS mo_cash ";
        }else{
            $sql = " SELECT  A.cash_dt, C.sum_cash AS mo_cash, '0' AS event_cash ";
        }
        
        $sql.= " FROM ";
        $sql.= " (SELECT DATE_FORMAT(cash_dt,'%Y-%m-%d') AS cash_dt ";
        $sql.= " FROM mountain.mo_cash_history "; 
        $sql.= " WHERE mem_no='".$cond['mem_no']."' AND cash_dt BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        if ($cond['kind'] != null){
            $sql.= "AND cash_type='".$cond['kind']."' ";
        }
        $sql.= " GROUP BY cash_dt) A ";
        $sql.= " LEFT JOIN ";
        $sql.= " (SELECT cash_type, sum(used_cash) AS sum_cash, DATE_FORMAT(cash_dt,'%Y-%m-%d') AS cash_dt ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no='".$cond['mem_no']."' AND cash_type='E' GROUP BY cash_dt) B ON (A.cash_dt=B.cash_dt) ";
        $sql.= " LEFT JOIN ";
        $sql.= " (SELECT cash_type, SUM(used_cash) AS sum_cash, DATE_FORMAT(cash_dt,'%Y-%m-%d') AS cash_dt ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no='".$cond['mem_no']."' AND cash_type='C' GROUP BY cash_dt) C ON (A.cash_dt=C.cash_dt) ";

        if($page_num == null || $page_num == ""){
            $page_num = 0;
        }
    
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_cash_history_count($cond){
        $sql = " SELECT  A.cash_dt ";
        $sql.= " FROM ";
        $sql.= " (SELECT DATE_FORMAT(cash_dt,'%Y-%m-%d') AS cash_dt ";
        $sql.= " FROM mountain.mo_cash_history "; 
        $sql.= " WHERE mem_no='".$cond['mem_no']."' AND cash_dt BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' "; 
        $sql.= " AND cash_type='".$cond['kind']."' ";
        $sql.= " GROUP BY cash_dt) A ";
    
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_cash_refund_list($cond, $per_page, $page_num){
        $sql = " SELECT A.*, B.*, C.*, ";
        $sql.= " (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = B.contact_no) AS contact_nm ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A LEFT JOIN ";
        $sql.= " mountain.mo_refund_request B ON (A.question_no = B.question_no) LEFT JOIN ";
        $sql.= " mountain.mo_members C ON (A.mem_no = C.mem_no) ";
        $sql.= " WHERE A.mem_no='".$cond['mem_no']."' AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' AND  A.question_fl = '6' ";
        if ($cond['kind'] != ""){
            $sql.= "AND refund_st='".$cond['kind']."' ";
        }
        if($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        
//         print_r($sql);
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_cash_refund_count($cond){
        $sql = " SELECT refund_no";
        $sql.= " FROM mountain.mo_cash_refund A";
        $sql.= " WHERE mem_no='".$cond['mem_no']."' AND refund_req_dt BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        if ($cond['kind'] != null){
            $sql.= "AND refund_st='".$cond['kind']."' ";
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
        return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_tax_bill_list($mem_no, $order_col, $per_page, $page_num, $from_date, $to_date){
        $sql = " SELECT *";
        if ($this->session->userdata('mem_type') == 'master'){
            $sql.= " FROM mountain.mo_calculate_tax_history ";
            $sql.= " WHERE mem_no = '".$mem_no."' AND publish_dt >= '".$from_date."' AND publish_dt <= '".$to_date."' ";
        }else{
            $sql.= " FROM mountain.mo_calculate_tax ";
            $sql.= " WHERE mem_no = '".$mem_no."' AND publish_dt >= '".substr($from_date,0,7)."' AND publish_dt <= '".substr($to_date,0,7)."' ";
        }
        if($page_num == null || $page_num == ""){
            $page_num = 0;
        }
        $sql.= " ORDER BY $order_col DESC ";

        $sql.= " LIMIT ".$page_num.",".$per_page;
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_tax_bill_count($mem_no, $from_date, $to_date){
        $sql = " SELECT * ";
        $sql.= " FROM mountain.mo_calculate_tax_history ";
        $sql.= " WHERE mem_no = '".$mem_no."' AND publish_dt >='".$from_date."' AND publish_dt <= '".$to_date."' ";
  
    
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_forecast_amount($mem_no){
        $sql = " SELECT DATE_FORMAT(cash_dt, '%Y-%m') AS forecast_dt, SUM(used_cash) AS forecast_amount ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no = '$mem_no' AND cash_type='C' GROUP BY DATE_FORMAT(cash_dt, '%Y-%m') ORDER BY cash_dt DESC ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_manager_tax_bill_detail($mem_no, $date_ym){
        $sql = " SELECT *";
        $sql.= " FROM mountain.mo_calculate_tax ";
        $sql.= " WHERE mem_no = '".$mem_no."' AND date_ym = '".$date_ym."' ";
    
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row;
    }
    
    public function select_manager_list($mem_no){

        $manager_list = array();

        $sql = " SELECT manager_no FROM mountain.mo_members_group WHERE agency_no = '".$mem_no."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                if($row['manager_no']!=""){
                    $manager_list[] = $row['manager_no'];
                }
            }
            return $manager_list;
        }
    }
    
    public function select_manager_use_daily_cash($manager_list){
        $manager_list=implode(',',$manager_list);
        if ($manager_list != ""){
            $sql = " SELECT DATE_FORMAT(cash_dt,'%Y-%m-%d') AS cash_dt, used_cash ";
            $sql.= " FROM mountain.mo_cash_history ";
            $sql.= " WHERE mem_no IN ($manager_list) ";
            $sql.= " AND cash_dt between DATE_FORMAT(now()- interval 7 day , '%Y-%m-%d') AND DATE_FORMAT(now(), '%Y-%m-%d') ";
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    $result[] = $row;
                }
                return $result;
            }
        }
    }
    
    public function select_manager_report_info($manager_list){

        if($manager_list == null){
            $result = array(
                    "0"=>array("kind"=>"strDailyAverage", "use_cash"=>"0"),
                    "1"=>array("kind"=>"strYesterdayCash", "use_cash"=>"0"),
                    "2"=>array("kind"=>"strTodayCash", "use_cash"=>"0"),
            );
            return $result;
        }

        $manager_list = implode(',',$manager_list);

        $sql = " SELECT 'strDailyAverage' AS kind , AVG(B.sum_cash) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history A, ";
        $sql.= " (SELECT SUM(used_cash) AS sum_cash FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no IN ($manager_list) ";
        $sql.= " AND cash_dt between DATE_FORMAT(now()- interval 7 day , '%Y-%m-%d') AND DATE_FORMAT(now(), '%Y-%m-%d') ";
        $sql.= " GROUP BY cash_dt) B ";
        $sql.= " UNION ALL ";
        $sql.= " SELECT 'strYesterdayCash', IFNULL(SUM(used_cash),0) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no IN ($manager_list) ";
        $sql.= " AND  cash_dt = DATE_FORMAT(now()- interval 1 day , '%Y-%m-%d') ";
        $sql.= " UNION ALL ";
        $sql.= " SELECT 'strTodayCash', IFNULL(SUM(used_cash),0) AS use_cash ";
        $sql.= " FROM mountain.mo_cash_history ";
        $sql.= " WHERE mem_no IN ($manager_list) ";
        $sql.= " AND cash_dt = DATE_FORMAT(now(), '%Y-%m-%d') ";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_manager_cash_list($master_no){
        $sql = " SELECT A.* ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members A, ";
        $sql.= " (SELECT manager_no FROM mountain.mo_members_group WHERE agency_no = '$master_no') B ";
        $sql.= " WHERE A.mem_no=B.manager_no ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_manager_cash_info($master_no){
        $sql = " SELECT  ";
        $sql.= " SUM(A.mem_cash) AS mo_cash, SUM(A.mem_event_cash) AS event_cash ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members A, ";
        $sql.= " (SELECT manager_no FROM mountain.mo_members_group WHERE agency_no = '$master_no') B ";
        $sql.= " WHERE B.manager_no=A.mem_no ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }

    public function select_event_cash_list($condition, $per_page, $page_num){
    
        $this->db->select("A.*, B.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE A.admin_no = mem_no) AS admin_nm");
        $this->db->from('mo_cash_charge A');
    
        $this->db->where("A.charge_way = '".$condition['charge_way']."'");
    
        if($condition['charge_nm'] != null){
            $this->db->where("A.charge_nm = '".$condition['charge_nm']."'");
        }
    
        if($condition['charge_st'] != null){
            $this->db->where("A.charge_st = '".$condition['charge_st']."'");
        }
    
        $this->db->where("A.cash_type = 'E'");
    
        $this->db->join('mo_members B', 'B.mem_no = A.mem_no');
        $this->db->order_by('A.charge_no', 'desc');
    
    
        $this->db->limit($per_page, $page_num);
    
        $query = $this->db->get();
        //print_r($this->db->last_query());
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    public function select_event_cash_list_count($condition){
        $this->db->select("A.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE A.admin_no = mem_no) AS admin_nm");
        $this->db->from('mo_cash_charge A');
    
        $this->db->where("A.charge_way = '".$condition['charge_way']."'");
    
        if($condition['charge_nm'] != null){
            $this->db->where("A.charge_nm = '".$condition['charge_nm']."'");
        }
    
        if($condition['charge_st'] != null){
            $this->db->where("A.charge_st = '".$condition['charge_st']."'");
        }
    
        $this->db->where("A.cash_type = 'E'");
    
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_cash_list($condition, $per_page, $page_num){

        $this->db->select("A.*, B.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE A.admin_no = mem_no) AS admin_nm");
        $this->db->from('mo_cash_charge A');

        $this->db->where("A.charge_way = '".$condition['charge_way']."'");
        
        if($condition['charge_nm'] != null){
            $this->db->where("A.charge_nm = '".$condition['charge_nm']."'");
        }
        
        if($condition['charge_st'] != null){
            $this->db->where("A.charge_st = '".$condition['charge_st']."'");
        }

        $this->db->where("A.cash_type = 'C'");
        
        $this->db->join('mo_members B', 'B.mem_no = A.mem_no');
        $this->db->order_by('A.charge_no', 'desc');
        

        $this->db->limit($per_page, $page_num);
        
        $query = $this->db->get();
        //print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    public function select_cash_list_count($condition){
        $this->db->select("A.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE A.admin_no = mem_no) AS admin_nm");
        $this->db->from('mo_cash_charge A');

        $this->db->where("A.charge_way = '".$condition['charge_way']."'");
        
        if($condition['charge_nm'] != null){
            $this->db->where("A.charge_nm = '".$condition['charge_nm']."'");
        }

        if($condition['charge_st'] != null){
            $this->db->where("A.charge_st = '".$condition['charge_st']."'");
        }
        
        $this->db->where("A.cash_type = 'C'");
        
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }

    public function select_cash_kind_count(){
        $sql = " SELECT SUM(IF(charge_way = 1, 1, 0 )) AS paid_cnt, SUM(IF(charge_way = 5, 1, 0 )) AS no_paid_cnt, SUM(IF(charge_way = 6, 1, 0 )) AS event_cnt ";
        $sql.= " FROM mountain.mo_cash_charge ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    public function select_cash_info($data){
        $this->db->select("A.*, B.*");
        $this->db->select("(SELECT mem_nm FROM mo_members WHERE mem_no = '".$data['admin_no']."') AS admin_nm");
        $this->db->from('mo_cash_charge A');
        $this->db->join('mo_members B', 'B.mem_no = A.mem_no');
        $this->db->where("A.charge_no ='".$data['charge_no']."'");
        $query = $this->db->get();


        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    //국제 통화 환율을 업데이트
    // URL/service/get_exchange/USD/KRW
    function get_exchange_($from_currency = null, $to_currency = null){
    
        if(!isset($from_currency))
            return "";
    
        if(!isset($to_currency))
            return "";
    
        $content = file_get_contents('https://www.google.com/finance/converter?a=1&from='.$from_currency.'&to='.$to_currency);
        $doc = new DOMDocument();
        @$doc->loadHTML($content);
        $xpath = new DOMXpath($doc);
        $result = $xpath->query('//*[@id="currency_converter_result"]/span')->item(0)->nodeValue;
        $exchange_rate = str_replace( $to_currency, '', $result);
    
        return $exchange_rate;
    }
    
    function usd_price($loc_price, $input_date){
        $this->db->select("exchange_rate");
        $this->db->where("from_code_key", "USD");
        $this->db->where("to_code_key", "KRW");
        $this->db->where("DATE_FORMAT(input_dt,'%Y-%m-%d')", $input_date);
        $this->db->from('mo_exchange');
        $this->db->limit(1);
        $query = $this->db->get();
        foreach ($query->result() as $row){
            $exchange_rate = $row->exchange_rate;
        }
    
        if(!isset($exchange_rate) || $exchange_rate == ""){
            $exchange_rate = $this->get_exchange_("USD", "KRW");
        }
        
        if($loc_price > 0) {
            $usd_price = $loc_price / $exchange_rate;
        }else{
            $usd_price = 0;
        }
        
        return $usd_price;
    }
    
    public function update_event_cash_pay($cash_data){
        //유저유무, 유저캐쉬
        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("mem_no = '".$cash_data['mem_no']."'");
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $mem_info[] = $row;
            }
        }else{
            return false;
        }
        //유저유무, 유저캐쉬
        
        //캐쉬지급
        $data = array(
                        'mem_event_cash' => $mem_info[0]['mem_event_cash'] + $cash_data['charge_cash'],
        );
        
        $this->db->where('mem_no', $cash_data['mem_no']);
        $this->db->update('mo_members', $data);
        //캐쉬지급
    }
    
    public function update_cash_pay($cash_data){

        //유저유무, 유저캐쉬
        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("mem_no = '".$cash_data['mem_no']."'");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $mem_info[] = $row;
            }
        }else{
            return false;
        }
        //유저유무, 유저캐쉬

        if($cash_data['charge_way'] == '5' && $cash_data['charge_st'] == "C" ){
            //후불이면서 입금완료된 상황에서는 상태값만 변경한다.
        }else{
            //캐쉬지급
            $data = array(
                'mem_cash' => $mem_info[0]['mem_cash'] + $cash_data['charge_cash'],
            );
    
            $this->db->where('mem_no', $cash_data['mem_no']);
            $this->db->update('mo_members', $data);
            //캐쉬지급
            
            //일일 충전 관련 테이블
            $this->db->select("*");
            //$this->db->from('mo_revenue_deposit');
            $this->db->from('mo_revenue');
            $this->db->where("mem_no", $cash_data['mem_no']);
            $this->db->where("date_ymd", date('Y-m-d'));
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                $revenue_deposit = $query->result_array();
            
                foreach($revenue_deposit as $rd){
                    $deposit = $rd['deposit'];
                    $loc_deposit = $rd['loc_deposit'];
                    $deposit_lt = $rd['deposit_lt'];
                    $loc_deposit_lt = $rd['loc_deposit_lt'];
                }
            
                if($cash_data['charge_st'] == "C" && $cash_data['charge_way'] != 5){
                    $data = array(
                                    'agency_no' => $mem_info[0]['agency_no'],
                                    'mem_id' => $mem_info[0]['mem_id'],
                                    'mem_type' => $mem_info[0]['mem_type'],
                                    'group_no' => $mem_info[0]['group_no'],
                                    'group_nm' => $mem_info[0]['group_nm'],
                                    'group_role' => $mem_info[0]['role'],
                                    'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                    'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                                    'mem_no' => $cash_data['mem_no'],
                                    'date_ymd' => date("Y-m-d"),
                                    'loc_deposit' => $loc_deposit + $cash_data['charge_cash'],
                                    'deposit' => $deposit + $this->usd_price($cash_data['charge_cash'], date("Y-m-d"))
                    );
            
                    $this->db->where('mem_no', $cash_data['mem_no']);
                    $this->db->where('date_ymd', date('Y-m-d'));
                    //$this->db->update('mo_revenue_deposit', $data);
                    $this->db->update('mo_revenue', $data);
                    
                    $data = array(
                                    'mem_pay_later' => "N"
                    
                    );
                    $this->db->where('mem_no', $cash_data['mem_no']);
                    $this->db->update('mo_members', $data);
                    
                }else if($cash_data['charge_st'] == "E"){
                    $data = array(
                                    'agency_no' => $mem_info[0]['agency_no'],
                                    'mem_id' => $mem_info[0]['mem_id'],
                                    'mem_type' => $mem_info[0]['mem_type'],
                                    'group_no' => $mem_info[0]['group_no'],
                                    'group_nm' => $mem_info[0]['group_nm'],
                                    'group_role' => $mem_info[0]['role'],
                                    'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                    'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                                    'mem_no' => $cash_data['mem_no'],
                                    'date_ymd' => date("Y-m-d"),
                                    'loc_deposit_lt' => $loc_deposit_lt + $cash_data['charge_cash'],
                                    'deposit_lt' => $deposit_lt + $this->usd_price($cash_data['charge_cash'], date("Y-m-d"))
                    );
            
                    $this->db->where('mem_no', $cash_data['mem_no']);
                    $this->db->where('date_ymd', date('Y-m-d'));
                    //$this->db->update('mo_revenue_deposit', $data);
                    $this->db->update('mo_revenue', $data);
                    
                    $data = array(
                                    'mem_pay_later' => "Y"
                                    
                    );
                    $this->db->where('mem_no', $cash_data['mem_no']);
                    $this->db->update('mo_members', $data);
                }
            
            }else{
                 
                if($cash_data['charge_st'] == "C" && $cash_data['charge_way'] != 5){
                    $data = array(
                                    'agency_no' => $mem_info[0]['agency_no'],
                                    'mem_id' => $mem_info[0]['mem_id'],
                                    'mem_type' => $mem_info[0]['mem_type'],
                                    'group_no' => $mem_info[0]['group_no'],
                                    'group_nm' => $mem_info[0]['group_nm'],
                                    'group_role' => $mem_info[0]['role'],
                                    'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                    'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                                    'mem_no' => $cash_data['mem_no'],
                                    'date_ymd' => date("Y-m-d"),
                                    'loc_deposit' => $cash_data['charge_cash'],
                                    'deposit' => $this->usd_price($cash_data['charge_cash'], date("Y-m-d"))
                    );
                    //$query = $this->db->insert_string('mo_revenue_deposit', $data);
                    $query = $this->db->insert_string('mo_revenue', $data);
                    $this->db->query($query);
                    
                    $data = array(
                                    'mem_pay_later' => "N"
                    
                    );
                    $this->db->where('mem_no', $cash_data['mem_no']);
                    $this->db->update('mo_members', $data);
                    
                }else if($cash_data['charge_st'] == "E"){
                    $data = array(
                                    'agency_no' => $mem_info[0]['agency_no'],
                                    'mem_id' => $mem_info[0]['mem_id'],
                                    'mem_type' => $mem_info[0]['mem_type'],
                                    'group_no' => $mem_info[0]['group_no'],
                                    'group_nm' => $mem_info[0]['group_nm'],
                                    'group_role' => $mem_info[0]['role'],
                                    'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                    'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                                    'mem_no' => $cash_data['mem_no'],
                                    'date_ymd' => date("Y-m-d"),
                                    'loc_deposit_lt' => $cash_data['charge_cash'],
                                    'deposit_lt' => $this->usd_price($cash_data['charge_cash'], date("Y-m-d"))
                    );
                    //$query = $this->db->insert_string('mo_revenue_deposit', $data);
                    $query = $this->db->insert_string('mo_revenue', $data);
                    $this->db->query($query);
                    
                    $data = array(
                                    'mem_pay_later' => "Y"
                    
                    );
                    $this->db->where('mem_no', $cash_data['mem_no']);
                    $this->db->update('mo_members', $data);
                }
            }
            //일일 충전 관련 테이블
        }

        $data = array(
            'admin_no' => $cash_data['admin_no'],
            'charge_confirm_dt' => $cash_data['charge_confirm_dt'],
            'charge_dt' => $cash_data['charge_dt'],
            'charge_way' => $cash_data['charge_way'],
            'charge_st' => $cash_data['charge_st']
        );

        $this->db->where('charge_no', $cash_data['charge_no']);
        $this->db->update('mo_cash_charge', $data);

        return true;
    }

    public function update_charge_confirm_dt($cash_data){

        $data = array(
            'charge_confirm_dt' => $cash_data['charge_confirm_dt']
        );

        $this->db->where('charge_no', $cash_data['charge_no']);
        $this->db->update('mo_cash_charge', $data);

        return true;
    }

    public function update_cash_revoke($cash_data){

        //유저유무, 유저캐쉬
        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("mem_no = '".$cash_data['mem_no']."'");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $mem_info[] = $row;
            }
        }else{
            return false;
        }
        //유저유무, 유저캐쉬

        //지급취소된것인지 여부
        $this->db->select("*");
        $this->db->from('mo_cash_charge');
        $this->db->where("charge_no = '".$cash_data['charge_no']."'");
        $this->db->where("charge_st = 'D'");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return false;
        }
        //지급취소된것인지 여부

        //캐쉬지급취소
        $result_cash = $mem_info[0]['mem_cash'] - $cash_data['charge_cash'];
        if($result_cash < 0){
            
            //미수금 발생 (후불일경우) : 기록 남기기 (미수금 관리)
            $receivable_cash = abs($result_cash);
            $result_cash = 0;
        }
        $data = array(
            'mem_cash' => $result_cash,
        );

        $this->db->where('mem_no', $cash_data['mem_no']);
        $this->db->update('mo_members', $data);
        //캐쉬지급취소
        
        //수익관련 로그 업데이트 
        $this->db->select("*");
        //$this->db->from('mo_revenue_refund');
        $this->db->from('mo_revenue');
        $this->db->where("mem_no", $cash_data['mem_no']);
        $this->db->where("date_ymd", date('Y-m-d'));
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            $revenue_refund = $query->result_array();
        
            foreach($revenue_refund as $re){
                $refund = $re['refund'];
                $loc_refund = $re['loc_refund'];
                $refund_lt = $re['refund_lt'];
                $loc_refund_lt = $re['loc_refund_lt'];
            }
            
            if($cash_data['charge_way'] == 5){
                $data = array(
                        'agency_no' => $mem_info[0]['agency_no'],
                        'mem_id' => $mem_info[0]['mem_id'],
                        'mem_type' => $mem_info[0]['mem_type'],
                        'group_no' => $mem_info[0]['group_no'],
                        'group_nm' => $mem_info[0]['group_nm'],
                        'group_role' => $mem_info[0]['role'],
                        'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                        'loc_refund_lt' => $loc_refund_lt + ($cash_data['charge_cash'] - $receivable_cash),
                        'refund_lt' => $refund_lt + $this->usd_price(($cash_data['charge_cash'] - $receivable_cash), date("Y-m-d"))
                );
                
                $this->db->where('mem_no', $cash_data['mem_no']);
                $this->db->where('date_ymd', date('Y-m-d'));
                //$this->db->update('mo_revenue_refund', $data);
                $this->db->update('mo_revenue', $data);
                
            }else if($cash_data['charge_way'] == 1){
                $data = array(
                        'agency_no' => $mem_info[0]['agency_no'],
                        'mem_id' => $mem_info[0]['mem_id'],
                        'mem_type' => $mem_info[0]['mem_type'],
                        'group_no' => $mem_info[0]['group_no'],
                        'group_nm' => $mem_info[0]['group_nm'],
                        'group_role' => $mem_info[0]['role'],
                        'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                        'loc_refund' => $loc_refund + ($cash_data['charge_cash'] - $receivable_cash),
                        'refund' => $refund + $this->usd_price(($cash_data['charge_cash'] - $receivable_cash), date("Y-m-d"))
                );
                
                $this->db->where('mem_no', $cash_data['mem_no']);
                $this->db->where('date_ymd', date('Y-m-d'));
                //$this->db->update('mo_revenue_refund', $data);
                $this->db->update('mo_revenue', $data);
                
            }
        }else{
            
            if($cash_data['charge_way'] == 5){
                $data = array(
                        'agency_no' => $mem_info[0]['agency_no'],
                        'mem_id' => $mem_info[0]['mem_id'],
                        'mem_type' => $mem_info[0]['mem_type'],
                        'group_no' => $mem_info[0]['group_no'],
                        'group_nm' => $mem_info[0]['group_nm'],
                        'group_role' => $mem_info[0]['role'],
                        'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                        'mem_no' => $cash_data['mem_no'],
                        'date_ymd' => date("Y-m-d"),
                        'loc_refund_lt' => ($cash_data['charge_cash'] - $receivable_cash),
                        'refund_lt' => $this->usd_price(($cash_data['charge_cash'] - $receivable_cash), date("Y-m-d"))
                );
                //$query = $this->db->insert_string('mo_revenue_refund', $data);
                $query = $this->db->insert_string('mo_revenue', $data);
                $this->db->query($query);
            
            }else if($cash_data['charge_way'] == 1){
                $data = array(
                        'agency_no' => $mem_info[0]['agency_no'],
                        'mem_id' => $mem_info[0]['mem_id'],
                        'mem_type' => $mem_info[0]['mem_type'],
                        'group_no' => $mem_info[0]['group_no'],
                        'group_nm' => $mem_info[0]['group_nm'],
                        'group_role' => $mem_info[0]['role'],
                        'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                        'mem_no' => $cash_data['mem_no'],
                        'date_ymd' => date("Y-m-d"),
                        'loc_refund' => ($cash_data['charge_cash'] - $receivable_cash),
                        'refund' => $this->usd_price(($cash_data['charge_cash'] - $receivable_cash), date("Y-m-d"))
                );
                //$query = $this->db->insert_string('mo_revenue_refund', $data);
                $query = $this->db->insert_string('mo_revenue', $data);
                $this->db->query($query);
            }
        } 
        //수익관련 로그 업데이트 

        //캐쉬지급 로그
        $data = array(
            'admin_no' => $cash_data['admin_no'],
            'mem_no' => $cash_data['mem_no'],
            'revoke_cash' => $cash_data['charge_cash'] - $receivable_cash,
            'revoke_log_dt' => date("Y-m-d H:i:s")
        );

        $query = $this->db->insert_string('mo_cash_revoke_log', $data);
        $this->db->query($query);
        //캐쉬지급 로그

        $data = array(
            'charge_st' => "D"
        );

        $this->db->where('charge_no', $cash_data['charge_no']);
        $this->db->update('mo_cash_charge', $data);

        return true;
    }
    
    public function select_refund_request_list($cond){
        $sql = " SELECT A.*, B.*, C.*, ";
        $sql.= " (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = B.contact_no) AS contact_nm ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A LEFT JOIN ";
        $sql.= " mountain.mo_refund_request B ON (A.question_no = B.question_no) LEFT JOIN ";
        $sql.= " mountain.mo_members C ON (A.mem_no = C.mem_no) ";
        $sql.= " WHERE A.question_fl = '6' ";
        if ($cond['cond_refund_st'] != ""){
            $sql.= " AND B.refund_st = '".$cond['cond_refund_st']."' ";
        }
        if ($cond['mem_com_nm'] != ""){
            $sql.= " AND C.mem_com_no LIKE '%".$cond['mem_com_no']."%' ";
        }
        if ($cond['mem_id'] != ""){
            $sql.= " AND C.mem_id LIKE '%".$cond['mem_id']."%' ";
        }
        if ($cond['fromto_date'] != ""){
            $sql.= " AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        }
        if ($cond['manager_yn'] == "on" && $cond['master_yn'] == "on"){
            $sql.= " AND C.mem_type IN ('manager', 'master') ";
        }else{
            if ($cond['master_yn'] == "on"){
                $sql.= " AND C.mem_type = 'master' ";
            }elseif ($cond['manager_yn'] == "on"){
                $sql.= " AND C.mem_type = 'manager' ";
            }
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    public function select_refund_request_count($cond){
        $sql = " SELECT A.*, B.*, C.*, ";
        $sql.= " (SELECT mem_nm FROM mountain.mo_members WHERE mem_no = B.contact_no) AS contact_nm ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A LEFT JOIN ";
        $sql.= " mountain.mo_refund_request B ON (A.question_no = B.question_no) LEFT JOIN ";
        $sql.= " mountain.mo_members C ON (A.mem_no = C.mem_no) ";
        $sql.= " WHERE A.question_fl = '6' ";
        if ($cond['cond_refund_st'] != ""){
            $sql.= " AND B.refund_st = '".$cond['cond_refund_st']."' ";
        }
        if ($cond['mem_com_nm'] != ""){
            $sql.= " AND C.mem_com_no LIKE '%".$cond['mem_com_no']."%' ";
        }
        if ($cond['mem_id'] != ""){
            $sql.= " AND C.mem_id LIKE '%".$cond['mem_id']."%' ";
        }
        if ($cond['fromto_date'] != ""){
            $sql.= " AND A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_refund_request_summary(){
        $sql = " SELECT ";
        $sql.= " COUNT(1) AS all_cnt, ";
        $sql.= " IFNULL(SUM(IF(left(A.date_ymd, 10) = date_format(now(),'%Y-%m-%d'), 1, 0)),0) AS today_cnt, ";
        $sql.= " IFNULL(SUM(IF(refund_st = '1', 1, 0)),0) AS soon_cnt, ";
        $sql.= " IFNULL(SUM(IF(refund_st = '2', 1, 0)),0) AS reject_cnt, ";
        $sql.= " IFNULL(SUM(IF(refund_st = '3', 1, 0)),0) AS done_cnt ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A LEFT JOIN ";
        $sql.= " mountain.mo_refund_request B ON (A.question_no = B.question_no) ";
        $sql.= " WHERE A.question_fl = '6' ";
        
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function insert_refund_request($question_no){
        $sql = " INSERT INTO mo_refund_request (question_no, refund_st) ";
        $sql.= " VALUES ";
        $sql.= " ('$question_no', '1' ) ";
        
        $this->db->query($sql);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    public function select_refund_request_view($cond){
        $sql = " SELECT A.*, B.*, C.*, A.mem_no AS request_mem_no ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_question A LEFT JOIN ";
        $sql.= " mountain.mo_refund_request B ON (A.question_no = B.question_no) LEFT JOIN ";
        $sql.= " mountain.mo_members C ON (A.mem_no = C.mem_no) ";
        $sql.= " WHERE A.question_fl = '6' AND B.request_no = '".$cond['request_no']."' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function update_refund_request($data, $request_no, $request_mem_no){
        
        //유저유무, 유저캐쉬
        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("mem_no = '".$request_mem_no."'");
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $mem_info[] = $row;
            }
        }else{
            return false;
        }
        //유저유무, 유저캐쉬
        
        $where = " request_no = '".$request_no."' ";
        $query = $this->db->update_string('mo_refund_request', $data, $where);
        $this->db->query($query);
        
        if ($data['refund_st'] == "3"){
            $sql = " UPDATE mountain.mo_members SET ";
            $sql.= " mem_cash = mem_cash - ".$data['refund_amount'];
            $sql.= " WHERE mem_no = '$request_mem_no' ";
            $this->db->query($sql);
            
            //수익관련 로그 업데이트
            $this->db->select("*");
            //$this->db->from('mo_revenue_refund');
            $this->db->from('mo_revenue');
            $this->db->where("mem_no", $request_mem_no);
            $this->db->where("date_ymd", date('Y-m-d'));
            $query = $this->db->get();
            
            if ($query->num_rows() > 0) {
                $revenue_refund = $query->result_array();
            
                foreach($revenue_refund as $re){
                    $refund = $re['refund'];
                    $loc_refund = $re['loc_refund'];
                }
                
                $data = array(
                                'agency_no' => $mem_info[0]['agency_no'],
                                'mem_id' => $mem_info[0]['mem_id'],
                                'mem_type' => $mem_info[0]['mem_type'],
                                'group_no' => $mem_info[0]['group_no'],
                                'group_nm' => $mem_info[0]['group_nm'],
                                'group_role' => $mem_info[0]['role'],
                                'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                                'loc_refund' => $loc_refund + $data['refund_amount'],
                                'refund' => $refund + $this->usd_price($data['refund_amount'], date("Y-m-d"))
                );
                
                $this->db->where('mem_no', $request_mem_no);
                $this->db->where('date_ymd', date('Y-m-d'));
                //$this->db->update('mo_revenue_refund', $data);
                $this->db->update('mo_revenue', $data);
            }else{
                $data = array(
                                'agency_no' => $mem_info[0]['agency_no'],
                                'mem_id' => $mem_info[0]['mem_id'],
                                'mem_type' => $mem_info[0]['mem_type'],
                                'group_no' => $mem_info[0]['group_no'],
                                'group_nm' => $mem_info[0]['group_nm'],
                                'group_role' => $mem_info[0]['role'],
                                'mem_com_nm' => $mem_info[0]['mem_com_nm'],
                                'mem_pay_later' => $mem_info[0]['mem_pay_later'],
                                'mem_no' => $request_mem_no,
                                'date_ymd' => date("Y-m-d"),
                                'loc_refund' => $data['refund_amount'],
                                'refund' => $this->usd_price($data['refund_amount'], date("Y-m-d"))
                );
                $query = $this->db->insert_string('mo_revenue', $data);
                $this->db->query($query);
            }
            //수익관련 로그 업데이트
        }
    }
    
    public function select_member_list(){
        $this->db->select("*");
        $this->db->from('mo_members');
        $this->db->where("mem_fl", "N");
        $query = $this->db->get();

        if ( $query->num_rows() > 0 ){
            $result = $query->result_array();
        }
        
        return $result;
    }
}

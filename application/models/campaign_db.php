<?php
class Campaign_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }
    
    function nm_check($cam_nm, $mem_no, $cam_no = null){

        if ((strlen($cam_nm) == 0 )|| ($cam_nm == '')) {
            return "none";
        } else {
            $sql = "SELECT
                        cam_no
                    FROM
                        mountain.mo_campaign
                    WHERE
                        cam_nm = '".$cam_nm."' AND adver_no = '".$mem_no."' AND cam_fl = 'N' ";
            if ($cam_no != ""){
                $sql.= " AND cam_no != '$cam_no' ";
            }
            $query = $this->db->query($sql);
        
            if ($query->num_rows() > 0) {
                return "false";
            } else {
                return "true";
            }
        }
    }

    function select_campaign_list($cond, $per_page=null, $page_num=null){
        $sql = " SELECT ";
        $sql.= " B.*, IFNULL(C.imp,0) AS imp, IFNULL(C.clk,0) AS clk, IFNULL(C.ctr,0) AS ctr, A.mem_com_nm adver_nm, ";
        $sql.= " ( SELECT code_desc FROM mountain.mo_code WHERE code_nm = 'status' AND code_key = cam_status ) AS cam_status_desc";
        $sql.= " FROM mo_members A";
        $sql.= " LEFT JOIN ";
        $sql.= " mo_campaign B ON (B.adver_no = A.mem_no) ";
        $sql.= " LEFT JOIN ";
        $sql.= " (SELECT  cam_no, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL(AVG(ctr),0) AS ctr ";   
        $sql.= " FROM mountain.mo_report_campaign ";
        $sql.= " WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' GROUP BY cam_no) C "; 
        $sql.= " ON (C.cam_no = B.cam_no) "; 
        $sql.= " WHERE B.cam_fl = 'N' ";
        $sql.= " AND A.mem_no = '".$cond['mem_no']."' AND A.mem_no != ''";
        if(isset($cond['cam_status'])){
            if($cond['cam_status'][0] != 0){
                $cam_status = implode(',',$cond['cam_status']);
                $sql.= " AND B.cam_status IN ($cam_status) ";
            }
        }
        
        if( $cond['cam_no'] != ""){
            $sql.= " AND B.cam_no = '".$cond['cam_no']."' ";
        }

        if($page_num == null || $page_num == ""){
            $page_num = 0;
        }

        $sql.= " ORDER BY B.cam_ymd DESC";
        if ($page_num != "" && $per_page != ""){
            $sql.= " LIMIT ".$page_num.",".$per_page;
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function select_sum_campaign_list($cond){
        $sql = " SELECT 
                    'strAll' AS row_nm, SUM(A.imp_cnt) AS imp, SUM(A.click_cnt) AS clk, (SUM(A.click_cnt) / SUM(A.imp_cnt) * 100) AS ctr, SUM(A.price) AS price 
                 FROM 
                    mountain.mo_report_campaign A
                 LEFT JOIN
                    mo_members B ON A.mem_no = B.mem_no
                 WHERE
                    A.mem_no = '".$cond['mem_no']."'
                 AND 
                    A.date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }

    function select_campaign_count($cond){
        $sql = " SELECT B.mem_nm ";
        $sql.= " FROM mountain.mo_campaign A LEFT JOIN mo_members B ON A.adver_no = B.mem_no WHERE A.adver_no = '".$cond['mem_no']."'";
        $sql.= " AND A.cam_fl='N' ";
        if(isset($cond['cam_status'])){
            if($cond['cam_status'][0] != 0){
                $cam_status=implode(',',$cond['cam_status']);
                $sql.= " AND A.cam_status IN ($cam_status) ";
            }
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    //여기서부터 수정
    function select_campaign_chart_data($cond){
    $sql = " SELECT
                C.date_ymd, IFNULL(C.imp,0) AS imp, IFNULL(C.clk,0) AS clk, IFNULL(C.clk / C.imp * 100,2) AS ctr, A.mem_com_nm adver_nm
             FROM 
                mo_members A
             LEFT JOIN
                mountain.mo_campaign B 
             ON 
                A.mem_no = B.adver_no 
             LEFT JOIN
                 (SELECT cam_no, date_ymd, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL(AVG(ctr),0) AS ctr 
                 FROM mountain.mo_report_campaign 
                 WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' group by date_ymd, cam_no
                 ) C ON (B.cam_no = C.cam_no) 
              WHERE
                  B.cam_fl = 'N'
              AND 
                  C.date_ymd != ''
              AND 
                  A.mem_no = '".$cond['mem_no']."'
              ";
        if(isset($cond['cam_status'])){
            if($cond['cam_status'][0] != 0){
                $cam_status=implode(',',$cond['cam_status']);
                $sql.= " AND B.cam_status IN ($cam_status) ";
            }
        }
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function select_campaign_info($cam_no){
        $sql = "SELECT
                    A.*, B.mem_com_nm
                AS
                    adver_nm
                FROM
                    mountain.mo_campaign A, mountain.mo_members B
                WHERE
                    A.cam_no = '".$cam_no."'
                AND
                    A.adver_no = B.mem_no ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }

    function insert_new_advertiser($data){
        $ist_query = $this->db->insert_string('mountain.mo_members', $data);
        $query = $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        return $ret;
        
    }
    
    function insert_new_member_group($data){

        $data_ = array(
            'agency_no'=>$data['agency_no']                
        );
        
        $where = " mem_no = '".$data['agency_no'] ."' ";
        $udt_query = $this->db->update_string('mo_members', $data_, $where);
        $this->db->query($udt_query);
        
        $ist_query = $this->db->insert_string('mountain.mo_members_group', $data);
        $query = $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        return $ret;
    }


    function sel_advertiser($mem_no){

        $logged_in = $this->session->userdata('logged_in');
        if($logged_in['mem_type'] == 'master'){
            $sql = "SELECT
                        A.adver_no, B.mem_com_nm
                    AS
                        adver_nm
                    FROM
                        mountain.mo_members_group A, mountain.mo_members B
                    WHERE
                        A.adver_no = B.mem_no
                    AND
                        A.agency_no = '".$mem_no."'
                    AND
                        A.adver_no != ''";
        
        }else if($logged_in['mem_type'] == 'manager'){
            $sql = "SELECT
                        A.adver_no, B.mem_com_nm
                    AS
                        adver_nm
                    FROM
                        mountain.mo_members_group A, mountain.mo_members B
                    WHERE
                        A.adver_no = B.mem_no
                    AND
                        A.manager_no = '".$mem_no."'";

        }
        
        $query = $this->db->query($sql);
        
        
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }


    function sel_campaign($mem_no){
        $sql = " SELECT ";
        $sql.= " A.cam_nm, A.cam_no";
        $sql.= " FROM ";
        $sql.= " mountain.mo_campaign A ";
        $sql.= " LEFT JOIN mo_members B ON A.adver_no = B.mem_no";
        $sql.= " WHERE A.adver_no = '".$mem_no."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
        
    function sel_creative_group($mem_no){
        $sql = "SELECT
                    A.cre_gp_no , A.cre_gp_nm
                FROM
                    mountain.mo_creative_group A
                LEFT JOIN
                    mountain.mo_campaign B
                ON
                    (
                        A.cam_no = B.cam_no
                    )
                LEFT JOIN
                    (
                        SELECT
                            *
                        FROM
                            mountain.mo_members
                        WHERE
                            mem_no = '".$mem_no."'
                    ) C
                ON
                    (
                        C.mem_no = B.adver_no
                    )
                ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function insert_campaign($data){

        $nm_check = $this->nm_check($data["cam_nm"]);
        if($nm_check == "true"){
            $ist_query = $this->db->insert_string('mountain.mo_campaign', $data);
            $this->db->query($ist_query);
            $ret = $this->db->insert_id();
        }else{
            $ret = 0;
        }

        return $ret;
    }
    
    function update_modify_campaign($data, $cam_no){
        $where = " cam_no = '".$cam_no."'";
        $udt_query = $this->db->update_string('mountain.mo_campaign', $data, $where);
        $this->db->query($udt_query);
        if ($this->db->affected_rows() > 0) {
            echo "ok";
        }else{
            echo "false";
        }
    }
    
    function campaign_list_count($cond){

        $sql = " SELECT
                    COUNT(1) AS all_cnt,
                    IFNULL(SUM(CASE WHEN cam_status = '1' THEN 1 ELSE 0 end), 0) AS run_cnt,
                    IFNULL(SUM(CASE WHEN cam_status = '2' THEN 1 ELSE 0 end), 0) AS ready_cnt,
                    IFNULL(SUM(CASE WHEN cam_status = '3' OR cam_status = '5' OR cam_status = '6' THEN 1 ELSE 0 end), 0) AS pause_cnt,
                    IFNULL(SUM(CASE WHEN cam_status = '4' THEN 1 ELSE 0 end), 0) AS done_cnt
                FROM mountain.mo_campaign A, mountain.mo_members B
                WHERE B.mem_no = A.adver_no AND A.cam_fl = 'N' AND B.mem_no = '".$cond['mem_no']."'";

        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    function campaign_status_change($data){
        
        $sql = " UPDATE mountain.mo_campaign SET ";
        if($data['status_key']=='Y'){
            $sql.= " cam_fl= '".$data['status_key']."' ";
        }else{
            $sql.= " cam_status= '".$data['status_key']."' ";
        }
        $sql.= " WHERE cam_no IN (".$data['cam_no_arr'].") ";
        $this->db->query($sql);
        
        if($data['status_key']=='Y'){
            $sel_cre_gp_no = " SELECT cre_gp_no FROM mountain.mo_creative_group ";
            $sel_cre_gp_no.= " WHERE cam_no IN (".$data['cam_no_arr'].") ";
            $sel_query= $this->db->query($sel_cre_gp_no);
            if ($sel_query->num_rows() > 0) {
                $sql = " UPDATE mountain.mo_creative_group SET ";
                $sql.= " cre_gp_fl= 'Y' ";
                $sql.= " WHERE cam_no IN (".$data['cam_no_arr'].") ";
                $query = $this->db->query($sql);
                
                foreach ($sel_query->result_array() as $row) {
                    $sql = " UPDATE mountain.mo_creative SET ";
                    $sql.= " cre_fl= 'Y' ";
                    $sql.= " WHERE cre_gp_no='".$row['cre_gp_no']."'";
                    $query = $this->db->query($sql);
                }
            }
        }else{
            $sel_cre_gp_no = " SELECT cre_gp_no FROM mountain.mo_creative_group ";
            $sel_cre_gp_no.= " WHERE cam_no IN (".$data['cam_no_arr'].") ";
            $sel_query= $this->db->query($sel_cre_gp_no);
            if ($sel_query->num_rows() > 0) {
                $sql = " UPDATE mountain.mo_creative_group SET ";
                $sql.= " cre_gp_status= '".$data['status_key']."' ";
                $sql.= " WHERE cam_no IN (".$data['cam_no_arr'].") ";
                $query = $this->db->query($sql);
            
                foreach ($sel_query->result_array() as $row) {
                    $sql = " UPDATE mountain.mo_creative SET ";
                    $sql.= " cre_status= '".$data['status_key']."' ";
                    $sql.= " WHERE cre_gp_no='".$row['cre_gp_no']."'";
                    $query = $this->db->query($sql);
                }
            }
        }
        
        if ($this->db->affected_rows() > 0) {
            return "ok";
        }else{
            return "false";
        }
    }
    
    function select_dashboard_campaign_chart_data($cond){
        $sql = " SELECT C.date_ymd, IFNULL(C.imp,0) AS imp, IFNULL(C.clk,0) AS clk, IFNULL(C.clk / C.imp * 100,2) AS ctr ";
        $sql.= " FROM ";
        $sql.= " ( SELECT mem_no FROM mountain.mo_members WHERE mem_no = '".$cond['mem_no']."' AND mem_no != '' ) A ";
        $sql.= " LEFT JOIN ";
        $sql.= " mountain.mo_campaign B ON (A.mem_no = B.adver_no) ";
        $sql.= " LEFT JOIN ";
        $sql.= " (SELECT cam_no, date_ymd, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL(AVG(ctr),0) AS ctr ";
        $sql.= " FROM mountain.mo_report_campaign ";
        $sql.= " WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' "; 
        if($cond['cam_no']!=""){
            $sql.= " AND cam_no='".$cond['cam_no']."' ";
        }
        $sql.= " GROUP BY date_ymd, cam_no) C ON (C.cam_no=B.cam_no) ";
        $sql.= " WHERE B.cam_fl = 'N' AND C.date_ymd != '' ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $result[] = $row;
            }
            return $result;
        }
    }
    
    function select_dashboard_campaign_summary_data($cond){
        $sql = " SELECT ";
        $sql.= " IFNULL(A.imp,0) AS imp, IFNULL((A.imp / B.imp * 100) - 100,0) AS imp_rate, ";
        $sql.= " IFNULL(A.clk,0) AS clk, IFNULL((A.clk / B.clk * 100) - 100,0) AS clk_rate, ";
        $sql.= " A.ctr, (A.ctr-B.ctr) AS ctr_rate, B.ctr AS last_ctr, ";
        $sql.= " (A.price / A.clk) AS ppc, (((A.price / A.clk) / (B.price / B.clk)) * 100) - 100   AS ppc_rate, ";
        $sql.= " (A.price / A.imp) AS ppi, (((A.price / A.imp) / (B.price / B.imp)) * 100) - 100   AS ppi_rate, ";
        $sql.= " A.price, IFNULL((A.price / B.price * 100) - 100,0) AS price_rate ";
        $sql.= " FROM ";
        $sql.= " (SELECT date_ymd, SUM(imp_cnt) AS imp, SUM(click_cnt) AS clk, (SUM(click_cnt) / SUM(imp_cnt) * 100) AS ctr, SUM(loc_price) AS price ";
        $sql.= " FROM mountain.mo_report_campaign ";
        $sql.= " WHERE date_ymd BETWEEN '".$cond['from_date']."' AND '".$cond['to_date']."' ";
        
        //if (isset($adver_no_list)){
        //     $sql.= " AND mem_no IN (".$adver_no_list.") ";
        //}else{
            $sql.= " AND mem_no = '".$cond['mem_no']."' ";
        //}
        if($cond['cam_no'] != ""){
            $sql.= " AND cam_no = '".$cond['cam_no']."' ";
        }
        $sql.= " ) A, ";
        $sql.= " (SELECT date_ymd, IFNULL(SUM(imp_cnt),0) AS imp, IFNULL(SUM(click_cnt),0) AS clk, IFNULL((SUM(click_cnt) / SUM(imp_cnt) * 100),0) AS ctr, IFNULL(SUM(price),0) AS price ";
        $sql.= " FROM mountain.mo_report_campaign ";
        $sql.= " WHERE date_ymd BETWEEN '".$cond['before_from_date']."' AND '".$cond['before_to_date']."' ";
        //if (isset($adver_no_list)){
        //    $sql.= " AND mem_no IN (".$adver_no_list.") ";
        //}else{
            $sql.= " AND mem_no = '".$cond['mem_no']."' ";
        //}
        if($cond['cam_no'] != ""){
            $sql.= " AND cam_no = '".$cond['cam_no']."' ";
        }
        $sql.= " ) B ";
     
        $result = $this->db->query($sql);
        
        
        if ($result->num_rows() > 0) {
            $row = $result->result_array();
            return $row[0];
        }
    }
    
    public function select_master_no($mem_no){
        
        $this->db->select("agency_no");
        $this->db->where('manager_no', $mem_no);
        $this->db->from('mo_members_group');
        $query = $this->db->get();
        
        if( $query->num_rows() > 0 ){
            $row = $query->row();
            $agency_no = $row->agency_no;
        
            return $agency_no;
        }
    }
    
}

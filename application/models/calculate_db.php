<?php
class Calculate_Db extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function select_exchange_list($condition, $per_page, $page_num){

        if($condition['term'] == null){
            $this->db->select("*");
            $this->db->from('mo_exchange');

            if($condition['from_code_key'] != null){
                $this->db->where("from_code_key = '".$condition['from_code_key']."'");
            }

            $this->db->order_by('input_dt', 'DESC');
            $this->db->limit($per_page, $page_num);
            $query = $this->db->get();

        }elseif($condition['term'] == "month"){
            if($page_num == null){
                $page_num = 0;
            }
            $sql = "SELECT SUBSTR(input_dt, '1', '7') input_dt, AVG(exchange_rate) exchange_rate";
            $sql .= " FROM mo_exchange";

            if($condition['from_code_key'] != null){
                $sql .= " WHERE from_code_key = '".$condition['from_code_key']."'";
            }
            $sql .= " GROUP BY SUBSTR(input_dt, '1', '7')";
            $sql .= " ORDER BY input_dt DESC";
            $sql .= " LIMIT ".$page_num.",".$per_page;

            $query = $this->db->query($sql);
        }

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }

    public function select_exchange_list_count($condition){

        if($condition['term'] == null){
            $this->db->select("*");
            $this->db->from('mo_exchange');

            if($condition['from_code_key'] != null){
                $this->db->where("from_code_key = '".$condition['from_code_key']."'");
            }

            $query = $this->db->get();

        }elseif($condition['term'] == "month"){

            $sql = "SELECT SUBSTR(input_dt, '1', '7') input_dt, AVG(exchange_rate) exchange_rate";
            $sql .= " FROM mo_exchange";

            if($condition['from_code_key'] != null){
                $sql .= " WHERE from_code_key = '".$condition['from_code_key']."'";
            }
            $sql .= " GROUP BY SUBSTR(input_dt, '1', '7')";

            $query = $this->db->query($sql);
        }


        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }

    
    public function select_fee_list($condition, $per_page, $page_num){

        $sql = " SELECT A.mem_com_fee, A.mem_no, SUBSTR(A.fee_apply_dt, '1', '10') fee_apply_dt, A.mem_com_nm, A.role, (SELECT mem_nm FROM mo_members WHERE B.sales_no = mem_no) AS sales_nm ";
        $sql.= " FROM mo_members A LEFT JOIN ";
        $sql.= " mo_members_group B ON (B.adver_no = A.mem_no ) ";
        $sql.= " WHERE A.admin_fl != 'Y' ";
        $sql.= " AND A.mem_fl != 'Y' ";

        if($condition['role'] == "all"){
            $sql .= " AND (A.role = 'agency' OR A.role = 'lab')";
        }else if($condition['role'] == "agency"){
            $sql .= " AND A.role = 'agency'";
        }else if($condition['role'] == "lab"){
            $sql .= " AND A.role = 'lab'";
        }

        $sql .= " AND A.role != 'sales'";
        $sql .= " AND A.role != 'ind'";
        $sql .= " AND A.role != 'adver'";
        $sql .= " AND A.mem_type = 'master'";
        
        if($condition['sales_nm'] != ''){
            $sql.= " AND sales_nm LIKE '%".$condition['sales_nm']."%' ";
        }

        if($condition['mem_com_nm'] != ''){
            $sql.= " AND A.mem_com_nm LIKE '%".$condition['mem_com_nm']."%' ";
        }

        if ($condition["fromto_date"] != ""){
            $sql.= " AND A.fee_apply_dt BETWEEN '".$condition['from_date']."' AND '".$condition['to_date']."' ";
        }

        $sql .= " ORDER BY A.mem_ymd DESC";
        $sql .= " LIMIT ".$page_num.",".$per_page;
        $query = $this->db->query($sql);

        //print_r($this->db->last_query());

        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }

    }

    
    public function select_fee_list_count($condition){

        $sql = " SELECT SUBSTR(A.fee_apply_dt, '1', '10') fee_apply_dt, A.mem_com_nm, A.role, (SELECT mem_nm FROM mo_members WHERE B.sales_no = mem_no) AS sales_nm ";
        $sql.= " FROM mo_members A LEFT JOIN ";
        $sql.= " mo_members_group B ON (B.adver_no = A.mem_no ) ";
        $sql.= " WHERE A.admin_fl != 'Y' ";
        $sql.= " AND A.mem_fl != 'Y' ";

        if($condition['role'] == "all"){
            $sql .= " AND A.role = 'agency' OR A.role = 'lab'";

        }else if($condition['role'] == "agency"){
            $sql .= " AND A.role = 'agency'";

        }else if($condition['role'] == "lab"){
            $sql .= " AND A.role = 'lab'";

        }

        $sql .= " AND A.role != 'sales'";
        $sql .= " AND A.role != 'ind'";
        $sql .= " AND A.role != 'adver'";

        if($condition['sales_nm'] != ''){
            $sql.= " AND sales_nm LIKE '%".$condition['sales_nm']."%' ";
        }

        if($condition['mem_com_nm'] != ''){
            $sql.= " AND A.mem_com_nm LIKE '%".$condition['mem_com_nm']."%' ";
        }

        if ($condition["fromto_date"] != ""){
            $sql.= " AND A.fee_apply_dt BETWEEN '".$condition['from_date']."' AND '".$condition['to_date']."' ";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }

    function modify_fee($mem_no, $mem_com_fee){

        $data = array(
            'mem_com_fee' => $mem_com_fee,
            'fee_apply_dt' => date("Y-m-d H:i:s")
        );

        $where = " agency_no = '".$mem_no."'";
        $sql = $this->db->update_string('mo_members', $data, $where);
        $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
            echo "ok";
        }else{
            echo "false";
        }
    }
    
    public function select_tax_bill_list($cond, $per_page, $page_num){
        if($page_num == null){
            $page_num = 0;
        }
    
        $sql = " SELECT A.*, B.*, A.mem_no AS member_no, ";
        $sql.= " IFNULL((SELECT IF(forward_fl = 'Y', 'Y', 'N') FROM mountain.mo_calculate_tax WHERE mem_no = A.mem_no AND forward_fl='Y' LIMIT 1),'N')  AS forward_fl ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members A LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_calculate_tax_history WHERE date_ym = '".$cond['year']."-".$cond['month']."' ) B ON (A.mem_no = B.mem_no)";
        $sql.= " WHERE A.role IN ('adver', 'agency', 'lab') ";
    
        if ($cond['adver_yn'] == "on"){
            $sql.= " AND A.role = 'adver' ";
        }
        if ($cond['agency_yn'] == "on"){
            $sql.= " AND A.role = 'agency' ";
        }
        if ($cond['lap_yn'] == "on"){
            $sql.= " AND A.role = 'lab' ";
        }
        if ($cond['publication_st'][0] != 0){
            $publication_st = implode(',',$cond['publication_st']);
            $sql.= " AND B.publish_fl IN ($publication_st) ";
        }
        
        $sql.= " AND A.mem_type = 'master' ";
        
        
        $sql .= " LIMIT ".$page_num.",".$per_page;
        
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    public function select_tax_bill_count($cond){
        $sql = " SELECT A.mem_no ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members A LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_calculate_tax_history WHERE date_ym = '".$cond['year']."-".$cond['month']."' ) B ON (A.mem_no = B.mem_no)";
        $sql.= " WHERE A.role IN ('adver', 'agency', 'lab') ";
    
        if ($cond['adver_yn'] == "on"){
            $sql.= " AND role = 'adver' ";
        }
        if ($cond['agency_yn'] == "on"){
            $sql.= " AND role = 'agency' ";
        }
        if ($cond['lap_yn'] == "on"){
            $sql.= " AND role = 'lab' ";
        }
        if ($cond['publication_st'][0] != 0){
            $publication_st = implode(',',$cond['publication_st']);
            $sql.= " AND publish_fl IN ($publication_st) ";
        }
    
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    
    public function select_tax_bill_summary($cond){
        $sql = " SELECT ";
        $sql.= " COUNT(A.mem_no) AS all_cnt, ";
        $sql.= " IFNULL(SUM(IF(B.publish_fl = 1, 1, IF(B.publish_fl is NULL, 1, 0))),0) AS no_cnt, ";
        $sql.= " IFNULL(SUM(IF(B.publish_fl = 2, 1, 0)),0) AS wait_cnt, ";
        $sql.= " IFNULL(SUM(IF(B.publish_fl = 3, 1, 0)),0) AS carryout_cnt, ";
        $sql.= " IFNULL(SUM(IF(B.publish_fl = 4, 1, 0)),0) AS done_cnt ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_members A LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_calculate_tax_history WHERE date_ym = '".$cond['year']."-".$cond['month']."' ) B ON (A.mem_no = B.mem_no)";
        $sql.= " WHERE A.mem_type = 'master' AND A.role IN ('adver', 'agency', 'lab')";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function select_tax_bill_view($mem_no, $date_ym){
        $sql = " SELECT A.*, B.*, A.mem_no AS member_no FROM  ";
        $sql.= " mountain.mo_members A LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_calculate_tax_history WHERE date_ym = '".$date_ym."') B ON A.mem_no = B.mem_no ";
        $sql.= " WHERE A.mem_no = '".$mem_no."' ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
    }
    
    public function select_tax_bill_detail_list($mem_no, $date_ym){
        $sql = " SELECT A.*, SUM(ROUND(A.loc_price)) AS loc_price";
        $sql.= " FROM ";
        $sql.= " mountain.mo_calculate_tax A LEFT JOIN ";
        $sql.= " (SELECT * FROM mountain.mo_calculate_tax_history WHERE date_ym = '".$date_ym."' ) B ON A.agency_no = B.mem_no ";
        $sql.= " WHERE A.agency_no = '".$mem_no."' AND (A.publish_fl != '4' OR A.history_no = B.history_no ) GROUP BY A.date_ym, A.agency_no";
        $sql.= " ORDER BY A.date_ym";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    public function select_tax_bill_detail_manager_list($mem_no, $date_ym){
        $sql = " SELECT *";
        $sql.= " FROM ";
        $sql.= " mountain.mo_calculate_tax ";
        $sql.= " WHERE agency_no = '".$mem_no."' AND date_ym = '".$date_ym."'";
        $sql.= " ORDER BY mem_type desc";
         
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    public function select_tax_bill_publish_list($mem_no, $date_ym){
        /*
        $sql = " SELECT * FROM  ";
        $sql.= " mountain.mo_calculate_tax  ";
        $sql.= " WHERE history_no = (SELECT history_no FROM mountain.mo_calculate_tax_history WHERE mem_no = '$mem_no' AND date_ym = '$date_ym') ";
        $sql.= " AND publish_fl IN ('4','2') GROUP BY date_ym, agency_no ORDER BY date_ym";
        */
        $sql = " SELECT * FROM  ";
        $sql.= " mountain.mo_calculate_tax_history  ";
        $sql.= " WHERE mem_no = '".$mem_no."' AND date_ym = '".$date_ym."'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    

    public function insert_calculate_tax_history($data){
        $ist_query = $this->db->insert_string('mountain.mo_calculate_tax_history', $data);
        $query = $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    public function update_calculate_tax_history($data, $history_no){
        $where = " history_no = '$history_no' ";
        $udt_query = $this->db->update_string('mountain.mo_calculate_tax_history', $data, $where);
        $this->db->query($udt_query);
    }
    
    
    public function update_publish_calculate_tax($chk_cal_tax_no, $history_no, $date_ym, $mem_no){
        $cal_tax_no = implode(',', $chk_cal_tax_no);
        $sql = " UPDATE mountain.mo_calculate_tax SET ";
        $sql.= " publish_fl = '4', history_no = '$history_no', publish_dt = '$date_ym' ";
        $sql.= " WHERE calculate_tax_no IN ($cal_tax_no) ";
        $this->db->query($sql);
        
        $sql = " UPDATE mountain.mo_calculate_tax SET ";
        $sql.= " publish_fl = '4', history_no = '$history_no', publish_dt = '$date_ym' ";
        $sql.= " WHERE agency_no = '$mem_no' AND date_ym = '$date_ym' ";
        $this->db->query($sql);
    }
    
    public function update_temp_calculate_tax($chk_cal_tax_no, $history_no){
        $cal_tax_no = implode(',',$chk_cal_tax_no);
        $sql = " UPDATE mountain.mo_calculate_tax SET ";
        $sql.= " publish_fl = '2', history_no = '$history_no' ";
        $sql.= " WHERE calculate_tax_no IN ($cal_tax_no) ";
        $this->db->query($sql);
    }
    
    public function update_tax_bill_detail($data, $cal_tax_no){
    
        
        if($data['publish_fl'] == 1){
        
            $this->db->where('mem_no', $data['agency_no']);
            $this->db->where('date_ym', $data['date_ym']);
            $this->db->delete('mountain.mo_calculate_tax_history');

        }
        
        $where = " calculate_tax_no = '".$cal_tax_no."' ";
        $query = $this->db->update_string('mountain.mo_calculate_tax', $data, $where);
        $this->db->query($query);
        $ret = $this->db->affected_rows();

        return $ret;
    }
    
    public function insert_tax_bill_detail($data){
        $sql = " SELECT * FROM mountain.mo_members WHERE mem_no = '".$data['mem_no']."' ";
        $result = $this->db->query($sql);
        $row = $result->row();
        
        $data['mem_com_nm'] = $row->mem_com_nm;
        $data['mem_com_no'] = $row->mem_com_no;
        $data['role'] = $row->role;
        
        $ist_query = $this->db->insert_string('mountain.mo_calculate_tax', $data);
        $this->db->query($ist_query);
        $ret = $this->db->insert_id();
        return $ret;
    }
    
    public function update_calculate_tax_carry_over($mem_no){
        $sql = " UPDATE mountain.mo_calculate_tax SET ";
        $sql.= " publish_fl = '3', forward_fl='Y' ";
        $sql.= " WHERE mem_no = '$mem_no' AND publish_fl != '4' ";
        $query = $this->db->query($sql);
        
    }
    
    
    public function select_revenue_intergration_list($mem_no, $term, $from_date, $to_date){
    
        $this->db->select("date_ymd, mem_com_nm, SUM(pre_deposit) AS pre_deposit ");
        $this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
        $this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
        $this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
        $this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
        $this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
        $this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
        $this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
        $this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
        $this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_revenue');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($term == "day"){
            $this->db->group_by("day");
        }else if($term == "week"){
            $this->db->group_by("week");
        }else if($term == "month"){
            $this->db->group_by("month");
        }
    
        $query = $this->db->get();
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_revenue_business_list($data, $from_date, $to_date){
    
        if($data['datatype'] == "time"){
            $this->db->select("date_ymd");
        }else if($data['datatype'] == "role"){
            $this->db->select("group_role role");
        }else if($data['datatype'] == "role_kind"){
            $this->db->select("MIN(mem_no) mem_no, mem_com_nm, group_role role");
        }else if($data['datatype'] == "group"){
            $this->db->select("mem_id, mem_no, mem_com_nm, mem_type");
        }
    
        $this->db->select("SUM(pre_deposit) AS pre_deposit ");
        $this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
        $this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
        $this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
        $this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
        $this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
        $this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
        $this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
        $this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
        $this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_revenue');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($data['datatype'] == "time"){
            if($term == "day"){
                $this->db->group_by("day");
            }else if($term == "week"){
                $this->db->group_by("week");
            }else if($term == "month"){
                $this->db->group_by("month");
            }
        }else if($data['datatype'] == "role"){
            $this->db->group_by("group_role");
        }else if($data['datatype'] == "role_kind"){
            $this->db->where('group_role', $data['datakey']);
            $this->db->group_by("agency_no");
        }else if($data['datatype'] == "group"){
            $this->db->where('agency_no', $data['datakey']);
            $this->db->group_by("mem_no");
        }
      
        if($data['datatype'] == "role" || $data['datatype'] == "role_kind"){
            $this->db->order_by("price", "desc");
        }
    
        $query = $this->db->get();
    
        //print_r($this->db->last_query());
        //exit;
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    

    public function select_deposit_summary(){
        $sql = " SELECT  SUM(loc_price) AS all_deposit, ";
        $sql.= " SUM(IF(mem_pay_later = 'Y' , mem_cash, 0 )) AS post_deposit, ";
        $sql.= " SUM(IF(mem_pay_later = 'N' , mem_cash, 0 )) AS pre_deposit ";
        $sql.= " FROM mountain.mo_revenue ";
        $result = $this->db->query($sql);
        $row = $result->result_array();
        return $row[0];
        
    }
    
    public function select_pre_deposit_list($cond){
        $sql = " SELECT A.mem_no, A.mem_id, A.mem_nm, A.mem_com_nm, A.role, B.first_date, C.last_date, D.charge_cash, E.result_price ";
        $sql.= " FROM ";
        $sql.= " (SELECT mem_no, mem_nm, mem_id, mem_com_nm, role, mem_fl FROM mountain.mo_members WHERE mem_pay_later='N') A LEFT JOIN ";
        $sql.= " (SELECT mem_no, charge_dt AS first_date FROM mountain.mo_cash_charge WHERE  charge_st='C' ORDER BY charge_dt ASC) B ON (B.mem_no = A.mem_no) LEFT JOIN ";
        $sql.= " (SELECT mem_no, charge_dt AS last_date FROM mountain.mo_cash_charge WHERE  charge_st='C' ORDER BY charge_dt DESC) C ON (C.mem_no = A.mem_no) LEFT JOIN ";
        $sql.= " (SELECT mem_no, SUM(charge_cash) AS charge_cash FROM mountain.mo_cash_charge WHERE charge_st='C'  GROUP BY mem_no) D ON (D.mem_no = A.mem_no) LEFT JOIN";
        $sql.= " (SELECT mem_no, result_price, publish_fl FROM mountain.mo_calculate_tax_history ORDER BY date_ym DESC) E ON (E.mem_no = A.mem_no) ";
        $sql.= " WHERE A.mem_fl = 'N' ";
        
        if ($cond['type'] != "all"){
            $sql.= " AND A.role = '".$cond['type']."' ";
        }
        
        if ($cond['mem_com_nm'] != "" ){
            $sql.= " AND A.mem_com_nm LIKE '%".$cond['mem_com_nm']."%' ";
        }
        
        if ($cond['charge_way'] != "all" ){
            $sql.= " AND A.charge_way LIKE '%".$cond['charge_way']."%' ";
        }
        
        if ($cond['fromto_date'] != "" ){
            $sql.= " AND B.first_date >= '".$cond['from_date']."' ";
            $sql.= " AND C.last_date <= '".$cond['to_date']."' ";
        }
        
        if ($cond['publish_fl'] != "all" ){
            $sql.= " AND E.publish_fl = '".$cond['publish_fl']."' ";
        }
        
        $sql.= " GROUP BY A.mem_no ";
        
        $result = $this->db->query($sql);
        
        if ($result->num_rows() > 0) {
            return $result->result_array();
        }else{
            return array();
        }
    }
    
    //후불매출 = 미수금
    public function select_no_paid_list($cond){
        
        $result = array();
        $this->db->select("mem_no, SUM(loc_price_lt) loc_price_lt");
        $this->db->from('mo_revenue');
        $this->db->where("mem_pay_later", "Y");
        
        if($cond['mem_com_nm'] != null){
            $this->db->where("mem_com_nm LIKE '%".$cond['mem_com_nm']."%'" );
        }
        
        $type = implode(',',$cond['type']);
        
        if($cond['type'] != null){
            $this->db->where("group_role IN (".$type.")");
        }
        
        $this->db->group_by("mem_no");
        $this->db->order_by("agency_no");
        $this->db->order_by("mem_type desc");
        $query = $this->db->get();        
        if ( $query->num_rows() > 0 ){
            $mem_info = $query->result_array();
            
            foreach($mem_info as $key=>$mem){
                $result[$key]['mem_no'] = $mem['mem_no'];
                $result[$key]['loc_price_lt'] = $mem['loc_price_lt'];
                
                $this->db->select("*");
                $this->db->from('mo_members');
                $this->db->where("mem_no", $result[$key]['mem_no']);
                $query = $this->db->get();
                $rows = $query->row();
                
                $result[$key]['mem_id'] = $rows->mem_id;
                $result[$key]['mem_type'] = $rows->mem_type;
                $result[$key]['mem_com_nm'] = $rows->mem_com_nm;
                $result[$key]['role'] = $rows->role;
                
            }

            return $result;
        }else{
            return array();
        }
    }
    
    public function select_no_paid_list_count($cond){
        $sql = " SELECT A.mem_no ";
        $sql.= " FROM ";
        $sql.= " mountain.mo_receivable A LEFT JOIN ";
        $sql.= " mountain.mo_members B ON (A.mem_no = B.mem_no) ";
        if (count($cond['type']) == 1){
            $type = "'adver', 'agency', 'lab'";
            
        }else{
            $type = implode(',',$cond['type']);
        }
        
        $sql.= " WHERE B.role IN ($type) ";
        
        if ($cond['mem_com_nm'] != "" ){
            $sql.= " AND B.mem_com_nm LIKE '%".$cond['mem_com_nm']."%' ";
        }
    
        $sql.= " GROUP BY A.mem_no ";
    
        $result = $this->db->query($sql);
    
        if ($result->num_rows() > 0) {
            return $result->num_rows();
        }else{
            return 0;
        }
    }
    
    function select_revenue_daily_list($data, $term, $from_date, $to_date){
    
        $this->db->select("date_ymd, mem_no, mem_com_nm, group_role, group_nm");
        $this->db->select("SUM(pre_deposit) AS pre_deposit ");
        $this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
        $this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
        $this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
        $this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
        $this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
        $this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
        $this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
        $this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
        $this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_revenue');
        //$this->db->where_in('mem_no', $adver_no);
    
        if($data['datatype'] == "role"){
            $this->db->where('group_role', $data['datakey']);
        }else if($data['datatype'] == "role_kind"){
            $this->db->where('agency_no', $data['datakey']);
        }else if($data['datatype'] == "group"){
            $this->db->where('mem_no', $data['datakey']);
        }
    
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($term == "day"){
            $this->db->group_by("day");
        }else if($term == "week"){
            $this->db->group_by("week");
        }else if($term == "month"){
            $this->db->group_by("month");
        }
    
        $query = $this->db->get();
    
        //print_r($this->db->last_query());
    
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    
    public function insert_receivable($data){
        //미수금에 해당업체가 있는지 확인
        $sel_sql = " SELECT * FROM mountain.mo_receivable WHERE mem_no = '".$data['mem_no']."' ";
        $result = $this->db->query($sel_sql);
        //있을경우
        if ($result->num_rows() > 0) {
            //미수금 내역 업댓
            $udt_sql = " UPDATE mountain.mo_receivable SET loc_price = loc_price+".$data['result_loc_price']." WHERE mem_no = '".$data['mem_no']."' ";
            $this->db->query($udt_sql);
        //없을경우
        }else{
            //업체정보 불러오기
            $sql = " SELECT * FROM mountain.mo_members WHERE mem_no = '".$data['mem_no']."' ";
            $query = $this->db->query($sql);
            $row = $query->row();
            //미수금 내역에 INSERT
            $ist_sql = " INSERT INTO mountain.mo_receivable (mem_no, mem_com_nm, group_role, recent_ymd, loc_price) ";
            $ist_sql.= " VALUES ";
            $ist_sql.= " ('".$data['mem_no']."', '$row->mem_com_nm', '$row->role', '".date('Y-m-d')."', '".$data['result_loc_price']."')";
            $this->db->query($ist_sql);
        }
        //미수금 상세내역에 추가
        $ist_sql = " INSERT INTO mountain.mo_receivable_detail (detail_fl, detail_amount, insert_ymd, mem_no) ";
        $ist_sql.= " VALUES ";
        $ist_sql.= " ('1', '".$data['result_loc_price']."', '".date('Y-m-d')."', '".$data['mem_no']."')";
        $this->db->query($ist_sql);
        
    }
    
    public function insert_receivable_detail($data, $detail_fl){
        if ($data['insert_ymd'] == ""){
            $insert_ymd = date('Y-m-d');
        }else{
            $insert_ymd = $data['insert_ymd'];
        }
        //미수금 업댓
        $udt_sql = " UPDATE mountain.mo_receivable SET loc_price = loc_price-".$data['paid_amount']." WHERE mem_no = '".$data['mem_no']."' ";
        $this->db->query($udt_sql);
        
        //미수금 상세내역 INSERT
        $ist_sql = " INSERT INTO mountain.mo_receivable_detail (detail_fl, detail_amount, insert_ymd, mem_no, cont) ";
        $ist_sql.= " VALUES ";
        $ist_sql.= " ('$detail_fl', '".$data['paid_amount']."', '$insert_ymd', '".$data['mem_no']."', '".$data['cont']."')";
        $this->db->query($ist_sql);
        //미수금 전액 입금시
        if ($data['no_paid_amount'] == '0'){
            //해당 업체 미수금 정산서 없애기
            $sql = " UPDATE mountain.mo_calculate_tax SET publish_fl = '4' WHERE mem_no = '".$data['mem_no']."' AND mem_pay_later = 'Y' ";
            $query = $this->db->query($sql);
        //미수금 일부입금시
        }else{
            //회원정보 가져오기
            $sql = " SELECT * FROM mountain.mo_members WHERE mem_no = '".$data['mem_no']."' ";
            $query = $this->db->query($sql);
            $row = $query->row();
            
            //정산내역에 미수금 추가
            $ist_sql = " INSERT INTO mountain.mo_calculate_tax (mem_no, mem_com_nm, date_ym, role, mem_com_no, tax_cont, loc_price, publish_fl, mem_pay_later) ";
            $ist_sql.= " VALUES ";
            $ist_sql.= " ('".$data['mem_no']."', '$row->mem_com_nm', '".date('Y-m-d')."', '$row->role', '$row->mem_com_no', '미수금', '".$data['no_paid_amount']/1.1."', '1', 'Y')";
            
            $this->db->query($ist_sql);
        }
    }
    

    
    public function select_no_paid_detail_list($mem_no){
        
        $result = array();
        
        $sql = " SELECT mem_no, detail_amount, insert_ymd, detail_fl, cont ";
        $sql.= " FROM mountain.mo_receivable_detail ";
        $sql.= " WHERE mem_no = '$mem_no' AND detail_fl = '2' ORDER BY insert_ymd, detail_fl ";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            
            $mem_info = $query->result_array();
            
            foreach($mem_info as $key=>$mem){
                $result[$key]['mem_no'] = $mem['mem_no'];
                $result[$key]['detail_amount'] = $mem['detail_amount'];
                $result[$key]['insert_ymd'] = $mem['insert_ymd'];
                $result[$key]['detail_fl'] = $mem['detail_fl'];
                $result[$key]['cont'] = $mem['cont'];
            
                $this->db->select("*");
                $this->db->from('mo_members');
                $this->db->where("mem_no", $result[$key]['mem_no']);
                $query = $this->db->get();
                $rows = $query->row();
            
                $result[$key]['mem_id'] = $rows->mem_id;
                $result[$key]['mem_type'] = $rows->mem_type;
                $result[$key]['mem_com_nm'] = $rows->mem_com_nm;
                $result[$key]['role'] = $rows->role;
            
            }
            
            return $result;
            
        }else{
            return array();
        }
    }
    
    public function select_sum_loc_price_lt($mem_no){
        $this->db->select("SUM(loc_price_lt) AS loc_price_lt ");
        $this->db->from("mo_revenue");
        $this->db->where("mem_no", $mem_no);
        $query = $this->db->get();
        $rows = $query->row();
    
        if($query->num_rows() > 0){
            return $rows->loc_price_lt;
        }else{
            return 0;
        }
    }
    
    public function select_sum_deposit_price($mem_no){
        $this->db->select("SUM(detail_amount) AS detail_amount ");
        $this->db->from("mo_receivable_detail");
        $this->db->where("detail_fl", "2");
        $this->db->where("mem_no", $mem_no);
        $query = $this->db->get();
        $rows = $query->row();
        
        if($query->num_rows() > 0){
            return $rows->detail_amount;
        }else{
            return 0;
        }
    }
}

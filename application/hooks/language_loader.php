<?php
/**
 * Created by PhpStorm.
 * User: adop
 * Date: 2014. 6. 19.
 * Time: 오후 1:03
 */
class Language_Loader
{
    function initialize() {
        $ci =& get_instance();
        $ci->load->helper('language');

        $site_lang = $ci->session->userdata('site_lang');
        if ($site_lang) {
            $ci->lang->load('word',$ci->session->userdata('site_lang'));
        } else {
            $ci->session->set_userdata('site_lang', $ci->config->config['language']);
            $ci->lang->load('word', $ci->config->config['language']);
        }

    }
}
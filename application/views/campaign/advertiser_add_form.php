<div class="modal-header">
    <h4 class="h4_blit"><?php echo lang('strAdvertiser')?> <?php echo lang('strAdd')?></h4>
</div>
<div class="modal-body">
    <form name="advertiser_add" action="#">
        <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
        <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
            <tr>
                <th><?php echo lang('strAdvertiser')?> <?php echo lang('strName')?></th>
                <td>
                    <input type="text" name="adver_com_nm" id="adver_com_nm">
                </td>
            </tr>
        </table>
        <input type="checkbox" id="detail_info_sel" onclick="adver_detail_info();" ><?php echo lang('strNewAdvertiserInfoEntry');?>
        <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" id="adver_detail_info" style="display:none">
            <tr>
                <th><?php echo lang('strManagerName')?></th>
                <td>
                    <input type="text" name="adver_nm" id="adver_nm">
                </td>
            <tr>
                <th><?php echo lang('strEmail')?></th>
                <td>
                    <input type="text" name="adver_email" id="adver_email">
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strOfficeNumber')?></th>
                <td>
                    <input type="text" name="adver_tel_no" id="adver_tel_no">
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strMemo')?></th>
                <td>
                    <input type="text" name="mem_cont" id="mem_cont">
                </td>
            </tr>
        </table>
    </form>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10">
    <div class="join_bt_group">
        <span onclick="new_advertiser_save();" class="btn btn-primary" id="id_find_step1_next_btn"><?php echo lang('strSave')?></span>
        <span data-dismiss="modal" class="btn btn-primary"><?php echo lang('strClose')?></span>
    </div>
</div>
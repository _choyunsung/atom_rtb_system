<?php 
    $mem_no = $this->session->userdata('mem_no');
?>
<!-- 광고주 만들기 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content_add"></div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAdMgr')?></a></li>
			<li class="active"><?php echo lang('strAddNewCampaign')?></li>
		</ol>
	</div>	
  <!--	<div style="text-align:center; margin-top:10px; width:100%;">
		<img src="/img/step_02.png">
	</div>-->
 	<div class="panel-body ma_t10 center step_frm">
 	<dl class="f-color-p f-weight-b bd_p bg_w box_shadow">
 	<dt>step.01</dt>
 	<dd><?php echo lang('strNewCampaign')?></dd>
 	</dl>
 	<dl>
 	<dt>step.02</dt>
 	<dd><?php echo lang('strNewAdGroup')?></dd>
 	</dl>
 	<dl>
 	<dt>step.03</dt>
 	<dd><?php echo lang('strSetTargeting')?></dd>
 	</dl>
 	<dl>
 	<dt>step.04</dt>
 	<dd><?php echo lang('strNewAd')?></dd>
 	</dl>
	</div> 
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strAddNewCampaign')?></h3>
	</div>
	<div class="panel-body">
        <div class="clear">
            <form name="campaign_add" action="#">
                <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
                <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="20%">
                	<col width="*">
                </colgroup>
                    <?php
                        //대행사나 랩사만 광고주 추가
                        //if($fee_fl == "Y" ){
                    ?>
                    <!--
                    <tr>
                        <th><?php echo lang('strAdvertiser')?> <?php echo lang('strSelect')?></th>
                        <td>
                            <div class="btn-group">
                                <span class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_adver_no">
                                <span id="adver_nm_view">&nbsp;  <?php echo lang('strSelect')?>  &nbsp;</span> 
                                <i class="dropdown-caret fa fa-caret-down"></i>
                                </span>
                                <ul class="dropdown-menu ul_sel_box">
                                	<?php 
                                	   $logged_in = $this->session->userdata('logged_in');
                                	
                            	       if($logged_in['mem_type'] == "manager"){
                                	?>
                                	<li><a href="javascript:sel_adver_no('<?php echo $logged_in['mem_no']?>','<?php echo $logged_in['mem_nm']?>');"><?php echo $logged_in['mem_nm']?></a></li>
                                	<?php 
                            	       }
                                	?>
                                <?php foreach ($advertiser_list as $row){?>
                                    <li><a href="javascript:sel_adver_no('<?php echo $row['adver_no']?>','<?php echo $row['adver_nm']?>');"><?php echo $row['adver_nm']?></a></li>
                                <?php }?>
                                </ul>
                            </div>
                            <input type="hidden" name="adver_no" id="adver_no"/>
                            <span onclick="adver_no_sel();" class="btn btn-default ma_l5"><?php echo lang('strAdd')?></span>
                        </td>
                    </tr>
                    -->
                    <?php
                        //}else{
                    ?>
                    <input type="hidden" id="adver_no" name="adver_no" value="<?php echo $mem_no?>"/>
                    <?php
                        //}
                    ?>
                    <tr>
                        <th><?php echo lang('strCampaign')?> <?php echo lang('strName')?></th>
                        <td>
                            <input type="text" name="campaign_nm" id="campaign_nm" value="" onblur="input_count()" style="ime-mode: disabled;">
                            <input type="hidden" id="nm_check_commit" name="nm_check_commit" value="N" validate="null,아이디">
                            <?php echo lang('strCampaignTxtCount');?>
                            <span id="nm_check_result" class="join_info ma_l20"> * <?php echo lang('strCampaignTxtInfo');?></span>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strDailyBudget')?></th>
                        <td>
                            <input type="radio" name="daily_budget_sel" id="daily_budget_sel" value="Y" onclick="avail_daily_budget('Y')" checked> <?php echo lang('strNone')?>
                            <input type="radio" class="ma_l20" name="daily_budget_sel" id="daily_budget_sel" value="N" onclick="avail_daily_budget('N');"> <?php echo lang('strSetBudget')?>
                            <input type="text" name="daily_budget" id="daily_budget" onkeyup="inputNumberFormat(this);" onblur="limit_daily_budget();" style="background-color:#ebebeb" disabled/> <?php echo lang('strMoney');?>
                            <input type="hidden"  id="daily_budget_check" value="N"/> 
                            <span id="daily_alert" style="display:none">(<?php echo lang('strMinimumWon')?>)</span>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <p class="txt-right">
        
            <a href="/campaign/campaign_list"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strCancel')?></span></a>
             <span onclick="campaign_save();" class="btn btn-primary float_r"><?php echo lang('strSaveAndNext')?></span>
        </p>
        
    </div>
    <form name="creative_group_add_form" method="get" action="/creative/creative_group_add_form/">
        <input type="hidden" name="cam_no">
        <input type="hidden" name="adver_no">
    </form>
</div>
<script type="text/javascript">
$(document).keydown(function(e){   
    if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA"){       
        if(e.keyCode === 8){
            alert('취소를 눌러주세요.');   
        return false;
        }
    }
});
    $("#modal1").on('hidden.bs.modal', function () {
        $('#modal_content_add').html("");
        $(this).data('bs.modal', null);
    });

    function nm_check(){
        var mem_no = '<?php echo $mem_no;?>';
        $.ajax({
            type:"POST",
            url:"/campaign/campaign_nm_check/",
            data : {campaign_nm : $("#campaign_nm").val(), mem_no : mem_no },
            timeout : 30000,
            async:false,
            cache : false,
            success: function (data){
                $("#nm_check_commit").val("N");
                switch(data.trim()){
                    case "true":
                        var show_args="<?php echo lang('strCampaignCheckAlert');?>";
                        $("#nm_check_commit").val("Y");
                        break;
                    case "false":
                        var show_args="<?php echo lang('strCampaignCheck1');?>";
                        alert("<?php echo lang('strCampaignCheckAlert1');?>");
                        break;
                    case "none":
                        var show_args="<?php echo lang('strCampaignCheck2');?>";
                        alert("<?php echo lang('strCampaignCheckAlert2');?>");
                        break;

                }

                $('#nm_check_result').html(show_args);
            },
            error: function whenError(e){
                alert("code : " + e.status + "\r\nmessage : " + e.responseText);
            }
        });
    }

    function adver_no_sel(){
        $.ajax({
            type:"POST",
            url:"/campaign/advertiser_add_form/",
            data : {mem_no : $("#mem_no").val()},
            success: function (data){
                $('#modal_content_add').append(data);
                $("#modal1").modal("show");
            }
        });
    }

    function avail_daily_budget(fl){
        if(fl == "N"){
            $("#daily_budget").attr("disabled",false);
            $("#daily_budget").css("background-color", "#FFFFFF");
            $("#daily_alert").show();
        }else{
            $("#daily_budget").attr("disabled",true);
            $("#daily_budget").val('');
            $("#daily_budget").css("background-color", "#DDDDDD");
            $("#daily_alert").hide();
        }
    }

    function adver_detail_info(){
        if($("#detail_info_sel").is(":checked")){
            $("#adver_detail_info").show();
        }else{
            $("#adver_detail_info").hide();
        }
    }

    function new_advertiser_save(){
        var url = '/campaign/new_advertiser_save';
        $.post(url,
            {
                mem_no: $("#mem_no").val(),
                mem_com_nm : $("#adver_com_nm").val(),
                mem_nm : $("#adver_nm").val(),
                mem_email : $("#adver_email").val(),
                mem_tel : $("#adver_tel_no").val(),
                mem_cont : $("#mem_cont").val()
            },
            function(data){
                console.log(data);
                if(data.trim()=="ok"){
                    alert("<?php echo lang('strNewAdvertiserSave');?>");
                    modal_clear();
                    $("#modal1").hide();
                    location.reload();
                }else{
                    alert("<?php echo lang('strNewAdvertiserSave1');?>");
                }
            }
        );
    }

    function input_count(){

        var limit_length = 15;
        var msg_length = 0;

        //String에 bytes() 함수 만들기
        String.prototype.bytes = function() {
            var msg = this;
            var cnt = 0;

            //한글이면 2, 아니면 1 count 증가
            for( var i=0; i< msg.length; i++) {
                cnt += (msg.charCodeAt(i) > 128 ) ? 1 : 1;
            }
            return cnt;
        }
        input_type_limit();
        //textarea에서 키를 입력할 때마다 동작
        msg_length = $("#campaign_nm").val().bytes();

        if( msg_length <= limit_length ) {
        	$("#input_count").css("color", "black");
            $("#input_count").html( msg_length );
        } else {
            $("#input_count").css("color", "red");
            $("#input_count").html( "15" );
            $("#campaign_nm").val($("#campaign_nm").val().substring(0, 15));
        }
        nm_check();
    }

    function input_type_limit(){
        var objEvent = event.srcElement;
        var numPattern = /  |[\[\]{}()<>?|`~!@#$%^&*+=;:\"'\\]/g;
        numPattern = objEvent.value.match(numPattern);

        if (numPattern != null) {
            alert("<?php echo lang('strCampaignTxtInfo2');?>");
            objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
            objEvent.focus();
            return false;
        }
    }

    function limit_daily_budget(){
        var budget = uncomma($("#daily_budget").val()) ;
        if (budget < 10000){
            alert("최소 10,000원 이상을 입력해 주세요");
            $("#daily_budget").focus();
        }else{
        	$("#daily_budget_check").val("Y");
        }
        
    }
    function inputNumberFormat(obj) {
        obj.value = comma(uncomma(obj.value));
    }

    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }

    function sel_adver_no(no, nm){
        $("#adver_no").val(no);
        $("#adver_nm_view").html(nm);
    }

    function campaign_save(){
        if($("#adver_no").val() == ""){
            alert("<?php echo lang('strCampaignSaveAlert1');?>");
        }else if ($("#campaign_nm").val()=="" || $("#nm_check_commit").val() == "N"){
        	alert("<?php echo lang('strCampaignSaveAlert3');?>");
        }else if($("input[name='daily_budget_sel']:checked").val()=="N"  && $("#nm_check_commit").val() == 'N'){
        	alert("<?php echo lang('strCampaignSaveAlert2');?>");
        }else{
            var url = '/campaign/campaign_save';
            $.post(url,
                {
                    mem_no: '<?php echo $mem_no?>',
                    campaign_nm : $("#campaign_nm").val(),
                    adver_no : $("#adver_no").val(),
                    daily_budget : $("#daily_budget").val().replace(/[^\d]+/g, '')
                },
                function(data){
                    if(data.trim() == "false"){
                        alert("<?php echo lang('strCampaignSaveAlert');?>");
                    }else{
                        document.creative_group_add_form.cam_no.value = data;
                        document.creative_group_add_form.adver_no.value = $("#adver_no").val();
                        document.creative_group_add_form.submit();
                    }
                }
            );
        }
    }
</script>
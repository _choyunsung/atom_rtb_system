<?php 
    $mem_no=$this->session->userdata('mem_no');
?>
<!-- 광고주 만들기 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content_add" id="modal_content_add">
            
		</div>
    </div>
</div>
<div class="boxed">
    <div id="content-container">
    	<div class="wrap">
    		<div class="panel float_r wid_970 min_height">
    			<div class="panel-heading">
    				<h3 class="page-header text-overflow tit_blit"><?php echo lang('strCampaign')?> <?php echo lang('strCopy')?></h3>
    			</div>
        		<div class="panel-body">
            	    <div class="clear">
                        <form name="campaign_add" action="#">
                            <input type="hidden" name="cam_no" id="cam_no" value="<?php echo $campaign_info['cam_no'];?>">
                            <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                                <tr>
                                    <th><?php echo lang('strAdvertiser')?> <?php echo lang('strSelect')?></th>
                                    <td>
                                        <select id="adver_no" name="adver_no" >
                                            <option value=""><?php echo lang('strSelect');?></option>
                                            <?php foreach ($advertiser_list as $row){?>
                                                <option value="<?php echo $row['adver_no']?>" <?php if ($campaign_info['adver_no'] == $row['adver_no']){echo "selected";}?>><?php echo $row['adver_nm']?></option>
                                            <?php }?>
                    				    </select>
                    				    <span onclick="adver_no_sel();" class="btn btn-default"><?php echo lang('strAdd')?></span>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo lang('strCampaign')?> <?php echo lang('strName')?></th>
                                    <td>
                                        <input type="text" name="campaign_nm" id="campaign_nm" onkeyup="input_count()" style="ime-mode: disabled;" value="<?php echo $campaign_info['cam_nm']?>">
                                        <input type="hidden" id="nm_check_commit" name="nm_check_commit" value="" validate="null,아이디">
                                        <?php echo lang('strCampaignTxtCount');?>
                                         <span id="nm_check_result" class="join_info ma_l20">* <?php echo lang('strCampaignTxtInfo');?></span>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th><?php echo lang('strCampaign')?> <?php echo lang('strAndBudget');?></th>
                                    <td>
                                        <input type="radio" name="daily_budget_sel" value="" onclick="avail_daily_budget('Y')"> <?php echo lang('strNone');?>
                                        <input type="radio" class="ma_l20" name="daily_budget_sel" value="" onclick="avail_daily_budget('N');"> <?php echo lang('strCustomRange');?>
                                        <input type="text" name="daily_budget" id="daily_budget" value="<?php echo $campaign_info['daily_budget']?>" disabled>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <p class="txt-right">
	                    <span onclick="new_campaign_save();" class="btn btn-primary"><?php echo lang('strSaveAndNext')?></span>
	                    <span class="btn btn-dark"  onclick="history.back();"><?php echo lang('strClose');?></span>
                    </p>
            	</div>	
            </div>	

<script type="text/javascript">
function nm_check(){
    $.ajax({
        type:"POST",
        url:"/campaign/campaign_nm_check/",
        data : {campaign_nm : $("#campaign_nm").val()},
        timeout : 30000,
        async:false,
        cache : false,
        success: function (data){
            $("#nm_check_commit").val("");
            switch(data.trim()){
                case "true":
                    var show_args="<?php echo lang('strCampaignCheckAlert');?>";
                    $("#nm_check_commit").val("Y");
                    break;
                case "false":
                	var show_args="<?php echo lang('strCampaignCheckAlert1');?>";
                    break;
                case "none":
                	var show_args="<?php echo lang('strCampaignCheckAlert2');?>";
                    break;
            
            }
            
            $('#nm_check_result').html(show_args);
        },
        error: function whenError(e){
            alert("code : " + e.status + "\r\nmessage : " + e.responseText);
        }
    });
}
function adver_no_sel(){
	$.ajax({
        type:"POST",
        url:"/campaign/new_advertiser_add/",
        data : {mem_no : $("mem_no").val()},
        success: function (data){
        	$('#modal_content').append(data);
        	$("#modal1").modal("show");
        }
    });
}

function avail_daily_budget(fl){
    if(fl=="N"){
	    $("#daily_budget").attr("disabled",false);
    }else{
    	$("#daily_budget").attr("disabled",true);
    	$("#daily_budget").val('');
    }
}

function adver_detail_info(){
	if($("#detail_info_sel").is(":checked")){
		$("#adver_detail_info").show();
	}else{
		$("#adver_detail_info").hide();
	}
}

function new_advertiser_save(){
    var url = '/campaign/new_advertiser_save';
	 $.post(url,
            {
		 mem_no: $("#mem_no").val(), 
		 adver_com_nm : $("#adver_com_nm").val(),
		 adver_nm : $("#adver_nm").val(),
		 adver_email : $("#adver_email").val(),
		 adver_tel_no : $("#adver_tel_no").val()
		 
            },
            function(data){
                if(data.trim()=="ok"){
                    alert("<?php echo lang('strNewAdvertiserSave');?>");
                    modal_clear();
                	$("#modal1").hide();
                	location.reload();
                }else{
                	alert("<?php echo lang('strNewAdvertiserSave2');?>");
                	
                }
            }
        );
}

function input_count(){
	
    var limit_length = 15;
    var msg_length = 0;
     
    //String에 bytes() 함수 만들기
    String.prototype.bytes = function() {
        var msg = this;
        var cnt = 0;
         
        //한글이면 2, 아니면 1 count 증가
        for( var i=0; i< msg.length; i++) {
            cnt += (msg.charCodeAt(i) > 128 ) ? 1 : 1;  
        }
        return cnt;
    }
    input_type_limit();
    //textarea에서 키를 입력할 때마다 동작
    msg_length = $("#campaign_nm").val().bytes(); 
 
    if( msg_length <= limit_length ) {     
        $("#input_count").html( msg_length );  
    } else {
    	$("#input_count").css("color", "red"); 
        $("#input_count").html( msg_length );   
    }
    nm_check();
}

function input_type_limit(){
	var objEvent = event.srcElement;
    //var numPattern = /^[a-zA-Z가-힣.,-]*$/;
    var numPattern = /[0-9]|[ \[\]{}()<>?|`~!@#$%^&*_+=;:\"'\\]/g;
    numPattern = objEvent.value.match(numPattern);
 
    if (numPattern != null) {
        alert("<?php echo lang('strCampaignTxtInfo2');?>");
        objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
        objEvent.focus();
        return false;
    }
}

function new_campaign_save(){
	var url = '/campaign/new_campaign_save';
	 $.post(url,
           {
		 mem_no: '<?php echo $mem_no?>', 
		 campaign_nm : $("#campaign_nm").val(),
		 adver_no : $("#adver_no").val(),
		 daily_budget : $("#daily_budget").val()
           },
           function(data){
               console.log(data);
               if(data.trim()>0){
                   alert("<?php echo lang('strCampaignCopyAlert');?>");
                   location.reload();
               }else{
               	alert("<?php echo lang('strCampaignCopyAlert2');?>");
               	
               }
           }
       );
}
</script>
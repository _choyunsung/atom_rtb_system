<?php 
    $mem_no=$this->session->userdata('mem_no');
?>
<!-- 광고주 만들기 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content_add" id="modal_content_add">
            <div class="modal-header">
                <h4 class="h4_blit"><?php echo lang('strAdvertiser')?> <?php echo lang('strAdd')?></h4>
            </div>
            <div class="modal-body">
                <form name="advertiser_add" action="#">
                    <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
                    <table  class="aut_tb bd-t2-gray"width="100%" cellpadding="0" cellspacing="0" border="0" >
                        <colgroup>
                            <col width="20%">
                            <col width="*">
                        </colgroup>
                        <tr>
                            <th><?php echo lang('strAdvertiser')?> <?php echo lang('strName')?></th>
                            <td>
                                <input type="text" name="adver_com_nm" id="adver_com_nm">
                            </td>
                        </tr>
                    </table>
                    <p class="txt-right ma_b10">
                    	<input type="checkbox" id="detail_info_sel" onclick="adver_detail_info();" ><?php echo lang('strNewAdvertiserInfoEntry');?>
                    </p>
                    <table class="aut_tb"  width="100%" cellpadding="0" cellspacing="0" border="0" id="adver_detail_info" style="display:none">
                    <colgroup>
                    	<col width="20%">
                    	<col width="*">
                    </colgroup>
                        <tr>
                            <th><?php echo lang('strManagerName')?></th>
                            <td>
                                <input type="text" name="adver_nm" id="adver_nm" >
                            </td>
                        <tr>
                            <th><?php echo lang('strEmail')?></th>
                            <td>
                                <input type="text" name="adver_email">
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strOfficeNumber')?></th>
                            <td>
                                <input type="text" name="adver_tel_no">
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strMemo')?></th>
                            <td>
                                <input type="text" name="">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10">
                <div class="join_bt_group">
                    <span onclick="new_advertiser_save();" class="btn btn-primary" id="id_find_step1_next_btn"><?php echo lang('strSave')?></span>
                    <span data-dismiss="modal"  class="btn btn-primary"><?php echo lang('strClose')?></span>
                </div>
            </div>
		</div>
    </div>
</div>
<div class="boxed">
    <div id="content-container">
    	<div class="wrap">
    		<div class="panel float_r wid_970 min_height">
    			<div class="panel-heading">
    				<h4 class="page-header text-overflow tit_blit"><?php echo lang('strCampaign')?> <?php echo lang('strModify')?></h4>
    			</div>
        		<div class="panel-body">
            	   <div class="clear">
                        <form name="campaign_add" action="#">
                            <input type="hidden" name="cam_no" id="cam_no" value="<?php echo $campaign_info['cam_no'];?>">
                            <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                            <!-- 
                                <?php
                                    //대행사나 랩사만 광고주 추가
                                   if($group_no == "3" || $group_no == "4" ){
                                ?>
                                <tr>
                                    <th><?php echo lang('strAdvertiser')?> <?php echo lang('strSelect')?></th>
                                    <td>
                                        <select id="adver_no" name="adver_no" onchange="adver_no_sel();" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                                            <option value="new"><?php echo lang('strNewAdvertiserSave2');?></option>
                                            <?php foreach ($advertiser_list as $row){?>
                                                <option value="<?php echo $row['adver_no']?>" <?php if ($campaign_info['adver_no'] == $row['adver_no']){echo "selected";}?>>
                                                    <?php echo $row['adver_nm']?>
                                                </option>
                                            <?php }?>
                    				    </select>
                    				    <span onclick="adver_no_sel();" class="btn btn-default"><?php echo lang('strAdd')?></span>
                                    </td>
                                </tr>
                                <?php
                                    }else{
                                ?>
                                <input type="hidden" id="adver_no" name="adver_no" value="<?php echo $mem_no?>"/>
                                <?php
                                    }
                                ?>
                                 -->
                                <tr>
                                    <th><?php echo lang('strCampaign')?> <?php echo lang('strName')?></th>
                                    <td>
                                        <input type="text" name="campaign_nm" id="campaign_nm" onblur="input_count()" value="<?php echo $campaign_info['cam_nm']?>"/>
                                        <input type="hidden" id="nm_check_commit" name="nm_check_commit" value="Y" validate="null,아이디">
                                        <?php echo lang('strCampaignTxtCount');?>
                                        <span id="nm_check_result" class="join_info ma_l20"> * <?php echo lang('strCampaignTxtInfo');?></span>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <th><?php echo lang('strDailyBudget')?></th>
                                    <td>
                                        <input type="radio" name="daily_budget_sel" value="" onclick="avail_daily_budget('Y')" <?php if($campaign_info['daily_budget'] == "" || $campaign_info['daily_budget'] == "0"){ echo "checked"; }?>> <?php echo lang('strNone')?>
                                        <input type="radio" class="ma_l20" name="daily_budget_sel" value="" onclick="avail_daily_budget('N');" <?php if($campaign_info['daily_budget'] != "0"){ echo "checked"; }?>> <?php echo lang('strSetBudget')?>
                                        <input type="text" name="daily_budget" id="daily_budget" onkeyup="inputNumberFormat(this);" value="<?php echo number_format($campaign_info['daily_budget']);?>" <?php if($campaign_info['daily_budget'] != "0"){ }else{ echo "disabled"; }?>>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <p class="txt-right">
	                    <span onclick="new_campaign_save();" class="btn btn-primary"><?php echo lang('strModify')?><?php echo lang('strDone')?></span>
	                    <span class="btn btn-dark" onclick="history.back();"> <?php echo lang('strCancel')?> </span>
                    </p>
            	</div>	
            </div>	
        </div>
    </div>

<script type="text/javascript">

    function inputNumberFormat(obj) {
        obj.value = comma(uncomma(obj.value));
    }

    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }

    function adver_no_sel(){
        $("#modal1").modal('show');
    }

    function avail_daily_budget(fl){
        if(fl=="N"){
            $("#daily_budget").attr("disabled",false);
            $("#daily_budget").css("background-color", "#FFFFFF");
        }else{
            $("#daily_budget").attr("disabled",true);
            $("#daily_budget").val('');
            $("#daily_budget").css("background-color", "#DDDDDD");
        }
    }

    function adver_detail_info(){
        if($("#detail_info_sel").is(":checked")){
            $("#adver_detail_info").show();
        }else{
            $("#adver_detail_info").hide();
        }
    }

    function new_advertiser_save(){
        var url = '/campaign/new_advertiser_save';
         $.post(url,
                {
                 mem_no: $("#mem_no").val(),
                 adver_com_nm : $("#adver_com_nm").val(),
                 adver_nm : $("#adver_nm").val(),
                 adver_email : $("#adver_email").val(),
                 adver_tel_no : $("#adver_tel_no").val()

                },
                function(data){
                    if(data.trim()=="ok"){
                        alert("<?php echo lang('strNewAdvertiserSave');?>");
                        modal_clear();
                        $("#modal1").hide();
                        location.reload();
                    }else{
                        alert("<?php echo lang('strNewAdvertiserSave2');?>");

                    }
                }
            );
    }

    function input_count(){

        var limit_length = 15;
        var msg_length = 0;

        //String에 bytes() 함수 만들기
        String.prototype.bytes = function() {
            var msg = this;
            var cnt = 0;

            //한글이면 2, 아니면 1 count 증가
            for( var i=0; i< msg.length; i++) {
                cnt += (msg.charCodeAt(i) > 128 ) ? 1 : 1;
            }
            return cnt;
        }
        input_type_limit();
        //textarea에서 키를 입력할 때마다 동작
        msg_length = $("#campaign_nm").val().bytes();

        if( msg_length <= limit_length ) {
        	$("#input_count").css("color", "black");
            $("#input_count").html( msg_length );
        } else {
            $("#input_count").css("color", "red");
            $("#input_count").html( "15" );
            $("#campaign_nm").val($("#campaign_nm").val().substring(0, 15));
        }
        nm_check();
    }

    function nm_check(){
        var mem_no = '<?php echo $mem_no;?>';
        var cam_no = '<?php echo $campaign_info['cam_no'];?>';
        $.ajax({
            type:"POST",
            url:"/campaign/modify_campaign_nm_check/",
            data : {campaign_nm : $("#campaign_nm").val(), mem_no : mem_no, cam_no : cam_no },
            timeout : 30000,
            async:false,
            cache : false,
            success: function (data){
                $("#nm_check_commit").val("N");
                switch(data.trim()){
                    case "true":
                        var show_args="<?php echo lang('strCampaignCheckAlert');?>";
                        $("#nm_check_commit").val("Y");
                        break;
                    case "false":
                        var show_args="<?php echo lang('strCampaignCheck1');?>";
                        alert("<?php echo lang('strCampaignCheckAlert1');?>");
                        break;
                    case "none":
                        var show_args="<?php echo lang('strCampaignCheck2');?>";
                        alert("<?php echo lang('strCampaignCheckAlert2');?>");
                        break;

                }

                $('#nm_check_result').html(show_args);
            },
            error: function whenError(e){
                alert("code : " + e.status + "\r\nmessage : " + e.responseText);
            }
        });
    }

    function input_type_limit(){
        var objEvent = event.srcElement;
        //var numPattern = /^[a-zA-Z가-힣.,-]*$/;
        var numPattern = /  |[\[\]{}()<>?|`~!@#$%^&*+=;:\"'\\]/g;
        numPattern = objEvent.value.match(numPattern);

        if (numPattern != null) {
            alert("<?php echo lang('strCampaignTxtInfo2');?>");
            objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
            objEvent.focus();
            return false;
        }
    }

    function new_campaign_save(){
        var url = '/campaign/modify_campaign_save';
        if ($("#nm_check_commit").val() != 'Y'){
            alert("캠페인명을 확인해 주세요.");
        }
        $.post(url,
            {
                mem_no: '<?php echo $mem_no?>',
                cam_no: $("#cam_no").val(),
                campaign_nm : $("#campaign_nm").val(),
                daily_budget : $("#daily_budget").val().replace(/[^\d]+/g, '')
            },
            function(data){
                if(data.trim()=="ok"){
                    alert("<?php echo lang('strCampaignEdit2');?>");
                    location.replace('/campaign/campaign_list');
                }else{
                    alert("<?php echo lang('strCampaignEdit');?>");
                }
            }
        );
    }
</script>
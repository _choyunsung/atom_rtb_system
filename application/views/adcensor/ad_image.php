<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAdCensor')?></a></li>
			<li class="active"><?php echo lang('strImgAd')?></li>
		</ol>
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strImgAd')?></h3>
	</div>
	<div class="panel-body">
        <form name="adcensor_list"  method="post">
            <div class="ma_b10">
                <table class="table table-bordered aut_tb">
                <colgroup>
                	<col width="20%">
                	<col width="*">
                	
                </colgroup>
                    <tr>
                        <th>
                            <?php echo lang("strEvaluation");?>
                        </th>
                        <td>
                        <ul class="float_l">
                            <li><input type="radio" name="cond_cre_evaluation" value="" <?php if($cond_cre_evaluation == ""){ echo "checked"; }?>/> <?php echo lang('strAll');?></li>
                            <li class="ma_l20"><input type="radio" name="cond_cre_evaluation" value="1" <?php if($cond_cre_evaluation == "1"){ echo "checked"; }?>/> <?php echo lang('strApproveWait');?></li>
                            <li class="ma_l20"><input type="radio" name="cond_cre_evaluation" value="2" <?php if($cond_cre_evaluation == "2"){ echo "checked"; }?>/> <?php echo lang('strApprove');?></li>
                            <li class="ma_l20"><input type="radio" name="cond_cre_evaluation" value="3" <?php if($cond_cre_evaluation == "3"){ echo "checked"; }?>/> <?php echo lang('strReject');?></li>
                            <li class="ma_l20"><input type="radio" name="cond_cre_evaluation" value="4" <?php if($cond_cre_evaluation == "4"){ echo "checked"; }?>/> <?php echo lang('strDefer');?></li>
                        
                        </ul>
               
                        <div class="float_r ma_r5 ma_t5">
				            <button class="btn btn-primary" onclick="this.form.submit();"><?php echo lang("strSearch");?></button>
		                </div>
                        </td>
                    </tr>
                </table>
        
			<div class="over_hidden">
                 <input class="btn btn-primary float_r ma_l5" type="button"  value="<?php echo lang('strApply');?>" onclick="all_apply();">
                    <!--  <div class="btn-group float_r ma_l5"">
    					<input type="hidden" name="charge_money1" id="charge_money1" >
    					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel">
    						<span id="charge_money1_view"> &nbsp; 승인대기 &nbsp;</span>
    						<i class="dropdown-caret fa fa-caret-down"></i>
    					</button>
    					<ul class="dropdown-menu ul_sel_box">
                            <li><a href="javascript:sel_money(1100000);">1,100,000</a></li>
                            <li><a href="javascript:sel_money(2200000);">2,200,000</a></li>
                            <li><a href="javascript:sel_money(3300000);">3,300,000</a></li>
                            <li><a href="javascript:sel_money(4400000);">4,400,000</a></li>
                            <li><a href="javascript:sel_money(5500000);">5,500,000</a></li>
    					</ul>
    					<span class="color_r"> 셀렉트 박스 적용해야 해요~~</span>
				    </div>-->
                
              <select id="apply" name="apply" class="form-control-static input-sm float_r ma_l5" style="background-color:#FFFFFF;">
                    <option value="1"> <?php echo lang('strApproveWait');?></option>
                    <option value="2"> <?php echo lang('strApprove');?></option>
                    <option value="3"> <?php echo lang('strReject');?></option>
                    <option value="4"> <?php echo lang('strDefer');?></option>
                </select>
                <input  class="btn btn-default float_r ma_l5" type="button"  value="<?php echo lang('strAllCheck')?>" onclick="all_check();">
				</div>
            </div>
            <input type="hidden" name="cre_no" id="cre_no" value="">
            <input type="hidden" name="admin_no" id="admin_no" value="<?php echo $mem_no;?>">
            <input type="hidden" name="mem_no" id="mem_no" value="">
            <input type="hidden" name="cre_evaluation" id="cre_evaluation" value="">
            
            <table id="list" name="list" class="inspection_tb" data-horizontal-width="100%"  cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="3%">
                    <col width="8%">
                    <col width="8%">
                    <col width="11%">
                    <col width="12%">
                    <col width="8%">
                    <col width="8%">
                    <col width="8%">
                    <col width="9%">
                    <col width="9%">
                    <col width="8%">
                    <col width="5%">
                </colgroup>
                <thead>
                    <tr>                                                                                                                                    
                        <th>&nbsp;</th>
                        <th><?php echo lang('strAd').lang('strName')?></th>
                        <th><?php echo lang('strMaterial');?></th>
                        <th>URL</th>
                        <th><?php echo lang('strMemo');?></th>
                        <th><?php echo lang('strCompany');?></th>
                        <th><?php echo lang('strAuthority');?></th>
                        <th>ID</th>
                        <th><?php echo lang('strRegisteredDate');?></th>
                        <th><?php echo lang('strEvaluatedDate');?></th>
                        <th><?php echo lang('strManager');?></th>
                        <th><?php echo lang('strEvaluation');?></th>
                   </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($adcensor_list as $key=>$row){
                    ?>
                    <tr>
                        <td class="">
                            <input type='checkbox' id='select_<?php echo $no_count?>' name='select_id' value="<?php echo $row['cre_no']?>">
                        </td>
                        <td><?php echo $row['cre_nm']?></td>
                        <td class="pad-ver">
                            <span class="display_block"><?php echo $row['cre_width']?> X <?php echo $row['cre_height']?></span>
                           <div class="wid_50 m_center">
	                           <a href="#popover" class="add-popover" rel="popover" data-popover-content="#ad_image_<?php echo $key?>" data-placement="right" data-trigger="focus" data-toggle="popover">
	                            	<img class="img-sm" onerror="this.src='/img/no_image.png';" src="<?php echo IMAGE_BASE_URL.$row['cre_img_link']?>"  style="border:1px solid #ebebeb;">
	                           </a>
                            </div> 
                            <div id="ad_image_<?php echo $key?>" class="hide">
                                <img src="<?php echo IMAGE_BASE_URL.$row['cre_img_link']?>" width="<?php echo $row['cre_width']?>" height="<?php echo $row['cre_height']?>" style="border:1px solid #ebebeb;">
                            </div>
                        </td>
                        <td><p class="tooltip-wide"><a class="btn-link add-tooltip" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $row['cre_link']?>" href="<?php echo $row['cre_link']?>" target="_blank"><?php echo substr($row['cre_link'], 0, 15)?>...</a></p></td>
                        <td class="txt-left"><p class="tooltip-wide"><a class="btn-link add-tooltip" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $row['cre_cont']?>"><?php echo mb_substr($row['cre_cont'], 0, 16)?>...</a></p></td>
                        <td><?php echo $row['mem_com_nm']?></td>
                        <td><?php echo $row['group_nm']?></td>
                        <td><?php echo $row['mem_id']?></td>
                        <td>
                            <?php echo $row['cre_ymd'];?>
                        </td>
                        <td>
                            <?php
                            if($row['evaluation_dt'] != ""){
                                echo $row['evaluation_dt'];
                            }else{
                                echo lang('strPendency');
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if($row['admin_nm'] != ""){
                                echo $row['admin_nm'];
                            }else{
                                echo lang('strPendency');
                            }
                            ?>
                        </td>
                        <td>

                            <div class="btn-group" style="cursor:pointer;">
                                <span class="span" data-toggle="dropdown" id="active_sel">
                                    <?php
                                    if($row['cre_evaluation'] == 1){
                                       echo lang('strApproveWait');
                                    }elseif($row['cre_evaluation'] == 2){
                                        echo lang('strApprove');
                                    }elseif($row['cre_evaluation'] == 3){
                                       echo lang('strReject');
                                    }elseif($row['cre_evaluation'] == 4){
                                       echo lang('strDefer');
                                    }
                                    ?>
                                    <i class="dropdown-caret fa fa-caret-down float_r ma_t5 ma_r5 cursor"></i>
                                </span>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php
                                        if($row['cre_evaluation'] != 1){
                                    ?>
                                    <li>
                                        <a href="javascript:adcensor_evaluation(<?php echo $row['cre_no']?>, <?php echo $mem_no?>, 1, <?php echo $row['mem_cash']?>);">
                                            <?php echo lang('strApproveWait');?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        if($row['cre_evaluation'] != 2){
                                    ?>
                                    <li>
                                        <a href="javascript:adcensor_evaluation(<?php echo $row['cre_no']?>, <?php echo $mem_no?>, 2, <?php echo $row['mem_cash']?>);">
                                            <?php echo lang('strApprove');?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        if($row['cre_evaluation'] != 3){
                                    ?>
                                    <li>
                                        <a href="javascript:adcensor_evaluation(<?php echo $row['cre_no']?>, <?php echo $mem_no?>, 3, <?php echo $row['mem_cash']?>);">
                                            <?php echo lang('strReject');?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        if($row['cre_evaluation'] != 4){
                                    ?>
                                    <li>
                                        <a href="javascript:adcensor_evaluation(<?php echo $row['cre_no']?>, <?php echo $mem_no?>, 4, <?php echo $row['mem_cash']?>);">
                                            <?php echo lang('strDefer');?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>


                        </td>
                    </tr>
                   <?php
                        }
                    ?>
                </tbody>
            </table>

            <div class="center">
                <div class="btn-group float_r">
	                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
	                    <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
	                        <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
	                        <i class="dropdown-caret fa fa-caret-down"></i>
	                    </button>
	                    <ul class="dropdown-menu ul_sel_box_pa" >
	                        <li><a href="javascript:page_change(10)">10</a></li>
	                        <li><a href="javascript:page_change(25)">25</a></li>
	                        <li><a href="javascript:page_change(50)">50</a></li>
	                        <li><a href="javascript:page_change(100)">100</a></li>
	                    </ul>
	                </div>
	                <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div> 
        </form>
   </div>
</div>
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm = document.adcensor_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.adcensor_list;
        frm.action="/adcensor/ad_image/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝

    $(document).ready(function() {
        /*
        $('#list').dataTable( {
            "dom": '<"toolbar"f>rt'
        } );
        */
        $("div.toolbar").append($("#all_btn_gp").html());
    } );


    function adcensor_evaluation(cre_no, admin_no, cre_evaluation, mem_cash){
        //if(confirm("하시겠습니까?")){
        if (mem_cash == '0'){
            alert("ATOM 캐쉬부족으로 승인 할 수 없습니다.");
        }else{
            var frm = document.adcensor_list;
            frm.action ="/adcensor/adcensor_evaluation";
            frm.cre_no.value = cre_no;
            frm.admin_no.value = admin_no;
            frm.cre_evaluation.value = cre_evaluation;
            frm.submit();
        }
        //}else{
            //alert("취소되었습니다");
        //}
    }

    $(function() {
        jQuery('.date').datetimepicker({
            format:'Y-m-d H:i'
        });
    });

    $(function(){
        $('[rel="popover"]').popover({
            container: 'body',
            html: true,
            content: function () {
                var clone = $($(this).data('popover-content')).clone(true).removeClass('hide');
                return clone;
            }
        }).click(function(e) {
            e.preventDefault();
        });
    });

    var chkstate = false;

    function all_check(){
        var objSel = document.getElementsByName("select_id");
        var chk = false;

        if(!chkstate){
            chk = true;
            chkstate = true;
        }else{
            chk = false;
            chkstate = false;
        }
        for(var i=0; i < objSel.length; i++){
            objSel[i].checked = chk;
        }
    }

    function all_apply(){
        var objSel = document.getElementsByName("select_id");
        var cre_evaluation = $("#apply option:selected").val();
        var chkSel = false;
        for(var i=0; i < objSel.length; i++){
            if(objSel[i].checked == true){
                chkSel = true;
                break;
            }
        }
        if(chkSel == false){
            alert("Check Choice!");
            return false;
        }
        if(confirm('Apply OK!?')){
            var cre_no = "";
            for(var i = 0; i < objSel.length; i++){
                if(objSel[i].checked == true){
                    cre_no += ","+objSel[i].value;
                }
            }
            cre_no = cre_no.substring(1, cre_no.length);

            $.post("/adcensor/adcensor_evaluation",{
                    cre_no:cre_no, cre_evaluation:cre_evaluation, admin_no:<?php echo $mem_no?>
                },
                function(data){

                    var frm = document.adcensor_list;
                    frm.submit();

                })
        }
    }
</script>
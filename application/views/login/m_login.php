<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<title>Mobile ATOM</title>
    <link rel="shortcut icon" href="http://www.adop.cc/wp-content/uploads/2015/01/adop_logo_16x16.png">
	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
	<!--Bootstrap Stylesheet [ REQUIRED ]-->  
	<link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="/template/bootstrap/css/nifty.css" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">
	<!--jQuery [ REQUIRED ]-->
    <script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0, user-scalable=yes" />
</head>
<body>
<div class="mobile_con">
	<div class="m_wrap">
		<div class="m_login">
			<div class="m_log_box">
			     <!-- 
				<span class="float_l ma_t10 ma_b20"><img src="/img/adop_logo.png"></span>
				 -->
				<span class="float_l ma_t8 ma_b20"><img src="/img/logo_atom.png"></span>
				<div class="btn-group float_r">
					<button class="btn bd_0 bg_w" data-toggle="dropdown">
						<span>
							<?php if ($this->session->userdata('site_lang') == "korean"){?>      
                            	<img alt="" src="/template/assets/img/icons/flag/KR-flag-32.png">
                           	<?php }else{?>
                            	<img alt="" src="/template/assets/img/icons/flag/US-flag-32.png">
                            <?php }?>
						</span>
						<i class="dropdown-caret fa fa-caret-down"></i>
					</button>
					<ul class="dropdown-menu ul_sel_box">
						<li>
							<a href="/login/switch_language/english">
								<img alt="" src="/template/assets/img/icons/flag/US-flag-32.png">
							</a>
						</li>
						<li>
							<a href="/login/switch_language/korean">
								<img alt="" src="/template/assets/img/icons/flag/KR-flag-32.png">
							</a>
						</li>
					</ul>
				</div>              
			</div>             
			<div class="m_log_box">
				<form name="login_info" action="" method="post">
					<div class="form-group">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-user"></i></div>
							<input type="text" class="form-control" placeholder="<?php echo lang('strID')?>" name="mem_id_login" id="mem_id_login" onkeydown="javascript:if(event.keyCode==13){$('#mem_pwd').focus();}">
						</div>
					</div>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-lock"></i>
						</div>
						<input type="password" class="form-control" placeholder="<?php echo lang('strPassword')?>" name="mem_pwd" id="mem_pwd" onkeydown="javascript:if(event.keyCode==13){login();}">
					</div>
				</form>
			</div>
			<div class="ma_t20">
				<span class="btn btn-dark wid_90p hei_35" onclick="login();"><?php echo lang('strLogin')?></span>
			</div>
		</div>
	 </div>
 </div>
 <div class="mobile_foot">
	<p class="bold">＠Copyright. 2015 ADOP Inc. All rights reserved.</p>
</div>
<script type="text/javascript">
function login(){
    var frm = document.login_info;
    url = '/login/login_check';
    frm.action = url;
    mem_id=frm.mem_id_login.value;
    mem_pwd=frm.mem_pwd.value;
    $.post(url,
            {
            mem_id : mem_id,
            mem_pwd : mem_pwd
            },
            function(data){
                if(data.trim()=="1"){
                    $('#login_error').modal('show');
                }else{
                    login_ok();
                }
            }
        );

}

function login_ok(){
    var frm = document.login_info;
    url = '/login/login_process';
    frm.action = url;
    mem_id=frm.mem_id_login.value;
    mem_pwd=frm.mem_pwd.value;
    frm.submit();
}
</script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="/template/bootstrap/js/bootstrap.min.js"></script>


<!--Fast Click [ OPTIONAL ]-->
<script src="/template/plugins/fast-click/fastclick.min.js"></script>


<!--Nifty Admin [ RECOMMENDED ]-->
<script src="/template/bootstrap/js/nifty.min.js"></script>


<!--Background Image [ DEMONSTRATION ]-->
<script src="/template/bootstrap/js/demo/bg-images.js"></script>

<!--Modals [ SAMPLE ]-->
<script src="/template/bootstrap/js/demo/ui-modals.js"></script>


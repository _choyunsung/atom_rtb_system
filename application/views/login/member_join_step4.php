<div class="modal-header">
    <h4 class="h4_blit ma_t5 ma_b5"><?php echo lang('strMemberJoin');?></h4>
</div>
<div class="modal-body">
    <div class="join_history">
    <ul>
        <li>01. <?php echo lang('strTermsAgree');?> &gt;</li>
        <li>&nbsp; 02. <?php echo lang('strIdentification');?> &gt;</li> 
        <li class="font_bold color_p">&nbsp; 03. <?php echo lang('strInfoEntry');?> &gt;</li>
        <li>&nbsp; 04. <?php echo lang('strJoinCompleted');?></li>
    </ul>
    </div>
    <form name="company_info" method="post">
        <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray">
            <colgroup>
                <col width="20%">
                <col width="30%">
                <col width="20%">
                <col width="30%">
            </colgroup>	
            <tr>
                <th class="point_blue"><?php echo lang('strBusinessName')?></th>
                    <td>
                        <input type="hidden" name="comapny_mem_com_name" value="<?php echo $mem_com_nm;?>"/> <?php echo $mem_com_nm;?>
                        <input type="hidden" name="mem_no" id="cancel_mem_no" value="<?php echo $mem_no;?>"/>
                    </td>
                    <th class="point_blue bd_l"><?php echo lang('strBusinessNo')?></th>
                    <td><input type="hidden" name="company_mem_com_no" id="company_mem_com_no" value="<?php echo $mem_com_no;?>"/><?php echo $mem_com_no;?> </td>
            </tr>	
            <tr> 
                <th class="point_blue">* <?php echo lang('strRepreName')?></th>
                <td colspa="2"><input type="text" class="wid_70p" id="mem_com_ceo" name="mem_com_ceo"></td>
                <th class="point_blue bd_l">* <?php echo lang('strBizConditions')?></th>
                <td><input type="text" class="wid_70p" id="mem_com_type" name="mem_com_type"></td>
            </tr>
           	<tr>
                <th class="point_blue">* <?php echo lang('strBizItems')?></th>
                <td><input type="text" class="wid_70p" id="mem_com_item" name="mem_com_item"></td>
            </tr>
            <tr>
                <th rowspan="2" class="point_blue">* <?php echo lang('strBizAdress')?></th>
                <td colspan="3"><input type="text" class="wid_30p" id="mem_post" name="mem_post">
                <span onclick="sample4_execDaumPostcode()" class="btn btn-sm btn-default ma_l5" ><?php echo lang('strFindZipcode')?></span><br></td>
            </tr>	
            <tr>
                <td colspan="3">
                <input type="text" class="wid_40p" id="mem_address1" name="mem_address1" readonly>
                &nbsp;<input type="text" class="wid_30p" id="mem_address2" name="mem_address2">
                </td>
            </tr>
        </table>
    </form>	
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10">
    <span onclick="member_join_step5();" class="btn btn-primary"><?php echo lang('strDone')?></span>
    <span onclick="join_cancel();" class="btn btn-primary"><?php echo lang('strCancel')?></span>
   <!-- <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-dark"><?php echo lang('strClose')?></span>-->
</div>
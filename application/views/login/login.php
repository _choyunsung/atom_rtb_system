<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ATOM</title>
        <link rel="shortcut icon" href="http://www.adop.cc/wp-content/uploads/2015/01/adop_logo_16x16.png">
        <!--STYLESHEET-->
        <!--=================================================-->

        <!--Open Sans Font [ OPTIONAL ] -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


        <!--Bootstrap Stylesheet [ REQUIRED ]-->
        <link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">


        <!--Nifty Stylesheet [ REQUIRED ]-->
        <link href="/template/bootstrap/css/nifty.css" rel="stylesheet">


        <!--Font Awesome [ OPTIONAL ]-->
        <link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">


        <!--Demo [ DEMONSTRATION ]-->
        <link href="/template/bootstrap/css/demo/nifty-demo.css" rel="stylesheet">

        <!--jQuery [ REQUIRED ]-->
        <script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>

        <!-- Highcharts -->
        <script src="/template/plugins/Highcharts-4.1.5/js/highcharts.js"></script>


        <script src="/js/adop.js"></script>
    </head>
    <body>
        <!-- 공용 modal -->
        <div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
            <div class="modal-dialog bd-t7 modal-lg">
                <div class="modal-content" id="modal_content">

                </div>
            </div>
        </div>
        <!-- 회원가입완료 -->
        <div class="modal" id="join_done" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog bd-t7 modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="center pa_t3p f-size-10 ma_t8">
                            <h4 class="point_blue bold pa_t10 f-size-18"><img src="/img/logo_atom.png" alt="atom" /></h4><p class="pa_t8"><?php echo lang('strJoinCompleted1');?></p>
                        </div>
                    </div>
                    <!--Modal footer-->
                    <div class="modal-footer ma_t8">
                        <button data-dismiss="modal"  data-toggle="modal" class="btn btn-primary"><?php echo lang('strOkay')?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- 로그인오류 modal -->
        <div class="modal" id="login_error" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog bd-t7 modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="h4_blit"><?php echo lang('strLoginError');?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="warnning_bg pa_t3p">
                            <?php echo lang('strLoginError1')?>
                            <?php echo lang('strLoginError2')?>
                        </div>
                    </div>
                    <!--Modal footer-->
                    <div class="modal-footer ma_t5">
                        <button data-dismiss="modal"  data-toggle="modal" class="btn btn-primary"><?php echo lang('strClose')?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- 비번찾기 오류 modal -->
        <div class="modal" id="pwd_find_error" role="dialog"tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true"  data-backdrop="static">
            <div class="modal-dialog bd-t7 modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="h4_blit">비밀번호 찾기</h4>
                    </div>
                    <div class="modal-body">
                        <div class="warnning_bg pa_t1p">
                            등록된 회원정보가 일치하지 않습니다.<br />
                            정확한 정보로 확인 후 다시 입력 부탁드립니다.<br /><br />

                            비밀번호 찾기가 불가능 할 경우, 담당 문의하시기 바랍니다.<br />
                            새로운 아이디로 가입을 원하시면 <span class="font_bold point_blue">회원가입</span> 을 해주시기 바랍니다.<br />
                        </div>
                    </div>
                    <!--Modal footer-->
                    <div class="modal-footer ma_t8 ma">
                   
                        <span class="btn btn-primary" onclick="modal_clear(); $('#pwd_find_error').modal('hide'); member_join_step1();">회원가입</span>
                        <span class="btn btn-dark" onclick="modal_clear(); $('#pwd_find_error').modal('hide'); member_pwd_find_step1();" >닫기</span>
                    </div>
                    
                     
                </div>
            </div>
        </div>
        <!-- 요기 -->
        <div id="container" class="">
            <!-- BACKGROUND IMAGE -->
            <!--===================================================-->
            <div id="bg-overlay" class="bg-img img-balloon"></div>
            <!-- LOGIN FORM -->
            <!--===================================================-->
            <div class="cls-content">
                <div class="adop_logo over_hidden">
                </div>
                <div class="cls-content-sm panel">
                   <div class="login_panel">
                     <div class="adop_logo hei_35">
                     <!-- 
                   	 <span class="float_l ma_t8"><img src="/img/adop_atio.png"></span>
                   	  -->
                   	 <span class="float_l ma_t8"><img src="/img/logo_atom.png"></span>
             	       <div class="btn-group float_r">
	        				<input type="hidden" name="" id="" value="">
	        				   <button class="bd_0 bg_w" data-toggle="dropdown" id="">
		                        <span>
		                          <?php if ($this->session->userdata('site_lang') == "korean"){?>      
		                              <img alt="" src="/template/assets/img/icons/flag/KR-flag-32.png">
	                              <?php }else{?>
	                                   <img alt="" src="/template/assets/img/icons/flag/US-flag-32.png">
                                   <?php }?>
	                            </span>
		                        <i class="dropdown-caret fa fa-caret-down"></i>
		                       </button>
		                     <ul class="dropdown-menu ul_sel_box">
		                       <li>
    		                       <a href="/login/switch_language/english">
    		                          <img alt="" src="/template/assets/img/icons/flag/US-flag-32.png">
    	                           </a>
		                       </li>
		                       <li>
    		                       <a href="/login/switch_language/korean">
    		                          <img alt="" src="/template/assets/img/icons/flag/KR-flag-32.png">
    	                           </a>
		                       </li>
		                     </ul>
	         	        </div>
	                   </div>
                        <div class="login_bottom">
                            <!-- <p class="txt-left f-size-16 float_left"><?php echo lang('strMemberLogin')?></p>-->
                        </div>
                        <form name="login_info" action="" method="post">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input type="text" class="form-control" placeholder="<?php echo lang('strID')?>" name="mem_id_login" id="mem_id_login" onkeydown="javascript:if(event.keyCode==13){$('#mem_pwd').focus();}">
                                </div>
                            </div>
                            <div>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-lock"></i>
                                    </div>
                                    <input type="password" class="form-control" placeholder="<?php echo lang('strPassword')?>" name="mem_pwd" id="mem_pwd" onkeydown="javascript:if(event.keyCode==13){login();}">
                                </div>
                                <div class="float_l ma_t20" style="margin:10px 0 20px 0; overflow:hidden;">
                                    <ul class="login_frm">
                                    <li onclick="member_join_step1()"  data-toggle="modal"><?php echo lang('strMemberJoin')?></li>
                                    <li onclick="member_id_find_step1()" data-toggle="modal"><?php echo lang('strForgotID')?></li>
                                    <li onclick="member_pwd_find_step1()" data-toggle="modal" class=""><?php echo lang('strForgotPW')?></li>
                                    </ul>
                                                                   
                                </div>
                                       
                                <div>
                                  <span class="btn btn-dark wid_100p ma_b10 btn-lg " onclick="login();"><?php echo lang('strLogin')?></span>
                                  <!-- <li onclick="$('#join_done').modal('show');"  data-toggle="modal"><?php echo lang('strMemberJoin')?></li> -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="adop_footer2">
                	<div class="adop_copy">
            		<span>＠Copyright. 2015 ADOP Inc. All rights reserved.</span>
<!--             		<p class="text-sm ma_t10">T. + 82 - 2-2052-1117   E. contact@adop.cc / A. (135-845) 서울특별시 강남구 테헤란로 86길 14 윤천빌딩 3, 4, 5 층 애드오피</p> -->
            	</div>
            </div>
        </div>
        <!-- 요기까지 -->
        <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
        <script>
      
            function sample4_execDaumPostcode() {
                new daum.Postcode({
                    oncomplete: function(data) {
                        // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                        // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                        // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                        var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                        var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                        // 법정동명이 있을 경우 추가한다.
                        if(data.bname !== ''){
                            extraRoadAddr += data.bname;
                        }
                        // 건물명이 있을 경우 추가한다.
                        if(data.buildingName !== ''){
                            extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                        }
                        // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                        if(extraRoadAddr !== ''){
                            extraRoadAddr = ' (' + extraRoadAddr + ')';
                        }
                        // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                        if(fullRoadAddr !== ''){
                            fullRoadAddr += extraRoadAddr;
                        }

                        // 우편번호와 주소 정보를 해당 필드에 넣는다.
                        document.getElementById("mem_post").value = data.postcode1+"-"+data.postcode2;
                        document.getElementById("mem_address1").value = fullRoadAddr;
                        //document.getElementById("sample4_jibunAddress").value = data.jibunAddress;
                        close();
                    }
                }).open();
            }
        </script>

    <script type="text/javascript">
        function member_join_step1(){
            $.ajax({
                type:"POST",
                url:"/member/member_join_step1/",
                data : {},
                success: function (data){
                    $('#modal_content').append(data);
                    $("#modal1").modal("show");
                }
            });

        }

        function member_join_step2(){
            if($("#agree_check1").prop("checked") && $("#agree_check2").prop("checked")){
                $.ajax({
                    type:"POST",
                    url:"/member/member_join_step2/",
                    data : {},
                    success: function (data){
                        $('#modal_content').empty();
                        $('#modal_content').append(data);
                    }
                });
            }else{
                if (!$("#agree_check1").prop("checked") && !$("#agree_check2").prop("checked")){
                    alert("약관 및 개인정보 수집 및 이용에 동의해 주세요.");
                }else if (!$("#agree_check1").prop("checked")){
                    alert("이용약관에 동의해 주세요.");
                }else{
                    alert("개인정보 수집 및 이용에 동의해 주세요.");
                }

            }

        }

        function sel_mem_type(type){
            if(type=="company"){
                $("#sel_mem_gb_div").show();
                $("#com_join_1").show();
                $("#com_join_2").show();
            }else{
                $("#sel_mem_gb_div").hide();
                $("#com_join_1").hide();
                $("#com_join_2").hide();
            }
        }

        function find_mem_gb(){
            type=$("input:radio[name='find_id_mem_gb']:checked").val();
            if(type=="company"){
                $("#find_id_com_no").show();
                $("#find_id_com_nm").show();
                $("#id_find_step1_next_btn").show();
                $("#id_email_find").hide();
                $("#id_email_btn").hide();
            }else{
                $("#find_id_com_no").hide();
                $("#find_id_com_nm").hide();
                $("#id_find_step1_next_btn").hide();
                $("#id_email_find").show();
                $("#id_email_btn").show();
            }
        }
        /*
        function sel_mem_gb(gb){
            $("#sel_mem_gb_name").empty();
            if(gb=="ADT"){
                gb_name="광고주";
            }else if(gb=="AGT"){
                gb_name="대행사";
            }else{
                gb_name="랩사";
            }
            $("#sel_mem_gb_name").append(gb_name);
            $("#sel_mem_gb").attr("value",gb);
        }
        */
        function com_reg_no_check(){
            only_number();
            com_reg_no = $("#join_com_no1").val()+"-"+$("#join_com_no2").val()+"-"+$("#join_com_no3").val();
            if(com_reg_no.length==12){
                $.ajax({
                    type:"POST",
                    url:"/member/reg_no_check/",
                    data : {com_reg_no : com_reg_no},
                    timeout : 30000,
                    async:false,
                    cache : false,
                    success: function (data){
                        $("#reg_no_check_commit").val("");
                        switch(data.trim()){
                            case "true":
                                var show_args="<font color='#5177bc'>가입가능한 사업자번호 입니다.</font>";
                                $("#reg_no_check_commit").val("Y");
                                break;
                            case "false":
                                var show_args="<font color='#c85757'>이미 가입된 사업자번호 입니다.</font>";
                                break;
                        }

                        $('#reg_no_check_result').html(show_args);
                    },
                    error: function whenError(e){
                        alert("code : " + e.status + "\r\nmessage : " + e.responseText);
                    }
                });
            }
        }

        function sel_mem_tel(code){
            $("#sel_mem_tel_name").empty();
            $("#sel_mem_tel_name").append(code);
            $("#sel_mem_tel").attr("value",code);
        }

        function sel_mem_email(code){
            if(code=="직접입력"){
                $("#join_tmp_email_host").attr("type",'text');
                $("#join_tmp_email_host").attr("disabled",false);
                $("#join_tmp_email_host").val('');
                $("#sel_mem_email").html(code+' <i class="dropdown-caret fa fa-caret-down"></i>');
                $("#join_tmp_email_host").css("background-color",'');
            }else{
                $("#join_tmp_email_host").attr("type",'hidden');
                $("#join_tmp_email_host").val(code);
                $("#join_tmp_email_host").attr("disabled",true);
                $("#sel_mem_email").html(code+' <i class="dropdown-caret fa fa-caret-down"></i>');
                $("#join_tmp_email_host").css("background-color",'#e2e2e2');

            }
        }

        function sel_mem_cell(code){
            $("#sel_mem_cell_name").empty();
            $("#sel_mem_cell_name").append(code);
            $("#sel_mem_cell").attr("value",code);
        }

        function join_send_email(){
            var url="/member/find_pwd_email_send";
            var exptext = /^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/;

            mem_email = $("#join_tmp_email").val()+"@"+$("#join_tmp_email_host").val();
            if ($("#join_tmp_email").val() == "" || $("#join_tmp_email_host").val() == "") {
                alert("메일주소를 입력해 주세요.");
            }else if (exptext.test(mem_email)!=true){
                alert('이메일 형식이 올바르지 않습니다.');
                $("#join_tmp_email_host").val('');
            }else{
                $("#join_send_email_btn").hide();
                $("#send_join_mail_loading").show();
                $.post(url,
                    {
                    mem_email : mem_email,
                    kind : "join"
                    },
                    function(data){

                        if(data.trim()=="OK"){
                            alert("<?php echo lang('strSendVerificationCodeDone');?>");
                        }else{
                            alert("입력하신 이메일로 가입한 아이디가 이미 있습니다.");
                        }
                        $("#send_join_mail_loading").hide();
                        $("#join_send_email_btn").show();
                    }
                );

            }


        }

        function member_join_step3(){
            mem_type=$("input:radio[name='sel_mem_type']:checked").val();
            if(mem_type==""){
                alert("회원구분을 선택해 주세요.");
                return false;
            }else if(mem_type=="company"){
                if($("#join_com_name").val()==""){
                    alert("회사명을 입력해 주세요");
                    return false;
                }
                if($("#join_com_no1").val()=="" || $("#join_com_no2").val()=="" || $("#join_com_no3").val()==""){
                    alert("사업자번호를 입력해 주세요");
                    return false;
                }
            }
            if($("#join_tmp_email").val()==""){
                alert("메일주소를 입력해 주세요.");
                return false;
            }
            if($("#join_tmp_email_host").val()==""){
                alert("메일주소를 입력해 주세요.");
                return false;
            }
            if($("#join_certify_no").val()==""){
                alert("인증번호를 입력해 주세요.");
                return false;
            }else{
                $.ajax({
                    type:"POST",
                    url : '/member/member_join_step3',
                    data : {
                        cert_no: $("#join_certify_no").val(),
                        mem_email : $("#join_tmp_email").val()+"@"+$("#join_tmp_email_host").val(),
                        kind : "join",
                        mem_type : mem_type,
                        mem_com_nm : $("#join_com_name").val(),
                        mem_com_no : $("#join_com_no1").val()+"-"+$("#join_com_no2").val()+"-"+$("#join_com_no3").val()
                        },
                    timeout : 30000,
                    async:false,
                    cache : false,
                    success: function (data){
                        if(data.trim()=="false"){
                            alert("<?echo lang('strVerificationCodeFailed')?>");
                            $("#id_certify_no").focus();
                        }else{
                            $('#modal_content').empty();
                            $('#modal_content').append(data);

                        }
                    }
                });
            }
        }

        function id_check(){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $("#mem_id").val();
                $("#mem_id").val(inputVal.replace(/[^a-z0-9]/gi,''));
            }

            $.ajax({
                type:"POST",
                url:"/member/id_check/",
                data : {mem_id: $("#mem_id").val()},
                timeout : 30000,
                async:false,
                cache : false,
                success: function (data){
                    $("#idcheck_commit").val("");
                    switch(data.trim()){
                        case "true":
                            var show_args="<font color='#5177bc'>사용가능한 아이디</font>";
                            $("#idcheck_commit").val("Y");
                            break;
                        case "false":
                            var show_args="<font color='#c85757'>중복된 아이디</font>";
                            break;
                        case "none":
                            var show_args="<font color='#c85757'>아이디를 입력하세요.</font>";
                            break;
                        case "short":
                            var show_args="<font color='#c85757'>3글자 이상 입력해 주세요.</font>";
                            break;
                        case "long":
                            var show_args="<font color='#c85757'>20글자 이하만 가능합니다.</font>";
                            break;

                    }

                    $('#idcheck_result').html(show_args);
                },
                error: function whenError(e){
                    alert("code : " + e.status + "\r\nmessage : " + e.responseText);
                }
            });
        }

        function sel_mem_lang(lang){
            $("#sel_mem_lang_name").empty();
            if(lang == "korean"){
                lang_name = "한국";
            }else if(lang == "JPN"){
                lang_name = "일본";
            }else{
                lang_name = "북한";
            }

            $("#sel_mem_lang_name").append(lang_name);
            $("#sel_mem_lang").attr("value",lang);
        }
        function member_join_step4_1(){

            var frm = document.member_info_com;
            url = '/member/member_join_step4';
            frm.action = url;
            mem_type = frm.sel_mem_type.value;
            mem_id = frm.mem_id.value;
            mem_pwd = frm.mem_pwd.value;
            mem_pwd2 = frm.mem_pwd2.value;
            mem_nm = frm.mem_nm.value;
            mem_com_site_nm = frm.mem_com_site_nm.value;
            mem_com_url = frm.mem_com_url.value;
            mem_com_nm = frm.mem_com_name.value;
            mem_tel = frm.sel_mem_tel.value+"-"+frm.mem_tel1.value+"-"+frm.mem_tel2.value;
            mem_cell = frm.sel_mem_cell.value+"-"+frm.mem_cell1.value+"-"+frm.mem_cell2.value;
            mem_email = frm.mem_email.value+"@"+frm.mem_email_host.value;
            mem_com_no = frm.mem_com_no.value;
            if (mem_id == ""){
                alert("아이디를 입력해 주세요.");
            }else if (mem_pwd == ""){
                alert("비밀번호를 입력해 주세요.");
            }else if (mem_pwd != mem_pwd2){
                alert("비밀번호가 일치하지 않습니다.");
            }else if (mem_pwd.length < 6){
                alert("비밀번호가 너무 짧습니다.(6자리 이상)");
            }else if (mem_com_nm == ""){
                alert("회사명을 입력해 주세요.");
            }else if (mem_nm == ""){
                alert("이름을 입력해 주세요.");
            }else if (mem_tel == "" || mem_tel == "--"){
                alert("전화번호를 입력해 주세요.");
            }else if (mem_cell == "" || mem_cell == "--"){
                alert("핸드폰번호를 입력해 주세요.");
            }else if (mem_email == "" || mem_email == "@"){
                alert("이메일을 입력해 주세요.");
            }else{
                $("#join3_next_btn").hide();
                $.post(url,
                        {
                    mem_id : mem_id,
                    mem_pwd : mem_pwd,
                    mem_type : mem_type,
                    mem_com_nm : mem_com_nm,
                    mem_com_no : mem_com_no,
                    mem_tel : mem_tel,
                    mem_cell : mem_cell,
                    mem_email : mem_email,
                    mem_nm : mem_nm,
                    mem_com_site_nm : mem_com_site_nm,
                    mem_com_url : mem_com_url,
                        },
                        function(data){
                            $('#modal_content').empty();
                            $('#modal_content').append(data);
                        }
                    );
            }
        }

        function join_cancel(){
            var url = '/member/member_join_cancel'
            if(confirm('회원가입을 취소할까요?')){
                $.post(url,
                    {
                        mem_no : $("#cancel_mem_no").val()
                    },
                    function(data){
                        $('#modal_content').empty();
                        $('#modal1').modal('hide');
                    }
                );
            }

        }

        function member_join_step4_2(){
            var frm = document.member_info_ind;
            url = '/member/member_join_step4';
            frm.action = url;

            mem_id=frm.mem_id.value;
            mem_pwd=frm.mem_pwd.value;
            mem_com_nm=frm.mem_com_name.value;
            mem_nm=frm.mem_nm.value;
            mem_post=frm.mem_post.value;
            mem_com_site_nm=frm.mem_com_site_nm.value;
            mem_com_url=frm.mem_com_url.value;
            mem_addr=frm.mem_address1.value+" "+frm.mem_address2.value;
            mem_tel=frm.sel_mem_tel.value+"-"+frm.mem_tel1.value+"-"+frm.mem_tel2.value;
            mem_cell=frm.sel_mem_cell.value+"-"+frm.mem_cell1.value+"-"+frm.mem_cell2.value;
            mem_email=frm.mem_email.value+"@"+frm.mem_email_host.value;

            if(mem_id==""){
                alert("아이디를 입력해 주세요.");
            }else if(mem_pwd==""){
                alert("비밀번호를 입력해 주세요.");
            }else if (mem_pwd.length < 6){
                alert("비밀번호가 너무 짧습니다.(6자리 이상)");
            }else if(mem_com_nm==""){
                alert("회사명을 입력해 주세요.");
            }else if(mem_nm==""){
                alert("이름을 입력해 주세요.");
            }else if(mem_post==""){
                alert("우편번호를 입력해 주세요.");
            }else if(mem_addr==""){
                alert("주소를 입력해 주세요.");
            }else if(mem_tel==""){
                alert("전화번호를 입력해 주세요.");
            }else if(mem_cell==""){
                alert("핸드폰번호를 입력해 주세요.");
            }else{
                $("#join3_next_btn").hide();
                $.post(url,
                        {
                    mem_id : mem_id,
                    mem_pwd : mem_pwd,
                    mem_nm : mem_nm,
                    mem_com_nm : mem_com_nm,
                    mem_com_site_nm : mem_com_site_nm,
                    mem_com_url : mem_com_url,
                    mem_tel : mem_tel,
                    mem_cell : mem_cell,
                    mem_email : mem_email,
                    mem_post : mem_post,
                    mem_addr: mem_addr,
                    mem_type : 'individual'
                        },
                        function(data){
                            if(data.trim()=="finish"){
                                $('#modal1').modal("hide");
                                modal_clear();
                                $('#join_done').modal('show');
                            }
                        }
                    );
            }
        }

        function member_join_step5(){
            var frm = document.company_info;
            url = '/member/member_join_step5';
            frm.action = url;
            mem_no=frm.mem_no.value;
            mem_com_ceo=frm.mem_com_ceo.value;
            mem_com_type=frm.mem_com_type.value;
            mem_com_item=frm.mem_com_item.value;
            mem_post=frm.mem_post.value;
            mem_address=frm.mem_address1.value+" "+frm.mem_address2.value;
            if (mem_com_ceo == ""){
                alert("대표자명을 입력해 주세요.");
            }
            else if (mem_com_type == ""){
                alert("업태를 입력해 주세요,");
            }
            else if (mem_com_item == ""){
                alert("종목을 입력해 주세요.");
            }
            else if (mem_address == "" || mem_post == ""){
                alert("사업장 주소를 입력해 주세요");
            }else{
                $.post(url,
                    {
                        mem_no : mem_no,
                        mem_com_ceo : mem_com_ceo,
                        mem_com_type : mem_com_type,
                        mem_com_item : mem_com_item,
                        mem_post : mem_post,
                        mem_address : mem_address
                    },
                    function(data){
                        if(data.trim()=="true"){
                            $('#modal1').modal("hide");
                            modal_clear();
                            $('#join_done').modal('show');
                        }
                    }
                );
            }
        }

        function member_id_find_step1(){
            $.ajax({
                type:"POST",
                url:"/member/member_id_find_step1",
                data : {},
                success: function (data){
                    $('#modal_content').append(data);
                    $("#modal1").modal("show");
                }
            });
        }

        function member_id_find_step2(){
            var frm = document.member_id_find_info;
            url = '/member/member_id_find_step2';
            type=$("input:radio[name='find_id_mem_gb']:checked").val();
            if(type==undefined){
                alert("회원구분을 선택해 주세요.");
            }else{
                if(type=="company"){
                    if(frm.mem_com_nm.value==''){
                        alert("회사명(법인명)을 입력해 주세요.");
                        frm.mem_com_nm.focus();
                        return;
                    }
                    else if((frm.id_com_no1.value=='')||(frm.id_com_no2.value=='')||(frm.id_com_no3.value=='')){
                        alert("사업자번호를 입력해 주세요.");
                        frm.id_com_no1.focus();
                        return;
                    }
                }

                frm.action = url;
                mem_com_nm=frm.mem_com_nm.value;
                mem_com_reg_no=frm.id_com_no1.value+"-"+frm.id_com_no2.value+"-"+frm.id_com_no3.value;
                $.post(url,
                    {
                        mem_com_nm : mem_com_nm,
                        mem_com_no : mem_com_reg_no
                    },
                    function(data){
                        if(data!="false"){
                             $('#modal_content').empty();
                             $('#modal_content').append(data);
                        }else{
                            alert("가입정보를 찾을 수 없습니다.");
                        }
                     });
            }
        }

        function email_certify(){
            var mem_no=0;
            if($('input[name="select_id"]:checked').val()>0){
                  mem_no=$('input[name="select_id"]:checked').val();
            }
            if(mem_no!="0"){
                $("#insert_email").append($('#email_certify_'+mem_no).val());
                $("#id_tmp_email").attr("value",$('#email_certify_'+mem_no).val());
                $("#select_id").attr("disabled",true);
                $("#after_hidden").hide();
                $("#login_btn").show();
                $("#email_find").show();
            }else{
                alert("아이디를 선택해 주세요.");
            }

        }

        function find_id_send_email(kind){
            var url="/member/find_pwd_email_send";
            if(kind=="com"){
                mem_email = $("#id_tmp_email").val();
            }else{
                mem_email= $("#join_tmp_email").val()+"@"+$("#join_tmp_email_host").val();
            }
            if(mem_email=="@"){
                alert("메일주소를 입력해 주세요.");
            }else{
                $("#send_id_mail_loading").show();
                $("#find_id_send_email_btn").hide();
                $.post(url,
                    {
                        mem_email : mem_email,
                        kind : "id"
                    },
                    function(data){
                        if(data.trim()=="OK"){
                            $("#find_id_send_email_btn").hide()
                            alert("<?php echo lang('strSendVerificationCodeDone');?>");
                        }else{
                            alert("메일발송 실패. 관리자에게 문의해 주세요.");
                        }
                        $("#send_id_mail_loading").hide();
                        $("#find_id_send_email_btn").show();
                    }
                );
            }
        }

        function member_id_find_step3(kind){
            if(kind=="com"){
                mem_email = $("#id_tmp_email").val();
            }else{
                mem_email= $("#join_tmp_email").val()+"@"+$("#join_tmp_email_host").val();
            }
            if(kind=="ind" && $("#id_certify_no").val()==""){
                alert("인증번호를 입력해 주세요.");
            }else{
                $.ajax({
                    type:"POST",
                    url : '/member/member_id_find_step3',
                    data : {cert_no: $("#id_certify_no").val(), mem_email : mem_email},
                    timeout : 30000,
                    async:false,
                    cache : false,
                    success: function (data){
                        if(data.trim()=="false"){
                            alert("인증번호가 일치하지 않습니다.");
                            $("#id_certify_no").focus();
                        }else{
                            $('#modal_content').empty();
                            $('#modal_content').append(data);
                        }
                    }
                });
            }
        }

        function member_pwd_find_step1(){
            $.ajax({
                type:"POST",
                url:"/member/member_pwd_find_step1",
                data : {},
                success: function (data){
                    $('#modal_content').append(data);
                    $("#modal1").modal("show");
                }
            });
        }

        function member_pwd_find_step2(){
            var frm = document.find_pwd_info;
            url = '/member/member_pwd_find_step2';
            mem_type=$("input:radio[name='sel_mem_type']:checked").val();
            if(mem_type==undefined){
                alert("회원 구분을 선택해 주세요.");
                return;
            }else{
                if(frm.mem_id.value==''){
                    alert("아이디를 입력해 주세요.");
                    frm.mem_id.focus();
                    return;
                }

                if(mem_type=="company"){
                    if((frm.pwd_com_no1.value=='')||(frm.pwd_com_no2.value=='')||(frm.pwd_com_no3.value=='')){
                        alert("사업자번호를 입력해 주세요.");
                        frm.pwd_com_no1.focus();
                        return;
                    }
                    mem_com_reg_no=frm.pwd_com_no1.value+"-"+frm.pwd_com_no2.value+"-"+frm.pwd_com_no3.value;
                }else{
                    mem_com_nm='';
                    mem_com_reg_no='';
                }

                frm.action = url;
                mem_id = frm.mem_id.value;
                $.post(url,
                    {
                    mem_id : mem_id,
                    mem_com_no : mem_com_reg_no,
                    mem_type : mem_type
                    },
                    function(data){
                        if(data!="false"){
                            $('#modal_content').empty();
                            $('#modal_content').append(data);
                        }else{
                            $('#modal1').modal('hide');
                            $('#pwd_find_error').modal('show');
                        }
                    }
                );
            }
        }

        function login(){
            var frm = document.login_info;
            frm.action = '/login/login_check';
            mem_id = frm.mem_id_login.value;
            mem_pwd = frm.mem_pwd.value;
            if (mem_id == "" && mem_pwd == ""){
                alert("아이디와 비밀번호를 입력해 주세요.");
            }else if (mem_id == ""){
                alert("아이디를 입력해 주세요");
            }else if (mem_pwd == ""){
                alert("비밀번호를 입력해 주세요");
            }else{
                frm.submit();
            }

        }

        function login_ok(){
            var frm = document.login_info;
            url = '/login/login_process';
            frm.action = url;
            mem_id=frm.mem_id_login.value;
            mem_pwd=frm.mem_pwd.value;
            frm.submit();
        }


        function length_check(id,length){
            if(length=="3"){
                if($("#"+id+"1").val().length==3){
                    $("#"+id+"2").focus();
                }
            }else if(length=="2"){
                if($("#"+id+"2").val().length==2){
                    $("#"+id+"3").focus();
                }
            }
        }

        $('#join_com_no1').blur(function() {
          alert('Handler for .blur() called.');
        });

        function find_pwd_send_email(){
            var url="/member/find_pwd_email_send";
            mem_email = $("#tmp_email").val();
            $("#send_pwd_mail_loading").show();
            $("#find_pwd_send_email_btn").hide();

            $.post(url,
                    {
                    mem_email : mem_email,
                    kind : 'pwd'
                    },
                    function(data){
                        if(data.trim()=="OK"){
                            alert("<?php echo lang('strSendVerificationCodeDone');?>");
                            $("#find_pwd_guide").show();
                            $("#find_pwd_input").show();
                            $("#new_pwd_save_btn").show();

                        }else{
                            alert("메일발송 실패. 관리자에게 문의해 주세요.");
                        }
                        $("#find_pwd_send_email_btn").show();
                        $("#send_pwd_mail_loading").hide();
                    }
                );
        }
        function cert_no_check(){
            if($("#pwd_certify_no").val().length=="6"){

                $.ajax({
                    type:"POST",
                    url : '/member/cert_no_check',
                    data : {cert_no: $("#pwd_certify_no").val(), mem_email : $("#tmp_email").val(), kind:"pwd" },
                    timeout : 30000,
                    async:false,
                    cache : false,
                    success: function (data){
                        $("#cert_no_check_commit").val("");
                        switch(data.trim()){
                            case "true":
                                var show_args="<font color='#5177bc'>인증완료</font>";
                                $("#cert_no_check_commit").val("Y");
                                $("#new_pwd").attr("disabled",false);
                                $("#new_pwd2").attr("disabled",false);
                                break;
                            case "false":
                                var show_args="<font color='#c85757'>인증번호 불일치</font>";
                                break;
                        }

                        $('#cert_no_check_result').html(show_args);
                    },
                    error: function whenError(e){
                        alert("code : " + e.status + "\r\nmessage : " + e.responseText);
                    }
                });
            }
        }

        function find_pwd_save(){
            if ($("#new_pwd").val() == "") {
                alert("비밀번호를 입력해 주세요.");
            }else if ( $("#new_pwd2").val() == ""){
                alert("비밀번호 확인을 입력해 주세요.");
            }else if ($("#new_pwd").val() != $("#new_pwd2").val()){
                alert("비밀번호가 일치하지 않습니다.");
                $("#new_pwd").val('')
                $("#new_pwd2").val('')
                $("#new_pwd").focus();
            }else if ($("#new_pwd").val().length < 6){
                alert("비밀번호가 너무 짧습니다.(6자리 이상)");
            }else{
                var url = '/member/find_pwd_save';
                $.post(url,
                        {
                            mem_pwd: $("#new_pwd").val(),
                            mem_email : $("#tmp_email").val()
                        },
                        function(data){
                            if(data.trim()=="OK"){
                                alert("비밀번호 변경이 완료되었습니다. 로그인해주세요");
                                modal_clear();
                               $("#find_password_info2").hide();

                            }else{
                               alert("비밀번호 변경 실패. 관리자에게 문의해 주세요.");

                            }
                        }
                    );
            }
        }

        function only_english(){
            var objEvent = event.srcElement;
            var numPattern =/^[A-Za-z]*$/;
            numPattern = objEvent.value.match(numPattern);

            if (numPattern == null) {
                alert("영어만 입력해 주세요.");
                objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
                objEvent.focus();
                return false;
            }
        }

        function only_english_number(){
            var objEvent = event.srcElement;
            var numPattern = /^[A-Za-z0-9+.]*$/;
            numPattern = objEvent.value.match(numPattern);

            if (numPattern == null) {
                alert("영어와 숫자만 입력해 주세요.");
                objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
                objEvent.focus();
                return false;
            }
        }

        function only_number(){
            var objEvent = event.srcElement;
            var numPattern = /^[0-9+]*$/;
            numPattern = objEvent.value.match(numPattern);

            if (numPattern == null) {
                alert("숫자만 입력해 주세요.");
                objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
                objEvent.focus();
                return false;
            }
        }
        function modal_clear(){
            $('#modal1').modal('hide');
            $('#modal_content').empty();
        }
    </script>

    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="/template/bootstrap/js/bootstrap.min.js"></script>


    <!--Fast Click [ OPTIONAL ]-->
    <script src="/template/plugins/fast-click/fastclick.min.js"></script>


    <!--Nifty Admin [ RECOMMENDED ]-->
    <script src="/template/bootstrap/js/nifty.min.js"></script>


    <!--Background Image [ DEMONSTRATION ]-->
    <script src="/template/bootstrap/js/demo/bg-images.js"></script>

    <!--Modals [ SAMPLE ]-->
    <script src="/template/bootstrap/js/demo/ui-modals.js"></script>

    <script>
        $(document).ready(function() {
            if('<?php echo $login_err;?>' == "err")
            {
                $("#login_error").modal('show');
            }
            $('[data-dismiss=modal]').on('click', function (e) {
                $("#modal1")
                    .find("input,textarea,select")
                    .val('')
                    .end()
                    .find("input[type=checkbox], input[type=radio]")
                    .prop("checked", "")
                    .end();
            })
        });

        $("#modal1").on('hidden.bs.modal', function () {
            $('#modal_content').html("");
            $(this).data('bs.modal', null);
        });

        $(document).keydown(function(e){
            if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA"){
                if(e.keyCode === 8){
                    return false;
                }
            }
        });
    </script>
    </body>
</html>

		
				

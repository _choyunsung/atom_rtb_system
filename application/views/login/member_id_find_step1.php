<div class="modal-header">
    <h4 class="h4_blit ma_t5 pa_b5"><?php echo lang('strForgotID')?></h4>
</div>
<div class="modal-body">
    <div class="join_history">
        <ul>
            <li class="font_bold color_p">01. <?php echo lang('strJoinInfo')?> &gt;</li>
            <li>&nbsp; 02. <?php echo lang('strIDVerification')?> &gt;</li> 
            <li>&nbsp; 03. <?php echo lang('strForgotIDCompleted')?></li>
        </ul>
    </div>
    <div style="clear:both;">
        <div class="ma_b10 pa_l10">
            <span class="sub_tit"><?php echo lang('strJoinInfo')?></span><br />
            <span><?php echo lang('strForgotID1')?></span>
        </div>
        <form name="member_id_find_info" method="post">
        <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray">
            <colgroup>
                <col width="30%">
                <col width="70%">
            </colgroup>
            <tr>
                <th class="point_blue"><?php echo lang('strCategory')?></th>
                <td>
                    <input type="radio"  name="find_id_mem_gb" onclick="find_mem_gb();" checked value="company"></input> <?php echo lang('strBusiness')?> 
                    <!-- 개인회원 <input class="ma_l20" type="radio" name="find_id_mem_gb" onclick="find_mem_gb();" value="individual"></input>  <?php echo lang('strIndividual')?> -->
                </td>
            </tr>
            <tr id="find_id_com_nm">
                <th class="point_blue"><?php echo lang('strCompanyName')?></th>
                <td><input type="text" id="mem_com_nm" name="mem_com_nm"></td>
            </tr> 
            <tr id="find_id_com_no">
                <th class="point_blue"><?php echo lang('strBusinessNo')?></th>
                <td>
                    <input type="text" class="wid_20p" id="id_com_no1" name="id_com_no1" maxlength="3" onkeyup="length_check('id_com_no','3');" onblur="only_number();">
                    - <input type="text" class="wid_20p" id="id_com_no2" name="id_com_no2" maxlength="2" onkeyup="length_check('id_com_no','2');" onblur="only_number();">
                    - <input type="text" class="wid_20p" id="id_com_no3" name="id_com_no3" maxlength="5" onblur="only_number();">
                </td>
            </tr>
            <tr id="id_email_find" style="display:none;">
                <th class="point_blue"><?php echo lang('strEmail')?></th>
                <td>
                    <input type="text" class="float_l wid_20p" id="join_tmp_email" onblur="only_english_number();"> 
                    <span class="float_l pa_t10">&nbsp;@&nbsp;</span> 
                    <input type="text" class="float_l wid_30p" style="background-color:#ebebeb;" id="join_tmp_email_host" onblur="only_english_number();" disabled> 
                    <div class="btn-group float_l ma_l5">
                        <span class="btn btn-default dropdown-toggle ma_l5" data-toggle="dropdown" id="sel_mem_email">
                        &nbsp;  <?php echo lang('strSelect')?>  &nbsp; 
                        <i class="dropdown-caret fa fa-caret-down"></i>
                        </span>
                        <ul class="dropdown-menu ul_sel_box_pa">
                            <?php foreach($email_host_list as $row){?>
                                <li class="txt-left"><a href="#" class="txt-left" onclick="sel_mem_email('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>
                    <span  onclick="find_id_send_email('ind');" class="btn btn-default ma_l5" id="join_send_email_btn"><?php echo lang('strSendCode')?></span>
                </td>
            </tr>
            <tr id="id_email_btn" style="display:none;">
                <th class="point_blue"><?php echo lang('strVerificationCode')?></th>
                <td>
                    <input type="text" class="wid_50p float_l" id="id_certify_no" onblur="only_number();">
                    <span onclick="member_id_find_step3('ind');" class=" float_l ma_l5 btn btn-primary"><?php echo lang('strConfirm')?></span>
                </td>
            </tr>
            </table>
        </form>
    </div>
    <div class="ma_t10 pa_l10 info_txt">
        <?php echo lang('strForgotID2')?>
        <?php echo lang('strForgotID3')?>
    </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10">
    <div class="join_bt_group">
        <span onclick="member_id_find_step2();" class="btn btn-primary" id="id_find_step1_next_btn"><?php echo lang('strNext')?></span>
        <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-dark"><?php echo lang('strClose')?></span>
    </div>
</div>
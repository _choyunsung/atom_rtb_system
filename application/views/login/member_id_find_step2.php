<div class="modal-header">
    <h4 class="h4_blit ma_t5 pa_b5"><?php echo lang('strForgotID')?></h4>
</div>
<div class="modal-body">
    <div class="join_history">
        <ul>
            <li>01. <?php echo lang('strJoinInfo')?> &gt;</li>
            <li class="font_bold color_p">&nbsp; 02. <?php echo lang('strIDVerification')?> &gt;</li> 
            <li>&nbsp; 03. <?php echo lang('strForgotIDCompleted')?></li>
        </ul>
    </div>
    <div style="clear:both;">
        <div class="ma_b10 pa_l10">
            <span class="sub_tit"><?php echo lang('strIDVerification')?></span><br />
            <span><?php echo lang('strIDVerification1')?></span>
        </div>
        <div class="search_id" id="search_id">
            <table width="50%" cellpadding="0" cellspacing="0" border="0" class="t_center">
                <?php foreach ($list as $key=>$row){?>
                <?php $id_cnt=count($row['mem_id'])?>
                <tr>
                    <td class="font_bold">
                        <input type="radio" name="select_id" id="select_id" value="<?php echo $row['mem_no'];?>">
                        </input> &nbsp; <?php echo substr($row['mem_id'],0,$id_cnt-3);?>**
                    </td>
                    <td>
                        (<?php echo lang('strSignupDate')?><?php echo $row['mem_ymd'];?>)
                        <input type="hidden" name="email_certify_<?php echo $row['mem_no'];?>" id="email_certify_<?php echo $row['mem_no'];?>" value="<?php echo $row['mem_email']?>">
                    </td>
                </tr>
                <?php }?>
            </table>
        </div>
    </div>
    <div class="join_bt_group" id="after_hidden">
        <span onclick="modal_clear();" data-dismiss="modal" data-target="#modal_select" data-toggle="modal" class="btn btn-primary"><?php echo lang('strLogin')?></span>
        <span onclick="email_certify();" class="btn btn-primary"><?php echo lang('strEmailVerification')?></span>
    </div>
    <div class="ma_t20 pa_l10 ma_b20 info_txt">
        <?php echo lang('strIDVerification2')?>
        <?php echo lang('strIDVerification3')?>
    </div>
    <div class="search_id" id="email_find" style="display:none;">
        <table width="60%" cellpadding="0" cellspacing="0" border="0" class="t_center send_mail_tb">
            <colgroup>
                <col width="30%">
                <col width="55%">
                <col width="15%">
            </colgroup>
            <tr>
                <th><?php echo lang('strEmail')?></th>
                <input type="hidden" name="id_tmp_email" id="id_tmp_email">
                <td id="insert_email"></td>
                <td>
                    <span id="send_id_mail_loading" style="display:none;"><img src="/img/loading-icon.gif" style="width:50px;height:30px"></span>
                    <span  onclick="find_id_send_email('com');" class="btn btn-default" id="find_id_send_email_btn"><?php echo lang('strSendCode')?></span>
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strVerificationCode')?></th>
                <td>
                    <input type="text" class="wid_45p" name="id_certify_no" id="id_certify_no"  />
                    <input type="hidden" name="id_mem_email" id="id_mem_email" />
                    <input type="hidden" id="id_cert_no_check_commit" name="id_cert_no_check_commit" value="" validate="null,아이디"> 
                    <div id="id_cert_no_check_result">
                        <font color='#ccc'></font>
                    </div>
                </td>
                <td><span onclick="member_id_find_step3('com');" class="btn btn-default"><?php echo lang('strConfirm')?></span></td>
            </tr>
        </table>
    </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10">
    <span onclick="modal_clear();" data-dismiss="modal" data-target="#modal_select" data-toggle="modal" class="btn btn-primary"><?php echo lang('strLogin')?></span>
    <span onclick="modal_clear()" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>
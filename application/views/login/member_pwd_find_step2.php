<div class="modal-header">
    <h4 class="h4_blit ma_t5 ma_b5"><?php echo lang('strForgotPW');?></h4>
</div>
<div class="modal-body">
    <div class="join_history">
        <ul>
            <li>01. <?php echo lang('strJoinInfo');?> ></li>
            <li class="font_bold color_p">&nbsp; 02. <?php echo lang('strEmailVerification');?> ></li> 
            <li>&nbsp; 03. <?php echo lang('strForgotPWCompleted');?></li>
        </ul>
    </div>
    <div class="ma_b10 pa_l10">
        <span class="sub_tit"><?php echo lang('strEmailVerification');?></span><br />
        <span><?php echo lang('strJoinMail');?></span>
    </div>
    <div class="search_id">
        <table width="50%" cellpadding="0" cellspacing="0" border="0" class="t_center">
            <tr>
                <th class="font_bold"><?php echo lang('strEmail')?></th>
                <td id="pwd_insert_email"><?php echo $list['mem_email'];?>
                    <input type="hidden" name="tmp_email" id="tmp_email" value="<?php echo $list['mem_email'];?>">
                </td>
                <td class="font_bold">
                    <span id="send_pwd_mail_loading" style="display:none;"><img src="/img/loading-icon.gif" style="width:50px;height:30px"></span>
                    <span id="find_pwd_send_email_btn" onclick="find_pwd_send_email();" class="btn btn-default"><?php echo lang('strSendCode');?> </span>
                </td>
            </tr>
        </table>	
    </div>
    <div class="ma_t20 ma_b10 pa_b20 pa_l10 info_txt color_dgray">
        <?php echo lang('strForgotID2');?><?php echo lang('strForgotID3');?>
    </div>
    
    
    <div class="ma_b10 pa_l10" id="find_pwd_guide" style="display:none;">
        <span class="sub_tit"><?php echo lang('strNewPassword');?></span><br />
        <span><?php echo lang('strProfile');?></span>
    </div>
    <div class="search_pw" id="find_pwd_input" style="display:none;">
        <form name="find_pwd_step3">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="t_center_pw">
            <colgroup>
                <col width="12%">
                <col width="85%">
            </colgroup>
            <tr>
                <th class="font_bold"><?php echo lang('strVerificationCode');?></th>
                <td>
                <input type="text" class="wid_30p" name="pwd_certify_no" id="pwd_certify_no" onkeyup="cert_no_check('pwd')" maxlength="6"/>
                    <input type="hidden" name="pwd_mem_email" id="pwd_mem_email" />
                    <input type="hidden" id="cert_no_check_commit" name="cert_no_check_commit" value="" validate="null,아이디"> 
                    <div id="cert_no_check_result">
                        <font color='#ccc'></font>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="font_bold"><?php echo lang('strNewPassword');?></th>
                <td><input type="password" class="wid_30p ma_l3" name="new_pwd" id="new_pwd" disabled>  &nbsp; <span class="f-size-9"><?php echo lang('strUsePW');?></span></td>
            </tr>
 
            <tr>
                <th class="font_bold"><?php echo lang('strPWConfirm');?></th>
                <td><input type="password" class="wid_30p" name="new_pwd2" id="new_pwd2" disabled> &nbsp; <span class="f-size-9"><?php echo lang('strPWConfirm1');?></span></td>
            </tr>
        </table>	
        </form>
    </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10 ma">
    <span  onclick="find_pwd_save();" class="btn btn-primary" id="new_pwd_save_btn" style="display:none;"><?php echo lang('strDone')?></span>
    <span  data-dismiss="modal" onclick="modal_clear();" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>
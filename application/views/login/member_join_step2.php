<div class="modal-header">
                <h4 class="h4_blit ma_t5 pa_b5"><?php echo lang('strMemberJoin')?></h4>
            </div>
            <div class="modal-body">
                <div class="join_history">
                    <ul>
                        <li>01. <?php echo lang('strTermsAgree');?> &gt;</li>
                        <li class="font_bold color_p">&nbsp; 02. <?php echo lang('strIdentification');?> &gt;</li> 
                        <li>&nbsp; 03. <?php echo lang('strInfoEntry');?> &gt;</li>
                        <li>&nbsp; 04. <?php echo lang('strJoinCompleted');?></li>
                    </ul>
                </div>
                <h4 class="ma_l10" ><?php echo lang('strIdentification');?></h4>
                <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray ma_b30">
                    <colgroup>
                        <col width="15%">
                        <col width="20%">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th class="point_blue"><?php echo lang('strUserType')?></th>
                        <td colspan="2">
                            <input type="radio"  name="sel_mem_type" onclick="sel_mem_type('company')" value="company" checked> <?php echo lang('strBusiness')?>
                            <!-- 개인회원 <input class="ma_l20" name="sel_mem_type" type="radio" onclick="sel_mem_type('individual')" value="individual">  <?php echo lang('strIndividual')?>  -->
                        </td>
                        <!--
                        <td>
                            <div class="btn-group" id="sel_mem_gb_div" style="display:none">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_gb_name">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo lang('strSelect')?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <input type="hidden" id="sel_mem_gb">
                                <ul class="dropdown-menu" >
                                    <?php foreach($member_gb_list as $row){?>
                                        <li><a href="#" onclick="sel_mem_gb('<?php echo $row['code_key'];?>')"><?php echo lang($row['code_desc']);?></a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                         -->
                    </tr>
                    <tr id="com_join_1">
                        <th class="point_blue"><?php echo lang('strCompanyName');?></th>
                        <td colspan="2"><input type="text" class="wid_60p" id="join_com_name"></td>
                    </tr>
                    <tr id="com_join_2">
                        <th class="point_blue"><?php echo lang('strBusinessNo');?></th>
                        <td colspan="2">
                            <input type="text" class="wid_20p" id="join_com_no1" name="join_com_no1" maxlength="3" onkeyup="length_check('join_com_no','3');" onblur="only_number();" >
                            - <input type="text" class="wid_20p" id="join_com_no2" name="join_com_no2" maxlength="2" onkeyup="length_check('join_com_no','2');" onblur="only_number();">
                            - <input type="text" class="wid_20p" id="join_com_no3" name="join_com_no3" maxlength="5" onkeyup="com_reg_no_check();" onblur="only_number(); ">
                            <input type="hidden" id="reg_no_check_commit" name="reg_no_check_commit" value="" validate="null,아이디">
                            <span id="reg_no_check_result">
                            </span>
                        </td>
                    </tr>
                </table>
                <h4 class="ma_l10"><?php echo lang('strEmailVerification');?></h4>
                <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray">
                    <colgroup>
                        <col width="15%">
                        <col width="*">
                        <col width="1%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strEmail')?></th>
                        <td>
                            <input type="text" class="float_l wid_25p" id="join_tmp_email">
                            <span class="float_l pa_t7">&nbsp;@&nbsp;</span> 
                            <input type="hidden" class="float_l wid_25p" id="join_tmp_email_host" style="background-color:#e2e2e2;" >
                            <div class="float_l ma_l5 btn-group">
                                <span class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_email">
                                &nbsp;  <?php echo lang('strSelect')?>  &nbsp;
                                <i class="dropdown-caret fa fa-caret-down"></i>
                                </span>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php foreach($email_host_list as $row){?>
                                        <li class="txt-left"><a href="#" class="txt-left" onclick="sel_mem_email('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                            <div id="send_join_mail" style="text-align:left;">
                                <span id="send_join_mail_loading" style="display:none;"><img src="/img/loading-icon.gif" style="width:50px;height:30px"></span>
                                <span  onclick="join_send_email();" class="btn btn-default ma_l10" id="join_send_email_btn"><?php echo lang('strSendCode');?>
                            </div>

                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strVerificationCode');?></th>
                        <td>
                            <input type="text" class="wid_25p float_l" id="join_certify_no" maxlength="6" onblur="only_number();">
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer ma_t10">
                <span onclick="member_join_step3();" class="btn btn-primary"><?php echo lang('strNext')?></span>
			    <span onclick="modal_clear();" class="btn btn-dark"><?php echo lang('strClose')?></span>
    	    </div>
<?php 
if($kind=="join"){
    $str1="회원가입";
}elseif($kind=="pwd"){
    $str1="비밀번호 확인";
}else{
    $str1="아이디 확인";
}
?>

<div style="width:650px; height:340px; padding:20px; border:5px solid #4859a1;overflow:hidden;">
    <div style="height:50px; border-bottom:1px solid #ebebeb;">
        <h3 style="width:20%; float:left"><span style="color:#83bf00">인증번호</span> 발송</h3>
        <span style="color:#ccc; margin-left:10px; margin-top:20px; float:left; font-size:12px;;"><?php echo $str1;?>을 위해 인증번호를 입력해 주시기 바랍니다.</span>
    </div>
    <div style="clear:both; width:100%; overflow:hidden;"> 
        <div style="float:left; width:159px; height:193px; margin-left:20px; margin-top:35px; background:url('http://atom.adop.cc/img/email_bg.png');">
        </div>
        <div style="float:left; width:60%; margin-left:40px; margin-top:20px;">
            <p style="font-size:1.5em; clear:both; margin-bottom:15px; ">
                <span style="color:#4859a1">ADOP</span>
                <span style="font-size:0.9em">에서</span> 
                <span style="font-weight:bold">알려드립니다.</span>
            </p>
            <p style="font-size:0.9em; margin-bottom:15px;">
                안녕하세요, 광고주 회원정보 담당자 입니다.<br />
                <?php echo $str1;?>을 위한 인증번호는 다음과 같습니다.
            </p>
            <div style="width:395px; border:1px solid #4859a1;margin-bottom:3px;"></div>
            <div style="width:395px; height:80px;border:1px solid #ebebeb;">
                <div style="text-align:center; margin-top:30px;">
                    <span style="text-align:center;">인증번호 : </span><span style="border-bottom:1px solid #ebebeb;"> <?php echo $cert_no?> </span>
                </div>
            </div>
            <p style="font-size:0.7em;margin-top:10px; width:100%;">
                다른 문의사항이 있으시면 담당부서 
                <span style="color:#509fd0">atom@adop.co.kr</span> 로 문의해 주시기 바랍니다.
            </p>
        </div>
    </div>
</div>

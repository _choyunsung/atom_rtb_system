<div class="modal-header">
    <h4 class="h4_blit ma_t5 ma_b5"><?php echo lang('strMemberJoin')?></h4>
</div>
<div class="modal-body">
    <div class="join_history">
        <ul>
            <li>01. <?php echo lang('strTermsAgree');?> &gt;</li>
            <li>&nbsp; 02. <?php echo lang('strIdentification');?> &gt;</li> 
            <li class="font_bold color_p">&nbsp; 03. <?php echo lang('strInfoEntry');?> &gt;</li>
            <li>&nbsp; 04. <?php echo lang('strJoinCompleted');?></li>
        </ul>
    </div>
    <div class="ma_b10 pa_l10">
        <span class="sub_tit"><?php echo lang('strInfoEntry');?></span><br />
        <span><?php echo lang('strIndividual1');?></span>
    </div>
    <form name="member_info_ind" method="post">
        <table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb bd-t2-gray">
            <colgroup>
                <col width="20%">
                <col widt="80%">
            </colgroup>	
            <tr>
                <th class="point_blue">* <?php echo lang('strName')?></th>
                <td>
                    <input type="text" class="wid_30p" id="mem_nm" name="mem_nm">
                    <input type="hidden" name="mem_gb" id="mem_gb" value="IND"/> 
                </td>
            </tr>	
            <tr>
                <th class="point_blue">* <?php echo lang('strID')?></th>
                <td>
                    <input type="text" class="wid_30p float_l" maxlength="15" minlength="3" id="mem_id" name="mem_id" style="ime-mode: disabled;" onkeyup="id_check()">
                    <input type="hidden" id="idcheck_commit" name="idcheck_commit" value="" validate="null,아이디" class="float_l"> 
                    <div id="idcheck_result">
                        <font class="float_l ma_l10 ma_t8 join_info">* <?php echo lang('strJoinBusiness');?></font>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strPassword')?></th>
                <td>
                    <input type="password" class="wid_30p" id="mem_pwd" name="mem_pwd" minlength="6" maxlength="10">
                    <font class="join_info ma_l10">* <?php echo lang('strJoinBusiness1');?></font>
                </td>
            </tr>
            <tr>
                <th class="point_blue ">* <?php echo lang('strPWConfirm')?></th>
                <td colspan="3"><input type="password" class="wid_30p" id="mem_pwd2" name="mem_pwd2"></td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strEmail')?></th>
                <td>
                    <input type="text" class="wid_20p" id="mem_email" name="mem_email" value="<?php echo $email?>" readonly> 
                    @ <input type="text" class="wid_30p" id="mem_email_host" name="mem_email_host" value="<?php echo $email_host?>" readonly>
                    <font class="join_info float_l ma_t5 ma_l10">* <?php echo lang('strJoinBusiness2');?></font>
                    
                </td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strOfficeNumber')?></th>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_tel_name">
                        &nbsp;  <?php echo lang('strSelect')?>  &nbsp; 
                        <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <input type="hidden" id="sel_mem_tel">
                        <ul class="dropdown-menu ul_sel_box">
                            <?php foreach($tel_no_list as $row){?>
                                <li><a href="#" onclick="sel_mem_tel('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>&nbsp;&ndash;
                    <input type="text" class="wid_10p" id="mem_tel1" name="mem_tel1" maxlength="4" onblur="only_number();">&nbsp;&ndash;
                    <input type="text" class="wid_10p" id="mem_tel2" name="mem_tel2" maxlength="4" onblur="only_number();">
                </td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strMobileNumber')?></th>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_cell_name">
                        &nbsp;  <?php echo lang('strSelect')?>  &nbsp; 
                        <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <input type="hidden" id="sel_mem_cell">
                        <ul class="dropdown-menu ul_sel_box">
                            <?php foreach($cell_no_list as $row){?>
                                <li><a href="#" onclick="sel_mem_cell('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>  
                    <span class="float_l ma_t8">&nbsp;&ndash;&nbsp;</span>  
                    <input type="text" class="wid_10p float_l" id="mem_cell1" name="mem_cell1" maxlength="4" onblur="only_number();">  
                    <span class="float_l ma_t8">&nbsp;&ndash;&nbsp;</span>  
                    <input type="text" class="wid_10p float_l" id="mem_cell2" name="mem_cell2" maxlength="4" onblur="only_number();">
                </td>
            </tr>	
            <tr>
                <th><?php echo lang('strWebsite');?></th>
                <td>
                    <span class="point_dgray"><?php echo lang('strSiteName')?></span> <input type="text" class="wid_30p" id="mem_com_site_nm" name="mem_com_site_nm">
                    <span class="point_dgray ma_l10">URL</span> <input type="text" class="wid_40p" id="mem_com_url" name="mem_com_url" value="http://">
                </td>
            </tr> 
            <tr>
                <th class="point_blue">* <?php echo lang('strCompany1')?></th>
                <td>
                    <input type="text" class="wid_30p" id="mem_com_name" name="mem_com_name"><font class="join_info ma_l10"><?php echo lang('strJoinBusiness3');?></font>
                </td>
            </tr> 	
            <tr>
                <th rowspan="2" class="point_blue">* <?php echo lang('strBizAdress')?></th>
                <td><input type="text" class="wid_30p" id="mem_post" name="mem_post">
                <span onclick="sample4_execDaumPostcode()" class="btn btn-sm btn-default ma_l5" ><?php echo lang('strFindZipcode')?></span><br></td>
            </tr>	
            <tr>
                <td colspan="3">
                <input type="text" class="wid_40p" id="mem_address1" name="mem_address1" readonly>
                &nbsp;<input type="text" class="wid_30p" id="mem_address2" name="mem_address2">
                </td>
            </tr>
        </table>	
    </form>	
</div>

<!--Modal footer-->
<div class="modal-footer ma_t8 ma">
    <span onclick="member_join_step4_2();" class="btn btn-primary" id="join3_next_btn"><?php echo lang('strNext')?></span>
    <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-primary"><?php echo lang('strCancel')?></span>
</div>
<script>
$(document).keydown(function(e){
    if(e.target.nodeName != "INPUT" && e.target.nodeName != "TEXTAREA"){
        if(e.keyCode === 8 || e.keyCode === 27){
            alert('취소를 눌러주세요.');
            return false;
        }
    }
});
</script>
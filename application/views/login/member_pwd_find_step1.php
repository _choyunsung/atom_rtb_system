<div class="modal-header">
    <h4 class="h4_blit ma_t5 ma_b5"><?php echo lang('strForgotPW');?></h4>
</div>
<div class="modal-body">
    <div class="join_history">
        <ul>
            <li class="font_bold color_p">01. <?php echo lang('strJoinInfo');?> ></li>
            <li>&nbsp; 02. <?php echo lang('strEmailVerification');?> ></li> 
            <li>&nbsp; 03. <?php echo lang('strForgotPWCompleted');?></li>
        </ul>
    </div>
    <div style="clear:both;">
        <div class="ma_b10 pa_l10">
            <span class="sub_tit"><?php echo lang('strJoinInfo');?></span><br />
            <span><?php echo lang('strForgotPW1');?></span>
        </div>
        <form name="find_pwd_info" method="post">
        <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray">
            <colgroup>
                <col width="30%">
                <col width="70%">
            </colgroup>
            <tr>
                <th class="point_blue"><?php echo lang('strUserType')?></th>
                <td>
                    <input type="radio"  name="sel_mem_type" onclick="$('#com_reg_no').show();" checked value="company"> <?php echo lang('strBusiness')?>
                    <!-- 개인회원 <input class="ma_l20" name="sel_mem_type" type="radio" onclick="$('#com_reg_no').hide();"  value="individual">  <?php echo lang('strIndividual')?> -->
                </td>
            </tr>
            <tr>
                <th class="point_blue"><?php echo lang('strID')?></th>
                <td><input type="text" class="wid_60p" name="mem_id" id="mem_id"></td>
            </tr>
            <tr>
                <!--  <th class="point_blue">회사명 <span class="point_dgray">(법인명)</span></th>
                <td><input type="text" class="wid_60p" id="pwd_com_nm" name="pwd_com_nm"></td>
                       <td>
                    <input type="text" class="wid_20p" id="mem_email" name="mem_email" value="<?php echo $email?>" readonly> 
                    @ <input type="text" class="wid_30p" id="mem_email_host" name="mem_email_host" value="<?php echo $email_host?>" readonly>
                </td> -->
            </tr>	
            <tr id="com_reg_no">
                <th class="point_blue"><?php echo lang('strBusinessNo');?></th>
                <td>
                    <input type="text" class="wid_20p" id="pwd_com_no1" name="pwd_com_no1" maxlength="3" onkeyup="length_check('pwd_com_no','3');" onblur="only_number();">
                    - <input type="text" class="wid_20p" id="pwd_com_no2" name="pwd_com_no2" maxlength="2" onkeyup="length_check('pwd_com_no','2');" onblur="only_number();">
                    - <input type="text" class="wid_20p" id="pwd_com_no3" name="pwd_com_no3" maxlength="5" onblur="only_number();">
                </td>
            </tr>
        </table>
        </form>
    </div>
    <div class="ma_t10 pa_l10 info_txt">
        <?php echo lang('strForgotID2');?><?php echo lang('strForgotID3');?>
    </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10 ma">
    <span onclick="member_pwd_find_step2();" class="btn btn-primary"><?php echo lang('strNext')?></span>
    <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>
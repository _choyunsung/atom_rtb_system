<div class="modal-header">
    <h4 class="h4_blit ma_t5 pa_b5"><?php echo lang('strMemberJoin');?></h4>
</div>
<div class="modal-body">
    <form name="member_info_com" method="post">
        <table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb bd-t2-gray">
            <colgroup>
                <col width="20%">
                <col width="30%">
                <col width="20%">
                <col width="30%">
            </colgroup>
            <tr>
                <th class="point_blue">* <?php echo lang('strBusinessName');?></th>
                <td id="mem_com_name_view">
                    <input type="hidden" name="mem_com_name" id="mem_com_name" value="<?php echo $mem_com_nm;?>"/><?php echo $mem_com_nm;?>
                    <input type="hidden" name="sel_mem_type" id="sel_mem_type" value="<?php echo $sel_mem_type;?>"/> 
                    
                </td>
                <th class="point_blue bd_l"><?php echo lang('strBusinessNo');?></th>
                <td id="mem_com_no_view"><input type="hidden" name="mem_com_no" id="mem_com_no" value="<?php echo $mem_com_no;?>"/><?php echo $mem_com_no;?></td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strID')?></th>
                <td colspan="3">
                    <input type="text" class="float_l wid_30p" maxlength="20" id="mem_id" name="mem_id" style="ime-mode: disabled;" onkeyup="id_check()">
                    <input type="hidden" class="float_l wid_30p" id="idcheck_commit" name="idcheck_commit" value="" validate="null,아이디"> 
                    <div class="float_l ma_l10" id="idcheck_result">
                        <font class="float_l ma_t8 join_info">* <?php echo lang('strJoinBusiness');?></font>
                    </div>
                </td>
                <!-- 
                <th class="bd_l">국가</th>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_lang_name">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;국가&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <input type="hidden" id="sel_mem_lang" name="mem_lang">
                        <ul class="dropdown-menu" >
                            <li><a href="#" onclick="sel_mem_lang('KOR');">한국</a>
                            </li>
                            <li><a href="#" onclick="sel_mem_lang('JPN');">일본</a>
                            </li>
                            <li><a href="#" onclick="sel_mem_lang('CHN');">중국</a>
                            </li>
                        </ul>
                    </div>
                </td>
                 -->
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strPassword')?></th>
                <td colspan="3">
                    <input type="password" class="wid_30p" id="mem_pwd" name="mem_pwd">
                    <font class="join_info ma_l10">* <?php echo lang('strJoinBusiness1');?></font>
                </td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strPWConfirm');?></th>
                <td colspan="3"><input type="password" class="wid_30p" id="mem_pwd2" name="mem_pwd2"></td>
            </tr>
            <tr>
                <th class="point_blue ">* <?php echo lang('strContactName');?></th>
                <td colspan="3">
                    <input class="wid_30p" id="mem_nm" name="mem_nm" type="text">
                </td>
            </tr> 
            <tr>
                <th class="point_blue">* <?php echo lang('strEmail')?></th>
                <td colspan="3">
                    <input type="text" class="wid_20p" id="mem_email" name="mem_email" value="<?php echo $email?>"disabled> 
                    @ <input type="text" class="wid_30p" id="mem_email_host" name="mem_email_host" value="<?php echo $email_host?>"disabled> 
                   
                    <font class="join_info float_l ma_l10 ma_t5">* <?php echo lang('strJoinBusiness2');?></font>
                </td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strOfficeNumber');?></th>
                <td colspan="3">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_tel_name">
                        &nbsp;  <?php echo lang('strSelect')?>  &nbsp; 
                        <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <input type="hidden" id="sel_mem_tel">
                        <ul class="dropdown-menu ul_sel_box">
                            <?php foreach($tel_no_list as $row){?>
                                <li><a href="#" onclick="sel_mem_tel('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>  <span class="float_l ma_t8">&nbsp;&ndash;&nbsp;</span> 
                    <input type="text" class="wid_10p float_l" id="mem_tel1" name="mem_tel1" maxlength="4"> <span class="float_l ma_t8">&nbsp;&ndash;&nbsp;</span> 
                    <input type="text" class="wid_10p float_l" id="mem_tel2" name="mem_tel2" maxlength="4">
                </td>
            </tr>
            <tr>
                <th class="point_blue">* <?php echo lang('strMobileNumber')?></th>
                <td colspan="3">
                    <div class="btn-group">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_cell_name">
                        &nbsp;  <?php echo lang('strSelect')?>  &nbsp; 
                        <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <input type="hidden" id="sel_mem_cell">
                        <ul class="dropdown-menu ul_sel_box">
                            <?php foreach($cell_no_list as $row){?>
                                <li><a href="#" onclick="sel_mem_cell('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                </li>
                            <?php }?>
                        </ul>
                    </div>  
                    <span class="float_l ma_t8">&nbsp;-&nbsp;</span>
                    <input type="text" class="wid_10p float_l" id="mem_cell1" name="mem_cell1" maxlength="4"> <span class="float_l ma_t8">&nbsp;-&nbsp;</span> 
                    <input type="text" class="wid_10p float_l" id="mem_cell2" name="mem_cell2" maxlength="4">
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strWebsite');?></th>
                <td colspan="3">
                    <span class="point_dgray"><?php echo lang('strSiteName');?></span> <input type="text" class="wid_30p" id="mem_com_site_nm" name="mem_com_site_nm">
                    <span class="point_dgray ma_l10">URL</span> <input type="text" class="wid_40p" id="mem_com_url" name="mem_com_url" value="http://">
                </td>
            </tr> 
        </table>
    </form>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t8">
    <span onclick="member_join_step4_1();" class="btn btn-primary" id="join3_next_btn"><?php echo lang('strNext')?></span>
    <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>

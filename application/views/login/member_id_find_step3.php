<div class="modal-header">
    <h4 class="h4_blit ma_t5 ma_b5"><?php echo lang('strForgotID')?></h4>
</div>
<div class="modal-body">
    <div class="ma_b10 pa_l10">
        <span class="sub_tit"><?php echo lang('strForgotIDCompleted')?></span><br />
        <span><?php echo lang('strForgotIDCompleted1')?></span>
    </div>
    <div class="search_id">
        <table width="50%" cellpadding="0" cellspacing="0" border="0" class="t_center">
            <tr>
                <th class="font_bold"><?php echo lang('strID')?></th>
                <td><?php echo $list['mem_id'];?></td>
                <th class="font_bold"><?php echo lang('strSignupDate')?></th>
                <td><?php echo $list['mem_ymd'];?></td>
            </tr>
        </table>	
    </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10">
    <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-primary"><?php echo lang('strLogin')?></span>
</div>
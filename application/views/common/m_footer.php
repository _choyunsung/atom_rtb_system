        <div class="mobile_foot">
            <p class="bold">＠Copyright. 2015 ADOP Inc. All rights reserved.</p>
<!--             <p><span class="bold">T.</span> + 82 - 2-2052-1117 <br /><span class="bold">E.</span> contact@adop.cc / <br /><span class="bold">A.</span> (135-845) 서울특별시 강남구 테헤란로 86길 14 윤천빌딩 3, 4, 5 층 애드오피</p> -->
        </div>
    </div>
</div>
<!-- FOOTER -->
        <!--===================================================-->

        <!-- END FOOTER -->

        <!-- SCROLL TOP BUTTON -->
        <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
	<!-- END OF CONTAINER -->

	<!--JAVASCRIPT--> 
	<!--=================================================-->

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/bootstrap.min.js"></script>


	<!--Fast Click [ OPTIONAL ]-->
	<script src="/template/plugins/fast-click/fastclick.min.js"></script>

	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/nifty.min.js"></script>

	<script src="/template/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

	<!--Background Image [ DEMONSTRATION ]-->
	<script src="/template/bootstrap/js/demo/bg-images.js"></script>

	<!--Modals [ SAMPLE ]-->
	<script src="/template/bootstrap/js/demo/ui-modals.js"></script>
	
</body>
<script>
$('#lmenu_trigger').click(function () {
	$('#left_menu').addClass('open')
    $('#left_menu').show('slide');
});
$('#m_con').click(function () {
	$('#left_menu').removeClass('open')
	$('#left_menu').hide('slide');
});

        
$(document).ready(function() {
	$('[data-dismiss=modal]').on('click', function (e) {
	    $("#modal1")
	    .find("input,textarea,select")
	       .val('')
	       .end()
	    .find("input[type=checkbox], input[type=radio]")
	       .prop("checked", "")
	       .end();
	})
});
function modal_clear(){
	$('#modal1').modal('hide');
	$('#modal_content').empty();
}

</script>
</html>

        <div class="adop_footer">
        	<div class="adop_copy">
        		<span>＠Copyright. 2015 ADOP Inc. All rights reserved.</span>
<!--         		<p class="text-sm ma_t10">T. + 82 - 2-2052-1117   E. contact@adop.cc / A. (135-845) 서울특별시 강남구 테헤란로 86길 14 윤천빌딩 3, 4, 5 층 애드오피</p> -->
        	</div>
        </div>
    </div>
</div>             	
<!-- 결제 modal -->
<div class="modal" id="cash_modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content">

        </div>
    </div>
</div> 	
            	
<!-- FOOTER -->
        <!--===================================================-->

        <!-- END FOOTER -->

        <!-- SCROLL TOP BUTTON -->
        <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
	<!-- END OF CONTAINER -->

	<!--JAVASCRIPT--> 
	<!--=================================================-->

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/bootstrap.min.js"></script>


	<!--Fast Click [ OPTIONAL ]-->
	<script src="/template/plugins/fast-click/fastclick.min.js"></script>

	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/nifty.min.js"></script>

	<script src="/template/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>

	<!--Background Image [ DEMONSTRATION ]-->
	<script src="/template/bootstrap/js/demo/bg-images.js"></script>

	<!--Modals [ SAMPLE ]-->
	<script src="/template/bootstrap/js/demo/ui-modals.js"></script>

    <!-- ADOP common js-->
	<script src="/js/adop.common.js" ></script>
	<script src="/js/adop.js" ></script>

</body>
<script>
function cash_charge_step1(){
	var mem_pay_type = '<?php echo $this->session->userdata('mem_pay_type')?>';
	if( mem_pay_type == 'Y'){
		alert("후불 사용자는 충전할 수 없습니다.");
	}else{
        $.ajax({
            type:"POST",
            url:"/cash/cash_charge_step1/",
            data : {mem_no: <?php echo $this->session->userdata('mem_no');?>},
            success: function (data){
                $('#cash_modal').modal('show');
                $('#modal_content').empty();
                $('#modal_content').append(data);
            }
        });
	}
}

function cash_charge_step2(mem_no){

    $("#cash_charge_btn").hide();
	url = "/cash/cash_charge_step2";
	if($('input:radio[name="sel_amount"]:checked').val() == "fix"){
		charge_money = uncomma($("#charge_money1").val());
		charge_cash = uncomma($("#charge_cash_view").html());
	}else{
		charge_money = uncomma($("#charge_money2").val());
		charge_cash = uncomma($("#charge_cash_view").html());
	}

	if ($("#charge_money1").val() == "" && $("#charge_money2").val() == ""){
        alert("금액을 선택해 주세요.");
    }else{
	   $.post(url,
            {
    		   mem_no : mem_no,
    		   charge_cash : charge_cash,
    		   charge_money : charge_money,
    		   cash_type : 'C',
    		   charge_st : 'R',
    		   charge_way : $('input:radio[name="charge_way"]:checked').val(),
    		   origin_cash : uncomma($("#origin_cash").html()),
    		   after_charge : uncomma($("#after_charge").html()),
            },
            function(data){
                	$('#modal_content').empty();
                    $('#modal_content').append(data)
            }
        );
    }
	
}
        
$(document).ready(function() {
	$('[data-dismiss=modal]').on('click', function (e) {
	    $("#modal1")
	    .find("input,textarea,select")
	       .val('')
	       .end()
	    .find("input[type=checkbox], input[type=radio]")
	       .prop("checked", "")
	       .end();
	})
});
function modal_clear(){
	$('#modal1').modal('hide');
	$('#modal_content').empty();
}

</script>
</html>
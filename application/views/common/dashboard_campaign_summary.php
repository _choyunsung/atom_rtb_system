<?php if($this->agent->is_mobile()){ ?>
<table id="cam_summary_tb" class="table table-striped table-bordered table-hover scroll m_dash_camp_tb" cellspacing='0'>
<?php }else{?>
<table id="cam_summary_tb" class="table table-striped table-bordered table-hover scroll dash_camp_tb" cellspacing='0'>
<?php }?>
    <colgroup>
        <col width="50%">
        <col width="*">
        <col width="20%">
    </colgroup>
    <thead>
        <tr>
            <th colspan="3"><?php echo date("Y년 m월 d일 ") ?> 집계 등락률(7일전 대비)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo lang('strImpression')?><?php echo lang('strCount')?></td>
            <td><span class="float_r ma_r5"><?php echo number_format($summary_data['imp'])?></span></td>
            <td>
                <span class="ma_r10 <?php if( substr($summary_data['imp_rate'], 0, 1)=="-"){echo "color_r"; }elseif ($summary_data['imp_rate'] == "0"){}else{ echo "color_g";} ?>">
                    <i class="fa <?php if (substr($summary_data['imp_rate'], 0, 1)=="-"){echo "fa-sort-down pa_b5"; }elseif ($summary_data['imp_rate'] == "0"){}else{ echo "fa-sort-up pa_t3";} ?>  ma_r10"></i>
                    <?php echo number_format($summary_data['imp_rate'], 0)?>%
                </span>
            </td>
        </tr>
        <tr>
            <td><?php echo lang('strClick')?><?php echo lang('strCount')?></td>
            <td><span class="float_r ma_r5"><?php echo number_format($summary_data['clk'])?></span></td>
            <td>
                <span class="ma_r10 <?php if (substr($summary_data['clk_rate'], 0, 1)=="-"){echo "color_r"; }elseif ($summary_data['clk_rate'] == "0"){}else{ echo "color_g";} ?>">
                    <i class="fa <?php if (substr($summary_data['clk_rate'], 0, 1)=="-"){echo "fa-sort-down pa_b5"; }elseif ($summary_data['clk_rate'] == "0"){}else{ echo "fa-sort-up pa_t3";} ?>  ma_r10"></i>
                    <?php echo number_format($summary_data['clk_rate'], 0)?>%
                </span>
            </td>
    
        </tr>
        <tr>
            <td>CTR</td>
            <td><span class="float_r ma_r5"><?php echo number_format($summary_data['ctr'], 2)?> %</span></td>
            <td>
                <span class="ma_r10 <?php if (substr($summary_data['ctr_rate'], 0, 1)=="-"){echo "color_r"; }elseif ($summary_data['ctr_rate'] == "0" || $summary_data['last_ctr'] == "0"){}else{ echo "color_g";} ?>">
                    <i class="fa <?php if (substr($summary_data['ctr_rate'], 0, 1)=="-"){echo "fa-sort-down pa_b5"; }elseif ($summary_data['ctr_rate'] == "0" || $summary_data['last_ctr'] == "0"){}else{ echo "fa-sort-up pa_t3";} ?>  ma_r10"></i>
                    <?php if ($summary_data['last_ctr'] != "0"){ echo number_format($summary_data['ctr_rate']);}else{echo "0";}?> %
                </span>
            </td>
        </tr>
        <tr>
            <td> <?php echo lang('strPPC')?> (PPC)</td>
            <td><span class="float_r ma_r5"><?php echo number_format($summary_data->ppc)?></span></td>
            <td>
                <span class="ma_r10 <?php if(substr($summary_data->ppc_rate,0,1)=="-"){echo "color_r"; }elseif ($summary_data->ppc_rate == ""){}else{ echo "color_g";} ?>">
                    <i class="fa <?php if(substr($summary_data->ppc_rate,0,1)=="-"){echo "fa-sort-down pa_b5"; }elseif ($summary_data->ppc_rate == ""){}else{ echo "fa-sort-up pa_t3";} ?>  ma_r10"></i>
                    <?php echo number_format($summary_data->ppc_rate)?> %
                </span>
            </td>
        </tr>
        <tr>
            <td> <?php echo lang('strPPI')?> (PPI)</td>
            <td><span class="float_r ma_r5"><?php echo number_format($summary_data['ppi'])?></span></td>
            <td>
                <span class="ma_r10 <?php if (substr($summary_data['ppi_rate'], 0, 1)=="-"){echo "color_r"; }elseif ($summary_data['ppi_rate'] == ""){}else{ echo "color_g";} ?>">
                    <i class="fa <?php if (substr($summary_data['ppi_rate'], 0, 1)=="-"){echo "fa-sort-down pa_b5"; }elseif ($summary_data['ppi_rate'] == ""){}else{ echo "fa-sort-up pa_t3";} ?>  ma_r10"></i>
                    <?php echo number_format($summary_data['ppi_rate'])?> %
                </span>
            </td>
        </tr>
        <tr>
            <td> 총 광고비용 (VAT 포함)</td>
            <td><span class="float_r ma_r5"><?php echo number_format($summary_data['price'])?></span></td>
            <td>
                <span class="ma_r10 <?php if(substr($summary_data['price_rate'], 0, 1)=="-"){echo "color_r"; }elseif ($summary_data['price_rate'] == "0"){}else{ echo "color_g";} ?>">
                    <i class="fa <?php if(substr($summary_data['price_rate'], 0, 1)=="-"){echo "fa-sort-down pa_b5"; }elseif ($summary_data['price_rate'] == "0"){}else{ echo "fa-sort-up pa_t3";} ?>  ma_r10"></i>
                    <?php echo number_format($summary_data['price_rate'])?> %
                </span>
            </td>
        </tr>
    </tbody>
</table>
<script>
</script>
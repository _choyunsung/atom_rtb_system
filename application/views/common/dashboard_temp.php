<div class="panel min_height" style="overflow:hidden;">
    <div class="history_box">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#">Dashboard</a></li>
        </ol>
    </div>	
    <div class="panel-body">

    	<br /><br />
    	
    	<div class="camp">
    		<div class="cash_div">
    			
    			<h4 class="cash_blit">이번달 예상 금액</h4>
    			<p class="ma_l10 f-size-16"><span class="point_blue f_bold">2,000,000</span>원</p>
    			<p class="ma_l10 wid_60p txt-right point_dgray">(부가세 별도)</p>
    			<p class="ma_l10 wid_60p txt-right point_dgray">2016-01-15 기준</p>
    		</div>
    	    <div class="cash_div">
    			<h4 class="cash_blit">증빙 서류 종류</h4>
    			<p class="ma_l10 f-size-16 ma_t10 point_blue f_bold">전자 세금계산서</p>
    			
    		</div>
     	    <div class="cash_div">
    			<h4 class="cash_blit">과금 형태</h4>
    			<p class="ma_l10 f-size-16 ma_t10 point_blue f_bold">선불제 및 후불제</p>
    		</div>   
     	    <div class="cash_div bd_r_0">
    			<h4 class="cash_blit">수신처</h4>
    			<span class="ma_l10 ma_t10 f-size-16 point_blue f_bold">삼성전자 <br /></span>
    			<span class="ma_l10 point_dgray">tax@adop.co.kr</span>
    			<p class="text-right point_dgray pa_b5"> 수신처변경 <i class="fa fa-gear"></i></p>
    			
    		</div>    		
    		
    		
    			
    	</div>
    	
    	
    	
    	
    	
    	
    	
    	<div style="width:420px; padding-bottom:20px;" class="bd-t7">    	 
		<div class="modal-header">
                <h4 class="h4_blit">세금계산서 보기</h4>
        </div>
   		<div style="pading-left:20px; width:100%;">
   		
   			<div style="padding:20px">
   				<h5 class="blue_blit">세금계산서 수신처</h5>
   			<table style="width:100%" class="bill_tb">
   				<tr>
   					<th>사업자명</th>
   					<td>삼성전자</td>
   				</tr>
   				<tr>
   					<th>담당자</th>
   					<td>홍길동</td>
   				</tr>
   				<tr>
   					<th>이메일 주소</th>
   					<td>tax.adop.co.krffhr</td>
   				</tr>   				
   				
   				   			
   			</table>
   			
   			
   			</div>
   			
   			

   			<div style="padding:20px">
   			  <h5 class="blue_blit">세금계산서 내역</h5>
	   			<table style="width:100%" class="bill_tb">
	   				<tr>
	   					<th colspan="2">삼성전자 8월 정산</th>
	   				</tr>
	   				<tr>
	   					<th>사업자명</th>
	   					<td>삼성전자</td>
	   				</tr>
	   				<tr>
	   					<th>담당자</th>
	   					<td>홍길동</td>
	   				</tr>
	   				<tr>
	   					<th>이메일 주소</th>
	   					<td>tax.adop.co.krffhr</td>
	   				</tr>   				
	   				
	   				   			
	   			</table>
	   			
	   			   			
   			<table style="width:100%" class="bill_tb">
   				<tr>
   					<th>세금계산서 발행일</th>
   			
   					<td>2015년 8월 31</td>
   				</tr>
   				
   				   			
   			</table>
   			</div>
   			

   			
   		</div>
	   		
    </div>
    	
    	<br /><br />
    	
    	
    	    	
    	<div class="camp">
    		<div class="report_tb_top">
				<div class="report_tb_div_l">
					<img src="/img/radio_on.png">
				</div>	
				<div  class="report_tb_div_r">
				    <span class="color_b">노출수</span>
					<p>10,504,559</p>	
				</div>		
    		</div>
    		
    	    <div class="report_tb_top">
				<div class="report_tb_div_l">
					<img src="/img/radio_on_p.png">
				</div>	
				<div  class="report_tb_div_r">
				    <span class="color_p">클릭수</span>
					<p>10,504,559</p>	
				</div>		
    		</div>
    		
     	    <div class="report_tb_top">
				<div class="report_tb_div_l">
					<img src="/img/radio_on_o.png">
				</div>	
				<div class="report_tb_div_r">
				    <span class="color_o">CTR</span>
					<p>10,504,559</p>	
				</div>			
    		</div>   
 	
     	    <div class="report_tb_top">
				<div class="report_tb_div_l">
					<img src="/img/radio_on_g.png">
				</div>	
				<div  class="report_tb_div_r">
				    <span class="color_g">평균 PPC</span>
					<p>10,504,559</p>	
				</div>					
    		</div>   
    		   		
         	 <div class="report_tb_top">
				<div class="report_tb_div_l">
					<img src="/img/radio_on_b2.png">
				</div>	
				<div  class="report_tb_div_r">
				    <span class="color_b2">평균 PPI</span>
					<p>10,504,559</p>	
				</div>		
			</div>
     	    <div class="report_tb_top bd_r_0 " >
				<div class="report_tb_div_l">
					<img src="/img/radio_on_r.png">
				</div>	
				<div  class="report_tb_div_r">
				    <span class="color_r">총 광고비</span>
					<p>10,504,559</p>	
				</div>			
    		</div>    		
      	</div>  		    	
    	
    	
    	
    	
    	<br /><br />    	
    	
    	
    	<div class="report_info clear" >
    		<span>이벤트 캐쉬는 환불이 불가합니다. 환불은 마운틴 캐쉬 금액안에서만 가능합니다.</span>
    		<span>환불을 신청하시면 금액은 재정산되고, 마운틴 캐쉬 금액과는 다소 상이할 수 있습니다</span>
    		<span>환불을 원하시면 환불하기를 클릭해주세요. </span>
    	</div>

    	</div>
    <br /><br />
    	
    	
    	
    	   	
    	<div class="camp">
    		<div class="target_div" >
    			<div class="target_dv_l"><img src="/img/target_time.png"></div>
    			<div class="float_l">
	    			<h5>요일 및 시간대</h5>
	    			<p>최대 노출 요일 및 시간대</p>
	    			<span>월요일, 07시</span>
    			</div>
    		</div>
  
    			<div class="target_div" >
    			<div class="target_dv_l"><img src="/img/target_os.png"></div>
    			<div class="float_l">
    	  		 <h5>운영체제</h5>
    			<p>최대 노출 운영체제</p>
    			<span>Windows7</span>
    		    </div>
    		</div>
    			<div class="target_div" >
    			<div class="target_dv_l"><img src="/img/target_bs.png"></div>
    			<div class="float_l">
    			<h5>브라우저</h5>
    			<p>최대 노출 브라우저</p>
    			<span>Safari</span>
    		    </div>   
    		</div>   
    			<div class="target_div" >
    			<div class="target_dv_l"><img src="/img/target_dv.png"></div>
    			<div class="float_l">
    			<h5>디바이스</h5>
    			<p>최대 노출 디바이스</p>
    			<span>Mobile</span>
    		</div>  
    		</div>     
    			<div class="target_div bd_r_0" >
	    			<div class="target_dv_l"><img src="/img/target_ct.png"></div>
	    			<div class="float_l">
	    			<h5>카테고리</h5>
	    			<p>최대 노출 카테고리</p>
	    			<span>엔터테이먼트</span>
    		</div>    		
    		</div>    
    		
    			
    	</div>
    	
    	
    	<br /><br />
    	

    	
    	
    	
    	
    	
 <div style="width:100%;">
 	<h4 class="tit_blit">캐쉬현황</h4>   	
	 <table class="dash_tb wid_100p"cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
	
		<!-- Table Header -->
		<thead>
			
		<colgroup>
			<col width="25%">
			<col width="25%">
			<col width="25%">
			<col width="25%">
		</colgroup>
		
			<tr>
				<th>잔액</th>
				<th>예상 소진일</th>
				<th>오늘 총비용</th>
				<th>어제 총비용</th>
			</tr>
		</thead>
		<!-- Table Header -->
	
		<!-- Table Body -->
		<tbody>
	
			<tr>
				<td class="color_g f-size-16">89,500</td>
				<td class="point_dgray font_bold f-size-16">2015/07/15</td>
				<td class="color_b f-size-16">345,200</td>
				<td class="color_o f-size-16" >15,000</td>
			</tr><!-- Table Row -->
	
		
			<!-- Darker Table Row -->
	
	
		</tbody>
		<!-- Table Body -->
	
	</table>
</div>    	
   
    	
    		
    	
        	
   <div class=" float_l wid_45p ma_r20">
 	<h4 class="clear tit_blit ma_l10">종료예정 광고그룹</h4>   	
			<table class="dash_tb"cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
				<colgroup>
					<col width="40%">
					<col width="*">
					<col width="15%">
				</colgroup>
			<!-- Table Header -->
			<thead>
				<tr>
					<th>번호</th>
					<th>제목</th>
					<th>등록일 </th>
				</tr>
			</thead>
			<!-- Table Header -->
		
			<!-- Table Body -->
			<tbody>
		
				<tr>
					<td class="txt-left pa_l20">현대자동차 팔아염</td>
					<td>2015/06/18 ~ 2017/07/01</td>
					<td class="color_r">D-01</td>
		
				</tr><!-- Table Row -->
		
				<tr class="even">
					<td class="txt-left pa_l20">추파춥스팔아염</td>
					<td>2015/06/18 ~ 2017/07/01</td>
					<td class="color_o">D-07</td>
				
				</tr>
					<tr>
					<td class="txt-left pa_l20">구루뽕팔아욤</td>
					<td>2015/06/18 ~ 2017/07/01</td>
					<td>D-30</td>
		
				</tr><!-- Table Row -->
		
				<tr class="even">
					<td class="txt-left pa_l20">중고 액정 팔아욤</td>
					<td>2015/06/18 ~ 2017/07/01</td>
					<td>D-30</td>
		
				</tr>	
					</tr>
					<tr>
					<td class="txt-left pa_l20">제이에스티나 광고이벤트</td>
					<td>2015/06/18 ~ 2017/07/01</td>
					<td>D-30</td>
			
				</tr>	
				
				
				<!-- Darker Table Row -->
		
		
			</tbody>
			<!-- Table Body -->
		
		</table>
    	
 </div> 	
    	
        	
         	
   
    	
    	
    	
   <div class="float_l wid_45p">
 	<h4 class="tit_blit ma_l10">공지사항</h4>       	
	<table class="dash_tb wid_45p  float_l"cellspacing='0'> <!-- cellspacing='0' is important, must stay -->

	<!-- Table Header -->
	<thead>
		<tr><th>&nbsp;</th>
			<th>캠페인</th>
			<th>광고그룹</th>
			<th>광고</th>
		</tr>
	</thead>
	<!-- Table Header -->

	<!-- Table Body -->
	<tbody>

		<tr>
			<td class="color_b sub_th" >전체</td>
			<td>17</td>
			<td>18</td>
			<td>18</td>
		</tr><!-- Table Row -->

		<tr class="even">
			<td class="color_g sub_th">진행</td>
			<td>17</td>
			<td>19</td>
			<td>18</td>
		</tr>
			<tr>
			<td class="color_o sub_th">준비</td>
			<td>17</td>
			<td>18</td>
			<td>18</td>
		</tr><!-- Table Row -->

		<tr class="even">
			<td class="color_r sub_th">중지</td>
			<td>17</td>
			<td>19</td>
			<td>18</td>
		</tr>	
			
		<tr>
			<td class="color_b2 sub_th">완료</td>
			<td>17</td>
			<td>18</td>
			<td>18</td>
		</tr	
		
		
		<!-- Darker Table Row -->


	</tbody>
	<!-- Table Body -->

</table>
</div>    	   	
    	
    	
 	   	
   	<div class="clear wid_100p">
	 	<h4 class="tit_blit"">문의사항</h4>    	
			<table class="dash_tb"cellspacing='0'> <!-- cellspacing='0' is important, must stay -->
					<colgroup>
						<col width="10%">
						<col width="29%">
						<col width="29%">
						<col width="29%">
					</colgroup>
				<!-- Table Header -->
				<thead>
					<tr>
					<th>&nbsp;</th>
					<th>검수중</th>
						<th>승인완료</th>
						<th>비승인</th>
					</tr>
				</thead>
				<!-- Table Header -->
			
				<!-- Table Body -->
				<tbody>
			
					<tr>
						<td>이미지</td>
						<td>17</td>
						<td>18</td>
						<td>18</td>
					</tr><!-- Table Row -->
			
					<tr class="even">
						<td>플래시</td>
						<td>17</td>
						<td>19</td>
						<td>18</td>
					</tr>
						<tr>
						<td>텍스트</td>
						<td>17</td>
						<td>18</td>
						<td>18</td>
					</tr><!-- Table Row -->
			
					
					
					<!-- Darker Table Row -->
			
			
				</tbody>
				<!-- Table Body -->
			
			</table>
		</div>   	    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
   </div>
   </div>
</div>

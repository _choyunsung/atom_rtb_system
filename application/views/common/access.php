<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ATOM</title>
        <link rel="shortcut icon" href="http://www.adop.cc/wp-content/uploads/2015/01/adop_logo_16x16.png">
        <!--STYLESHEET-->
        <!--=================================================-->

        <!--Open Sans Font [ OPTIONAL ] -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


        <!--Bootstrap Stylesheet [ REQUIRED ]-->
        <link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">


        <!--Nifty Stylesheet [ REQUIRED ]-->
        <link href="/template/bootstrap/css/nifty.css" rel="stylesheet">


        <!--Font Awesome [ OPTIONAL ]-->
        <link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">


        <!--Demo [ DEMONSTRATION ]-->
        <link href="/template/bootstrap/css/demo/nifty-demo.css" rel="stylesheet">

        <!--jQuery [ REQUIRED ]-->
        <script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>

        <!-- Highcharts -->
        <script src="/template/plugins/Highcharts-4.1.5/js/highcharts.js"></script>

    </head>
    <body>
        <!-- 요기 -->
        <div id="container" class="">
            <!-- BACKGROUND IMAGE -->
            <!--===================================================-->
            <div id="bg-overlay" class="bg-img img-balloon"></div>
            <!-- LOGIN FORM -->
            <!--===================================================-->
            <div class="cls-content">
                <div class="cls-content-sm">
                     <div class="access_frm">
                   	 <span><img src="/img/logo_atom.png"></span>
                   	 <p><?php echo lang('strAccess');?></p>
                   	 <button onclick="location.replace('/')"><?php echo lang('strGohome');?></button>
	                 </div><!-- //access_frm -->
                </div><!-- //cls-content-sm -->
            </div><!-- //cls-content -->
        </div><!-- //container -->
        <!-- 요기까지 -->
        <script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
 
        <!--JAVASCRIPT-->
        <!--=================================================-->

        <!--BootstrapJS [ RECOMMENDED ]-->
        <script src="/template/bootstrap/js/bootstrap.min.js"></script>

        <!--Nifty Admin [ RECOMMENDED ]-->
        <script src="/template/bootstrap/js/nifty.min.js"></script>

    </body>
</html>
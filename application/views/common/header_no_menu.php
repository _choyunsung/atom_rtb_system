<?php
    if($this->session->userdata('logged_in')){

    }else{
        redirect("/login");
    }
?>
<!DOCTYPE html>
<html lang="ko">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ATOM</title>
    <link rel="shortcut icon" href="http://www.adop.cc/wp-content/uploads/2015/01/adop_logo_16x16.png">
	<!--STYLESHEET-->
	<!--=================================================-->

	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


	<!--Bootstrap Stylesheet [ REQUIRED ]-->  
	<link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">


	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="/template/bootstrap/css/nifty.css" rel="stylesheet">

	 
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">
	
	<!--Bootstrap Datepicker [ OPTIONAL ]-->
	<link href="/template/plugins/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
    

	<!--Demo [ DEMONSTRATION ]-->
	<link href="/template/bootstrap/css/demo/nifty-demo.css" rel="stylesheet">

    <!--jQuery [ REQUIRED ]-->
	<script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>

	<!-- Highcharts -->
	<script src="/template/plugins/Highcharts-4.1.5/js/highcharts.src.js"></script>

	<script type="text/javascript" src="/template/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/template/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>
    <!-- optional -->
    <script type="text/javascript" src="/template/plugins/datatables/extensions/ColVis/js/dataTables.colVis.js"></script>
    <!-- optional -->
    <script type="text/javascript" src="/template/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
	<!-- optional(행열고정 -->

    <!--Animate.css [ OPTIONAL ]-->
    <link href="/template/plugins/animate-css/animate.min.css" rel="stylesheet">

    <!--Bootbox Modals [ OPTIONAL ]-->
    <script src="/template/plugins/bootbox/bootbox.min.js"></script>


    <script type="text/javascript" src="/template/plugins/datatables/extensions/FixedColumns/js/dataTables.fixedColumns.js"></script>
    <link href="/template/plugins/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet">
    <link href="/template/plugins/datatables/extensions/FixedColumns/css/dataTables.fixedColumns.css" rel="stylesheet">
    <link href="/template/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.css" rel="stylesheet">
    <link href="/template/plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet">
    <script type="text/javascript" src="/template/assets/js/plugins.js"></script>

    <link href="/js/datetimepicker/jquery.datetimepicker.css" rel="stylesheet">
    <script src="/js/datetimepicker/jquery.datetimepicker.js"></script>
    <script src="/js/battatech_excelexport/src/Scripts/jquery.battatech.excelexport.min.js"></script>
    
    <script src="/template/plugins/ckeditor/ckeditor.js"></script>
    <script>
    function logout(){
        location.replace('/login/logout_process');
    }

   	function alarm_confirm(alarm_no){           	
		$('#alarm_list_' + alarm_no).hide();

       	var alarm_count = $(".alarm_list").filter(function() {
       	    return $(this).css('display') !== 'none';
       	}).length;
       	
        $.ajax({
            url : "/service/alarm_confirm",
            dataType : "html",
            beforeSend: function() {
                $('#alarm_count').show().fadeIn('slow');
                $('#alarm_msg_count').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {alarm_no: alarm_no},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){   	
    			$('#alarm_count').html(alarm_count);
    			$('#alarm_msg_count').html(alarm_count);
            }
        });
        
   	}
    </script>
    <style>
        .navi_02 ul>li {
            display: inline;
            color: #fff;
            overflow: hidden;
            margin: 0 <?php if(count($rows) > 7){echo "8px";}else{echo "20px";}?>;
            font-weight: bold;
            font-size: 1.2em;
        }
    </style>
</head>

<body>
    <div id="container" class="effect mainnav-lg">
        <!--NAVBAR-->
        <header id="navbar">
        	<div id="navbar-container" class="boxed">
            	<div class="nav_mid">
        		    <!--Brand logo & name-->
                    <div class="navbar-header">
                    	<a href="/" class="navbar-brand">
                    		<div class="brand-title">
                    			<img src="/img/logo_atom2.png">
                    		</div>
                    	</a>
                    </div>
                    <!--End brand logo & name-->
                    <div class="navbar-content clearfix">
                		<div class="navi_02">
                            <ul>
                			    <!-- <li><a href="/dashboard"><?php echo lang('strHome')?></a></li> -->
                			    <?php 
                			    //print_r($rows);
                			    
                			         foreach($rows as $row){
                			         
                			    ?>
                			        <li>
                			             <a href="
                			                 <?php
                			                     if($row['cate_nm'] == "strAccountMgr"&&$this->session->userdata('mem_type') == "manager"){
                                                    echo "/account/account_manager_view";
                                                 }else{
                                                    echo $row['cate_url'];
                                                 }
                                             ?>
                                          " style="<?php if($row['cate_url'] == $main_menu[0]['cate_url']){ echo 'border-bottom:4px solid #f05;'; };?>">
                			                 <?php echo lang($row['cate_nm'])?>
            			                 </a>
        			                </li>
                				<?php }?>
                			</ul>
                		</div>
                		<div class="navi_03">
    					   <ul class="nav navbar-top-links pull-left float_r">
    							<li class="dropdown ma_l0 ma_r0">
    								<a href="#" data-toggle="dropdown" class="dropdown-toggle">	
    									<!-- ID로 출력 
    									<span class="user_name"><?php echo $this->session->userdata('mem_nm')?></span>
    									
    									아래 mb_strimwidth 안에 출력될 내용은 회사명입니다. mem_com_nm 넣으시면 됩니다.
    									 -->
    									<img alt="" src="/img/top_nav_01.png" /> 
    									<span class="user_name">
    									
    									<?php 
                                            function str_count($str){
                                                $kChar = 0;
                                                for( $i = 0 ; $i < strlen($str) ;$i++){
                                                    $lastChar = ord($str[$i]);
                                                    if($lastChar >= 127){
                                                        $i= $i+2;
                                                    }
                                                    $kChar++;
                                                }
                                                return $kChar;
                                            }
    									
                                            if(str_count($this->session->userdata('mem_com_nm')) > 14){
                                                echo mb_strimwidth($this->session->userdata('mem_com_nm'), 0, 28)."..";
                                            }else{
                                                echo $this->session->userdata('mem_com_nm');
                                            }
    									?>
    									</span>
    								</a>
                                    <div class="dropdown-menu dropdown-menu-right with-arrow panel-default">
                                        <div class="center">
                                            <a href="/language/switch_language/korean">
                                                <img alt="" src="/img/icons/KR-flag-32.png" /> 한국어 <?php if($this->session->userdata("site_lang") == 'korean'){echo '*';} ?>
                                            </a>
                                        </div>
                                        <div class="ma_t8 center">
                                            <a href="/language/switch_language/english">
                                                <img alt="" src="/img/icons/US-flag-32.png" /> English <?php if($this->session->userdata("site_lang") == 'english'){echo "*";} ?>
                                            </a>
                                        </div>
                                        <div class="ma_t8 ma_b10 center">
                                            <a onclick="logout();" class="btn btn-primary">
                                                <i class="fa fa-sign-out fa-fw"></i> <?php echo lang('strLogout')?>
                                            </a>
                                        </div>
                                    </div>
    							</li>
    							<li class="dropdown">
    							    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
    									<img src="/img/top_nav_03.png">
										<?php 
    									   if(count($alarm_info) > 0){
    									?>
										<span class="badge badge-header badge-warning" id="alarm_count"><?php echo count($alarm_info);?></span>
										<?php 
    									   }
										?>
    								</a>
    								<div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow" style="width:460px;">
        								<div class="pad-all bord-btm" style="width:100%;">
        									<p class="text-lg text-muted text-thin mar-no">You have <span id="alarm_msg_count"><?php echo count($alarm_info);?></span> messages.</p>
        								</div>
        								<div class="nano scrollable">
        									<div class="nano-content">
        										<ul class="head-list">
        											<?php 
                                                        foreach($alarm_info as $alarm){
        											?>
        											<!-- Dropdown list-->
        											<li class="alarm_list" id="alarm_list_<?php echo $alarm['alarm_no'];?>">
        												<a class="media" style="padding-left:0px;">
        													<div class="media-left">
        														<span class="icon-wrap icon-circle bg-primary">
    																<i class="fa fa-comment fa-lg"></i>
    															</span>
        													</div>
        													<div class="media-body">
        														<div class="text-nowrap"><?php echo lang($alarm['alarm_cont']);?></div>
        														<small class="text-muted">
        															<?php echo $alarm['date_ymd'];?>
        															<span class="pull-right label label-danger" style="margin-top:3px;cursor:pointer;" onclick="alarm_confirm(<?php echo $alarm['alarm_no']?>);"><?php echo lang('strDelete');?></span>
        														</small>								
        													</div>
        												</a>
        											</li>
        											<?php 
                                                        }
        											?>
        										</ul>
        									</div>
        								</div>
        							</div>

    							</li>
    							<!-- community
    							<li class="dropdown">
    							    <a href="/board/member_notice_list">
    									<img src="/img/top_nav_04.png">
								<span class="badge badge-header badge-warning">9</span>
    								</a>
    							</li> -->
    							<li class="dropdown">
    								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
    									<img src="/img/top_nav_05.png">
    								</a>
    								<div class="dropdown-menu dropdown-menu-md dropdown-menu-right with-arrow ">
                                   		<div class="cash_tip" >
                                		   	<div class="ma_b20"><img src="/img/cash_charge_full.png"></div>
                                		   	<span class="float_l ma_l20 f-size-1_2em point_dgray"><?php echo lang('strCurrentBalance')?></span>
                                		   	
                                		   	<span class="float_r ma_b20 ma_r20">
                                		   		<span class="pay_span color_p"><?php echo number_format($cash_info->mem_cash+$cash_info->mem_event_cash)?><span class="point_dgray f-size-12">원</span> 
                                		   	</span>
                                		</div>
                        		       <?php 
                                            $logged_in = $this->session->userdata('logged_in');  
                                            if($logged_in['mem_pay_later'] == "N"){
                                        ?>
                                	   	<p class="center ma_b20">
                                	   	<span class="btn btn-primary wid_85p" onclick="cash_charge_step1()";><?php echo lang('strMakeAPayment')?></span>
                                	    </p>
                                        <?php 
                                            }
                                        ?>
                            	    </div>
    							</li>
    						</ul>
    					</div>
                    </div>
        		<!--End Navbar Dropdown-->
        		</div>
        	</div>
        </header>
        <!--END NAVBAR-->
	    <div class="boxed">
        	<!--CONTENT CONTAINER-->
            <div id="content-container">
        	<!--Page content-->
                <div id="page-content">
                	<div class="row">
        			    <!--Chat Widget-->
                            <div class="wrap">

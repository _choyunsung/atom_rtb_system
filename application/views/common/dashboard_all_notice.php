<h4>공지사항</h4>
<table class="m_dash_tb" cellspacing='0'>
    <colgroup>
        <col width="13%">
        <col width="*">
        <col width="30%">
    </colgroup>
    <thead>
    <tr>
        <th>No</th>
        <th>제목</th>
        <th>등록일 </th>
    </tr>
    </thead>
    <tbody>
        <?php $no = 0;?>
        <?php foreach($notice_data as $row){?>
        <?php $no ++;?>
            <tr>
                <td><?php echo $no;?></td>
                <td class="txt-left">
                    <a href="javascript:notice_detail_view('<?php echo $row['notice_no'];?>', '<?php echo $row['notice_fl'];?>')">
                        <?php echo mb_substr($row['notice_nm'], 0, 10);?>
                        <?php if (mb_strlen($row['notice_nm'])>10){?>
                            ...
                        <?php }?>
                    </a>
                </td>
                <td><?php echo substr($row['date_ymd'], 0, 10);?></td>
            </tr>
        <?php }?>
    </tbody>
</table>
<form name="notice_detail_view" method="post">
    <input type="hidden" name="notice_no">
    <input type="hidden" name="notice_fl">
    <input type="hidden" name="mem_no" value="<?php echo $this->session->userdata('mem_no')?>">
</form>
<script>
    function notice_detail_view(notice_no, notice_fl){
        var frm = document.notice_detail_view;
        frm.action = '/board/m_notice_detail_view';
        frm.notice_no.value= notice_no;
        frm.notice_fl.value= notice_fl;
        frm.submit();
    }


</script>

    <div class="m_wrap ma_b20"">
		<div class="ma_b20 ma_t20" id="cash_summary">
        </div>
		<div class="ma_b20" id="all_cre_summary">
        </div>
        <div class="ma_b20" id="all_cre_evaluation">
        </div>
        <div class="ma_b20" id="dday_cre_group">
		</div>
        <div class="m_camp_info hei_45">
            <h4 class="float_l ma_t3 ma_l5"><?php echo lang('strCampaign')?> <?php echo lang('strSelect')?></h4>
  		    <div class="btn-group float_r ma_l20">
                <button class="btn btn-default wid_150 pa_r20 txt-right dropdown-toggle" data-toggle="dropdown">
                    <span id="cam_nm_view">
                        <?php if($cam_nm==""){?>
                        <?php echo lang('strAll');?>
                        <?php }else{?>
                        <?php echo $cam_nm;?>
                        <?php }?>
                    </span>
                    <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu ul_sel_box">
                    <li>
                        <a href="javascript:change_campaign_chart('all')">
                            <?php echo lang('strAll');?>
                        </a>
                    </li>
                    <?php if (isset($sel_campaign)){?>
                    <?php foreach($sel_campaign as $sel){?>
                        <li>
                            <a href="javascript:change_campaign_chart('<?php echo $sel['cam_no'];?>','<?php echo $sel['cam_nm'];?>')" >
                                    <?php echo $sel['cam_nm'];?>
                                </font>
                            </a>
                        </li>
                    <?php }?>
                    <?php }?>
                </ul>
            </div>
        </div>      
        <div class="over_hidden ma_b20">
            <div id="campaign_report">
            </div>
        	<div class="ma_b20" id="campaign_summary">
        	</div>
        </div>   
        <div class="ma_b20" id="all_notice">
        </div>
    <form name="dashboard" method="post">
        <input type="hidden" id="mem_no" value="<?php echo $mem_no;?>">
        <input type="hidden" id="cam_no" value="<?php echo $cam_no;?>">
        <input type="hidden" id="cam_nm" value="<?php echo $cam_nm;?>">
    </form>
<script>
function get_cash_summary(){
    $.ajax({
        url : "/dashboard/dashboard_cash_summary/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#cash_summary').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#cash_summary").html(result);
        }
    });
}

function get_dday_cre_group(){
    $.ajax({
        url : "/dashboard/dashboard_dday_cre_group/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#dday_cre_group').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#dday_cre_group").html(result);
        }
    });
}

function get_all_cre_summary(){
    $.ajax({
        url : "/dashboard/dashboard_all_cre_summary/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#all_cre_summary').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#all_cre_summary").html(result);
        }
    });
}

function get_all_cre_evaluation(){
    $.ajax({
        url : "/dashboard/dashboard_all_cre_evaluation/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#all_cre_evaluation').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#all_cre_evaluation").html(result);
        }
    });
}


function get_campaign_chart(){
    $.ajax({
        url : "/dashboard/dashboard_campaign_chart/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#campaign_report').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val(),
            cam_no : $("#cam_no").val()
            },

        success : function(result){
            $("#campaign_report").html(result);
        }
    });
}
                    
function get_campaign_chart(){
    $.ajax({
        url : "/dashboard/dashboard_campaign_chart/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#campaign_report').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val(),
            cam_no : $("#cam_no").val()
            },

        success : function(result){
            $("#campaign_report").html(result);
        }
    });
}

function get_campaign_summary(){
    $.ajax({
        url : "/dashboard/dashboard_campaign_summary/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#campaign_summary').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val(),
            cam_no : $("#cam_no").val(),
            },

        success : function(result){
            $("#campaign_summary").html(result);
        }
    });
}

function change_campaign_chart(cam_no, cam_nm){
	if(cam_no == "all"){
		$("#cam_no").val('');
		$("#cam_nm").val('<?php echo lang('strAll')?>');
		$("#cam_nm_view").html('<?php echo lang('strAll')?>');
	}else{
	    $("#cam_no").val(cam_no);
	    $("#cam_nm").val(cam_nm);
	    $("#cam_nm_view").html(cam_nm);
	}
	get_campaign_chart();
	get_campaign_summary();
}

function get_notice(){
    $.ajax({
        url : "/dashboard/dashboard_all_notice/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#all_notice').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
        },

        success : function(result){
            $("#all_notice").html(result);
        }
    });
}


$(function () {
	get_cash_summary();
	get_dday_cre_group();
	get_all_cre_summary();
	get_all_cre_evaluation();
    get_campaign_chart();
    get_campaign_summary();
    get_notice();
    
});


</script>




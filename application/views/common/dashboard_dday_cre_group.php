<?php if ($this->agent->is_mobile()){?>
    <h4 class="ma_b5">종료예정 광고그룹</h4>   	
    <table id="dday_tb" class="table table-striped table-bordered table-hover scroll m_dash_tb" cellspacing='0'>
<?php }else{?>
    <h4 class="ma_b10">종료예정 광고그룹</h4>
    <table id="dday_tb" class="table table-striped table-bordered table-hover scroll dash_tb" cellspacing='0'>
<?php }?>
    <colgroup>
        <col width="40%">
        <col width="*">
        <col width="15%">
    </colgroup>
    <thead>
        <tr>
            <th>광고그룹명</th>
            <th>종료예정일</th>
            <th> D-day </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($cre_gp_d_day as $row){?>
            <tr>
                <td class="txt-left pa_l20"><a href="/creative/creative_group_list/"><?php echo $row['cre_gp_nm']?></a></td>
                <td><?php echo substr($row['start_ymd'], 0, 10)?> ~ <?php echo substr($row['end_ymd'], 0, 10)?></td>
                <td class="<?php if ($row['d_day'] < 5){echo "color_r";}elseif ($row['d_day'] < 10){echo "color_o";}else{};?>">D-<?php echo $row['d_day'];?></td>
            </tr>
        <?php }?>
        <?php if (count($cre_gp_d_day) < 5){
                for ($i = 0; $i < 5 - count($cre_gp_d_day); $i++){
        ?>
            <tr>
                <td class="txt-left pa_l20">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="color_r">&nbsp;</td>
            </tr>
        <?php }
                }
        ?>
    </tbody>
</table>

<script>
$('#dday_tb').dataTable( {
    "dom": 't',
    ordering:  false
     } );
</script>
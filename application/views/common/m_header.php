<?php
    if($this->session->userdata('logged_in')){

    }else{
        redirect("/login");
    }
?>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8" />
<title>Mobile ATOM</title>
    <link rel="shortcut icon" href="http://www.adop.cc/wp-content/uploads/2015/01/adop_logo_16x16.png">
	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">
	<!--Bootstrap Stylesheet [ REQUIRED ]-->  
	<link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="/template/bootstrap/css/nifty.css" rel="stylesheet">
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">
	<!--Bootstrap Datepicker [ OPTIONAL ]-->
	<link href="/template/plugins/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
	<!--jQuery [ REQUIRED ]-->
	<script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>

	<!-- Highcharts -->
	<script src="/template/plugins/Highcharts-4.1.5/js/highcharts.src.js"></script>
	
	<!-- DataTables -->
	<script type="text/javascript" src="/template/plugins/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="/template/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.js"></script>
    <script type="text/javascript" src="/template/plugins/datatables/extensions/ColVis/js/dataTables.colVis.js"></script>
    <script type="text/javascript" src="/template/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
    
	<!-- DatePicker -->
	<script type="text/javascript" src="/template/plugins/daterangepicker/moment.js"></script>
    <script type="text/javascript" src="/template/plugins/daterangepicker/daterangepicker.js"></script>
    <link href="/template/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0, user-scalable=yes" />
</head>
<body>
<div class="mobile_con mainnav-lg navbar-out">
    <div class="mobile_nav">
		<div class="m_menu float_l color_w" id="lmenu_trigger">
			<i class="fa fa-bars fa-2"></i>
		</div>
		<div class="m_logo float_l"><a href="/dashboard"><img src="/img/logo_atom2.png"></a></div>
		<div class="m_logout rloat_r">
    		<a href="/login/logout_process">
    			<span>
                     <font class="color_w">  
                        <?php echo lang('strLogout')?>                                            
                     </font>
                </span>
            </a>
		</div>
	</div>
	<div id="m_con">

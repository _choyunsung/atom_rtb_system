<?php if ($this->agent->is_mobile()){?>
    <h4 class="ma_b5"><?php echo lang('strAdState')?></h4>       	
    <table id="cre_summary_tb" class="table table-striped table-bordered table-hover scroll m_dash_tb" cellspacing='0'>
<?php }else{?>
    <h4 class="ma_b10"><?php echo lang('strAdState')?></h4>
    <table id="cre_summary_tb" class="table table-striped table-bordered table-hover scroll dash_tb" cellspacing='0'>
<?php }?>
	<colgroup>
		<col width="25%">
		<col width="25%">
		<col width="25%">
		<col width="25%">
	</colgroup>
    <thead>
        <tr>
            <th><?php echo lang('strStatus');?></th>
            <th><?php echo lang('strCampaign')?></th>
            <th><?php echo lang('strAdGroup')?></th>
            <th><?php echo lang('strCr')?></th>
        </tr>
    </thead>
    <?php if ($this->agent->is_mobile()){?>
        <tbody>
            <tr>
                <td class="color_b sub_th" ><?php echo lang('strAll')?></td>
                <td><?php echo $creative_all_summary['cam_cnt'];?></td>
                <td><?php echo $creative_all_summary['cre_gp_cnt'];?></td>
                <td><?php echo $creative_all_summary['cre_cnt'];?></td>
            </tr>
            <?php foreach ($creative_summary as $row){?>
                <tr>
                    
                    <td class="<?php if ($row['code_key'] == 1){ echo "color_g";}elseif ($row['code_key'] == "2"){ echo "color_o";}elseif ($row['code_key'] == "3"){echo "color_r";}else{echo "color_b2";}?> sub_th" >
                        <?php echo lang($row['code_desc'])?>
                    </td>
                    <td><?php echo $row['cam_cnt']?></td>
                    <td><?php echo $row['cre_gp_cnt']?></td>
                    <td><?php echo $row['cre_cnt']?></td>
                </tr>
            <?php }?>
        </tbody>
    <?php }else{?>
        <tbody>
            <tr>
                <td class="color_b sub_th" ><?php echo lang('strAll')?></td>
                <td><a href="/campaign/campaign_list"><?php echo $creative_all_summary['cam_cnt'];?></a></td>
                <td><a href="/creative/creative_group_list"><?php echo $creative_all_summary['cre_gp_cnt'];?></a></td>
                <td><a href="/creative/creative_list"><?php echo $creative_all_summary['cre_cnt'];?></a></td>
            </tr>
            <?php foreach ($creative_summary as $row){?>
                <tr>
                    
                    <td class="<?php if ($row['code_key'] == 1){ echo "color_g";}elseif ($row['code_key'] == "2"){ echo "color_o";}elseif ($row['code_key'] == "3"){echo "color_r";}else{echo "color_b2";}?> sub_th" >
                        <?php echo lang($row['code_desc'])?>
                    </td>
                    <td><a href="/campaign/campaign_list"><?php echo $row['cam_cnt']?></a></td>
                    <td><a href="/creative/creative_group_list"><?php echo $row['cre_gp_cnt']?></a></td>
                    <td><a href="/creative/creative_list"><?php echo $row['cre_cnt']?></a></td>
                </tr>
            <?php }?>
        </tbody>
    <?php }?>
</table>
<script>
$('#cre_summary_tb').dataTable( {
    "dom": 't',
    ordering:  false
     } );
</script>

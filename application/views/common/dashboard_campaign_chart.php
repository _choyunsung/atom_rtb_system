<div id="dashboard_campaign_chart" style="min-width: 100%; min-height: 300px; margin: auto"></div>
<script type="text/javascript">

$(function () {
    $('#dashboard_campaign_chart').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [
                         <?php if(isset($chart_data)){?>
                             <?php foreach ($chart_data as $row){?>
                                <?php echo "'".$row['date_ymd']."',"?>
                             <?php }?>
                         <?php }?>
                         ],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
        	min: 0,
        	allowDecimals: false,
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '<?php echo lang("strClick");?>',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
        	min: 0,
        	allowDecimals: false,
            title: {
                text: '<?php echo lang("strImpression");?>',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: '<?php echo lang("strImpression");?>',
            type: 'column',
            yAxis: 1,
            data: [
                    <?php if(isset($chart_data)){?>
                        <?php foreach ($chart_data as $row){?>
                           <?php echo $row['imp'].","?>
                        <?php }?>
                    <?php }?>
                   ],
            tooltip: {
                valueSuffix: ' '
            }

        }, {
            name: '<?php echo lang("strClick");?>',
            type: 'spline',
            data: [
                    <?php if(isset($chart_data)){?>
                        <?php foreach ($chart_data as $row){?>
                           <?php echo $row['clk'].","?>
                        <?php }?>
                    <?php }?>
                   ],
            tooltip: {
                valueSuffix: ' '
            }
        }]
    });
});
</script>

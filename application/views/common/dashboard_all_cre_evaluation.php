<?php 
$img_apply = 0;
$img_wait = 0;
$img_reject = 0;
$fla_apply = 0;
$fla_wait = 0;
$fla_reject = 0;
$txt_apply = 0;
$txt_wait = 0;
$txt_reject = 0;
foreach ($all_cre_evaluation as $row){
    if ($row['code_key'] == "1"){
        if ($row['cre_evaluation'] == "1"){
            $img_wait ++;
        }elseif ($row['cre_evaluation'] == "2"){
            $img_apply ++;
        }else{
            $img_reject ++;
        }
    }elseif ($row['code_key'] == "2"){
        if ($row['cre_evaluation'] == "1"){
            $fla_wait ++;
        }elseif ($row['cre_evaluation'] == "2"){
            $fla_apply ++;
        }else{
            $fla_reject ++;
        }
    }else{
        if ($row['cre_evaluation'] == "1"){
            $txt_wait ++;
        }elseif ($row['cre_evaluation'] == "2"){
            $txt_apply ++;
        }else{
            $txt_reject ++;
        }
    }
}
?>
<?php if ($this->agent->is_mobile()){?>
    <h4 class="ma_b5"><?php echo lang('strExamine');?></h4>
    <table class="m_dash_tb" cellspacing="0">
<?php }else{?>
    <h4 class="ma_b10"><?php echo lang('strExamine');?></h4>
    <table class="dash_tb" cellspacing='0'>
<?php }?>
    <colgroup>
        <col width="*">
        <col width="28%">
        <col width="28%">
        <col width="28%">
    </colgroup>
    <thead>
        <tr>
            <th><?php echo lang('strType');?></th>
            <th><?php echo lang('strApproveWait');?></th>
            <th><?php echo lang('strApproveComplete');?></th>
            <th><?php echo lang('strDisagree');?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo lang('strImage');?></td>
            <td><?php echo $img_wait;?></td>
            <td><?php echo $img_apply;?></td>
            <td><?php echo $img_reject;?></td>
        </tr>
        <!-- 
        <tr class="even">
            <td><?php echo lang('strFlash');?></td>
            <td><?php echo $fla_wait;?></td>
            <td><?php echo $fla_apply;?></td>
            <td><?php echo $fla_reject;?></td>
        </tr>
            <tr>
            <td><?php echo lang('strText');?></td>
            <td><?php echo $txt_wait;?></td>
            <td><?php echo $txt_apply;?></td>
            <td><?php echo $txt_reject;?></td>
        </tr>
         -->
    </tbody>
</table>
<div class="panel min_height bd_l1" style="overflow:hidden;">
    <div class="history_box">
    </div>
    <div class="panel-body">
        <!-- 캐쉬현황 영역 -->
        <div class="wid_96p m_center" id="cash_summary">
        </div>
        <!-- 종료예정 광고그룹 영역 -->
        <div class=" float_l wid_47p ma_l23 ma_b15" id="dday_cre_group">
        </div>
        <!-- 광고현황 영역 -->
        <div class="float_r wid_47p ma_r23 ma_b15" id="all_cre_summary">
        </div>
        <!-- 심사현황 영역 -->
        <div class="clear wid_96p m_center" id="all_cre_evaluation">
        </div>
        <div class="camp_info pa_l10 hei_45 wid_96p">
            <h4 class="float_l wid_10p ma_t3 ma_r20"><?php echo lang('strCampaign')?> <?php echo lang('strSelect')?></h4>
        	<div class="btn-group float_l">
                <button class="btn btn-default wid_200 pa_r20 txt-right dropdown-toggle" data-toggle="dropdown">
                    <span id="cam_nm_view">
                        <?php if($cam_nm==""){?>
                        <?php echo lang('strAll');?>
                        <?php }else{?>
                        <?php echo $cam_nm;?>
                        <?php }?>
                    </span>
                    <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu ul_sel_box">
                    <li>
                        <a href="javascript:change_campaign_chart('all')">
                            <?php echo lang('strAll');?>
                        </a>
                    </li>
                    <?php if (isset($sel_campaign)){?>
                    <?php foreach($sel_campaign as $sel){?>
                        <li>
                            <a href="javascript:change_campaign_chart('<?php echo $sel['cam_no'];?>','<?php echo $sel['cam_nm'];?>')" >
                                    <?php echo $sel['cam_nm'];?>
                                </font>
                            </a>
                        </li>
                    <?php }?>
                    <?php }?>
                </ul>
            </div>
            <span class="float_r font_bold ma_t5"> 
       	   	    <!-- 광고운영 보고서 보기
       	   	    <a href="/report/report_operation"><i class="fa fa-external-link fa-1_5x ma_t8"></i></a>
       	   	     -->
       	   	     <i class="fa fa-reorder"></i>  &nbsp;<a href="/report/report_operation"><?php echo lang('strOperationReport')?> <?php echo lang('strReport')?> <?php echo lang('strView')?></a>
            </span>
        </div>    
        <div class="over_hidden bd_eb wid_96p ma_b20 m_center pa_b10 pa_t10">
            <div class="camp_l_gp" id="campaign_report">
            </div>
            <div class="camp_r_tb" id="campaign_summary">
            </div>
        </div>
        <!-- 
        <div class=" float_l wid_47p ma_l20">
            <h4 class="dash_tit float_l wid_30p">공지사항</h4>
            <span class="float_r f-size-9 ma_t5">more &nbsp; <i class="fa ma_b1 fa-angle-right"></i></span>
            <table class="dash_tb"cellspacing='0'> 
                <colgroup>
                    <col width="13%">
                    <col width="*">
                    <col width="30%">
                </colgroup>
                <thead>
                    <tr>
                        <th>번호</th>
                        <th>제목</th>
                        <th>등록일 </th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td>1</td>
                        <td class="txt-left"><a href="#">현대자동차 팔아염</a></td>
                        <td>2015/06/18</td>
                    </tr>
                    <tr class="even">
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>2015/06/18</td>
                    </tr>
                        <tr>
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>2015/06/18</td>
                    </tr>
  					 <tr class="even">
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>2015/06/18</td>
                    </tr>
                        <tr>
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>2015/06/18</td>
                    </tr>                    
                </tbody>
            </table>
        </div>
        <div class="float_r wid_47p ma_r20">
            <h4 class="dash_tit float_l wid_30p">문의사항</h4>
            <span class="float_r f-size-9 ma_t5">more &nbsp; <i class="fa ma_b1 fa-angle-right"></i></span>
            <table class="dash_tb float_l"cellspacing='0'> 
                <colgroup>
                    <col width="13%">
                    <col width="*">
                    <col width="15%">
                </colgroup>
                <thead>
                    <tr>
                        <th>번호</th>
                        <th>제목</th>
                        <th>답변</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td class="txt-left"><a href="#">현대자동차 팔아염</a></td>
                        <td>답변완료</td>
                    </tr>
                    <tr class="even">
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>확인</td>
                    </tr>
                        <tr>
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>미확인</td>
                    </tr>
   					 <tr class="even">
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>미확인</td>
                    </tr>
                        <tr>
                        <td>1</td>
                        <td class="txt-left">현대자동차 팔아염</td>
                        <td>확인</td>
                    </tr>      
                </tbody>
            </table>
            <br /><br /><br /><br /><br />
        </div>
        -->
    </div>
</div>
<form name="dashboard" method="post">
    <input type="hidden" id="mem_no" value="<?php echo $mem_no;?>">
    <input type="hidden" id="cam_no" value="<?php echo $cam_no;?>">
    <input type="hidden" id="cam_nm" value="<?php echo $cam_nm;?>">
</form>
<script>
function get_cash_summary(){
    $.ajax({
        url : "/dashboard/dashboard_cash_summary/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#cash_summary').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#cash_summary").html(result);
        }
    });
}

function get_dday_cre_group(){
    $.ajax({
        url : "/dashboard/dashboard_dday_cre_group/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#dday_cre_group').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#dday_cre_group").html(result);
        }
    });
}

function get_all_cre_summary(){
    $.ajax({
        url : "/dashboard/dashboard_all_cre_summary/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#all_cre_summary').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#all_cre_summary").html(result);
        }
    });
}

function get_all_cre_evaluation(){
    $.ajax({
        url : "/dashboard/dashboard_all_cre_evaluation/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#all_cre_evaluation').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val()
            },

        success : function(result){
            $("#all_cre_evaluation").html(result);
        }
    });
}


function get_campaign_chart(){
    $.ajax({
        url : "/dashboard/dashboard_campaign_chart/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#campaign_report').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val(),
            cam_no : $("#cam_no").val()
            },

        success : function(result){
            $("#campaign_report").html(result);
        }
    });
}

function get_campaign_summary(){
    $.ajax({
        url : "/dashboard/dashboard_campaign_summary/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#campaign_summary').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : $("#mem_no").val(),
            cam_no : $("#cam_no").val(),
            },

        success : function(result){
            $("#campaign_summary").html(result);
        }
    });
}

function change_campaign_chart(cam_no, cam_nm){
	if(cam_no == "all"){
		$("#cam_no").val('');
		$("#cam_nm").val('<?php echo lang('strAll')?>');
		$("#cam_nm_view").html('<?php echo lang('strAll')?>');
	}else{
	    $("#cam_no").val(cam_no);
	    $("#cam_nm").val(cam_nm);
	    $("#cam_nm_view").html(cam_nm);
	}
	get_campaign_chart();
	get_campaign_summary();
}

$(function () {
	get_cash_summary();
	get_dday_cre_group();
	get_all_cre_summary();
	get_all_cre_evaluation();
    get_campaign_chart();
    get_campaign_summary();
    
});

</script>
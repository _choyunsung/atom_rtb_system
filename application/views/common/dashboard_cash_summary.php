<?php if ($this->agent->is_mobile()){?>
    <h4 class="ma_b5"><?php echo lang('strCashState');?></h4> 
    <table class="m_dash_tb wid_100p" cellspacing="0">
<?php }else{?> 
    <div class="over_hidden">
        <h4 class="float_l wid_30p ma_t5 ma_b10"><?php echo lang('strCashState');?></h4>
        
        <?php 
            $logged_in = $this->session->userdata('logged_in');  
            if($logged_in['mem_pay_later'] == "N"){
        ?>
        <span class="btn btn-primary float_r" onclick="cash_charge_step1()";>
        <?php echo lang('strMakeAPayment')?></span>
        <?php 
            }
        ?>
    </div>
    <table class="dash_tb wid_100p" cellspacing='0'>
<?php }?>
    <colgroup>
        <col width="25%">
        <col width="25%">
        <col width="25%">
        <col width="25%">
    </colgroup>
    <thead>
        <tr>
            <th><?php echo lang('strCashBalance');?>(<?php echo lang('strMoneySymbol');?>)</th>
            <th><?php echo lang('strAssumeRunOut');?></th>
            <th><?php echo lang('strToDayRunOut');?>(<?php echo lang('strMoneySymbol');?>)</th>
            <th><?php echo lang('strYesterDayRunOut');?>(<?php echo lang('strMoneySymbol');?>)</th>
        </tr>
    </thead>
    <?php if ($this->agent->is_mobile()){?>
        <tbody>
            <tr>
                <td class="color_p f-size-1_3em">
                    <?php echo number_format($cash_summary['mem_cash'])?>
                </td>
                <td class="point_dgray f-size-1_3em">
                    <?php
                        if ($cash_summary['yesterday_cash'] > 0){
                            $day = round($cash_summary['mem_cash'] / $cash_summary['yesterday_cash'], 0);
                            $running_out_day = date("Y-m-d", strtotime("+".$day." day" ));
                        }else{
                            $running_out_day = "-";
                        }
                        echo $running_out_day;
                    ?>
                </td>
                <td class="color_b f-size-1_3em">
                    <?php echo number_format($cash_summary['today_cash'])?>
                </td>
                <td class="color_o f-size-1_3em" >
                    <?php echo number_format($cash_summary['yesterday_cash'])?>
                </td>
            </tr>
        </tbody>
    <?php }else{?>
        <tbody>
            <tr>
                <td class=" f-size-16">
                    <a class="color_p" href="/cash/cash_charge_list">
                        <?php echo number_format($cash_summary['mem_cash'] + $cash_summary['mem_event_cash'])?>
                    </a>
                </td>
                <td class="color_o  f-size-16">
                    <a class="color_o" href="/cash/cash_charge_list">
                        <?php
                            if ($cash_summary['yesterday_cash'] > 0){
                                $day = round($cash_summary['mem_cash'] / $cash_summary['yesterday_cash'], 0);
                                $running_out_day = date("Y-m-d", strtotime("+".$day." day" ));
                            }else{
                                $running_out_day = "-";
                            }
                            echo $running_out_day;
                        ?>
                    </a>
                </td>
                <td class="f-size-16">
                    <a class="color_b" href="/cash/cash_history_list">
                        <?php echo number_format($cash_summary['today_cash'])?>
                    </a>
                </td>
                <td class="point_dgray font_bold f-size-16" >
                    <a class="point_dgray" href="/cash/cash_history_list">
                        <?php echo number_format($cash_summary['yesterday_cash'])?>
                    </a>
                </td>
            </tr>
        </tbody>
    <?php }?>
</table>
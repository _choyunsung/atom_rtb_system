<div style="width:650px; height:340px; padding:20px; border:5px solid #4859a1;overflow:hidden;">
    <div style="height:50px; border-bottom:1px solid #ebebeb;">
        <h3 style="width:20%; float:left"><span style="color:#83bf00">안내 메세지</span> 발송</h3>
    </div>
    <div style="clear:both; width:100%; overflow:hidden;"> 
        <div style="float:left; width:159px; height:193px; margin-left:20px; margin-top:35px; background:url('http://atom.adop.cc/img/email_bg.png');">
        </div>
        <div style="float:left; width:60%; margin-left:40px; margin-top:20px;">
            <p style="font-size:1.5em; clear:both; margin-bottom:15px; ">
                <span style="color:#4859a1">ADOP</span>
                <span style="font-size:0.9em">에서</span> 
                <span style="font-weight:bold">알려드립니다.</span>
            </p>
            <p style="font-size:0.9em; margin-bottom:15px;">
                안녕하세요. <br />
                <?php echo lang($content);?>
            </p>
            <p style="font-size:0.7em;margin-top:10px; width:100%;">
                다른 문의사항이 있으시면 담당부서 
                <span style="color:#509fd0">atom@adop.co.kr</span> 로 문의해 주시기 바랍니다.
            </p>
        </div>
    </div>
</div>

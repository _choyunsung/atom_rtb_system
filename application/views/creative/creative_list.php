<?php 
    $mem_no = $this->session->userdata('mem_no');
    $run_sum_imp = 0;
    $run_sum_clk = 0;
    $run_sum_ctr = 0;
    $ready_sum_imp = 0;
    $ready_sum_clk = 0;
    $ready_sum_ctr = 0;
    $pause_sum_imp = 0;
    $pause_sum_clk = 0;
    $pause_sum_ctr = 0;
    $done_sum_imp = 0;
    $done_sum_clk = 0;
    $done_sum_ctr = 0;
    if (isset($creative_list)){
        foreach ($creative_list as $row){
            if ($row['cre_status'] == "1"){
                $run_sum_imp += $row['imp'];
                $run_sum_clk += $row['clk'];
            }
    
            if ($row['cre_status'] == "2"){
                $ready_sum_imp += $row['imp'];
                $ready_sum_clk += $row['clk'];
            }
    
            if ($row['cre_status'] == "3" || $row['cre_status'] == "5" || $row['cre_status'] == "6"){
                $pause_sum_imp += $row['imp'];
                $pause_sum_clk += $row['clk'];
            }
    
            if ($row['cre_status'] == "4"){
                $done_sum_imp += $row['imp'];
                $done_sum_clk += $row['clk'];
            }
        }
    }
    if ($run_sum_clk != "0"){
        $run_sum_ctr = $run_sum_clk / $run_sum_imp * 100;
    }
    if ($ready_sum_clk != "0"){
        $ready_sum_ctr = $ready_sum_clk / $ready_sum_imp * 100;
    }
    if ($pause_sum_clk != "0"){
        $pause_sum_ctr = $pause_sum_clk / $pause_sum_imp * 100;
    }
    if ($done_sum_clk != "0"){
        $done_sum_ctr = $done_sum_clk / $done_sum_imp * 100;
    }
?>
        
<script type="text/javascript">
$(document).ready(function() {
    if(($('#all').val()=="Y")||($('#ready').val()=="Y"&&$('#run').val()=="Y"&&$('#pause').val()=="Y"&&$('#done').val()=="Y")){
        $('#all').attr('value','Y');
        $('#run').attr('value','Y');
        $('#ready').attr('value','Y');
        $('#pause').attr('value','Y');
        $('#done').attr('value','Y');
        $('#sel_all').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
        $('#sel_run').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
        $('#sel_ready').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
        $('#sel_pause').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
        $('#sel_done').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
        $('#all_img').attr('src','/img/radio_on.png');
        $('#run_img').attr('src','/img/radio_on_g.png');
        $('#ready_img').attr('src','/img/radio_on_o.png');
        $('#pause_img').attr('src','/img/radio_on_r.png');
        $('#done_img').attr('src','/img/radio_on_b2.png');
        $('#run_sum_tr').show();
        $('#ready_sum_tr').show();
        $('#pause_sum_tr').show();
        $('#done_sum_tr').show();
    }else{
    	$('#sel_all').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
        if($('#run').val()=="Y"){
            $('#run').attr('value','Y');
            $('#sel_run').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
            $('#run_img').attr('src','/img/radio_on_g.png');
            $('#run_sum_tr').show();
        }else{
            $('#run').attr('value','N');
            $('#sel_run').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#run_img').attr('src','/img/radio_off.png');
            $('#run_sum_tr').hide();
        }
        if($('#ready').val()=="Y"){
            $('#ready').attr('value','Y');
            $('#sel_ready').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
            $('#ready_img').attr('src','/img/radio_on_o.png');
            $('#ready_sum_tr').show();
        }else{
            $('#ready').attr('value','N');
            $('#sel_ready').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#ready_img').attr('src','/img/radio_off.png');
            $('#ready_sum_tr').hide();
        }
        if($('#pause').val()=="Y"){
            $('#pause').attr('value','Y');
            $('#sel_pause').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
            $('#pause_img').attr('src','/img/radio_on_r.png');
            $('#pause_sum_tr').show();
        }else{
            $('#pause').attr('value','N');
            $('#sel_pause').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#pause_img').attr('src','/img/radio_off.png');
            $('#pause_sum_tr').hide();
        }
        if($('#done').val()=="Y"){
            $('#done').attr('value','Y');
            $('#sel_done').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
            $('#done_img').attr('src','/img/radio_on_b2.png');
            $('#done_sum_tr').show();
        }else{
            $('#done').attr('value','N');
            $('#sel_done').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#done_img').attr('src','/img/radio_off.png');
            $('#done_sum_tr').hide();
        }
    }
});

    $(document).ready(function() {
        if (($('#all').val()=="Y")||($('#ready').val()=="Y"&&$('#run').val()=="Y"&&$('#pause').val()=="Y"&&$('#done').val()=="Y")){
            $('#all').attr('value','Y');
            $('#run').attr('value','Y');
            $('#ready').attr('value','Y');
            $('#pause').attr('value','Y');
            $('#done').attr('value','Y');
            $('#all_img').attr('src','/img/radio_on.png');
            $('#run_img').attr('src','/img/radio_on_g.png');
            $('#ready_img').attr('src','/img/radio_on_o.png');
            $('#pause_img').attr('src','/img/radio_on_r.png');
            $('#done_img').attr('src','/img/radio_on_b2.png');
            $('#run_sum_tr').show();
            $('#ready_sum_tr').show();
            $('#pause_sum_tr').show();
            $('#done_sum_tr').show();
        }else{
            if ($('#run').val()=="Y"){
                $('#run').attr('value','Y');
                $('#run_img').attr('src','/img/radio_on_g.png');
                $('#run_sum_tr').show();
            }else{
                $('#run').attr('value','N');
                $('#run_img').attr('src','/img/radio_off.png');
                $('#run_sum_tr').hide();
            }
            if ($('#ready').val()=="Y"){
                $('#ready').attr('value','Y');
                $('#ready_img').attr('src','/img/radio_on_o.png');
                $('#ready_sum_tr').show();
            }else{
                $('#ready').attr('value','N');
                $('#ready_img').attr('src','/img/radio_off.png');
                $('#ready_sum_tr').hide();
            }
            if ($('#pause').val()=="Y"){
                $('#pause').attr('value','Y');
                $('#pause_img').attr('src','/img/radio_on_r.png');
                $('#pause_sum_tr').show();
            }else{
                $('#pause').attr('value','N');
                $('#pause_img').attr('src','/img/radio_off.png');
                $('#pause_sum_tr').hide();
            }
            if ($('#done').val()=="Y"){
                $('#done').attr('value','Y');
                $('#done_img').attr('src','/img/radio_on_b2.png');
                $('#done_sum_tr').show();
            }else{
                $('#done').attr('value','N');
                $('#done_img').attr('src','/img/radio_off.png');
                $('#done_sum_tr').hide();
            }
        }
    });

    function show_btn(cre_gp_no){
    	$("#cre_gp_btn_group_"+cre_gp_no).show();
    }
    
    function hide_btn(cre_gp_no){
    	$("#cre_gp_btn_group_"+cre_gp_no).hide();
    }
    
    function select_status(kind){
        if (kind=="all"){
            if ($('#all').val()=="Y"){
                $('#all').attr('value','N');
                $('#run').attr('value','Y');
                $('#ready').attr('value','N');
                $('#pause').attr('value','N');
                $('#done').attr('value','N');
                $('#all_img').attr('src','/img/radio_off.png');
                $('#run_img').attr('src','/img/radio_on_g.png');
                $('#ready_img').attr('src','/img/radio_off.png');
                $('#pause_img').attr('src','/img/radio_off.png');
                $('#done_img').attr('src','/img/radio_off.png');
            }else{
                $('#all').attr('value','Y');
                $('#run').attr('value','Y');
                $('#ready').attr('value','Y');
                $('#pause').attr('value','Y');
                $('#done').attr('value','Y');
                $('#all_img').attr('src','/img/radio_on.png');
                $('#run_img').attr('src','/img/radio_on_g.png');
                $('#ready_img').attr('src','/img/radio_on_o.png');
                $('#pause_img').attr('src','/img/radio_on_r.png');
                $('#done_img').attr('src','/img/radio_on_b2.png');

            }
        }
        if (kind=="run"){
            if ($('#run').val()=="Y"){
                $('#run').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#run_img').attr('src','/img/radio_off.png');
            }else{
                $('#run').attr('value','Y');
                $('#run_img').attr('src','/img/radio_on_g.png');
            }
        }
        if (kind=="ready"){
            if ($('#ready').val()=="Y"){
                $('#ready').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#ready_img').attr('src','/img/radio_off.png');
            }else{
                $('#ready').attr('value','Y');
                $('#ready_img').attr('src','/img/radio_on_o.png');
            }
        }
        if (kind=="pause"){
            if ($('#pause').val()=="Y"){
                $('#pause').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#pause_img').attr('src','/img/radio_off.png');
            }else{
                $('#pause').attr('value','Y');
                $('#pause_img').attr('src','/img/radio_on_b2.png');
            }
        }
        if (kind=="done"){
            if ($('#done').val()=="Y"){
                $('#done').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#done_img').attr('src','/img/radio_off.png');
            }else{
                $('#done').attr('value','Y');
                $('#done_img').attr('src','/img/radio_on_b2.png');
            }
        }
        search();
    }
	function search(){
		var frm=document.creative_list;
		frm.action="/creative/creative_list";
		frm.fromto_date.value=$("#daterange").val();
		frm.submit();
	}
</script>
<!-- 광고정보modal -->
<div class="modal" id="modal_creative" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content" id="modal_content_creative">
            <div class="modal-header">
                <h4 class="h4_blit"><?php echo lang('strFullSizeView');?></h4>
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <div class="ma_b10 pa_l10">
                        <span class="sub_tit"><?php echo lang('strAdInfo')?></span><br />
                    </div>
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray">
                        <colgroup>
                            <col width="30%">
                            <col width="70%">
                        </colgroup>
                        <tr>
                            <th class="point_blue"><?php echo lang('strSize')?></th>
                            <td>
                                <span id="modal_size"></span>
                                
                            </td>
                        </tr>
                        
                        <tr class="image-adver-tr" >
                            <th class="point_blue">URL</th>
                            
                            <td><span id="modal_url"></span></td>
                        </tr>
                        <tr class="image-adver-tr" >
                            <th class="point_blue"><?php echo lang('strCaption');?></th>
                            <td><span id="modal_cont"></span></td>
                        </tr>	
                        <tr class="image-adver-tr" >
                            <th class="point_blue"><?php echo lang('strImage');?></th>
                            <td><img src = "" id="modal_img" style="max-width:550px;"></td>
                        </tr>

                        <tr class="auction-form" >
                            <th class="point_blue"><?php echo lang('strAuctionSample');?></th>
                            <td><img src = "" style="max-width:550px;" id="auction_img" ></td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span data-dismiss="modal" onclick="$('#modal_creative').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<!-- // modal 상세정보 -->

<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAdMgr')?></a></li>
			<li class="active"><?php echo lang('strAd')?></li>
		</ol>
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strAd')?></h3>
	</div>
	<div class="panel-body">
     <div class="clear">
	<!-- seonu -->
    <div class="camp_rept">
        <div class="camp_rept_his">
            <span class="f-color-g font_bold"><i class="fa fa-reorder"></i> &nbsp;<a href="/report/report_operation"><?php echo lang('strOperationReport')?> <?php echo lang('strReport')?> <?php echo lang('strView')?></a></span>
        </div>
        <div class="camp2 cursor2" >
			<div class="camp_div2" id="sel_all" onclick="select_status('all');">
				<p class="camp_tit"><?php echo lang('strAll')?></p> 
				<span class="camp_con"><?php echo $creative_list_count['all_cnt'];?><?php echo lang('strEa');?></span>
			</div>
			<div class="camp_div2" id="sel_run" onclick="select_status('run');">
				<p class="camp_tit"><?php echo lang('strRun')?></p>
				<span class="camp_con"><?php echo $creative_list_count['run_cnt'];?><?php echo lang('strEa');?></span>
			</div>
			<div class="camp_div2" id="sel_ready" onclick="select_status('ready');">
				<p class="camp_tit"><?php echo lang('strReady')?></p>
				<span class="camp_con"><?php echo $creative_list_count['ready_cnt'];?><?php echo lang('strEa');?></span>
			</div>		    				
			<div class="camp_div2" id="sel_pause" onclick="select_status('pause');">
				<p class="camp_tit"><?php echo lang('strPause')?></p>
				<span class="camp_con"><?php echo $creative_list_count['pause_cnt'];?><?php echo lang('strEa');?></span>
			</div>		    				
			<div class="camp_div2" id="sel_done" onclick="select_status('done');">
				<p class="camp_tit"><?php echo lang('strDone')?></p>
				<span class="camp_con"><?php echo $creative_list_count['done_cnt'];?><?php echo lang('strEa');?></span>
			</div>		    					
		</div>
		
		<!-- //seonu -->
			<div class="camp_info hei_45">
				<div class="camp_info_box">
    				<div id="demo-dp-component" class="camp_info_r2">
        				<input type="text" name="daterange" id="daterange" class="range float_l ma_r10 wid_70p" value="<?php echo $fromto_date?>" /> 
        				<span class="iput-group-addon  float_l ma_t8">
        					<i class="fa fa-calendar fa-lg" onclick="$('#daterange').focus();"></i>
        				</span>
        				<a href="javascript:search();" class="btn btn-primary float_l ma_l10"><?php echo lang('strSearch')?></a>
        			</div>		
		      </div>
		</div>
    </div>
	   <div class="clear bd_eb" id="report">
	   
       </div> 
          <div class="float_l wid_100p">
	  	  <div class="float_l pa_t20 ma_b3" id="all_btn_gp">
            <a href="javascript:creative_add('<?php echo $cre_gp_no?>')" class="btn btn-primary float_l"><?php echo lang('strNewAd')?></a>
            <div class="btn-group float_l ma_l5">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="cre_status_sel">
                    &nbsp;<?php echo lang('strModify')?>  &nbsp;
                    <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu ul_sel_box">
                    <?php 
                        foreach($sel_status as $sel){
                            if($sel['code_key'] != 5 && $sel['code_key'] != 6){
                    ?>
                        <li>
                            <a href="#" onclick="creative_status_change('<?php echo $sel['code_key'];?>','all')">
                                <font class="bold" color="<?php if ($sel['code_key']=="1"){echo "#69ae30";}elseif ($sel['code_key']=="2"){echo "#fe9a00";}elseif ($sel['code_key']=="3"){echo "#c85757";}else{echo "#5177bc";}?>">
                                    <?php echo lang($sel['code_desc']);?>
                                </font>
                            </a>
                        </li>
                    <?php 
                            }
                        }
                    ?>
                    <li>
                        <a href="#" onclick="creative_status_change('del','all')">
                            <?php echo lang('strDelete');?>
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="creative_status_change('copy','all')">
                            <?php echo lang('strCopy');?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
       	<span class="btn btn-primary ma_t20 float_r" onClick ="excel_down();"><?php echo lang('strDownload')?></span>
     	</div>
        <div class="pa_b20 clear">
        <form name="creative_list" method="post">
            <input type="hidden" name="all" id="all" value="<?php echo $all?>">
		    <input type="hidden" name="run" id="run" value="<?php echo $run?>">
		    <input type="hidden" name="ready" id="ready" value="<?php echo $ready?>">
		    <input type="hidden" name="pause" id="pause" value="<?php echo $pause?>">
		    <input type="hidden" name="done" id="done" value="<?php echo $done?>">
            <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
            <input type="hidden" name="cre_gp_no" value="<?php echo $cre_gp_no;?>">
            <input type="hidden" name="cre_no" >
            <input type="hidden" name="status_key" >
            <input type="hidden" name="fromto_date" id="fromto_date" value="<?php echo $fromto_date;?>" >
            <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="4%">
                    <col width="11%">
                    <col width="11%">
                    <col width="12%">
                    <col width="9%">
                    <col width="10%">
                    <col width="8%">
                    <col width="8%">
                    <col width="8%">
                    <col width="8%">
                    <col width="8%">
                </colgroup>
                <thead>
                    <tr>
                        <th class="center wid_20"><input type="checkbox" id="allCheck" onclick="all_check()"></th>
                        <th><?php echo lang('strAd')?></th>
                        <th><?php echo lang('strAdGroup')?></th>
                        <th><?php echo lang('strCampaign')?></th>
                        <th><?php echo lang('strAdvertiser')?></th>
                        <th><?php echo lang('strStatus')?></th>
                        <th><?php echo lang('strScreening')?></th>
                        <th><?php echo lang('strType')?></th>
                        <th><?php echo lang('strImpressions')?></th>
                        <th><?php echo lang('strClicks')?></th>
                        <th>CTR</th>
                   </tr>
               </thead>
               <tbody>
                   <?php if (isset($creative_list)){ ?>
                        <?php
                            foreach ($creative_list as $row){
                                $cre_no = $row['cre_no'];
                                $cre_gp_no = $row['cre_gp_no'];
                                
                        ?>
                            <tr>
                                <td class="center wid_20"><input type="checkbox" name="sel_creative[]" id="sel_creative" value="<?php echo $row['cre_no']?>" /></td>
                                <td onmouseover="show_btn('<?php echo $row['cre_no']?>');" onmouseout="hide_btn('<?php echo $row['cre_no']?>');">
                                    <a href="javascript:creative_detail_view('<?php echo $cre_no?>', '<?php echo $cre_gp_no?>')"><?php echo $row['cre_nm']?></a>
                                    <div class="btn-group float_r ma_r10p cursor" id="cre_btn_group_<?php echo $row['cre_no']?>" style="display:none;">
                                        <a class="dropdown-caret fa fa-image cursor add-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="상세 보기" href="javascript:modal_view('<?php echo $row['cre_width']?>x<?php echo $row['cre_height']?>','<?php echo $row['cre_link']?>','<?php echo $row['cre_cont']?>','<?php echo $row['cre_img_link']?>','<?=$row['cre_type']?>','<?=$row['cre_auction_type']?>')"></a>
                                        <a class="dropdown-caret fa fa-bar-chart cursor add-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="리포트 보기" href="/report/report_operation" ></a>
                                        <a class="dropdown-caret fa fa-gear cursor add-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="광고 수정" href="javascript:creative_modify('<?php echo $cre_no?>')"></a>
                                    </div>
                                </td>
                                <td>
                                    <a href="/creative/creative_group_list?cre_gp_no=<?php echo $row['cre_gp_no']?>" ><?php echo $row['cre_gp_nm']?></a> 
                                </td>
                                <td>
                                    <a href="/campaign/campaign_list?cam_no=<?php echo $row['cam_no']?>" ><?php echo $row['cam_nm']?></a>
                                </td>
                                <td><?php echo $row['adver_nm']?></td>
                                <td class="txt_center">
                                    <div class="btn-group"> 
                                        <span class="cre_gp_span " data-toggle="dropdown" id="cre_gp_status_sel">
                                            <font class="bold" color="<?php if ($row['cre_status']=="1"){echo "#69ae30";}elseif ($row['cre_status']=="2"){echo "#fe9a00";}elseif ($row['cre_status']=="3"){echo "#c85757";}else{echo "#5177bc";}?>">
                                                <?php echo lang($row['cre_status_desc'])?>
                                             </font>
                                            <i class="dropdown-caret fa fa-caret-down ma_t5 ma_l10 cursor"></i>
                                        </span>
                                        <ul class="dropdown-menu float_r ul_sel_box">
                                            <?php 
                                                foreach($sel_status as $sel){
                                                    if($sel['code_key'] != 5 && $sel['code_key'] != 6){
                                            ?>
                                                <li>
                                                    <a href="#" onclick="creative_status_change('<?php echo $sel['code_key'];?>','<?php echo $row['cre_no']?>')">
                                                        <font class="bold" color="<?php if ($sel['code_key']=="1"){echo "#69ae30";}elseif ($sel['code_key']=="2"){echo "#fe9a00";}elseif ($sel['code_key']=="3"){echo "#c85757";}else{echo "#5177bc";}?>">
                                                            <?php echo lang($sel['code_desc']);?>
                                                        </font>
                                                    </a>
                                                </li>
                                            <?php 
                                                    }
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                </td>
                                <td><?php echo lang($row['cre_evaluation_desc'])?></td>
                                <td><?php echo lang($row['cre_gp_type_desc'])?></td>
                                <td class="txt-right"><?php echo number_format($row['imp'])?></td>
                                <td class="txt-right"><?php echo number_format($row['clk'])?></td>
                                <td class="txt-right"><?php echo number_format($row['ctr'],2)?> %</td>
                           </tr>
                       <?php }?> 
                   <?php }?>
               </tbody>
               <tfoot>
               <tr class="even" id="run_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold run"><?php echo lang('strRun')?></span> <?php echo lang('strAd')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($run_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($run_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($run_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even" id="ready_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold ready"><?php echo lang('strReady')?></span> <?php echo lang('strAd')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($ready_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($ready_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($ready_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even" id="pause_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold pause"><?php echo lang('strPause')?></span> <?php echo lang('strAd')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($pause_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($pause_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($pause_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even" id="done_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold done"><?php echo lang('strDone')?></span> <?php echo lang('strAd')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($done_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($done_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($done_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <span class="bold adall"><?php echo lang($sum_creative_list->row_nm)?></span> <?php echo lang('strAd')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($sum_creative_list['imp'])?></td>
                    <td class="txt-right"><?php echo number_format($sum_creative_list['clk'])?></td>
                    <td class="txt-right"><?php echo number_format($sum_creative_list['ctr'], 2)?> %</td>
                </tr>
            </tfoot>       
            </table>
            <div class="center hei_35">
                <div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box_pa" >
                        <li><a href="javascript:page_change(10)">10</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(100)">100</a></li>
                    </ul>
                </div>
                <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div>
        </form>
        <form name="creative_modify" action="/creative/creative_modify_form" method="get">
            <input type="hidden" name="cre_no">
        </form>
		</div>
    </div>

</div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.creative_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function show_btn(cre_no){
        $("#cre_btn_group_"+cre_no).show();
    }

    function hide_btn(cre_no){
        $("#cre_btn_group_"+cre_no).hide();
    }

    function paging(number){
        var frm = document.creative_list;
        frm.action="/creative/creative_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝

    function decode_url(str){
            var s0, i, j, s, ss, u, n, f;
            s0 = "";                // decoded str
            for (i = 0; i < str.length; i++){   // scan the source str
                s = str.charAt(i);
                if (s == "+"){s0 += " ";}       // "+" should be changed to SP
                else {
                    if (s != "%"){s0 += s;}     // add an unescaped char
                    else{               // escape sequence decoding
                        u = 0;          // unicode of the character
                        f = 1;          // escape flag, zero means end of this sequence
                        while (true) {
                            ss = "";        // local str to parse as int
                            for (j = 0; j < 2; j++ ) {  // get two maximum hex characters for parse
                                sss = str.charAt(++i);
                                if (((sss >= "0") && (sss <= "9")) || ((sss >= "a") && (sss <= "f"))  || ((sss >= "A") && (sss <= "F"))) {
                                    ss += sss;      // if hex, add the hex character
                                } else {--i; break;}    // not a hex char., exit the loop
                            }
                            n = parseInt(ss, 16);           // parse the hex str as byte
                            if (n <= 0x7f){u = n; f = 1;}   // single byte format
                            if ((n >= 0xc0) && (n <= 0xdf)){u = n & 0x1f; f = 2;}   // double byte format
                            if ((n >= 0xe0) && (n <= 0xef)){u = n & 0x0f; f = 3;}   // triple byte format
                            if ((n >= 0xf0) && (n <= 0xf7)){u = n & 0x07; f = 4;}   // quaternary byte format (extended)
                            if ((n >= 0x80) && (n <= 0xbf)){u = (u << 6) + (n & 0x3f); --f;}         // not a first, shift and add 6 lower bits
                            if (f <= 1){break;}         // end of the utf byte sequence
                            if (str.charAt(i + 1) == "%"){ i++ ;}                   // test for the next shift byte
                            else {break;}                   // abnormal, format error
                        }
                        s0 += String.fromCharCode(u);           // add the escaped character
                    }
                }
            }
            return s0;
        }

    function modal_view( size, url, cont, img, type,auction_v) {
        $("#modal_size").html(size);
        $("#modal_url").html(url);
        $("#modal_cont").html(cont);
        if (img==""){
            $("#modal_img").attr('src','/img/no_img.png');
        }else{
            $("#modal_img").attr('src','<?php echo IMAGE_BASE_URL?>'+decode_url(img));
        }
        if(type==4) {
            $('.auction-form').show();
            $('.image-adver-tr').hide();

            auction_v = (auction_v > 0)?auction_v:'';
            $("#auction_img").attr('src','/img/auction/img_auction_view'+auction_v+'.png');
        }else{
            $('.auction-form').hide();
            $('.image-adver-tr').show();
        }
        $("#modal_creative").modal();
        $("#modal_creative").modal({ keyboard: false });
        $("#modal_creative").modal('show');
    }

    function get_chart(){
    	$.ajax({
            url : "/creative/creative_chart/",
            dataType : "html",
            beforeSend: function() {
                //통신을 시작할때 처리
                $('#report').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data:{
                mem_no : document.creative_list.mem_no.value,
                cre_gp_no : document.creative_list.cre_gp_no.value,
                fromto_date : document.creative_list.fromto_date.value,
                all : document.creative_list.all.value,
                run : document.creative_list.run.value,
                ready : document.creative_list.ready.value,
                pause : document.creative_list.pause.value,
                done : document.creative_list.done.value
                },

            success : function(result){
                $("#report").html(result);
            }
        });
    }

    $(function () {
        get_chart();
    });

    function creative_status_change(status_key,cre_no){
        if ($(":checkbox[name='sel_creative[]']:checked").length<1&&cre_no=="all"){
            alert("<?php echo lang('strAdSelect');?>");
        }else{
            if (status_key=="del"){
                if (confirm("<?php echo lang('strStatusChangeAlert');?>")){
                    var frm = document.creative_list;
                    frm.action="/creative/sel_creative_delete";
                    frm.status_key.value=status_key;
                    frm.cre_no.value=cre_no;
                    frm.submit();
                }
            }else if (status_key=="copy"){
                new_creative_copy();
            }else{
                if (confirm("<?php echo lang('strStatusChangeAlert1');?>")){
                    var frm=document.creative_list;
                    frm.action="/creative/sel_creative_status_change";
                    frm.status_key.value=status_key;
                    frm.cre_no.value=cre_no;
                    frm.submit();
                }else{
                    alert("<?php echo lang('strStatusChangeAlert2');?>");
                }
            }
        }
    }

    function new_creative_copy(){
        if ($(":checkbox[name='sel_creative[]']:checked").length>1){
            alert("<?php echo lang('strAdCopy');?>");
        }else{
            if (confirm("<?php echo lang('strAdSelectCopy');?>")){
                var frm = document.creative_list;
                frm.action="/creative/new_creative_copy";
                frm.submit();
            }else{
                alert("<?php echo lang('strAdCopyCancel');?>");
            }
        }

    }
    
    function all_check(){
        if ($("#allCheck").prop("checked")) {
           $("input[id=sel_creative]").prop("checked", true);
        } else {
           $("input[id=sel_creative]").prop("checked", false);
        }
    }

    function creative_add(cre_gp_no){
        if (cre_gp_no==""){
            alert("<?php echo lang('strAdGroupAlert1');?>");
        }else{
            frm = document.creative_list;
            frm.action="/creative/creative_add_form/?cre_gp_no="+cre_gp_no;
            frm.submit();
        }
    }

    function creative_modify(cre_no){
        frm = document.creative_modify;
        frm.cre_no.value = cre_no;
        frm.submit();
    }

    function creative_detail_view(cre_no, cre_gp_no){
        frm=document.creative_list;
        frm.action="/creative/creative_detail/?cre_no="+cre_no+"&cre_gp_no="+cre_gp_no;
        frm.cre_no.value = cre_no;
        frm.submit();
    }

    function excel_down(){
    	var cnt = '<?php echo count($creative_list); ?>';
    	if (cnt > 0){
    		var frm = document.creative_list;
            frm.action = '/excel/cre_excel_down';
            frm.submit();
        }else{
        	alert("<?php echo lang('strExcelDwon');?>");
        }
    }
    
    $(document).ready(function() {
        /*
        $('#list').dataTable( {
            "dom": '<"toolbar"Tf>t',
            "order": [[ 1, "asc" ]],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ]
        });
        */
        $("div.toolbar").append($("#all_btn_gp").html());
    });
</script>
<?php 
    $mem_no=$this->session->userdata('mem_no');
?>
<Script>

    var auction_creative_add = false;
    var cheetah_creative_add = false;

    $(document).ready(function() {

        var cre_type = <?php echo $creative_info['cre_type'];?>;
        var cre_width = '<?php echo $creative_info['cre_width']?>';
        var cre_height = '<?php echo $creative_info['cre_height']?>';
        var auction_adver_type = '<?php echo $auction_info[0]['adver_type']?>';
        var tmp_src = "/img/"+cre_width+"x"+cre_height+"_on.png";

        if(cre_type == 4) {
            auction_creative_add = true;
        }
        else if(cre_type == 5) {
            cheetah_creative_add = true;
        }
        else {
            auction_creative_add = false;
            cheetah_creative_add = false;
        }
        
        switch (cre_type) {
            case 4: //옥션
                
                $('.form-imagead').hide();
                $('.form-actionad').show();
                $('.form-cheetah').hide();

                $("#"+cre_width+cre_height+"_action").attr('src', tmp_src);
                
                var class_list = $('ul.crea_tb_ul2 li img[value="'+auction_adver_type+'"]').attr('class');

                var datas_code;
//                class_list.split(' ').forEach(function(i,v){
//                    if(i.indexOf('_view') > 0 )
//                        datas_code = i;
//                })
//
//                codes = datas_code.replace('_view','');
//                $('.'+datas_code).show();
//                $('.'+codes).show();
//                $('ul.crea_tb_ul2 li img[value="'+auction_adver_type+'"]').parent().addClass('selected');
                
                break;

            case 5: //치타

                $('.form-imagead').hide();
                $('.form-actionad').hide();
                $('.form-cheetah').show();

                $("#"+cre_width+cre_height+"_cheetah").attr('src', tmp_src);

                break;

            default:
                $("#"+cre_width).attr('src', tmp_src);
                $('.form-imagead').show();
                $('.form-actionad').hide();
                $('.form-cheetah').hide();
        }
    });
</Script>
<!-- 광고정보modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <h4 class="h4_blit"><?php echo lang('strTestURL');?></h4>
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <img src = "" id="modal_img"></td>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span data-dismiss="modal" onclick="$('#modal1').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strEditCreativeInformation')?></h3>
	</div>
	<div class="panel-body">
	    <div class="clear">

            <form id="creative_modify" name="creative_modify" action="/creative/creative_modify_save" method="post" enctype="multipart/form-data">
                <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
                <input type="hidden" name="cre_no" value="<?php echo $creative_info['cre_no'];?>">
                <input type="hidden" name="cre_gp_no" value="<?php echo $creative_info['cre_gp_no'];?>">
                <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                    <colgroup>
                        <col width="25%">
                        <col width="*">
                    </colgroup>
           
                <tr>
                    <th><?php echo lang('strAd')?><?php echo lang('strName')?></th>
                    <td>
                        <input type="text" class="wid_40p" name="cre_nm" id="cre_nm" value="<?php echo $creative_info['cre_nm']?>" onblur="input_count_nm()"/>
                        <?php echo lang('strCampaignTxtCount');?>
                        <?php echo lang('strCampaignTxtInfo');?>
                        <input type="hidden" id="nm_check_commit" name="nm_check_commit" value="" validate="null,아이디"><br />
                        <span id="nm_check_result" class="join_info ma_l5"></span>
                    </td>
                </tr>
                <tr>
                    <th><?php echo lang('strFormOfAd')?></th>
                    <td>
                        <input type="radio" name="cre_type" value="1" class="ad-types" <?php if ($creative_info['cre_type'] == 1){echo "checked";}?> /> <?php echo lang('strImage');?>&nbsp;

                        <!-- 옥션 선택 버튼 -->
                        <?php if($this->session->userdata('admin_fl')=='Y' || $this->session->userdata('mem_id') == AUCTION_USER_ID || DEVEL_ADMIN === true ) {?>
                            <input type="radio" name="cre_type" value="4"  class="ad-types" <?php if ($creative_info['cre_type'] == 4){echo "checked";}?>/> <?php echo lang('strAction');?>&nbsp;
                        <!-- 치타 선택 버튼 -->
                            <input type="radio" name="cre_type" value="5"  class="ad-types" <?php if ($creative_info['cre_type'] == 5){echo "checked";}?>/> <?php echo lang('strChetah');?>
                        <?php } ?>
                    </td>
                </tr>
                <style>
                    .form-actionad{display:none;}
                    .form-cheetah{display:none;}
                </style>
                <tr class="form-imagead" >
                    <th><?php echo lang('strSize')?></th>
                    <td>
                        <input type="hidden" id="cre_width"  name="cre_width" value="<?php if($creative_info['cre_type'] == 1) { echo $creative_info['cre_width']; }?>">
                        <input type="hidden" id="cre_height" name="cre_height" value="<?php if($creative_info['cre_type'] == 1) { echo $creative_info['cre_height']; }?>">
                      	<ul class="crea_tb_ul">
                            <li onclick="sel_cre_size('120','600')"><img src="/img/120x600.png" id="120" alt="120x600"></li>
                            <li onclick="sel_cre_size('160','600')" ><img src="/img/160x600.png" id="160" alt="160x600"></li>
                            <li onclick="sel_cre_size('170','128')" ><img src="/img/170x128.png" id="170" alt="170x128"></li>
                            <li onclick="sel_cre_size('200','200')" ><img src="/img/200x200.png" id="200" alt="200x200"></li>
                            <li onclick="sel_cre_size('250','250')" ><img src="/img/250x250.png" id="250" alt="250x250"></li>
                            <li onclick="sel_cre_size('300','250')" class="clear_l"><img src="/img/300x250.png" id="300" alt="300x250"></li>
                            <li onclick="sel_cre_size('336','280')" ><img src="/img/336x280.png" id="336" alt="336x280"></li>
                            <li onclick="sel_cre_size('468','60')" ><img src="/img/468x60.png" id="468" alt="468x60"></li>
                            <li onclick="sel_cre_size('728','90')"><img src="/img/728x90.png" id="728" alt="728x90"></li>
                            <li onclick="sel_cre_size('970','90')" ><img src="/img/970x90.png" id="970" alt="970x90"></li>
                      	</ul>
                    </td>
                </tr>
                <tr class="form-imagead">
                    <th><?php echo lang('strMaterial')?></th>
                    <td>
                        <span class="pull-left btn btn-default btn-file">
                            <?php echo lang('strSelectFile')?>  <input type="file" onchange="creative_image_link()" id="cre_link_file" name="cre_link_file">
                            <input type="hidden" id="upload_file_nm" name="upload_file_nm">
                        </span>
                        <input type="hidden" name="cre_img_link" id="cre_img_link" value="<?php echo $creative_info['cre_img_link'];?>">
                        <input type="hidden" name="ori_cre_img_link" id="ori_cre_img_link" value="<?php echo $creative_info['cre_img_link'];?>">

                        <div class="float_l point_dgray ma_l5 ma_t8" id="cre_img_nm">
                            <?php if ($creative_info['cre_img_link'] != ""){?>
                                <?php echo $creative_info['cre_img_link'];?>
                            <?php }else{?>
                                <?php echo lang('strThereIsNoFileSelected')?>
                            <?php }?>
                        </div>
                    </td>
                </tr>
                <tr class="form-imagead">
                    <th>URL</th>
                    <td>
                        <input type="hidden" id="cre_link_type" class="float_l" name="cre_link_type" value="1">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_cre_link_type">
                                &nbsp;  <?php echo "http://"?>  &nbsp;
                                <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <li><a href="javascript:sel_cre_link_type('http');">http://</a></li>
                                    <li><a href="javascript:sel_cre_link_type('https');">https://</a></li>
                                </ul>
                            </div>
                        <input type="text" class="wid_40p float_l ma_l5 ma_r5" name="cre_link" id="cre_link" value="<?php echo $creative_info['cre_link'];?>">
                        <input type="hidden" name="ori_cre_link" id="ori_cre_link" value="<?php echo $creative_info['cre_link'];?>">
                        <span class="btn btn-default" class="float_l " onclick="url_test();"><?php echo lang('strTestURL')?></span>
                    </td>
                </tr>
                <tr class="form-imagead" >
                    <th><?php echo lang('strDescription')?></th>
                    <td>
                        <textarea class="crea_txt_box" id="cre_cont" name="cre_cont" onkeyup="input_count_cont()" ><?php echo $creative_info['cre_cont'];?></textarea>
                        <input type="hidden" name="ori_cre_cont" id="ori_cre_cont" value="<?php echo $creative_info['cre_cont'];?>">
                        <span class="float_left ma_t20 ma_l10" id="cre_cont_byte">0 / 30<?php echo lang('strCampaignTxtCount2');?></span>
                        <p class="clear ma_b10 point_dgray"><?php echo lang('strWarningAdCreate')?></p>
                    </td>
                </tr>
                <tr class="form-imagead" >
                    <th><?php echo lang('strPreview')?></th>
                    <td>
                        <div class="sum_view">
                            <?php if ($creative_info['cre_img_link'] == ""){?>
                               <img src="/img/no_img.png" width="120px" height="120px" id="preview">
                           <?php }else{?>
                               <img src="<?php echo IMAGE_BASE_URL?><?php echo $creative_info['cre_img_link'];?>" width="120px" height="120px" id="preview">
                           <?php }?>
                        </div>
                    </td>
                </tr>
                <?php if($this->session->userdata('admin_fl')=='Y' || $this->session->userdata('mem_id') == AUCTION_USER_ID || DEVEL_ADMIN === true ) { ?>
                    <!-- 옥션 광고 -->
                    <input type="hidden" name="cre_bncode_site" id="cre_bncode_site" />
                    <tr class="form-actionad">
                        <th><?php echo lang('strAPI')?></th>
                        <td>
                            <div class="ex_frm">
                                <table class="inner-datas-auction" >
                                    <colgroup>
                                        <col width="100px" />
                                        <col width="*" />
                                    </colgroup>
                                    <tr>
                                        <td style="height:30px;" >BNCODE</td>
                                        <td>사이트</td>
                                    </tr>
                                    <?php if (isset($auction_info)){?>
                                        <?php foreach($auction_info as $row){?>
                                            <tr>
                                                <td><?php echo $row['bn_code']?></td>
                                                <td><?php echo $row['site_domain']?></td>
                                            </tr>
                                        <?php }?>
                                    <?php }?>
                                </table>
                            </div>
                            <form name="aution_file_upload" method="post" id="aution_file_upload" enctype="multipart/form-data" >
                                <span class="btn btn-dark ma_t8 ma_r3"  id="clean_excel_btn" ><?php echo lang('strCancel')?></span>
                                <label class="btn btn-primary ma_t8 ma_r3" for="cre_excel_auction" ><?php echo lang('strUpload')?></label>
                                <a href="/template/auction_sample.xlsx" target="_blank" class="btn btn-default ma_t8"  ><?php echo lang('strExcelSample')?></a>
                                <input type="file" name="cre_excel_auction" id="cre_excel_auction" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"  class="hidden" >
                            </form>
                        </td>
                    </tr>
                    <tr class="form-actionad" >

                        <th><?php echo lang('strSize')?></th>
                        <td>
                            <input type="hidden" id="cre_width_action" name="cre_width_action" value="<?php if($creative_info['cre_type'] == 4) { echo $creative_info['cre_width']; }?>"/>
                            <input type="hidden" id="cre_height_action" name="cre_height_action" value="<?php if($creative_info['cre_type'] == 4) { echo $creative_info['cre_height']; }?>"/>
                            <ul class="crea_tb_ul">
                                <li onclick="sel_cre_size_action('200','200',1,1)" ><img src="/img/200x200.png" id="200200_action" alt="200x200"></li>
                                <li onclick="sel_cre_size_action('300','250',1,2)" ><img src="/img/300x250.png" id="300250_action" alt="300x250"></li>
                                <li onclick="sel_cre_size_action('320','50',2,3)" ><img src="/img/320x50.png" id="32050_action" alt="336x280"></li>
                                <li onclick="sel_cre_size_action('320','100',2,4)" ><img src="/img/320x100.png" id="320100_action" alt="336x280"></li>
                            </ul>
                        </td>

                    </tr>
                    <input type="hidden" name="auction_adtype" id="auction_adtype" value="1"/>
                    <!-- 옥션 미리보기 
                    <tr class="form-actionad">
                        <input type="hidden" name="auction_adtype" id="auction_adtype" value="<?=$auction_info[0]['adver_type']?>"/>
                        <th><?php echo lang('strPreview')?></th>
                        <td>
                            <ul class="crea_tb_ul2">
                                <li><img src="/img/auction/img_auction_view.png" alt="preview" class="type2_view  typs3 preview-image" value="1" /></li>
                                <li><img src="/img/auction/img_auction_view2.png" alt="preview" class="type2_view typs3 preview-image" value="2" /></li>
                                <li><img src="/img/auction/img_auction_view3.png" alt="preview" class="type2_view typs4 preview-image" value="3" /></li>
                                <li><img src="/img/auction/img_auction_view4.png" alt="preview" class="type1_view typs2 typs1 preview-image" value="4" /></li>
                                <li><img src="/img/auction/img_auction_view5.png" alt="preview" class="type1_view typs2 preview-image" value="5" /></li>
                                <li><img src="/img/auction/img_auction_view6.png" alt="preview" class="type1_view typs1 preview-image" value="6" /></li>
                            </ul>
                        </td>
                    </tr>-->
                    <script>
                        var html_form_own_acution = "";

                        /*옥션 광고 스크립트 추가 */
                        $('.crea_tb_ul2 li img').click(function(){
                            var $this = $(this);
                            var _get_value = $(this).attr('value');
                            $('.crea_tb_ul2 li').removeClass('selected');
                            $this.parent().addClass('selected');
                            $('input[name=auction_adtype]').val(_get_value);
                        });

                        $('.ad-types').click(function(){
                            switch($(this).val())
                            {
                                case '4':
                                    $('.form-imagead').hide();
                                    $('.form-actionad').show();
                                    $('.form-cheetah').hide();

                                    auction_creative_add = true;
                                    cheetah_creative_add = false;

                                    break;

                                case '5':
                                    $('.form-imagead').hide();
                                    $('.form-actionad').hide();
                                    $('.form-cheetah').show();

                                    auction_creative_add = false;
                                    cheetah_creative_add = true;

                                    break;

                                default:
                                    $('.form-imagead').show();
                                    $('.form-actionad').hide();
                                    $('.form-cheetah').hide();

                                    auction_creative_add = false;
                                    cheetah_creative_add = false;
                            }
                        });


                        $("input[name=cre_excel_auction]:file").change(function (){
                            var get_form = $('form#aution_file_upload');
                            var formData = new FormData();
                            formData.append("cre_excel_auction",$("input[name=cre_excel_auction]")[0].files[0]);

                            $.ajax({
                                url: '/excel/read_excel_form',
                                processData: false,
                                contentType: false,
                                data: formData,
                                type: 'POST',
                                dataType : 'json',
                                success: function (result) {
                                    $('input#cre_bncode_site').val(result.data);
                                    html_form_own_acution = $('table.inner-datas-auction').clone();
                                    $('table.inner-datas-auction').html(result.html);
                                },error:function(data){
                                    console.log(data);
                                    
                                }
                            });
                        });

                        $('#clean_excel_btn').click(function(){

                            html_form_own_acution = $('table.inner-datas-auction');

                            if(html_form_own_acution=="") {
                                return false;
                            }

//                            $('table.inner-datas-auction').html(html_form_own_acution);
                            $('table.inner-datas-auction').html("");
                            $("input[name=cre_excel_auction]:file").val('');
                            $('input#cre_bncode_site').val('');
                        });
                    </script>
                <?php }?>

                <!-- 치타 -->
                <?php if($this->session->userdata('admin_fl')=='Y' || $this->session->userdata('mem_id') == AUCTION_USER_ID || DEVEL_ADMIN === true ) { ?>
                    <input type="hidden" name="cre_zoneid_site" id="cre_zoneid_site" />
                    <tr class="form-cheetah">
                        <th><?php echo lang('strSize')?></th>
                        <td>
                            <input type="hidden" id="cre_width_cheetah" name="cre_width_cheetah" value="<?php if($creative_info['cre_type'] == 5) { echo $creative_info['cre_width']; }?>">
                            <input type="hidden" id="cre_height_cheetah" name="cre_height_cheetah" value="<?php if($creative_info['cre_type'] == 5) { echo $creative_info['cre_height']; }?>">
                            <input type="hidden" name="cre_status" value="<?php echo $creative_info['cre_status']?>"/>
                            <input type="hidden" name="cre_evaluation" value="<?php echo $creative_info['cre_evaluation']?>"/>

                            <ul class="crea_tb_ul">
                                <li onclick="sel_cre_size_cheetah('320','50')"><img src="/img/320x50.png" id="32050_cheetah" alt="320x50"></li>
                                <li onclick="sel_cre_size_cheetah('320','100')"><img src="/img/320x100.png" id="320100_cheetah" alt="320x100"></li>
                                <li onclick="sel_cre_size_cheetah('200','200')"><img src="/img/200x200.png" id="200200_cheetah" alt="200x200"></li>
                                <li onclick="sel_cre_size_cheetah('300','250')"><img src="/img/300x250.png" id="300250_cheetah" alt="300x250"></li>
                            </ul>
                        </td>
                    </tr>

                    <tr class="form-cheetah">
                        <th>
                            ZoneID 등록
                        </th>
                        <td>
                            <div class="ex_frm">
                                <table class="inner-datas-cheetah" >
                                    <colgroup>
                                        <col width="100px" />
                                        <col width="*" />
                                    </colgroup>
                                    <tr>
                                        <td style="height:30px;" >ZoneID</td>
                                        <td>사이트</td>
                                    </tr>
                                    <?php if (isset($cheetah_info)){?>
                                        <?php foreach($cheetah_info as $row){?>
                                            <tr>
                                                <td><?php echo $row['zone_id']?></td>
                                                <td><?php echo $row['site_domain']?></td>
                                            </tr>
                                        <?php }?>
                                    <?php }?>
                                </table>
                            </div>
                            <form name="cheetah_file_upload" method="post" id="cheetah_file_upload" enctype="multipart/form-data" >
                                <span class="btn btn-dark ma_t8 ma_r3"  id="clean_cheetah_excel_btn" ><?php echo lang('strCancel')?></span>
                                <label class="btn btn-primary ma_t8 ma_r3" for="cre_excel_cheetah" ><?php echo lang('strUpload')?></label>
                                <a href="/template/auction_sample.xlsx" target="_blank" class="btn btn-default ma_t8"  ><?php echo lang('strExcelSample')?></a>
                                <input type="file" name="cre_excel_cheetah" id="cre_excel_cheetah" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"  class="hidden" >
                            </form>
                        </td>
                    </tr>

                    <tr class="form-cheetah">
                        <th>
                            <?php echo lang('strPreview')?>
                        </th>
                        <td>
                            <div class="sum_view">
                                <img src="/img/no_img.png" width="120px" height="120px" id="">
                            </div>
                        </td>
                    </tr>
                    <script>
                        var html_form_own_cheetah = "";
                        $("input[name=cre_excel_cheetah]:file").change(function (){
                            var get_form_cheetah = $('form#cheetah_file_upload');
                            var formData_cheetah = new FormData();
                            formData_cheetah.append("cre_excel_cheetah",$("input[name=cre_excel_cheetah]")[0].files[0]);

                            $.ajax({
                                url: '/excel/read_excel_form_cheetah',
                                processData: false,
                                contentType: false,
                                data: formData_cheetah,
                                type: 'POST',
                                dataType : 'json',
                                success: function (result) {
                                    $('input#cre_zoneid_site').val(result.data);
                                    html_form_own_cheetah = $('table.inner-datas-cheetah').clone();
                                    $('table.inner-datas-cheetah').html(result.html);
                                }
                            });
                        });

                        $('#clean_cheetah_excel_btn').click(function(){

                            html_form_own_cheetah = $('table.inner-datas-cheetah')

                            if(html_form_own_cheetah=='') {
                                return false;
                            }

//                            $('table.inner-datas-cheetah').html(html_form_own_cheetah);
                            $('table.inner-datas-cheetah').html("");
                            $("input[name=cre_excel_cheetah]:file").val('');
                            $('input#cre_zoneid_site').val('');
                        });

                        $('.ad-types').click(function(){
                            switch($(this).val())
                            {
                                case '4':
                                    $('.form-imagead').hide();
                                    $('.form-actionad').show();
                                    $('.form-cheetah').hide();

                                    auction_creative_add = true;
                                    cheetah_creative_add = false;

                                    break;

                                case '5':
                                    $('.form-imagead').hide();
                                    $('.form-actionad').hide();
                                    $('.form-cheetah').show();

                                    auction_creative_add = false;
                                    cheetah_creative_add = true;

                                    break;

                                default:
                                    $('.form-imagead').show();
                                    $('.form-actionad').hide();
                                    $('.form-cheetah').hide();

                                    auction_creative_add = false;
                                    cheetah_creative_add = false;
                            }
                        });
                    </script>
                <?php } ?>
            </table>
            </form>
        </div>
        <p class="txt-right" >
        	<span onclick="creative_modify_save();" class="btn btn-primary"><?php echo lang('strSave')?></span>
        	<span class="btn btn-dark" onclick="history.back();"><?php echo lang('strClose')?></span>
        </p>
	</div>
</div>
<script type="text/javascript">
    function creative_modify_save(){
        if($("#cre_nm").val()==""){
            alert("<?php echo lang('strAdNameEntry');?>");
            return false;

        }else if($("input[name='cre_type']:checked").length==0){
            alert("<?php echo lang('strAdTypeSelect');?>");
            return false;
        }

        switch ($('input[name=cre_type]:checked').val())
        {

            case '4': //옥션
                if($("#cre_width_action").val()=="") {
                    alert("<?php echo lang('strAdSizeSelect');?>");
                    return false;
                }else if($('input#cre_action_type').val()=="0")
                {
                    alert("광고타입을 선택해 주세요.");
                    return false;
                }

                formsave();
                break;

            case '5': //치타
                if($("#cre_width_cheetah").val()=="") {
                    alert("<?php echo lang('strAdSizeSelect');?>");
                    return false;
                }

                formsave();
                break;

            case '1':
            default:
                if($("#cre_width").val()==""){
                    alert("<?php echo lang('strAdSizeSelect');?>");
                    return false;
                }else if($("#cre_img_link").val()==""){
                    alert("<?php echo lang('strAdSubSelect');?>");
                    return false;
                }else if($("#cre_link").val()=="" || $("#cre_link").val()=="http://" || $("#cre_link").val()=="https://"){
                    alert("<?php echo lang('strUrlEntry');?>");
                    return false;
                }else if($("#cre_cont").val()=="") {
                    alert("<?php echo lang('strAdCaption');?>");
                    return false;
                }
                formsave();


        }

        function formsave()
        {

            var url = '/creative/creative_modify_save';
            if(auction_creative_add==true)
            {
                var post_optin = {
                    mem_no: '<?php echo $mem_no?>',
                    cre_gp_no: '<?php echo $creative_info['cre_gp_no']?>',
                    cre_no : '<?php echo $creative_info['cre_no']?>',
                    cre_nm : $("#cre_nm").val(),
                    cre_width : $("#cre_width").val(),
                    cre_height : $("#cre_height").val(),
                    cre_link : $("#cre_link").val(),
                    cre_cont : $("#cre_cont").val(),
                    cre_img_link : $("#cre_img_link").val(),
                    cre_status :'<?php echo $creative_info['cre_status']?>',
                    cre_evaluation :'<?php echo $creative_info['cre_evaluation']?>',

                    /*옥션 추가*/
                    cre_action_bncode : $('#cre_action_bncode').val(),
                    cre_width_action : $('#cre_width_action').val(),
                    cre_height_action : $('#cre_height_action').val(),
                    cre_bncode_site : $('#cre_bncode_site').val(),
                    cre_action_type : $('#auction_adtype').val(),
                    cre_type : $(':radio[name="cre_type"]:checked').val()
                }

            }
            else if(cheetah_creative_add == true){

                var post_optin = {

                    mem_no: '<?php echo $mem_no?>',
                    cre_gp_no: '<?php echo $creative_info['cre_gp_no']?>',
                    cre_no : '<?php echo $creative_info['cre_no']?>',
                    cre_nm : $("#cre_nm").val(),
                    cre_width : $("#cre_width").val(),
                    cre_height : $("#cre_height").val(),
                    cre_link : $("#cre_link").val(),
                    cre_cont : $("#cre_cont").val(),
                    cre_img_link : $("#cre_img_link").val(),
                    cre_status :'<?php echo $creative_info['cre_status']?>',
                    cre_evaluation :'<?php echo $creative_info['cre_evaluation']?>',

                    /* 치타 */
                    cre_width_cheetah : $('#cre_width_cheetah').val(),
                    cre_height_cheetah : $('#cre_height_cheetah').val(),
                    cre_zoneid_site : $('#cre_zoneid_site').val(),
                    cre_type : $(':radio[name="cre_type"]:checked').val()
                }
            }
            else{
                var post_optin = {
                    mem_no: '<?php echo $mem_no?>',
                    cre_gp_no: '<?php echo $creative_info['cre_gp_no']?>',
                    cre_no : '<?php echo $creative_info['cre_no']?>',
                    cre_nm : $("#cre_nm").val(),
                    cre_width : $("#cre_width").val(),
                    cre_height : $("#cre_height").val(),
                    cre_link : $("#cre_link").val(),
                    cre_cont : $("#cre_cont").val(),
                    ori_cre_link : $("#ori_cre_link").val(),
                    ori_cre_cont : $("#ori_cre_cont").val(),
                    ori_cre_img_link : $("#ori_cre_img_link").val(),
                    cre_img_link : $("#cre_img_link").val(),
                    cre_status :'<?php echo $creative_info['cre_status']?>',
                    cre_evaluation :'<?php echo $creative_info['cre_evaluation']?>',
                    cre_type : $(':radio[name="cre_type"]:checked').val()
                }
            }

            $.post(url,
                post_optin,
                function(data){

                    if(data.trim()=="false"){
                        alert("<?php echo lang('strNewAdSave');?>");
                        return false;
                    }else{
                        location.replace("/creative/creative_list/");
                    }
                }
            );
        }
    }

/*
    function creative_modify_save(){

        if($("#cre_nm").val()==""){
            alert("<?php echo lang('strAdNameEntry');?>");
        }else if($("input[name='cre_type']:checked").length==0){
            alert("<?php echo lang('strAdTypeSelect');?>");
        }else if($("#cre_width").val()==""){
            alert("<?php echo lang('strAdSizeSelect');?>");
        }else if($("#cre_img_link").val()==""){
            alert("<?php echo lang('strAdSubSelect');?>");
        }else if($("#cre_cont").val()==""){
            alert("<?php echo lang('strAdCaption');?>");
        }else if($("#cre_link").val()=="" || $("#cre_link").val()=="http://" || $("#cre_link").val()=="https://"){
            alert("<?php echo lang('strUrlEntry');?>");
        }else{
            var url = '/creative/creative_modify_save';
            $.post(url,
                {
                    mem_no: '<?php echo $mem_no?>',
                    cre_gp_no: '<?php echo $creative_info->cre_gp_no?>',
                    cre_no : '<?php echo $creative_info->cre_no?>',
                    cre_nm : $("#cre_nm").val(),
                    cre_width : $("#cre_width").val(),
                    cre_height : $("#cre_height").val(),
                    cre_link : $("#cre_link").val(),
                    ori_cre_link : $("#ori_cre_link").val(),
                    cre_cont : $("#cre_cont").val(),
                    ori_cre_cont : $("#ori_cre_cont").val(),
                    cre_img_link : $("#cre_img_link").val(),
                    ori_cre_img_link : $("#ori_cre_img_link").val(),
                    cre_link : $("#cre_link").val(),
                    cre_type : $(':radio[name="cre_type"]:checked').val()
                },
                function(data){
                if(data.trim()=="false"){
                    alert("<?php echo lang('strAdEdit');?>");
                }else{
                    alert("<?php echo lang('strAdEdit2');?>");
                    var frm = document.creative_modify;
                    frm.action = '/creative/creative_list/';
                    frm.submit();
                    //location.href="/creative/creative_list/1/"+<?php echo $creative_info->cre_gp_no?>;
                }
            }
        );
        }
    }
*/
    function creative_image_link(){

        if($('#cre_width').val() == ""){
            alert('<?php echo lang('strAdCreativeAlert');?>');
        }else{
            var form = $('form')[0];
            var formData = new FormData(form);
            $.ajax({
                url: '/creative/creative_image_file',
                processData: false,
                contentType: false,
                data: formData,
                type: 'POST',
                success: function(result){
                    if(result.trim() == 'false'){
                        alert('<?php echo lang('strAdCreativeAlert1');?>');
                    }else{
                        $("#cre_img_nm").empty();
                        $("#cre_img_nm").append(result);
                        $("#cre_img_link").attr('value', result);
                        $("#preview").attr('src',"<?php echo IMAGE_BASE_URL?>"+result);
                    }
                }
            });
        }
        $("#cre_link_file").val('');
    }

    function input_count_cont(){

        var limit_length = 30;
        var msg_length = 0;

        //String에 bytes() 함수 만들기
        String.prototype.bytes = function() {
            var msg = this;
            var cnt = 0;

            //한글이면 2, 아니면 1 count 증가
            for( var i=0; i< msg.length; i++) {
                cnt += (msg.charCodeAt(i) > 128 ) ? 1 : 1;
            }
            return cnt;
        }
        //textarea에서 키를 입력할 때마다 동작
        msg_length = $("#cre_cont").val().bytes();

        if( msg_length <= limit_length ) {
            $("#cre_cont_byte").html( msg_length +' / 30<?php echo lang('strCampaignTxtCount2');?>' );
        } else {
            alert("<?php echo lang('strCampaignTxtCountOver');?>");
            $("#cre_cont_byte").css("color", "red");
            $("#cre_cont").val($("#cre_cont").val().substring(0, 30));
            $("#cre_cont_byte").html( msg_length +' / 30<?php echo lang('strCampaignTxtCount2');?>');
            
        }
    }

    function input_count_nm(){

        var limit_length = 15;
        var msg_length = 0;

        String.prototype.bytes = function() {
            var msg = this;
            var cnt = 0;

            //한글이면 2, 아니면 1 count 증가
            for( var i = 0; i < msg.length; i++) {
                cnt += (msg.charCodeAt(i) > 128 ) ? 1 : 1;
            }
            return cnt;
        }
        input_type_limit();
        //textarea에서 키를 입력할 때마다 동작
        msg_length = $("#cre_nm").val().bytes();

        if( msg_length <= limit_length ) {
        	$("#input_count_nm").css("color", "black");
            $("#input_count_nm").html( msg_length );
        } else {
            $("#input_count_nm").css("color", "red");
            $("#input_count_nm").html( "15" );
            $("#cre_nm").val($("#cre_nm").val().substring(0, 15));
        }
        nm_check();
    }

    function nm_check(){
        var cre_gp_no = '<?php echo $cre_gp_no;?>';
        var cre_no = '<?php echo $creative_info['cre_no'];?>';
        $.ajax({
            type:"POST",
            url:"/creative/modify_creative_nm_check/",
            data : {cre_nm : $("#cre_nm").val(), cre_gp_no : cre_gp_no, cre_no : cre_no },
            timeout : 30000,
            async:false,
            cache : false,
            success: function (data){
                $("#nm_check_commit").val("");
                switch(data.trim()){
                    case "true":
                        var show_args="<?php echo lang('strCreativeCheckAlert');?>";
                        $("#nm_check_commit").val("Y");
                        break;
                    case "false":
                        var show_args="<?php echo lang('strCreativeCheckAlert1');?>";
                        alert("<?php echo lang('strCreativeCheck1');?>");
                        break;
                    case "none":
                        var show_args="<?php echo lang('strCreativeCheckAlert2');?>";
                        alert("<?php echo lang('strCreativeCheck2');?>");
                        break;

                }

                $('#nm_check_result').html(show_args);
            },
            error: function whenError(e){
                alert("code : " + e.status + "\r\nmessage : " + e.responseText);
            }
        });
    }

    function input_type_limit(){
        var objEvent = event.srcElement;
        var numPattern = / [\[\]{}()<>?|`~!@#$%^&*+=;:\"'\\]/g;
        numPattern = objEvent.value.match(numPattern);

        if (numPattern != null) {
            alert("<?php echo lang('strCampaignTxtInfo2');?>");
            objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
            objEvent.focus();
            return false;
        }
    }
    /*
    function modal_view() {
        $("#modal_img").attr('src',$("#cre_link").val());
        $("#modal1").modal();
        $("#modal1").modal({ keyboard: false });
        $("#modal1").modal('show');
    }
    */
    function url_test(){
        window.open($("#cre_link").val());
    }

    function sel_cre_size(width,height){
        if($("#cre_width").val()==""){
            $("#"+width).attr('src','/img/'+width+"x"+height+"_on.png");
            $("#cre_width").attr('value',width);
            $("#cre_height").attr('value',height);
        }else{
            var ori_width=$("#cre_width").val();
            var reset=$("#"+ori_width).attr('alt');
            $("#"+ori_width).attr('src','/img/'+reset+".png");
            $("#"+width).attr('src','/img/'+width+"x"+height+"_on.png");
            $("#cre_width").attr('value',width);
            $("#cre_height").attr('value',height);
        }

        $("#cre_img_nm").empty();
        $("#cre_img_nm").append("<?php echo lang('strThereIsNoFileSelected');?>");
        $("#cre_img_link").val('');
        $("#cre_link_file").val('');
        $("#preview").attr('src',"/img/no_img.png");
    }

    function sel_cre_size_action(width,height,preview,types){

        if($("#cre_width_action").val()==""){
            $("#"+width+height+'_action').attr('src','/img/'+width+"x"+height+"_on.png");

            $("#cre_width_action").attr('value',width);
            $("#cre_height_action").attr('value',height);
        }else{
            if (confirm("<?php echo lang('strImgSize');?>")){
                var ori_width=$("#cre_width_action").val();
                var ori_height=$("#cre_height_action").val();

                $('#'+ori_width+ori_height+'_action').attr('src','/img/'+ori_width+"x"+ori_height+".png");
                $("#"+width+height+'_action').attr('src','/img/'+width+"x"+height+"_on.png");

                $("#cre_width_action").attr('value',width);
                $("#cre_height_action").attr('value',height);


            }
        }
        $('.type1_view,.type2_view').hide();
        $('.type'+preview+'_view').show();
        $('.preview-image').hide();
        switch (types)
        {
            case 1:
                $('.typs1').show();
                break;
            case 2:
                $('.typs2').show();
                break;
            case 3:
                $('.typs3').show();
                break;
            case 4:
                $('.typs4').show();
                break;
        }

    }

    function sel_cre_size_cheetah(width,height) {

        if($("#cre_width_cheetah").val()==""){

            $("#" + width + height + '_cheetah').attr('src','/img/'+width+"x"+height+"_on.png");
            $("#cre_width_cheetah").attr('value',width);
            $("#cre_height_cheetah").attr('value',height);

        }else{

            if (confirm("<?php echo lang('strImgSize');?>")){
                var ori_width=$("#cre_width_cheetah").val();
                var ori_height=$("#cre_height_cheetah").val();

                $('#'+ori_width+ori_height+'_cheetah').attr('src','/img/'+ori_width+"x"+ori_height+".png");
                $("#"+width+height+'_cheetah').attr('src','/img/'+width+"x"+height+"_on.png");

                $("#cre_width_cheetah").attr('value',width);
                $("#cre_height_cheetah").attr('value',height);
            }
        }

        $("#preview").attr('src',"/img/no_img.png");

    }

    $(function () {
        var ori_width = $("#cre_width").val();
        var reset = $("#"+ori_width).attr('alt');
        $("#"+ori_width).attr('src','/img/'+reset+"_on.png");
    });

</script>
<?php 
    $mem_no=$this->session->userdata('mem_no');
?>
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <h4 class="h4_blit"><?php echo lang('strFullSizeView')?></h4>
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <div class="ma_b10 pa_l10">
                        <span class="sub_tit"><?php echo lang('strAdInfo');?></span><br />
                    </div>
                    
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="join_tb bd-t2-gray">
                        <colgroup>
                            <col width="30%">
                            <col width="70%">
                        </colgroup>
                        <tr>
                            <th class="point_blue"><?php echo lang('strSize')?></th>
                            <td>
                                <span id="modal_size"></span>
                                
                            </td>
                        </tr>
                        <tr>
                            <th class="point_blue">URL</th>
                            
                            <td><span id="modal_url"></span></td>
                        </tr>
                        <tr>
                            <th class="point_blue"><?php echo lang('strCaption')?></th>
                            <td><span id="modal_cont"></span></td>
                        </tr>	
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span data-dismiss="modal" onclick="$('#modal1').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo $creative_detail->cam_nm;?></a></li>
    		<li><a href="#"><?php echo $creative_detail->cre_gp_nm;?></a></li>
    		<li class="active"><?php echo $creative_detail->cre_nm;?></li>
    	</ol>
    </div>	
    <div class="panel-heading">
    	<h3 class="page-header text-overflow tit_blit"><?php echo lang('strAd')?> <?php echo lang('strOverview')?></h3>
    </div>
    <!--요기 -->
    <div class="panel-body">
	<div class="camp_rept">
		<div class="camp_rept_his">
	 		<span><?php echo lang('strAd')?> <?php echo lang('strReport')?> <?php echo lang('strView')?></span>
		</div>
		<div class="camp_info hei_45">
				<div class="camp_info_box">
    				<div class="camp_info_l">
    				    <ul>
                            <li class="ma_l0">
        					   <i class="fa fa-bars fa-sm fa_b color_b2"> 
        					       <?php echo lang('strImpressions')?> :
        				       </i>  
        				       <span class="font_bold"><?php echo number_format($sum_creative_detail['imp'])?></span>
        			        </li>
        					<li>
        					   <i class="fa fa-hand-o-up fa-sm fa_b color_b2">
        					        <?php echo lang('strClicks')?> : 
        				       </i> 
        				       <span class="font_bold"> <?php echo number_format($sum_creative_detail['clk'])?></span>
        			        </li>	    	
        					<li>
        					   <i class="fa fa-pie-chart fa-sm fa_b color_b2"> CTR :</i> 
        					   <span class="font_bold"> 
        					       <?php echo number_format($sum_creative_detail['ctr'],2)?> %
        					   </span>
        				    </li>
                            <li><i class="fa fa-won fa-sm fa_b color_b2"> <?php echo lang('strTotalValue')?> : </i> <span class="font_bold"> <?php echo number_format($sum_creative_detail['price'])?></span></li>
                        </ul>
    				</div>
    				<div id="demo-dp-component" class="camp_info_r2">
        				<input type="text" name="daterange" id="daterange" class="float_l ma_r10 range wid_70p" value="<?php echo $fromto_date?>" /> 
        				<span class="iput-group-addon float_l ma_t8">
        					<i class="fa fa-calendar fa-lg" onclick="$('#daterange').focus();"></i>
        				</span>
        				<a href="javascript:search();" class="float_l btn btn-primary ma_l10"><?php echo lang('strSearch')?></a>
        			</div>
		      </div>
		</div>
    </div>
   </div>
    <div class="panel-body pa_t0">
	   <div class="clear bd_eb" id="report">
       </div>
	</div>
    <div class="panel-heading">
    </div>
	<div class="panel-body">
        <div class="clear" id="all_btn_gp">
            <div class="btn-group float_r ma_b10">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="cre_status_sel">
                    &nbsp;  <?php echo lang('strModify')?>  &nbsp;
                    <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu ul_sel_box">
                    <?php foreach($sel_status as $sel){?>
                        <li>
                            <a href="#" onclick="creative_status_change('<?php echo $sel['code_key'];?>','<?php echo $creative_detail['cre_no'];?>')">
                                <font color="<?php if($sel['code_key']=="1"){echo "#ffa200";}elseif($sel['code_key']=="2"){echo "6ae6ce";}elseif($sel['code_key']=="3"){echo "#5177bc";}else{echo "#e2918f";}?>">
                                    <?php echo lang($sel['code_desc']);?>
                                </font>
                            </a>
                        </li>
                    <?php }?>
                    <li>
                        <a href="#" onclick="creative_status_change('del','all')">
                            <?php echo lang('strDelete');?>
                        </a>
                    </li>
                    <li>
                        <a href="#" onclick="creative_status_change('copy','all')">
                            <?php echo lang('strCopy');?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <form name="creative_detail" method="get">
            <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
            <input type="hidden" name="cre_gp_no" value="<?php echo $cre_gp_no;?>">
            <input type="hidden" name="cre_no" value="<?php echo $cre_no;?>">
            <input type="hidden" name="status_key" >
            <input type="hidden" name="fromto_date" id="fromto_date" value="<?php echo $fromto_date;?>">
            <table class="mng_98_tb">
                <colgroup>
                    <col width="15%">
                    <col width="15%">
                    <col width="8%">
                    <col width="8%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="8%">
                    <col width="8%">
                    <col width="8%">
                </colgroup>
                <tr>
                    <th><?php echo lang('strAd')?><?php echo lang('strName')?></th>
                    <th><?php echo lang('strMaterial')?></th>
                    <th><?php echo lang('strStatus')?></th>
                    <th><?php echo lang('strScreening')?></th>
                    <th><?php echo lang('strType')?></th>
                    <th><?php echo lang('strDailyBudget')?></th>
                    <th><?php echo lang('strBiddingPrice')?></th>
                    <th><?php echo lang('strImpressions')?></th>
                    <th><?php echo lang('strClicks')?></th>
                    <th>CTR</th>
               </tr>
                <tr>
                    <td class="txt_center pa_l0">
                        <?php echo $creative_detail['cre_nm'];?>
                    </td>
                    <td>
                        <?php 
                        if($creative_detail['cre_type'] == 4)
                        {
                        ?>
                            <img src="/img/auction/img_auction_view<?=str_replace($creative_detail['auction'], '1', '')?>.png" width="100px" height="100px"><br />
                            <?php echo $creative_detail['cre_width'];?> X <?php echo $creative_detail['cre_height'];?><br />
                            <?php echo lang($creative_detail['cre_type_desc']);?> <?php echo lang('strCr');?>
                        <?php }else{?>
                            <img src="<?php echo IMAGE_BASE_URL.$creative_detail['cre_img_link'];?>" width="100px" height="100px"><br />
                            <?php echo $creative_detail['cre_width'];?> X <?php echo $creative_detail['cre_height'];?><br />
                            <?php echo lang($creative_detail['cre_type_desc']);?> <?php echo lang('strCr');?>
                        <?php }?>
                    </td>
                    <td>
                        <div class="btn-group"> 
                            <span class="cre_gp_span" data-toggle="dropdown" id="cre_gp_status_sel">
                                <font color="<?php if ($creative_detail['cre_status'] == "1"){echo "#ffa200";}elseif ($creative_detail['cre_status'] == "2"){echo "6ae6ce";}elseif ($creative_detail['cre_status'] == "3"){echo "#5177bc";}else{echo "#e2918f";}?>">
                                    <?php echo lang($creative_detail['cre_status_desc'])?>
                                 </font>
                            </span>
                        </div>
                    </td>
                    <td><?php echo lang($creative_detail['cre_evaluation_desc']);?></td>
                    <td>
                        <?php echo lang($creative_detail['cre_gp_type_desc']);?>
                    </td>
                    <td class="txt-right">￦ <?php echo number_format($creative_detail['daily_budget']);?></td>
                    <td class="txt-right">￦ <?php echo number_format($creative_detail['bid_loc_price']);?></td>
                    <td class="txt-right"><?php echo number_format($creative_detail['imp'])?></td>
                    <td class="txt-right"><?php echo number_format($creative_detail['clk'])?></td>
                    <td class="txt-right"><?php echo number_format($creative_detail['ctr'], 2)?> %</td>
               </tr>
            </table>
        </form>
        <form name="creative_modify" action="/creative/creative_modify" method="post">
            <input type="hidden" name="cre_no">
        </form>
    </div>
</div>

<script type="text/javascript">

function modal_view( size, url, cont, img) {
    $("#modal_size").append(size);
    $("#modal_url").append(url);
    $("#modal_cont").append(cont);
    $("#modal_img").append(img);
    $("#modal1").modal();
    $("#modal1").modal({ keyboard: false });
    $("#modal1").modal('show');
}


function get_chart(){
	$.ajax({
        url : "/creative/creative_detail_chart/",
        dataType : "html",
        beforeSend: function() {
            //통신을 시작할때 처리
            $('#report').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data:{
            mem_no : document.creative_detail.mem_no.value,
            cre_no : document.creative_detail.cre_no.value,
            fromto_date : document.creative_detail.fromto_date.value,
            },

        success : function(result){
            $("#report").html(result);
        }
    });
}

$(function () {
    get_chart();
});

function creative_status_change(status_key,cre_no){
	if(status_key=="del"){
		if(confirm("<?php echo lang('strStatusChangeAlert');?>")){
			var frm=document.creative_detail;
        	frm.action="/creative/sel_creative_delete";
        	frm.status_key.value=status_key;
        	frm.cre_no.value=cre_no;
        	frm.submit();
		}
	}else if(status_key=="copy"){
		new_creative_copy();	
	}else{
	    if(confirm("<?php echo lang('strStatusChangeAlert1');?>")){
        	var frm=document.creative_detail;
        	frm.action="/creative/sel_creative_status_change";
        	frm.status_key.value=status_key;
        	frm.cre_no.value=cre_no;
        	frm.submit();
		}else{
			alert("<?php echo lang('strStatusChangeAlert2');?>");
		}
	}
}
function search(){
	var frm=document.creative_detail;
	frm.action="/creative/creative_detail";
	frm.fromto_date.value=$("#daterange").val();
	frm.submit();
}

function creative_add(){
	frm=document.creative_detail;
	frm.action="/creative/creative_add";
	frm.submit();	
}

</script>
<?php 
    $mem_no = $this->session->userdata('mem_no');
    $run_sum_imp = 0;
    $run_sum_clk = 0;
    $run_sum_ctr = 0;
    $ready_sum_imp = 0;
    $ready_sum_clk = 0;
    $ready_sum_ctr = 0;
    $pause_sum_imp = 0;
    $pause_sum_clk = 0;
    $pause_sum_ctr = 0;
    $done_sum_imp = 0;
    $done_sum_clk = 0;
    $done_sum_ctr = 0;
    if (isset($creative_group_list)){
        foreach ($creative_group_list as $row){
            if ($row['cre_gp_status'] == "1"){
                $run_sum_imp += $row['imp'];
                $run_sum_clk += $row['clk'];
            }
            
            if ($row['cre_gp_status'] == "2"){
                $ready_sum_imp += $row['imp'];
                $ready_sum_clk += $row['clk'];
            }
            
            if ($row['cre_gp_status'] == "3" || $row['cre_gp_status'] == "5" || $row['cre_gp_status'] == "6"){
                $pause_sum_imp += $row['imp'];
                $pause_sum_clk += $row['clk'];
            }
            
            if ($row['cre_gp_status'] == "4"){
                $done_sum_imp += $row['imp'];
                $done_sum_clk += $row['clk'];
            }
        }
    }
    if ($run_sum_clk != "0"){
        $run_sum_ctr = $run_sum_clk / $run_sum_imp * 100;
    }
    if ($ready_sum_clk != "0"){
        $ready_sum_ctr = $ready_sum_clk / $ready_sum_imp * 100;
    }
    if ($pause_sum_clk != "0"){
        $pause_sum_ctr = $pause_sum_clk / $pause_sum_imp * 100;
    }
    if ($done_sum_clk != "0"){
        $done_sum_ctr = $done_sum_clk / $done_sum_imp * 100;
    }
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAdMgr')?></a></li>
			<li class="active"><?php echo lang('strAdGroup')?></li>
		</ol>
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strAdGroup')?></h3>
	</div>
	<div class="panel-body">
     <div class="clear">
	<!-- seonu -->
    <div class="camp_rept">
        <div class="camp_rept_his">
            <span class="f-color-g font_bold"><i class="fa fa-reorder"></i> &nbsp;<a href="/report/report_operation"><?php echo lang('strOperationReport')?> <?php echo lang('strReport')?> <?php echo lang('strView')?></a></span>
        </div>
        <div class="camp2 cursor2" >
			<div class="camp_div2" id="sel_all" onclick="select_status('all');">
				<p class="camp_tit"><?php echo lang('strAll')?></p> 
				<span class="camp_con"><?php echo $creative_group_list_count['all_cnt'];?><?php echo lang('strEa');?></span>
			</div>
			<div class="camp_div2" id="sel_run" onclick="select_status('run');">
				<p class="camp_tit"><?php echo lang('strRun')?></p>
				<span class="camp_con"><?php echo $creative_group_list_count['run_cnt'];?><?php echo lang('strEa');?></span>
			</div>
			<div class="camp_div2" id="sel_ready" onclick="select_status('ready');">
				<p class="camp_tit"><?php echo lang('strReady')?></p>
				<span class="camp_con"><?php echo $creative_group_list_count['ready_cnt'];?><?php echo lang('strEa');?></span>
			</div>		    				
			<div class="camp_div2" id="sel_pause" onclick="select_status('pause');">
				<p class="camp_tit"><?php echo lang('strPause')?></p>
				<span class="camp_con"><?php echo $creative_group_list_count['pause_cnt'];?><?php echo lang('strEa');?></span>
			</div>		    				
			<div class="camp_div2" id="sel_done" onclick="select_status('done');">
				<p class="camp_tit"><?php echo lang('strDone')?></p>
				<span class="camp_con"><?php echo $creative_group_list_count['done_cnt'];?><?php echo lang('strEa');?></span>
			</div>		    					
		</div>
		
		<!-- //seonu -->
        <div class="camp_info hei_45">
            <div id="demo-dp-component" class="camp_info_r2">
                <input type="text" name="daterange" id="daterange" class="range float_l ma_r10 wid_70p" value="<?php echo $fromto_date?>" />
                <span class="iput-group-addon float_l ma_t8">
                    <i class="fa fa-calendar fa-lg" onclick="$('#daterange').focus();"></i>
                </span>
                <a href="javascript:search();" class="float_l btn btn-primary ma_l10"><?php echo lang('strSearch')?></a>
            </div>
        </div>
    </div>

	   <div class="clear bd_eb" id="report">
       </div>
      <div class="float_l wid_100p">
        <div class="float_l pa_t20 ma_b3" id="all_btn_gp">
            <a href="javascript:creative_group_add('<?php echo $cam_no?>');" class="btn btn-primary float_l"><?php echo lang('strNewAdGroup')?></a>
            <div class="btn-group float_l ma_l5">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="cre_gp_status_sel">
                    &nbsp;  <?php echo lang('strModify')?>  &nbsp;
                    <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu ul_sel_box">
                    <?php 
                        foreach($sel_status as $sel){
                            if($sel['code_key'] != 5 && $sel['code_key'] != 6){
                    ?>
                        <li>
                            <a href="javascript:void(0);" onclick="creative_group_status_change('<?php echo $sel['code_key'];?>','all')">
                                <font class="bold" color="<?php if ($sel['code_key'] == "1"){echo "#97cd19";}elseif ($sel['code_key'] == "2"){echo "#ffa200";}elseif ($sel['code_key'] == "3"){echo "#e2918f";}else{echo "#5177bc";}?>">
                                    <?php echo lang($sel['code_desc']);?>
                                </font>
                            </a>
                        </li>
                    <?php 
                            }
                        }
                    ?>
                    <li>
                        <a href="javascript:void(0);" onclick="creative_group_status_change('del','all')">
                            <?php echo lang('strDelete');?>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" onclick="creative_group_status_change('copy','all')">
                            <?php echo lang('strCopy');?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <span class="btn btn-primary ma_t20 float_r" onClick ="excel_down();"><?php echo lang('strDownload')?></span>
    </div>
    <div class="pa_b20 clear">
        <form name="creative_group_list" method="post">
            <input type="hidden" name="all" id="all" value="<?php echo $all?>">
            <input type="hidden" name="run" id="run" value="<?php echo $run?>">
            <input type="hidden" name="ready" id="ready" value="<?php echo $ready?>">
            <input type="hidden" name="pause" id="pause" value="<?php echo $pause?>">
            <input type="hidden" name="done" id="done" value="<?php echo $done?>">
            <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
            <input type="hidden" name="cre_gp_no" >
            <input type="hidden" name="cam_no" value="<?php echo $cam_no?>">
            <input type="hidden" name="status_key" >
            <input type="hidden" name="fromto_date" id="fromto_date" value="<?php echo $fromto_date;?>">
            <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="4%">
                    <col width="16%">
                    <col width="11%">
                    <col width="10%">
                    <col width="8%">
                    <col width="11%">
                    <col width="10%">
                    <col width="7%">
                    <col width="7%">
                    <col width="7%">
                </colgroup>
                <thead>
                    <tr>
                        <th class="center wid_20"><input type="checkbox" id="allCheck" onclick="all_check()"></th>
                        <th><?php echo lang('strAdGroup')?></th>
                        <th><?php echo lang('strCampaign')?></th>
                        <th><?php echo lang('strPeriod')?></th>
                        <th><?php echo lang('strStatus')?></th>
                        <th><?php echo lang('strDailyBudget')?></th>
                        <th><?php echo lang('strBiddingPrice')?></th>
                        <th><?php echo lang('strImpressions')?></th>
                        <th><?php echo lang('strClicks')?></th>
                        <th>CTR</th>
                   </tr>
                </thead>
                <tbody>
                    <?php if (isset($creative_group_list)){?>
                        <?php foreach ($creative_group_list as $row){?>
                            <tr>
                                <td class="center wid_20"><input type="checkbox" name="sel_creative_group[]" id="sel_creative_group" value="<?php echo $row['cre_gp_no']?>"></td>
                                <td onmouseover="show_btn('<?php echo $row['cre_gp_no']?>');" onmouseout="hide_btn('<?php echo $row['cre_gp_no']?>');">
                                    <span id="cre_gp_nm_view_<?php echo $row['cre_gp_no']?>">
                                        <a href="javascript:void(0);" onclick="creative_list('<?php echo $row['cre_gp_no']?>')">
                                            <span id="cre_gp_nm_<?php echo $row['cre_gp_no']?>"><?php echo $row['cre_gp_nm']?>
                                            <!-- (<?php echo lang($row['cre_gp_type'])?>) -->
                                        </a>
                                        <div class="btn-group float_r ma_r10p cursor" id="cre_gp_btn_group_<?php echo $row['cre_gp_no']?>" style="display:none;">
                                            <a class="dropdown-caret fa fa-bar-chart cursor add-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="리포트 보기" href="/report/report_operation" ></a>
                                            <i class="dropdown-caret fa fa-gear cursor add-tooltip" data-toggle="dropdown" data-placement="bottom" data-original-title="광고그룹 수정" id="cre_gp_status_sel">
                                            </i>
                                            <ul class="dropdown-menu ul_sel_box" style="top:5px;left:5px;">
                                                <li>
                                                    <a href="javascript:creative_group_modify('cre_gp','<?php echo $row['cre_gp_no']?>');" >
                                                        <?php echo lang('strAdGroup');?><?php echo lang('strModify');?>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:creative_group_modify('targeting','<?php echo $row['cre_gp_no']?>');" >
                                                        <?php echo lang('strTargeting');?><?php echo lang('strModify');?>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </span>
                                </td>
                                <td><?php echo $row['cam_nm']?></td>
                                <td>
                                    <?php echo substr($row['start_ymd'],0,10)?>
                                    <br />~
                                    <?php if (substr($row['end_ymd'],0,10) == "0000-00-00" || $row['end_ymd'] == ""){echo "한도없음";}else{ echo substr($row['end_ymd'], 0, 10);}?></td>
                                <td class="txt_center">
                                    <div class="btn-group">
                                        <span class="cre_gp_span" data-toggle="dropdown" id="cre_gp_status_sel">
                                            <font class="bold" color="<?php if ($row['cre_gp_status'] == "1"){echo "#69ae30";}elseif ($row['cre_gp_status'] == "2"){echo "fe9a00";}elseif ($row['cre_gp_status'] == "3"){echo "#c85757";}else{echo "#5177bc";}?>">
                                                <?php echo lang($row['cre_gp_status_desc'])?>
                                             </font>
                                            <i class="dropdown-caret fa fa-caret-down ma_t5 ma_l10 cursor"></i>
                                        </span>
                                        <ul class="dropdown-menu ul_sel_box">
                                            <?php 
                                                foreach($sel_status as $sel){
                                                    if($sel['code_key'] != 5 && $sel['code_key'] != 6){
                                            ?>
                                                <li>
                                                    <a href="javascript:void(0);" onclick="creative_group_status_change('<?php echo $sel['code_key'];?>','<?php echo $row['cre_gp_no']?>')">
                                                        <font class="bold" color="<?php if ($sel['code_key'] == "1"){echo "#69ae30";}elseif ($sel['code_key'] == "2"){echo "fe9a00";}elseif ($sel['code_key'] == "3"){echo "#c85757";}else{echo "#5177bc";}?>">
                                                            <?php echo lang($sel['code_desc']);?>
                                                        </font>
                                                    </a>
                                                </li>
                                            <?php 
                                                    }
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                </td>
                                <td class="txt-right">
                                    <?php if ($row['daily_budget'] == "" || $row['daily_budget'] == "0"){?>
                                        <?php echo lang('strNone');?>
                                    <?php }else{?>
                                        <?php echo "￦".number_format($row['daily_budget']);?>
                                    <?php }?>
                                </td>
                                <td class="txt-right">
                                    <span id="cre_gp_bid_price_view_<?php echo $row['cre_gp_no']?>">
                                        <?php
                                            if ($row['bid_cur'] == "USD"){
                                                $bid_unit = "$";
                                                $bid_price = number_format($row['bid_price']);
                                            }elseif ($row['bid_cur'] == "KRW"){
                                                $bid_unit = "￦";
                                                $bid_price = number_format($row['bid_loc_price']);
                                            }

                                            echo $bid_unit;
                                        ?>
                                        <a href="javascript:void(0);" onclick="creative_list('<?php echo $row['cre_gp_no']?>')">
                                            <span id="cre_gp_bid_price_<?php echo $row['cre_gp_no']?>">
                                                <?php
                                                    echo $bid_price;
                                                ?>
                                            </span>
                                        </a>
                                        <!-- 
                                        <div class="btn-group float_r ma_r10p cursor" id="modi_bid_btn" style="display:none;">
                                            <a class="dropdown-caret fa fa-edit cursor" onclick="cre_gp_bid_price_modi('<?php echo $row['cre_gp_no']?>');" ></a>
                                        </div>
                                         -->
                                    </span>
                                    <span id="cre_gp_bid_price_modi_<?php echo $row['cre_gp_no']?>" style="display:none;">
                                        <?php
                                            if($row['bid_cur'] == "USD"){
                                        ?>
                                        <input type="text" class="s_txt_box" id="modi_cre_gp_bid_price_<?php echo $row['cre_gp_no']?>" name="cre_gp_nm" value="<?php echo $bid_price?>" class="wid_50p">
                                        <?php
                                            }elseif($row['bid_cur'] == "KRW"){
                                        ?>
                                        <input type="text" class="s_txt_box" id="modi_cre_gp_bid_price_<?php echo $row['cre_gp_no']?>" onkeyup="inputNumberFormat(this);" name="cre_gp_nm" value="<?php echo $bid_price?>" class="wid_50p">
                                        <?php
                                            }
                                        ?>
                                        <span class="fa fa-check color_g cursor" onclick="cre_gp_bid_price_modi_save('<?php echo $row['cre_gp_no']?>', '<?php echo $row['bid_cur']?>');"></span>
                                        <span class="fa fa-close color_r cursor" onclick="cre_gp_bid_price_modi('<?php echo $row['cre_gp_no']?>');"></span>
                                    </span>
                                </td>
                                <td class="txt-right"><?php echo number_format($row['imp'])?></td>
                                <td class="txt-right"><?php echo number_format($row['clk'])?></td>
                                <td class="txt-right"><?php echo number_format($row['ctr'],2)?> %</td>
                           </tr>
                       <?php }?>
                   <?php }?>
               </tbody>
               <tfoot>
               <tr class="even" id="run_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold run"><?php echo lang('strRun')?></span> <?php echo lang('strAdGroup')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($run_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($run_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($run_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even" id="ready_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold ready"><?php echo lang('strReady')?></span> <?php echo lang('strAdGroup')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($ready_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($ready_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($ready_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even" id="pause_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold pause"><?php echo lang('strPause')?></span> <?php echo lang('strAdGroup')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($pause_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($pause_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($pause_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even" id="done_sum_tr" style="display:none">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <?php echo lang('strOnly')?> <span class="bold done"><?php echo lang('strDone')?></span> <?php echo lang('strAdGroup')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($done_sum_imp)?></td>
                    <td class="txt-right"><?php echo number_format($done_sum_clk)?></td>
                    <td class="txt-right"><?php echo number_format($done_sum_ctr,2)?> %</td>
               </tr>
               <tr class="even">
                    <td></td>
                    <td><?php echo lang('strSum')?> - <span class="bold adall"><?php echo lang($sum_creative_group_list['row_nm'])?></span> <?php echo lang('strAdGroup')?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="txt-right"><?php echo number_format($sum_creative_group_list['imp'])?></td>
                    <td class="txt-right"><?php echo number_format($sum_creative_group_list['clk'])?></td>
                    <td class="txt-right"><?php echo number_format($sum_creative_group_list['ctr'], 2)?> %</td>
               </tr>
            </tfoot>     
            </table>
            <div class="center hei_35">
                <div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box_pa" >
                        <li><a href="javascript:page_change(10)">10</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(100)">100</a></li>
                    </ul>
                </div>
                <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div>
        </form>
        <form name="creative_group_modify" action="/creative/creative_group_modify_form" method="get">
            <input type="hidden" name="cre_gp_no">
            <input type="hidden" name="cam_no" value="<?php echo $cam_no?>">
        </form>
		</div>
    </div>
</div>
</div>


        
<script type="text/javascript">
$(document).ready(function() {
    if(($('#all').val()=="Y")||($('#ready').val()=="Y"&&$('#run').val()=="Y"&&$('#pause').val()=="Y"&&$('#done').val()=="Y")){
        $('#all').attr('value','Y');
        $('#run').attr('value','Y');
        $('#ready').attr('value','Y');
        $('#pause').attr('value','Y');
        $('#done').attr('value','Y');
        $('#sel_all').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
        $('#sel_run').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
        $('#sel_ready').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
        $('#sel_pause').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
        $('#sel_done').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
        $('#all_img').attr('src','/img/radio_on.png');
        $('#run_img').attr('src','/img/radio_on_g.png');
        $('#ready_img').attr('src','/img/radio_on_o.png');
        $('#pause_img').attr('src','/img/radio_on_r.png');
        $('#done_img').attr('src','/img/radio_on_b2.png');
        $('#run_sum_tr').show();
        $('#ready_sum_tr').show();
        $('#pause_sum_tr').show();
        $('#done_sum_tr').show();
    }else{
    	$('#sel_all').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
        if($('#run').val()=="Y"){
            $('#run').attr('value','Y');
            $('#sel_run').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
            $('#run_img').attr('src','/img/radio_on_g.png');
            $('#run_sum_tr').show();
        }else{
            $('#run').attr('value','N');
            $('#sel_run').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#run_img').attr('src','/img/radio_off.png');
            $('#run_sum_tr').hide();
        }
        if($('#ready').val()=="Y"){
            $('#ready').attr('value','Y');
            $('#sel_ready').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
            $('#ready_img').attr('src','/img/radio_on_o.png');
            $('#ready_sum_tr').show();
        }else{
            $('#ready').attr('value','N');
            $('#sel_ready').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#ready_img').attr('src','/img/radio_off.png');
            $('#ready_sum_tr').hide();
        }
        if($('#pause').val()=="Y"){
            $('#pause').attr('value','Y');
            $('#sel_pause').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
            $('#pause_img').attr('src','/img/radio_on_r.png');
            $('#pause_sum_tr').show();
        }else{
            $('#pause').attr('value','N');
            $('#sel_pause').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#pause_img').attr('src','/img/radio_off.png');
            $('#pause_sum_tr').hide();
        }
        if($('#done').val()=="Y"){
            $('#done').attr('value','Y');
            $('#sel_done').css({"padding-bottom":"1px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
            $('#done_img').attr('src','/img/radio_on_b2.png');
            $('#done_sum_tr').show();
        }else{
            $('#done').attr('value','N');
            $('#sel_done').css({"padding-bottom":"2px","background":"#fff","border-bottom":"1px solid #ddd"});
            $('#done_img').attr('src','/img/radio_off.png');
            $('#done_sum_tr').hide();
        }
    }
});

    $(document).ready(function() {
        if (($('#all').val()=="Y")||($('#ready').val()=="Y"&&$('#run').val()=="Y"&&$('#pause').val()=="Y"&&$('#done').val()=="Y")){
            $('#all').attr('value','Y');
            $('#run').attr('value','Y');
            $('#ready').attr('value','Y');
            $('#pause').attr('value','Y');
            $('#done').attr('value','Y');
            $('#all_img').attr('src','/img/radio_on.png');
            $('#run_img').attr('src','/img/radio_on_g.png');
            $('#ready_img').attr('src','/img/radio_on_o.png');
            $('#pause_img').attr('src','/img/radio_on_r.png');
            $('#done_img').attr('src','/img/radio_on_b2.png');
            $('#run_sum_tr').show();
            $('#ready_sum_tr').show();
            $('#pause_sum_tr').show();
            $('#done_sum_tr').show();
        }else{
            if ($('#run').val()=="Y"){
                $('#run').attr('value','Y');
                $('#run_img').attr('src','/img/radio_on_g.png');
                $('#run_sum_tr').show();
            }else{
                $('#run').attr('value','N');
                $('#run_img').attr('src','/img/radio_off.png');
                $('#run_sum_tr').hide();
            }
            if ($('#ready').val()=="Y"){
                $('#ready').attr('value','Y');
                $('#ready_img').attr('src','/img/radio_on_o.png');
                $('#ready_sum_tr').show();
            }else{
                $('#ready').attr('value','N');
                $('#ready_img').attr('src','/img/radio_off.png');
                $('#ready_sum_tr').hide();
            }
            if ($('#pause').val()=="Y"){
                $('#pause').attr('value','Y');
                $('#pause_img').attr('src','/img/radio_on_r.png');
                $('#pause_sum_tr').show();
            }else{
                $('#pause').attr('value','N');
                $('#pause_img').attr('src','/img/radio_off.png');
                $('#pause_sum_tr').hide();
            }
            if ($('#done').val()=="Y"){
                $('#done').attr('value','Y');
                $('#done_img').attr('src','/img/radio_on_b2.png');
                $('#done_sum_tr').show();
            }else{
                $('#done').attr('value','N');
                $('#done_img').attr('src','/img/radio_off.png');
                $('#done_sum_tr').hide();
            }
        }
    });

    function show_btn(cre_gp_no){
    	$("#cre_gp_btn_group_"+cre_gp_no).show();
    }
    
    function hide_btn(cre_gp_no){
    	$("#cre_gp_btn_group_"+cre_gp_no).hide();
    }
    
    function select_status(kind){
        if (kind=="all"){
            if ($('#all').val()=="Y"){
                $('#all').attr('value','N');
                $('#run').attr('value','Y');
                $('#ready').attr('value','N');
                $('#pause').attr('value','N');
                $('#done').attr('value','N');
                $('#all_img').attr('src','/img/radio_off.png');
                $('#run_img').attr('src','/img/radio_on_g.png');
                $('#ready_img').attr('src','/img/radio_off.png');
                $('#pause_img').attr('src','/img/radio_off.png');
                $('#done_img').attr('src','/img/radio_off.png');
            }else{
                $('#all').attr('value','Y');
                $('#run').attr('value','Y');
                $('#ready').attr('value','Y');
                $('#pause').attr('value','Y');
                $('#done').attr('value','Y');
                $('#all_img').attr('src','/img/radio_on.png');
                $('#run_img').attr('src','/img/radio_on_g.png');
                $('#ready_img').attr('src','/img/radio_on_o.png');
                $('#pause_img').attr('src','/img/radio_on_r.png');
                $('#done_img').attr('src','/img/radio_on_b2.png');

            }
        }
        if (kind=="run"){
            if ($('#run').val()=="Y"){
                $('#run').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#run_img').attr('src','/img/radio_off.png');
            }else{
                $('#run').attr('value','Y');
                $('#run_img').attr('src','/img/radio_on_g.png');
            }
        }
        if (kind=="ready"){
            if ($('#ready').val()=="Y"){
                $('#ready').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#ready_img').attr('src','/img/radio_off.png');
            }else{
                $('#ready').attr('value','Y');
                $('#ready_img').attr('src','/img/radio_on_o.png');
            }
        }
        if (kind=="pause"){
            if ($('#pause').val()=="Y"){
                $('#pause').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#pause_img').attr('src','/img/radio_off.png');
            }else{
                $('#pause').attr('value','Y');
                $('#pause_img').attr('src','/img/radio_on_b2.png');
            }
        }
        if (kind=="done"){
            if ($('#done').val()=="Y"){
                $('#done').attr('value','N');
                if ($('#all').val()=='Y'){
                    $('#all').attr('value','N');
                    $('#all_img').attr('src','/img/radio_off.png');
                }
                $('#done_img').attr('src','/img/radio_off.png');
            }else{
                $('#done').attr('value','Y');
                $('#done_img').attr('src','/img/radio_on_b2.png');
            }
        }
        search();
    }
    function search(){
        var frm = document.creative_group_list;
        frm.action = "/creative/creative_group_list";
        frm.fromto_date.value=$("#daterange").val();
        frm.submit();
    }

    //페이징 스크립트 시작
    function page_change(row){
        frm = document.creative_group_list;
        frm.per_page.value = row;
        frm.submit();
    }

    function paging(number){
        var frm = document.creative_group_list;
        frm.action = "/creative/creative_group_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    function get_chart(){

        $.ajax({
            url : "/creative/creative_group_chart/",
            dataType : "html",
            beforeSend: function() {
                //통신을 시작할때 처리
                $('#report').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data:{
                mem_no : document.creative_group_list.mem_no.value,
                cam_no : document.creative_group_list.cam_no.value,
                fromto_date : document.creative_group_list.fromto_date.value,
                all : document.creative_group_list.all.value,
                run : document.creative_group_list.run.value,
                ready : document.creative_group_list.ready.value,
                pause : document.creative_group_list.pause.value,
                done : document.creative_group_list.done.value
                },

            success : function(result){
                $("#report").html(result);
            }
        });
    }

    $(function () {
        get_chart();
    });

    function all_check(){
        if ($("#allCheck").prop("checked")) {
           $("input[id=sel_creative_group]").prop("checked", true);
          } else {
           $("input[id=sel_creative_group]").prop("checked", false);
          }
    }

    function creative_group_status_change(status_key,cre_gp_no){
        if ($(":checkbox[name='sel_creative_group[]']:checked").length<1&&cre_gp_no=="all"){
            alert("<?php echo lang('strAdGroupSelect');?>");
        }else{
            if (status_key=="del"){
                if (confirm("<?php echo lang('strAdGroupDelete');?>")){
                    var frm = document.creative_group_list;
                    frm.action="/creative/sel_creative_group_delete";
                    frm.status_key.value=status_key;
                    frm.cre_gp_no.value=cre_gp_no;
                    frm.submit();
                }
            }else if (status_key=="copy"){
                new_creative_group_copy();
            }else{
                if (confirm("<?php echo lang('strAdGroupStatusChange');?>")){
                    var frm=document.creative_group_list;
                    frm.action="/creative/sel_creative_group_status_change";
                    frm.status_key.value=status_key;
                    frm.cre_gp_no.value=cre_gp_no;
                    frm.submit();
                }else{
                    alert("<?php echo lang('strStatusChangeAlert2');?>");
                }
            }
        }
    }

    function new_creative_group_copy(){
        if ($(":checkbox[name='sel_creative_group[]']:checked").length>1){
            alert("<?php echo lang('strAdGroupCopy');?>");
        }else{
            if (confirm("<?php echo lang('strAdGroupSelectCopy');?>")){
                var frm = document.creative_group_list;
                frm.action="/creative/new_creative_group_copy";
                frm.submit();
            }else{
                alert("<?php echo lang('strAdGroupCopyCancel');?>");
            }
        }
    }
    
    function creative_group_add(cam_no){
        if (cam_no == ""){
            alert("<?php echo lang('strAdGroupAlert');?>");
        }else{
            frm=document.creative_group_list;
            frm.action="/creative/creative_group_add_form/?cam_no="+cam_no;
            frm.cam_no.value = cam_no;
            frm.submit();
        }
    }

    function creative_group_modify(kind,cre_gp_no){
        frm = document.creative_group_modify;
        if (kind == "cre_gp"){
            frm.action="/creative/creative_group_modify_form";
        }else{
            frm.action="/admanagement/targeting_modify_form";
        }
        frm.cre_gp_no.value = cre_gp_no;
        frm.submit();
    }

    function creative_list(cre_gp_no){
        var frm = document.creative_group_list;
        frm.action = "/creative/creative_list/?cre_gp_no="+cre_gp_no;
        frm.cre_gp_no.value = cre_gp_no;
        frm.submit();

    }

    function cre_gp_nm_modi(cre_gp_no){
        if ($("#cre_gp_nm_view_"+cre_gp_no).css('display')=="none"){
            $("#cre_gp_nm_view_"+cre_gp_no).show();
            $("#cre_gp_nm_modi_"+cre_gp_no).hide();
        }else{
            $("#cre_gp_nm_view_"+cre_gp_no).hide();
            $("#cre_gp_nm_modi_"+cre_gp_no).show();
        }
    }

    function cre_gp_name_modi_save(cre_gp_no){
        var cre_gp_nm = $("#modi_cre_gp_nm_"+cre_gp_no).val()
        var url = '/creative/creative_group_name_modi_save';
        $.post(url,
            {
                cre_gp_no : cre_gp_no,
                cre_gp_nm : cre_gp_nm
            },
            function(data){
                if (data.trim()=="ok"){
                    alert("<?php echo lang('strAdGroupNameSave');?>");
                    $("#cre_gp_nm_view_"+cre_gp_no).show();
                    $("#cre_gp_nm_"+cre_gp_no).html(cre_gp_nm);
                    $("#cre_gp_nm_modi_"+cre_gp_no).hide();
                }else{
         
                }
            }
        );
    }

    function cre_gp_bid_price_modi(cre_gp_no){
        if ($("#cre_gp_bid_price_view_"+cre_gp_no).css('display')=="none"){
            $("#cre_gp_bid_price_view_"+cre_gp_no).show();
            $("#cre_gp_bid_price_modi_"+cre_gp_no).hide();
        }else{
            $("#cre_gp_bid_price_view_"+cre_gp_no).hide();
            $("#cre_gp_bid_price_modi_"+cre_gp_no).show();
        }
    }

    function cre_gp_bid_price_modi_save(cre_gp_no, bid_cur){
        var bid_price = $("#modi_cre_gp_bid_price_"+cre_gp_no).val();
        var url = '/creative/creative_group_bid_price_modi_save';

        if(bid_cur == "KRW"){
            bid_price = uncomma(bid_price);
        }

        $.post(url,
            {
                cre_gp_no : cre_gp_no,
                bid_price : bid_price,
                bid_cur : bid_cur
            },
            function(data){
                if (data.trim()=="ok"){
                  alert("<?php echo lang('strBudChange');?>");
                  $("#cre_gp_bid_price_view_"+cre_gp_no).show();
                  $("#cre_gp_bid_price_"+cre_gp_no).html(bid_price);
                  $("#cre_gp_bid_price_modi_"+cre_gp_no).hide();
                }else{
           
                }
            }
        );
    }
    
    function excel_down(){
    	var cnt = '<?php echo count($creative_group_list); ?>';
    	if (cnt > 0){
    		var frm = document.creative_group_list;
            frm.action = '/excel/cre_gp_excel_down';
            frm.submit();
        }else{
        	alert("<?php echo lang('strExcelDwon');?>");
        }
    	
    }
    
    $(document).ready(function() {
        /*
        $('#list').dataTable( {
            "dom": '<"toolbar"Tf>t',
            "order": [[ 1, "asc" ]],
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ]
        });
        */
        $("div.toolbar").append($("#all_btn_gp").html());
    });

    function inputNumberFormat(obj) {
        obj.value = comma(uncomma(obj.value));
    }

    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }
</script>
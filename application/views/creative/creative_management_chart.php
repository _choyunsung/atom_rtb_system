<?php
/*
$x = $_POST['x'];
$y = $_POST['y'];
$kind = $_POST['kind'];
$network_id = $_POST['network_id'];
$user_lang = $_POST['user_lang'];
$usd_money = $_POST['usd_money'];
$height = 110 + count($chart_data) * 20;

if($user_lang=="korean"){
    $money = "$";
} else {
    $money = "₩";
}
*/
?>
<div id="creative_management_chart" style="min-width: 170px; min-height: 240px; margin: auto"></div>
<script type="text/javascript">

$(function () {
    $('#creative_management_chart').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
            categories: [
                         <?php if(isset($chart_data)){?>
                             <?php foreach ($chart_data as $row){?>
                                <?php echo "'".$row['date_ymd']."',"?>
                             <?php }?>
                         <?php }?>
                         ],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
        	min: 0,
        	allowDecimals: false,
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '<?php echo lang("strClicks");?>',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
        	min: 0,
        	allowDecimals: false,
            title: {
                text: '<?php echo lang("strImpressions");?>',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            floating: false,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: '<?php echo lang("strImpressions");?>',
            type: 'column',
            yAxis: 1,
            data: [
                    <?php if(isset($chart_data)){?>
                        <?php foreach ($chart_data as $row){?>
                           <?php echo $row['imp'].","?>
                        <?php }?>
                    <?php }?>
                   ],
            tooltip: {
                valueSuffix: ''
            }

        }, {
            name: '<?php echo lang("strClicks");?>',
            type: 'spline',
            data: [
                    <?php if(isset($chart_data)){?>
                        <?php foreach ($chart_data as $row){?>
                           <?php echo $row['clk'].","?>
                        <?php }?>
                    <?php }?>
                   ],
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
});
</script>

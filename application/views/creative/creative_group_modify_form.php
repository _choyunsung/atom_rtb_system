<?php 
    $mem_no=$this->session->userdata('mem_no');
?>
<script type="text/javascript">
    $(function() {
        $(".date").datepicker({
            format:'yyyy-mm-dd',
            startDate:"today",
            todayHighlight: true
        });
        //auto_cal();
    });
    
</script>
<div class="boxed">
    <div id="content-container">
    	<div class="wrap">
    	    <div class="panel float_r wid_970 min_height">
                <div class="history_box">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#"><?php echo lang('strAdMgr')?></a></li>
                        <li class="active"><?php echo lang('strAdGroup')?><?php echo lang('strModify')?></li>
                    </ol>
                </div>
    			<div class="panel-heading">
    				<h3 class="page-header text-overflow tit_blit"><?php echo lang('strEditCreativeGroupInformation')?></h3>
    			</div>
        		<div class="panel-body">
            	    <div class="clear">
                        <form name="creative_group_modify" action="" method="post">
                            <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
                            <input type="hidden" name="cam_no" value="<?php echo $creative_group_info['cam_no'];?>">
                            <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                            <colgroup>
                            	<col width="20%">
                            	<col width="*">
                            </colgroup>
                                <tr>
                                    <th class="point_blue">* <?php echo lang('strAdGroup')?> <?php echo lang('strName')?></th>
                                    <td>
                                        <input type="text" name="cre_gp_nm" id="cre_gp_nm" value="<?php echo $creative_group_info['cre_gp_nm'];?>" class="wid_40p" onblur="input_count()"/>
                                        <?php echo lang('strCampaignTxtCount');?>
                                        <?php echo lang('strCampaignTxtInfo');?>
                                        <input type="hidden" id="nm_check_commit" name="nm_check_commit" value="Y"><br />
                                        <span id="nm_check_result" class="join_info ma_l20"></span>
                                        <!--
                                        <input type="text" name="cre_gp_nm" id="cre_gp_nm"  class="wid_40p" value="<?php echo $creative_group_info['cre_gp_nm'];?>" onkeyup="input_count()" onfocus="input_count()"/>
                                        <?php echo lang('strCampaignTxtCount');?>
                                        -->
                                    </td>
                                </tr>
                                <tr>
                                    <th class="point_blue">* <?php echo lang('strStart')?><?php echo lang('strTime')?></th>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default dropdown-toggle ma_r5" data-toggle="dropdown" id="start_sel_ymd">
                                            &nbsp;  <?php echo lang('strSelect')?>  &nbsp;
                                            <i class="dropdown-caret fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu ul_sel_box">
                                                <li><a href="javascript:sel_ymd('start','today');" ><?php echo lang('strToday')?></a></li>
                                                <li><a href="javascript:sel_ymd('start','tomorrow')"><?php echo lang('strTomorrow')?></a></li>
                                                <li><a href="javascript:sel_ymd('start','input')"><?php echo lang('strCustomRange')?></a></li>
                                            </ul>
                                        </div>
                                        <input class="date"  type="hidden" name="start_ymd" id="start_ymd"  >

                                    </td>
                                </tr>
                                <tr>
                                    <th class="point_blue">* <?php echo lang('strEnd')?><?php echo lang('strTime')?></th>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-default dropdown-toggle ma_r5" data-toggle="dropdown" id="end_sel_ymd">
                                            &nbsp;  <?php echo lang('strSelect')?>  &nbsp;
                                            <i class="dropdown-caret fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu ul_sel_box">
                                                <li><a href="javascript:sel_ymd('end','1month');" ><?php echo lang('str1Month')?></a></li>
                                                <li><a href="javascript:sel_ymd('end','nolimit')"><?php echo lang('strNoLimit')?></a></li>
                                                <li><a href="javascript:sel_ymd('end','input')"><?php echo lang('strCustomRange')?></a></li>
                                            </ul>
                                        </div>
                                        <input class="date" type="hidden" name="end_ymd" id="end_ymd" onchange="auto_cal()"/>
                                    </td>
                                </tr>
                                <!-- 
                                <tr>
                                    <th class="point_blue">* <?php echo lang('strAdGroup')?> <?php echo lang('strType')?></th>
                                    <td>
                                        <input type="radio" name="cre_gp_type" value="1" <?php if ($creative_group_info['cre_gp_type'] == "1"){echo "checked";}?>/> <?php echo lang('strPc');?>
                                        <input type="radio" class="ma_l10" name="cre_gp_type" value="2" <?php if ($creative_group_info['cre_gp_type'] == "2"){echo "checked";}?>/> <?php echo lang('strMobileWeb');?>
                                        <input type="radio" class="ma_l10" name="cre_gp_type" value="3" <?php if ($creative_group_info['cre_gp_type'] == "3"){echo "checked";}?>/> <?php echo lang('strMobileApp');?>
                                    </td>
                                </tr>
                                 -->
                                <tr>
                                    <th><?php echo lang('strDailyBudget')?></th>
                                    <td>
                                        <input type="radio" name="daily_budget_sel" onchange="avail_daily_budget('Y');" <?php if ($creative_group_info['daily_budget'] == "0"){echo "checked";}?>> <?php echo lang('strNone')?>
                                        <input type="radio" class="ma_l10" name="daily_budget_sel" onchange="avail_daily_budget('N');" <?php if ($creative_group_info['daily_budget'] != "0"){echo "checked";}?>> <?php echo lang('strSetBudget')?>
                                        <input type="text"name="daily_budget" id="daily_budget" value="<?php echo number_format($creative_group_info['daily_budget']);?>" <?php if ($creative_group_info['daily_budget'] == "0"){ echo "disabled";}?> onkeyup="inputNumberFormat(this); auto_cal();">
                                    </td>
                                </tr>
                                <tr>
                                    <th class="point_blue">* <?php echo lang('strBiddingPrice')?></th>
                                    <td>
                                        <input type="hidden" id="bid_type" name="bid_type" value="1">
                                        <input type="hidden" id="bid_cur" name="bid_cur" value="<?php echo $creative_group_info['bid_cur'];?>">
                                        <div class="btn-group">
                                            <button class="btn btn-default dropdown-toggle ma_r5" data-toggle="dropdown" id="sel_bid_type">
                                            &nbsp;  <?php if ($creative_group_info['bid_type'] == "1"){echo "CPM";}else{echo "CPC";}?>&nbsp;
                                            <i class="dropdown-caret fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu ul_sel_box">
                                                <li><a href="javascript:sel_bid_type('1')">CPM</a></li>
                                                <!--<li><a href="javascript:sel_bid_type('2')">CPC</a></li>-->
                                            </ul>
                                        </div>

                                        <?php
                                            if($creative_group_info['bid_cur'] == "KRW"){
                                                $bid_loc_price = number_format($creative_group_info['bid_loc_price']);
                                            }elseif($creative_group_info['bid_cur'] == "USD"){
                                                $bid_loc_price = number_format($creative_group_info['bid_price'], 2);
                                            }

                                        ?>
                                        <script type="text/javascript">
                                            $(function() {
                                                <?php
                                                    if($creative_group_info['bid_cur'] == "KRW"){
                                                ?>
                                                $("#bid_loc_price").keyup(function(){$("#bid_loc_price").val(comma(uncomma($("#bid_loc_price").val())));});
                                                <?php
                                                    }
                                                ?>
                                            });
                                        </script>
                                        <input type="text" name="bid_loc_price" class="float_l" id="bid_loc_price" value="<?php echo $bid_loc_price?>">
                                        <div class="btn-group  float_l">
                                            <button class="btn btn-default dropdown-toggle ma_l5" data-toggle="dropdown" id="sel_bid_cur">
                                                &nbsp;  <?php if($creative_group_info['bid_cur'] == "KRW"){echo "KRW";}else{echo "USD";}?>&nbsp;
                                                <i class="dropdown-caret fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu ul_sel_box" style="top:1px;left:5px;">
                                                <li><a href="javascript:sel_bid_cur('KRW')">KRW</a></li>
                                                <li><a href="javascript:sel_bid_cur('USD')">USD</a></li>
                                            </ul> 
                                         </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo lang('strTotalCharge') ?></th>
                                    <td>
                                        <span id="bid_fee_view"><?php echo number_format($creative_group_info['bid_fee']);?>원</span>
                                        <input type="hidden" id="bid_fee" name="bid_fee" />
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <p class="txt-right">
                        <a href="javascript:creative_group_modify();" class="btn btn-primary"><?php echo lang('strSave')?></a>
                        <a href="javascript:history.back();" class="btn btn-default"><?php echo lang('strCancel')?></a>
                    </p>
            	</div>	
            </div>	
            <form name="creative_add" method="post" action="/creative/creative_add">
                <input type="hidden" name="cre_gp_no">
                <input type="hidden" name="cam_no" value="<?php echo $creative_group_info['cam_no']?>">
            </form>
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function() {
    sel_ymd('start','input');
    sel_ymd('end','input');
    $("#start_ymd").val('<?php echo $creative_group_info['start_ymd'];?>');
    $("#end_ymd").val('<?php echo $creative_group_info['end_ymd'];?>');
    auto_cal();
});
    
    function number_format(num){
        var num_str = num.toString();
        var result = "";

        for(var i=0; i<num_str.length; i++){
            var tmp = num_str.length - (i+1);

            if(((i%3)==0) && (i!=0))    result = ',' + result;
            result = num_str.charAt(tmp) + result;
        }

        return result;
    }

    function sel_bid_type(type){

        if(type == "1"){
            var type_nm="CPM";
        }else{
            var type_nm="CPC";
        }

        $("#bid_type").attr('value',type);
        $("#sel_bid_type").empty();
        $("#sel_bid_type").append(type_nm+" &nbsp;<i class='dropdown-caret fa fa-caret-down'></i>");

    }
    function sel_ymd(kind, value){
        var today = new Date();
        var tmpDt = new Date();

        if(value == "today"){
            var val="<?php echo lang('strToday')?>";
            $("#start_ymd").attr('type','hidden');
        }else if(value == "tomorrow"){
            var val="<?php echo lang('strTomorrow')?>";
            $("#start_ymd").attr('type','hidden');
        }else if(value == "nolimit"){
            var val="<?php echo lang('strNoLimit')?>";
            $("#end_ymd").attr('type','hidden');
        }else if(value == "1month"){
            var val="<?php echo lang('str1Month')?>";
            $("#end_ymd").attr('type','hidden');
        }else{
            var val="<?php echo lang('strCustomRange');?>";
        }


        if (value == "today") {
            var strDateS = formatDt(today);
        }

        if (value == "tomorrow") {
            var tmpVal = tmpDt.setDate(tmpDt.getDate() + 1);
            tmpVal = new Date(tmpVal);
            var strDateS = formatDt(tmpVal);
        }

        if (value == "nolimit") {
            strDateS="nolimit";
            $("#bid_fee_view").empty();
            $("#bid_fee_view").append("<?php echo lang('strAdEndView');?>");
            $("#bid_fee").val('');
        }

        if (value == "1month") {
            var tmpVal = tmpDt.setMonth(tmpDt.getMonth() + 1);
            tmpVal = new Date(tmpVal);
            var strDateS = formatDt(tmpVal);
        }

        if(kind=="start"){
            if(value=="nolimit"){
            }else{
                $("#start_ymd").attr('type','text');
                $("#start_ymd").attr('readonly', true);
            }
            $("#start_sel_ymd").empty();
            $("#start_sel_ymd").append(val+" &nbsp;<i class='dropdown-caret fa fa-caret-down'></i>");
            $("#start_ymd").val(strDateS);
            auto_cal();
        }else{
        	if(value=="nolimit"){
            }else{
                $("#end_ymd").attr('type','text');
                $("#end_ymd").attr('readonly',true);
            }
            $("#end_sel_ymd").empty();
            $("#end_sel_ymd").append(val+" &nbsp;<i class='dropdown-caret fa fa-caret-down'></i>");
            $("#end_ymd").val(strDateS);
            auto_cal();
        }
    }
    function formatDt(dt) {
        var year = dt.getFullYear();
        var mon = (dt.getMonth() + 1) > 9 ? '' + (dt.getMonth() + 1) : '0' + (dt.getMonth() + 1);
        var day = dt.getDate() > 9 ? '' + dt.getDate() : '0' + dt.getDate();
        var val = year + '-' + mon + '-' + day;
        return val;
    }

    function sel_bid_cur(type){
        if(type=="USD"){
            var type_nm="USD";

            //$("#bid_loc_price").keyup(function(){alert(111)});
            $("#bid_loc_price").off('keyup');
        }else{
            var type_nm="KRW";
            $("#bid_loc_price").keyup(function(){$("#bid_loc_price").val(comma(uncomma($("#bid_loc_price").val())));});
        }
        $("#bid_cur").attr('value', type);
        $("#sel_bid_cur").empty();
        $("#sel_bid_cur").append(type_nm+" &nbsp;<i class='dropdown-caret fa fa-caret-down'></i>");

    }

    function input_count(){

        var limit_length = 15;
        var msg_length = 0;

        //String에 bytes() 함수 만들기
        String.prototype.bytes = function() {
            var msg = this;
            var cnt = 0;

            //한글이면 2, 아니면 1 count 증가
            for( var i=0; i< msg.length; i++) {
                cnt += (msg.charCodeAt(i) > 128 ) ? 1 : 1;
            }
            return cnt;
        }
        input_type_limit();
        //textarea에서 키를 입력할 때마다 동작
        msg_length = $("#cre_gp_nm").val().bytes();

        if( msg_length <= limit_length ) {
        	$("#input_count").css("color", "black");
            $("#input_count").html( msg_length );
        } else {
            $("#input_count").css("color", "red");
            $("#input_count").html( "15" );
            $("#cre_gp_nm").val($("#cre_gp_nm").val().substring(0, 15));
        }
        nm_check();
    }

    function nm_check(){
        var cam_no = '<?php echo $cam_no;?>';
        var cre_gp_no = '<?php echo $cre_gp_no;?>';
        $.ajax({
            type:"POST",
            url:"/creative/modify_creative_group_nm_check/",
            data : {cre_gp_nm : $("#cre_gp_nm").val(), cam_no : cam_no, cre_gp_no : cre_gp_no  },
            timeout : 1,
            async:false,
            cache : false,
            success: function (data){
                $("#nm_check_commit").val("N");
                switch(data.trim()){
                    case "true":
                        var show_args="<?php echo lang('strCreativeGroupCheckAlert');?>";
                        $("#nm_check_commit").val("Y");
                        break;
                    case "false":
                        var show_args="<?php echo lang('strCreativeGroupCheckAlert1');?>";
                        alert("<?php echo lang('strCreativeGroupCheck1');?>");
                        break;
                    case "none":
                        var show_args="<?php echo lang('strCreativeGroupCheckAlert2');?>";
                        alert("<?php echo lang('strCreativeGroupCheck2');?>");
                        break;
                }

                $('#nm_check_result').html(show_args);
            },
            error: function whenError(e){
                alert("code : " + e.status + "\r\nmessage : " + e.responseText);
            }
        });
    }

    function input_type_limit(){
        var objEvent = event.srcElement;
        //var numPattern = /^[a-zA-Z가-힣.,-]*$/;
        var numPattern = / [\[\]{}()<>?|`~!@#$%^&*+=;:\"'\\]/g;
        numPattern = objEvent.value.match(numPattern);

        if (numPattern != null) {
            alert("<?php echo lang('strCampaignTxtInfo2');?>");
            objEvent.value = objEvent.value.substr(0, objEvent.value.length - 1);
            objEvent.focus();
            return false;
        }
    }
    
    function avail_daily_budget(fl){
        if(fl == "N"){
            $("#daily_budget").attr("disabled",false);
            $("#daily_budget").css("background-color", "#FFFFFF");
            $("#daily_budget").val('');
        }else{
            $("#daily_budget").attr("disabled",true);
            $("#daily_budget").val('');
            $("#daily_budget").css("background-color", "#DDDDDD");
            $("#bid_fee_view").empty();
        }
    }


    function auto_cal(){
        var cam_daily_budget = '<?php echo $creative_group_info['cam_daily_budget']?>';
        var daily_budget= $("#daily_budget").val().replace(/[^\d]+/g, '');
        if (cam_daily_budget != '0' && cam_daily_budget < daily_budget){
            alert("광고그룹 일 허용예산이 캠페인 일 허용예산을 초과했습니다."); 
            $("#daily_budget").val('');
            $("#bid_fee_view").empty();
        }else{
            var iDay = 1000 * 60 * 60 * 24;
            var startDateArray = $("#start_ymd").val().split("-");
            var startDateObj = new Date(startDateArray[0], Number(startDateArray[1])-1, startDateArray[2]);
    
            var endDateArray = $("#end_ymd").val().split("-");
            var endDateObj = new Date(endDateArray[0], Number(endDateArray[1])-1, endDateArray[2]);
    
            var betweenDay = (endDateObj.getTime() - startDateObj.getTime()) / 1000 / 60 / 60 / 24 + 1; // 1을 더해야 기간
            var bid_fee = betweenDay*daily_budget;
            if(daily_budget!=""){
                if($("#end_ymd").val()!="nolimit"){
                    $("#bid_fee_view").empty();
                    $("#bid_fee_view").append("<font color='blue'>"+number_format(bid_fee)+" <?php echo lang('strMoney');?>(<?php echo lang('strSurtaxIsExcluded')?>)</font>");
                    $("#bid_fee").attr('value',bid_fee);
                }
            }
        }
    }

    function inputNumberFormat(obj) {
        obj.value = comma(uncomma(obj.value));
    }

    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }

    function creative_group_modify(){

        if($("#cre_gp_nm").val()==""){
            alert("<?php echo lang('strAdGroupNameEntry');?>");
            $("#cre_gp_nm").focus();
            return;
        }
        else if($("#start_ymd").val()==""){
            alert("<?php echo lang('strAdStart');?>");
            $("#start_ymd").focus();
            return;
        }else if($("#end_ymd").val()==""){
            alert("<?php echo lang('strAdEnd');?>");
            $("#start_ymd").focus();
            return;
        }else if($(':radio[name="cre_gp_type"]:checked').val()==""){
            alert("<?php echo lang('strAdGroupType');?>");
            $("#cre_gp_type").focus();
            return;
        }else if($(':radio[name="daily_budget_sel"]:checked').val()==""){
            alert("<?php echo lang('strDayBudget');?>");
            $("#daily_budget_sel").focus();
            return;
        }else if($('#bid_price').val()==""){
            alert("<?php echo lang('strAdBud');?>");
            $("#bid_price").focus();
            return;
        }else if($("#nm_check_commit").val() != "Y"){
            alert("광고그룹명을 확인해 주세요.");
        }else{

            if($("#bid_cur").val() == "KRW"){
                bid_loc_price_val = uncomma($("#bid_loc_price").val());
            }else if($("#bid_cur").val() == "USD"){
                bid_loc_price_val = $("#bid_loc_price").val();
            }
            
            var url = '/creative/modify_creative_group_save';
            $.post(url,
                {
                    mem_no: '<?php echo $mem_no?>',
                    cam_no: '<?php echo $creative_group_info['cam_no']?>',
                    cre_gp_no: '<?php echo $creative_group_info['cre_gp_no']?>',
                    cre_gp_nm : $("#cre_gp_nm").val(),
                    start_ymd : $("#start_ymd").val(),
                    end_ymd : $("#end_ymd").val(),
                    cre_gp_type : $(':radio[name="cre_gp_type"]:checked').val(),
                    daily_budget : uncomma($("#daily_budget").val()),
                    bid_loc_price : bid_loc_price_val,
                    bid_type : $("#bid_type").val(),
                    bid_cur : $("#bid_cur").val(),
                    bid_fee : uncomma($("#bid_fee").val())
                },
                function(data){
                    //if(data.trim() == "false"){
                        //alert("광고그룹 수정실패");
                    //}else{
                        //alert("광고그룹 수정완료");
                        document.creative_group_modify.action="/admanagement/targeting_modify_form/?cre_gp_no=<?php echo $creative_group_info['cre_gp_no'];?>&cam_no=<?php echo $creative_group_info['cam_no'];?>";
                        document.creative_group_modify.submit();
                    //}
                }
            );
        }
    }
</script>
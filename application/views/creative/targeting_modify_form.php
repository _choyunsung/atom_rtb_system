<?php
    $time_comparison = "";
    $time_data = "";
    $os_comparison = "";
    $os_data = "";
    $browser_comparison = "";
    $browser_data = "";
    $device_comparison = "";
    $device_data = "";
    $category_comparison = "";
    $category_data = "";

    if(isset($targeting)){
        foreach($targeting as $tt){
            if($tt['tgt_type'] == 'time'){
                $time_comparison = $tt['tgt_comparison'];
                $time_data = $tt['tgt_data'];
            }

            if($tt['tgt_type'] == 'os'){
                $os_comparison = $tt['tgt_comparison'];
                $os_data = $tt['tgt_data'];
            }

            if($tt['tgt_type'] == 'browser'){
                $browser_comparison = $tt['tgt_comparison'];
                $browser_data = $tt['tgt_data'];
            }

            if($tt['tgt_type'] == 'device'){
                $device_comparison = $tt['tgt_comparison'];
                $device_data = $tt['tgt_data'];
            }

            if($tt['tgt_type'] == 'category'){
                $category_comparison = $tt['tgt_comparison'];
                $category_data = $tt['tgt_data'];
            }
        }
    }

    $time_data_arr = explode(",", $time_data);
    $os_data_arr = explode(",", $os_data);
    $browser_data_arr = explode(",", $browser_data);
    $device_data_arr = explode(",", $device_data);
    $category_data_arr = explode(",", $category_data);

?>

<script type="text/javascript">

    function selected_time(tr_val, td_val){

        tr_val = tr_val + 2;

        var td_id = $("tr:eq("+tr_val+") > td:eq("+td_val+")").prop('id');

        if($("#"+td_id).val() == 0){
            $("tr:eq("+tr_val+") > td:eq("+td_val+")").css("background", "#92b4d1");
            $("#"+td_id).val(1);

            var count = 0;
            for(i = 0; i < 24; i++){
                td_temp_id = $("tr:eq("+tr_val+") > td:eq("+i+")").attr('id');
                count = count + parseInt($("#"+td_temp_id).val());
            }
            if(count == 24){
                tr_id = td_id.substring(0, 3);
                $("input:checkbox[id='"+tr_id+"']").prop("checked", true);
            }
        }else{
            $("tr:eq("+tr_val+") > td:eq("+td_val+")").css("background", "#FFF");
            $("#"+td_id).val(0);
            tr_id = td_id.substring(0, 3);
            $("input:checkbox[id='"+tr_id+"']").prop("checked", false);
        }
    }

    function select_time(tr_val, td_val, e){

        tr_val = tr_val + 2;
        
        var td_id = e.id;

        if($("#"+td_id).val() == 0){
            $("#" + td_id).css("background", "#92b4d1");
            $("#"+td_id).val(1);

            var count = 0;
            for(i = 0; i < 24; i++){
                td_temp_id = $("tr:eq("+tr_val+") > td:eq("+i+")").attr('id');
                count = count + parseInt($("#"+td_temp_id).val());
            }
            if(count == 24){
                tr_id = td_id.substring(0, 3);
                $("input:checkbox[id='"+tr_id+"']").prop("checked", true);
            }
        }else{
            $("#" + td_id).css("background", "#FFF");
            $("#"+td_id).val(0);
            tr_id = td_id.substring(0, 3);
            $("input:checkbox[id='"+tr_id+"']").prop("checked", false);
        }
    }

    function check_time(obj, tr_val){
        var this_id = obj.id;
        if($(obj).is(':checked')){
            for(i = 0; i < 24; i++){
                $("#" + this_id.substring(0,3) + i).css("background", "#92b4d1");
                $("#"+this_id.substring(0,3) + i).val(1);
            }
        }else{
            for(i = 0; i < 24; i++){
                $("#" + this_id.substring(0,3) + i).css("background", "#FFF");
                $("#" + this_id.substring(0,3) + i).val(0);
            }
        }
    }

    function init_option(){
        <?php
            if($time_data == ""){
        ?>
        display_option("time", false);
        <?php
            }else{
        ?>
        display_option("time", true);
        <?php
            }
        ?>

        <?php
            if($os_data == ""){
        ?>
        display_option("os", false);
        <?php
            }else{
        ?>
        display_option("os", true);
        <?php
            }
        ?>

        <?php
            if($browser_data == ""){
        ?>
        display_option("browser", false);
        <?php
            }else{
        ?>
        display_option("browser", true);
        <?php
            }
        ?>

        <?php
            if($device_data == ""){
        ?>
        display_option("device", false);
        <?php
            }else{
        ?>
        display_option("device", true);
        <?php
            }
        ?>

        <?php
            if($category_data == ""){
        ?>
        display_option("category", false);
        <?php
            }else{
        ?>
        display_option("category", true);
        <?php
            }
        ?>
    }

    function display_option(type, display){

        if(display){
            $("#div_"+type).show();
            $("#btn_"+type+"_all").removeClass("btn-primary");
            $("#btn_"+type+"_all").addClass("btn-default");
            $("#btn_"+type+"_option").removeClass("btn-default");
            $("#btn_"+type+"_option").addClass("btn-primary");
            $("#option_"+type).val(1);
            $("#option_"+type).val(1);
            $("#option_"+type).val(1);
            $("#option_"+type).val(1);
        }else{
            $("#div_"+type).hide();
            $("#btn_"+type+"_all").removeClass("btn-default");
            $("#btn_"+type+"_all").addClass("btn-primary");
            $("#btn_"+type+"_option").removeClass("btn-primary");
            $("#btn_"+type+"_option").addClass("btn-default");
            $("#option_"+type).val(0);
            $("#option_"+type).val(0);
            $("#option_"+type).val(0);
            $("#option_"+type).val(0);
        }
    }


    function targeting_submit(){

        var check_os_obj = document.getElementsByName('check_os');
        var arr_value = new Array();

        for(var i = 0; i < check_os_obj.length; i++){
            if(check_os_obj[i].checked){
                arr_value[i] = check_os_obj[i].value;
            }else{
                arr_value[i] = "";
            }
        }

        var vals = '';
        for(var j = 0; j < arr_value.length; j++){
            if(arr_value[j] != ""){
                vals += arr_value[j] + ",";
            }
        }

        var data_os = vals.substring(0, vals.length - 1);

        var check_bs_obj = document.getElementsByName('check_bs');
        var arr_value = new Array();

        for(var i = 0; i < check_bs_obj.length; i++){
            if(check_bs_obj[i].checked){
                arr_value[i] = check_bs_obj[i].value;
            }else{
                arr_value[i] = "";
            }
        }

        var vals = '';
        for(var j = 0; j < arr_value.length; j++){
            if(arr_value[j] != ""){
                vals += arr_value[j] + ",";
            }
        }

        var data_browser = vals.substring(0, vals.length - 1);


        var check_dv_obj = document.getElementsByName('check_dv');
        var arr_value = new Array();

        for(var i = 0; i < check_dv_obj.length; i++){
            if(check_dv_obj[i].checked){
                arr_value[i] = check_dv_obj[i].value;
            }else{
                arr_value[i] = "";
            }
        }

        var vals = '';
        for(var j = 0; j < arr_value.length; j++){
            if(arr_value[j] != ""){
                vals += arr_value[j] + ",";
            }
        }

        var data_device = vals.substring(0, vals.length - 1);

        var check_cont_cate_obj = document.getElementsByName('check_cont_cate');
        var arr_value = new Array();

        for(var i = 0; i < check_cont_cate_obj.length; i++){
            if(check_cont_cate_obj[i].checked){
                arr_value[i] = check_cont_cate_obj[i].value;
            }else{
                arr_value[i] = "";
            }
        }

        var vals = '';
        for(var j = 0; j < arr_value.length; j++){
            if(arr_value[j] != ""){
                vals += arr_value[j] + ",";
            }
        }

        var data_category = vals.substring(0, vals.length - 1);


        var data_time = "";
        var count = 0;
        for(j = 3; j < 11; j++){
            for(i = 0; i < 24; i++){
                $("tr:eq("+j+") > td:eq("+i+")").css("background", "#FFF");
                td_id = $("tr:eq("+j+") > td:eq("+i+")").attr('id');
                if($("#"+td_id).val() == 1){
                    data_time += td_id + ",";
                    count++;
                }
            }
        }

        //전체 다 선택시 전체선택과 같다.
        if(count == 168){
            data_time = "";
        }

        //마지막에 ,를 없애준다.
        if(data_time.substring(data_time.length-1, data_time.length) == ','){
            data_time = data_time.substring(0, data_time.length-1);
        }
        
        var frm = document.targeting;
        frm.data_time.value = data_time;
        frm.data_os.value = data_os;
        frm.data_browser.value = data_browser;
        frm.data_device.value = data_device;
        frm.data_category.value = data_category;

        frm.submit();
    }


    $(function() {
        init_option();
    });

</script>
<div class="panel float_r wid_970 min_height">
     <div class="ma_t10 center">
		<img src="/img/step_03.png">
	</div> 
     <div class="panel-heading">
			<h3 class="page-header text-overflow tit_blit"><?php echo lang('strEditTargeting')?></h3>
	</div>

    <!-- karin -->
  <div class="panel-body">
    <form name="targeting" action="/admanagement/modify_targeting" method="POST">
        <input type="hidden" id="cre_gp_no" name="cre_gp_no" value="<?php echo $_GET['cre_gp_no']?>">
        <input type="hidden" id="cam_no" name="cam_no" value="<?php echo $_GET['cam_no']?>">
        <table width="100%" class="m_center tar_md_tb" cellpadding="0" cellspacing="0" border="0">
            <colgroup>
                <col width="13%">
                <col width="*">
            </colgroup>
            <tr>
                <th class="tar_md_th"><span class="join_blit"><?php echo lang('strDaynTime')?></span></th>
                <td>
                    <p>
                        <input type="hidden" id="option_time" name="option_time" value="0"/>
                        <input type="hidden" id="data_time" name="data_time" value=""/>
                        <input id="btn_time_all" class="btn btn-default" type="button" onclick="display_option('time', false);" value="<?php echo lang('strAll')?>">
                        <input id="btn_time_option" class="btn btn-default" type="button" onclick="display_option('time', true);" value="<?php echo lang('strSelectOption')?>">
                    </p>
                </td>
            </tr>
            <tr id="div_time">
                <th class="tar_md_th">&nbsp;</th>
                <td class="bd-b1">
                    <p>    
                    <input type="radio" name="comparison_time" value="0" <?php if($time_comparison == "=~" || $time_comparison == ""){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdByChosenOption')?>&nbsp;&nbsp;
                    <input type="radio" name="comparison_time" value="1" <?php if($time_comparison == "!~"){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdExceptChosenOption')?>
                    </p>
                    <div class="wid_98p m_center">
                        <table id="tb_time" class="tar_md_time_box m_center" cellpadding="0" border="0" cellspacing="0">
                            <colgroup>
            				    <col width="9.7%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                                <col width="3.8%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>0<br/>~<br/>1</th>
                                    <th>1<br/>~<br/>2</th>
                                    <th>2<br/>~<br/>3</th>
                                    <th>3<br/>~<br/>4</th>
                                    <th>4<br/>~<br/>5</th>
                                    <th>5<br/>~<br/>6</th>
                                    <th>6<br/>~<br/>7</th>
                                    <th>7<br/>~<br/>8</th>
                                    <th>8<br/>~<br/>9</th>
                                    <th>9<br/>~<br/>10</th>
                                    <th>10<br/>~<br/>11</th>
                                    <th>11<br/>~<br/>12</th>
                                    <th>12<br/>~<br/>13</th>
                                    <th>13<br/>~<br/>14</th>
                                    <th>14<br/>~<br/>15</th>
                                    <th>15<br/>~<br/>16</th>
                                    <th>16<br/>~<br/>17</th>
                                    <th>17<br/>~<br/>18</th>
                                    <th>18<br/>~<br/>19</th>
                                    <th>19<br/>~<br/>20</th>
                                    <th>20<br/>~<br/>21</th>
                                    <th>21<br/>~<br/>22</th>
                                    <th>22<br/>~<br/>23</th>
                                    <th>23<br/>~<br/>24</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="color_r"><input type="checkbox" class='btn_check' id='sun' value="" onclick="check_time(this, 1);">&nbsp;<?php echo lang('strSunday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='sun<?php echo $i?>' onclick="select_time(1, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("sun".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(1, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class='btn_check' id='mon' value="" onclick="check_time(this, 2);">&nbsp;<?php echo lang('strMonday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='mon<?php echo $i?>' onclick="select_time(2, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("mon".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(2, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class='btn_check' id='tue' value="" onclick="check_time(this, 3);">&nbsp;<?php echo lang('strTuesday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='tue<?php echo $i?>' onclick="select_time(3, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("tue".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(3, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class='btn_check' id='wed' value="" onclick="check_time(this, 4);">&nbsp;<?php echo lang('strWednesday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='wed<?php echo $i?>' onclick="select_time(4, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("wed".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(4, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class='btn_check' id='thu' value="" onclick="check_time(this, 5);">&nbsp;<?php echo lang('strThursday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='thu<?php echo $i?>' onclick="select_time(5, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("thu".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(5, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <th><input type="checkbox" class='btn_check' id='fri' value="" onclick="check_time(this, 6);">&nbsp;<?php echo lang('strFriday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='fri<?php echo $i?>' onclick="select_time(6, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("fri".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(6, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <th class="color_b2"><input type="checkbox" class='btn_check' id='sat' value="" onclick="check_time(this, 7);">&nbsp;<?php echo lang('strSaturday')?></th>
                                    <?php for($i = 0; $i < 24 ;$i++){ ?>
                                        <td id='sat<?php echo $i?>' onclick="select_time(7, <?php echo $i?>, this)"></td>
                                        <?php if(in_array("sat".$i, $time_data_arr)){ ?>
                                            <script type="text/javascript">
                                                selected_time(7, <?php echo $i?>);
                                            </script>
                                        <?php
                                            }
                                        }
                                    ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="tar_md_th"><span class="join_blit"><?php echo lang('strOperatingSystem')?></span></th>
                
                <td>
                    <p>    
                        <input type="hidden" id="option_os" name="option_os" value="0"/>
                        <input type="hidden" id="data_os" name="data_os" value=""/>
                        <input id="btn_os_all" class="btn btn-default" type="button" onclick="display_option('os', false);" value="<?php echo lang('strAll')?>">
                        <input id="btn_os_option" class="btn btn-default" type="button" onclick="display_option('os', true);" value="<?php echo lang('strSelectOption')?>">
                    </p>
                </td>
            </tr>
            <tr id="div_os">
                <th class="tar_md_th">&nbsp;</th>
                <td class="bd-b1 pa_b10">
                    <p>
                        <input type="radio" name="comparison_os" value="0" <?php if($os_comparison == "=~" || $os_comparison == ""){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdByChosenOption')?>&nbsp;&nbsp;
                        <input type="radio" name="comparison_os" value="1" <?php if($os_comparison == "!~"){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdExceptChosenOption')?>
                    </p>
                    <div class="tar_md_div">
                        <?php 
                            foreach($os as $key=>$o){
                                if($key%5 == 0){
                                    echo "<div style='clear:left;'>&nbsp;</div>";
                                }
                                if(in_array($o['os_cd'], $os_data_arr)){
                                    $checked = "checked";
                                }else{
                                    $checked = "";
                                }
                        ?>
                            <div class="float_l wid_20p pa_b20"><input type="checkbox" name="check_os" value="<?php echo $o['os_cd'];?>" <?php echo $checked;?>/>&nbsp;<?php echo $o['os_nm'];?></div>
                        <?php
                            }
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="tar_md_th"><span class="join_blit"><?php echo lang('strBrowser')?></span></th>
                <td>
                    <p>    
                        <input type="hidden" id="option_browser" name="option_browser" value="0"/>
                        <input type="hidden" id="data_browser" name="data_browser" value=""/>
                        <input id="btn_browser_all" class="btn btn-default" type="button" onclick="display_option('browser', false);" value="<?php echo lang('strAll')?>">
                        <input id="btn_browser_option" class="btn btn-default" type="button" onclick="display_option('browser', true);" value="<?php echo lang('strSelectOption')?>">
                    </p>
                </td>
            </tr>
            <tr id="div_browser"> 
                <th class="tar_md_th">&nbsp;</th>
                <td class="bd-b1 pa_b10">
                    <p>         
                        <input type="radio" name="comparison_browser" value="0" <?php if($browser_comparison == "=~" || $browser_comparison == ""){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdByChosenOption')?>&nbsp;&nbsp;
                        <input type="radio" name="comparison_browser" value="1" <?php if($browser_comparison == "!~"){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdExceptChosenOption')?>
                    </p>   
                    <div class="tar_md_div">
                        <?php
                            foreach($browser as $key=>$b){
                                if($key%5 == 0){
                                    echo "<div style='clear:left;'>&nbsp;</div>";
                                }
                                if(in_array($b['bs_cd'], $browser_data_arr)){
                                    $checked = "checked";
                                }else{
                                    $checked = "";
                                }
                        ?>
                             <div class="float_l wid_20p pa_b20"><input type="checkbox" name="check_bs" value="<?php echo $b['bs_cd'];?>" <?php echo $checked;?>/>&nbsp;<?php echo $b['bs_nm'];?></div>
                        <?php
                            }
                        ?>
                    </div>   
                </td>
            </tr>
            <tr>
                <th class="tar_md_th"><span class="join_blit"> <?php echo lang('strDevice')?></span></th>
                <td>
                    <p>    
                        <input type="hidden" id="option_device" name="option_device" value="0"/>
                        <input type="hidden" id="data_device" name="data_device" value=""/>
                        <input id="btn_device_all" class="btn btn-default" type="button" onclick="display_option('device', false);" value="<?php echo lang('strAll')?>">
                        <input id="btn_device_option" class="btn btn-default" type="button" onclick="display_option('device', true);" value="<?php echo lang('strSelectOption')?>">
                    </p>
                </td>
            </tr>
            <tr id="div_device"> 
                <th class="tar_md_th">&nbsp;</th>
                <td class="bd-b1 pa_b10"> 
                    <p>
                        <input type="radio" name="comparison_device" value="0" <?php if($device_comparison == "=~" || $device_comparison == ""){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdByChosenOption')?>&nbsp;&nbsp;
                        <input type="radio" name="comparison_device" value="1" <?php if($device_comparison == "!~"){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdExceptChosenOption')?>
                    </p> 
                    <div class="tar_md_div">
                        <?php
                            foreach($device as $key=>$d){
                                if($key%5 == 0){
                                    echo "<div style='clear:left;'>&nbsp;</div>";
                                }
                                if(in_array($d['dv_cd'], $device_data_arr)){
                                    $checked = "checked";
                                }else{
                                    $checked = "";
                                }
                        ?>
                             <div class="float_l wid_20p pa_b20"><input type="checkbox" name="check_dv" value="<?php echo $d['dv_cd'];?>" <?php echo $checked;?>/>&nbsp;<?php echo $d['dv_nm'];?></div>
                        <?php
                            }
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <th class="tar_md_th"><span class="join_blit"><?php echo lang('strCategory')?></span></th>
                <td>
                    <p>    
                        <input type="hidden" id="option_category" name="option_category" value="0"/>
                        <input type="hidden" id="data_category" name="data_category" value=""/>
                        <input id="btn_category_all" class="btn btn-default" type="button" onclick="display_option('category', false);" value="<?php echo lang('strAll')?>">
                        <input id="btn_category_option" class="btn btn-default" type="button" onclick="display_option('category', true);" value="<?php echo lang('strSelectOption')?>">
                    </p>
                </td>
            </tr>
            <tr id="div_category"> 
                <th class="tar_md_th">&nbsp;</th>
                <td class="bd-b1 pa_b10">
                    <p>
                        <input type="radio" name="comparison_category" value="0" <?php if($category_comparison == "=~" || $category_comparison == ""){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdByChosenOption')?>&nbsp;&nbsp;
                        <input type="radio" name="comparison_category" value="1" <?php if($category_comparison == "!~"){ echo "checked";}?>>&nbsp;<?php echo lang('strRunAdExceptChosenOption')?>
                    </p>
                    <div class="tar_md_div">
                        <?php
                        foreach($category as $key=>$cg){
                            if($key%5 == 0){
                                echo "<div style='clear:left;'>&nbsp;</div>";
                            }
                            if(in_array($cg['cont_cate_cd'], $category_data_arr)){
                                $checked = "checked";
                            }else{
                                $checked = "";
                            }
                        ?>
                            <div class="float_l wid_20p pa_b20"><input type="checkbox" name="check_cont_cate" value="<?php echo $cg['cont_cate_cd'];?>" <?php echo $checked;?>/>&nbsp;<?php echo lang($cg['cont_cate_nm']);?></div>
                        <?php
                            }
                        ?>
                    </div> 
                </td>
            </tr>  
        </tbody>
    </table>
    <div class="bottom_bt">
        <span onclick="targeting_submit();" id="demo-state-btn" class="btn btn-primary" data-loading-text="Loading..." type="button">
            <?php echo lang('strSave')?>
        </span>
        <span onclick="location.replace('/creative/creative_group_list')" id="demo-state-btn" class="btn btn-default" data-loading-text="Loading..." type="button">
            <?php echo lang('strCancel')?>
        </span>
    </div>
        </form>
    </div>
</div>

<div class="modal" id="show_banner_modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog bd-t7 modal-lg">
		<div class="modal-content">
			<div id="show_banner"></div>
			<button class="close_banner_modal">닫기</button>
		</div>
	</div>
</div>

<div class="panel float_r wid_970 min_height">
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit">옥션 광고 배너 등록</h3>
	</div>

	<div class="panel-body">
		<div class="clear">
			<form name="creative_group_list" method="post">
				<input type="hidden" name="items_date">
			</form>
			<form name="creative_modify" id="creative_modify" action="/auction/items_add_save" method="post" enctype="multipart/form-data">
				<input type="hidden" name="items_idx" value = "<?php echo $idx[0]['items_idx']?>">
				<table>
					<tr>
						<td>광고예정일</td>
						<td>
							<input type="text" name="daterange" id="daterange" value="<?php if ($date){echo $date;}else{echo date('Y-m-d', strtotime('+1 day'));}?>">
						</td>
					</tr>
				</table>
				<table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
					<colgroup>
						<col width="4%">
						<col width="4%">
						<col width="20%">
						<col width="20%">
						<col width="10%">
						<col width="8%">
						<col width="30%">
						<col width="15%">
					</colgroup>
					<thead>
					<tr>
						<th>번호</th>
						<th>스킵</th>
						<th>상품명</th>
						<th>상품정보</th>
						<th>가격</th>
						<th>이미지</th>
						<th>링크</th>
						<th>초기화</th>
					</tr>
					</thead>
					<tbody>
					<?php for($i=1;$i<11;$i++) {?>
						<tr idex="<?=$i?>" class="auction_items" >
							<td class=" txt-center">
								<input type="hidden" name="item_idx[]" id="item_idx[]" value="<?php echo $list[$i-1]['item_idx'];?>">
								<?=$i?>
							</td>

							<td class=" txt-center">
								<?php if($list[$i-1]['item_skip'] == 'Y') { ?>
									<input type="checkbox" name="item_skip_chk[]" class="item_skip_chk" checked/>
									<input type="hidden" name="item_skip[]" class="item_skip" value="Y"/>
								<?php } else { ?>
									<input type="checkbox" name="item_skip_chk[]" class="item_skip_chk"/>
									<input type="hidden" name="item_skip[]" class="item_skip" value="N"/>
								<?php }?>
							</td>
							<td>
								<input type="text" name="item_name[]" class="item_name wid_100p"  style="font-size:12px !important;" value="<?php echo $list[$i-1]['item_name'];?>"/>
							</td>
							<td>
								<input type="text" name="item_info[]" class="item_info wid_100p"  style="font-size:12px !important;" value="<?php echo $list[$i-1]['item_info'];?>"/>
							</td>
							<td>
								<input type="text" name="item_price[]" class="item_price wid_100p txt-right" style="font-size:12px !important;" lang="nb" value="<?php echo $list[$i-1]['item_price'];?>"/>
							</td>
							<td style="text-align:center;">
								<?php if(isset($list[$i-1]['item_idx'])) { ?>
									<span style="cursor:pointer;" onclick="show_banner('<?php echo $date?>', <?php echo $i;?>)">미리보기</span>
								<?php } ?>
							</td>
							<td>
								<input type="text" name="item_link[]" class="item_link wid_100p" style="font-size:12px !important;" value="<?php echo $list[$i-1]['item_link'];?>"/>
							</td>
							<td>
								<button type="button" class="reset btn btn-danger" >reset</button>
							</td>
						</tr>
					<?php }?>
					</tbody>
				</table>
				<p style="float:right;">
					<a href="/auction/items" class="btn btn-primary">취소</a>
					<button type="submit" id="auction_submit" name="auction_submit" class="btn btn-danger">저장</button>
				</p>
			</form>
		</div>
	</div>
</div>
<script>
	$('#creative_modify').on('click','.reset',function(){
		var $this = $(this);
		var $sele = $this.parents('tr');
		var _number =$sele.attr('idex');
		if(confirm(_number+'라인을 초기화!!'))
		{
			$sele.find('input[type="text"]').val('');
			$sele.find('span').hide();
		}
	})

	$('.item_skip_chk').click(function(){

		if($(this).is(':checked')) {
			$(this).parents('tr').children().children('input:text').css('opacity', '0.4');
			$(this).parents('td').children('.item_skip').val('Y');
		}
		else {
			$(this).parents('tr').children().children('input:text').css('opacity', '1');
			$(this).parents('td').children('.item_skip').val('N');
		}
	});

	function show_banner(date, idx) {

		var url_200200 = "http://cdn.ads-optima.com/atom/allkill_item/" + date + "/img_product" + idx + "_200200.jpg";
		var url_300250 = "http://cdn.ads-optima.com/atom/allkill_item/" + date + "/img_product" + idx + "_300250.jpg";
		var url_320100 = "http://cdn.ads-optima.com/atom/allkill_item/" + date + "/img_product" + idx + "_320100.jpg";
		var url_32050 = "http://cdn.ads-optima.com/atom/allkill_item/" + date + "/img_product" + idx + "_32050.jpg";

		var tag = "";

		tag  = "<table>";
		tag += 		"<tr>";
		tag += "		<th> 200 X 200 </th>";
		tag += "		<td><img src='" + url_200200 + "'></td>";
		tag += "	</tr>";
		tag += "	<tr>";
		tag += "		<th> 300 X 250 </th>";
		tag += "		<td><img src='" + url_300250 + "'></td>";
		tag += "	</tr>";
		tag += "	<tr>";
		tag += "		<th> 320 X 100 </th>";
		tag += "		<td><img src='" + url_320100 + "'></td>";
		tag += "	</tr>";
		tag += "	<tr>";
		tag += "		<th> 320 X 50 </th>";
		tag += "		<td><img src='" + url_32050 + "'></td>";
		tag += "	</tr>";
		tag += "</table>";

		document.getElementById("show_banner").innerHTML = tag;

		$("#show_banner_modal").modal("show");
	}

	$("#daterange").change(function(){
		search();
	});

	function search() {
		var frm = document.creative_group_list;
		frm.action = "/auction/items_add_form";
		frm.items_date.value = $("#daterange").val();
		frm.submit();
	}

	$(function() {
		$('input[name="daterange"]').daterangepicker({
			singleDatePicker: true
		});

		$('.close_banner_modal').click(function(){
			$("#show_banner_modal").modal("hide");
		});
	});
</script>
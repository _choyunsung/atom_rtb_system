<div class="panel float_r wid_970 min_height">
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit">옥션 일자별 상품등록 리스트</h3>
    </div>
    <div class="panel-body">
        <div class="clear">
            <div class="float_l wid_100p">
                <div class="float_l pa_t20 ma_b3" id="all_btn_gp">
                    <a href="/auction/items_add_form" class="btn btn-primary float_l">광고상품 등록</a>
                </div>
            </div>
            <div class="pa_b20 clear">
                <form name="creative_group_list" method="post">
                    <input type="hidden" name="items_date">
                    <input type="hidden" id="start" name="start" value="<?php echo $start?>">
                    <input type="hidden" id="cnt" name="cnt" value="<?php echo $cnt?>">
                    <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                        <colgroup>
                            <col width="10%">
                            <col width="45%">
                            <col width="45%">
                        </colgroup>
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>옥션광고일</th>
                            <th>상품수</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (isset($list)){?>
                            <?php foreach ($list as $key=>$row){?>
                                <tr>
                                    <td>
                                        <?php echo $key+1;?>
                                    </td>
                                    <td>
                                        <a href = "javascript:items_detail('<?php echo $row['items_date']?>')">
                                            <?php echo $row['items_date'];?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php echo $row['items_cnt']?>
                                    </td>
                                </tr>
                            <?php }?>
                        <?php }?>
                        </tbody>
                    </table>
                    <div class="center hei_35">
                        <?php
                            if ($total_cnt % 10 == 0) {
                                $total_page = $total_cnt / $page_cnt;
                            } else if($total_cnt % $page_cnt == 0){
                                $total_page = floor($total_cnt / $page_cnt);
                            } else {
                                $total_page = floor($total_cnt / $page_cnt) + 1;
                            }

                            for($i=0; $i<$total_page; $i++) {
                                $page_num = ($start_ten * 10) + $i;
                                if($start == $i) {
                                    echo "<span style='font-weight:bold;padding:10px;margin:5px;border:1px solid #4e5254;background-color:#4e5254;color:white;' href='javascript:void(0);' onclick='page_move(" . $page_num . ");return false;'>" . ($page_num + 1) . "</span>";
                                }
                                else {
                                    echo "<a style='font-weight:bold;padding:10px;margin:5px;border:1px solid #4e5254;' href='javascript:void(0);' onclick='page_move(" . $page_num . ");return false;'>" . ($page_num + 1) . "</a>";
                                }
                            }
                        ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


        
<script type="text/javascript">

    function search(){
        var frm = document.creative_group_list;
        frm.action = "/creative/creative_group_list";
        frm.fromto_date.value=$("#daterange").val();
        frm.submit();
    }

    function page_move(number){
        var frm = document.creative_group_list;
        frm.start.value = number;
        frm.action = "/auction/items/";
        frm.submit();
    }

    function items_detail(date){
        var frm = document.creative_group_list;
        frm.action = "/auction/items_add_form"
        frm.items_date.value = date;
        frm.submit();
    }
</script>
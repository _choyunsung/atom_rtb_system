<style type="text/css">
.daterangepicker {
	position: absolute;
	z-index: 9999999;
}
</style>
<script type="text/javascript">
$(function(){
    $('.range').daterangepicker({
    	startDate: moment().subtract(7, 'days'),
    	endDate: moment(),
    	minDate: '2012-01-01',
    	maxDate: '2018-12-31',
    	dateLimit: { days: 60 },
    	showDropdowns: true,
    	showWeekNumbers: true,
    	timePicker: false,
    	timePickerIncrement: 1,
    	timePicker12Hour: true,
    	ranges: {
     	   'Today': [moment(), moment()],
    	   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    	   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    	   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    	   'This Month': [moment().startOf('month'), moment().endOf('month')],
    	   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	opens: 'left',
    	buttonClasses: ['btn btn-default'],
    	applyClass: 'btn-sm btn-primary',
    	cancelClass: 'btn-sm',
    	format: 'YYYY-MM-DD',
    	separator: ' ~ ',
    	locale: {
    		applyLabel: 'Submit',
    		fromLabel: 'From',
    		toLabel: 'To',
    		customRangeLabel: 'Custom Range',
    		daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
    		monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    		firstDay: 1
    	}
    },
    
    function (start, end) {
    	$('#fromto_date').val(start.format('YYYY-MM-DD') + ' ~ ' + end.format('YYYY-MM-DD'));
    	var range_updated = start.format('YYYY-MM-DD') + ' ~ ' + end.format('YYYY-MM-DD');
    	$('.range span').html(range_updated);
    });
});

function d_select_term(term){

    if(term == "day"){
    	$('#d_term').attr('value','day');
        $('#d_day_btn').css('background-Color', '#dbdbdb');
        $('#d_week_btn').css('background-Color', '#f3f3f3');
        $('#d_month_btn').css('background-Color', '#f3f3f3');

    }else if(term == "week"){
    	$('#d_term').attr('value','week');
        $('#d_day_btn').css('background-Color', '#f3f3f3');
        $('#d_week_btn').css('background-Color', '#dbdbdb');
        $('#d_month_btn').css('background-Color', '#f3f3f3');
        
    }else if(term == "month"){
    	$('#d_term').attr('value','month');
        $('#d_day_btn').css('background-Color', '#f3f3f3');
        $('#d_week_btn').css('background-Color', '#f3f3f3');
        $('#d_month_btn').css('background-Color', '#dbdbdb'); 
    }
    
    parent.report_daily_reset('<?php echo $datatype;?>', '<?php echo $datakey?>', $('#daterange').val(), $('#d_term').val(), $('#imp_val').val(), $('#click_val').val(), $('#ctr_val').val(), $('#ppc_val').val(), $('#ppi_val').val(), $('#price_val').val());

}
</script>
<div class="modal-header">
    <h4 class="h4_blit ma_b5 ma_t5"><?php echo lang('strDaily');?> <?php echo lang('strReport');?>&nbsp;&gt;
    <?php 
    
        if($datatype == "role"){
            if($datakey == "adver"){
                echo lang('strAdvertiser');
            }else if($datakey == "ind"){
                echo lang('strIndividual');
            }else if($datakey == "agency"){
                echo lang('strAgency');
            }else if($datakey == "rep"){
                echo lang('strLab');
            }
        }else if($datatype == "role_kind"){
            
            if($group_role == "adver"){
                echo lang('strAdvertiser')." > ";
            }else if($group_role == "ind"){
                echo lang('strIndividual')." > ";
            }else if($group_role == "agency"){
                echo lang('strAgency')." > ";
            }else if($group_role == "rep"){
                echo lang('strLab')." > ";
            }
             
            echo $group_nm;
        }else if($datatype == "sales"){
            echo $datakey;
        }else if($datatype == "sales_kind"){
            echo $sales_nm." > ".$group_nm;
        }else if($datatype == "group"){
            echo $mem_com_nm." > ".$mem_type;
        }
        
    ?>
    <span data-dismiss="modal" class="btn btn-dark float_r"><?php echo lang('strClose')?></span></h4>
</div>
<div class="modal-body">
	<div class="panel-heading">
	</div>
	<div class="panel-body">
        <!--날짜 검색부 -->
        <div class="clear" id="all_btn_gp" style="padding-bottom:5px;">
            <input type="hidden" name="d_term" id="d_term" value="<?php echo $term?>" />
            <div id="term_form" class="float_r ma_l10">
                <a href="javascript:d_select_term('month')" class="btn float_right btn-default"  id="d_month_btn"  style="<?php if($term == 'month'){echo "background-color:#dbdbdb";}?>"><?php echo lang("strMonth")?></a>
                <a href="javascript:d_select_term('week')" class="btn float_right btn-default" id="d_week_btn" style="<?php if($term == 'week'){echo "background-color:#dbdbdb";}?>"><?php echo lang("strWeek")?></a>
                <a href="javascript:d_select_term('day')" class="btn float_right btn-default"  id="d_day_btn" style="<?php if($term == 'day'){echo "background-color:#dbdbdb";}?>"><?php echo lang("strDay")?></a>
            </div>
            <div class="float_r ma_l10">
                <a href="javascript:parent.report_daily_reset('<?php echo $datatype;?>', '<?php echo $datakey?>', $('#daterange').val(), $('#term').val(), $('#imp_val').val(), $('#click_val').val(), $('#ctr_val').val(), $('#ppc_val').val(), $('#ppi_val').val(), $('#price_val').val());" class="btn btn-primary"><?php echo lang('strSearch');?></a>&nbsp;
            </div>
            <div id="demo_dp_component" class="camp_report_r" style="width:37%;">
                <input type="text" name="fromto_date" id="daterange" class="range wid_75p" value="<?php echo $fromto_date?>" />
				<span class="iput-group-addon">
					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
				</span>
            </div>
        </div>
    </div>
<?php
    function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
        if ($force_number) {
            $number = preg_replace('/^[^\d]+/', '', $number);
        } else if (preg_match('/^[^\d]+/', $number)) {
            return false;
        }
        $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
        $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
        settype($number, $type);
        return $number;
    }
    
    $site_lang = $this->session->userdata('site_lang');
    
    if($site_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }
    
    if($currency == "KRW"){
        $currency_unit = "₩";
        $currency_loc_unit = lang('strWon');
    }elseif($currency == "USD"){
        $currency_unit = "$";
        $currency_loc_unit = lang('strDollar');
    }
?>
<!-- 리스트 -->
<div class=" ma_t20">
<div class="panel-body">
    <div class="camp2">
        <div class="camp_div6" id="d_imp_img" onclick="javascipt:d_select_chart_option('imp');">
        <p class="camp_tit"><?php echo lang('strImpressions');?></p><span class="camp_con" id="sum_imp_cnt">0</span>
        </div>
        <div class="camp_div6" id="d_click_img" onclick="javascipt:d_select_chart_option('click');">
        <p class="camp_tit"><?php echo lang('strClicks');?></p><span class="camp_con" id="sum_click_cnt">0</span>
        </div>
        <div class="camp_div6" id="d_ctr_img" onclick="javascipt:d_select_chart_option('ctr');">
        <p class="camp_tit">CTR (%)</p><span class="camp_con" id="sum_ctr">0</span>
        </div>
        <div class="camp_div6" id="d_ppc_img" onclick="javascipt:d_select_chart_option('ppc');">
        <p class="camp_tit"><?php echo lang('strAverage');?> PPC (<?php echo $currency_unit?>)</p><span class="camp_con" id="sum_ppc">0</span>
        </div>
        <div class="camp_div6" id="d_ppi_img"  onclick="javascipt:d_select_chart_option('ppi');">
        <p class="camp_tit"><?php echo lang('strAverage');?> PPI (<?php echo $currency_unit?>)</p><span class="camp_con" id="sum_ppi">0</span>
        </div>
        <div class="camp_div6" id="d_price_img" onclick="javascipt:d_select_chart_option('price');">
        <p class="camp_tit"><?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)</p><span class="camp_con" id="sum_price">0</span>
        </div>
    </div>
    <div id="report_daily_chart"></div>

</div>

<div class="panel-heading">
</div>

<div class="panel-body">
    <form name="report_daily_detail" method="post">
        <input type="hidden" name="d_imp_val" id="d_imp_val" value="<?php echo $option_imp?>">
        <input type="hidden" name="d_click_val" id="d_click_val" value="<?php echo $option_click?>">
        <input type="hidden" name="d_ctr_val" id="d_ctr_val" value="<?php echo $option_ctr?>">
        <input type="hidden" name="d_ppc_val" id="d_ppc_val" value="<?php echo $option_ppc?>">
        <input type="hidden" name="d_ppi_val" id="d_ppi_val" value="<?php echo $option_ppi?>">
        <input type="hidden" name="d_price_val" id="d_price_val" value="<?php echo $option_price?>">
        <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
            <colgroup>
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <thead>
                <tr>
                    <th>
                        <?php echo lang('strDate');?>
                    </th>
                    <th>
                        <?php echo lang('strImpressions');?>
                    </th>
                    <th>
                        <?php echo lang('strClicks');?>
                    </th>
                    <th>
                        CTR (%)
                    </th>
                    <th>
                        <?php echo lang('strAverage');?> PPC (<?php echo $currency_unit?>)
                    </th>
                    <th>
                        <?php echo lang('strAverage');?> PPI (<?php echo $currency_unit?>)
                    </th>
                    <th>
                        <?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $chart_data = array();
                    $sum_imp_cnt = 0;
                    $sum_click_cnt = 0;
                    $sum_loc_price = 0;
                    $sum_price = 0;
                    $count = count($report_list);
                    $sum_ctr = 0;
                    $sum_loc_ppi = 0;
                    $sum_ppi = 0;
                    $sum_loc_ppc = 0;
                    $sum_ppc = 0;

                    foreach($report_list as $key=>$rl){
                        //ctr = 클릭수 / 노출수 * 100
                        //평균 ppc = 총 광고비 / 클릭수
                        //평균 ppi = 총 광고비 / 노출수
                        if($rl['imp_cnt'] > 0){
                            $ctr = $rl['click_cnt'] / $rl['imp_cnt'] * 100;
                            $loc_ppi = $rl['loc_price'] / $rl['imp_cnt'];
                            $ppi = $rl['price'] / $rl['imp_cnt'];
                        }else{
                            $ctr = 0;
                            $loc_ppi = 0;
                            $ppi = 0;
                        }

                        if($rl['click_cnt'] > 0){
                            $loc_ppc = $rl['loc_price'] / $rl['click_cnt'];
                            $ppc = $rl['price'] / $rl['click_cnt'];
                        }else{
                            $loc_ppc = 0;
                            $ppc = 0;
                        }

                        $sum_imp_cnt += $rl['imp_cnt'];
                        $sum_click_cnt += $rl['click_cnt'];
                        $sum_loc_price += $rl['loc_price'];
                        $sum_price += $rl['price'];

                        if($sum_imp_cnt > 0){
                            $sum_ctr = $sum_click_cnt / $sum_imp_cnt * 100;
                            $sum_loc_ppi = $sum_loc_price / $sum_imp_cnt;
                            $sum_ppi = $sum_price / $sum_imp_cnt;
                        }else{
                            $sum_ctr = 0;
                            $sum_loc_ppi = 0;
                            $sum_ppi = 0;
                        }

                        if($sum_click_cnt > 0){
                            $sum_loc_ppc = $sum_loc_price / $sum_click_cnt;
                            $sum_ppc = $sum_price / $sum_click_cnt;
                        }else{
                            $sum_loc_ppc = 0;
                            $sum_ppc = 0;
                        }

                        $result_imp_cnt = number_format($rl['imp_cnt']);
                        $result_click_cnt = number_format($rl['click_cnt']);
                        $result_ctr = number_format(round($ctr, 2), 2);

                        if($currency == "KRW"){
                            $result_ppc = number_format(round($loc_ppc, 0));
                            $result_ppi = number_format(round($loc_ppi, 0));
                            $result_price = number_format(round($rl['loc_price'], 0));
                        }elseif($currency == "USD"){
                            $result_ppc = number_format(round($ppc, 2), 2);
                            $result_ppi = number_format(round($ppi, 2), 2);
                            $result_price = number_format(round($rl['price'], 2), 2);
                        }

                        $chart_data[$key]['date_ymd'] = $rl['date_ymd'];
                        $chart_data[$key]['imp_cnt'] = $result_imp_cnt;
                        $chart_data[$key]['click_cnt'] = $result_click_cnt;
                        $chart_data[$key]['ctr'] = $result_ctr;
                        $chart_data[$key]['ppc'] = $result_ppc;
                        $chart_data[$key]['ppi'] = $result_ppi;
                        $chart_data[$key]['price'] = $result_price;
                ?>
                <tr>
                    <td style="text-align:center;padding-right:5px;">
                        <?php 
                            if($term == "day"){
                                echo $rl['date_ymd'];
                            }else if($term == "week"){
                                $year = substr($rl['date_ymd'], 0, 4);  // 2000
                                $month = substr($rl['date_ymd'], 5, 2);  // 1, 2, 3, ..., 12
                                $day = substr($rl['date_ymd'], 8, 2);  // 1, 2, 3, ..., 31
                                $cur_day = date("w", mktime(0, 0, 0, $month, $day, $year));
                                $minus_day = 6 - $cur_day;
                                $week_first = date("Y-m-d", mktime(0, 0, 0, $month, $day - $cur_day, $year));
                                $week_last  = date("Y-m-d", mktime(0, 0, 0, $month, $day + $minus_day, $year));
                                $date_ymd = $rl['date_ymd'];
                                echo $date_ymd."<br/>~".$week_last;
                            }else if($term == "month"){
                                $date_ymd_temp = explode( "-", $rl['date_ymd']);
                                echo $date_ymd_temp[1];
                                
                            }
                        ?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_imp_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_click_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_ctr;?>%
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_ppc;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_ppi;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_price;?>
                    </td>
                </tr>
                <?php
                    }
                    if($currency == "KRW"){
                        if($count > 0){
                            $avg_price = number_format(round($sum_loc_price / $count, 0));
                        }else{
                            $avg_price = 0;
                        }
                    }elseif($currency == "USD"){
                        if($count > 0){
                            $avg_price = number_format(round($sum_price / $count, 2), 2);
                        }else{
                            $avg_price = 0;
                        }
                    }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td style="text-align:center;padding-right:5px;">
                        <?php echo lang('strAverage');?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php
                            if($count > 0){
                                echo number_format(round($sum_imp_cnt / $count, 0));
                            }else{
                                echo "0";
                            }
                        ?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php
                            if($count > 0){
                                echo number_format(round($sum_click_cnt / $count, 0));
                            }else{
                                echo "0";
                            }
                        ?>
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        -
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $avg_price;?>
                    </td>
                </tr>
                <tr>
                    <?php
                        $result_sum_imp_cnt = number_format(round($sum_imp_cnt, 0));
                        $result_sum_click_cnt = number_format(round($sum_click_cnt, 0));
                        $result_sum_ctr = number_format(round($sum_ctr, 2), 2);

                        if($currency == "KRW"){
                            $result_sum_ppc = number_format(round($sum_loc_ppc, 0));
                            $result_sum_ppi = number_format(round($sum_loc_ppi, 0));
                            $result_sum_price = number_format(round($sum_loc_price, 0));
                        }elseif($currency == "USD"){
                            $result_sum_ppc = number_format(round($sum_ppc, 2), 2);
                            $result_sum_ppi = number_format(round($sum_ppi, 2), 2);
                            $result_sum_price = number_format(round($sum_price, 2), 2);
                        }
                    ?>
                    <td style="text-align:center;padding-right:5px;">
                        <?php echo lang('strSum');?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_imp_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_click_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_ctr;?>%
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_ppc;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_ppi;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_price;?>
                    </td>
                </tr>
            </tfoot>
        </table>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $('#sum_imp_cnt').html('<?php echo $result_sum_imp_cnt?>');
        $('#sum_click_cnt').html('<?php echo $result_sum_click_cnt?>');
        $('#sum_ctr').html('<?php echo $result_sum_ctr?>');
        $('#sum_ppc').html('<?php echo $result_sum_ppc?>');
        $('#sum_ppi').html('<?php echo $result_sum_ppi?>');
        $('#sum_price').html('<?php echo $result_sum_price?>');

        if($('#d_imp_val').val() == "Y"){
            $('#d_imp_text').css('color', '#4db4ff');
            $('#d_imp_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
        }else{
            $('#d_imp_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#d_click_val').val() == "Y"){
            $('#d_click_text').css('color', '#a783cc');
            $('#d_click_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #a783cc"});
        }else{
            $('#d_click_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#d_ctr_val').val() == "Y"){
            $('#d_ctr_text').css('color', '#ffc600');
            $('#d_ctr_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
        }else{
            $('#d_ctr_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#d_ppc_val').val() == "Y"){
            $('#d_ppc_text').css('color', '#97cc00');
            $('#d_ppc_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
        }else{
            $('#d_ppc_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#d_ppi_val').val() == "Y"){
            $('#d_ppi_text').css('color', '#668eda');
            $('#d_ppi_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
        }else{
            $('#d_ppi_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#d_price_val').val() == "Y"){
            $('#d_price_text').css('color', '#e87a75');
            $('#d_price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
        }else{
            $('#d_price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
    });

    function d_select_chart_option(kind){

        if(kind == "imp"){
            if($('#d_imp_val').val() == "Y"){
                $('#d_imp_val').attr('value', 'N');
                $('#d_imp_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#d_imp_val').attr('value', 'Y');
                $('#d_imp_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
            }
        }else if(kind == "click"){
            if($('#d_click_val').val() == "Y"){
                $('#d_click_val').attr('value', 'N');
                $('#d_click_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#d_click_val').attr('value', 'Y');
                $('#d_click_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #a783cc"});
            }
        }else if(kind == "ctr"){
            if($('#d_ctr_val').val() == "Y"){
                $('#d_ctr_val').attr('value', 'N');
                $('#d_ctr_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#d_ctr_val').attr('value', 'Y');
                $('#d_ctr_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
            }
        }else if(kind == "ppc"){
            if($('#d_ppc_val').val() == "Y"){
                $('#d_ppc_val').attr('value', 'N');
                $('#d_ppc_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#d_ppc_val').attr('value', 'Y');
                $('#d_ppi_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
            }
        }else if(kind == "ppi"){
            if($('#d_ppi_val').val() == "Y"){
                $('#d_ppi_val').attr('value', 'N');
                $('#d_ppi_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#d_ppi_val').attr('value', 'Y');
                $('#d_ppi_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
            }
        }else if(kind == "price"){
            if($('#d_price_val').val() == "Y"){
                $('#d_price_val').attr('value', 'N');
                $('#d_price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#d_price_val').attr('value', 'Y');
                $('#d_price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
            }
        }

        var imp = $('#d_imp_val').val();
        var click = $('#d_click_val').val();
        var ctr = $('#d_ctr_val').val();
        var ppc = $('#d_ppc_val').val();
        var ppi = $('#d_ppi_val').val();
        var price = $('#d_price_val').val();

        d_chart_option(imp, click, ctr, ppc, ppi, price);
    }

    function d_chart_option(imp, click, ctr, ppc, ppi, price){
        parent.report_daily_reset('<?php echo $datatype?>', '<?php echo $datakey?>', '<?php echo $daterange?>', '<?php echo $term?>', imp, click, ctr, ppc, ppi, price);
    }

</script>
<?php
    $kind = "day";
    
    if($option_price == "N"){
        $option_price = "Y";
    }
?>
<script type="text/javascript">

	function daily_chart(){
	   $('#report_daily_chart').highcharts({
            chart:{
                type:'spline',
                height: 350
            },
            title: {
                style: {
                    fontSize: '17px',
                    fontFamily: 'Verdana, sans-serif'
                },
                text: '',
                x: 20
            },
            subtitle: {
                text: '',
                x: 20
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    events: {},
                    lineWidth: 2,
                    marker: {
                        radius:3,
                        symbol:'circle'
                    }
                }
            },
            xAxis: {
                <?php
                    if($term == "day"){
                ?>
                categories: [
                    <?php
                        foreach($chart_data as $key=>$cd){
                            $date_ymd = $cd['date_ymd'];
                    ?>
                        '<?php echo $date_ymd;?>',
                    <?php
                        }
                    ?>
                     ],

                 <?php
                    }else if($term == "month"){
                 ?>
                 categories: [
                     <?php
                         foreach($chart_data as $key=>$cd){
                             $date_ymd = $cd['date_ymd'];
                     ?>
                         '<?php $date_ymd_temp = explode( "-", $date_ymd);echo $date_ymd_temp[1];?>',
                     <?php
                         }
                     ?>
                      ],     
                <?php
                    }else if($term == "week"){
                ?>
                categories: [
                    <?php
                        foreach ($chart_data as $key=>$cd) {
                            $year = substr($cd['date_ymd'], 0, 4);  // 2000
                            $month = substr($cd['date_ymd'], 5, 2);  // 1, 2, 3, ..., 12
                            $day = substr($cd['date_ymd'], 8, 2);  // 1, 2, 3, ..., 31
                            $cur_day = date("w", mktime(0, 0, 0, $month, $day, $year));
                            $minus_day = 6 - $cur_day;
                            $week_first = date("Y-m-d", mktime(0, 0, 0, $month, $day - $cur_day, $year));
                            $week_last  = date("Y-m-d", mktime(0, 0, 0, $month, $day + $minus_day, $year));
                            $date_ymd = $cd['date_ymd'];
                    ?>
                    '<?php echo $date_ymd?>~<?php echo $week_last?>',
                    <?php
                        }
                    ?>
                    ],
                <?php
                    }
                ?>
                gridLineWidth: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    },
                    overflow: 'justify'
                }
                },
                yAxis: [
                    {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {
    
                        enabled: true,
    
                        style: {
                            color: '#43acfb',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "<?php echo lang('strImpressions');?>",
                        align: 'low',
                        rotation: 0,
                        x: -31,
                        y: 25
                    },
                    style: {
                        color: '#43acfb'
                    },
                    format:'{value}',
                   	opposite: true,
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {
    
                        enabled: <?php if($option_imp == "N" && $option_price == "N" && $option_ppc == "N" && $option_ppi == "N" && $option_ctr == "N"){ echo "true";}else{echo "false";}?>,
    
                        overflow: 'justify',
                        style: {
                            color: '#a780ca',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "<?php echo lang('strClicks');?>",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#a780ca'
                    },
                    format:'{value}',
                    opposite: true,
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {
                        enabled: <?php if($option_imp == "N" && $option_price == "N" && $option_ppc == "N" && $option_ppi == "N" && $option_click == "N"){ echo "true";}else{echo "false";}?>,
                        overflow: 'justify',
                        style: {
                            color: '#ffa200',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: 'CTR (%)',
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#ffa200'
                    },
                    format:'{value}',
                    opposite: true,
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    allowDecimals: true,
                    labels: {
    
                        enabled: <?php if($option_imp == "N" && $option_price == "N" && $option_click == "N" && $option_ppi == "N" && $option_ctr == "N"){ echo "true";}else{echo "false";}?>,
    
                        overflow: 'justify',
                        style: {
                            color: '#82bd00',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "<?php echo lang('strAverage');?> PPC (<?php echo $currency_unit?>)",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#82bd00'
                    },
                    format:'{value}'
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    allowDecimals: true,
                    labels: {
    
                        enabled: false,
    
                        overflow: 'justify',
                        style: {
                            color: '#5177bc',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: <?php if($option_imp == "N" && $option_price == "N" && $option_ppc == "N" && $option_click == "N" && $option_ctr == "N"){ echo "true";}else{echo "false";}?>,
                        text: "<?php echo lang('strAverage');?> PPI (<?php echo $currency_unit?>)",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#5177bc'
                    },
                    format:'{value}'
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    gridLineColor: '#e6e6e6',
                    labels: {
                        enabled: true,
                        overflow: 'justify',
                        style: {
                            color: "#c85757",
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "<?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: "#c85757"
                    }
                }
            ],
            tooltip: {
                headerFormat: '<span style="font-size: 12px"><b>{point.key}</b></span><br/>',
                xDateFormat: '%Y-%m-%d',
                shared: true

            },
            legend: {
                enabled:false,
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: [
                <?php
                    if($option_imp == 'Y'){
                ?>
                {
                name: "<?php echo lang('strImpressions');?>",
                yAxis: 0,
                zIndex: 0,
                color: '#43acfb',
                data: [
                <?php
                    foreach($chart_data as $key=>$cd){
                        echo number_unformat($cd['imp_cnt']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
                <?php
                    if($option_click == 'Y'){
                ?>
                {
                name: "<?php echo lang('strClicks');?>",
                zIndex:1,
                yAxis: 1,
                color: '#a780ca',
                data: [
                <?php
                    foreach($chart_data as $key=>$cd) {
                        echo number_unformat($cd['click_cnt']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
                <?php
                    if($option_ctr == 'Y'){
                ?>
                {
                name: 'CTR (%)',
                zIndex:2,
                yAxis: 2,
                color: '#ffa200',
                data: [
                <?php
                    foreach($chart_data as $key=>$cd) {
                        echo number_unformat($cd['ctr']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
                <?php
                    if($option_ppc == 'Y'){
                ?>
                {
                name: "<?php echo lang('strAverage');?> PPC (<?php echo $currency_unit?>)",
                zIndex:3,
                yAxis: 3,
                color: '#82bd00',
                data: [
                <?php
                    foreach($chart_data as $key => $cd){
                        echo number_unformat($cd['ppc']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
                <?php
                    if($option_ppi == 'Y'){
                ?>
                {
                name: "<?php echo lang('strAverage');?> PPI (<?php echo $currency_unit?>)",
                zIndex:4,
                yAxis: 4,
                color: '#5177bc',
                data: [
                <?php
                    foreach($chart_data as $key=>$cd){
                        echo number_unformat($cd['ppi']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
                <?php
                    if($option_price == 'Y'){
                ?>
                {
                type: 'column',
                name: "<?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)",
                yAxis: 5,
                zIndex: -1,
                color: "#c85757",
                data: [
                <?php
                    foreach($chart_data as $key=>$cd){
                        echo number_unformat($cd['price']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
            ]
        });
	}
    $(function () {

    	//d_select_chart_option('price');
    	daily_chart();
    });
</script>
</div>
<div class="modal-footer ma_t10">
    <span data-dismiss="modal" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>


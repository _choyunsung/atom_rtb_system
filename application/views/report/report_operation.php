<link href="/css/jquery.treegrid.css" rel="stylesheet" type="text/css" />

<script src="/js/jquery.treegrid.js"></script>
<script src="/js/jquery.treegrid.bootstrap3.js"></script>
<script type="text/javascript">
    <!--
    http://maxazan.github.io/jquery-treegrid/
    //-->
    $(document).ready(function() {
        $('.tree').treegrid({
            expanderExpandedClass : 'glyphicon glyphicon-minus',
            expanderCollapsedClass : 'glyphicon glyphicon-plus',
            initialState : 'collapsed',
        });
    });
</script>
<?php
    $mem_no = $this->session->userdata('mem_no');

    $temp_date = explode(' ~ ', $fromto_date);
    $from_date = $temp_date[0];
    $to_date = $temp_date[1];

    $sitr_lang = $this->session->userdata('site_lang');

    if($sitr_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }

    if($currency == "KRW"){
        $currency_unit = "₩";
    }elseif($currency == "USD"){
        $currency_unit = "$";
    }
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strReport');?></a></li>
			<li class="active"><?php echo lang('strOperationReport');?></li>
		</ol>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"> <?php echo lang('strOperationReport');?></h3>
	</div>
	<!--요기 -->
	<div class="panel-body pa_t0">
        <div class="clear bd_eb" id="report"">
        </div>
	</div>
	<div class="panel-heading">
	</div>
	<div class="panel-body">
        <script type="text/javascript">
            $(document).ready(function () {
                $("#export_excel").on('click', function () {
                    var uri = $("#list").battatech_excelexport({
                        containerid: "list"
                        , datatype: 'table'
                        , returnUri: true
                    });
                    $(this).attr('download', 'Report.xls').attr('href', uri).attr('target', '_blank');
                });
            });
        </script>
        <form name="report_operation"  method="post">
            <input type="hidden" name="range" value="<?php echo $range;?>">
            <!--날짜 검색부 -->
            <div class="clear" id="all_btn_gp" style="padding-bottom:5px;">
                <div class="float_r ma_l10">
                    <a href="javascript:document.report_operation.submit()" class="btn btn-primary"><?php echo lang('strSearch');?></a>&nbsp;
                    <a href="#" id="export_excel" download="" class="btn btn-default"><?php echo lang('strDownload');?></a>&nbsp;
                </div>
                <a href="javascript:sel_fromto_date('7')" class="btn btn-default"><?php echo lang('strLast7');?></a>&nbsp;
                <a href="javascript:sel_fromto_date('30')" class="btn btn-default"><?php echo lang('strLast30');?></a>&nbsp;
                <a href="javascript:sel_fromto_date('60')" class="btn btn-default"><?php echo lang('strLast60');?></a>&nbsp;
                <a href="javascript:sel_fromto_date('90')" class="btn btn-default"><?php echo lang('strLast90');?></a>&nbsp;
                
                <div id="demo_dp_component" class="camp_report_r" style="width:37%;">
                    <input type="text" name="fromto_date" id="daterange" class="range wid_55p" value="<?php echo $fromto_date?>" />

    				<span class="iput-group-addon">
    					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
    				</span>
                </div>

            </div>

            <!-- 리스트 -->
            <table id="list" name="list" class="table tree table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="20%">
                    <col width="5%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                    <tr>
                        <th>
                            <?php echo lang('strADT');?>
                        </th>
                        <th>
                            <?php echo lang('strStatus');?>
                        </th>
                        <th>
                            <?php echo lang('strImpressions');?>
                        </th>
                        <th>
                            <?php echo lang('strClicks');?>
                        </th>
                        <th>
                            CTR (%)
                        </th>
                        <th>
                            <?php echo lang('strAverage');?> PPC (<?php echo $currency_unit?>)
                        </th>
                        <th>
                            <?php echo lang('strAverage');?> PPI (<?php echo $currency_unit?>)
                        </th>
                        <th>
                            <?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)
                        </th>
                    </tr>
                </thead>
                <tbody>
        <?php

            //echo $this->report_db->test(1);

            foreach($report_member_list as $key=>$rml){

                //ctr = 클릭수 / 노출수 * 100
                //평균 ppc = 총 광고비 / 클릭수
                //평균 ppi = 총 광고비 / 노출수
                if($rml['imp_cnt'] > 0){
                    $ctr = $rml['click_cnt'] / $rml['imp_cnt'] * 100;
                    $loc_ppi = $rml['loc_price'] / $rml['imp_cnt'];
                    $ppi = $rml['price'] / $rml['imp_cnt'];
                }else{
                    $ctr = 0;
                    $loc_ppi = 0;
                    $ppi = 0;
                }

                if($rml['click_cnt'] > 0){
                    $loc_ppc = $rml['loc_price'] / $rml['click_cnt'];
                    $ppc = $rml['price'] / $rml['click_cnt'];
                }else{
                    $loc_ppc = 0;
                    $ppc = 0;
                }

                $mem_com_nm_ = $rml['mem_com_nm_'];

                if($currency == "KRW"){
                    $ppc = number_format(round($loc_ppc, 0));
                    $ppi = number_format(round($loc_ppi, 0));
                    $price = number_format(round($rml['loc_price'], 0));
                }elseif($currency == "USD"){
                    $ppc = number_format(round($ppc, 2), 2);
                    $ppi = number_format(round($ppi, 2), 2);
                    $price = number_format(round($rml['price'], 2), 2);
                }
                
                $mem_no = $rml['mem_no'];

        ?>
                    <tr class="treegrid-1-<?php echo $key?>">
                        <td>
                            <a class="init_color" id="mem_<?php echo $mem_no;?>" href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#mem_<?php echo $mem_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('member', <?php echo $rml['mem_no'];?>, '<?php echo $mem_com_nm_;?>');"><?php echo $rml['mem_com_nm_'];?></a>
                        </td>
                        <td style="text-align:center;padding-right:5px;">
                            <?php
                                if($rml['mem_active_st'] == 1){  
                                    echo lang('strRun');   
                                }elseif($rml['mem_active_st'] == 2){
                                    echo lang('strDormancy');
                                }elseif($rml['mem_active_st'] == 3){
                                    echo lang('strPaused');
                                }elseif($rml['mem_active_st'] == 4){
                                    echo lang('strWithdrawal');
                                }
                            ?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rml['imp_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rml['click_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format(round($ctr, 2), 2);?>%
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppc;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppi;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $price;?>
                        </td>
                    </tr>
                    <?php
                    foreach($this->report_db->select_report_campaign_view($rml['mem_no'], $from_date, $to_date) as $key_=>$rcav){
                        //ctr = 클릭수 / 노출수 * 100
                        //평균 ppc = 총 광고비 / 클릭수
                        //평균 ppi = 총 광고비 / 노출수
                        if($rcav['imp_cnt'] > 0){
                            $ctr = $rcav['click_cnt'] / $rcav['imp_cnt'] * 100;
                            $loc_ppi = $rcav['loc_price'] / $rcav['imp_cnt'];
                            $ppi = $rcav['price'] / $rcav['imp_cnt'];
                        }else{
                            $ctr = 0;
                            $loc_ppi = 0;
                            $ppi = 0;
                        }

                        if($rcav['click_cnt'] > 0){
                            $loc_ppc = $rcav['loc_price'] / $rcav['click_cnt'];
                            $ppc = $rcav['price'] / $rcav['click_cnt'];
                        }else{
                            $loc_ppc = 0;
                            $ppc = 0;
                        }
                        $cam_nm_ = $rcav['cam_nm_'];

                        if($currency == "KRW"){
                            $ppc = number_format(round($loc_ppc, 0));
                            $ppi = number_format(round($loc_ppi, 0));
                            $price = number_format(round($rcav['loc_price'], 0));
                        }elseif($currency == "USD"){
                            $ppc = number_format(round($ppc, 2), 2);
                            $ppi = number_format(round($ppi, 2), 2);
                            $price = number_format(round($rcav['price'], 2), 2);
                        }
                        
                        $cam_no = $rcav['cam_no'];
                    ?>
                    <tr class="treegrid-2-<?php echo $key?>-<?php echo $key_?> treegrid-parent-1-<?php echo $key?>">
                        <td>
                            <a class="init_color" id="cam_<?php echo $cam_no;?>" href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#cam_<?php echo $cam_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('campaign', <?php echo $rcav['cam_no'];?>, '<?php echo $cam_nm_;?>');"><?php echo $rcav['cam_nm_'];?></a>
                        </td>
                        <td style="text-align:center;padding-right:5px;">
                        	
                            <?php          
                                if($rcav['cam_fl'] == "N"){
                                    if($rcav['cam_status'] == 1){
                                        $status = lang('strRun');
                                    }elseif($rcav['cam_status'] == 2){
                                        $status = lang('strReady');
                                    }elseif($rcav['cam_status'] == 3){
                                        $status = lang('strPause');
                                    }elseif($rcav['cam_status'] == 4){
                                        $status = lang('strDone');
                                    }elseif($rcav['cam_status'] == 5){
                                        $status = lang('strPause');
                                        $status_explan = lang('strDailyBudgetLow');
                                    }elseif($rcav['cam_status'] == 6){
                                        $status = lang('strPause');
                                        $status_explan = lang('strCashLow');
                                    }
                                }else{
                                    $status = lang('strDelete');
                                }
                            ?>
                            <?php 
                                if($rcav['cam_status'] == 5 || $rcav['cam_status'] == 6){
                            ?>
                            <a class="btn-link add-tooltip" style="cursor:pointer;color:#515151;" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $status_explan;?>">
                            	<?php echo $status;?>
                            </a>
                            <?php 
                                }else{
                            ?>
                            	<?php echo $status;?>
                            <?php 
                                }
                            ?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rcav['imp_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rcav['click_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format(round($ctr, 2), 2);?>%
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppc;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppi;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $price;?>
                        </td>
                    </tr>
                    <?php
                    foreach($this->report_db->select_report_creative_group_view($rcav['cam_no'], $from_date, $to_date) as $key__=>$rcgv){
                        //ctr = 클릭수 / 노출수 * 100
                        //평균 ppc = 총 광고비 / 클릭수
                        //평균 ppi = 총 광고비 / 노출수
                        if($rcgv['imp_cnt'] > 0){
                            $ctr = $rcgv['click_cnt'] / $rcgv['imp_cnt'] * 100;
                            $loc_ppi = $rcgv['loc_price'] / $rcgv['imp_cnt'];
                            $ppi = $rcgv['price'] / $rcgv['imp_cnt'];
                        }else{
                            $ctr = 0;
                            $loc_ppi = 0;
                            $ppi = 0;
                        }

                        if($rcgv['click_cnt'] > 0){
                            $loc_ppc = $rcgv['loc_price'] / $rcgv['click_cnt'];
                            $ppc = $rcgv['price'] / $rcgv['click_cnt'];
                        }else{
                            $loc_ppc = 0;
                            $ppc = 0;
                        }

                        $cre_gp_nm_ = $rcgv['cre_gp_nm_'];

                        if($currency == "KRW"){
                            $ppc = number_format(round($loc_ppc, 0));
                            $ppi = number_format(round($loc_ppi, 0));
                            $price = number_format(round($rcgv['loc_price'], 0));
                        }elseif($currency == "USD"){
                            $ppc = number_format(round($ppc, 2), 2);
                            $ppi = number_format(round($ppi, 2), 2);
                            $price = number_format(round($rcgv['price'], 2), 2);
                        }
                        
                        $cre_gp_no = $rcgv['cre_gp_no'];
                    ?>
                    <tr class="treegrid-3-<?php echo $key?>-<?php echo $key_?>-<?php echo $key__?> treegrid-parent-2-<?php echo $key?>-<?php echo $key_?>">
                        <td>
                            <a class='init_color' id='cre_gp_<?php echo $cre_gp_no;?>' href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#cre_gp_<?php echo $cre_gp_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('creative_group', <?php echo $rcgv['cre_gp_no'];?>, '<?php echo $cre_gp_nm_?>');"><?php echo $rcgv['cre_gp_nm_'];?></a>
                        </td>
                        <td style="text-align:center;padding-right:5px;">
                            <?php 
                            if($rcgv['cre_gp_fl'] == "N"){
                                if($rcgv['cre_gp_status'] == 1){
                                    $status = lang('strRun');
                                }elseif($rcgv['cre_gp_status'] == 2){
                                    $status = lang('strReady');
                                }elseif($rcgv['cre_gp_status'] == 3){
                                    $status = lang('strPause');
                                }elseif($rcgv['cre_gp_status'] == 4){
                                    $status = lang('strDone');
                                }elseif($rcgv['cre_gp_status'] == 5){
                                    $status = lang('strPause');
                                    $status_explan = lang('strDailyBudgetLow');
                                }elseif($rcgv['cre_gp_status'] == 6){
                                    $status = lang('strPause');
                                    $status_explan = lang('strCashLow');
                                }
                            }else{
                                $status = lang('strDelete');
                            }
                            ?>
                            <?php
                                if($rcgv['cre_gp_status'] == 5 || $rcgv['cre_gp_status'] == 6){
                            ?>
                            <a class="btn-link add-tooltip" style="cursor:pointer;color:#515151;" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $status_explan;?>">
                            	<?php echo $status;?>
                            </a>
                            <?php 
                                }else{
                            ?>
                            	<?php echo $status;?>
                            <?php 
                                }
                            ?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rcgv['imp_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rcgv['click_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format(round($ctr, 2), 2);?>%
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppc;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppi;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $price;?>
                        </td>
                    </tr>
                    <?php
                        foreach($this->report_db->select_report_creative_view($rcgv['cre_gp_no'], $from_date, $to_date) as $key___=>$rcv){
                            //ctr = 클릭수 / 노출수 * 100
                            //평균 ppc = 총 광고비 / 클릭수
                            //평균 ppi = 총 광고비 / 노출수
                            if($rcv['imp_cnt'] > 0){
                                $ctr = $rcv['click_cnt'] / $rcv['imp_cnt'] * 100;
                                $loc_ppi = $rcv['loc_price'] / $rcv['imp_cnt'];
                                $ppi = $rcv['price'] / $rcv['imp_cnt'];
                            }else{
                                $ctr = 0;
                                $loc_ppi = 0;
                                $ppi = 0;
                            }

                            if($rcv['click_cnt'] > 0){
                                $loc_ppc = $rcv['loc_price'] / $rcv['click_cnt'];
                                $ppc = $rcv['price'] / $rcv['click_cnt'];
                            }else{
                                $loc_ppc = 0;
                                $ppc = 0;
                            }

                            $cre_nm_ = $rcv['cre_nm_'];

                            if($currency == "KRW"){
                                $ppc = number_format(round($loc_ppc, 0));
                                $ppi = number_format(round($loc_ppi, 0));
                                $price = number_format(round($rcv['loc_price'], 0));
                            }elseif($currency == "USD"){
                                $ppc = number_format(round($ppc, 2), 2);
                                $ppi = number_format(round($ppi, 2), 2);
                                $price = number_format(round($rcv['price'], 2), 2);
                            }
                            
                            $cre_no = $rcv['cre_no'];
                    ?>
                    <tr class="treegrid-4-<?php echo $key?>-<?php echo $key_?>-<?php echo $key__?>-<?php echo $key___?> treegrid-parent-3-<?php echo $key?>-<?php echo $key_?>-<?php echo $key__?>">
                        <td>
                            <a class="init_color" id='cre_<?php echo $cre_no;?>' href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#cre_<?php echo $cre_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('creative', <?php echo $rcv['cre_no'];?>, '<?php echo $cre_nm_?>');"><?php echo $rcv['cre_nm_'];?></a>
                        </td>
                        
                        <td style="text-align:center;padding-right:5px;">
                            <?php    
                                if($rcv['cre_fl'] == "N"){
                                    if($rcv['cre_status'] == 1){
                                        $status = lang('strRun');
                                    }elseif($rcv['cre_status'] == 2){
                                        $status = lang('strReady');
                                    }elseif($rcv['cre_status'] == 3){
                                        $status = lang('strPause');
                                    }elseif($rcv['cre_status'] == 4){
                                        $status = lang('strDone');
                                    }elseif($rcv['cre_status'] == 5){
                                        $status = lang('strPause');
                                        $status_explan = lang('strDailyBudgetLow');
                                    }elseif($rcv['cre_status'] == 6){
                                        $status = lang('strPause');
                                        $status_explan = lang('strCashLow');
                                    }   
                                }else{
                                    $status = lang('strDelete');
                                }
                            ?>
                        	<?php
                            if($rcv['cre_status'] == 5 || $rcv['cre_status'] == 6){
                            ?>
                            <a class="btn-link add-tooltip" style="cursor:pointer;color:#515151;" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $status_explan;?>">
                            	<?php echo $status;?>
                            </a>
                            <?php 
                                }else{
                            ?>
                            	<?php echo $status;?>
                            <?php 
                                }
                            ?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rcv['imp_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format($rcv['click_cnt']);?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo number_format(round($ctr, 2), 2);?>%
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppc;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $ppi;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $price;?>
                        </td>
                    </tr>
                        <?php
                            }
                        ?>
                    <?php
                        }
                    ?>
                <?php
                    }
                ?>
        <?php
            }
        ?>
                </tbody>
            </table>
        </form>
    </div>
    <div id="report_operation_detail"></div>
</div>

<script type="text/javascript">

    function sel_fromto_date(range){
        var frm = document.report_operation;
        frm.action = "/report/report_operation";
        frm.range.value = range;
        frm.submit();
    }

    function report_operation_detail(data_type, data_key, data_name, per_page, page_num, imp, click, ctr, ppc, ppi, price ){

        if(page_num == null){
            page_num = 1;
        }

        $.ajax({
            url : "/report/report_operation_detail/"+page_num,
            dataType : "html",
            beforeSend: function() {
                $('#report_operation_detail').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {data_type: data_type, data_key: data_key, daterange: '<?php echo $fromto_date;?>', per_page: per_page, data_name: data_name, option_imp: imp, option_click: click, option_ctr: ctr, option_ppc: ppc, option_ppi: ppi, option_price: price },   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
                $("#report_operation_detail").html(result);
            }
        });
    }
</script>
<div class="modal" id="modal_report" sales="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content" id="modal_report_content"></div>
    </div>
</div>
<?php
    $mem_no = $this->session->userdata('mem_no');

    $temp_date = explode(' ~ ', $fromto_date);
    $from_date = $temp_date[0];
    $to_date = $temp_date[1];

    $sitr_lang = $this->session->userdata('site_lang');

    if($sitr_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }

    if($currency == "KRW"){
        $currency_unit = "₩";
    }elseif($currency == "USD"){
        $currency_unit = "$";
    }
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strReport');?></a></li>
			<li class="active"><?php echo lang('strSalesReport');?></li>
		</ol>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"> 
			<a href="javascript:report_sales_detail('sales', '', $('#daterange').val(), 'day', 'N', 'N', 'N', 'N', 'N', 'Y');$('#sales').html('');" style="cursor:pointer;">
			<?php echo lang('strSalesReport');?>
			</a>
			<span id="sales"></span>
		</h3>
	</div>
	<!--요기 -->
	<div class="panel-body pa_t0">
        <div class="clear bd_eb" id="report"">
        </div>
	</div>
    <div id="report_sales_detail"></div>
</div>

<script type="text/javascript">

    $("#modal_report").on('hidden.bs.modal', function () {
        $('#modal_report_content').html("");
        $(this).data('bs.modal', null);
    });

    function report_daily_form(datatype, datakey, daterange, term, imp, click, ctr, ppc, ppi, price){  
        
        $.ajax({
            type:"POST",
            url:"/report/report_daily_form",
            data : {datatype: datatype, datakey: datakey, daterange: daterange, term: term, option_imp: imp, option_click: click, option_ctr: ctr, option_ppc: ppc, option_ppi: ppi, option_price: price},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success: function (data){
            	$('#modal_report_content').html("");
            	$("#modal_report").modal("show");  
                $('#modal_report_content').append(data);           
            }
        });
    }

    function report_daily_reset(datatype, datakey, daterange, term, imp, click, ctr, ppc, ppi, price){
    	
        $.ajax({
            type:"POST",
            url:"/report/report_daily_form",
            data : {datatype: datatype, datakey: datakey, daterange: daterange, term: term, option_imp: imp, option_click: click, option_ctr: ctr, option_ppc: ppc, option_ppi: ppi, option_price: price},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success: function (data){
            	$('#modal_report_content').html("");
                $('#modal_report_content').append(data);
            }
        });
    }
    
    function report_sales_detail(datatype, datakey, daterange, term, imp, click, ctr, ppc, ppi, price){

        $.ajax({
            url : "/report/report_sales_detail/",
            dataType : "html",
            beforeSend: function() {
                $('#report_sales_detail').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {datatype: datatype, datakey: datakey, daterange: daterange, term: term, option_imp: imp, option_click: click, option_ctr: ctr, option_ppc: ppc, option_ppi: ppi, option_price: price},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
				if(datakey != ''){
            		$("#sales").html(" > "+datakey); 
				}
                $("#report_sales_detail").html(result);
            }
        });
    }

    $(function () {
    	report_sales_detail('sales', '', '<?php echo $daterange?>', '<?php echo $term?>', 'N', 'N', 'N', 'N', 'N', 'Y');
    });

 
</script>
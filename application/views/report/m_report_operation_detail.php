<?php

function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
    if ($force_number) {
        $number = preg_replace('/^[^\d]+/', '', $number);
    } else if (preg_match('/^[^\d]+/', $number)) {
        return false;
    }
    $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
    $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
    settype($number, $type);
    return $number;
}

$site_lang = $this->session->userdata('site_lang');

if($site_lang == "korean"){
    $currency = "KRW";
}else{
    $currency = "USD";
}

if($currency == "KRW"){
    $currency_unit = "₩";
    $currency_loc_unit = "원";
}elseif($currency == "USD"){
    $currency_unit = "$";
    $currency_loc_unit = "달러";
}


if($data_type == "member"){
    $info_type = "[광고주]";
    $info_name = $report_info[0]['mem_com_nm'];
    $info_detail = "";
}elseif($data_type == "campaign"){
    $info_type = "[캠페인]";
    $info_name = $mem_com_nm.' <i class="fa pa_b3 fa-angle-right"></i> '.$report_info[0]['cam_nm'];

    if($report_info[0]['daily_budget'] == "" || $report_info[0]['daily_budget'] == 0){
        $daily_budget = "설정안함";
    }else{
        $daily_budget = number_format($report_info[0]['daily_budget'])."원";
    }
    $info_detail = "<li class='sub_tit_blit point_dgray'>일허용예산 : ".$daily_budget."</li>";
}elseif($data_type == "creative_group"){
    $info_type = "[광고그룹]";
    $info_name = $mem_com_nm.' <i class="fa pa_b3 fa-angle-right"></i> '.$cam_nm.' <i class="fa pa_b3 fa-angle-right"></i> '.$report_info[0]['cre_gp_nm'];


    if($report_info[0]['daily_budget'] == "" || $report_info[0]['daily_budget'] == 0){
        $daily_budget = "설정안함";
    }else{
        $daily_budget = number_format($report_info[0]['daily_budget'])."원";
    }

    if($report_info[0]['bid_type'] == 1){
        $info_bid_type = "PC";
    }elseif($report_info[0]['bid_type'] == 2){
        $info_bid_type = "모바일웹";
    }elseif($report_info[0]['bid_type'] == 3){
        $info_bid_type = "모바일앱";
    }

    if($currency == "KRW"){
        $info_bid_price = $report_info[0]['bid_loc_price'];
    }elseif($currency == "USD"){
        $info_bid_price = $report_info[0]['bid_price'];
    }

    $info_detail = "<li class='sub_tit_blit point_dgray'>일허용예산 : ".$daily_budget."</li>";
    $info_detail .= "<li class='sub_tit_blit point_dgray'>광고타입 : ".$info_bid_type."</li>";
    $info_detail .= "<li class='sub_tit_blit point_dgray'>대표입찰가 : ".number_format($info_bid_price).$currency_loc_unit."</li>";

}elseif($data_type == "creative"){
    $info_type = "[광고]";
    $info_name = $mem_com_nm.' <i class="fa pa_b3 fa-angle-right"></i> '.$cam_nm.' <i class="fa pa_b3 fa-angle-right"></i> '.$cre_gp_nm.' <i class="fa pa_b3 fa-angle-right"></i> '.$report_info[0]['cre_nm'];

    if($report_info[0]['cre_type'] == 1){
        $info_cre_type = "Image";
    }elseif($report_info[0]['cre_type'] == 2){
        $info_cre_type = "Flash";
    }elseif($report_info[0]['cre_type'] == 3){
        $info_cre_type = "Text";
    }

    $info_detail = "<li class='sub_tit_blit point_dgray'>광고형태 : ".$info_cre_type."</li>";
    $info_detail .= "<li class='sub_tit_blit point_dgray'>광고사이즈 : ".$report_info[0]['cre_width']."x".$report_info[0]['cre_height']."</li>";
}
?>
<h4 class=""><?php echo lang('strDaily');?> <?php echo lang('strReport');?> : <?php echo $info_name?> <span class="f-size-9 point_dgray">(<?php echo lang('strPeriod')?> : <?php echo $fromto_date;?>)</span></h4>
<div class="m_report_de">
    <ul>
        <?php echo $info_detail?>
    </ul>
</div>
    <div class="m_camp2">
        <div class="camp_div3" id="imp_img" onclick="javascipt:select_chart_option('imp');">
        <p class="camp_tit"><?php echo lang('strImpressions');?></p><span class="camp_con" id="sum_imp_cnt">0</span>
        </div>
        <div class="camp_div3" id="click_img" onclick="javascipt:select_chart_option('click');">
        <p class="camp_tit"><?php echo lang('strClicks');?></p><span class="camp_con" id="sum_click_cnt">0</span>
        </div>
        <div class="camp_div3" id="price_img" onclick="javascipt:select_chart_option('price');">
        <p class="camp_tit"><?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)</p><span class="camp_con" id="sum_price">0</span>
        </div>
    </div>

<div id="report_operation_chart"></div>


<div class="clear over_hidden">
	<div class="ma_b10">
        <form name="report_operation_detail" method="post">
            <input type="hidden" name="imp_val" id="imp_val" value="<?php echo $option_imp?>">
            <input type="hidden" name="click_val" id="click_val" value="<?php echo $option_click?>">
            <input type="hidden" name="price_val" id="price_val" value="<?php echo $option_price?>">
            <table id="list" name="list" class="m_report_tb" data-horizontal-width="100%" cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="12%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="9%">
                    <col width="9%">
                    <col width="10%">
                </colgroup>
                <thead>
                    <tr>
                        <th>
                            일자
                        </th>
                        <th>
                            노출수
                        </th>
                        <th>
                            클릭수
                        </th>
                        <th>
                            CTR
                        </th>
                        <th>
                            PPC <br />(<?php echo $currency_unit?>)
                        </th>
                        <th>
                            PPI <br />(<?php echo $currency_unit?>)
                        </th>
                        <th>
                            총 광고비 <br />(<?php echo $currency_unit?>)
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $chart_data = array();
                        $sum_imp_cnt = 0;
                        $sum_click_cnt = 0;
                        $sum_loc_price = 0;
                        $sum_price = 0;
                        $count = count($report_list);
                        $sum_ctr = 0;
                        $sum_loc_ppi = 0;
                        $sum_ppi = 0;
                        $sum_loc_ppc = 0;
                        $sum_ppc = 0;
    
                        foreach($report_list as $key=>$rl){
                            //ctr = 클릭수 / 노출수 * 100
                            //평균 ppc = 총 광고비 / 클릭수
                            //평균 ppi = 총 광고비 / 노출수
                            if($rl['imp_cnt'] > 0){
                                $ctr = $rl['click_cnt'] / $rl['imp_cnt'] * 100;
                                $loc_ppi = $rl['loc_price'] / $rl['imp_cnt'];
                                $ppi = $rl['price'] / $rl['imp_cnt'];
                            }else{
                                $ctr = 0;
                                $loc_ppi = 0;
                                $ppi = 0;
                            }
    
                            if($rl['click_cnt'] > 0){
                                $loc_ppc = $rl['loc_price'] / $rl['click_cnt'];
                                $ppc = $rl['price'] / $rl['click_cnt'];
                            }else{
                                $loc_ppc = 0;
                                $ppc = 0;
                            }
    
                            $sum_imp_cnt += $rl['imp_cnt'];
                            $sum_click_cnt += $rl['click_cnt'];
                            $sum_loc_price += $rl['loc_price'];
                            $sum_price += $rl['price'];
    
                            if($sum_imp_cnt > 0){
                                $sum_ctr = $sum_click_cnt / $sum_imp_cnt * 100;
                                $sum_loc_ppi = $sum_loc_price / $sum_imp_cnt;
                                $sum_ppi = $sum_price / $sum_imp_cnt;
                            }else{
                                $sum_ctr = 0;
                                $sum_loc_ppi = 0;
                                $sum_ppi = 0;
                            }
    
                            if($sum_click_cnt > 0){
                                $sum_loc_ppc = $sum_loc_price / $sum_click_cnt;
                                $sum_ppc = $sum_price / $sum_click_cnt;
                            }else{
                                $sum_loc_ppc = 0;
                                $sum_ppc = 0;
                            }
    
                            $result_imp_cnt = number_format($rl['imp_cnt']);
                            $result_click_cnt = number_format($rl['click_cnt']);
                            $result_ctr = number_format(round($ctr, 2), 2);
    
                            if($currency == "KRW"){
                                $result_ppc = number_format(round($loc_ppc, 0));
                                $result_ppi = number_format(round($loc_ppi, 0));
                                $result_price = number_format(round($rl['loc_price'], 0));
                            }elseif($currency == "USD"){
                                $result_ppc = number_format(round($ppc, 2), 2);
                                $result_ppi = number_format(round($ppi, 2), 2);
                                $result_price = number_format(round($rl['price'], 2), 2);
                            }
    
                            $chart_data[$key]['date_ymd'] = $rl['date_ymd'];
                            $chart_data[$key]['imp_cnt'] = $result_imp_cnt;
                            $chart_data[$key]['click_cnt'] = $result_click_cnt;
                            $chart_data[$key]['ctr'] = $result_ctr;
                            $chart_data[$key]['ppc'] = $result_ppc;
                            $chart_data[$key]['ppi'] = $result_ppi;
                            $chart_data[$key]['price'] = $result_price;
                    ?>
                    <tr>
                        <td style="text-align:center;padding-right:5px;">
                            <?php echo substr($rl['date_ymd'],-5)?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_imp_cnt;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_click_cnt;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_ctr;?>%
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_ppc;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_ppi;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_price;?>
                        </td>
                    </tr>
                    <?php
                        }
                        if($currency == "KRW"){
                            if($count > 0){
                                $avg_price = number_format(round($sum_loc_price / $count, 0));
                            }else{
                                $avg_price = 0;
                            }
                        }elseif($currency == "USD"){
                            if($count > 0){
                                $avg_price = number_format(round($sum_price / $count, 2), 2);
                            }else{
                                $avg_price = 0;
                            }
                        }
    
                    ?>
                    <tr>
                        <td style="text-align:center;padding-right:5px;">
                            평균
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php
                                if($count > 0){
                                    echo number_format(round($sum_imp_cnt / $count, 0));
                                }else{
                                    echo "0";
                                }
                            ?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php
                                if($count > 0){
                                    echo number_format(round($sum_click_cnt / $count, 0));
                                }else{
                                    echo "0";
                                }
                            ?>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            -
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $avg_price;?>
                        </td>
                    </tr>
                    <tr>
                        <?php
                            $result_sum_imp_cnt = number_format(round($sum_imp_cnt, 0));
                            $result_sum_click_cnt = number_format(round($sum_click_cnt, 0));
                            $result_sum_ctr = number_format(round($sum_ctr, 2), 2);
    
                            if($currency == "KRW"){
                                $result_sum_ppc = number_format(round($sum_loc_ppc, 0));
                                $result_sum_ppi = number_format(round($sum_loc_ppi, 0));
                                $result_sum_price = number_format(round($sum_loc_price, 0));
                            }elseif($currency == "USD"){
                                $result_sum_ppc = number_format(round($sum_ppc, 2), 2);
                                $result_sum_ppi = number_format(round($sum_ppi, 2), 2);
                                $result_sum_price = number_format(round($sum_price, 2), 2);
                            }
                        ?>
                        <td style="text-align:center;padding-right:5px;">
                            합계
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_sum_imp_cnt;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_sum_click_cnt;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_sum_ctr;?>%
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_sum_ppc;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_sum_ppi;?>
                        </td>
                        <td style="text-align:right;padding-right:5px;">
                            <?php echo $result_sum_price;?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>

<script type="text/javascript">
                        
$(document).ready(function() {
    $('#sum_imp_cnt').html('<?php echo $result_sum_imp_cnt?>');
    $('#sum_click_cnt').html('<?php echo $result_sum_click_cnt?>');
    $('#sum_price').html('<?php echo $result_sum_price?>');

    if($('#imp_val').val() == "Y"){
        $('#imp_text').css('color', '#4db4ff');
        $('#imp_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
    }else{
        $('#imp_text').css('color', '#d7d7d7');
        $('#imp_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
    }
    if($('#click_val').val() == "Y"){
        $('#click_text').css('color', '#a783cc');
        $('#click_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #a783cc"});
    }else{
        $('#click_text').css('color', '#d7d7d7');
        $('#click_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
    }
    if($('#price_val').val() == "Y"){
        $('#price_text').css('color', '#e87a75');
        $('#price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
    }else{
        $('#price_text').css('color', '#d7d7d7');
        $('#price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
    }
});

function select_chart_option(kind){

	 if(kind == "imp"){
         if($('#imp_val').val() == "Y"){
             $('#imp_val').attr('value', 'N');
             $('#imp_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
         }else{
             $('#imp_val').attr('value', 'Y');
             $('#imp_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #43acfb"});
         }
     }else if(kind == "click"){
         if($('#click_val').val() == "Y"){
             $('#click_val').attr('value', 'N');
             $('#click_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
         }else{
             $('#click_val').attr('value', 'Y');
             $('#click_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #a780ca"});
         }
     }else if(kind == "price"){
         if($('#price_val').val() == "Y"){
             $('#price_val').attr('value', 'N');
             $('#price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
         }else{
             $('#price_val').attr('value', 'Y');
             $('#price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #c85757"});
         }
     }

    var imp = $('#imp_val').val();
    var click = $('#click_val').val();
    var price = $('#price_val').val();

    chart_option(imp, click, price);
}

    function chart_option(imp, click, price){
        parent.report_operation_detail('<?php echo $data_type?>', <?php echo $data_key?>, '<?php echo $data_name?>', <?php echo $per_page?>, <?php echo $page_num?>, imp, click, price);

    }
</script>
<?php
    $kind = "day";
?>
<script type="text/javascript">
    $(function () {
        $('#report_operation_chart').highcharts({
            chart:{
                type:'spline',
                height: 250
            },
            title: {
                style: {
                    fontSize: '17px',
                    fontFamily: 'Verdana, sans-serif'
                },
                text: '',
                x: 20
            },
            subtitle: {
                text: '',
                x: 20
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    events: {},
                    lineWidth: 2,
                    marker: {
                        radius:3,
                        symbol:'circle'
                    }
                }
            },
            xAxis: {
                <?php
                    if(count($chart_data) >= "30"){
                ?>
                tickInterval: 7,
                <?php
                    }
                ?>
                <?php
                    if($kind != "week"){
                ?>
                categories: [
                    <?php
                        foreach($chart_data as $key=>$cd){
                    ?>
                        '<?php echo $cd['date_ymd']?>',
                    <?php
                        }
                    ?>
                     ],
                <?php
                    }else{
                ?>
                categories: [
                    <?php
                        foreach ($chart_data as $key=>$cd) {
                            $year = substr($cd['date_ymd'], 0, 4);  // 2000
                            $month = substr($cd['date_ymd'], 5, 2);  // 1, 2, 3, ..., 12
                            $day = substr($cd['date_ymd'], 8, 2);  // 1, 2, 3, ..., 31
                            $cur_day = date("w", mktime(0, 0, 0, $month, $day, $year));
                            $minus_day = 6 - $cur_day;
                            $week_first = date("Y-m-d", mktime(0, 0, 0, $month, $day - $cur_day, $year));
                            $week_last  = date("Y-m-d", mktime(0, 0, 0, $month, $day + $minus_day, $year));
                    ?>
                    '<?php echo $cd['date_ymd']?>~<?php echo $week_last?>',
                    <?php
                        }
                    ?>
                    ],
                <?php
                    }
                ?>
                gridLineWidth: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    },
                    overflow: 'justify'
                }
                },
                yAxis: [
                    {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {

                        enabled: false,

                        style: {
                            color: '#43acfb',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: '노출수',
                        align: 'low',
                        rotation: 0,
                        x: -31,
                        y: 25
                    },
                    style: {
                        color: '#43acfb'
                    },
                    format:'{value}'
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {

                        enabled: false,

                        overflow: 'justify',
                        style: {
                            color: '#a780ca',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: '클릭수',
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#a780ca'
                    },
                    format:'{value}'
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    gridLineColor: '#e6e6e6',
                    labels: {

                        enabled: true,

                        overflow: 'justify',
                        style: {
                            color: "#c85757",
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: '총 광고비 (<?php echo $currency_unit?>)',
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: "#c85757"
                    }
                }
            ],
            tooltip: {
                headerFormat: '<span style="font-size: 12px"><b>{point.key}</b></span><br/>',
                xDateFormat: '%Y-%m-%d',
                shared: true

            },
            legend: {
                enabled:false,
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: [
                <?php
                    if($option_imp == 'Y'){
                ?>
                {
                name: '노출수',
                yAxis: 0,
                zIndex: 0,
                color: '#43acfb',
                data: [
                <?php
                    foreach($chart_data as $key=>$cd){
                        echo number_unformat($cd['imp_cnt']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
                <?php
                    if($option_click == 'Y'){
                ?>
                {
                name: '클릭수',
                zIndex:1,
                yAxis: 1,
                color: '#a780ca',
                data: [
                <?php
                    foreach($chart_data as $key=>$cd) {
                        echo number_unformat($cd['click_cnt']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
               
                <?php
                    if($option_price == 'Y'){
                ?>
                {
                name: '총 광고비 (<?php echo $currency_unit?>)',
                yAxis: 2,
                zIndex: 2,
                color: "#c85757",
                data: [
                <?php
                    foreach($chart_data as $key=>$cd){
                        echo number_unformat($cd['price']);
                ?>,
                <?php
                    }
                ?>
                ]
                },
                <?php
                    }
                ?>
            ]
        });
    });
</script>
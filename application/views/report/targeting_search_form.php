<table class="table table-bordered aut_tb">
    <colgroup>
        <col width="10%">
        <col width="20%">
        <col width="20%">
        <col width="20%">
        <col width="20%">
    </colgroup>
    <tr>
    	<th><?php echo lang('strTargeting');?></th>
       	<th><?php echo lang('strAdvertiser');?></th> 	
       	<th><?php echo lang('strCampaign');?></th> 	
       	<th><?php echo lang('strAdGroup');?></th>
       	<th><?php echo lang('strAd');?></th> 	
    </tr>
    <tr>
        <td>
           <select id="targeting" name="targeting" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                <option value="" <?php if($targeting == ""){ echo 'selected';}?>><?php echo lang('strAll');?></option>
                <option value="time" <?php if($targeting == "time"){ echo 'selected';}?>><?php echo lang('strDaynTime');?></option>
            	<option value="os" <?php if($targeting == "os"){ echo 'selected';}?>><?php echo lang('strOperatingSystem');?></option>
                <option value="browser" <?php if($targeting == "browser"){ echo 'selected';}?>><?php echo lang('strBrowser');?></option>
                <option value="device" <?php if($targeting == "device"){ echo 'selected';}?>><?php echo lang('strDevice');?></option>
                <option value="category" <?php if($targeting == "category"){ echo 'selected';}?>><?php echo lang('strCategory');?></option>
            </select>
        </td>
        <td>
        	<select id="adver_no" name="adver_no" class="form-control-static input-sm wid_100p" onchange="parent.targeting_search_form($('#targeting').val(), $('#adver_no').val(), '', '', '')">
        		<?php 
                    $logged_in = $this->session->userdata('logged_in');
                    if($logged_in['mem_type'] == "master"){
        		?>
        		<option value="" <?php if($adver_no == ""){ echo 'selected';}?>><?php echo lang('strAll');?></option>
        		<?php 
                    }
        		?>
        		<?php 
        		  foreach($advertiser_list as $adver){
        		?>
        		<option value="<?php echo $adver['mem_no']?>" <?php if($adver_no == $adver['mem_no']){ echo 'selected';}?>><?php echo $adver['mem_com_nm']?></option>
        		<?php 
        		  }
        		?>
        	</select>
        </td>
        <td>
        	<select id="cam_no" name="cam_no" class="form-control-static input-sm wid_100p bg_w" onchange="parent.targeting_search_form($('#targeting').val(), '<?php echo $adver_no;?>', $('#cam_no').val(), '', '')">
        		<option value="" <?php if($cam_no == ""){ echo 'selected';}?>><?php echo lang('strAll');?></option>
        		<?php 
        		  foreach($campaign_list as $cam){
        		?>
        		<option value="<?php echo $cam['cam_no']?>" <?php if($cam_no == $cam['cam_no']){ echo 'selected';}?> ><?php echo $cam['cam_nm']?></option>
        		<?php 
        		  }
        		?>
        	</select>
        </td>
        <td>
        	<select id="cre_gp_no" name="cre_gp_no" class="form-control-static input-sm wid_100p bg_w" onchange="parent.targeting_search_form($('#targeting').val(), '<?php echo $adver_no;?>', '<?php echo $cam_no;?>', $('#cre_gp_no').val(), '')">
        		<option value="" <?php if($cre_gp_no == ""){ echo 'selected';}?>><?php echo lang('strAll');?></option>
        		<?php 
        		  foreach($creative_group_list as $cre_gp){
        		?>
        		<option value="<?php echo $cre_gp['cre_gp_no']?>" <?php if($cre_gp_no == $cre_gp['cre_gp_no']){ echo 'selected';}?> ><?php echo $cre_gp['cre_gp_nm']?></option>
        		<?php 
        		  }
        		?>
        	</select>
        </td>
        <td>
        	<select id="cre_no" name="cre_no" class="form-control-static input-sm wid_100p bg_w">
        		<option value="" <?php if($cre_no == ""){ echo 'selected';}?>><?php echo lang('strAll');?></option>
        		<?php 
        		  foreach($creative_list as $cre){
        		?>
        		<option value="<?php echo $cre['cre_no']?>" <?php if($cre_no == $cre['cre_no']){ echo 'selected';}?> ><?php echo $cre['cre_nm']?></option>
        		<?php 
        		  }
        		?>
        	</select>
        </td>
    </tr>
</table>

<script type="text/javascript">
	$('#targeting option:eq(1)').attr('selected', 'true');
</script>
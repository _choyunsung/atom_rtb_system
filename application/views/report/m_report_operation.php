<link href="/css/jquery.treegrid.css" rel="stylesheet" type="text/css" />
<script src="/js/jquery.treegrid.js"></script>
<script src="/js/jquery.treegrid.bootstrap3.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.tree').treegrid({
        expanderExpandedClass : 'glyphicon glyphicon-minus',
        expanderCollapsedClass : 'glyphicon glyphicon-plus',
        initialState : 'collapsed',
    });
});
</script>
<?php 
$today = lang('strToday').": ".date("Y-m-d");
$yesterday = lang('strYesterday').": ".date("Y-m-d", strtotime("-1 day"));
$last7 = lang('strLast7').": ".date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
$last30 = lang('strLast30').": ".date("Y-m-d", strtotime("-30 day")) . " ~ " . date("Y-m-d");
$this_month = lang('strThisMonth').": ".date("Y-m-01") . " ~ " . date("Y-m-t") ;
$last_month = lang('strLastMonth').": ".date("Y-m-01", strtotime("-1 month")) . " ~ " . date("Y-m-t", strtotime("-1 month"));

if ($range == "today"){
    $fromto_date_view = $today;
}elseif ($range == "yesterday"){
    $fromto_date_view = $yesterday;
}elseif ($range == "last7"){
    $fromto_date_view = $last7;
}elseif ($range == "last30"){
    $fromto_date_view = $last30;
}elseif ($range == "thismonth"){
    $fromto_date_view = $this_month;
}elseif ($range == "lastmonth"){
    $fromto_date_view = $last_month;
}else{
    $fromto_date_view = $last7;
}

?>
<div class="m_wrap ma_b20 pa_b20">
    <form name="m_report" method="post">
        <input type="hidden" name="range" id="range" value="<?php echo $range;?>">
    	<div class="ma_b20">
    		<div class="btn-group float_l wid_100p ma_b10">
    			<span class="btn btn-default txt-left ma_r5 wid_100p dropdown-toggle" data-toggle="dropdown">
    				<?php echo $fromto_date_view;?>
    				<i class="dropdown-caret fa float_r pa_r10 pa_t3 fa-caret-down"></i>
    			</span>
    			<ul class="dropdown-menu ul_sel_box wid_100p">
    			    <li><a href="javascript:sel_fromto_date('today')"><?php echo $today?></a></li>
    			    <li><a href="javascript:sel_fromto_date('yesterday')"><?php echo $yesterday?></a></li>
    			    <li><a href="javascript:sel_fromto_date('last7')"><?php echo $last7?></a></li>
                    <li><a href="javascript:sel_fromto_date('last30')"><?php echo $last30?></a></li>
                    <li><a href="javascript:sel_fromto_date('thismonth')"><?php echo $this_month?></a></li>
                    <li><a href="javascript:sel_fromto_date('lastmonth')"><?php echo $last_month?></a></li>
                    
                </ul>
    		</div>
    	</div>
    	<div class="m_camp_info over_hidden ma_b5">
    		<div class="btn-group float_l wid_50p">
    			<input type="text" value=" 전체검색 " class="wid_100p"> 
    		</div>
    		<div class="btn-group float_l">
    			<button class="float_r btn btn-default">
    				<i class="fa fa-search"></i>
    			</button>
    		</div>
    		<div class="btn-group float_r">
    			<button class="float_r btn btn-default">
    				<i class="fa fa-arrow-down "></i>
    			</button>
    		</div>
    	</div>
	</form>
	<div class="clear over_hidden">
		<div class="ma_b20">
    		<form name="report_operation"  method="post">
    		    <table id="list" name="list" class="table tree table-striped table-bordered table-hover datatable table-tabletools scroll m_report_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
    				<colgroup>
    					<col width="70%">
    					<col width="30%">
    				</colgroup>
                    <thead>
                        <tr>
                            <th>
                                광고주
                            </th>
                            <th>
                                상태
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($report_member_list as $key=>$rml){
                                $mem_com_nm_ = $rml['mem_com_nm_'];
                                $mem_no = $rml['mem_no'];
                        ?>
                        <tr class="treegrid-1-<?php echo $key?>">
                            <td>
                                <a class="init_color" id="mem_<?php echo $mem_no;?>" href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#mem_<?php echo $mem_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('member', <?php echo $rml['mem_no'];?>, '<?php echo $mem_com_nm_;?>');"><?php echo $rml['mem_com_nm_'];?></a>
                            </td>
                            <td style="text-align:center;padding-right:5px;">
                                <?php
                                    if($rml['mem_active_st'] == 1){  
                                        echo "진행";   
                                    }elseif($rml['mem_active_st'] == 2){
                                        echo "일시정지";
                                    }elseif($rml['mem_active_st'] == 3){
                                        echo "삭제";
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php
                            foreach($this->report_db->select_report_campaign_view($rml['mem_no'], $from_date, $to_date) as $key_=>$rcav){
                                $cam_nm_ = $rcav['cam_nm_'];
                                $cam_no = $rcav['cam_no'];
                        ?>
                        <tr class="treegrid-2-<?php echo $key?>-<?php echo $key_?> treegrid-parent-1-<?php echo $key?>">
                            <td>
                                <a class="init_color" id="cam_<?php echo $cam_no;?>" href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#cam_<?php echo $cam_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('campaign', <?php echo $rcav['cam_no'];?>, '<?php echo $cam_nm_;?>');"><?php echo $rcav['cam_nm_'];?></a>
                            </td>
                            <td style="text-align:center;padding-right:5px;">
                                <?php                             
                                    if($rcav['cam_status'] == 1){
                                        $status = lang('strRun');
                                    }elseif($rcav['cam_status'] == 2){
                                        $status = lang('strReady');
                                    }elseif($rcav['cam_status'] == 3){
                                        $status = lang('strPause');
                                    }elseif($rcav['cam_status'] == 4){
                                        $status = lang('strDone');
                                    }elseif($rcav['cam_status'] == 5){
                                        $status = lang('strPause');
                                        $status_explan = lang('strDailyBudgetLow');
                                    }elseif($rcav['cam_status'] == 6){
                                        $status = lang('strPause');
                                        $status_explan = lang('strCashLow');
                                    }
                                ?>
                                <?php 
                                    if($rcav['cam_status'] == 5 || $rcav['cam_status'] == 6){
                                ?>
                                <a class="btn-link add-tooltip" style="cursor:pointer;color:#515151;" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $status_explan;?>">
                                	<?php echo $status;?>
                                </a>
                                <?php 
                                    }else{
                                ?>
                                	<?php echo $status;?>
                                <?php 
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php
                            foreach($this->report_db->select_report_creative_group_view($rcav['cam_no'], $from_date, $to_date) as $key__=>$rcgv){
                                $cre_gp_nm_ = $rcgv['cre_gp_nm_'];
                                $cre_gp_no = $rcgv['cre_gp_no'];
                        ?>
                        <tr class="treegrid-3-<?php echo $key?>-<?php echo $key_?>-<?php echo $key__?> treegrid-parent-2-<?php echo $key?>-<?php echo $key_?>">
                            <td>
                                <a class='init_color' id='cre_gp_<?php echo $cre_gp_no;?>' href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#cre_gp_<?php echo $cre_gp_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('creative_group', <?php echo $rcgv['cre_gp_no'];?>, '<?php echo $cre_gp_nm_?>');"><?php echo $rcgv['cre_gp_nm_'];?></a>
                            </td>
                            <td style="text-align:center;padding-right:5px;">
                                <?php 
                                if($rcgv['cre_gp_status'] == 1){
                                    $status = lang('strRun');
                                }elseif($rcgv['cre_gp_status'] == 2){
                                    $status = lang('strReady');
                                }elseif($rcgv['cre_gp_status'] == 3){
                                    $status = lang('strPause');
                                }elseif($rcgv['cre_gp_status'] == 4){
                                    $status = lang('strDone');
                                }elseif($rcgv['cre_gp_status'] == 5){
                                    $status = lang('strPause');
                                    $status_explan = lang('strDailyBudgetLow');
                                }elseif($rcgv['cre_gp_status'] == 6){
                                    $status = lang('strPause');
                                    $status_explan = lang('strCashLow');
                                }
                                ?>
                                <?php
                                    if($rcgv['cre_gp_status'] == 5 || $rcgv['cre_gp_status'] == 6){
                                ?>
                                <a class="btn-link add-tooltip" style="cursor:pointer;color:#515151;" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $status_explan;?>">
                                	<?php echo $status;?>
                                </a>
                                <?php 
                                    }else{
                                ?>
                                	<?php echo $status;?>
                                <?php 
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php
                            foreach($this->report_db->select_report_creative_view($rcgv['cre_gp_no'], $from_date, $to_date) as $key___=>$rcv){
                                $cre_nm_ = $rcv['cre_nm_'];
                                $cre_no = $rcv['cre_no'];
                        ?>
                        <tr class="treegrid-4-<?php echo $key?>-<?php echo $key_?>-<?php echo $key__?>-<?php echo $key___?> treegrid-parent-3-<?php echo $key?>-<?php echo $key_?>-<?php echo $key__?>">
                            <td>
                                <a class="init_color" id='cre_<?php echo $cre_no;?>' href="javascript:$('.init_color').css({'color': '#4e5254', 'font-size': '100%'});$('#cre_<?php echo $cre_no;?>').css({'color': '#ff2956', 'font-size': '100%', 'font-weight':'bold'});report_operation_detail('creative', <?php echo $rcv['cre_no'];?>, '<?php echo $cre_nm_?>');"><?php echo $rcv['cre_nm_'];?></a>
                            </td>
                            
                            <td style="text-align:center;padding-right:5px;">
                                <?php    
                                    if($rcv['cre_status'] == 1){
                                        $status = lang('strRun');
                                    }elseif($rcv['cre_status'] == 2){
                                        $status = lang('strReady');
                                    }elseif($rcv['cre_status'] == 3){
                                        $status = lang('strPause');
                                    }elseif($rcv['cre_status'] == 4){
                                        $status = lang('strDone');
                                    }elseif($rcv['cre_status'] == 5){
                                        $status = lang('strPause');
                                        $status_explan = lang('strDailyBudgetLow');
                                    }elseif($rcv['cre_status'] == 6){
                                        $status = lang('strPause');
                                        $status_explan = lang('strCashLow');
                                    }
                                ?>
                            	<?php
                                if($rcv['cre_status'] == 5 || $rcv['cre_status'] == 6){
                                ?>
                                <a class="btn-link add-tooltip" style="cursor:pointer;color:#515151;" data-toggle="tooltip" data-placement="right" data-original-title="<?php echo $status_explan;?>">
                                	<?php echo $status;?>
                                </a>
                                <?php 
                                    }else{
                                ?>
                                	<?php echo $status;?>
                                <?php 
                                    }
                                ?>
                            </td>
                        </tr>
                            <?php
                                }
                            ?>
                        <?php
                            }
                        ?>
                    <?php
                        }
                    ?>
            <?php
                }
            ?>
                    </tbody>
                </table>
            </form>
		</div>
	</div>
	<div class="ma_b10" id = "report_operation_detail" >
	</div>
</div>

<script>
function search(){
    var frm = document.m_report;
    frm.action = "/report/m_report_operation";
    frm.submit();
}

function sel_fromto_date(value){
	$("#range").val(value);
	search();
}

function report_operation_detail(data_type, data_key, data_name, per_page, page_num, imp, click, price ){

    if(page_num == null){
        page_num = 1;
    }

    $.ajax({
        url : "/report/m_report_operation_detail/"+page_num,
        dataType : "html",
        beforeSend: function() {
            $('#report_operation_detail').show().fadeIn('slow');
        },
        type : "post",  // post 또는 get
        data : {data_type: data_type, data_key: data_key, daterange: '<?php echo $fromto_date;?>', per_page: per_page, data_name: data_name, option_imp: imp, option_click: click, option_price: price },   // 호출할 url 에 있는 페이지로 넘길 파라메터
        success : function(result){
            $("#report_operation_detail").html(result);
        }
    });
}
</script>



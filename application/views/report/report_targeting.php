<link href="/css/jquery.treegrid.css" rel="stylesheet" type="text/css" />

<script src="/js/jquery.treegrid.js"></script>
<script src="/js/jquery.treegrid.bootstrap3.js"></script>
<script type="text/javascript">
    <!--
    http://maxazan.github.io/jquery-treegrid/
    //-->
    $(document).ready(function() {
        $('.tree').treegrid({
            expanderExpandedClass : 'glyphicon glyphicon-minus',
            expanderCollapsedClass : 'glyphicon glyphicon-plus',
            initialState : 'collapsed',
        });
    });
</script>
<?php
    $mem_no = $this->session->userdata('mem_no');

    $temp_date = explode(' ~ ', $fromto_date);
    $from_date = $temp_date[0];
    $to_date = $temp_date[1];

    $sitr_lang = $this->session->userdata('site_lang');

    if($sitr_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }

    if($currency == "KRW"){
        $currency_unit = "₩";
    }elseif($currency == "USD"){
        $currency_unit = "$";
    }
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strReport');?></a></li>
			<li class="active"><?php echo lang('strTargetingReport');?></li>
		</ol>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"> <?php echo lang('strTargetingReport');?></h3>
	</div>
	<!--요기 -->
	<div class="panel-body pa_t0">
        <div class="clear bd_eb" id="report"">
        </div>
	</div>
	<div class="panel-heading">
	</div>
	<div class="panel-body">
        <script type="text/javascript">
            $(document).ready(function () {
                $("#export_excel").on('click', function () {
                    var uri = $("#list").battatech_excelexport({
                        containerid: "list"
                        , datatype: 'table'
                        , returnUri: true
                    });
                    $(this).attr('download', 'Report.xls').attr('href', uri).attr('target', '_blank');
                });
            });
        </script>
        <form name="report_targeting"  method="post">
            <input type="hidden" name="range" value="<?php echo $range;?>">
            <!-- 타겟팅 검색부 -->
			<div id="targeting_search_form" name="targeting_search_form"></div>
                
            <!--날짜 검색부 -->
            <div class="clear" id="all_btn_gp" style="padding-bottom:5px;">
                <div class="float_r ma_l10">
                    <a href="javascript:search();" class="btn btn-primary"><?php echo lang('strSearch');?></a>&nbsp;
                    <a href="#" id="export_excel" download="" class="btn btn-default"><?php echo lang('strDownload');?></a>&nbsp;
                </div>
                <a href="javascript:sel_fromto_date('7')" class="btn btn-default"><?php echo lang('strLast7');?></a>&nbsp;
                <a href="javascript:sel_fromto_date('30')" class="btn btn-default"><?php echo lang('strLast30');?></a>&nbsp;
                <a href="javascript:sel_fromto_date('60')" class="btn btn-default"><?php echo lang('strLast60');?></a>&nbsp;
                <a href="javascript:sel_fromto_date('90')" class="btn btn-default"><?php echo lang('strLast90');?></a>&nbsp;
                
                <div id="demo_dp_component" class="camp_report_r" style="width:37%;">
                    <input type="text" name="fromto_date" id="daterange" class="range wid_55p" value="<?php echo $fromto_date?>" />

    				<span class="iput-group-addon">
    					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
    				</span>
                </div>
            </div>
        </form>
    </div>
    <div id="report_targeting_detail"></div>
</div>

<script type="text/javascript">

    function sel_fromto_date(range){
        var frm = document.report_targeting;
        frm.action = "/report/report_targeting";
        frm.range.value = range;
        frm.submit();
    }

    function report_targeting_detail(targeting, data_type, data_key, per_page, page_num){

        if(page_num == null){
            page_num = 1;
        }

        $.ajax({
            url : "/report/report_targeting_detail/"+page_num,
            dataType : "html",
            beforeSend: function() {
                $('#report_targeting_detail').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {data_type: data_type, data_key: data_key, daterange: $('#daterange').val(), per_page: per_page, targeting: targeting },   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
                $("#report_targeting_detail").html(result);
            }
        });
    }


    function search(){
 
    	var targeting = $('#targeting').val();
    	var adver_no = $('#adver_no').val();
    	var cam_no = $('#cam_no').val();
    	var cre_gp_no = $('#cre_gp_no').val();
    	var cre_no = $('#cre_no').val();

		data_type = "creative";
		data_key = cre_no;

    	if(cre_no == ""){
			data_type = "creative_group";
			data_key = cre_gp_no;
    	}

    	if(cre_gp_no == ""){
			data_type = "campaign";
			data_key = cam_no;
    	}
    	
    	if(cam_no == ""){
			data_type = "advertiser";
			data_key = adver_no;
    	}
    	
    	if(adver_no == ""){
			data_type = "targeting";
			data_key = "";
    	}

    	report_targeting_detail(targeting, data_type, data_key, 100, 1);
    }
    
    function targeting_search_form(targeting, adver_no, cam_no, cre_gp_no, cre_no){

        $.ajax({
            url : "/report/targeting_search_form/",
            dataType : "html",
            beforeSend: function() {
                $('#targeting_search_form').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {targeting: targeting, adver_no: adver_no, cam_no: cam_no, cre_gp_no: cre_gp_no, cre_no: cre_no},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
                $("#targeting_search_form").html(result);
            }
        });
    }

    $(function () {
    	targeting_search_form('<?php echo $targeting?>', '<?php echo $mem_no?>', '<?php echo $cam_no?>', '<?php echo $cre_gp_no?>', '<?php echo $cre_no?>');
    	report_targeting_detail('time', 'advertiser', '<?php echo $mem_no;?>', 100, 1);
    });
</script>
<?php
    $mem_no = $this->session->userdata('mem_no');

    $temp_date = explode(' ~ ', $fromto_date);
    $from_date = $temp_date[0];
    $to_date = $temp_date[1];

    $sitr_lang = $this->session->userdata('site_lang');

    if($sitr_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }

    if($currency == "KRW"){
        $currency_unit = "₩";
    }elseif($currency == "USD"){
        $currency_unit = "$";
    }
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strReport');?></a></li>
			<li class="active"><?php echo lang('strIntegratedReport');?></li>
		</ol>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"> <?php echo lang('strIntegratedReport');?></h3>
	</div>
	<!--요기 -->
	<div class="panel-body pa_t0">
        <div class="clear bd_eb" id="report"">
        </div>
	</div>
	<div class="panel-heading">
	</div>
	<div class="panel-body">
        <!--날짜 검색부 -->
        <div class="clear" id="all_btn_gp" style="padding-bottom:5px;">
        	<input type="hidden" name="term" id="term" value="<?php echo $term?>" />
            <div class="float_r ma_l10">
                <a href="javascript:select_term('month')" class="btn float_right btn-default"  id="month_btn"  style="<?php if($term == 'month'){echo "background-color:#dbdbdb";}?>"><?php echo lang("strMonth")?></a>
                <a href="javascript:select_term('week')" class="btn float_right btn-default" id="week_btn" style="<?php if($term == 'week'){echo "background-color:#dbdbdb";}?>"><?php echo lang("strWeek")?></a>
                <a href="javascript:select_term('day')" class="btn float_right btn-default"  id="day_btn" style="<?php if($term == 'day'){echo "background-color:#dbdbdb";}?>"><?php echo lang("strDay")?></a>
            </div>
            <div class="float_r ma_l10">
                <a href="javascript:report_integration_detail($('#daterange').val(), $('#term').val(), $('#imp_val').val(), $('#click_val').val(), $('#ctr_val').val(), $('#ppc_val').val(), $('#ppi_val').val(), $('#price_val').val());" class="btn btn-primary"><?php echo lang('strSearch');?></a>&nbsp;
            </div>
            <div id="demo_dp_component" class="camp_report_r" style="width:37%;">
                <input type="text" name="fromto_date" id="daterange" class="range wid_55p" value="<?php echo $fromto_date?>" />
				<span class="iput-group-addon">
					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
				</span>
            </div>
        </div>
    </div>
    <div id="report_integration_detail"></div>
</div>

<script type="text/javascript">

    function report_integration_detail(daterange, term, imp, click, ctr, ppc, ppi, price){
        $.ajax({
            url : "/report/report_integration_detail/",
            dataType : "html",
            beforeSend: function() {
                $('#report_integration_detail').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {daterange: daterange, term: term, option_imp: imp, option_click: click, option_ctr: ctr, option_ppc: ppc, option_ppi: ppi, option_price: price},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
                $("#report_integration_detail").html(result);
            }
        });
    }

    $(function () {
    	report_integration_detail('<?php echo $fromto_date?>', "day", "N", "N", "N", "N", "N", "Y");
    });


    function select_term(term){
        if(term == "day"){
        	$('#term').attr('value','day');
            $('#day_btn').css('background-Color', '#dbdbdb');
            $('#week_btn').css('background-Color', '#f3f3f3');
            $('#month_btn').css('background-Color', '#f3f3f3');

        }else if(term == "week"){
        	$('#term').attr('value','week');
            $('#day_btn').css('background-Color', '#f3f3f3');
            $('#week_btn').css('background-Color', '#dbdbdb');
            $('#month_btn').css('background-Color', '#f3f3f3');
            
        }else if(term == "month"){
        	$('#term').attr('value','month');
            $('#day_btn').css('background-Color', '#f3f3f3');
            $('#week_btn').css('background-Color', '#f3f3f3');
            $('#month_btn').css('background-Color', '#dbdbdb'); 
        }
        
        report_integration_detail($('#daterange').val(), $('#term').val(), $('#imp_val').val(), $('#click_val').val(), $('#ctr_val').val(), $('#ppc_val').val(), $('#ppi_val').val(), $('#price_val').val());
    
    }
 
    
</script>
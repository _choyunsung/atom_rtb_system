<?php

function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
    if ($force_number) {
        $number = preg_replace('/^[^\d]+/', '', $number);
    } else if (preg_match('/^[^\d]+/', $number)) {
        return false;
    }
    $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
    $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
    settype($number, $type);
    return $number;
}

$site_lang = $this->session->userdata('site_lang');

if($site_lang == "korean"){
    $currency = "KRW";
}else{
    $currency = "USD";
}

if($currency == "KRW"){
    $currency_unit = "₩";
    $currency_loc_unit = lang('strWon');
}elseif($currency == "USD"){
    $currency_unit = "$";
    $currency_loc_unit = lang('strDollar');
}

if($targeting == "os"){
    $info_name = lang('strOperatingSystem');
    $info_detail = "";
}elseif($targeting == "device"){
    $info_name = lang('strDevice');
    $info_detail = "";
}elseif($targeting == "time"){
    $info_name = lang('strDaynTime');
    $info_detail = "";
}elseif($targeting == "browser"){
    $info_name = lang('strBrowser');
    $info_detail = "";
}elseif($targeting == "category"){
    $info_name = lang('strCategory');
    $info_detail = "";    
}
?>
<!-- 리스트 -->
<div class=" ma_t20 ma_l10 ma_b20 pa_l20" style="padding-right:40px;">
	<h4 class=""><?php echo lang('strSummation');?> <?php echo lang('strReport');?> <span class="f-size-9 point_dgray">(<?php echo lang('strPeriod')?> : <?php echo $fromto_date;?>)</span></h4>
	<!-- seonu -->
	<dl class="target_div" onclick="parent.report_targeting_detail('time', '<?php echo $data_type?>', '<?php echo $data_key?>', 100, 1);$('#targeting option:eq(1)').attr('selected', 'true');">
	<dt><?php if($targeting == "time")?> <?php echo lang('strDaynTime');?><i class="float_r bd_squ fa ma_t3 fa-pie-chart ma_r5 <?php if($targeting == "time"){ echo 'point_blue';}?>"></i></dt>
	<dd><?php echo lang('strMaximum');?> <?php echo lang('strImpression')?> <?php echo lang('strDaynTime');?></dd>
	<dd style="<?php if($targeting == 'time'){ echo 'color:#ff2956;';}?>"><strong>
	<?php 
    	if($max_time[0]['day'] == "mon"){
    	    $day = lang('strMonday');
    	}else if($max_time[0]['day'] == "tue"){
    	    $day = lang('strTuesday');
    	}else if($max_time[0]['day'] == "wed"){
    	    $day = lang('strWednesday');
    	}else if($max_time[0]['day'] == "thu"){
    	    $day = lang('strThursday');
    	}else if($max_time[0]['day'] == "fri"){
    	    $day = lang('strFriday');
    	}else if($max_time[0]['day'] == "sat"){
    	    $day = lang('strSaturday');
    	}else if($max_time[0]['day'] == "sun"){
    	    $day = lang('strSunday');
    	}
        if($max_time[0]['time'] == "0"){
            $time = "00:00 ~ 00:59";
        }else if($max_time[0]['time'] == "1"){
            $time = "01:00 ~ 01:59";
        }else if($max_time[0]['time'] == "2"){
            $time = "02:00 ~ 02:59";
        }else if($max_time[0]['time'] == "3"){
            $time = "03:00 ~ 03:59";
        }else if($max_time[0]['time'] == "4"){
            $time = "04:00 ~ 04:59";
        }else if($max_time[0]['time'] == "5"){
            $time = "05:00 ~ 05:59";
        }else if($max_time[0]['time'] == "6"){
            $time = "06:00 ~ 06:59";
        }else if($max_time[0]['time'] == "7"){
            $time = "07:00 ~ 07:59";
        }else if($max_time[0]['time'] == "8"){
            $time = "08:00 ~ 08:59";
        }else if($max_time[0]['time'] == "9"){
            $time = "09:00 ~ 09:59";
        }else if($max_time[0]['time'] == "10"){
            $time = "10:00 ~ 10:59";
        }else if($max_time[0]['time'] == "11"){
            $time = "11:00 ~ 11:59";
        }else if($max_time[0]['time'] == "12"){
            $time = "12:00 ~ 12:59";
        }else if($max_time[0]['time'] == "13"){
            $time = "13:00 ~ 13:59";
        }else if($max_time[0]['time'] == "14"){
            $time = "14:00 ~ 14:59";
        }else if($max_time[0]['time'] == "15"){
            $time = "15:00 ~ 15:59";
        }else if($max_time[0]['time'] == "16"){
            $time = "16:00 ~ 16:59";
        }else if($max_time[0]['time'] == "17"){
            $time = "17:00 ~ 17:59";
        }else if($max_time[0]['time'] == "18"){
            $time = "18:00 ~ 18:59";
        }else if($max_time[0]['time'] == "19"){
            $time = "19:00 ~ 19:59";
        }else if($max_time[0]['time'] == "20"){
            $time = "20:00 ~ 20:59";
        }else if($max_time[0]['time'] == "21"){
            $time = "21:00 ~ 21:59";
        }else if($max_time[0]['time'] == "22"){
            $time = "22:00 ~ 22:59";
        }else if($max_time[0]['time'] == "23"){
            $time = "23:00 ~ 23:59";                   
        }
    
        echo $day."&nbsp;".$time;
    ?>
	</strong></dd>
	</dl>
	<dl class="target_div" onclick="parent.report_targeting_detail('os', '<?php echo $data_type?>', '<?php echo $data_key?>', 100, 1);$('#targeting option:eq(2)').attr('selected', 'true');">
    <dt><?php if($targeting == "os")?><?php echo lang('strOperatingSystem');?><i class="float_r bd_squ fa ma_t3 fa-pie-chart ma_r5 <?php if($targeting == "os"){ echo 'point_blue';}?>"></i></dt>
    <dd><?php echo lang('strMaximum');?> <?php echo lang('strImpression')?> <?php echo lang('strOperatingSystem');?></dd>
    <dd style="<?php if($targeting == 'os'){ echo 'color:red;';}?>"><strong>
	<?php 	
		$OS = array(
            /* OS */
            array('Windows CE', 'Windows CE'),
            array('Win98', 'Windows 98'),
            array('Windows 9x', 'Windows ME'),
            array('Windows me', 'Windows ME'),
            array('Windows 98', 'Windows 98'),
            array('Windows 95', 'Windows 95'),
            array('Windows NT 6.3', 'Windows 8.1'),
            array('Windows NT 6.2', 'Windows 8'),
            array('Windows NT 6.1', 'Windows 7'),
            array('Windows NT 6.0', 'Windows Vista'),
            array('Windows NT 5.2', 'Windows 2003/XP x64'),
            array('Windows NT 5.01', 'Windows 2000 SP1'),
            array('Windows NT 5.1', 'Windows XP'),
            array('Windows NT 5', 'Windows 2000'),
            array('Windows NT', 'Windows NT'),
            array('Macintosh', 'Macintosh'),
            array('Mac_PowerPC', 'Mac PowerPC'),
            array('Unix', 'Unix'),
            array('bsd', 'BSD'),
            array('Linux', 'Linux'),
            array('Wget', 'Linux'),
            array('windows', 'ETC Windows'),
            array('mac', 'ETC Mac')
		);
		
		foreach($OS as $val){
		    if(mb_eregi($val[0], $max_os[0]['os'])){
		        $os_name = $val[1];
		        break;
		    }
		}
		if($os_name == ""){
		    echo "&nbsp;";
		}else{
            echo $os_name;
		}
	?>
    </strong></dd>
    </dl>
	<dl class="target_div" onclick="parent.report_targeting_detail('browser', '<?php echo $data_type?>', '<?php echo $data_key?>', 100, 1);$('#targeting option:eq(3)').attr('selected', 'true');">
    <dt><?php if($targeting == "browser")?><?php echo lang('strBrowser');?><i class="float_r bd_squ fa ma_t3 fa-pie-chart ma_r5 <?php if($targeting == "browser"){ echo 'point_blue';}?>"></i></dt>
    <dd><?php echo lang('strMaximum');?> <?php echo lang('strImpression')?> <?php echo lang('strBrowser');?></dd>
    <dd style="<?php if($targeting == 'browser'){ echo 'color:red;';}?>"><strong>
    <?php 
	$BW = array(
        /* BROWSER */
        array('MSIE 2', 'InternetExplorer 2'),
        array('MSIE 3', 'InternetExplorer 3'),
        array('MSIE 4', 'InternetExplorer 4'),
        array('MSIE 5', 'InternetExplorer 5'),
        array('MSIE 6', 'InternetExplorer 6'),
        array('MSIE 7', 'InternetExplorer 7'),
        array('MSIE 8', 'InternetExplorer 8'),
        array('MSIE 9', 'InternetExplorer 9'),
        array('MSIE 10', 'InternetExplorer 10'),
        array('Trident/7.0', 'InternetExplorer 11'),
        array('MSIE', 'ETC InternetExplorer'),
        array('Firefox', 'FireFox'),
        array('Chrome', 'Chrome'),
        array('Safari', 'Safari'),
        array('Opera', 'Opera'),
        array('Lynx', 'Lynx'),
        array('LibWWW', 'LibWWW'),
        array('Konqueror', 'Konqueror'),
        array('Internet Ninja', 'Internet Ninja'),
        array('Download Ninja', 'Download Ninja'),
        array('WebCapture', 'WebCapture'),
        array('LTH', 'LTH Browser'),
        array('Gecko', 'Gecko compatible'),
        array('Mozilla', 'Mozilla compatible'),
        array('wget', 'Wget command')
	);
	
	foreach($BW as $val){
	    if(mb_eregi($val[0], $max_browser[0]['browser'])){
	        $br_name = $val[1];
	        break;
	    }
	}
	if($br_name == ""){
	    echo "&nbsp;";
	}else{
	    echo $br_name;
	}
    ?>
    </strong></dd>
    </dl>
	<dl class="target_div" onclick="parent.report_targeting_detail('device', '<?php echo $data_type?>', '<?php echo $data_key?>', 100, 1);$('#targeting option:eq(4)').attr('selected', 'true');">
        <dt>
        	<?php echo lang('strDevice');?>
        	<i class="float_r bd_squ fa ma_t3 fa-pie-chart ma_r5 <?php if($targeting == "device"){ echo 'point_blue';}?>"></i>
        </dt>
        <dd><?php echo lang('strMaximum');?> <?php echo lang('strImpression')?> <?php echo lang('strDevice');?>&nbsp;</dd>
        <dd style="<?php if($targeting == 'device'){ echo 'color:red;';}?>"><strong>
        <?php 
            if($max_device[0]['device'] == ""){
                echo "&nbsp;";   
            }else{
                echo $max_device[0]['device'];
            }
        ?> 
        </strong></dd>
    </dl>
	<dl class="target_div" onclick="parent.report_targeting_detail('category', '<?php echo $data_type?>', '<?php echo $data_key?>', 100, 1);$('#targeting option:eq(5)').attr('selected', 'true');">
        <dt><?php if($targeting == "category"){ echo lang('strCategory');}?><i class="float_r bd_squ fa ma_t3 fa-pie-chart ma_r5 <?php if($targeting == "category"){ echo 'point_blue';}?>"></i></dt>
        <dd><?php echo lang('strMaximum');?> <?php echo lang('strImpression')?> <?php echo lang('strCategory');?></dd>
        <dd style="<?php if($targeting == 'category'){ echo 'color:red;';}?>"><strong>
        <?php 
            if($max_category[0]['category'] == ""){
                echo "&nbsp;";
            }else{
                echo lang($max_category[0]['category']);
            }
        ?>
        </strong></dd>
    </dl>
	<!-- //seonu -->
<?php 
    if($targeting != null){
?>
<div>
<h4 class="ma_b10 clear"><?php echo lang('strTargeting');?> <?php echo lang('strReport');?> : <?php echo $info_name?> <span class="f-size-9 point_dgray">(<?php echo lang('strPeriod')?> : <?php echo $fromto_date;?>)</span></h4>
<div>
    <form name="report_operation_detail" method="post">
        <input type="hidden" name="imp_val" id="imp_val" value="<?php echo $option_imp?>">
        <input type="hidden" name="click_val" id="click_val" value="<?php echo $option_click?>">
        <input type="hidden" name="ctr_val" id="ctr_val" value="<?php echo $option_ctr?>">
        <input type="hidden" name="ppc_val" id="ppc_val" value="<?php echo $option_ppc?>">
        <input type="hidden" name="ppi_val" id="ppi_val" value="<?php echo $option_ppi?>">
        <input type="hidden" name="price_val" id="price_val" value="<?php echo $option_price?>">
        <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb f-size-9" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
            <colgroup>
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <thead>
                <tr>
                    <th>
                        <?php echo $info_name;?>
                    </th>
                    <th>
                        <?php echo lang('strImpressions');?>
                    </th>
                    <th>
                        <?php echo lang('strClicks');?>
                    </th>
                    <th>
                        CTR (%)
                    </th>
                    <th>
                        <?php echo lang('strAverage');?> PPC (<?php echo $currency_unit?>)
                    </th>
                    <th>
                        <?php echo lang('strAverage');?> PPI (<?php echo $currency_unit?>)
                    </th>
                    <th>
                        <?php echo lang('strTotalCharge');?> (<?php echo $currency_unit?>)
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $chart_data = array();
                    $sum_imp_cnt = 0;
                    $sum_click_cnt = 0;
                    $sum_loc_price = 0;
                    $sum_price = 0;
                    $count = count($report_list);
                    $sum_ctr = 0;
                    $sum_loc_ppi = 0;
                    $sum_ppi = 0;
                    $sum_loc_ppc = 0;
                    $sum_ppc = 0;

                    foreach($report_list as $key=>$rl){
                        //ctr = 클릭수 / 노출수 * 100
                        //평균 ppc = 총 광고비 / 클릭수
                        //평균 ppi = 총 광고비 / 노출수
                        if($rl['imp_cnt'] > 0){
                            $ctr = $rl['click_cnt'] / $rl['imp_cnt'] * 100;
                            $loc_ppi = $rl['loc_price'] / $rl['imp_cnt'];
                            $ppi = $rl['price'] / $rl['imp_cnt'];
                        }else{
                            $ctr = 0;
                            $loc_ppi = 0;
                            $ppi = 0;
                        }

                        if($rl['click_cnt'] > 0){
                            $loc_ppc = $rl['loc_price'] / $rl['click_cnt'];
                            $ppc = $rl['price'] / $rl['click_cnt'];
                        }else{
                            $loc_ppc = 0;
                            $ppc = 0;
                        }

                        $sum_imp_cnt += $rl['imp_cnt'];
                        $sum_click_cnt += $rl['click_cnt'];
                        $sum_loc_price += $rl['loc_price'];
                        $sum_price += $rl['price'];

                        if($sum_imp_cnt > 0){
                            $sum_ctr = $sum_click_cnt / $sum_imp_cnt * 100;
                            $sum_loc_ppi = $sum_loc_price / $sum_imp_cnt;
                            $sum_ppi = $sum_price / $sum_imp_cnt;
                        }else{
                            $sum_ctr = 0;
                            $sum_loc_ppi = 0;
                            $sum_ppi = 0;
                        }

                        if($sum_click_cnt > 0){
                            $sum_loc_ppc = $sum_loc_price / $sum_click_cnt;
                            $sum_ppc = $sum_price / $sum_click_cnt;
                        }else{
                            $sum_loc_ppc = 0;
                            $sum_ppc = 0;
                        }

                        $result_imp_cnt = number_format($rl['imp_cnt']);
                        $result_click_cnt = number_format($rl['click_cnt']);
                        $result_ctr = number_format(round($ctr, 2), 2);

                        if($currency == "KRW"){
                            $result_ppc = number_format(round($loc_ppc, 0));
                            $result_ppi = number_format(round($loc_ppi, 0));
                            $result_price = number_format(round($rl['loc_price'], 0));
                        }elseif($currency == "USD"){
                            $result_ppc = number_format(round($ppc, 2), 2);
                            $result_ppi = number_format(round($ppi, 2), 2);
                            $result_price = number_format(round($rl['price'], 2));
                        }

                        $chart_data[$key]['imp_cnt'] = $result_imp_cnt;
                        $chart_data[$key]['click_cnt'] = $result_click_cnt;
                        $chart_data[$key]['ctr'] = $result_ctr;
                        $chart_data[$key]['ppc'] = $result_ppc;
                        $chart_data[$key]['ppi'] = $result_ppi;
                        $chart_data[$key]['price'] = $result_price;
                ?>
                <tr>
                    <td style="text-align:center;padding-right:5px;">
                        <?php 
                            if($targeting == "os"){

                                $OS = array(
                                    /* OS */
                                    array('Windows CE', 'Windows CE'),
                                    array('Win98', 'Windows 98'),
                                    array('Windows 9x', 'Windows ME'),
                                    array('Windows me', 'Windows ME'),
                                    array('Windows 98', 'Windows 98'),
                                    array('Windows 95', 'Windows 95'),
                                    array('Windows NT 6.3', 'Windows 8.1'),
                                    array('Windows NT 6.2', 'Windows 8'),
                                    array('Windows NT 6.1', 'Windows 7'),
                                    array('Windows NT 6.0', 'Windows Vista'),
                                    array('Windows NT 5.2', 'Windows 2003/XP x64'),
                                    array('Windows NT 5.01', 'Windows 2000 SP1'),
                                    array('Windows NT 5.1', 'Windows XP'),
                                    array('Windows NT 5', 'Windows 2000'),
                                    array('Windows NT', 'Windows NT'),
                                    array('Macintosh', 'Macintosh'),
                                    array('Mac_PowerPC', 'Mac PowerPC'),
                                    array('Unix', 'Unix'),
                                    array('bsd', 'BSD'),
                                    array('Linux', 'Linux'),
                                    array('Wget', 'Linux'),
                                    array('windows', 'ETC Windows'),
                                    array('mac', 'ETC Mac')
                                );
                                
                                foreach($OS as $val){
                                    if(mb_eregi($val[0], $rl['os'])){
                                        $os_name = $val[1];
                                        break;
                                    }
                                }
                                
                                echo $os_name;
                                
                            }else if($targeting == "time"){
                                if($rl['day'] == "mon"){
                                    $day = lang('strMonday');
                                }else if($rl['day'] == "tue"){
                                    $day = lang('strTuesday');
                                }else if($rl['day'] == "wed"){
                                    $day = lang('strWednesday');
                                }else if($rl['day'] == "thu"){
                                    $day = lang('strThursday');
                                }else if($rl['day'] == "fri"){
                                    $day = lang('strFriday');
                                }else if($rl['day'] == "sat"){
                                    $day = lang('strSaturday');
                                }else if($rl['day'] == "sun"){
                                    $day = lang('strSunday');
                                }
                                
                                if($rl['time'] == "0"){
                                    $time = "00:00 ~ 00:59";
                                }else if($rl['time'] == "1"){
                                    $time = "01:00 ~ 01:59";
                                }else if($rl['time'] == "2"){
                                    $time = "02:00 ~ 02:59";
                                }else if($rl['time'] == "3"){
                                    $time = "03:00 ~ 03:59";
                                }else if($rl['time'] == "4"){
                                    $time = "04:00 ~ 04:59";
                                }else if($rl['time'] == "5"){
                                    $time = "05:00 ~ 05:59";
                                }else if($rl['time'] == "6"){
                                    $time = "06:00 ~ 06:59";
                                }else if($rl['time'] == "7"){
                                    $time = "07:00 ~ 07:59";
                                }else if($rl['time'] == "8"){
                                    $time = "08:00 ~ 08:59";
                                }else if($rl['time'] == "9"){
                                    $time = "09:00 ~ 09:59";
                                }else if($rl['time'] == "10"){
                                    $time = "10:00 ~ 10:59";
                                }else if($rl['time'] == "11"){
                                    $time = "11:00 ~ 11:59";
                                }else if($rl['time'] == "12"){
                                    $time = "12:00 ~ 12:59";
                                }else if($rl['time'] == "13"){
                                    $time = "13:00 ~ 13:59";
                                }else if($rl['time'] == "14"){
                                    $time = "14:00 ~ 14:59";
                                }else if($rl['time'] == "15"){
                                    $time = "15:00 ~ 15:59";
                                }else if($rl['time'] == "16"){
                                    $time = "16:00 ~ 16:59";
                                }else if($rl['time'] == "17"){
                                    $time = "17:00 ~ 17:59";
                                }else if($rl['time'] == "18"){
                                    $time = "18:00 ~ 18:59";
                                }else if($rl['time'] == "19"){
                                    $time = "19:00 ~ 19:59";
                                }else if($rl['time'] == "20"){
                                    $time = "20:00 ~ 20:59";
                                }else if($rl['time'] == "21"){
                                    $time = "21:00 ~ 21:59";
                                }else if($rl['time'] == "22"){
                                    $time = "22:00 ~ 22:59";
                                }else if($rl['time'] == "23"){
                                    $time = "23:00 ~ 23:59";                   
                                }
                
                                
                                echo $day."&nbsp;".$time;
                                
                            }else if($targeting == "browser"){
                                $BW = array(
                                    /* BROWSER */
                                    array('MSIE 2', 'InternetExplorer 2'),
                                    array('MSIE 3', 'InternetExplorer 3'),
                                    array('MSIE 4', 'InternetExplorer 4'),
                                    array('MSIE 5', 'InternetExplorer 5'),
                                    array('MSIE 6', 'InternetExplorer 6'),
                                    array('MSIE 7', 'InternetExplorer 7'),
                                    array('MSIE 8', 'InternetExplorer 8'),
                                    array('MSIE 9', 'InternetExplorer 9'),
                                    array('MSIE 10', 'InternetExplorer 10'),
                                    array('Trident/7.0', 'InternetExplorer 11'),
                                    array('MSIE', 'ETC InternetExplorer'),
                                    array('Firefox', 'FireFox'),
                                    array('Chrome', 'Chrome'),
                                    array('Safari', 'Safari'),
                                    array('Opera', 'Opera'),
                                    array('Lynx', 'Lynx'),
                                    array('LibWWW', 'LibWWW'),
                                    array('Konqueror', 'Konqueror'),
                                    array('Internet Ninja', 'Internet Ninja'),
                                    array('Download Ninja', 'Download Ninja'),
                                    array('WebCapture', 'WebCapture'),
                                    array('LTH', 'LTH Browser'),
                                    array('Gecko', 'Gecko compatible'),
                                    array('Mozilla', 'Mozilla compatible'),
                                    array('wget', 'Wget command')
                                );
                            
                                foreach($BW as $val){
                                    if(mb_eregi($val[0], $rl['browser'])){ 
                                        $br_name = $val[1];
                                        break;
                                    }
                                }
                                echo $br_name;
                                
                            }else if($targeting == "device"){   
                                echo $rl['device'];
                            }else if($targeting == "category"){   
                                echo lang($rl['category']);
                            }
                            
                         ?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_imp_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_click_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_ctr;?>%
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_ppc;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_ppi;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_price;?>
                    </td>
                </tr>
                <?php
                    }
                    if($currency == "KRW"){
                        if($count > 0){
                            $avg_price = number_format(round($sum_loc_price / $count, 0));
                        }else{
                            $avg_price = 0;
                        }
                    }elseif($currency == "USD"){
                        if($count > 0){
                            $avg_price = number_format(round($sum_price / $count, 2), 2);
                        }else{
                            $avg_price = 0;
                        }
                    }

                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td style="text-align:center;padding-right:5px;">
                        <?php echo lang('strAverage');?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php
                            if($count > 0){
                                echo number_format(round($sum_imp_cnt / $count, 0));
                            }else{
                                echo "0";
                            }
                        ?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php
                            if($count > 0){
                                echo number_format(round($sum_click_cnt / $count, 0));
                            }else{
                                echo "0";
                            }
                        ?>
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        -
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $avg_price;?>
                    </td>
                </tr>
                <tr>
                    <?php
                        $result_sum_imp_cnt = number_format(round($sum_imp_cnt, 0));
                        $result_sum_click_cnt = number_format(round($sum_click_cnt, 0));
                        $result_sum_ctr = number_format(round($sum_ctr, 2), 2);

                        if($currency == "KRW"){
                            $result_sum_ppc = number_format(round($sum_loc_ppc, 0));
                            $result_sum_ppi = number_format(round($sum_loc_ppi, 0));
                            $result_sum_price = number_format(round($sum_loc_price, 0));
                        }elseif($currency == "USD"){
                            $result_sum_ppc = number_format(round($sum_ppc, 2), 2);
                            $result_sum_ppi = number_format(round($sum_ppi, 2), 2);
                            $result_sum_price = number_format(round($sum_price, 2), 2);
                        }
                    ?>
                    <td style="text-align:center;padding-right:5px;">
                        <?php echo lang('strSum');?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_imp_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_click_cnt;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_ctr;?>%
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_ppc;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_ppi;?>
                    </td>
                    <td style="text-align:right;padding-right:5px;">
                        <?php echo $result_sum_price;?>
                    </td>
                </tr>
            </tfoot>
        </table>
        <div class="center">
            <div class="btn-group float_r">

                <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                    <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                    <i class="dropdown-caret fa fa-caret-down"></i>
                </button>
                <ul class="dropdown-menu ul_sel_box" >
                    <li><a href="javascript:page_change(10)">10</a></li>
                    <li><a href="javascript:page_change(25)">25</a></li>
                    <li><a href="javascript:page_change(50)">50</a></li>
                    <li><a href="javascript:page_change(100)">100</a></li>
                </ul>
            </div>

            <?php
            /*페이징처리*/
            echo $page_links;
            /*페이징처리*/
            ?>
        </div>
    </form>
</div>
<?php 
    }
?>
<script type="text/javascript">

    $(document).ready(function() {
        $('#sum_imp_cnt').html('<?php echo $result_sum_imp_cnt?>');
        $('#sum_click_cnt').html('<?php echo $result_sum_click_cnt?>');
        $('#sum_ctr').html('<?php echo $result_sum_ctr?>');
        $('#sum_ppc').html('<?php echo $result_sum_ppc?>');
        $('#sum_ppi').html('<?php echo $result_sum_ppi?>');
        $('#sum_price').html('<?php echo $result_sum_price?>');  
    });


    //페이징 스크립트 시작
    function page_change(row){
        parent.report_targeting_detail( '<?php echo $targeting?>', '<?php echo $data_type?>', '<?php echo $data_key?>', row, '<?php echo $page_num?>');
    }

    function paging(number){
        parent.report_targeting_detail( '<?php echo $targeting?>', '<?php echo $data_type?>', '<?php echo $data_key?>', '<?php echo $per_page?>', number);
    }
    //페이징 스크립트 끝
</script>


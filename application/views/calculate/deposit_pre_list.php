<script type="text/javascript">
$(document).ready(function() {
	
});

</script>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
    </div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strDepositMgr')?> </h3>
    </div>
    <div class="panel-body">
        <table class="aut_tb">
            <colgroup>
                <col width="25%">
                <col width="*">
                <col width="25%">
                <col width="*">
                <col width="25%">
                <col width="*">
            </colgroup>
            <tr>
                <th colspan="2">
                    <?php echo lang('strResidualDeposit')?>
                </th>
                <th colspan="2">
                    <?php echo lang('strPreDeposit')?>
                </th>
                <th colspan="2">
                    <?php echo lang('strPostDeposit')?>
                </th>
            </tr>
            <tr>
                <td class="txt_right">
                    <?php echo number_format($deposit_summary['all_deposit'])?>
                </td>
                <td class="txt_center">
                    100%
                </td>
                <td class="txt_right">
                    <?php echo number_format($deposit_summary['post_deposit'])?>
                </td>
                <td class="txt_center">
                    <?php echo number_format(($deposit_summary['post_deposit'] / $deposit_summary['all_deposit'])*100)?>%
                </td>
                <td class="txt_right">
                    <?php echo number_format($deposit_summary['pre_deposit'])?>
                </td>
                <td class="txt_center">
                    <?php echo number_format(($deposit_summary['pre_deposit'] / $deposit_summary['all_deposit'])*100)?>%
                </td>
                
            </tr>
        </table>
	</div>
    <div class="panel-body">
        <form name="deposit_pre_list" id="deposit_pre_list" method="post">
            <input type="hidden" name="year" id="year" value="<?php echo $cond['year'];?>">
            <input type="hidden" name="month" id="month" value="<?php echo $cond['month'];?>">
            <input type="hidden" name="date_ym" id="date_ym">
            
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="15%">
                        <col width="*">
                        <col width="15%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strType')?></th>
                        <td>
                            <input type="radio" name="type" value="all" <?php if ($cond['type'] == "all"){echo "checked";}?>> <?php echo lang('strAll')?>&nbsp;&nbsp;
                            <input type="radio" name="type" value="adver" <?php if ($cond['type'] == "adver"){echo "checked";}?>> <?php echo lang('strAdvertiser')?>&nbsp;&nbsp;
                            <input type="radio" name="type" value="agency" <?php if ($cond['type'] == "agency"){echo "checked";}?>> <?php echo lang('strAgency')?>&nbsp;&nbsp;
                            <input type="radio" name="type" value="rep" <?php if ($cond['type'] == "lab"){echo "checked";}?>> <?php echo lang('strLab')?>&nbsp;&nbsp;
                            <input type="radio" name="type" value="ind" <?php if ($cond['type'] == "ind"){echo "checked";}?>> <?php echo lang('strIndividual')?>&nbsp;&nbsp;
                        </td>
                        <td rowspan="5" class="txt_center">
                            <span class="btn btn-primary" onClick ="search();"><?php echo lang('strSearch')?></span>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strCompanyName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_com_nm" value="<?php echo $cond['mem_com_nm'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strPaymentMethod')?></th>
                        <td>
                            <input type="radio" name="charge_way" value="all" <?php if ($cond['charge_way'] == "all"){echo "checked";}?>> <?php echo lang('strAll')?>&nbsp;&nbsp;
                            <input type="radio" name="charge_way" value="1" <?php if ($cond['charge_way'] == "1"){echo "checked";}?>> <?php echo lang('strVirtualBankAccount')?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    
                    <tr>
                        <th><?php echo lang('strPublicationStatus')?></th>
                        <td>
                            <input type="radio" name="publish_fl" value="all" <?php if ($cond['publish_fl'] == "all"){echo "checked";}?>> <?php echo lang('strAll')?>&nbsp;&nbsp;
                            <input type="radio" name="publish_fl" value="1" <?php if ($cond['publish_fl'] == "1"){echo "checked";}?>> <?php echo lang('strWaitPublication')?>&nbsp;&nbsp;
                            <input type="radio" name="publish_fl" value="2" <?php if ($cond['publish_fl'] == "2"){echo "checked";}?>> <?php echo lang('strNoPublication')?>&nbsp;&nbsp;
                            <input type="radio" name="publish_fl" value="3" <?php if ($cond['publish_fl'] == "3"){echo "checked";}?>> <?php echo lang('strCarryOver')?>&nbsp;&nbsp;
                            <input type="radio" name="publish_fl" value="4" <?php if ($cond['publish_fl'] == "4"){echo "checked";}?>> <?php echo lang('strDonePublication')?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    
                    <tr>
                        <th><?php echo lang('strPeriod')?></th>
                        <td>
                            <div id="demo-dp-component">
                				<input type="text" name="fromto_date" id="daterange" class="ma_r10 range wid_40p" value="<?php echo $fromto_date?>" />
                				<span class="iput-group-addon  ma_t8">
                					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
                				</span>
                			</div>	
                        </td>
                    </tr>
                </table>
                <div class="float_r ma_b3" id="all_btn_gp">
                    
                </div>
         	</div>
            <div class="pa_b20">
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
    			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th rowspan="2"><?php echo lang('strCompanyName')?></th>
                            <th rowspan="2"><?php echo lang('strType')?></th>
                            <th rowspan="2"><?php echo lang('strPeriod')?></th>
                            <th rowspan="2"><?php echo lang('strChargeRequestAmount')?></th>
                            <th colspan="2"><?php echo lang('strChargedAmount')?></th>
                            <th colspan="2"><?php echo lang('strUseAmount')?></th>
                            <th rowspan="2"><?php echo lang('strRealRevenue')?></th>
                            <th rowspan="2"><?php echo lang('strTaxInvoiceAmount')?></th>
                        </tr>
                        <tr>
                            <th><?php echo lang('strVirtualBankAccount')?></th>
                            <th><?php echo lang('strSum')?></th>
                            <th><?php echo lang('strFee')?></th>
                            <th><?php echo lang('strRefund')?></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($deposit_list)){ ?>
                            <?php foreach ($deposit_list as $row){ ?>
                                <tr>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['mem_com_nm'] == ""){
                                                echo $row['mem_id'];
                                            }else{
                                                echo $row['mem_com_nm'];
                                            }
                                        ?>
                                                
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['role'] == "adver"){
                                                echo lang('strAdvertiser');
                                            }elseif ($row['role'] == "agency"){
                                                echo lang('strAgency');
                                            }elseif ($row['role'] == "ind"){
                                                echo lang('strIndividual');
                                            }elseif ($row['role'] == "lab"){
                                                echo lang('strLab');
                                            }elseif ($row['role'] == "sales"){
                                                echo lang('strSales');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo substr($row['first_date'], 0, 10)." ~ ".substr($row['last_date'], 0, 10); ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo number_format($row['charge_cash'])?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo number_format($row['charge_cash'])?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo number_format($row['charge_cash'])?>
                                    </td>
                                    <td class="txt_right">
                                        <?php echo number_format($row['price'])?>
                                    </td>
                                    <td class="txt_right">
                                        <?php echo number_format($row['tax_price'])?>
                                    </td>
                                    <td class="txt_center">
                                    </td>
                                    <td class="txt_center">
                                        <?php echo number_format($row['result_price'])?>
                                    </td>
                               </tr>
                           <?php $no++;}?> 
                       <?php }?>
                   </tbody>
                </table>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
		</div>
    </div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.tax_bill_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.tax_bill_list;
        frm.action="/calculate/tax_bill_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    
    function change_year(year){
        $("#year_view").html(year);
        $("#year").val(year);
    }

    function change_month(month){
        $("#month_view").html(month);
        $("#month").val(month);
    }
    
    function search(){
    	var frm = document.deposit_pre_list;
    	frm.action = '/calculate/deposit_pre_list';
    	frm.submit();
    }

    function change_publish_status(mem_no, date_ym){
        var frm = document.tax_bill_list;
        frm.mem_no.value = mem_no;
        frm.date_ym.value = date_ym;
        frm.action = "/calculate/tax_bill_view";
        frm.submit();        
    }

    function select_status(type){
        var frm = document.refund_request_list;
        frm.mem_id.value = "";
        frm.mem_com_nm.value = "";
        frm.fromto_date.value = "";
        $("#manager_yn").prop("checked", false);
        $("#master_yn").prop("checked", false);
        $("#answer_done").prop("checked", false);
        
        if (type == "all"){
            location.href('/cash/refund_request_list');
        }else if (type == "today"){
            $("#daterange").val('<?php echo date('Y-m-d') ?>'+ ' ~ ' + '<?php echo date('Y-m-d')?>');
        }else if (type == "soon"){
            $("#cond_refund_st").val("1");
        }else if (type == "reject"){
        	$("#cond_refund_st").val("2");
        }else if (type == "done"){
        	$("#cond_refund_st").val("3");
        }
        search();
    }
            
</script>
   public function select_revenue_intergration_list($mem_no, $term, $from_date, $to_date){
    
        $this->db->select("date_ymd, mem_com_nm, SUM(pre_deposit) AS pre_deposit ");
        $this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
        $this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
        $this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
        $this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
        $this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
        $this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
        $this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
        $this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
        $this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_revenue');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($term == "day"){
            $this->db->group_by("day");
        }else if($term == "week"){
            $this->db->group_by("week");
        }else if($term == "month"){
            $this->db->group_by("month");
        }
    
        $query = $this->db->get();
    
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    function select_revenue_business_list($data, $from_date, $to_date){
    
        if($data['datatype'] == "time"){
            $this->db->select("date_ymd");
        }else if($data['datatype'] == "role"){
            $this->db->select("group_role role");
        }else if($data['datatype'] == "role_kind"){
            $this->db->select("mem_no, mem_com_nm, group_role role");
        }
    
        $this->db->select("SUM(pre_deposit) AS pre_deposit ");
        $this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
        $this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
        $this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
        $this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
        $this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
        $this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
        $this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
        $this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
        $this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_revenue');
        //$this->db->where_in('mem_no', $adver_no);
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($data['datatype'] == "time"){
            if($term == "day"){
                $this->db->group_by("day");
            }else if($term == "week"){
                $this->db->group_by("week");
            }else if($term == "month"){
                $this->db->group_by("month");
            }
        }else if($data['datatype'] == "role"){
            $this->db->group_by("group_role");
        }else if($data['datatype'] == "role_kind"){
            $this->db->where('group_role', $data['datakey']);
            $this->db->group_by("group_no");
        }
    
    
        if($data['datatype'] == "role" || $data['datatype'] == "role_kind"){
            $this->db->order_by("price", "desc");
        }
    
        $query = $this->db->get();
    
        //print_r($this->db->last_query());
        //exit;
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
   function select_revenue_daily_list($data, $term, $from_date, $to_date){
    
        $this->db->select("date_ymd, mem_no, mem_com_nm, group_role role");
        $this->db->select("SUM(pre_deposit) AS pre_deposit ");
        $this->db->select("SUM(loc_pre_deposit) AS loc_pre_deposit, SUM(pre_deposit_lt) AS pre_deposit_lt ");
        $this->db->select("SUM(loc_pre_deposit_lt) AS loc_pre_deposit_lt, SUM(deposit) AS deposit ");
        $this->db->select("SUM(loc_deposit) AS loc_deposit, SUM(deposit_lt) AS deposit_lt ");
        $this->db->select("SUM(loc_deposit_lt) AS loc_deposit_lt, SUM(price) AS price, SUM(loc_price) AS loc_price ");
        $this->db->select("SUM(price_lt) AS price_lt, SUM(loc_price_lt) AS loc_price_lt, SUM(fee_price) AS fee_price ");
        $this->db->select("SUM(loc_fee_price) AS loc_fee_price, SUM(fee_price_lt) AS fee_price_lt ");
        $this->db->select("SUM(loc_fee_price_lt) AS loc_fee_price_lt, SUM(refund) AS refund, SUM(loc_refund) AS loc_refund ");
        $this->db->select("SUM(refund_lt) AS refund_lt, SUM(loc_refund_lt) AS loc_refund_lt, SUM(revenue) AS revenue ");
        $this->db->select("SUM(loc_revenue) AS loc_revenue, SUM(revenue_lt) AS revenue_lt, SUM(loc_revenue_lt) AS loc_revenue_lt ");
        $this->db->select("DATE_FORMAT(date_ymd, '%y-%m-%d') AS day, DATE_FORMAT(date_ymd, '%y-%V') AS week, DATE_FORMAT(date_ymd, '%y-%m') AS month", FALSE);
        $this->db->from('mo_revenue');
        //$this->db->where_in('mem_no', $adver_no);
        
        if($data['datatype'] == "role"){
            $this->db->where('group_role', $data['datakey']);
        }else if($data['datatype'] == "role_kind"){
            $this->db->where('group_no', $data['datakey']);
        }
        
        $this->db->where('date_ymd >=', $from_date);
        $this->db->where('date_ymd <=', $to_date);
    
        if($term == "day"){
            $this->db->group_by("day");
        }else if($term == "week"){
            $this->db->group_by("week");
        }else if($term == "month"){
            $this->db->group_by("month");
        }
    
        $query = $this->db->get();
        
        //print_r($this->db->last_query());
        
        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }else{
            return array();
        }
    }
    
    
    
    
        public function revenue_business(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        //$mem_no = 54;
        $range = $this->input->post('range');
        $fromto_date = $this->input->post('fromto_date');
    
        if ($fromto_date == null && $range == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day")) . " ~ " . date("Y-m-d");
        }elseif($fromto_date != null && $range != null){
            $fromto_date = date("Y-m-d", strtotime("-".$range." day")) . " ~ " . date("Y-m-d");
        }
    
        $data['fromto_date'] = $fromto_date;
        $data['daterange'] = $data['fromto_date'];
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = "day";
        $data['term'] = $term;
    
        //메뉴 라이브러리 - 시작
        $params = array(
                        'url' => '/calculate/revenue_business'
        );
        $menu_data = $this->menu->menu_info($params);
        //메뉴 라이브러리 - 끝
    
        $this->load->view('common/header',$menu_data);
        $this->load->view('calculate/revenue_business', $data);
        $this->load->view('common/footer');
    }
    
    public function revenue_business_detail(){
    
        $mem_no = $this->session->userdata('mem_no');
    
        $data['datatype'] = $this->input->post('datatype');
        $data['datakey'] = $this->input->post('datakey');
    
        if( $data['datatype'] == null){
            $data['datatype'] = "role";
        }
    
        $data['option_pre_deposit'] = $this->input->post('option_pre_deposit');
        $data['option_deposit'] = $this->input->post('option_deposit');
        $data['option_price'] = $this->input->post('option_price');
        $data['option_fee_price'] = $this->input->post('option_fee_price');
        $data['option_refund'] = $this->input->post('option_refund');
        $data['option_revenue'] = $this->input->post('option_revenue');
    
        if($data['option_pre_deposit'] == null){
            $data['option_pre_deposit'] = "N";
        }
        if($data['option_deposit'] == null){
            $data['option_deposit'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "N";
        }
        if($data['option_fee_price'] == null){
            $data['option_fee_price'] = "N";
        }
        if($data['option_refund'] == null){
            $data['option_refund'] = "N";
        }
        if($data['option_revenue'] == null){
            $data['option_revenue'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['revenue_list'] = $this->calculate_db->select_revenue_business_list($data, $from_date, $to_date);
    
        $this->load->view('calculate/revenue_business_detail', $data);
    }
        function revenue_daily_form(){
        $mem_no = $this->session->userdata('mem_no');
    
        $datatype = $this->input->post('datatype');
        $data['datatype'] = $datatype;
        $datakey = $this->input->post('datakey');
        $data['datakey'] = $datakey;
    
        $data['option_pre_deposit'] = $this->input->post('option_pre_deposit');
        $data['option_deposit'] = $this->input->post('option_deposit');
        $data['option_price'] = $this->input->post('option_price');
        $data['option_fee_price'] = $this->input->post('option_fee_price');
        $data['option_refund'] = $this->input->post('option_refund');
        $data['option_revenue'] = $this->input->post('option_revenue');
    
        if($data['option_pre_deposit'] == null){
            $data['option_pre_deposit'] = "N";
        }
        if($data['option_deposit'] == null){
            $data['option_deposit'] = "N";
        }
        if($data['option_price'] == null){
            $data['option_price'] = "N";
        }
        if($data['option_fee_price'] == null){
            $data['option_fee_price'] = "N";
        }
        if($data['option_refund'] == null){
            $data['option_refund'] = "N";
        }
        if($data['option_revenue'] == null){
            $data['option_revenue'] = "Y";
        }
    
        $data['daterange'] = $this->input->post('daterange');
    
        $fromto_date = $data['daterange'];
    
        if ($fromto_date == null) {
            $fromto_date = date("Y-m-d", strtotime("-7 day"))." ~ ".date("Y-m-d", strtotime("-1 day"));
        }
    
        $data['fromto_date'] = $fromto_date;
    
        $temp_date = explode(' ~ ', $fromto_date);
        $from_date = $temp_date[0];
        $to_date = $temp_date[1];
    
        $term = $this->input->post('term');
        $data['term'] = $term;
    
        $data['revenue_list'] = $this->calculate_db->select_revenue_daily_list($data, $term, $from_date, $to_date);
    
        $data['group_role'] = $data['revenue_list'][0]['group_role'];
        $data['group_nm'] = $data['revenue_list'][0]['group_nm'];
    
        $this->load->view('calculate/revenue_daily_form', $data);
    }
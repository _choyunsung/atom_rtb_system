<div class="modal" id="modal_revenue" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content" id="modal_revenue_content"></div>
    </div>
</div>
<?php
    $mem_no = $this->session->userdata('mem_no');

    $temp_date = explode(' ~ ', $fromto_date);
    $from_date = $temp_date[0];
    $to_date = $temp_date[1];

    $sitr_lang = $this->session->userdata('site_lang');

    if($sitr_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }

    if($currency == "KRW"){
        $currency_unit = "₩";
    }elseif($currency == "USD"){
        $currency_unit = "$";
    }
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strReport');?></a></li>
			<li class="active"><?php echo lang('strBusinessRev');?></li>
		</ol>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"> 
			<a href="javascript:revenue_business_detail('role', '', $('#daterange').val(), 'day', 'N', 'N', 'N', 'N', 'N', 'Y');$('#role').html('');" style="cursor:pointer;">
			<?php echo lang('strBusinessRev');?>
			</a>
			<span id="role"></span>
		</h3>
	</div>
	<!--요기 -->
	<div class="panel-body pa_t0">
        <div class="clear bd_eb" id="report"">
        </div>
	</div>
    <div id="revenue_business_detail"></div>
</div>

<script type="text/javascript">

    $("#modal_report").on('hidden.bs.modal', function () {
        $('#modal_revenue_content').html("");
        $(this).data('bs.modal', null);
    });

    function revenue_daily_form(datatype, datakey, daterange, term, pre_deposit, deposit, price, fee_price, refund, revenue){  
        $.ajax({
            type:"POST",
            url:"/calculate/revenue_daily_form",
            data : {datatype: datatype, datakey: datakey, daterange: daterange, term: term, option_pre_deposit: pre_deposit, option_deposit: deposit, option_price: price, option_fee_price: fee_price, option_refund: refund, option_revenue: revenue},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success: function (data){
            	$('#modal_revenue_content').html("");
            	$("#modal_revenue").modal("show");  
                $('#modal_revenue_content').append(data);
            }
        });
    }

    function revenue_daily_reset(datatype, datakey, daterange, term, pre_deposit, deposit, price, fee_price, refund, revenue){
    	
        $.ajax({
            type:"POST",
            url:"/calculate/revenue_daily_form",
            data : {datatype: datatype, datakey: datakey, daterange: daterange, term: term, option_pre_deposit: pre_deposit, option_deposit: deposit, option_price: price, option_fee_price: fee_price, option_refund: refund, option_revenue: revenue},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success: function (data){
            	$('#modal_revenue_content').html("");
                $('#modal_revenue_content').append(data);
            }
        });
    }
    
    function revenue_business_detail(datatype, datakey, daterange, term, pre_deposit, deposit, price, fee_price, refund, revenue){

 
        $.ajax({
            url : "/calculate/revenue_business_detail/",
            dataType : "html",
            beforeSend: function() {
                $('#revenue_business_detail').show().fadeIn('slow');
            },
            type : "post",  // post 또는 get
            data : {datatype: datatype, datakey: datakey, daterange: daterange, term: term, option_pre_deposit: pre_deposit, option_deposit: deposit, option_price: price, option_fee_price: fee_price, option_refund: refund, option_revenue: revenue},   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
				if(datakey == "adver"){
					$("#role").html(" > <?php echo lang('strAdvertiser');?>"); 
				}else if(datakey == "ind"){
					$("#role").html(" > <?php echo lang('strIndividual');?>"); 
				}else if(datakey == "agency"){
					$("#role").html(" > <?php echo lang('strAgency');?>"); 
				}else if(datakey == "lab"){
					$("#role").html(" > <?php echo lang('strLab');?>"); 
				}
				
                $("#revenue_business_detail").html(result);
            }
        });
    }

    $(function () {
    	revenue_business_detail('role', '', '<?php echo $daterange?>', '<?php echo $term?>', 'N', 'N', 'N', 'N', 'N', 'Y');
    });

 
</script>
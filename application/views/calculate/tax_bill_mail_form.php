<style>
@import url(http://fonts.googleapis.com/earlyaccess/nanumgothic.css);
</style>
<div style="border:5px solid #4859a1; padding:1%; color:#000; overflow:hidden; font-family:'nanumgothic';">
<table style="width:100%; border:1px solid #ccc; border-collapse:collapse; font-size:9pt; font-family:'nanumgothic';">
    <thead>
    <tr>
    <th colspan="4" style="padding:1%; background-color:#fff; color:#000; font-weight:bold; font-size:12pt;"><?php echo $taxbill_view['mem_com_nm']?> <?php echo date('m')?>월 정산</th>
    </tr>
    <tr>
    <!--
        <th style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5;"><?php echo lang('strPeriod')?></th>
         -->
        <th style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5;"><?php echo lang('strDetailCont')?></th>
        <th style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5;"><?php echo lang('strPublishPrice')?></th>
        <th style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5;"><?php echo lang('strTaxPrice')?></th>
        <th style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5;"><?php echo lang('strSum')?></th>
    </tr>
</thead>
<tbody>
    <?php 
        foreach($publish_list as $row){
            $loc_price = round($row['loc_price'], -2);
            $loc_tax = $loc_price * 0.1;
            $loc_result = $loc_price + $loc_tax;
    ?>
    <?php $price += $loc_price;?>
    <?php $tax_price += $loc_tax;?>
    <?php $result_price += $loc_result;?>
    <!-- 
        <tr>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:center;"><?php echo substr($row['date_ym'],0,4)."년 ".substr($row['date_ym'],5,2)."월 "?></td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:center;">광고비</td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:right;"><?php echo number_format($loc_price)?></td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:right;"><?php echo number_format($loc_tax)?></td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:right;"><?php echo number_format($loc_result)?></td>
        </tr>
         -->
       <?php 
            foreach($this->calculate_db->select_tax_bill_detail_manager_list($row['mem_no'], $row['date_ym']) as $key_=>$row_){
        ?>
        	<?php 
                $loc_price = round($row_['loc_price'], -2);
                $loc_tax = $loc_price * 0.1;
                $loc_result = $loc_price + $loc_tax;
            ?>
        <tr>    
        <!-- 
        	<td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:center;"></td>
        	 -->
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:center;"><?php echo $row_['mem_id']?>(<?php echo $row_['mem_type']?>)</td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:right;"><?php echo number_format($loc_price)?></td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:right;"><?php echo number_format($loc_tax)?></td>
            <td style="padding:1%; color:#000; border:1px dotted #ccc; text-align:right;"><?php echo number_format($loc_result)?></td>
        </tr>
        <?php 
            }
        ?>
    <?php }?>
</tbody>
<tbody>
    <tr id="prepend_standard">
        <td style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5; text-align:center !important;"><?php echo lang('strSum')?></td>
        <td style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5; text-align:right;" id="sum_price"><?php echo number_format($price)?></td>
        <td style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5; text-align:right;" id="sum_tax"><?php echo number_format($tax_price)?></td>
        <td style="padding:1%; color:#000; border:1px solid #ccc; background-color:#f5f5f5; text-align:right;" id="sum_result"><?php echo number_format($result_price)?></td>
    </tr>
</tbody>
</table>
    <p style="float:left;"><img src="http://atom.adop.cc/img/adop_logo.png" alt="ADOP" /></p>
    <div style="float:right;">
    <dl style="padding:0; margin:8px 10px 8px 0; font-size:10pt; text-align:right;">
        <dt style="color:#ff2956; font-weight:bold; display:inline-block; width:120px;"><?php echo lang('strTaxInvoice')?> <?php echo lang('strWriteDate')?></dt>
        <dd style="display:inline-block; color:#000; width:120px;"><?php echo date('Y년 m월 d일')?></dd>
    </dl>
    <dl style="padding:0; margin:8px 10px 8px 0; font-size:10pt; text-align:right;" id="prepend_standard">
        <dt style="color:#ff2956; font-weight:bold; display:inline-block; width:120px;"><?php echo lang('strTaxInvoice')?> <?php echo lang('strPublishDate')?></dt>
        <dd style="display:inline-block; color:#000; width:120px;"><?php echo substr($taxbill_view['publish_dt'],0,4)."년 ".substr($taxbill_view['publish_dt'],5,2)."월 ".substr($taxbill_view['publish_dt'],8,2)."일 "?></dd>
    </dl>
    <dl style="padding:0; margin:8px 10px 8px 0; font-size:10pt; text-align:right;">
        <dt style="color:#ff2956; font-weight:bold; display:inline-block; width:120px;">정산서 e-mail</dt>
        <dd style="display:inline-block; color:#000; width:120px;">tax@adop.co.kr</dd>
    </dl>
    </div>
</div>

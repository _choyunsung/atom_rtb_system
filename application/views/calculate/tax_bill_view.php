<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strCalMgr')?></a></li>
			<li class="active"><?php echo lang('strTaxBillPublish')?></li>
		</ol>
	</div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit20"><?php echo lang('strTaxBillPublish')?> </h3>
    </div>
    <div class="panel-body">
        <h2 class="page-header text-overflow tit_blit0 ma_t20"><?php echo lang('strTaxInvoice')?> <?php echo lang('strInfo')?> </h2>
        <table class="aut_tb wid_90p ">
            <colgroup>
                <col width="15%">
                <col width="35%">
                <col width="15%">
                <col width="35%">
            </colgroup>
            <tr>
                <th><?php echo lang('strID')?></th>
                <td>
                    <?php echo $taxbill_view['mem_id']?>
                </td>
                <th class="bd_l1"><?php echo lang('strCompanyName')?></th>
                <td>
                    <?php echo $taxbill_view['mem_com_nm']?>
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strType')?></th>
                <td>
                    <?php 
                        if ($taxbill_view['role'] == "adver"){
                            echo lang('strAdvertiser');
                        }elseif ($taxbill_view['role'] == "agency"){
                            echo lang('strAgency');
                        }elseif ($taxbill_view['role'] == "ind"){
                            echo lang('strIndividual');
                        }elseif ($taxbill_view['role'] == "lab"){
                            echo lang('strLab');
                        }
                    ?>
                </td>
                <th class="bd_l1"><?php echo lang('strAdvanceDeferred')?></th>
                <td>
                    <?php
                        if($taxbill_view['mem_pay_later'] == "Y"){
                            echo "후불";  
                        }else if($taxbill_view['mem_pay_later'] == "N"){
                            echo "선불";
                        }
                    ?>
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strTaxInvoice')?> <?php echo lang('strContactName')?></th>
                <td>
                    <?php echo $taxbill_view['tax_mem_nm']?>
                </td>
                <th class="bd_l1"><?php echo lang('strAnswerName')?> <?php echo lang('strEmail')?></th>
                <td>
                    <?php echo $taxbill_view['tax_mem_email']?>
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strBusinessNo')?></th>
                <td>
                    <?php echo $taxbill_view['mem_com_no']?>
                </td>
                <th class="bd_l1"><?php echo lang('strRepreName')?></th>
                <td>
                    <?php echo $taxbill_view['mem_com_ceo']?>
                </td>
            </tr>
            <tr>
                <th><?php echo lang('strBizConditions')?> / <?php echo lang('strBizItems')?></th>
                <td>
                    <?php echo $taxbill_view['mem_com_type']?> / <?php echo $taxbill_view['mem_com_item']?>
                </td>
                <th class="bd_l1"><?php echo lang('strAddress')?></th>
                <td>
                    <?php echo $taxbill_view['mem_addr']?>
                </td>
            </tr>
            
        </table>
	</div>
	<!-- 발행내역 -->
	<div class="panel-body">
        <h2 class="page-header text-overflow tit_blit0">
        	&lt;<?php echo date('m')?><?php echo lang('strMonth');?> 
        	<?php echo lang('strCalculate');?> 
        	<?php echo $taxbill_view['mem_com_nm']?>&gt;
        	<?php echo lang('strTaxInvoice')?> 발행내역
      	</h2>
        <table class="aut_tb wid_90p">
            <thead>
                <tr>
                    <th class="bd_l1"><?php echo lang('strPeriod')?></th>
                    <th class="bd_l1"><?php echo lang('strDetailCont')?></th>
                    <th class="bd_l1"><?php echo lang('strPublishPrice')?></th>
                    <th class="bd_l1"><?php echo lang('strTaxPrice')?></th>
                    <th class="bd_l1"><?php echo lang('strSum')?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($publish_list as $row){?>
                    <?php 
                        $pub_loc_price = round($row['loc_price'], -2);
                        $pub_loc_tax = $pub_loc_price * 0.1;
                        $pub_loc_result = $pub_loc_price + $pub_loc_tax;
                    ?>
                    <?php $price += $pub_loc_price;?>
                    <?php $tax_price += $pub_loc_tax;?>
                    <?php $result_price += $pub_loc_result;?>
                    <tr>
                        <td class="txt_center"><?php echo $row['date_ym']?></td>
                        <td class="txt_center"><?php echo $row['tax_cont']?></td>
                        <td class="txt_right"><?php echo number_format($pub_loc_price)?></td>
                        <td class="txt_right"><?php echo number_format($pub_loc_tax)?></td>
                        <td class="txt_right"><?php echo number_format($pub_loc_result)?></td>
                    </tr>
                <?php }?>
            </tbody>
            <tbody>
                <tr>
                    <td class="txt_center" colspan="2"><?php echo lang('strSum')?></td>
                    <td class="txt_right"><?php echo number_format($price)?></td>
                    <td class="txt_right"><?php echo number_format($tax_price)?></td>
                    <td class="txt_right"><?php echo number_format($result_price)?></td>
                </tr>
            </tbody>
        </table>
        <div class="wid_970p">
            <table class="aut_tb wid_90p">
            <colgroup>
            <col width="25%" />
            <col width="25%" />
            <col width="25%" />
            <col width="25%" />
            </colgroup>
                <tr>
                    <th><?php echo lang('strTaxInvoice')?> <?php echo lang('strWriteDate')?></th>
                    <td class="txt_center">
                    	<?php echo substr($taxbill_view['calculate_tax_dt'], 0, 10)?>
                    </td>
                    <th class="bd_l1"><?php echo lang('strTaxInvoice')?> <?php echo lang('strPublishDate')?></th>
                    <td class="txt_center">
        				<?php echo substr($taxbill_view['publish_dt'], 0, 10)?>
                    </td>
                </tr>
            </table>
        </div>
	</div>
	
	<form name="tax_bill_history" method="post">
    	<div class="panel-body">
            <h2 class="page-header text-overflow tit_blit0"><?php echo lang('strMonth')?> <?php echo lang('strUsedHistory')?> </h2>
            <table class="aut_tb wid_90p ">
                <colgroup>
                    <col width="10%">
                    <col width="*">
                    <col width="12%">
                    <col width="12%">
                    <col width="12%">
                    <col width="7%">
                    <col width="7%">
                    <col width="10%">
                    <!-- <col width="10%"> -->
                </colgroup>
                <thead>
                    <tr>
                        <th rowspan="2"><?php echo lang('strCalculateDate')?></th>
                        <th rowspan="2" class="bd_l1"><?php echo lang('strDetailCont')?></th>
                        <th colspan="3" class="bd_l1"><?php echo lang('strAdValue')?></th>
                        <th rowspan="2" class="bd_l1"><?php echo lang('strCarryOver')?></th>
                        <th rowspan="2" class="bd_l1"><?php echo lang('strPublication')?></th>
                        <th rowspan="2" class="bd_l1"><?php echo lang('strAddTaxBill')?></th>
                        <!--<th rowspan="2" class="bd_l1"><?php echo lang('strModify')?></th>-->
                    </tr>
                    <tr>
                        <th><?php echo lang('strPublishPrice')?></th>
                        <th class="bd_l1"><?php echo lang('strTaxPrice')?></th>
                        <th class="bd_l1"><?php echo lang('strSum')?></th>
                    </tr>
                </thead>
                <tbody id="list_row">
                    <?php foreach ($taxbill_list as $row) {?>
                        <?php 
                            $loc_price = round($row['loc_price'], -2);
                            $loc_tax = $loc_price * 0.1;
                            $loc_result = $loc_price + $loc_tax;
                        ?>
                            <!-- 
                            publish_fl : 1 미발행
                            publish_fl : 4 발행
                             -->
                        <tr id="taxbill_view_<?php echo $row['calculate_tax_no']?>">
                            <td class="txt_center"><?php echo substr($row['date_ym'],0,4)."년 ".substr($row['date_ym'], 5, 2)."월";?></td>
                            <td class="txt_center"><?php echo $row['tax_cont']?></td>
                            <td class="txt_right"><?php echo number_format($loc_price)?></td>
                            <td class="txt_right"><?php echo number_format($loc_tax)?></td>
                            <td class="txt_right"><?php echo number_format($loc_result)?></td>
                            <td class="txt_center"><input type="checkbox" id="forward_fl_<?php echo $row['calculate_tax_no'];?>" <?php if ($row['forward_fl'] == 'Y'){echo "checked";}?> onchange="forward_change('<?php echo $row['calculate_tax_no'];?>')"></td>
                            <td class="txt_center"><input type="checkbox" id="nopublish_fl_<?php echo $row['calculate_tax_no'];?>" <?php if ($row['publish_fl'] == '4'){echo "checked";}?> disabled></td>
                            <td class="txt_center">
                                <input type="checkbox" name="chk_cal_tax_no[]" value="<?php echo $row['calculate_tax_no'];?>" id="add_chk_<?php echo $row['calculate_tax_no'];?>" onchange="check_publish_list('<?php echo $row['calculate_tax_no']?>','<?php echo $row['date_ym']?>', '<?php echo $row['tax_cont']?>', '<?php echo $loc_price?>', '<?php echo $loc_tax?>', '<?php echo $loc_result?>');">
                            </td>
                            <!--
                            <td class="txt_center">
                            <span class="dropdown-caret fa fa-edit cursor" onClick ="taxbill_modify('<?php echo $row['calculate_tax_no']?>');"></span>
                            <span class="dropdown-caret fa fa-plus cursor" onClick ="add_taxbill_list();"></span>
                            <span class="btn btn-primary" onClick ="taxbill_modify('<?php echo $row['calculate_tax_no']?>');"><?php echo lang('strModify')?></span></td>
                            -->
                        </tr>
                        <tr id="taxbill_modify_<?php echo $row['calculate_tax_no']?>" style="display:none;">
                            <td class='txt_center'><input type='text' class="wid_100" id='modify_date_ym_<?php echo $row['calculate_tax_no']?>' value="<?php echo $row['date_ym']?>"/></td>
                            <td class="txt_center"><input type='text' id='modify_tax_cont_<?php echo $row['calculate_tax_no']?>' value="<?php echo $row['tax_cont']?>"/></td>
                            <td class="txt_right"><input type="text" class="wid_100" id="modify_price_<?php echo $row['calculate_tax_no']?>" value="<?php echo number_format($loc_price)?>" onkeyup="modify_auto_cal('<?php echo $row['calculate_tax_no']?>')"></td>
                            <td class="txt_right"><input type="text" class="wid_100" id="modify_tax_price_<?php echo $row['calculate_tax_no']?>" value="<?php echo number_format($loc_tax)?>"></td>
                            <td class="txt_right"><input type="text" class="wid_100" id="modify_result_price_<?php echo $row['calculate_tax_no']?>" value="<?php echo number_format($loc_result)?>"></td>
                            <td class="txt_center" colspan="4">
                                <span class="btn btn-primary" onClick ="taxbill_modify_save('<?php echo $row['calculate_tax_no']?>');"><?php echo lang('strSave')?></span>
                                <span class="btn btn-primary" onClick ="modify_cancel('<?php echo $row['calculate_tax_no']?>');"><?php echo lang('strCancel')?></span>
                            </td>
                        </tr>
                        <?php 
                            foreach($this->calculate_db->select_tax_bill_detail_manager_list($row['mem_no'], $row['date_ym']) as $key_=>$row_){
                        ?>
                        	<?php 
                                $loc_price = round($row_['loc_price'], -2);
                                $loc_tax = $loc_price * 0.1;
                                $loc_result = $loc_price + $loc_tax;
                            ?>
                        <tr>    
                        	<td class="txt_center"></td>
                            <td class="txt_center"><?php echo $row_['mem_id']?>(<?php echo $row_['mem_type']?>)</td>
                            <td class="txt_right"><?php echo number_format($loc_price)?></td>
                            <td class="txt_right"><?php echo number_format($loc_tax)?></td>
                            <td class="txt_right"><?php echo number_format($loc_result)?></td>
                            <td class="txt_center"></td>
                            <td class="txt_center"></td>
                            <td class="txt_center"></td>
                        </tr>
                        <?php 
                            }
                        ?>
                    <?php }?>
                </tbody>
            </table>
            
            <!-- <span class="btn btn-primary ma_b20 float_r" onClick ="add_taxbill_list();"><?php echo lang('strAdd')?></span> -->
    	</div>
    	<div class="panel-body">
            <h2 class="page-header text-overflow tit_blit0">&lt;<?php echo date('m')?><?php echo lang('strMonth');?> <?php echo lang('strCalculate');?> <?php echo $taxbill_view->mem_com_nm?>&gt; <?php echo lang('strTaxInvoice')?> </h2>
            <input type="hidden" name="write_ymd" value="<?php echo date('Y-m-d');?>">
            <input type="hidden" name="date_ym" value="<?php echo $date_ym;?>">
            <input type="hidden" name="history_no" value="<?php echo $taxbill_view['history_no'];?>">
            <input type="hidden" name="publish_price" id="publish_price">
            <input type="hidden" name="publish_tax" id="publish_tax">
            <input type="hidden" name="publish_result" id="publish_result">
            <input type="hidden" name="tax_mem_email" value="<?php echo $taxbill_view['tax_mem_email'];?>">
            <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $taxbill_view['member_no']?>">
            <input type="hidden" name="mem_com_nm" id="mem_com_nm" value="<?php echo $taxbill_view['mem_com_nm']?>">
            <input type="hidden" name="mem_com_no" id="mem_com_no" value="<?php echo $taxbill_view['mem_com_no']?>">
            <input type="hidden" name="mem_pay_later" id="mem_pay_later" value="<?php echo $taxbill_view['mem_pay_later']?>">
            <input type="hidden" name="role" id="role" value="<?php echo $taxbill_view['role']?>">
            <input type="hidden" name="publish_mem_no" id="publish_mem_no" value="<?php echo $this->session->userdata('mem_no')?>">
            <input type="hidden" name="publish_mem_nm" id="publish_mem_nm" value="<?php echo $this->session->userdata('mem_nm')?>">
            
            <table class="aut_tb wid_90p">
                <thead>
                    <tr>
                        <th class="bd_l1"><?php echo lang('strPeriod')?></th>
                        <th class="bd_l1"><?php echo lang('strDetailCont')?></th>
                        <th class="bd_l1"><?php echo lang('strPublishPrice')?></th>
                        <th class="bd_l1"><?php echo lang('strTaxPrice')?></th>
                        <th class="bd_l1"><?php echo lang('strSum')?></th>
                    </tr>
                </thead>

                <tbody>
                    <tr id="prepend_standard">
                        <td class="txt_center" colspan="2"><?php echo lang('strSum')?></td>
                        <td class="txt_right" id="sum_price">0</td>
                        <td class="txt_right" id="sum_tax">0</td>
                        <td class="txt_right" id="sum_result">0</td>
                    </tr>
                </tbody>
            </table>
            <div class="wid_970p">
                <table class="aut_tb wid_90p">
                <colgroup>
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
                <col width="25%" />
                </colgroup>
                    <tr>
                        <th><?php echo lang('strTaxInvoice')?> <?php echo lang('strWriteDate')?></th>
                        <td class="txt_center"><?php echo date('Y-m-d')?></td>
                        <!-- 
                    </tr>
                    <tr id="prepend_standard">
                     -->
                        <th class="bd_l1"><?php echo lang('strTaxInvoice')?> <?php echo lang('strPublishDate')?></th>
                        <td class="txt_center">
            				<input type="text" name="publish_dt" id="daterange" class="float_l ma_r10 range" value="" />
            				<span class="iput-group-addon float_l ma_t8">
            					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
            				</span>
                        </td>
                    </tr>
                </table>
            </div>
    	</div>
	</form>
	<div class="m_center txt_center pa_b20">
	   <span class="btn btn-primary ma_t20 " onClick ="location.replace('/calculate/tax_bill_list')"><?php echo lang('strCancel')?></span>
	   <span class="btn btn-primary ma_t20 " onClick ="publish_tax_bill();"><?php echo lang('strPublication')?></span>
	   <!--
	   <span class="btn btn-primary ma_t20 " onClick ="temp_save();"><?php echo lang('strTmpSave')?></span>
	   <span class="btn btn-primary ma_t20 " onClick ="carry_over();"><?php echo lang('strCarryOver')?></span>
	   -->
	</div>
</div>
        
<script type="text/javascript">
    $(function(){
    	$('input[name="publish_dt"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            format : 'YYYY-MM-DD'
        });
    });

    function change_year(year){
        $("#year_view").html(year);
        $("#year").val(year);
    }

    function change_month(month){
        $("#month_view").html(month);
        $("#month").val(month);
    }


    function check_publish_list(tax_no, date_ym, tax_cont, price, tax, result){
        if ($("#add_chk_"+tax_no).is(":checked")){
			//체크시
            var add_tr = "<tr id='taxbill_detail_"+tax_no+"'>";
            add_tr+="<td class='txt_center'>"+date_ym+"</td>";
            add_tr+="<td class='txt_center'>"+tax_cont+"</td>";
            add_tr+="<td class='txt_right'>"+comma(price)+"</td>";
            add_tr+="<td class='txt_right'>"+comma(tax)+"</td>";
            add_tr+="<td class='txt_right'>"+comma(result)+"</td>";
            add_tr+="</tr>";
            
            $("#prepend_standard").before(add_tr);
            
            var sum_price = uncomma($("#sum_price").html());
            var tmp_price = comma(Number(sum_price)+Number(price));
            var sum_tax = uncomma($("#sum_tax").html());
            var tmp_tax = comma(Number(sum_tax)+Number(tax));
            var sum_result = uncomma($("#sum_result").html());
            var tmp_result = comma(Number(sum_result)+Number(result));
            
            $("#sum_price").html(tmp_price);
            $("#sum_tax").html(tmp_tax);
            $("#sum_result").html(tmp_result);

        }else{

        	var sum_price = uncomma($("#sum_price").html());
            var tmp_price = comma(Number(sum_price)-Number(price));
            var sum_tax = uncomma($("#sum_tax").html());
            var tmp_tax = comma(Number(sum_tax)-Number(tax));
            var sum_result = uncomma($("#sum_result").html());
            var tmp_result = comma(Number(sum_result)-Number(result));
            
            $("#sum_price").html(tmp_price);
            $("#sum_tax").html(tmp_tax);
            $("#sum_result").html(tmp_result);
            $("#taxbill_detail_"+tax_no).remove();
        }  
    }

//정산내역 추가 관련 스크립트
    function add_taxbill_list(){
    	var add_tr = "<tr id='add_row'>";
        add_tr+="<td class='txt_center'><input type='text' class='wid_150' id='add_date_ym' placeholder='ex) 2015-08'/></td>";
        add_tr+="<td class='txt_center'><input type='text' id='add_tax_cont'/></td>";
        add_tr+="<td class='txt_right'><input type='text' class='wid_100' id='add_price' onkeyup='add_auto_cal()'/></td>";
        add_tr+="<td class='txt_right'><input type='text' class='wid_100' id='add_tax_price'/></td>";
        add_tr+="<td class='txt_right'><input type='text' class='wid_100' id='add_result_price'/></td>";
        add_tr+="<td colspan='4' class='txt_center'>";
        add_tr+="<span class='btn btn-primary' onClick ='taxbill_add_save();'><?php echo lang('strSave')?></span>&nbsp;";
        add_tr+="<span class='btn btn-primary' onClick ='add_cancel();'><?php echo lang('strCancel')?></span>";
        add_tr+="</td>";
        add_tr+="</tr>";
        $("#list_row").append(add_tr);
    }

    function add_auto_cal(cal_tax_no){
    	var tmp_price = uncomma($("#add_price").val());
    	var tax_price = Number(tmp_price) * 0.1;
    	var result_price = Number(tax_price)+Number(tmp_price);
    	$("#add_price").val(comma(tmp_price));
    	$("#add_tax_price").val(comma(tax_price));
    	$("#add_result_price").val(comma(result_price)); 
    }
    
    function add_cancel(){
        $("#add_row").remove();
    }

    function taxbill_add_save(){
    	var price = uncomma($("#add_price").val());
    	var tax_price = uncomma($("#add_tax_price").val());
    	var result_price = uncomma($("#add_result_price").val());
    	var date_ym = $("#add_date_ym").val();
    	var tax_cont = $("#add_tax_cont").val();
    	var mem_no = '<?php echo $taxbill_view->member_no?>';
        var url = '/calculate/tax_bill_detail_add';
        $.post(url,
        {
            price : price,
            tax_price : tax_price,
            result_price : result_price,
            date_ym : date_ym,
            tax_cont : tax_cont,
            mem_no : mem_no
        },
        function(data){
            if(data.trim()=="ok"){
                alert("저장되었습니다.");
                location.reload();
            }
        }
        );
    }
//정산내역 추가 관련 스크립트 끝
    //정산내역 수정 관련 스크립트
    function taxbill_modify(cal_tax_no){
    	$("#taxbill_view_"+cal_tax_no).hide();
    	$("#taxbill_modify_"+cal_tax_no).show();
    }
        
    function modify_auto_cal(cal_tax_no){
    	var tmp_price = uncomma($("#modify_price_"+cal_tax_no).val());
    	var tax_price = Number(tmp_price) * 0.1;
    	var result_price = Number(tax_price)+Number(tmp_price);
    	$("#modify_price_"+cal_tax_no).val(comma(tmp_price));
    	$("#modify_tax_price_"+cal_tax_no).val(comma(tax_price));
    	$("#modify_result_price_"+cal_tax_no).val(comma(result_price)); 
    }

    function taxbill_modify_save(cal_tax_no){
    	var price = uncomma($("#modify_price_"+cal_tax_no).val());
    	var tax_price = uncomma($("#modify_tax_price_"+cal_tax_no).val());
    	var result_price = uncomma($("#modify_result_price_"+cal_tax_no).val());
    	var date_ym = $("#modify_date_ym_"+cal_tax_no).val();
    	var tax_cont = $("#modify_tax_cont_"+cal_tax_no).val();
        var url = '/calculate/tax_bill_detail_modify_save';
        $.post(url,
        {
            price : price,
            tax_price : tax_price,
            result_price : result_price,
            date_ym : date_ym,
            calculate_tax_no : cal_tax_no,
            tax_cont : tax_cont
        },
        function(data){
            if(data.trim()=="ok"){
                alert("수정되었습니다.");
                location.reload();
            }else{
                alert("캠페인 이름이");
            }
        }
        );
    }

    function modify_cancel(cal_tax_no){
    	$("#taxbill_view_"+cal_tax_no).show();
    	$("#taxbill_modify_"+cal_tax_no).hide();
    }
    //정산내역 수정 관련 스크립트 끝
    //버튼 처리에 대한 스트립트
    function publish_tax_bill(){
        var tax_email = '<?php echo $taxbill_view['tax_mem_email']?>';
    	if ($('input:checkbox[name="chk_cal_tax_no[]"]:checked').length > 0){
        	if (tax_email == ""){
            	alert("등록된 메일주소가 없습니다.");
            	return false;
        	}
        	if ($("#daterange").val() == ""){
            	alert("계산서 발행일을 입력해 주세요.");
        	}else{
        		if(confirm("<?php echo lang('strPublishTaxAlert');?>")){
                    var frm = document.tax_bill_history;
                    frm.publish_price.value = uncomma($("#sum_price").html());
                    frm.publish_tax.value = uncomma($("#sum_tax").html());
                    frm.publish_result.value = uncomma($("#sum_result").html());
                    frm.action = "/calculate/publish_tax_bill";
                    frm.submit();
        		}
        	}
    	}else{
        	alert("정산할 정산서를 선택하세요.");
    	}
    }

    function temp_save(){
        var frm = document.tax_bill_history;
        frm.publish_price.value = uncomma($("#sum_price").html());
        frm.publish_tax.value = uncomma($("#sum_tax").html());
        frm.publish_result.value = uncomma($("#sum_result").html());
        frm.action = "/calculate/tax_bill_temp_save";
        frm.submit();
    }
    
    function carry_over(){
    	if(confirm("이월하시겠습니까?")){
            var history_no = '<?php echo $taxbill_view['history_no']?>';
            var mem_no = '<?php echo $taxbill_view['member_no']?>';
            var date_ym = '<?php echo $date_ym?>';
            var url = '/calculate/tax_bill_carry_over';
            $.post(url,
            {
                history_no : history_no,
                mem_no : mem_no,
                date_ym : date_ym
            },
            function(data){
                if(data.trim()=="ok"){
                    alert("이월처리가 완료되었습니다.");
                    location.replace('/calculate/tax_bill_list');
                }
            }
            );
    	}
    }

    function forward_change(cal_tax_no){
    	if ($("#forward_fl_"+cal_tax_no).is(":checked")){
    		var publish_fl = '3';
    		var forward_fl = 'Y';
    		var history_no = '';
    		var str = "이월처리가 완료되었습니다.";
        		
    	}else{
    		var publish_fl = '1';
    		var forward_fl = 'N';
    		var history_no = '<?php echo $taxbill_view['history_no'];?>';
    		var str = "이월해제가 완료되었습니다.";
    	}
    		var calculate_tax_no = cal_tax_no;
            var url = '/calculate/tax_bill_detail_forward';
            $.post(url,
            {
            	calculate_tax_no : calculate_tax_no,
            	publish_fl : publish_fl,
            	forward_fl : forward_fl,
            	history_no : history_no
            },
            function(data){
                if(data.trim()=="ok"){
                    alert("이월처리가 완료되었습니다.");
                    location.reload();
            }
        });
	}
    

    // 공통 스크립트
    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }

    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }  
    
    
</script>
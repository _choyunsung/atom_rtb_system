<?php
    function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
        if ($force_number) {
            $number = preg_replace('/^[^\d]+/', '', $number);
        } else if (preg_match('/^[^\d]+/', $number)) {
            return false;
        }
        $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
        $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
        settype($number, $type);
        return $number;
    }
    
    $site_lang = $this->session->userdata('site_lang');
    
    if($site_lang == "korean"){
        $currency = "KRW";
    }else{
        $currency = "USD";
    }
    
    if($currency == "KRW"){
        $currency_unit = "₩";
        $currency_loc_unit = lang('strWon');
    }elseif($currency == "USD"){
        $currency_unit = "$";
        $currency_loc_unit = lang('strDollar');
    }
?>
<!-- 리스트 -->
<div class=" ma_t20">
<div class="panel-body">
    <div class="camp2">
        <div class="camp_div6b" id="deposit_img" onclick="javascipt:select_chart_option('deposit');">
        <p class="camp_tit"><?php echo lang('strDeposited');?></p><span class="camp_con" id="sum_deposit">0</span>
        </div>
        <div class="camp_div6b" id="price_img" onclick="javascipt:select_chart_option('price');">
        <p class="camp_tit"><?php echo lang('strRevenue');?></p><span class="camp_con" id="sum_price">0</span>
        </div>
        <div class="camp_div6b" id="refund_img" onclick="javascipt:select_chart_option('refund');">
        <p class="camp_tit"><?php echo lang('strRefund');?></p><span class="camp_con" id="sum_refund">0</span>
        </div>
        <div class="camp_div6b" id="fee_price_img" onclick="javascipt:select_chart_option('fee_price');">
        <p class="camp_tit"><?php echo lang('strFee');?></p><span class="camp_con" id="sum_fee_price">0</span>
        </div>
        <div class="camp_div6b" id="revenue_img" onclick="javascipt:select_chart_option('revenue');">
        <p class="camp_tit"><?php echo lang('strRealRevenue');?></p><span class="camp_con" id="sum_revenue">0</span>
        </div>
        <div class="camp_div6b" id="pre_deposit_img" onclick="javascipt:select_chart_option('pre_deposit');">
        <p class="camp_tit"><?php echo lang('strResidualDeposit');?></p><span class="camp_con" id="sum_pre_deposit">0</span>
        </div>
    </div>
    <div id="revenue_intergration_chart"></div>
</div>

<div class="panel-heading">
</div>

<div class="panel-body">
    <form name="revenue_integration_detail" method="post">
        <input type="hidden" name="pre_deposit_val" id="pre_deposit_val" value="<?php echo $option_pre_deposit?>">
        <input type="hidden" name="deposit_val" id="deposit_val" value="<?php echo $option_deposit?>">
        <input type="hidden" name="price_val" id="price_val" value="<?php echo $option_price?>">
        <input type="hidden" name="fee_price_val" id="fee_price_val" value="<?php echo $option_fee_price?>">
        <input type="hidden" name="refund_val" id="refund_val" value="<?php echo $option_refund?>">
        <input type="hidden" name="revenue_val" id="revenue_val" value="<?php echo $option_revenue?>">
        <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
            <colgroup>
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <thead>
                <tr>
                	<th rowspan="2"><?php echo lang('strDate');?></th>
                	<th colspan="2"><?php echo lang('strDeposited');?></th>
                	<th colspan="2"><?php echo lang('strRevenue');?></th>
                	<th colspan="2"><?php echo lang('strRefund');?></th>
                	<th colspan="2"><?php echo lang('strFee');?></th>
                	<th colspan="2"><?php echo lang('strRealRevenue');?></th>
                	<th colspan="2"><?php echo lang('strResidualDeposit');?></th>
                </tr>
                <tr>
                    <th><?php echo lang('strPrePay');?></th>
                    <th><?php echo lang('strPayLater');?></th>     
                    <th><?php echo lang('strPrePay');?></th>
                    <th><?php echo lang('strPayLater');?></th>
                    <th><?php echo lang('strPrePay');?></th>
                    <th><?php echo lang('strPayLater');?></th>     
                    <th><?php echo lang('strPrePay');?></th>
                    <th><?php echo lang('strPayLater');?></th>     
                    <th><?php echo lang('strPrePay');?></th>
                    <th><?php echo lang('strPayLater');?></th>     
                    <th><?php echo lang('strPrePay');?></th>
                    <th><?php echo lang('strPayLater');?></th>                
                </tr>
            </thead>
            <tbody>
                <?php
                    $chart_data = array();
                    $sum_pre_deposit = 0;
                    $sum_pre_deposit_lt = 0;
                    $sum_pre_deposit_ = 0;
                    $sum_deposit = 0;
                    $sum_deposit_lt = 0;
                    $sum_deposit_ = 0;
                    $sum_price = 0;
                    $sum_price_lt = 0;
                    $sum_price_ = 0;
                    $sum_fee_price = 0;
                    $sum_fee_price_lt = 0;
                    $sum_fee_price_ = 0;
                    $sum_refund = 0;
                    $sum_refund_lt = 0;
                    $sum_refund_ = 0;
                    $sum_revenue = 0;
                    $sum_revenue_lt = 0;
                    $sum_revenue_ = 0;
                    
                    foreach($revenue_list as $key=>$rev){
                        if($currency == "KRW"){
                            $pre_deposit = number_format(round($rev['loc_pre_deposit'], 0));
                            $pre_deposit_lt = number_format(round($rev['loc_pre_deposit_lt'], 0));
                            $deposit = number_format(round($rev['loc_deposit'], 0));
                            $deposit_lt = number_format(round($rev['loc_deposit_lt'], 0));
                            $price = number_format(round($rev['loc_price'], 0));
                            $price_lt = number_format(round($rev['loc_price_lt'], 0));
                            $fee_price = number_format(round($rev['loc_fee_price'], 0));
                            $fee_price_lt = number_format(round($rev['loc_fee_price_lt'], 0));
                            $refund = number_format(round($rev['loc_refund'], 0));
                            $refund_lt = number_format(round($rev['loc_refund_lt'], 0));
                            $revenue = number_format(round($rev['loc_revenue'], 0));
                            $revenue_lt = number_format(round($rev['loc_revenue_lt'], 0));
                            
                            $chart_data[$key]['pre_deposit'] = number_format(round($rev['loc_pre_deposit'] + $rev['loc_pre_deposit_lt'], 0));
                            $chart_data[$key]['deposit'] = number_format(round($rev['loc_deposit'] + $rev['loc_deposit_lt'], 0));
                            $chart_data[$key]['price'] = number_format(round($rev['loc_price'] + $rev['loc_price_lt'], 0));
                            $chart_data[$key]['fee_price'] = number_format(round($rev['loc_fee_price'] + $rev['loc_fee_price_lt'], 0));
                            $chart_data[$key]['refund'] = number_format(round($rev['loc_refund'] + $rev['loc_refund_lt'], 0));
                            $chart_data[$key]['revenue'] = number_format(round($rev['loc_revenue'] + $rev['loc_revenue_lt'], 0));
                            
                            $sum_pre_deposit += $rev['loc_pre_deposit'];
                            $sum_pre_deposit_lt += $rev['loc_pre_deposit_lt'];         
                            $sum_deposit += $rev['loc_deposit'];
                            $sum_deposit_lt += $rev['loc_deposit_lt'];
                            $sum_price += $rev['loc_price'];
                            $sum_price_lt += $rev['loc_price_lt'];
                            $sum_fee_price += $rev['loc_fee_price'];
                            $sum_fee_price_lt += $rev['loc_fee_price_lt'];  
                            $sum_refund += $rev['loc_refund'];
                            $sum_refund_lt += $rev['loc_refund_lt'];
                            $sum_revenue += $rev['loc_revenue'];
                            $sum_revenue_lt += $rev['loc_revenue_lt'];
                            
                            $sum_pre_deposit_ += ($rev['loc_pre_deposit'] + $rev['loc_pre_deposit_lt']);
                            $sum_deposit_ += ($rev['loc_deposit'] + $rev['loc_deposit_lt']);
                            $sum_price_ += ($rev['loc_price'] + $rev['loc_price_lt']);
                            $sum_fee_price_ += ($rev['loc_fee_price'] + $rev['loc_fee_price_lt']);
                            $sum_refund_ += ($rev['loc_refund'] + $rev['loc_refund_lt']);
                            $sum_revenue_ += ($rev['loc_revenue'] + $rev['loc_revenue_lt']);
                        
                        }else if($currency == "USD"){
                            $pre_deposit = number_format(round($rev['pre_deposit'], 2), 2);
                            $pre_deposit_lt = number_format(round($rev['pre_deposit_lt'], 2), 2);
                            $deposit = number_format(round($rev['deposit'], 2), 2);
                            $deposit_lt = number_format(round($rev['deposit_lt'], 2), 2);
                            $price = number_format(round($rev['price'], 2), 2);
                            $price_lt = number_format(round($rev['price_lt'], 2), 2);
                            $fee_price = number_format(round($rev['fee_price'], 2), 2);
                            $fee_price_lt = number_format(round($rev['fee_price_lt'], 2), 2);
                            $refund = number_format(round($rev['refund'], 2), 2);
                            $refund_lt = number_format(round($rev['refund_lt'], 2), 2);
                            $revenue = number_format(round($rev['revenue'], 2), 2);
                            $revenue_lt = number_format(round($rev['revenue_lt'], 2), 2);
                            
                            $chart_data[$key]['pre_deposit'] = number_format(round($rev['pre_deposit'] + $rev['pre_deposit_lt'], 2), 2);
                            $chart_data[$key]['deposit'] = number_format(round($rev['deposit'] + $rev['deposit_lt'], 2), 2);
                            $chart_data[$key]['price'] = number_format(round($rev['price'] + $rev['price_lt'], 2), 2);
                            $chart_data[$key]['fee_price'] = number_format(round($rev['fee_price'] + $rev['fee_price_lt'], 2), 2);
                            $chart_data[$key]['refund'] = number_format(round($rev['refund'] + $rev['refund_lt'], 2), 2);
                            $chart_data[$key]['revenue'] = number_format(round($rev['revenue'] + $rev['revenue_lt'], 2), 2);
                            
                            $sum_pre_deposit += $rev['pre_deposit'];
                            $sum_pre_deposit_lt += $rev['pre_deposit_lt'];
                            $sum_deposit += $rev['deposit'];
                            $sum_deposit_lt += $rev['deposit_lt'];
                            $sum_price += $rev['price'];
                            $sum_price_lt += $rev['price_lt'];
                            $sum_fee_price += $rev['fee_price'];
                            $sum_fee_price_lt += $rev['fee_price_lt'];
                            $sum_refund += $rev['refund'];
                            $sum_refund_lt += $rev['refund_lt'];
                            $sum_revenue += $rev['revenue'];
                            $sum_revenue_lt += $rev['revenue_lt'];
                            
                            $sum_pre_deposit_ += ($rev['pre_deposit'] + $rev['pre_deposit_lt']);
                            $sum_deposit_ += ($rev['deposit'] + $rev['deposit_lt']);
                            $sum_price_ += ($rev['price'] + $rev['price_lt']);
                            $sum_fee_price_ += ($rev['fee_price'] + $rev['fee_price_lt']);
                            $sum_refund_ += ($rev['refund'] + $rev['refund_lt']);
                            $sum_revenue_ += ($rev['revenue'] + $rev['revenue_lt']);
                        }
                        
                        $chart_data[$key]['date_ymd'] = $rev['date_ymd'];

                ?>
                <tr>
                	<td style="text-align:center;padding-right:5px;">
                        <?php 
                            if($term == "day"){
                                echo $rev['date_ymd'];
                            }else if($term == "week"){
                                $year = substr($rev['date_ymd'], 0, 4);  // 2000
                                $month = substr($rev['date_ymd'], 5, 2);  // 1, 2, 3, ..., 12
                                $day = substr($rev['date_ymd'], 8, 2);  // 1, 2, 3, ..., 31
                                $cur_day = date("w", mktime(0, 0, 0, $month, $day, $year));
                                $minus_day = 6 - $cur_day;
                                $week_first = date("Y-m-d", mktime(0, 0, 0, $month, $day - $cur_day, $year));
                                $week_last  = date("Y-m-d", mktime(0, 0, 0, $month, $day + $minus_day, $year));
                                $date_ymd = $rev['date_ymd'];
                                echo $date_ymd."<br/>~".$week_last;
                            }else if($term == "month"){
                                $date_ymd_temp = explode( "-", $rev['date_ymd']);
                                echo $date_ymd_temp[1];
                                
                            }
                        ?>
                	</td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $deposit;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $deposit_lt;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $price;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $price_lt;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $refund;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $refund_lt;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $fee_price;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $fee_price_lt;?></td>                              
                    <td style="text-align:right;padding-right:5px;"><?php echo $revenue;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $revenue_lt;?></td>      
                    <td style="text-align:right;padding-right:5px;"><?php echo $pre_deposit;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $pre_deposit_lt;?></td>
                </tr>
                <?php
                    }

                    if($currency == "KRW"){
                        
                        $sum_pre_deposit = number_format(round($sum_pre_deposit, 0));
                        $sum_pre_deposit_lt = number_format(round($sum_pre_deposit_lt, 0));
                        $sum_deposit = number_format(round($sum_deposit, 0));
                        $sum_deposit_lt = number_format(round($sum_deposit_lt, 0));
                        $sum_price = number_format(round($sum_price, 0));
                        $sum_price_lt = number_format(round($sum_price_lt, 0));
                        $sum_fee_price = number_format(round($sum_fee_price, 0));
                        $sum_fee_price_lt = number_format(round($sum_fee_price_lt, 0));
                        $sum_refund = number_format(round($sum_refund, 0));
                        $sum_refund_lt = number_format(round($sum_refund_lt, 0));
                        $sum_revenue = number_format(round($sum_revenue, 0));
                        $sum_revenue_lt = number_format(round($sum_revenue_lt, 0));
                        
                        $sum_pre_deposit_ = number_format(round($sum_pre_deposit_, 0));
                        $sum_deposit_ = number_format(round($sum_deposit_, 0));
                        $sum_price_ = number_format(round($sum_price_, 0));
                        $sum_fee_price_ = number_format(round($sum_fee_price_, 0));
                        $sum_refund_ = number_format(round($sum_refund_, 0));
                        $sum_revenue_ = number_format(round($sum_revenue_, 0));
                    
                    }else if($currency == "USD"){
                   
                        $sum_pre_deposit = number_format(round($sum_pre_deposit, 2), 2);
                        $sum_pre_deposit_lt = number_format(round($sum_pre_deposit_lt, 2), 2);
                        $sum_deposit = number_format(round($sum_deposit, 2), 2);
                        $sum_deposit_lt = number_format(round($sum_deposit_lt, 2), 2);
                        $sum_price = number_format(round($sum_price, 2), 2);
                        $sum_price_lt = number_format(round($sum_price_lt, 2), 2);
                        $sum_fee_price = number_format(round($sum_fee_price, 2), 2);
                        $sum_fee_price_lt = number_format(round($sum_fee_price_lt, 2), 2);
                        $sum_refund = number_format(round($sum_refund, 2), 2);
                        $sum_refund_lt = number_format(round($sum_refund_lt, 2), 2);
                        $sum_revenue = number_format(round($sum_revenue, 2), 2);
                        $sum_revenue_lt = number_format(round($sum_revenue_lt, 2), 2);
                        
                        $sum_pre_deposit_ = number_format(round($sum_pre_deposit_, 2), 2);
                        $sum_deposit_ = number_format(round($sum_deposit_, 2), 2);
                        $sum_price_ = number_format(round($sum_price_, 2), 2);
                        $sum_fee_price_ = number_format(round($sum_fee_price_, 2), 2);
                        $sum_refund_ = number_format(round($sum_refund_, 2), 2);
                        $sum_revenue_ = number_format(round($sum_revenue_, 2), 2);
                    }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td rowspan="2" style="text-align:center;padding-right:5px;">
                        <?php echo lang('strSum');?>
                    </td>

                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_deposit;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_deposit_lt;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_price;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_price_lt;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_refund;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_refund_lt;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_fee_price;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_fee_price_lt;?></td>                        
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_revenue;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_revenue_lt;?></td>    
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_pre_deposit;?></td>
                    <td style="text-align:right;padding-right:5px;"><?php echo $sum_pre_deposit_lt;?></td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align:right;padding-right:5px;"><?php echo $sum_deposit_;?></td>
                    <td colspan="2" style="text-align:right;padding-right:5px;"><?php echo $sum_price_;?></td>
                    <td colspan="2" style="text-align:right;padding-right:5px;"><?php echo $sum_refund_;?></td>
                    <td colspan="2" style="text-align:right;padding-right:5px;"><?php echo $sum_fee_price_;?></td>
                    <td colspan="2" style="text-align:right;padding-right:5px;"><?php echo $sum_revenue_;?></td>
                    <td colspan="2" style="text-align:right;padding-right:5px;"><?php echo $sum_pre_deposit_;?></td>
                </tr>
            </tfoot>
        </table>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('#sum_deposit').html('<?php echo $sum_deposit_;?>');
        $('#sum_price').html('<?php echo $sum_price_;?>');
        $('#sum_refund').html('<?php echo $sum_refund_;?>');
        $('#sum_fee_price').html('<?php echo $sum_fee_price_;?>');
        $('#sum_revenue').html('<?php echo $sum_revenue_;?>');
        $('#sum_pre_deposit').html('<?php echo $sum_pre_deposit_;?>');

		if($('#pre_deposit_val').val() == "Y"){
        /* $('#imp_img').attr('src','/img/radio_on.png'); */
           $('#pre_deposit_text').css('color', '#e87a75');
           $('#pre_deposit_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
        }else{
           $('#pre_deposit_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#deposit_val').val() == "Y"){
           $('#deposit_text').css('color', '#a783cc');
           $('#deposit_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #a783cc"});
        }else{
           $('#deposit_text').css('color', '#d7d7d7');
           $('#deposit_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#price_val').val() == "Y"){
           $('#price_text').css('color', '#ffc600');
           $('#price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
        }else{
           $('#price_text').css('color', '#d7d7d7');
           $('#price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#fee_price_val').val() == "Y"){
           $('#fee_price_text').css('color', '#97cc00');
           $('#fee_price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
        }else{
           $('#fee_price_text').css('color', '#d7d7d7');
           $('#fee_price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#refund_val').val() == "Y"){
           $('#refund_text').css('color', '#668eda');
           $('#refund_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
        }else{
           $('#refund_text').css('color', '#d7d7d7');
           $('#refund_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
        if($('#revenue_val').val() == "Y"){
           $('#revenue_text').css('color', '#e87a75');
           $('#revenue_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
        }else{
           $('#revenue_text').css('color', '#d7d7d7');
           $('#revenue_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
        }
    });

    function select_chart_option(kind){
        if(kind == "pre_deposit"){
            if($('#pre_deposit_val').val() == "Y"){
                $('#pre_deposit_val').attr('value', 'N');
                $('#pre_deposit_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#pre_deposit_val').attr('value', 'Y');
                $('#pre_deposit_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #4db4ff"});
            }
        }else if(kind == "deposit"){
            if($('#deposit_val').val() == "Y"){
                $('#deposit_val').attr('value', 'N');
                $('#deposit_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#deposit_val').attr('value', 'Y');
                $('#deposit_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #a783cc"});
            }
        }else if(kind == "price"){
            if($('#price_val').val() == "Y"){
                $('#price_val').attr('value', 'N');
                $('#price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#price_val').attr('value', 'Y');
                $('#price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #ffc600"});
            }
        }else if(kind == "fee_price"){
            if($('#fee_price_val').val() == "Y"){
                $('#fee_price_val').attr('value', 'N');
                $('#fee_price_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#fee_price_val').attr('value', 'Y');
                $('#fee_price_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #97cc00"});
            }
        }else if(kind == "refund"){
            if($('#refund_val').val() == "Y"){
                $('#refund_val').attr('value', 'N');
                $('#refund_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#refund_val').attr('value', 'Y');
                $('#refund_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #668eda"});
            }
        }else if(kind == "revenue"){
            if($('#revenue_val').val() == "Y"){
                $('#revenue_val').attr('value', 'N');
                $('#revenue_img').css({"padding-bottom":"3px","background":"#fff","border-bottom":"1px solid #ddd"});
            }else{
                $('#revenue_val').attr('value', 'Y');
                $('#revenue_img').css({"padding-bottom":"2px","background":"#f2f2f2","border-bottom":"2px solid #e87a75"});
            }
        }

        var pre_deposit = $('#pre_deposit_val').val();
        var deposit = $('#deposit_val').val();
        var price = $('#price_val').val();
        var fee_price = $('#fee_price_val').val();
        var refund = $('#refund_val').val();
        var revenue = $('#revenue_val').val();

        chart_option(pre_deposit, deposit, price, fee_price, refund, revenue);
    }

    function chart_option(pre_deposit, deposit, price, fee_price, refund, revenue){
        parent.revenue_intergration_detail('<?php echo $daterange?>', '<?php echo $term?>', pre_deposit, deposit, price, fee_price, refund, revenue);

    }
</script>
<?php
    $kind = "day";
    
    if($option_revenue == "N"){
        $option_revenue = "Y";
    }
?>
<script type="text/javascript">

	$(function () {
	   $('#revenue_intergration_chart').highcharts({
            chart:{
                type:'spline',
                height: 350
            },
            title: {
                style: {
                    fontSize: '17px',
                    fontFamily: 'Verdana, sans-serif'
                },
                text: '',
                x: 20
            },
            subtitle: {
                text: '',
                x: 20
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    events: {},
                    lineWidth: 2,
                    marker: {
                        radius:3,
                        symbol:'circle'
                    }
                }
            },
            xAxis: {
                <?php
                    if($term == "day"){
                ?>
                categories: [
                    <?php
                        foreach($chart_data as $key=>$cd){
                            $date_ymd = $cd['date_ymd'];
                    ?>
                        '<?php echo $date_ymd;?>',
                    <?php
                        }
                    ?>
                     ],

                 <?php
                    }else if($term == "month"){
                 ?>
                 categories: [
                     <?php
                         foreach($chart_data as $key=>$cd){
                             $date_ymd = $cd['date_ymd'];
                     ?>
                         '<?php $date_ymd_temp = explode( "-", $date_ymd);echo $date_ymd_temp[1];?>',
                     <?php
                         }
                     ?>
                      ],     
                <?php
                    }else if($term == "week"){
                ?>
                categories: [
                    <?php
                        foreach ($chart_data as $key=>$cd) {
                            $year = substr($cd['date_ymd'], 0, 4);  // 2000
                            $month = substr($cd['date_ymd'], 5, 2);  // 1, 2, 3, ..., 12
                            $day = substr($cd['date_ymd'], 8, 2);  // 1, 2, 3, ..., 31
                            $cur_day = date("w", mktime(0, 0, 0, $month, $day, $year));
                            $minus_day = 6 - $cur_day;
                            $week_first = date("Y-m-d", mktime(0, 0, 0, $month, $day - $cur_day, $year));
                            $week_last  = date("Y-m-d", mktime(0, 0, 0, $month, $day + $minus_day, $year));
                            $date_ymd = $cd['date_ymd'];
                    ?>
                    '<?php echo $date_ymd?>~<?php echo $week_last?>',
                    <?php
                        }
                    ?>
                    ],
                <?php
                    }
                ?>
                gridLineWidth: 0,
                title: {
                    text: ''
                },
                labels: {
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    },
                    overflow: 'justify'
                }
                },
                yAxis: [

                {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {
    
                        enabled: <?php if($option_pre_deposit == "N" && $option_price == "N" && $option_fee_price == "N" && $option_refund == "N"){ echo "true";}else{echo "false";}?>,
    
                        overflow: 'justify',
                        style: {
                            color: '#a780ca',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "예치금",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#a780ca'
                    },
                    format:'{value}',
                    opposite: true,
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {
                        enabled: <?php if($option_pre_deposit == "N" && $option_deposit == "N" && $option_fee_price == "N" && $option_refund == "N"){ echo "true";}else{echo "false";}?>,
                        overflow: 'justify',
                        style: {
                            color: '#ffa200',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: '매출',
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#ffa200'
                    },
                    format:'{value}',
                    opposite: true,
                },
    
                {
                    min:0,
                    minorGridLineWidth: 0,
                    allowDecimals: true,
                    labels: {
    
                        enabled: <?php if($option_pre_deposit == "N" && $option_deposit == "N" && $option_fee_price == "N" && $option_price == "N"){ echo "true";}else{echo "false";}?>,
    
                        overflow: 'justify',
                        style: {
                            color: '#5177bc',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "환불",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#5177bc'
                    },
                    format:'{value}',
                    opposite: true
                },

                {
                    min:0,
                    minorGridLineWidth: 0,
                    allowDecimals: true,
                    labels: {
    
                        enabled: <?php if($option_pre_deposit == "N" && $option_deposit == "N" && $option_price == "N" && $option_refund == "N"){ echo "true";}else{echo "false";}?>,
    
                        overflow: 'justify',
                        style: {
                            color: '#82bd00',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "수수료",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: '#82bd00'
                    },
                    format:'{value}',
                    opposite: true
                },
                {
                    min:0,
                    minorGridLineWidth: 0,
                    gridLineColor: '#e6e6e6',
                    labels: {
                        enabled: true,
                        overflow: 'justify',
                        style: {
                            color: "#c85757",
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "실 매출",
                        align: 'low',
                        rotation: 0,
                        y: 25
                    },
                    style: {
                        color: "#c85757"
                    }
                },

                {
                    min:0,
                    minorGridLineWidth: 0,
                    labels: {
    
                        enabled: true,
    
                        style: {
                            color: '#43acfb',
                            fontSize: '12px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                    title: {
                        enabled: false,
                        text: "잔여예치금",
                        align: 'low',
                        rotation: 0,
                        x: -31,
                        y: 25
                    },
                    style: {
                        color: '#43acfb'
                    },
                    format:'{value}',
                   	opposite: true,
                },
            ],
            tooltip: {
                headerFormat: '<span style="font-size: 12px"><b>{point.key}</b></span><br/>',
                xDateFormat: '%Y-%m-%d',
                shared: true

            },
            legend: {
                enabled:false,
                align: 'center',
                verticalAlign: 'bottom',
                borderWidth: 0
            },
            series: [
                <?php
                 if($option_deposit == 'Y'){
                ?>
                {
                name: "<?php echo lang('strDeposited');?>",
                zIndex:1,
                yAxis: 1,
                color: '#a780ca',
                data: [
                <?php
                 foreach($chart_data as $key=>$cd) {
                     echo number_unformat($cd['deposit']);
                ?>,
                <?php
                 }
                ?>
                ]
                },
                <?php
                 }
                ?>
                <?php
                 if($option_price == 'Y'){
                ?>
                {
                name: '<?php echo lang('strRevenue');?>',
                zIndex:2,
                yAxis: 2,
                color: '#ffa200',
                data: [
                <?php
                 foreach($chart_data as $key=>$cd) {
                     echo number_unformat($cd['price']);
                ?>,
                <?php
                 }
                ?>
                ]
                },
                <?php
                 }
                ?>
                <?php
                 if($option_refund == 'Y'){
                ?>
                {
                name: "<?php echo lang('strReFund');?>",
                zIndex:4,
                yAxis: 4,
                color: '#5177bc',
                data: [
                <?php
                 foreach($chart_data as $key=>$cd){
                     echo number_unformat($cd['refund']);
                ?>,
                <?php
                 }
                ?>
                ]
                },
                <?php
                 }
                ?>
                <?php
                 if($option_fee_price == 'Y'){
                ?>
                {
                name: "<?php echo lang('strFee');?>",
                zIndex:3,
                yAxis: 3,
                color: '#82bd00',
                data: [
                <?php
                 foreach($chart_data as $key => $cd){
                     echo number_unformat($cd['fee_price']);
                ?>,
                <?php
                 }
                ?>
                ]
                },
                <?php
                 }
                ?>
                <?php
                 if($option_revenue == 'Y'){
                ?>
                {
                type: 'column',
                name: "<?php echo lang('strRealRevenue');?>",
                yAxis: 5,
                zIndex: -1,
                color: "#c85757",
                data: [
                <?php
                 foreach($chart_data as $key=>$cd){
                     echo number_unformat($cd['revenue']);
                ?>,
                <?php
                 }
                ?>
                ]
                },
                <?php
                 }
                ?>

                <?php
                    if($option_pre_deposit == 'Y'){
                ?>
                {
                name: "<?php echo lang('strResidualDeposit');?>",
                yAxis: 0,
                zIndex: 0,
                color: '#43acfb',
                data: [
                <?php
                foreach($chart_data as $key=>$cd){
                    echo number_unformat($cd['pre_deposit']);
                ?>,
                <?php
                }
                ?>
                ]
                },
                <?php
                    }
                ?>
            ]
        });
    });

</script>
<script type="text/javascript">
$(document).ready(function() {
	var check_sel = '<?php echo in_array('all', $cond['publication_st']);?>';
	if (check_sel > 0){
		$("input[name='publication_st[]']").prop('checked',true);
	}

	
});

$("#all").click(function(){      // name 이 check_all 인것이 클릭이 되면
	 
    if ( $("#all").is(":checked") ){      // name이 check_all 인 체크박스가 checked 가 되어 있다면
       
         $("[name='publication_st[]']").attr("checked",true);    // class는 box_class 인 체크박스의 속성 checked는 checked이다
    }
    else{      // 그렇지 않으면
       
         $("[name='publication_st[]']").attr("checked","");      // class는 box_class인 체크박스의 속성 checked 는 "" 공백이다
    }
});
function sel_public_st(type){
	if (type == "all"){
		if ( $("#all").is(":checked") ){
			$("input[name='publication_st[]']").prop('checked',true); 
			  
		}else{
			$("input[name='publication_st[]']").prop('checked','');
		}
	}else{
		if ($("#all").is(":checked")){
			$("#all").prop('checked','');
		}else{
			if ($("#wait").is(":checked") && $("#no").is(":checked") && $("#carry").is(":checked") && $("#done").is(":checked")){
				$("#all").prop('checked',true);	
			}
		}
	}
}

</script>
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content" id="modal_content_add">

        </div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
    </div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strNoPaidMgr')?> </h3>
    </div>
    <div class="panel-body">
        <form name="no_paid_list" id="no_paid_list" method="post">
            <input type="hidden" name="mem_no" id="mem_no">
            <input type="hidden" name="date_ym" id="date_ym">
            
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="15%">
                        <col width="*">
                        <col width="10%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strType')?></th>
                        <td>
                            <input type="checkbox" name="type[]" value="'adver'" <?php if (in_array("'adver'", $cond['type']) > 0){echo "checked";}?>> <?php echo lang('strAdvertiser')?>&nbsp;&nbsp;
                            <input type="checkbox" name="type[]" value="'agency'" <?php if (in_array("'agency'", $cond['type']) > 0){echo "checked";}?>> <?php echo lang('strAgency')?>&nbsp;&nbsp;
                            <input type="checkbox" name="type[]" value="'lab'" <?php if (in_array("'lab'", $cond['type']) > 0){echo "checked";}?>> <?php echo lang('strLab')?>&nbsp;&nbsp;
                        </td>
                        <td rowspan="2" class="txt_center">
                            <span class="btn btn-primary" onClick ="search();"><?php echo lang('strSearch')?></span>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strCompanyName')?></th>
                        <td>
                            <input type="text" class="wid_40p" name="mem_com_nm" value="<?php echo $cond['mem_com_nm'];?>">
                        </td>
                    </tr>
                </table>
         	</div>
            <div class="pa_b20">
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
    			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
    			    <colgroup>
    			         <col width="15%">
     			         <col width="10%">
    			         <col width="10%">
    			         <col width="7%">
    			         <col width="20%">
    			         <col width="10%">
    			         <col width="10%">
    			    </colgroup>
                    <thead>
                        <tr>
                            <th><?php echo lang('strCompanyName')?></th>
                            <th>ID</th>
                            <th><?php echo lang('strType')?></th>                    
                            <th><?php echo lang('strNoPaidAmount')?></th>
                            <th><?php echo lang('strPaidDate')?></th>
                            <th><?php echo lang('strPaidAmount')?></th>
                          	<th><?php echo "비고"?></th>
                            <th><?php echo lang('strModify')?></th>
                        </tr>
                        <tr>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($nopaid_list)){ ?>
                            <?php foreach ($nopaid_list as $row){?>
                                <tr>
                                    <td class="txt_center">
                                        <a href="javascript:detail_list('<?php echo $row['mem_no']?>');">
                                            <?php echo $row['mem_com_nm']?>
                                        </a>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['mem_id']?><br/>(<?php echo $row['mem_type']?>)
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['role'] == "adver"){
                                                echo lang('strAdvertiser');
                                            }elseif ($row['role'] == "agency"){
                                                echo lang('strAgency');
                                            }elseif ($row['role'] == "ind"){
                                                echo lang('strIndividual');
                                            }elseif ($row['role'] == "lab"){
                                                echo lang('strLab');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo number_format($row['loc_price_lt'] - $this->calculate_db->select_sum_deposit_price($row['mem_no']))?>
                                    </td>
                                    <td class="txt_center">
                        				<input type="text" name="paid_dt" id="daterange_<?php echo $row['mem_no'];?>" class="range" style="width:120px !important;" />
                    					<i style="cursor:pointer;" class="fa fa-calendar fa-lg range" onclick="$('#daterange_<?php echo $row['mem_no'];?>').focus();"></i>
                                    </td>
                                    <td class="txt_center">
                                        <input type="text" id="detail_amount_<?php echo $row['mem_no'];?>" style="width:140px !important;">
                                    </td>
                                    <td class="txt_center">
                                        <input type="text" id="detail_cont_<?php echo $row['mem_no'];?>">
                                    </td>
                                    <td class="txt_center">
                                        <span class="btn btn-primary" onClick ="save('<?php echo $row['mem_no']?>', '<?php echo $row['loc_price']?>');"><?php echo lang('strSave')?></span>
                                    </td>
                               </tr>
                           <?php $no++;}?> 
                       <?php }?>
                   </tbody>
                </table>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
		</div>
    </div>
</div>
        
<script type="text/javascript">

    $(function(){
    	$('input[name="paid_dt"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            format : 'YYYY-MM-DD'
        });
    });
    
    $("#modal1").on('hidden.bs.modal', function () {
        $('#modal_content_add').html("");
        $(this).data('bs.modal', null);
    });

    //페이징 스크립트 시작
    function page_change(row){
        frm=document.tax_bill_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.tax_bill_list;
        frm.action="/calculate/tax_bill_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    
    function detail_list(mem_no){
        url = '/calculate/no_paid_detail_list';
        $.post(url,
                {
            		mem_no : mem_no
                },
                function(data){
                    $('#modal_content_add').append(data);
                    $("#modal1").modal("show");
                }
            );
    }
    

    function save(mem_no, loc_price){
        var paid_amount = $("#detail_amount_"+mem_no).val();
        var paid_cont = $("#detail_cont_"+mem_no).val();
        var no_paid_amount = Number(loc_price) - Number(paid_amount);
        var url = '/calculate/update_receivable_detail';
        $.post(url,
        {
        	mem_no : mem_no,
        	insert_ymd : $("#daterange_"+mem_no).val(),
        	detail_fl : '2',
        	paid_amount : paid_amount,
        	paid_cont : paid_cont,
        	no_paid_amount : no_paid_amount
        },
        function(data){
            console.log(data);
            if(data.trim()=="ok"){
                alert("입력한 입금내용을 저장했습니다.");
                location.reload();
            }
        });
            
    }

    function search(){
        var frm = document.no_paid_list;
        frm.action = '/calculate/no_paid_list';
        frm.submit();
    }
    
    function all_amount_paid(mem_no, loc_price){
        var paid_amount = loc_price;
        var no_paid_amount = Number(loc_price) - Number(paid_amount);
        var url = '/calculate/update_receivable_detail';
        $.post(url,
        {
        	mem_no : mem_no,
        	insert_ymd : '<?php echo date('Y-m-d');?>',
        	detail_fl : '2',
        	paid_amount : paid_amount,
        	no_paid_amount : no_paid_amount
        },
        function(data){
            console.log(data);
            if(data.trim()=="ok"){
                alert("미수금 전액 입금처리 되었습니다.");
                location.reload();
            }
        });
            
    }
            
</script>
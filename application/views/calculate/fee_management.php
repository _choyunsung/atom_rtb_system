<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strCalMgr')?></a></li>
			<li class="active"><?php echo lang('strFeeMgr')?></li>
		</ol>
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strFeeMgr')?></h3>
	</div>
	<div class="panel-body pa_b20">
        <form name="fee_list"  method="post">
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="15%">
                        <col width="35%">
                        <col width="15%">
                        <col width="35%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strDivision')?></th>
                        <td>
                          	<input type="radio" name="role" value="all" <?php if($role == "all"){echo "checked";}?>> <?php echo lang('strAll');?>&nbsp;&nbsp;
                          	<input type="radio" name="role" value="agency" <?php if($role == "agency"){echo "checked";}?>> <?php echo lang('strAgency');?>&nbsp;&nbsp;
                         	<input type="radio" name="role" value="lab" <?php if($role == "lab"){echo "checked";}?>> <?php echo lang('strLab');?>
                        </td>
                        <th><?php echo lang('strSales')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="sales_nm" id="sales_nm" value="<?php echo $sales_nm;?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strCompanyName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_com_nm" id="mem_com_nm" value="<?php echo $mem_com_nm;?>">
                        </td>
                        <th><?php echo lang('strFeeApplyDate')?></th>
                        <td>
            				<input type="text" class="wid_65p" name="fromto_date" id="daterange" value="<?php echo $fromto_date?>" />
            				<span class="iput-group-addon">
            					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
            				</span>
                        </td>
                    </tr>
                </table>
                <div class="float_r ma_b3" id="all_btn_gp">
                    <button class="btn btn-primary ma_l5 float_r" onclick="this.form.submit();"><?php echo lang('strSearch')?></button>
                    <a href="#" id="export_excel" download="" class="btn btn-primary "><?php echo lang('strDownload');?></a>
                </div>
         	</div> 
            <div class="clear">
                <table class="inspection_tb">
                    <colgroup>
                        <col width="10%">
                        <col width="18%">
                        <col width="18%">
                        <col width="18%">
                        <col width="18%">
                        <col width="18%">
                    </colgroup>
                    <thead>
                        <tr>
                        	<th>No.</th>
                        	<th><?php echo lang('strDivision');?></th>
                        	<th><?php echo lang('strCompanyName');?></th>
                            <th><?php echo lang('strFeeApplyDate');?></th>
                            <th><?php echo lang('strFeeRate');?> (%)</th>
                            <th><?php echo lang('strSales');?></th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($fee_list as $fee){
                        ?>
                        <tr>
                            <td class="txt_center pa_l0"><?php echo $fee['mem_no']?></td>
                            <td class="txt_center pa_l0">
                            	<?php 
                            	   if($fee['role'] == "agency"){
                            	       echo lang("strAgency");
                            	   }else if($fee['role'] == "lab"){
                            	       echo lang("strLab");
                            	   }
                            	?>
                            </td>
                            <td class="txt_center pa_l0"><?php echo $fee['mem_com_nm']?></td>
                            <td class="txt_center pa_l0" id="fee_apply_dt_<?php echo $fee['mem_no']?>"><?php echo $fee['fee_apply_dt']?></td>
                            <td class="txt_center pa_l0">
                                <span id="fee_view_<?php echo $fee['mem_no']?>">             
                                    <span id="fee_<?php echo $fee['mem_no']?>"><?php echo $fee['mem_com_fee']?></span>
                                    <div class="btn-group float_r ma_r10p cursor" id="fee_btn_group_<?php echo $fee['mem_no']?>">
                                        <a class="dropdown-caret fa fa-edit cursor add-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="수수료 수정" onclick="fee_modify('<?php echo $fee['mem_no']?>');" ></a>
                                    </div>
                                </span>    
                                <span id="fee_modify_<?php echo $fee['mem_no']?>" style="display:none;">
                                    <input type="text" id="modify_fee_<?php echo $fee['mem_no']?>" name="mem_com_fee" value="<?php echo $fee['mem_com_fee']?>" class="wid_70p">
                                    <span class="fa ma_l20 fa-check color_g cursor" onclick="fee_modify_save('<?php echo $fee['mem_no']?>');"></span>
                                    <span class="fa ma_l3 fa-close color_r cursor" onclick="fee_modify('<?php echo $fee['mem_no']?>');"></span>
                                </span> 
                            </td>
                            <td class="txt_center pa_l0"><?php echo $fee['sales_nm']?></td>
                        </tr>
                        <?php 
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="clear center">
                <div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box" >
                        <li><a href="javascript:page_change(100)">100</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(10)">10</a></li>
                    </ul>
                </div>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div> 
        </form>
   </div>
</div>
<script type="text/javascript">

    function fee_modify_save(mem_no){  
        var fee = $("#modify_fee_"+mem_no).val()
        var url = '/calculate/fee_modify';
        $.post(url,
        {
        	mem_no : mem_no,
            mem_com_fee : $("#modify_fee_"+mem_no).val()
        },
        function(data){
            if(data.trim()=="ok"){
                alert("수수료율이 저장되었습니다.");
                $("#fee_view_"+mem_no).show();
                $("#fee_"+mem_no).html(fee);
                $("#fee_apply_dt_"+mem_no).html("<?php echo date('Y-m-d')?>");
                $("#fee_modify_"+mem_no).hide();
                
            }else{
                alert("Error!");
            }
        }
        );
    }


    function fee_modify(mem_no){
        if($("#fee_view_"+mem_no).css('display') == "none"){
            $("#fee_view_"+mem_no).show();
            $("#fee_modify_"+mem_no).hide();
        }else{
            $("#fee_view_"+mem_no).hide();
            $("#fee_modify_"+mem_no).show();
        }
    }

    //페이징 스크립트 시작
    function page_change(row){
        frm = document.exchange_list;
        frm.per_page.value = row;
        frm.submit();
    }

    function paging(number){
        var frm = document.exchange_list;
        frm.action = "/calculate/fee_management/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
</script>
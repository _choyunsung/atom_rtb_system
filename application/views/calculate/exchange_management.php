<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strCalMgr')?></a></li>
			<li class="active"><?php echo lang('strExchangeMgr')?></li>
		</ol>
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strExchangeMgr')?></h3>
	</div>
	<div class="panel-body pa_b20">
        <!--
	    <div class="clear" id="all_btn_gp" style="margin-bottom:5px;">
	       <a href="javascript:$('#modal1').modal('show');" class="btn btn-primary">계정생성</a>&nbsp;
        </div>
        -->
        <form name="exchange_list"  method="post">
            <div>
                <table class="table table-bordered aut_tb">
                	<colgroup>
                		<col width="20%">
                		<col width="*">
                	</colgroup>
                    <tr>
                        <th>
                           <?php echo lang('strType');?>
                        </th>
                        <td>
                          <input type="radio" name="cond_term" value="" <?php if($cond_term == ""){echo "checked";}?>> <?php echo lang('strDaily');?> <?php echo lang('strAverage');?>
                          <input type="radio" name="cond_term" class="ma_l20" value="month" <?php if($cond_term == "month"){echo "checked";}?>> <?php echo lang('strMonthly');?> <?php echo lang('strAverage');?>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <?php echo lang('strCurrency');?>
                        </th>
                        <td>
                        <!--  <div class="btn-group">
    					<input type="hidden" name="charge_money1" id="charge_money1" >
    					<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel">
    						<span id="charge_money1_view"> &nbsp; USD &nbsp;</span>
    						<i class="dropdown-caret fa fa-caret-down"></i>
    					</button>
    					<ul class="dropdown-menu ul_sel_box">
                            <li><a href="javascript:sel_money(1100000);">1,100,000</a></li>
                            <li><a href="javascript:sel_money(2200000);">2,200,000</a></li>
                            <li><a href="javascript:sel_money(3300000);">3,300,000</a></li>
                            <li><a href="javascript:sel_money(4400000);">4,400,000</a></li>
                            <li><a href="javascript:sel_money(5500000);">5,500,000</a></li>
    					</ul>
    					<span class="color_r"> 셀렉트 박스 적용해야 해요~~</span>
				    </div>-->
                           <select id="cond_from_code_key" name="cond_from_code_key" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                                <option value="USD">USD</option>

                            </select>
                            
                               <button class="btn btn-primary float_r ma_r5" onclick="this.form.submit();"><?php echo lang('strSearch');?></button>
                        </td>
                    </tr>
                </table>
            </div>
            
            
            
            <input type="hidden" name="cre_no" id="cre_no" value="">
            <input type="hidden" name="admin_no" id="admin_no" value="<?php echo $mem_no;?>">
            <input type="hidden" name="mem_no" id="mem_no" value="">
            <input type="hidden" name="cre_evaluation" id="cre_evaluation" value="">
            
            <div class="clear">
                <table class="inspection_tb">
                    <colgroup>
                        <col width="20%">
                        <col width="80%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th><?php echo lang('strDate');?></th>
                            <th><?php echo lang('strAverage');?></th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php
                        
                        
                            foreach ($exchange_list as $key=>$row){
                        ?>
                        <tr>
                            <td class="txt_center pa_l0"><?php echo substr($row['input_dt'], 0, 10)?></td>
                            <td class="txt_center pa_l0"><?php echo number_format(round($row['exchange_rate'], 2), 2)?></td>
                        </tr>
                       <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="clear center">
                <div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box" >
                        <li><a href="javascript:page_change(100)">100</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(10)">10</a></li>
                    </ul>
                </div>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div> 
        </form>
   </div>
</div>
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm = document.exchange_list;
        frm.per_page.value = row;
        frm.submit();
    }

    function paging(number){
        var frm = document.exchange_list;
        frm.action = "/calculate/exchange_management/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
</script>
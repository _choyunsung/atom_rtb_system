<script type="text/javascript">
$(document).ready(function() {
	var check_sel = '<?php echo in_array('all', $cond['publication_st']);?>';
	if (check_sel > 0){
		$("input[name='publication_st[]']").prop('checked',true);
	}

	
});

$("#all").click(function(){      // name 이 check_all 인것이 클릭이 되면
	 
    if ( $("#all").is(":checked") ){      // name이 check_all 인 체크박스가 checked 가 되어 있다면
       
         $("[name='publication_st[]']").attr("checked",true);    // class는 box_class 인 체크박스의 속성 checked는 checked이다
    }
    else{      // 그렇지 않으면
       
         $("[name='publication_st[]']").attr("checked","");      // class는 box_class인 체크박스의 속성 checked 는 "" 공백이다
    }
});
function sel_public_st(type){
	if (type == "all"){
		if ( $("#all").is(":checked") ){
			$("input[name='publication_st[]']").prop('checked',true); 
			  
		}else{
			$("input[name='publication_st[]']").prop('checked','');
		}
	}else{
		if ($("#all").is(":checked")){
			$("#all").prop('checked','');
		}else{
			if ($("#wait").is(":checked") && $("#no").is(":checked") && $("#carry").is(":checked") && $("#done").is(":checked")){
				$("#all").prop('checked',true);	
			}
		}
	}
}

</script>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
    </div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strTaxBillMgr')?> </h3>
    </div>
    <div class="panel-body">
    	<div class="camp_rept">
    		<div class="camp2">
    			<div class="camp_div2b txt_center" >
    				<p class="camp_tit"><?php echo lang('strAll')?></p> 
    				<span class="camp_con"><?php echo $taxbill_summary['all_cnt'];?></span>
    			</div>
    			<div class="camp_div2b txt_center">
    				<p class="camp_tit"><?php echo lang('strWaitPublication')?></p>
    				<span class="camp_con"><?php echo $taxbill_summary['wait_cnt'];?></span>
    			</div>
    			<div class="camp_div2b txt_center">
    				<p class="camp_tit"><?php echo lang('strNoPublication')?></p>
    				<span class="camp_con"><?php echo $taxbill_summary['no_cnt'];?></span>
    			</div>		    				
    			<div class="camp_div2b txt_center" >
    				<p class="camp_tit"><?php echo lang('strCarryOver')?></p>
    				<span class="camp_con"><?php echo $taxbill_summary['carryout_cnt'];?></span>
    			</div>		    				
    			<div class="camp_div2b txt_center" style="border-right:0px">
    				<p class="camp_tit"><?php echo lang('strDonePublication')?></p>
    				<span class="camp_con"><?php echo $taxbill_summary['done_cnt'];?></span>
    			</div>		    					
    		</div>	
    	</div>
	</div>
    <div class="panel-body">
        <form name="tax_bill_list" id="tax_bill_list" method="post">
            <input type="hidden" name="year" id="year" value="<?php echo $cond['year'];?>">
            <input type="hidden" name="month" id="month" value="<?php echo $cond['month'];?>">
            <input type="hidden" name="mem_no" id="mem_no">
            <input type="hidden" name="type" id="type">
            <input type="hidden" name="date_ym" id="date_ym">
            
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="15%">
                        <col width="35%">
                        <col width="15%">
                        <col width="35%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strType')?></th>
                        <td colspan="3">
                            <input type="checkbox" name="adver_yn" <?php if ($cond['adver_yn'] == "on"){echo "checked";}?>> <?php echo lang('strAdvertiser')?>&nbsp;&nbsp;
                            <input type="checkbox" name="agency_yn" <?php if ($cond['agency_yn'] == "on"){echo "checked";}?>> <?php echo lang('strAgency')?>&nbsp;&nbsp;
                            <input type="checkbox" name="rep_yn" <?php if ($cond['rep_yn'] == "on"){echo "checked";}?>> <?php echo lang('strLab')?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strPublicationStatus')?></th>
                        <td colspan="3">
                            <input type="checkbox" name="publication_st[]" value="all" id="all" onchange="sel_public_st('all');" <?php if (in_array('all', $cond['publication_st']) > 0){echo "checked";}?>> <?php echo lang('strAll')?>&nbsp;&nbsp;
                            <input type="checkbox" name="publication_st[]" value="1" id="no" onchange="sel_public_st('no');" <?php if (in_array('1', $cond['publication_st']) > 0){echo "checked";}?>> <?php echo lang('strWaitPublication')?>&nbsp;&nbsp;
                            <input type="checkbox" name="publication_st[]" value="2" id="wait" onchange="sel_public_st('wait');" <?php if (in_array('2', $cond['publication_st']) > 0){echo "checked";}?>> <?php echo lang('strNoPublication')?>&nbsp;&nbsp;
                            <input type="checkbox" name="publication_st[]" value="3" id="carry" onchange="sel_public_st('carry');" <?php if (in_array('3', $cond['publication_st']) > 0){echo "checked";}?>> <?php echo lang('strCarryOver')?>&nbsp;&nbsp;
                            <input type="checkbox" name="publication_st[]" value="4" id="done" onchange="sel_public_st('done');" <?php if (in_array('4', $cond['publication_st']) > 0){echo "checked";}?>> <?php echo lang('strDonePublication')?>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strCompanyName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_com_nm" value="<?php echo $cond['mem_com_nm'];?>">
                        </td>
                        <th><?php echo lang('strPublishDate')?></th>
                        <td>
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    &nbsp;
                                        <span id="year_view"><?php if ($cond['year'] == ""){echo lang('strYear').lang('strSelect');}else{echo $cond['year']." 년";}?></span>  
                                    &nbsp;
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php for($i=2015; $i<2200; $i++){?>
                                        <li>
                                            <a href="#" onclick="change_year('<?php echo $i;?>')">
                                                <?php echo $i;?> <?php echo lang('strYear');?>
                                            </a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                            <div class="btn-group">
                                &nbsp;
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    &nbsp;
                                        <span id="month_view"><?php if ($cond['month'] == ""){echo lang('strMonth').lang('strSelect');}else{echo $cond['month']." 월";}?></span>  
                                    &nbsp;
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php for($j=1; $j<13; $j++){?>
                                        <li>
                                            <a href="#" onclick="change_month('<?php echo $j;?>')">
                                                <?php echo $j;?> <?php echo lang('strMonth');?>
                                            </a>
                                        </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="float_r ma_b3" id="all_btn_gp">
                    <span class="btn btn-primary float_r" onClick ="search();"><?php echo lang('strSearch')?></span>
                </div>
         	</div>
            <div class="pa_b20">
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
    			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th rowspan="2">No</th>
                            <th rowspan="2"><?php echo lang('strCalculateDate')?></th>
                            <th rowspan="2"><?php echo lang('strPublicationStatus')?></th>
                            <th colspan="3"><?php echo lang('strCompanyInfo')?></th>
                            <th colspan="2"><?php echo lang('strAmountInfo')?></th>
                            <th rowspan="2"><?php echo lang('strCarryOver')?></th>
                            <th rowspan="2"><?php echo lang('strManager')?></th>
                        </tr>
                        <tr>
                            <th><?php echo lang('strCompanyName')?></th>
                            <th><?php echo lang('strType')?></th>
                            <th><?php echo lang('strBusinessNo')?></th>
                            <th><?php echo lang('strPublishPrice')?></th>
                            <th><?php echo lang('strTaxPrice')?></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($taxbill_list)){ ?>
                            <?php
                                $no = 1;
                                foreach ($taxbill_list as $row){
                            ?>
                                <tr>
                                    <td class="txt_center"><?php echo $row['member_no']?></td>
                                    <td class="txt_center">
                                        <?php echo date('Y')."년 ".date('m')."월";?>
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['publish_fl'] == '1'){
                                                echo lang('strNoPublication');
                                            }elseif ($row['publish_fl'] == '2'){
                                                echo lang('strWaitPublication');
                                            }elseif ($row['publish_fl'] == '3'){
                                                echo lang('strCarryOver');
                                            }elseif ($row['publish_fl'] == '4'){
                                                echo lang('strDonePublication');
                                            }else{
                                                echo lang('strNoPublication');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <a href="javascript:change_publish_status('<?php echo $row['member_no']?>', '<?php echo date('Y')."-".date('m');?>')">
                                            <?php if ($row['mem_com_nm'] != ""){?>
                                                <?php echo $row['mem_com_nm']?>
                                            <?php }else{?>
                                                <?php echo $row['mem_id']?>
                                            <?php }?>
                                        </a>
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['role'] == "adver"){
                                                echo lang('strAdvertiser');
                                            }elseif ($row['role'] == "agency"){
                                                echo lang('strAgency');
                                            }elseif ($row['role'] == "ind"){
                                                echo lang('strIndividual');
                                            }elseif ($row['role'] == "lab"){
                                                echo lang('strLab');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['mem_com_no']?>
                                    </td>
                                    <td class="txt_right">
                                        <?php echo number_format($row['loc_price'])?>
                                    </td>
                                    <td class="txt_right">
                                        <?php echo number_format($row['tax_loc_price'])?>
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['forward_fl'] == "Y"){    
                                                echo lang('strCarryOver');
                                            }else{
                                                echo "-";
                                            }

                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['publish_mem_nm']?>
                                    </td>
                               </tr>
                           <?php $no++;}?> 
                       <?php }?>
                   </tbody>
                </table>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
		</div>
    </div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.tax_bill_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.tax_bill_list;
        frm.action="/calculate/tax_bill_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    
    function change_year(year){
        $("#year_view").html(year);
        $("#year").val(year);
    }

    function change_month(month){
        $("#month_view").html(month);
        $("#month").val(month);
    }
    
    function search(){
    	var frm = document.tax_bill_list;
    	frm.action = '/calculate/tax_bill_list';
    	frm.submit();
    }

    function change_publish_status(mem_no, date_ym){
        var frm = document.tax_bill_list;
        frm.mem_no.value = mem_no;
        frm.date_ym.value = date_ym;
        frm.action = "/calculate/tax_bill_view?mem_no="+mem_no+"&date_ym="+date_ym;
        
        frm.submit();        
    }

    function select_status(type){
        var frm = document.refund_request_list;
        frm.mem_id.value = "";
        frm.mem_com_nm.value = "";
        frm.fromto_date.value = "";
        $("#manager_yn").prop("checked", false);
        $("#master_yn").prop("checked", false);
        $("#answer_done").prop("checked", false);
        
        if (type == "all"){
            location.replace('/cash/refund_request_list');
        }else if (type == "today"){
            $("#daterange").val('<?php echo date('Y-m-d') ?>'+ ' ~ ' + '<?php echo date('Y-m-d')?>');
        }else if (type == "soon"){
            $("#cond_refund_st").val("1");
        }else if (type == "reject"){
        	$("#cond_refund_st").val("2");
        }else if (type == "done"){
        	$("#cond_refund_st").val("3");
        }
        search();
    }
            
</script>
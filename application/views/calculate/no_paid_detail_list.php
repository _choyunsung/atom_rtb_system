<div class="modal-header">
    <h4 class="h4_blit ma_t5 ma_b5"><?php echo lang('strNoPaidMgr')?></h4>
</div>
<div class="modal-body">

    <div class="ma_b10 pa_l10">
        <span class="sub_tit"><?php echo lang('strNoPaidSummary')?></span><br />
        <table class="aut_tb">
            <colgroup>
                <col width="33%">
                <col width="33%">
                <col width="33%">
            </colgroup>
            <thead>
                <tr>
                    <th><?php echo lang('strRevenue');?></th>
                    <th><?php echo lang('strPaidAmount');?></th>
                    <th><?php echo lang('strNoPaidAmount')?></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="txt_right"><?php echo number_format($loc_price_lt)?></td>
                    <td class="txt_right"><?php echo number_format($deposit)?></td>
                    <td class="txt_right"><?php echo number_format($loc_price_lt - $deposit)?></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="search_id">
        <table class="aut_tb" cellpadding="0" cellspacing="0" border="0" class="t_center">
            <thead>
                <tr>
                    <th><?php echo lang('strCompanyName')?></th>
                    <th>ID</th>
                    <th><?php echo lang('strType')?></th>   
                    <th><?php echo lang('strPaidDate')?></th>
                    <th><?php echo lang('strPaidAmount');?></th>
                    <th>비고</th>
                </tr>
            </thead>
            <?php if (isset ($nopaid_list)){?>
                <?php foreach ($nopaid_list as $row){?>
                    <tr>
                        <td><?php echo $row['mem_com_nm']?></td>
                        <td><?php echo $row['mem_id']?></td>
                        <td>
                            <?php 
                                if ($row['role'] == "adver"){
                                    echo lang('strAdvertiser');
                                }elseif ($row['role'] == "agency"){
                                    echo lang('strAgency');
                                }elseif ($row['role'] == "ind"){
                                    echo lang('strIndividual');
                                }elseif ($row['role'] == "lab"){
                                    echo lang('strLab');
                                }
                            ?>
            
                        </td>   
                        <td class="txt_center">
                            <?php if ($row['detail_fl'] == "2"){?>
                                <?php echo $row['insert_ymd'];?>
                            <?php }?>
                        </td>
                        <td class="txt_right">
                            <?php if ($row['detail_fl'] == "2"){?>
                                <?php echo number_format($row['detail_amount']);?>
                            <?php }?>
                        </td>     
                        <td class="txt_center">
                            <?php echo $row['cont']?>
                        </td>
                    </tr>
                <?php }?>
            <?php }?>
        </table>	
    </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t10">
    <span data-dismiss="modal" onclick="modal_clear();" class="btn btn-primary"><?php echo lang('strClose')?></span>
</div>
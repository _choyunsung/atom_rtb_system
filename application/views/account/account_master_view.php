<div class="panel float_r wid_970 min_height">
    <div class="history_box">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strPersnalInfoMgr')?></li>
        </ol>
    </div>	
    <div class="panel-body">
        <div class="tab-base">
        <!--Nav Tabs-->
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#demo-lft-tab-1" aria-expanded="true"><?php echo lang('strMasterInfomation');?></a>
                </li>
                <?php if ($this->session->userdata('mem_gb') != 'manager'){?>
                    <li>
                        <a href="/account/account_manager_list"><?php echo lang('strManagerList');?></a>
                    </li>
                <?php }?>
                <li>
                    <a href="/account/account_login_list"><?php echo lang('strLoginHistory');?></a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="demo-lft-tab-1" class="tab-pane fade active in">
                    <div class="panel-heading over_hidden">
                        <h3 class="page-header text-overflow wid_30p ma_t8 float_l tit_blit0"><?php echo lang('strCompanyInfo');?></h3>
                      
                        <p class="txt-right wid_30p float_r">
                          <a class="btn btn-primary" href="/account/account_master_modify"><?php echo lang('strMasterInfomation');?><?php echo lang('strModify');?></a>
                        </p>
                    </div>
                    <table class="aut_tb" cellpaddig="0" cellspacing="0">
                        <colgroup>
                            <col width="20%">
                            <col width="30%">
                            <col width="20%">
                            <col width="30%">
                        </colgroup>
                        <tr>
                            <th><?php echo lang('strID');?></th>
                            <td><?php echo $master_info['mem_id']?></td>
                            <th class="bd_l1"><?php echo lang('strPassword');?></th>
                            <td>****</td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strBusinessName');?></th>
                            <td><?php echo $master_info['mem_com_nm']?></td>
                            <th class="bd_l1"><?php echo lang('strCompany');?></th>
                            <td><?php echo $master_info['mem_com_nm']?></td>
                        </tr>														
                        <tr>
                            <th><?php echo lang('strBusinessNo');?></th>
                            <td colspan="3"><?php echo $master_info['mem_com_no']?></td>
                        </tr>													
                        <tr>
                            <th>대표자명</th>
                            <td colspan="3"><?php echo $master_info['mem_com_ceo']?></td>
                        </tr>
                        <tr>
                            <th>업태 / 종목</th>
                            <td colspan="3"><?php echo $master_info['mem_com_type']?> / <?php echo $master_info['mem_com_item']?></td>
                        </tr>																										
                        <tr>
                            <th><?php echo lang('strAddress');?></th>
                            <td colspan="3"><?php echo $master_info['mem_addr']?></td>
                        </tr>													
                    </table>
                       <div class="panel-heading">
                        <h3 class="page-header text-overflow tit_blit0">세금계산서 담당자 정보</h3>
                    </div>
                    <table class="aut_tb" cellpaddig="0" cellspacing="0">
                        <colgroup>
                            <col width="20%">
                            <col width="*">
                        </colgroup>
                        <tr>
                            <th>담당자 이름</th>
                            <td><?php echo $master_info['tax_mem_nm']?></td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strEmail');?></th>
                            <td><?php echo $master_info['tax_mem_email']?></td>
                        </tr>														
                        <tr>
                            <th><?php echo lang('strOfficeNumber');?></th>
                            <td><?php echo $master_info['tax_mem_tel']?></td>
                        </tr>													
                        <tr>
                            <th><?php echo lang('strMobileNumber');?></th>
                            <td><?php echo $master_info['tax_mem_cell']?></td>
                        </tr>
                    </table>												
                    <div class="panel-heading">
                        <h3 class="page-header text-overflow tit_blit0">알림 설정</h3>
                    </div>
                    <table class="aut_tb" cellpaddig="0" cellspacing="0">
                        <colgroup>
                            <col width="20%">
                            <col width="*">
                        </colgroup>
                        <!--
                        <tr>
                            <th>정보성 SMS</th>
                            <td class="point_dgray">결제알림 <i class="fa fa-bell"></i></td>
                            <td>
                                <input type="checkbox" <?php if($master_info['sms_alarm_c10_fl'] == "Y"){echo "checked";}?> disabled> 캠페인 일예산 10% 미만 시
                                <input type="checkbox" class="ma_l20" <?php if($master_info['sms_alarm_d10_fl'] == "Y"){echo "checked";}?> disabled> <?php if($master_info['fee_fl'] == "Y"){ echo "100,000";}else{echo "10,000";}?>캐쉬 미만 시
                                <input type="checkbox" class="ma_l20" <?php if($master_info['sms_alarm_c0_fl'] == "Y"){echo "checked";}?> disabled> 캐쉬 모두 소진 시
                            </td>
                        </tr>
                        -->
                        <tr>
                            <th>정보성 이메일</th>
                            <td class="point_dgray">결제알림 <i class="fa fa-bell"></i></td>															
                            <td>
                                <input type="checkbox" <?php if($master_info['email_alarm_c10_fl'] == "Y"){echo "checked";}?> disabled> 캠페인 일예산 10% 미만 시
                                <input type="checkbox" class="ma_l20" <?php if($master_info['email_alarm_d10_fl'] == "Y"){echo "checked";}?> disabled> <?php if($master_info['fee_fl'] == "Y"){ echo "100,000";}else{echo "10,000";}?>캐쉬 미만 시
                                <input type="checkbox" class="ma_l20" <?php if($master_info['email_alarm_c0_fl'] == "Y"){echo "checked";}?> disabled> 캐쉬 모두 소진 시
                            </td>
                        </tr>														
                    </table>													
                    
                    <div class="panel-heading">
                        <h3 class="page-header text-overflow tit_blit0"><?php echo lang('strRefundBankInfo');?></h3>
                    </div>															
                    <table class="aut_tb" cellpaddig="0"  cellspacing="0">
                        <colgroup>
                            <col width="20%">
                            <col width="*">
                        </colgroup>
                        <?php if($master_info['bank_no']==""){?>
                            <tr>
                                <th><?php echo lang('strRefundBankInfo');?></th>
                                <td class="join_info">수정 버튼을 클릭하여, 환불계좌 정보를 입력해 주세요.</td>
                            </tr>
                        <?php }else{?>
                            <tr>
                                <th><?php echo lang('strBank');?></th>
                                <td><?php echo $master_info['bank_nm']?></td>
                            </tr>
                            <tr>
                                <th><?php echo lang('strBankNumber');?></th>
                                <td><?php echo $master_info['bank_no']?></td>
                            </tr>
                            <tr>
                                <th><?php echo lang('strBankName');?></th>
                                <td><?php echo $master_info['bank_mem_nm']?></td>
                            </tr>
                        <?php }?>
                    </table>									
<!--                     <p class="pa_b20 ma_l10"> -->
<!--                         해당 계정의 해지를 원하시면, 문의하기를 통해 문의해주시기 바랍니다. <span class="point_blue bold">문의하기 페이지로 이동</span>  -->
<!--                     </p> -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 매니저 생성 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content_add">
            <div class="modal-header">
                <h4 class="h4_blit  ma_t5 ma_b5"><?php echo lang("strManagerAdd");?></h4>
            </div>
            <div class="modal-body">
             <div class="ma_b5 ma_l5">
                <span class="sub_tit"><?php echo lang("strManagerAddInfoEntry");?></span><br />
            </div>
                <div style="clear:both;">
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb">
                        <colgroup>
                            <col width="20%">
                            <col width="80%">
                        </colgroup>
                        <tr>
                            <th class="point_blue"><?php echo lang('strID')?></th>
                            <td>
                                <input type="text" class="wid_45p float_l" maxlength="20" id="manager_mem_id" name="mem_id" style="ime-mode: disabled;" onkeyup="id_check()">
                                <input type="hidden" id="idcheck_commit" class="float_l" name="idcheck_commit" value="N" validate="null,아이디"> 
                                <div id="idcheck_result" class="float_l ma_t5 ma_l5">
                                    <font class="join_info">* <?php echo lang("strJoinBusiness");?></font>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="point_blue"><?php echo lang('strPassword')?></th>
                            <td><input type="password" class="wid_45p" id="manager_mem_pwd"></td>
                        </tr>
                        <tr>
                            <th class="point_blue"><?php echo lang('strMemo')?></th>
                            <td><input type="text" class="wid_45p" id="manager_mem_cont"></td>
                        </tr>	
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span onclick="manager_add();" id="manager_add_btn" class="btn btn-primary"><?php echo lang('strSave')?></span>
                <span data-dismiss="modal" onclick="$('#modal1').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<!-- 매니저 생성 modal-->
<!-- 매니저정보 Modal -->
<div class="modal" id="modal2" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content_view">
            <div class="modal-header">
                <h4 class="h4_blit ma_b5 ma_t5"><?php echo lang("strManagerInfo");?></h4>
            </div>
            <div class="pa_l10">
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb">
                        <colgroup>
                            <col width="20%">
                            <col width="80%">
                        </colgroup>
                        <tr>
                            <th><?php echo lang('strID')?></th>
                            <td>
                                <input type="hidden" id="manager_id_view">
                                <span id="mem_id_view"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strPassword')?></th>
                            <td><span>****</span></td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strMemo')?></th>
                            <td><span id="mem_cont_view"></span></td>
                        </tr>	
                        <tr>
                            <th><?php echo lang('strManagerName')?></th>
                            <td><span id="mem_mamger_view"></span></td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strEmail')?></th>
                            <td><span id="mem_email_view"></span></td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strOfficeNumber')?></th>
                            <td><span id="mem_office_view"></span></td>
                        </tr>
                        <tr>
                            <th><?php echo lang('strCellPhone')?></th>
                            <td><span id="mem_cell_view"></span></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span data-dismiss="modal" onclick="manager_modify($('#manager_id_view').val());" class="btn btn-primary"><?php echo lang('strModify')?></span>
                <span data-dismiss="modal" onclick="$('#modal2').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strPersnalInfoMgr')?></li>
        </ol>
    </div>	
    <div class="panel-body">
        <div class="tab-base">
            <ul class="nav nav-tabs">
                <li>
                    <a href="/account/account_master_view"><?php echo lang("strMasterInfomation");?></a>
                </li>
                <li class="active">
                    <a href="#"><?php echo lang("strManagerList");?></a>
                </li>
                <li>
                    <a href="/account/account_login_list"><?php echo lang("strLoginHistory");?></a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="demo-lft-tab-1" class="tab-pane fade active in">
                    <div class="panel-heading">
	    				<h3 class="page-header tit_blit0"><?php echo lang("strManagerList");?></h3>
	       			</div>
					<div class="panel_body ma_b5 hei_35">
				        <?php if(count($manager_list)<10){?>
						  <span class="btn btn-primary float_l" onclick="$('#modal1').modal('show');"><?php echo lang("strManagerAdd");?></span>
						<?php }?>
						
						<div class="btn-group ma_l5 float_l">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                &nbsp;  <?php echo lang('strSelect')?> 
                                <i class="dropdown-caret fa fa-caret-down"></i>
                            </button>
                          
                            <ul class="dropdown-menu ul_sel_box_pa">
                                <?php foreach($sel_manager_status as $sel){?>
                                    <li>
                                        <a href="#" onclick="manager_status_change('<?php echo $sel['code_key'];?>','all')">
                                            <font class="bold" color="<?php if($sel['code_key']=="1"){echo "#69ae30";}elseif($sel['code_key']=="2"){echo "#c85757";}?>">
                                                <?php echo lang($sel['code_desc']);?>
                                            </font>
                                        </a>
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
                        <span class="btn btn-primary float_r" onClick ="excel_down();"><?php echo lang('strDownload')?></span>
					</div>
					<form name="manager_list" method="post"> 
					    <input type="hidden" name="status_key">
					    <input type="hidden" name="mem_no">
					    <input type="hidden" name="master_no" value="<?php echo $mem_no?>">
    					<table class="mng_98_tb" id="list" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="5%">
    							<col width="5%">
    							<col width="25%">
    							<col width="15%">
    							<col width="*">
    							<col width="15%">
    						</colgroup>
    						<tr>
    							<th><input type="checkbox" id="allCheck" onclick="all_check()"></th>
    							<th><?php echo lang("strNum");?></th>
    							<th><?php echo lang("strID");?></th>
    							<th><?php echo lang("strStatus");?></th>
    							<th><?php echo lang("strMemo");?></th>
    							<th class="bd_r_0"><?php echo lang("strRegisteredDate");?></th>																															
    						</tr>
    						<?php if(isset($manager_list)){?>
    						    <?php $no = 1;?>
        						<?php foreach ($manager_list as $row){?>
            						<tr>
            							<td class="txt_center"><input type="checkbox" name="sel_manager_no[]" id="sel_manager_no" value="<?php echo $row['mem_no']?>"></td>
            							<td class="txt_center"><?php echo $no;?></td>
            							<td>
            							    <a href="javascript:manager_detail_modal('<?php echo $row['mem_no'];?>', '<?php echo $row['mem_id'];?>', '<?php echo $row['mem_cont'];?>', '<?php echo $row['mem_nm'];?>', '<?php echo $row['mem_email'];?>', '<?php echo $row['mem_tel'];?>', '<?php echo $row['mem_cell'];?>');"<span><?php echo $row['mem_id']?></span></a>
            								<i class="fa fa-gear float_r ma_t5 cursor" onclick="manager_modify(<?php echo $row['mem_no']?>);"></i>
            							</td>
            							<td class="txt_center">
            							     <font class="bold" color="<?php if($row['mem_active_st']=="1"){echo "#69ae30";}elseif($row['mem_active_st']=="2"){echo "#c85757";}?>" >
							                     <?php echo lang($row['manager_st'])?>
							                 </font>
						                </td>
            							<td><?php echo $row['mem_cont']?></td>
            							<td class="txt_center bd_r_0"><?php echo substr($row['mem_ymd'],0,10)?></td>																															
            						</tr>	
            						<?php $no++;?>													
        						<?php }?>
        					<?php }?>
    					</table>
					</form>												
					<p class="ma_l10 pa_b20">
					<?php echo lang('strMaxConunt10');?><span><?php echo count($manager_list);?></span><?php echo lang('strUserAdd');?>
					</p>
				</div>
				<form name="manager_modify_from" method="post">
                    <input type="hidden" id="manager_no" name="manager_no">
                    <input type="hidden" id="master_no" name="master_no">
				</form>
            </div>
        </div>
    </div>
</div>
<script>
function all_check(){
    if($("#allCheck").prop("checked")) { 
       $("input[id=sel_manager_no]").prop("checked", true);
      } else {
       $("input[id=sel_manager_no]").prop("checked", false);  
      }
}

function id_check(){
    if (!(event.keyCode >=37 && event.keyCode<=40)) {
        var inputVal = $("#manager_mem_id").val();
        $("#manager_mem_id").val(inputVal.replace(/[^a-z0-9]/gi,''));
    }

    $.ajax({
        type:"POST",
        url:"/member/id_check/",
        data : {mem_id: $("#manager_mem_id").val()},
        timeout : 30000,
        async:false,
        cache : false,
        success: function (data){
            $("#idcheck_commit").val("N");
            switch(data.trim()){
                case "true":
                    var show_args="<font color='blue'><?php echo lang('strUseID');?></font>";
                    $("#idcheck_commit").val("Y");
                    break;
                case "false":
                	var show_args="<font color='red'><?php echo lang('strOverID');?></font>";
                    break;
                case "none":
                	var show_args="<font color='red'><?php echo lang('strIDEntry');?></font>";
                    break;
                case "short":
                	var show_args="<font color='red'><?php echo lang('strConunt4');?></font>";
                    break;
                case "long":
                    var show_args="<font color='red'><?php echo lang('strConunt15');?></font>";
                    $("#manager_mem_id").val($("#manager_mem_id").val().substring(0,15));
                    break;
            }
            $('#idcheck_result').html(show_args);
        },
        error: function whenError(e){
            alert("code : " + e.status + "\r\nmessage : " + e.responseText);
        }
    });
}

function manager_add(){
	mem_id = $("#manager_mem_id").val();
	mem_pwd = $("#manager_mem_pwd").val();
	mem_cont = $("#manager_mem_cont").val();
	mem_type = "manager";
	master_mem_no = '<?php echo $mem_no;?>';
    if (mem_id == ""){
        alert('<?php echo lang('strIDEntry');?>');
    }else if (mem_pwd == ""){
    	alert('<?php echo lang('strPWEntry');?>');
    }else if (mem_cont == ""){
    	alert('<?php echo lang('strMemoEntry');?>');
    }else if ($("#idcheck_commit").val() != "Y"){
        alert("<?php echo lang('strIDCheck');?>");
    }else if (mem_pwd.length < 6){
        alert("<?php echo lang('strPWCount');?>");
    }else{ 
    	$("#manager_add_btn").hide();
        url = '/account/manager_add';
        $.post(url,
            {
        	mem_id : mem_id,
            mem_pwd : mem_pwd,
            mem_cont : mem_cont,
            master_mem_no : master_mem_no,
            mem_type : mem_type
            },
            function(data){
                console.log(data);
                if(data.trim()=="ok"){
                	alert("<?php echo lang("strManagerAdd2");?>");
                    location.reload();
                }
            }
        );
    }
}

function manager_detail_modal(mem_no, mem_id, mem_cont, mem_nm, mem_email, mem_tel, mem_cell){
	$('#manager_id_view').attr('value',mem_no);
	$('#mem_id_view').html(mem_id);
	$('#mem_cont_view').html(mem_cont);
	$('#mem_nm_view').html(mem_nm);
	$('#mem_email_view').html(mem_email);
	$('#mem_tel_view').html(mem_tel);
	$('#mem_cell_view').html(mem_cell);
	
	$('#modal2').modal('show');
}

function manager_modify(mem_no){
	var frm=document.manager_modify_from;
	frm.action="/account/account_manager_modify";
	frm.manager_no.value=mem_no;
	frm.master_no.value=<?php echo $mem_no?>;
	frm.submit();
}

function manager_status_change(status_key,mem_no){
	if($(":checkbox[name='sel_manager_no[]']:checked").length<1&&mem_no=="all"){
		alert("<?php echo lang("strManagerSelect");?>");
	}else{
		if(status_key=="del"){
			if(confirm("<?php echo lang("strManagerDelete");?>")){
				var frm=document.manager_list;
            	frm.action="/account/sel_manager_delete";
            	frm.status_key.value=status_key;
            	frm.mem_no.value=mem_no;
            	frm.submit();
			}
		}else{
    	    if(confirm("<?php echo lang("strManagerStatusChange");?>")){
            	var frm=document.manager_list;
            	frm.action="/account/sel_manager_status_change";
            	frm.status_key.value=status_key;
            	frm.mem_no.value=mem_no;
            	frm.submit();
    		}else{
    			alert("<?php echo lang("strStatusChangeAlert2");?>");
    		}
		}
	}
}

function excel_down(){
	var cnt = '<?php echo count($manager_list); ?>';
	if (cnt > 0){
		var frm = document.manager_list;
	    frm.action = '/excel/manager_excel_down';
	    frm.submit();
    }else{
    	alert("<?php echo lang("strExcelDwon");?>");
    }
}

$(document).ready(function() {
//     $('#list').dataTable( {
//         "dom": '<"toolbar"f>t'
//     });
});

		
</script>
<!-- 비번변경 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog modal-md">
        <div class="modal-content" id="modal_content">
           <!-- 
           <div class="modal-header">
                <h4 class="h4_blit">광고주 생성</h4>
            </div>
            <div class="pa_l20 ma_t8 ">
                <span class="sub_tit">광고주 정보 입력</span><br />
            </div>
             --> 
            <div class="modal-header">
                <h4 class="h4_blit">광고주 생성 <span class="sub_tit"> / 광고주 정보 입력</span></h4>
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb">
                        <colgroup>
                            <col width="30%">
                            <col width="70%">
                        </colgroup>
                        <tr>
                            <th class="point_blue">광고주명</th>
                            <td>
                                <input type="text" class="wid_80p" maxlength="20" name="mem_com_nm" id="mem_com_nm" >
                            </td>
                        </tr>
                        <tr>
                            <th class="point_blue">URL</th>
                            <td><input type="text" class="wid_80p" id="mem_com_url"></td>
                        </tr>
                        <tr>
                            <th class="point_blue">담당자 이름</th>
                            <td><input type="text" class="wid_80p" id="mem_nm"></td>
                        </tr>	
                        <tr>
                            <th class="point_blue">이메일</th>
                            <td><input type="text" class="wid_80p" id="mem_email"></td>
                        </tr>	
                        <tr>
                            <th class="point_blue">사무실번호</th>
                            <td><input type="text" class="wid_80p" id="mem_tel"></td>
                        </tr>	
                        <tr>
                            <th class="point_blue">휴대폰번호</th>
                            <td><input type="text" class="wid_80p" id="mem_cell"></td>
                        </tr>	
                        <tr>
                            <th class="point_blue">사업자번호</th>
                            <td><input type="text" class="wid_80p" id="mem_com_no"></td>
                        </tr>	
                        <tr>
                            <th class="point_blue">주소</th>
                            <td><input type="text" class="wid_80p" id="mem_addr"></td>
                        </tr>	
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span onclick="new_advertiser_save();" class="btn btn-primary"><?php echo lang('strSave')?></span>
                <span data-dismiss="modal" onclick="$('#modal1').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<!-- 비번변경 modal2-->
<!-- 매니저정보 Modal -->
<div class="modal" id="modal2" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <h4 class="h4_blit">매니저 계정 상세정보</h4>
            </div>
            <div class="pa_l10">
                <span class="sub_tit">매니저 계정 정보</span><br />
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray">
                        <colgroup>
                            <col width="30%">
                            <col width="70%">
                        </colgroup>
                        <tr>
                            <th class="point_blue"><?php echo lang('strID')?></th>
                            <td>
                                <input type="hidden" id="manager_id_view">
                                <span id="mem_id_view"></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="point_blue"><?php echo lang('strPassword')?></th>
                            <td><span>****</span></td>
                        </tr>
                        <tr>
                            <th class="point_blue"><?php echo lang('strMemo')?></th>
                            <td><span id="mem_cont_view"></span></td>
                        </tr>	
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t10 ma">
                <span onclick="manager_modify();" class="btn btn-primary"><?php echo lang('strModify')?></span>
                <span data-dismiss="modal" onclick="$('#modal2').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
			<li class="active"><?php echo lang('strAdvertiserMgr')?></li>
		</ol>
	</div>	
   	<div class="panel-body ma_t20"> 
        <div class="panel-heading over_hidden">
            <h3 class="page-header text-overflow wid_30p ma_t8 float_l tit_blit0"><?php echo lang('strAdvertiser')?> <?php echo lang('strList')?></h3>
            <p class="txt-right wid_30p float_r">
              <a class="btn btn-primary" onclick="$('#modal1').modal('show');">광고주 추가</a>
            </p>
        </div>
        <div>
            <form name="advertiser_list"  method="post">
                <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="50" cellpadding="0" cellspacing="0" border="0">
                    <colgroup>
                        <col width="8%">
                        <col width="25%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="12%">
                        <col width="13%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr>                                                                                                                                    
                            <th>번호</th>
                            <th><?php echo lang('strAdvertiser')?> <?php echo lang('strName')?></th>
                            <th><?php echo lang('strStatus')?></th>
                            <th>URL</th>
                            <th>담당자명</th>
                            <th>Email</th>
                            <th>사무실번호</th>
                            <th>등록일</th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no =0; 
                            foreach ($adver_list as $row){
                            $no++;
                        ?>
                            <tr>
                                <td class="txt_center pa_l0"><?php echo $no?></td>
                                <td> 
                                    <?php echo $row['mem_com_nm']?>
                                    <i class="fa fa-gear float_r ma_r20 ma_t3 cursor add-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="광고주 수정" onclick="advertiser_modify(<?php echo $row['mem_no']?>);"></i>
                                </td>
                                <td class="center"><?php if ($row['mem_fl'] == 'N'){echo "<font class='bold run'>".lang('strRun')."<font>";}else{echo "<font class='bold pause'>".lang('strPause')."<font>";}?></td>
                                <td><?php echo $row['mem_com_url']?></td>
                                <td><?php echo $row['mem_nm']?></td>
                                <td><?php echo $row['mem_email']?></td>
                                <td><?php echo $row['mem_tel']?></td>
                                <td class="center" ><?php echo substr($row['mem_ymd'],0,10)?></td>
                           </tr>
                       <?php }?> 
                    </tbody>
                </table>
                <p class="ma_l10 pa_b20">
    		        현재 최대 1,000개 중 <span><?php echo count($adver_list);?></span>개의 광고주가 생성 되었습니다.
    		    </p>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
       </div>
    </div>	
</div>	
          
<form name="advertiser_modify_from" method="post">
    <input type="hidden" id="adver_no" name="adver_no">
    <input type="hidden" id="master_no" name="master_no">
</form>

<script>
//페이징 스크립트 시작
function page_change(row){
	frm=document.advertiser_list;
	frm.per_page.value=row;
	frm.submit();
}

function paging(number){
    var frm = document.advertiser_list;
    frm.action="/account/account_advertiser_list/" + number;
    frm.submit();
}
//페이징 스크립트 끝

function manager_detail_modal(mem_no,mem_id, mem_cont){
	$('#manager_id_view').attr('value',mem_no);
	$('#mem_id_view').append(mem_id);
	$('#mem_cont_view').append(mem_cont);
	$('#modal2').modal('show');
}

function new_advertiser_save(){
	if($("#mem_com_nm").val()==""){
		alert("광고주명을 입력해 주세요");
		$("#mem_com_nm").focus();
	}else{
        var url = '/account/new_advertiser_save';
        $.post(url,
            {
                mem_no: $("#mem_no").val(),
                mem_com_nm : $("#mem_com_nm").val(),
                mem_nm : $("#mem_nm").val(),
                mem_email : $("#mem_email").val(),
                mem_tel : $("#mem_tel").val(),
                mem_com_url : $("#mem_com_url").val(),
                mem_cell : $("#mem_cell").val(),
                mem_com_no : $("#mem_com_no").val(),
                mem_addr : $("#mem_addr").val()
            },
            function(data){
                console.log(data);
                if(data.trim()=="ok"){
                    alert("광고주 등록완료");
                    modal_clear();
                    $("#modal1").hide();
                    location.reload();
                }else{
                    alert("광고주 등록실패");

                }
            }
        );
	}
}

function advertiser_modify(mem_no){
	var frm=document.advertiser_modify_from;
	frm.action="/account/account_advertiser_modify";
	frm.adver_no.value=mem_no;
	frm.master_no.value=<?php echo $mem_no?>;
	frm.submit();
}
$(document).ready(function() {
// 	$('#list').dataTable( {
// 		"dom": '<"toolbar"f>rt'
//     } );
    $("div.toolbar").append($("#all_btn_gp").html());
} );

</script>
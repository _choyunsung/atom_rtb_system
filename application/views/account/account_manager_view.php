<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strPersnalInfoMgr')?></li>
		</ol>
	</div>
	<div class="panel-body">
		<div class="tab-base">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#"><?php echo lang('strManagerInfo2');?></a></li>
				<li><a href="/account/account_login_list"><?php echo lang('strLoginHistory');?></a></li>
			</ul>
			<div class="tab-content">
				<div id="demo-lft-tab-1" class="tab-pane fade active in">
			       <div class="panel-heading over_hidden">
                        <h3 class="page-header text-overflow wid_30p ma_t8 float_l tit_blit0"><?php echo lang('strCompanyInfo');?></h3>
                        <p class="txt-right wid_30p float_r">
                            <a class="btn btn-primary" href="javascript:account_manager_modify();"><?php echo lang('strManagerInfoModify');?></a>
                        </p>
                    </div>
					<form name="manager_detail" method="post">
					    <input type="hidden" name="manager_no" id="manager_no" value="<?php echo $mem_no?>"?>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th><?php echo lang('strAuthority');?></th>
    							<td><?php echo lang('strManager');?></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strID');?></th>
    							<td><?php echo $manager_info['mem_id']?></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strPassword');?></th>
    							<td><p class="ma_t8 wid_30p float_l">****</p> </td>
    						</tr>
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0"><?php echo lang('strContactInfo');?></h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th><?php echo lang('strContactName');?></th>
    							<td><?php echo $manager_info['mem_nm'];?></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strEmail');?></th>
    							<td><?php echo $manager_info['mem_email'];?></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strOfficeNumber');?></th>
    							<td><?php echo $manager_info['mem_tel'];?></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strMobileNumber');?></th>
    							<td><?php echo $manager_info['mem_cell'];?></td>
    						</tr>
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0"><?php echo lang('strAlertSetting');?></h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th><?php echo lang('strInfoEmail');?></th>
    							<td class="point_dgray"><?php echo lang('strCashAlert');?> <i class="fa fa-bell"></i></td>
    							<td><input type="checkbox" name="email_alarm_c10_fl" <?php if($manager_info['email_alarm_c10_fl'] == "Y"){echo "checked";}?> disabled> <?php echo lang('strCampaignCashAlert');?>
    							    <input type="checkbox" name="email_alarm_d10_fl" <?php if($manager_info['email_alarm_d10_fl'] == "Y"){echo "checked";}?> disabled> <?php if($manager_info['fee_fl'] == "Y"){ echo "100,000";}else{echo "10,000";}?><?php echo lang('strCampaignCashAlert2');?>
    							    <input type="checkbox" name="email_alarm_c0_fl" <?php if($manager_info['email_alarm_c0_fl'] == "Y"){echo "checked";}?> disabled> <?php echo lang('strCampaignCashAlert3');?>
    						    </td>
    						</tr>
    					</table>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>

    function account_manager_modify(){
        var frm=document.manager_detail;
        frm.action="/account/account_manager_modify";
        frm.submit();
    }
</script>

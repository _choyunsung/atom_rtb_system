<!-- 비번변경 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <h4 class="h4_blit ma_b5 ma_t5">비밀번호 변경</h4>
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb">
                        <colgroup>
                            <col width="30%">
                            <col width="70%">
                        </colgroup>
                        <tr>
                            <th class="point_blue">현재 비밀번호</th>
                            <td><input type="password" class="wid_50p" id="tmp_pwd"></td>
                        </tr>
                        <tr>
                            <th class="point_blue">새로운 비밀번호</th>
                            <td><input type="password" class="wid_50p" id="new_pwd"></td>
                        </tr>
                        <tr>
                            <th class="point_blue">비밀번호 확인</th>
                            <td><input type="password" class="wid_50p" id="new_pwd_check"></td>
                        </tr>	
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t8">
                <span onclick="member_master_pwd_modify();" class="btn btn-primary"><?php echo lang('strSave')?></span>
                <span data-dismiss="modal" onclick="$('#modal1').modal('hide');" class="btn btn-dark close" ><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>

<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strPersnalInfoMgr')?></li>
		</ol>
	</div>
	<div class="panel-body">
		<div class="tab-base">
			<div class="tab-content">
				<div id="demo-lft-tab-1" class="tab-pane fade active in">
					<div class="panel-heading">
						<h3 class="page-header text-overflow tit_blit0">업체정보 수정</h3>
					</div>
					<form name="master_detail" method="post">
					    <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no?>"?>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="30%">
    							<col width="20%">
    							<col width="30%">
    						</colgroup>
    						<tr>
    							<th><?php echo lang('strID');?></th>
    							<td><?php echo $master_info['mem_id']?></td>
    							<th class="bd_l"><?php echo lang('strPassword');?></th>
    							<td><p class="wid_10p float_l ma_t8">****</p> <span class="btn btn-default float_r" onclick="$('#modal1').modal('show');">비밀번호 변경</span></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strBusinessName');?></th>
                                <td><?php echo $master_info['mem_com_nm']?></td>
                                <th class="bd_l"><?php echo lang('strCompany');?></th>
                                <td><?php echo $master_info['mem_com_nm']?></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strBusinessNo');?></th>
    							<td colspan="3"><?php echo $master_info['mem_com_no']?></td>
    						</tr>
    						<tr>
    							<th class="point_blue">대표자명</th>
    							<td colspan="3"><input type="text" class="wid_40p" name="mem_com_ceo" value="<?php echo $master_info['mem_com_ceo']?>"></td>
    						</tr>
    						<tr>
    							<th class="point_blue">업태</th>
    							<td colspan="3"><input type="text" class="wid_40p" name="mem_com_type" value="<?php echo $master_info['mem_com_type']?>"></td>
    						</tr>
    						<tr>
    							<th class="point_blue">종목</th>
    							<td colspan="3"><input type="text" class="wid_40p" name="mem_com_item" value="<?php echo $master_info['mem_com_item']?>"></td>
    						</tr>
    						<tr>
    							<th class="point_blue"><?php echo lang('strAddress');?></th>
    							<td colspan="3" class="pa_t5 pa_b5">
    							<div class="over_hidden">
    							     <input type="text" id="mem_post1" name="mem_post1" class="wid_10p float_l" value="<?php echo substr($master_info['mem_post'], 0, 3);?>" readonly>
    							     <span class="float_l ma_t5">&nbsp;- &nbsp;</span> 
    							     <input type="text" id="mem_post2" name="mem_post2" class="wid_10p float_l" value="<?php echo substr($master_info['mem_post'], 4, 3);?>" readonly>
    							     <span class="btn btn-default float_l ma_l5" onclick="sample4_execDaumPostcode();"><?php echo lang('strZipCode');?> <?php echo lang('strFind');?></span>
    							</div>
    							<input type="text" id="mem_addr" name="mem_addr" class="clear wid_60p ma_t5" value="<?php echo $master_info['mem_addr'];?>" >
    							</td>
    						</tr>
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0">세금계산서 담당자 정보 수정</h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th><?php echo lang('strManagerName');?></th>
    							<td><input type="text" class="wid_30p" name="tax_mem_nm" value="<?php echo $master_info['tax_mem_nm'];?>"></td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strEmail');?></th>
    							<td><input type="text" class="wid_30p" name="tax_mem_email" value="<?php echo $master_info['tax_mem_email'];?>"></td>
    						</tr>
    						<tr>
                                <th><?php echo lang('strOfficeNumber');?></th>
                                <td>
                                    <?php 
                                        if ($master_info['tax_mem_tel'] == ""){
                                            $mem_tel="--";
                                        }else{
                                            $mem_tel = $master_info['tax_mem_tel'];
                                        }

                                        if ($master_info['tax_mem_cell'] == ""){
                                            $mem_cell="--";
                                        }else{
                                            $mem_cell = $master_info['tax_mem_cell'];
                                        }
                                        $tax_mem_tel = explode('-', $mem_tel);
                                        $tax_mem_cell = explode('-', $mem_cell);
                                    ?>
                                    <div class="btn-group">
                                        <span class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span id="sel_mem_tel_name">&nbsp;<?php if ($tax_mem_tel[0] != ""){ echo $tax_mem_tel[0]; }else{ echo lang('strSelect'); }?>&nbsp;</span> 
                                        <i class="dropdown-caret fa fa-caret-down"></i>
                                        </span>
                                        <input type="hidden" id="tax_mem_tel1" name="tax_mem_tel1" value="<?php echo $tax_mem_tel[0];?>">
                                        <ul class="dropdown-menu ul_sel_box">
                                            <?php foreach($tel_no_list as $row){?>
                                                <li><a href="javascript:sel_mem_tel('<?php echo $row['code_desc'];?>')"><?php echo $row['code_desc'];?></a>
                                                </li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                    <span class="float_l ma_t8">&nbsp;-&nbsp; </span>
                                    <input type="text" class="wid_10p float_l" id="tax_mem_tel2" name="tax_mem_tel2" maxlength="4" onblur="only_number();" value="<?php if ($tax_mem_tel[1] != ""){echo $tax_mem_tel[1];}?>">
                                    <span class="float_l ma_t8">&nbsp;-&nbsp;</span>
                                    <input type="text" class="wid_10p float_l" id="tax_mem_tel3" name="tax_mem_tel3" maxlength="4" onblur="only_number();" value="<?php echo $tax_mem_tel[2]?>">
                                </td>
                            </tr>
                            <tr>
                                <th><?php echo lang('strMobileNumber')?></th>
                                <td>
                                    <div class="btn-group">
                                        <span class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="">
                                        <span id="sel_mem_cell_name">&nbsp;<?php if ($tax_mem_cell[0] != ""){ echo $tax_mem_cell[0]; }else{ echo lang('strSelect'); }?>&nbsp;</span> 
                                        <i class="dropdown-caret fa fa-caret-down"></i>
                                        </span>
                                        <input type="hidden" id="tax_mem_cell1" name="tax_mem_cell1" value="<?php echo $tax_mem_cell[0];?>">
                                        <ul class="dropdown-menu ul_sel_box">
                                            <?php foreach($cell_no_list as $row){?>
                                                <li><a href="javascript:sel_mem_cell('<?php echo $row['code_desc'];?>')" ><?php echo $row['code_desc'];?></a>
                                                </li>
                                            <?php }?>
                                        </ul>
                                    </div>  
                                    <span class="float_l ma_t8">&nbsp;&ndash;&nbsp;</span>  
                                    <input type="text" class="wid_10p float_l" id="tax_mem_cell2" name="tax_mem_cell2" maxlength="4" onblur="only_number();" value="<?php echo $tax_mem_cell[1];?>">  
                                    <span class="float_l ma_t8">&nbsp;&ndash;&nbsp;</span>  
                                    <input type="text" class="wid_10p float_l" id="tax_mem_cell3" name="tax_mem_cell3" maxlength="4" onblur="only_number();" value="<?php echo $tax_mem_cell[2];?>">
                                    <font class="join_info float_l ma_t5 ma_l10">* 아이디 검색/비밀번호 재발급을 위해 정확한 휴대폰 번호가 필요합니다.</font>
                                </td>
                            </tr>	
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0">알림 설정 수정</h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th>정보성 이메일</th>
    							<td class="point_dgray">결제알림 <i class="fa fa-bell"></i></td>
    							<td><input type="checkbox" name="email_alarm_c10_fl" <?php if($master_info['email_alarm_c10_fl'] == "Y"){echo "checked";}?> value="Y"> 캠페인 일예산 10% 미만 시
    							    <input type="checkbox" name="email_alarm_d10_fl" <?php if($master_info['email_alarm_d10_fl'] == "Y"){echo "checked";}?> value="Y" class="ma_l20"> <?php if($master_info['fee_fl'] == "Y"){ echo "100,000";}else{echo "10,000";}?>캐쉬 미만 시
    							    <input type="checkbox" name="email_alarm_c0_fl" <?php if($master_info['email_alarm_c0_fl'] == "Y"){echo "checked";}?> value="Y" class="ma_l20"> 캐쉬 모두 소진 시
    						    </td>
    						</tr>
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0">환불계좌 정보 수정</h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th><?php echo lang('strBank');?></th>
    							<td class="join_info">
        						    <div class="btn-group">
        						        <input type="hidden" name="bank_nm" id="bank_nm" value="<?php echo $master_info['bank_nm']?>">
                                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="bank_sel">
                                            <?php if($master_info['bank_nm'] == ""){?>
                                                <span id="bank_sel_view">&nbsp;  <?php echo lang('strSelect')?>  &nbsp;</span>
                                            <?php }else{?>
                                                <span id="bank_sel_view">&nbsp;  <?php echo $master_info['bank_nm']?>  &nbsp;</span>
                                            <?php }?>
                                            <i class="dropdown-caret fa fa-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu ul_sel_box">
                                            <?php foreach($sel_bank_name as $sel){?>
                                                <li>
                                                    <a href="javascript:bank_sel('<?php echo $sel['code_desc'];?>')">
                                                        <?php echo $sel['code_desc'];?>
                                                    </a>
                                                </li>
                                            <?php }?>
                                        </ul>
                                    </div>
    						     </td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strBankNumber');?></th>
    							<td class="join_info">
    							     <input type="text" class="wid_30p" name="bank_no" value="<?php echo $master_info['bank_no'];?>" onblur="only_number()">
    							     <span class="ma_l5 point_dgray">숫자만 입력</span>
    						     </td>
    						</tr>
    						<tr>
    							<th><?php echo lang('strBankName');?></th>
    							<td class="join_info"><input type="text" class="wid_30p" name="bank_mem_nm" value="<?php echo $master_info['bank_mem_nm'];?>"></td>
    						</tr>
    					</table>
    					
    					
					</form>
					<p class="pa_b20 ma_l10 txt-right">
						<span class="btn btn-primary" onclick="master_info_modify_save()">저장</span> 
						<span class="btn btn-default"><a href="/account/account_master_view">취소</a></span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script>
    function sample4_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                document.getElementById("mem_post1").value = data.postcode1;
                document.getElementById("mem_post2").value = data.postcode2;
                document.getElementById("mem_addr").value = fullRoadAddr;
                //document.getElementById("sample4_jibunAddress").value = data.jibunAddress;
                close();
            }
        }).open();
        
    }

    function only_number(){
        var objEvent = event.srcElement;
        var numPattern = /^[0-9+]*$/;
        numPattern = objEvent.value.match(numPattern);

        if (numPattern == null) {
            alert("숫자만 입력해 주세요.");
            objEvent.value = "";
            objEvent.focus();
            return false;
        }
    }
    
    function bank_sel(bank_nm){
    	$("#bank_sel_view").empty();
    	$("#bank_sel_view").html(bank_nm);
    	$("#bank_nm").attr('value',bank_nm);
    }

    function sel_mem_tel(code){
        $("#sel_mem_tel_name").empty();
        $("#sel_mem_tel_name").html(code);
        $("#tax_mem_tel1").attr("value",code);
    }

    function sel_mem_cell(code){
        $("#sel_mem_cell_name").empty();
        $("#sel_mem_cell_name").html(code);
        $("#tax_mem_cell1").attr("value",code);
    }
    
    function member_master_pwd_modify(){
        var tmp_pwd = $("#tmp_pwd").val();
        var new_pwd = $("#new_pwd").val();
        var pwd_check = $("#new_pwd_check").val();
        if(new_pwd == pwd_check){
            url = '/account/account_master_pwd_save';
            $.post(url,
                {
                mem_pwd : new_pwd,
                tmp_pwd : tmp_pwd,
                mem_no : '<?php echo $mem_no;?>'
                },
                function(data){
                    if(data.trim()=="OK"){
                        alert("비밀번호 수정 완료");
                        $('#modal1').modal('hide');
                        $('div#modal1 input').val('');
                    }else if(data.trim() == "check_false"){
                        alert("기존 비밀번호가 일치하지 않습니다.");
                    }else{
                        alert("비밀번호 변경 실패");
                    }
                }
            );
        }else{
            alert("새로운 비밀번호와 비밀번호 확인이 일치하지 않습니다.");
        }
    }

    function master_info_modify_save(){
        var frm=document.master_detail;
        frm.action="/account/master_info_modify_save";
        frm.submit();
    }
</script>

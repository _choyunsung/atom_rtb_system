<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strPersnalInfoMgr')?></li>
		</ol>
	</div>
	<div class="panel-body">
		<div class="panel-heading">
			<h3 class="page-header text-overflow tit_blit0"><?php echo lang('strAdvertiser')?> <?php echo lang('strModify')?></h3>
		</div>
		<form name="advertiser_modify" method="post">
		    <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no?>"?>
			<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
				<colgroup>
					<col width="20%">
					<col width="*">
				</colgroup>
				<tr>
					<th>등록일</th>
					<td><?php echo $adver_info['mem_ymd'];?></td>
				</tr>
				<tr>
					<th>광고주명</th>
					<td><input type="text" class="wid_30p" name="mem_com_nm" value="<?php echo $adver_info['mem_com_nm'];?>"></td>
				</tr>
				<tr>
					<th>URL</th>
					<td><input type="text" class="wid_30p" name="mem_com_url" value="<?php echo $adver_info['mem_com_url'];?>"></td>
				</tr>
				<tr>
					<th>담당자 이름</th>
					<td><input type="text" class="wid_30p" name="mem_nm" value="<?php echo $adver_info['mem_nm'];?>"></td>
				</tr>
				<tr>
					<th>이메일</th>
					<td><input type="text" class="wid_30p" name="mem_email" value="<?php echo $adver_info['mem_email'];?>"></td>
				</tr>
				<tr>
					<th>사무실 번호</th>
					<td><input type="text" class="wid_30p" name="mem_tel" value="<?php echo $adver_info['mem_tel'];?>"></td>
				</tr>
				<tr>
					<th>휴대폰 번호</th>
					<td><input type="text" class="wid_30p" name="mem_cell" value="<?php echo $adver_info['mem_cell'];?>"></td>
				</tr>
				<tr>
					<th>사업자번호</th>
					<td><input type="text" class="wid_30p" name="mem_com_no" value="<?php echo $adver_info['mem_com_no'];?>"></td>
				</tr>
				<tr>
					<th>주소</th>
					<td><input type="text" class="wid_70p" name="mem_addr" value="<?php echo $adver_info['mem_addr'];?>"></td>
				</tr>
			</table>
		</form>
    		<p class="pa_b20 ma_l10 txt-right">
    			<span class="btn btn-primary" onclick="advertiser_modify_save()">저장</span> 
    			<span class="btn btn-default" onclick="location.replace('/account/account_advertiser_list')">취소</span>
			</p>
		</div>
	</div>
<script>

function advertiser_modify_save(){
    var frm=document.advertiser_modify;
    frm.action="/account/advertiser_modify_save";
    frm.submit();
}

</script>

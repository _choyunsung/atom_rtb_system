<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
			<li class="active"><?php echo lang('strAccountList')?></li>
		</ol>
	</div>	
	<div class="panel-body">
	    <div class="panel-heading">
			<h3 class="page-header wid_30p float_l text-overflow tit_blit"><?php echo lang('strAccountInfo')?></h3>
		</div>
        <form name="account_list" method="post">
            <div class="float_l wid_100p">
                <table class="aut_tb">
<!--                     <colgroup> -->
<!--                         <col width="10%"> -->
<!--                         <col width="35%"> -->
<!--                         <col width="10%"> -->
<!--                         <col width="35%"> -->
<!--                         <col width="10%"> -->
<!--                         <col width="35%"> -->
<!--                         <col width="10%"> -->
<!--                         <col width="35%"> -->
<!--                         <col width="10%"> -->
<!--                         <col width="35%"> -->
<!--                         <col width="10%"> -->
<!--                         <col width="35%"> -->
<!--                     </colgroup> -->
                    <tr>
                        <th><?php echo lang('strID')?></th>
                        <td>
                            <?php echo $detail_info['mem_id'];?>
                        </td>
                        <th><?php echo lang('strAuthority')?></th>
                        <td>
                            <?php if($detail_info['mem_type'] == "master"){
                                echo lang('strMaster');
                            }else{
                                echo lang('strManager');
                            }?>
                        </td>
                        <th><?php echo lang('strUserType')?></th>
                        <td>
                            <?php if ($detail_info['role'] == "agency"){
                                echo lang('strAgency');
                            }elseif ($detail_info['role'] == "rep"){
                                echo lang('strLab');
                            }elseif ($detail_info['role'] == "adver"){
                                echo lang('strAdvertiser');
                            }elseif ($detail_info['role'] == "ind"){
                                echo lang('strIndividual');
                            }elseif ($detail_info['role'] == "sales"){
                                echo lang('strSales');
                            }?>
                        </td>
                        <th><?php echo lang('strSales')?></th>
                        <td>
                            <?php echo $detail_info['sales_nm'];?>
                        </td>
                        <th><?php echo lang('strStatus')?></th>
                        <td>
                            <?php
                                if ($detail_info['mem_active_st'] == 1){
                                    echo lang('strRun');
                                }elseif ($detail_info['mem_active_st'] == 2){
                                    echo lang('strDormancy');
                                }elseif ($detail_info['mem_active_st'] == 3){
                                    echo lang('strPause');
                                }elseif ($detail_info['mem_active_st'] == 4){
                                    echo lang('strWithdrawal');
                                }
                            ?>
                        </td>
                        <th><?php echo lang('strLastConnect')?></th>
                        <td>
                            <?php echo $detail_info['mem_last_login']?>
                        </td>
                    </tr>
                </table>
                <div class="float_r ma_b3" id="all_btn_gp">
                    <span class="btn btn-primary ma_l5 float_r" onClick ="search();"><?php echo lang('strSearch')?></span>
                    <a href="#" id="export_excel" download="" class="btn btn-primary "><?php echo lang('strDownload');?></a>
                </div>
         	</div>
            <div class="clear"></div>
            <div class="tab-base">
            <!--Nav Tabs-->
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#demo-lft-tab-1" aria-expanded="true"><?php echo lang('strAccountInfo')?></a>
                    </li>
                    <?php if ($master_info->group_no != 2){?>
                        <li>
                            <a href="/account/account_manager_list">매니저 리스트</a>
                        </li>
                    <?php }?>
                    <li>
                        <a href="/account/account_login_list">로그인 이력</a>
                    </li>
                </ul>
            </div>
            <table id="list" name="list" class="inspection_tb" data-horizontal-width="100%" data-display-length="50" cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="5%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="15%">
                    <col width="10%">
                    <col width="12%">
                    <col width="8%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                    <tr>                                                                                                                                    
                        <th>No.</th>
                        <th><?php echo lang('strJoinDate');?></th>
                        <th>ID</th>
                        <th><?php echo lang('strAuthority');?></th>
                        <th><?php echo lang('strUserType');?></th>
                        <th><?php echo lang('strCompany');?></th>
                        <th><?php echo lang('strManager');?></th>
                        <th><?php echo lang('strStatus');?></th>
                        <th><?php echo lang('strLastConnect')?></th>
                        <th><?php echo lang('strSales');?></th>
                   </tr>
                </thead>
                <tbody>
                    <?php if (isset($account_list)){?>
                    <?php foreach ($account_list as $row){ ?>
                    <tr>
                        <td><?php echo $row['mem_no']?></td>
                        <td><?php echo substr($row['mem_ymd'], 0, 10)?></td>
                        <td><a href="javascript:detail_view('<?php echo $row['mem_no']?>')"> <?php echo $row['mem_id']?></a></td>
                        <td>
                            <div class="btn-group" style="cursor:pointer;">
                                <span class="span" data-toggle="dropdown">
                                <?php
                                    if($row['mem_type'] == "master"){
                                        echo lang('strMaster');
                                    }else{
                                        echo lang('strManager');
                                    }
                                ?>
                                    <i class="dropdown-caret fa fa-caret-down float_r ma_t5 ma_r5 cursor"></i>
                                </span>
                                <ul class="dropdown-menu ul_sel_box">
                                    <li>
                                        <a href="javascript:modify_type('<?php echo $row['mem_no']?>', 'master')">
                                            <?php echo lang('strMaster');?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:modify_type('<?php echo $row['mem_no']?>', 'manager')">
                                            <?php echo lang('strManager'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                        <td><?php echo $row['group_nm']?></td>
                        <td class="txt-left"><?php echo $row['mem_com_nm']?></td>
                        <td><?php echo $row['mem_nm']?></td>
                        <td class="txt_center">
                            <div class="btn-group" style="cursor:pointer;">
                                <span class="span" data-toggle="dropdown" id="active_sel">
                                <?php
                                    if($row['mem_active_st'] == 1){
                                        echo lang('strRun');
                                    }elseif($row['mem_active_st'] == 2){
                                        echo lang('strDormancy');
                                    }elseif($row['mem_active_st'] == 3){
                                        echo lang('strPause');
                                    }elseif($row['mem_active_st'] == 4){
                                        echo lang('strWithdrawal');
                                    }
                                ?>
                                    <i class="dropdown-caret fa fa-caret-down float_r ma_t5 ma_r5 cursor"></i>
                                </span>
                                <ul class="dropdown-menu ul_sel_box">
                                    <li>
                                        <a href="javascript:modify_active(<?php echo $row['mem_no']?>, 1)">
                                            <?php echo lang('strRun');?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:modify_active(<?php echo $row['mem_no']?>, 2)">
                                            <?php echo lang('strDormancy'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:modify_active(<?php echo $row['mem_no']?>, 3)">
                                           <?php echo lang('strPause'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:modify_active(<?php echo $row['mem_no']?>, 4)">
                                            <?php echo lang('strWithdrawal'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                        <td><?php echo $row['mem_last_login']?></td>
                        <td class="txt_center">
                            <div class="btn-group" style="cursor:pointer;">
                                <span class="span" data-toggle="dropdown" id="sales_sel">
                                    <?php
                                        if($row['sales_nm'] != ""){
                                            echo $row['sales_nm'];
                                        }else{
                                            echo lang('strSelect');
                                        }
                                    ?>
                                    <i class="dropdown-caret fa fa-caret-down float_r ma_t5 ma_r5 cursor"></i>
                                </span>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php
                                        if($row['sales_nm'] != ""){
                                    ?>
                                    <li>
                                        <a href="javascript:modify_sales(<?php echo $row['mem_no']?>, '')">
                                            선택안함
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        foreach($sales_list as $sales){
                                    ?>
                                    <li>
                                        <a href="javascript:modify_sales(<?php echo $row['mem_no']?>, <?php echo $sales['mem_no'];?>)">
                                            <?php echo $sales['mem_nm'];?>
                                        </a>
                                    </li>
                                    <?php
                                        }
                                    ?>
                                </ul>
                            </div>
                        </td>
                   </tr>
                   <?php
                        }}
                    ?>
                </tbody>
            </table>
            <div class="center">
                <div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box" >
                        <li><a href="javascript:page_change(100)">100</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(10)">10</a></li>
                    </ul>
                </div>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div> 
        </form>
   </div>
</div>
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm = document.account_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.account_list;
        frm.action="/account/account_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    function select_status(type){
        $("#master_yn").prop('checked',false);
        $("#manager_yn").prop('checked',false);
        $("#mem_com_nm").val('');
        $("#mem_id").val('');
        $("#run_yn").prop('checked',false);
        $("#pause_yn").prop('checked',false);
        $("#last_connect").val('');
        $("#daterange").val('');
        if (type == "today"){
        	$("#daterange").val('<?php echo date('Y-m-d') ?>'+ ' ~ ' + '<?php echo date('Y-m-d')?>');
        }else{        
            $("#"+type).val('Y');
        }
        
        search();
    }
    $(document).ready(function() {
        /*
        $('#list').dataTable( {
            "dom": '<"toolbar"f>rt'
        } );
        */
        $("div.toolbar").append($("#all_btn_gp").html());
    } );

    function modify_sales(mem_no, sales_no){
        if(confirm("저장하시겠습니까?")){
            var frm = document.account_list;
            frm.action="/account/account_sales_modify";
            frm.mem_no.value = mem_no;
            frm.sales_no.value = sales_no;
            frm.submit();
        }else{
            //alert("취소되었습니다");
        }
    }
    function modify_active(mem_no, active_no){
        if(confirm("저장하시겠습니까?")){
            var frm = document.account_list;
            frm.action="/account/account_active_modify";
            frm.mem_no.value = mem_no;
            frm.active_no.value = active_no;
            frm.submit();
        }else{
            //alert("취소되었습니다");
        }
    }

    function modify_type(mem_no, type){
        if(confirm("권한을 변경하시겠습니까?")){
            var frm = document.account_list;
            frm.action="/account/account_type_modify";
            frm.mem_no.value = mem_no;
            frm.mem_type.value = type;
            frm.submit();
        }else{
            //alert("취소되었습니다");
        }
    }

    function search(){
		var frm = document.account_list;
		frm.action="/account/account_list";
		frm.submit();
	}

</script>
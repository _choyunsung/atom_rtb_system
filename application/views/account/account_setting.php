<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strAccountSetting')?></li>
		</ol>
	</div>	
	<div class="panel-body">
		<div class="panel-heading ma_t30">
			<h3 class="page-header tit_blit0 wid_40p float_l"><?php echo lang('strAccountSetting')?></h3>
																								
			<span class="btn btn-primary float_r ma_b10" onclick="setting_modify();"><?php echo lang('strAccountSettingModify');?></span>
		</div>
	    <table class="aut_tb" cellpaddig="0" cellspacing="0" id="setting_view">
			<colgroup>
				<col width="20%">
				<col width="*">
			
			</colgroup>
			<tr>
				<th><?php echo lang('strUserID');?></th>
				<td><?php echo $account_setting['mem_id']?></td>
		    </tr>
			<tr>
				<th><?php echo lang('strLangMark');?></th>
				<td><?php if ($account_setting['mem_lang'] == "korean"){echo lang('strKorean');}else{echo lang('strEnglish');}?></td>
			</tr>
			<tr>
				<th><?php echo lang('strTermCurrency');?></th>
				<td>
				    <?php 
				        if ($account_setting['mem_cur'] == ""){
				            if ($account_setting['mem_lang'] == "korean"){
				                echo lang('strWon');
				            }else{
				                echo lang('strDollar');
				            }
				        }else{
				            echo $account_setting['mem_cur'];
				        }
                    ?>
				</td>
			</tr>														
			<tr>
				<th><?php echo lang('strTimeZone');?></th>
				<td>
				    <?php 
				        if ($account_setting['tz_nm'] == ""){
				            if ($account_setting['mem_lang'] == "korean"){
				                echo "Asia/Seoul(+09.00)";
				            }else{
				                echo "-";
				            }
				        }else{
				            echo $account_setting['tz_nm']."(".$account_setting['tz_val'].")";
				        }
                    ?>
                </td>
			</tr>														
		</table>	
		<form name="account_setting_form" method="post">	
		    <input type="hidden" name="mem_no" value="<?php echo $mem_no?>">										
    	    <table class="aut_tb" cellpaddig="0" cellspacing="0" id="setting_modify" style="display:none">
    			<colgroup>
    				<col width="20%">
    				<col width="*">
    			</colgroup>
    			<tr>
    				<th><?php echo lang('strUserID');?></th>
    				<td><?php echo $account_setting['mem_id']?></td>
    		    </tr>
    		    <tr>
    				<th><?php echo lang('strLangMark');?></th>
    				<td>
    				    <input type="hidden" class="float_l" name="mem_lang" id="mem_lang" value="<?php echo $account_setting['mem_lang']?>">
    				    <input type="hidden" class="float_l" name="mem_cur" id="mem_cur" value="<?php echo $account_setting['mem_cur']?>">
    				    <div class="float_l btn-group">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span id="mem_lang_view">&nbsp;  <?php if ($account_setting['mem_lang'] == "korean"){echo lang('strKorean');}else{echo lang('strEnglish');}?>  &nbsp;</span>
                                <i class="dropdown-caret fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu ul_sel_box">
                                <?php foreach($sel_country as $sel){?>
                                    <li>
                                        <a href="#" onclick="account_lang_change('<?php echo $sel['ct_cd'];?>','<?php echo $sel['ct_iso_cd'];?>','<?php echo $sel['ct_nm'];?>','<?php echo $sel['ct_cur_cd'];?>')">
                                            <?php if ($sel['ct_cd'] == "KR"){echo lang('strKorean');}else{echo lang('strEnglish');}?>
                                        </a>
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
    				</td>
    			</tr>
    			<tr>
    				<th><?php echo lang('strTermCurrency');?></th>
    				<td>
    				    <span id="mem_cur_view"><?php echo $account_setting['mem_cur'];?></span>
    				</td>
    			</tr>														
    			<tr>
    				<th><?php echo lang('strTimeZone');?></th>
    				<td>
                         Asia/Seoul(+09.00)
    				</td>
    			</tr>										
    		</table>	
		</form>											
		<p class="pa_b20 ma_l10 txt-right" id="setting_btn" style="display:none">
			<span class="btn btn-primary" onclick="account_setting_modify_save()"><?php echo lang('strSave');?></span> 
    		<span class="btn btn-default" onclick="setting_modify_cancel()"><?php echo lang('strCancel');?></span>
    	</p>									
    </div>
</div>
<script>
function setting_modify(){
	$("#setting_view").hide();
	$("#setting_modify").show();
	$("#setting_btn").show();
}

function setting_modify_cancel(){
	$("#setting_view").show();
	$("#setting_modify").hide();
	$("#setting_btn").hide();
}

function account_lang_change(ct_cd, ct_uso_cd, ct_nm, ct_cur_cd){
	if (ct_cd == "KR"){
		var view = '<?php echo lang('strKorean')?>';
		ct_cd = "korean";
	}else{
		var view = '<?php echo lang('strEnglish')?>';
		ct_cd = "english";
	}
	$("#mem_lang").val(ct_cd);
	$("#mem_lang_view").html(view);
	$("#mem_cur").val(ct_cur_cd);
	$("#mem_cur_view").html(ct_cur_cd);
}

function account_timezone_change(tz_no, tz_nm, tz_val){
	$("#mem_timezone").val(tz_no);
	$("#mem_timezone_view").html(tz_nm+'('+tz_val+')');
}

function account_setting_modify_save(){
	var frm = document.account_setting_form;
	frm.action="/account/account_setting_save"
	frm.submit();
}
</script>

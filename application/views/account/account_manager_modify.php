<!-- 비번변경 modal -->
<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_content">
            <div class="modal-header">
                <h4 class="h4_blit ma_b5 ma_t5">비밀번호 변경</h4>
            </div>
            <div class="modal-body">
                <div style="clear:both;">
                    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb">
                        <colgroup>
                            <col width="30%">
                            <col width="70%">
                        </colgroup>
                        <tr>
                            <th class="point_blue">현재 비밀번호</th>
                            <td><input type="password" class="wid_70p" id="tmp_pwd"></td>
                        </tr>
                        <tr>
                            <th class="point_blue">새로운 비밀번호</th>
                            <td><input type="password" class="wid_70p" id="new_pwd"></td>
                        </tr>
                        <tr>
                            <th class="point_blue">비밀번호 확인</th>
                            <td><input type="password" class="wid_70p" id="new_pwd_check"></td>
                        </tr>	
                    </table>
                </div>
            </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t8 ma">
                <span onclick="member_master_pwd_modify();" class="btn btn-primary"><?php echo lang('strSave')?></span>
                <span data-dismiss="modal" onclick="$('#modal1').modal('hide');" class="btn btn-dark"><?php echo lang('strClose')?></span>
            </div>
		</div>
    </div>
</div>

<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
            <li class="active"><?php echo lang('strPersnalInfoMgr')?></li>
		</ol>
	</div>
	<div class="panel-body">
		<div class="tab-base">
			<ul class="nav nav-tabs">
			    <?php if($this->session->userdata('mem_type')=="master"){?>
				    <li ><a data-toggle="tab" href="/account/account_master_info">마스터 정보 </a></li>
				<?php }?>
				<li class="active"><a data-toggle="tab" href="#">매니저 정보</a></li>
				<li><a data-toggle="tab" href="/account/account_login_view">로그인 이력</a></li>
			</ul>
			<div class="tab-content">
				<div id="demo-lft-tab-1" class="tab-pane fade active in">
					<div class="panel-heading">
						<h3 class="page-header text-overflow tit_blit0">업체정보</h3>
					</div>
					<form name="master_detail" method="post">
					    <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no?>"?>
					    <input type="hidden" name="master_mem_no" id="master_mem_no" value="<?php echo $master_mem_no?>"?>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th>권한</th>
    							<td>매니저</td>
    						</tr>
    						<tr>
    							<th>아이디</th>
    							<td><?php echo $manager_info['mem_id']?></td>
    						</tr>
    						<tr>
    							<th>비밀번호</th>
    							<td>
	    							<p class="ma_t8 float_l wid_30p">*****</p>
	    							<span class="float_r ma_r10 btn-default btn" onclick="$('#modal1').modal('show');">비밀번호 변경</span>
    							</td>
    						</tr>
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0">담당자 정보</h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th>담당자 이름</th>
    							<td><input type="text" class="wid_30p" name="mem_nm" value="<?php echo $manager_info['mem_nm'];?>"></td>
    						</tr>
    						<tr>
    							<th>이메일</th>
    							<td><input type="text" class="wid_30p" name="mem_email" value="<?php echo $manager_info['mem_email'];?>"></td>
    						</tr>
    						<tr>
    							<th>사무실 번호</th>
    							<td><input type="text" class="wid_30p" name="mem_tel" value="<?php echo $manager_info['mem_tel'];?>"></td>
    						</tr>
    						<tr>
    							<th>휴대폰 번호</th>
    							<td><input type="text" class="wid_30p" name="mem_cell" value="<?php echo $manager_info['mem_cell'];?>"></td>
    						</tr>
    						<tr>
    							<th>설명</th>
    							<td><input type="text" class="wid_30p" name="mem_cont" value="<?php echo $manager_info['mem_cont'];?>"></td>
    						</tr>
    					</table>
    					<div class="panel-heading">
    						<h3 class="page-header text-overflow tit_blit0">알림 설정</h3>
    					</div>
    					<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
    						<colgroup>
    							<col width="20%">
    							<col width="*">
    						</colgroup>
    						<tr>
    							<th>정보성 이메일</th>
    							<td class="point_dgray">결제알림 <i class="fa fa-bell"></i></td>
    							<td><input type="checkbox" name="email_alarm_c10_fl" <?php if ($manager_info['email_alarm_c10_fl'] == "Y"){echo "checked";}?> value="Y"> 캠페인 일예산 10% 미만 시
    							    <input type="checkbox" name="email_alarm_d10_fl" <?php if ($manager_info['email_alarm_d10_fl'] == "Y"){echo "checked";}?> value="Y" class="ma_l20"> <?php if ($manager_info['fee_fl'] == "Y"){ echo "100,000";}else{echo "10,000";}?>캐쉬 미만 시
    							    <input type="checkbox" name="email_alarm_c0_fl" <?php if ($manager_info['email_alarm_c0_fl'] == "Y"){echo "checked";}?> value="Y" class="ma_l20"> 캐쉬 모두 소진 시
    						    </td>
    						</tr>
    					</table>
					</form>
					<p class="pa_b20 ma_l10 txt-right">
						<span class="btn btn-primary" onclick="manager_info_modify_save()">저장</span> 
						<span class="btn btn-default" onclick="location.replace('/account/account_manager_view');">취소</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
    function member_master_pwd_modify(){
        var tmp_pwd = $("#tmp_pwd").val();
        var new_pwd = $("#new_pwd").val();
        var pwd_check = $("#new_pwd_check").val();
        if(new_pwd == pwd_check){
            url = '/account/account_master_pwd_save';
            $.post(url,
                {
                mem_pwd : new_pwd,
                tmp_pwd : tmp_pwd,
                mem_no : '<?php echo $mem_no;?>'
                },
                function(data){
                    if(data.trim()=="OK"){
                        alert("비밀번호 수정 완료");
                        $('#modal1').modal('hide');
                        $('div#modal1 input').val('');
                    }else if(data.trim() == "check_false"){
                        alert("기존 비밀번호가 일치하지 않습니다.");
                    }else{
                        alert("비밀번호 변경 실패");
                    }
                }
            );
        }else{
            alert("새로운 비밀번호와 비밀번호 확인이 일치하지 않습니다.");
        }
    }

    function manager_info_modify_save(){
        var frm=document.master_detail;
        frm.action="/account/manager_info_modify_save";
        frm.submit();
    }
</script>

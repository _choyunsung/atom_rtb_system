<?php 
    $logged_in = $this->session->userdata('logged_in');
    $mem_type = $logged_in["mem_type"];
?>

<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#"><?php echo lang('strAccountMgr')?></a></li>
			<li><a href="#"><?php echo lang('strPersnalInfoMgr')?></a></li>
			<li class="active"><?php echo lang('strLoginHistory')?></li>
		</ol>
	</div>	
	<div class="panel-body">
		<div class="tab-base">
    		<ul class="nav nav-tabs">
                <?php if ($mem_type == "master"){?>
                <li>
                    <a href="/account/account_master_view"><?php echo lang('strMaster')?> <?php echo lang('strInfo')?> </a>
                </li>
                <?php }else{?>
                <li>
                    <a href="/account/account_manager_view"><?php echo lang('strManager')?> <?php echo lang('strInfo')?> </a>
                </li>
                <?php } ?>
                
                <?php if ($mem_type == "master"){?>
                <li>
                    <a href="/account/account_manager_list"><?php echo lang('strManagerList')?></a>
                </li>
                <?php }?>
                <li class="active">
                    <a href="#"><?php echo lang('strLoginHistory')?></a>
                </li>
            </ul>
    		<!--Tabs Content-->
    		<div class="tab-content">
    			<div id="demo-lft-tab-1" class="tab-pane fade active in">
    				<div class="panel-heading over_hidden">
        				<h3 class="page-header wid_50p float_l tit_blit0">로그인 정보</h3>
        				<div id="demo-dp-component" class="camp_info_r">
    						<input type="text" name="daterange" id="daterange" class="range wid_26p" value="<?php echo $fromto_date?>" /> 
            				<span class="iput-group-addon"">
            					<i class="fa fa-calendar fa-lg" onclick="$('#daterange').focus();"></i>
            				</span>
            				<a href="javascript:search();" class="btn btn-primary ma_l10"><?php echo lang('strSearch')?></a>&nbsp;
    					</div>
           			</div>
           			<form name="login_list"  method="post">
       			        <input type="hidden" name="fromto_date" id="fromto_date" value="<?php echo $fromto_date;?>" >
        				<table class="aut_tb" cellpaddig="0" cellspacing="0">
        					<colgroup>
        						<col width="25%">
        						<col width="25%">
        						<col width="25%">
        						<col width="25%">
        					</colgroup>
        					<tr>
        						<th>ID</th>
        						<th><?php echo lang('strAuthority');?></th>
        						<th><?php echo lang('strMemo');?></th>
        						<th class="bd_r_0">로그인 시간</th>																															
        					</tr>
        					<?php foreach ($login_list as $row){?>
            					<tr>
            						<td><span><?php echo $row['mem_id']?></span></td>
            						<td class="txt_center"><?php echo $row['group_nm']?></td>
            						<td><?php echo $row['mem_id']?></td>
            						<td class="txt_center bd_r_0"><?php echo $row['login_dt']?></td>		
            					</tr>
        					<?php }?>																
        				</table>
        				<div class="center">
                            <div class="btn-group float_r">
                                <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                                <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                                    <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box_pa" >
                                    <li><a href="javascript:page_change(10)">10</a></li>
                                    <li><a href="javascript:page_change(25)">25</a></li>
                                    <li><a href="javascript:page_change(50)">50</a></li>
                                    <li><a href="javascript:page_change(100)">100</a></li>
                                </ul>
                            </div>
                            <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                            <?php
                            /*페이징처리*/
                                echo $page_links;
                            /*페이징처리*/
                            ?>
                        </div>	
                    </form>											
    			</div>
    		</div>
		</div>
	</div>
</div>						
<script>
//페이징 스크립트 시작
function page_change(row){
	frm=document.login_list;
	frm.per_page.value=row;
	frm.submit();
}

function paging(number){
    var frm = document.login_list;
    frm.action="/account/account_login_list/" + number;
    frm.submit();
}
//페이징 스크립트 끝

function search(){
	var frm=document.login_list;
	frm.action="/account/account_login_list";
	frm.fromto_date.value=$("#daterange").val();
	frm.submit();
}
</script>			


<div class="modal-header">
    <h4 class="h4_blit ma_b5 ma_t5">
    <?php 
        if($charge_type == "pay_later"){
            echo "후불요청";
        }else if($charge_type == "event"){
            echo "이벤트 캐쉬 지급";    
        }else{
            echo lang('strMakeAPayment');
        }
    ?>
    </h4>
</div>

    <?php 
        if($charge_type == "pay_later"){
    ?>
    
    <div class="panel-bordered panel-success wid_95p m_center ma_t20 ma_b20 pa_t10 pa_b10 cash_dv bg_f5 va_m">
        <h4 class="ma_l10"><?php echo lang('strMEDIA')?> <?php echo lang('strSelect')?>
        <span class="ma_l5">
            <select name="mem_no" id="mem_no" class="form-control-static input-sm">
            	<?php 
            	   foreach($select_mem_list as $mem_list){
            	?>
            		<option value="<?php echo $mem_list['mem_no']?>"><?php echo $mem_list['mem_com_nm']?> | <?php echo $mem_list['mem_id']?> | <?php echo $mem_list['mem_type']?></option>
            	<?php 
            	   }
                ?>
            </select>
            </span>
        </h4>
    </div>
    
    <?php 
        }else if($charge_type == "event"){
    ?>
    
    <div class="panel-bordered panel-success wid_95p m_center ma_t20 ma_b20 pa_t10 pa_b10 cash_dv bg_f5 va_m">
        <h4 class="ma_l10"><?php echo lang('strMEDIA')?> <?php echo lang('strSelect')?>
        <span class="ma_l5">
            <select name="mem_no" id="mem_no" class="form-control-static input-sm">
            	<?php 
            	   foreach($select_mem_list as $mem_list){
            	?>
            		<option value="<?php echo $mem_list['mem_no']?>"><?php echo $mem_list['mem_com_nm']?> | <?php echo $mem_list['mem_id']?> | <?php echo $mem_list['mem_type']?></option>
            	<?php 
            	   }
                ?>
            </select>
            </span>
        </h4>
    </div>

    <?php 
        }else{
    ?>
    <div class="pay_cont">
        <div class="float_l" >
            <ul>
                <li>  
                    <p class="ma_b10">
                        <span class="pay_span"><?php echo lang('strPayment')?> : <span class="color_p" id="input_cash_view">0</span> <span class="point_dgray f-size-12">원</span> 
                    </p>	
                </li>
                <li> 
                    <span class="point_dgray"><?php echo lang('strCurrentBalance')?> : <span id="origin_cash" class="color_t"><?php echo number_format($mem_cash_info['mem_cash']);?></span>원</span>	+
                	<span class="point_dgray"><?php echo lang('strPayment')?> : <span id="input_charge_cash_view" class="color_t">0</span>원</span> =
                	<span class="point_dgray"><?php echo lang('strBalanceAfterPay')?> : <span id="after_charge" class="color_p">0</span>원</span>
                </li>
            </ul>
        </div>
    </div>
    <?php 
        }
    ?>

<div class="clear pa_t10"> 
    <div class="panel-bordered panel-success wid_95p m_center ma_b20 cash_dv" style="background-color:#f5f5f5; margin-bottom:10px !important">
        <div class="hei_35 ma_t5">
            <h3 class="line_20 f-size-12 ma_l8"><?php echo lang('strChoosePayAmount')?></h3>
        </div>
        <div>
            <ul class="tb_ul">
                <li class="tb_li ma_l8">
                    <input type="radio" class="float_l" style="margin-top:8px;" name="sel_amount" value="fix" onchange="sel_charge_money('fix')" checked="checked">
                    <div class="btn-group float_l">
    					<input type="hidden" name="charge_money1" id="charge_money1" >
    					<button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel">
    						<span id="charge_money1_view"> &nbsp; <?php echo lang('strSelect')?>   &nbsp;</span>
    						<i class="dropdown-caret fa fa-caret-down"></i>
    					</button>
    					<ul class="dropdown-menu ul_sel_box">
                            <li><a href="javascript:sel_money(1100000);">1,100,000</a></li>
                            <li><a href="javascript:sel_money(2200000);">2,200,000</a></li>
                            <li><a href="javascript:sel_money(3300000);">3,300,000</a></li>
                            <li><a href="javascript:sel_money(4400000);">4,400,000</a></li>
                            <li><a href="javascript:sel_money(5500000);">5,500,000</a></li>
    					</ul>
				    </div>
                    <input type="radio" name="sel_amount" class="ma_r3 ma_l10" value="self" onchange="sel_charge_money('self')"> 
                    <input type="text"  name="charge_money2" id="charge_money2" value="<?php echo lang('strCustomRange')?>" onkeyup="inputNumberFormat(this); sel_money();" onblur="min_check(this);" disabled> 
                    <span class="ma_l5">원 
                    (<?php echo lang('strMinimumWon')?>) 
                    </span>
                </li>
            </ul>
        </div>
        <p class="ma_t8 point_dgray"> * <?php echo lang('strChargedCash')?> <span id="charge_cash_view">0 </span>원 (<?php echo lang('strSurtaxIsExcluded')?>)</p>
    </div>
    <div class="panel-bordered panel-success wid_95p m_center ma_b20 cash_dv"  style="background-color:#f5f5f5;">
        <div class="ma_t0">
            <h3 class="line_20 f-size-12 ma_l8"><?php echo lang('strPayWith')?></h3>
        </div>
        <div class="ma_l8 ma_b15 pa_t10">
            <?php 
                if($charge_type == "pay_later"){
            ?>
            <input type="radio" name="charge_way" value="5" checked> 후불
            
            <?php 
                }else if($charge_type == "event"){
            ?>
            <input type="radio" name="charge_way" value="6" checked> 이벤트 캐쉬
        	<?php 
                }else{
            ?>
            <input type="radio" name="charge_way" value="1" checked> <?php echo lang('strVirtualBankAccount')?>
            <input class="ma_l20" type="radio" disabled > <?php echo lang('strCreditCard')?>
            <input class="ma_l20" type="radio" disabled > <?php echo lang('strAccountTransfer')?>
            <input class="ma_l20" type="radio" disabled > <?php echo lang('strCellPhone')?>
            <?php 
                }
            ?>
        </div>
    </div>
    <p class="modal-footer txt-right ma_t10 ma_r10">
        <?php 
            if($charge_type == "pay_later" || $charge_type == "event"){
        ?>
         <span class="btn btn-primary" onclick="cash_charge_step2($('select[name=mem_no]').val());"><?php echo lang('strNext') ?></span>   
        <?php 
            }else{
        ?>
        <span class="btn btn-primary" id="cash_charge_btn" onclick="cash_charge_step2()"><?php echo lang('strNext') ?></span>
        <?php 
            }
        ?>
        <span class="btn btn-dark" onclick="$('#cash_modal').modal('hide'); modal_clear();"><?php echo lang('strCancel') ?></span>
    </p>
</div>
<script>
    function sel_charge_money(type){
    	
    	if (type=="self"){
    		$("#charge_money1").val('');
    		$("#charge_money1").attr('disabled',true);
    		$("#charge_money2").attr('disabled',false);
    		$("#charge_money2").val('');
    	}
    	if (type=="fix"){
    		$("#charge_money1").attr('disabled',false);
    		$("#charge_money2").val('<?php echo lang('strCustomRange')?>');
    		$("#charge_money2").attr('disabled',true);
    	}
    	$("#input_cash_view").html(0);
    	$("#charge_cash_view").html(0);
    	$("#input_charge_cash_view").html(0);
    	$("#after_charge").html($("#origin_cash").html());
    	
    }

    function sel_money(sel_money){
    	if ($('input:radio[name="sel_amount"]:checked').val() == "fix"){
    	   var money_tmp = sel_money;
    	   var money = Math.round(money_tmp/1.1);
    	   $("#charge_money1_view").html(comma(money_tmp));
    	   $("#charge_money1").val(comma(money_tmp));
    	}else{
    	   var money_tmp = uncomma($("#charge_money2").val());
    	   var money = Math.round(money_tmp/1.1);
    	}
        var ori_cash = $("#origin_cash").html();
        var sum_cash = Number(uncomma(ori_cash))+Number(money);

        $("#input_cash_view").html(comma(money_tmp));
    	$("#input_charge_cash_view").html(comma(money));
    	$("#charge_cash_view").html(comma(money));
    	$("#after_charge").html(comma(sum_cash));
        
    }
    
    function min_check(obj){
    	if (uncomma(obj.value)<10000){
    		alert("최소 10,000원부터 충전 가능합니다.");
    		obj.value="";
    		obj.focus();
    	}
    }
    function inputNumberFormat(obj) {
        obj.value = comma(uncomma(obj.value));
    }
    
    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
    
    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }
</script>

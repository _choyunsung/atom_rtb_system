<div class="m_wrap ma_b20 pa_t20 pa_b20">
    <div class="panel panel-bordered panel-success wid_100p m_cash_dv bg_f5">
        <div class="m_panel_cash_body">
        <h4 class="ma_t8 line_20 f-size-12"><?php echo lang('strChoosePayAmount')?></h4>
            <ul>
                <li>
                    <input type="radio" class="float_l ma_t8" name="sel_amount" value="fix" onclick="sel_charge_money('fix')" checked>
                    <div class="btn-group float_l wid_90p ma_l10 ma_b10">
    					<input type="hidden" name="charge_money1" id="charge_money1" >
    					<button class="btn btn-default txt-left ma_r5 wid_100p dropdown-toggle" data-toggle="dropdown">
    						<span id="charge_money1_view"> &nbsp; <?php echo lang('strSelect')?>   &nbsp;</span>
    						<i class="dropdown-caret fa float_r pa_r10 pa_t3 fa-caret-down"></i>
    					</button>
    					<ul class="dropdown-menu  wid_p100">
                            <li><a href="javascript:sel_money(1100000);">1,100,000</a></li>
                            <li><a href="javascript:sel_money(2200000);">2,200,000</a></li>
                            <li><a href="javascript:sel_money(3300000);">3,300,000</a></li>
                            <li><a href="javascript:sel_money(4400000);">4,400,000</a></li>
                            <li><a href="javascript:sel_money(5500000);">5,500,000</a></li>
    					</ul>
				    </div>
                </li>
                <li class="clear">
                    <input type="radio" class="clear" name="sel_amount" value="self" onclick="sel_charge_money('self')"> 
                    <input type="text" name="charge_money2" id="charge_money2" class="ma_l8 wid_90p" value="직접입력" onkeyup="inputNumberFormat(this); sel_money();" disabled="">
                    <p>(1,000원 단위 입력)</p> 
                </li>
            </ul>
        </div>
        <p class="point_dgray ma_b10"> * 충전되는 캐쉬  <span id="charge_cash_view">0</span> (부가제 10% 제외하고 충전됨)</p>
    </div>
    <div class="panel panel-bordered-gray ma_t8 pa_t10 pa_b10 bg_f5">
        <div class="m_panel_body ma_b10">
            <h3 class="f-size-12 line_20 ma_b5">결제수단 선택</h3>
            <input type="radio" name="cash_type" checked> <?php echo lang('strVirtualBankAccount')?>
            <input class="ma_l5" type="radio" disabled > <?php echo lang('strCreditCard')?>
            <input class="ma_l5" type="radio" disabled > <?php echo lang('strAccountTransfer')?>
            <input class="ma_l5" type="radio" disabled > <?php echo lang('strCellPhone')?>
        </div>
    </div>
    <div class="panel panel-bordered-gray ma_t8 pa_t10 pa_l10 pa_b10 bg_f5">
	<p class="ma_b10">
	<span class="m_pay_span"><?php echo lang('strPayment')?> : <span class="color_p f-size-1_2em" id="input_cash_view">0</span><span class="point_dgray f-size-12">원</span></span>
	</p>    
    <span class="point_dgray"> <?php echo lang('strCurrentBalance')?> : <span id="origin_cash" class="color_t"><?php echo number_format($mem_cash_info['mem_cash']);?></span>원</span> +
    <span class="point_dgray"><?php echo lang('strPayment')?> : <span id="input_cash_view" class="color_t">0</span>원</span> =
    <span class="point_dgray"><?php echo lang('strBalanceAfterPay')?> :  <span id="after_charge" class="color_p">0</span>원</span></li>
	</div>
	
    <div class="clear ma_t8">
    <div class="panel panel-bordered panel-success wid_100p m_cash_dv bg_f5">
            <div class="m_panel_cash_body pa_l10 pa_r10 pa_b10">
                <h4 class="ma_t8 ma_b10 line_20 f-size-12">입금 계좌정보</h4>
                <table class="m_cash_tb2 f-size-8"border="1">
                    <colgroup>
                        <col width="20%">
                        <col width="80%">
                    </colgroup>
                    <tbody>
                        <tr>
                            <th class="center">입금은행</th>
                            <td><span class="point_blue">기업은행</span></td>
                        </tr>
                        <tr>
                            <th class="center">입금계좌</th>
                            <td><span class="point_blue">123-12-12354</span></td>
                        </tr>
                        <tr>
                            <th class="center">예금주명</th>
                            <td>주식회사 애드오피</td>
                        </tr>
                        <tr>
                            <th class="center">입금기한</th>
                            <td>2015-07-17 00:00:00 까지 입금 요망</td>
                        </tr>
                        <tr>
                            <th class="center">입금자명</th>
                            <td><input type="text" name="charge_nm" id="charge_nm"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <p class="txt-right ma_t15">
            <span class="btn btn-primary" onclick="cash_charge_save();">완료</span>
        </p>
    </div>
</div>
<script>
function sel_charge_money(type){
	if (type == "self"){
		$("#charge_money1").val('');
		$("#charge_money1").attr('disabled',true);
		$("#charge_money2").attr('disabled',false);
		$("#charge_money2").val('');
		
	}
	if (type == "fix"){
		$("#charge_money1").attr('disabled',false);
		$("#charge_money2").val('<?php echo lang('strCustomRange')?>');
		$("#charge_money2").attr('disabled',true);
	}
	$("#input_cash_view").html(0);
	$("#charge_cash_view").html(0);
	$("#after_charge").html($("#origin_cash").html());
}

function sel_money(sel_money){
	if($('input:radio[name="sel_amount"]:checked').val() == "fix"){
	   var money_tmp = sel_money;
	   var money = Math.round(money_tmp/1.1);
	   $("#charge_money1_view").html(comma(money_tmp));
	   $("#charge_money1").val(comma(money_tmp));
	}else{
	   var money_tmp = uncomma($("#charge_money2").val());
	   var money = Math.round(money_tmp/1.1);
	}
    var ori_cash = $("#origin_cash").html();
    var sum_cash = Number(uncomma(ori_cash))+Number(money);
    
	$("#input_cash_view").html(comma(money_tmp));
	$("#charge_cash_view").html(comma(money));
	$("#after_charge").html(comma(sum_cash));
	$("#payment_charge").html(comma(money_tmp));
	
}

function inputNumberFormat(obj) {
    obj.value = comma(uncomma(obj.value));
}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function uncomma(str) {
    str = String(str);
    return str.replace(/[^\d]+/g, '');
}

function cash_charge_save(){
	if ($("#charge_nm").val()==""){
		alert("송금자명을 입력해 주세요.");
	}else{
		charge_money = uncomma($("#input_cash_view").html());//부가세포함금액
		charge_cash = uncomma($("#charge_cash_view").html());//부가세 제외 실 충전금액
		cash_type = 'C';
		charge_st = 'R';
    	url = "/cash/m_cash_charge_save";
    	$.post(url,
            {
                charge_money : charge_money,
                charge_cash : charge_cash,
                cash_type : cash_type,
                charge_st : charge_st,
                charge_nm : $("#charge_nm").val()
            },
            function(data){
                console.log(data);
                if(data.trim()=="false"){
                    alert("실패");
                }else{
                    alert("충전 요청이 완료되었습니다.");
                    location.replace('/dashboard');
                }
            }
        );
	}
}
</script>

<div class="modal" id="modal1" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modal_pay_content"></div>
    </div>
</div>
<!--
<div class="modal" id="modify_modal" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-md">
        <div class="modal-content" id="modify_modal_content"></div>
    </div>
</div>
-->
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAdopWallet')?></a></li>
			<li class="active"><?php echo lang('strCashPayMgr')?></li>
		</ol>
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strCashPayMgr')?></h3>
	</div>
	<div class="panel-body pa_b20">
	
        <div class="tab-base">
        <!--Nav Tabs-->
            <ul class="nav nav-tabs">
                <li>
                    <a href="/cash/cash_pay_management"><?php echo lang('strVirtualBankAccount')?> (<?php echo $kind_cnt['paid_cnt'];?>)</a>
                </li>
                <li>
                    <a href="/cash/cash_pay_later_management">후불 지급 (<?php echo $kind_cnt['no_paid_cnt'];?>)</a>
                </li>
                <li class="active">
                    <a aria-expanded="true">이벤트 캐쉬 지급 (<?php echo $kind_cnt['event_cnt'];?>)</a>
                </li>
            </ul>
        
    	    <div class="clear" style="margin-bottom:20px;"></div>
    
            <form name="cash_list"  method="post">

                <div>

                    <div class="float_r pa_b20">
                        <span class="btn btn-primary" onclick="cash_pay_later_step1();">이벤트 캐쉬 지급</span>
                    </div>
                </div>
          
                <input type="hidden" name="charge_no" id="charge_no" value="">
                <input type="hidden" name="admin_no" id="admin_no" value="<?php echo $mem_no;?>">
                <input type="hidden" name="mem_no" id="mem_no" value="">
                <input type="hidden" name="charge_cash" id="charge_cash" value="">
                <input type="hidden" name="charge_way" id="charge_way" value="5">
                <table id="list" name="list" class="inspection_tb" data-horizontal-width="100%" data-display-length="50" cellpadding="0" cellspacing="0" border="0">
                    <colgroup>
                        <col width="5%">
                        <col width="8%">
                        <col width="6%">
                        <col width="10%">
                        <col width="8%">
                        <col width="8%">
                        <col width="6%">
                        <col width="7%">
                        <col width="7%">
                        <col width="7%">
                    </colgroup>
                    <thead>
                        <tr>                                                                                                                                    
                            <th>No.</th>
                            <th>ID</th>
                            <th><?php echo lang('strUserType');?></th>
                            <th><?php echo lang('strCompany');?></th>
                            <th><?php echo lang('strAuthority');?></th>
                            <th><?php echo lang('strApply');?> <?php echo lang('strCash');?></th>
                            <th><?php echo lang('strApply');?> <?php echo lang('strDate');?></th>
                            <!-- <th><?php echo lang('strStatus');?></th> -->
                       </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($cash_list as $row){
                        ?>
                        <tr>
                            <td class="txt_center pa_l0"><?php echo $row['charge_no']?></td>
                            <td><?php echo $row['mem_id']?></td>
                            <td><?php echo $row['mem_type']?></td>
                            <td><?php echo $row['mem_com_nm']?></td>
                            <td><?php echo $row['group_nm']?></td>
                            <td class="txt-right"><?php echo number_format($row['charge_cash'])?></td>
                            <td class="txt_center pa_l0">
                                <?php echo substr($row['charge_req_dt'], 0, 10);?>
                            </td>
                            <!--
                            <td class="txt_center pa_l0">
                                <?php
                                    if($row['charge_st'] != "D"){
                                ?>
                                <div class="btn-group" style="cursor:pointer;">
                                    <span class="span" data-toggle="dropdown" id="active_sel">
                                    <?php
                                        if($row['charge_st'] == "R"){
                                            echo lang('strApply')." ".lang('strPlanned');
                                        }elseif($row['charge_st'] == "C"){
                                            echo lang('strApply')." (입금)";
                                        }elseif($row['charge_st'] == "E"){
                                            echo lang('strApply')." (미입금)";
                                        }elseif($row['charge_st'] == "D"){
                                            echo lang('strPayCancel');
                                        }
                                    ?>
                                        <i class="dropdown-caret fa fa-caret-down float_r ma_t5 ma_r5 cursor"></i>
                                    </span>
                                    <ul class="dropdown-menu ul_sel_box">
                                        <?php
                                            if($row['charge_st'] == "R"){
                                        ?>
                                        <li>
                                            <a href="javascript:cash_pay_form(<?php echo $row['charge_no']?>, <?php echo $mem_no?>, 'C');">
                                                <?php echo lang('strApply');?> (입금)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:cash_pay_form(<?php echo $row['charge_no']?>, <?php echo $mem_no?>, 'E');">
                                                <?php echo lang('strApply');?> (미입금)
                                            </a>
                                        </li>
                                        
                                        <?php
                                            }
                                        ?>
                                        <?php
                                            if($row['charge_st'] == "E"){
                                        ?>
                                        <li>
                                            <a href="javascript:cash_pay_form(<?php echo $row['charge_no']?>, <?php echo $mem_no?>, 'C')">
                                                <?php echo lang('strApply')?> (입금)
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:cash_revoke(<?php echo $row['charge_no']?>, <?php echo $mem_no?>, <?php echo $row['mem_no']?>, <?php echo $row['charge_cash']?>)">
                                                <?php echo lang('strPayCancel')?>
                                            </a>
                                        </li>
                                        <?php
                                            }
                                        ?>
                                        
                                    	<?php
                                            if($row['charge_st'] == "C"){
                                        ?>
                                        <li>
                                            <a href="javascript:cash_revoke(<?php echo $row['charge_no']?>, <?php echo $mem_no?>, <?php echo $row['mem_no']?>, <?php echo $row['charge_cash']?>)">
                                                <?php echo lang('strPayCancel')?>
                                            </a>
                                        </li>
                                        <?php
                                            }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                                    }else if($row['charge_st'] == "D"){
                                ?>
                                <div class="btn-group">
                                    <span class="span">
                                       <?php echo lang('strPayCancel')?>
                                     </span>
                                </div>
                                <?php
                                    }
                                ?>
                            </td>
                            -->
                        </tr>
                       <?php
                            }
                        ?>
                    </tbody>
                </table>
    
                <div class="center">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box" >
                            <li><a href="javascript:page_change(100)">100</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(10)">10</a></li>
                        </ul>
                    </div>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div> 
            </form>
        </div>
   </div>
</div>
<script type="text/javascript">

    function cash_pay_later_step1(){
        $.ajax({
            type:"POST",
            url:"/cash/cash_charge_step1/",
            data : {mem_no: <?php echo $this->session->userdata('mem_no');?>, charge_type : 'event'},
            success: function (data){
                $('#cash_modal').modal('show');
                $('#modal_content').empty();
                $('#modal_content').append(data);
            }
        });
    }

    //페이징 스크립트 시작
    function page_change(row){
        frm = document.cash_list;
        frm.per_page.value = row;
        frm.submit();
    }

    function paging(number){
        var frm = document.cash_list;
        frm.action = "/cash/cash_pay_management/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝

    $(document).ready(function() {
        /*
        $('#list').dataTable( {
            "dom": '<"toolbar"f>rt'
        } );
        */
        $("div.toolbar").append($("#all_btn_gp").html());
    } );


    function cash_revoke(charge_no, admin_no, mem_no, charge_cash){

        if(confirm("Cancel?")){
           var frm = document.cash_list;
            frm.action="/cash/cash_revoke";
            frm.charge_no.value = charge_no;
            frm.admin_no.value = admin_no;
            frm.mem_no.value = mem_no;
            frm.charge_cash.value = charge_cash;
            frm.charge_way.value = "6";
            frm.submit();
        }else{
            //alert("취소되었습니다");
        }
    }

    $("#modal1").on('hidden.bs.modal', function () {
        $('#modal_pay_content').html("");
        $(this).data('bs.modal', null);
    });

    function cash_pay_form(charge_no, admin_no, charge_st){
        $.ajax({
            type:"POST",
            url:"/cash/cash_pay_form",
            data : {charge_no : charge_no, admin_no : admin_no, charge_st : charge_st, charge_way : '6'},
            success: function (data){
                $('#modal_pay_content').append(data);
                $("#modal1").modal("show");
            }
        });
    }
<!--
    function cash_modify_form(charge_no){
        $.ajax({
            type:"POST",
            url:"/cash/cash_modify_form",
            data : {charge_no : charge_no},
            success: function (data){
                $('#modify_modal_content').append(data);
                $("#modify_modal").modal("show");
            }
        });
    }
-->
    function charge_confirm_dt_modify(charge_no){
        if($("#charge_confirm_dt_view_"+charge_no).css('display') == "none"){
            $("#charge_confirm_dt_view_"+charge_no).show();
            $("#charge_confirm_dt_modify_"+charge_no).hide();
        }else{
            $("#charge_confirm_dt_view_"+charge_no).hide();
            $("#charge_confirm_dt_modify_"+charge_no).show();
        }
    }

    function charge_confirm_dt_save(charge_no){
        var charge_confirm_dt = $("#charge_confirm_dt_"+charge_no).val();
        var url = '/cash/cash_pay_modify';
        $.post(url,
            {
                charge_no : charge_no,
                charge_confirm_dt : charge_confirm_dt
            },
            function(data){
                if(data.trim() == "ok"){
                    alert("Changed Deposit Date");
                    $("#charge_confirm_dt_view_"+charge_no).show();
                    $("#charge_confirm_dt_value_"+charge_no).html(charge_confirm_dt);
                    $("#charge_confirm_dt_modify_"+charge_no).hide();
                }else{
                    alert("Input Deposit Date Please.");
                    $("#charge_confirm_dt_"+charge_no).focus();
                }
            }
        );
    }

    $(function() {
        jQuery('.date').datetimepicker({
            format:'Y-m-d H:i'
        });
    });

</script>
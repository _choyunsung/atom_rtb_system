<script>
$(document).ready(function() {
    var refund_type = "<?php echo $refund_type?>";
    if (refund_type == "1"){
    	$("#refund_avail").prop('checked', true);
    	$("#cash_table1").show('');
    	$("#cash_table2").hide('');
    	$("#bank_table1").show('');
    	$("#bank_table2").hide('');
    }else{
    	$("#refund_reject").prop('checked', true);
    	$("#cash_table1").hide('');
    	$("#cash_table2").show('');
    	$("#bank_table1").hide('');
    	$("#bank_table2").show('');
    }
});
</script>

<div class="panel float_r wid_970 min_height">
    <div class="history_box">
    </div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strRefundRun')?> </h3>
    </div>
    <div class="panel-body">
        <form name="refund_request_view" id="refund_request_view" method="post">
            <input type="hidden" name=request_no id="request_no" value="<?php echo $request_view['request_no']?>">
            <input type="hidden" name="request_mem_no" id="request_mem_no" value="<?php echo $request_view['request_mem_no']?>">
            <input type="hidden" name="type" id="type">
            <div class="float_l wid_100p">
            <!-- 환불여부 테이블 -->
                <table class="aut_tb wid_33p float_l hei_300">
                    <colgroup>
                        <col width="35%">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th colspan="2"><?php echo lang('strRefundStatus')?></th>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strCompanyName')?></td>
                        <td>
                            <?php echo $request_view['mem_com_nm'];?>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundRequestDate')?></td>
                        <td>
                            <?php echo $request_view['date_ymd'];?>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundReason')?></td>
                        <td>    
                            <input type="hidden" name="refund_cont" id="refund_cont">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    &nbsp;<span id="refund_cont_view"><?php echo lang('strSelect')?></span>&nbsp; 
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <li>
                                        <a href="#" onclick="change_refund_reason('<?php echo lang('strRefundReason1');?>','1')">
                                            <?php echo lang('strRefundReason1');?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="change_refund_reason('<?php echo lang('strRefundReason2');?>','2')">
                                            <?php echo lang('strRefundReason2');?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="change_refund_reason('<?php echo lang('strRefundReason3');?>','3')">
                                            <?php echo lang('strRefundReason3');?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="change_refund_reason('<?php echo lang('strRefundReason4');?>','4')">
                                            <?php echo lang('strRefundReason4');?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundStatus')?></td>
                        <td>
                            <input type="radio" name="refund_status" id="refund_avail" onchange="sel_refund_status('1');" value="1"> <?php echo lang('strRefundAvail')?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="radio" name="refund_status" id="refund_reject" onchange="sel_refund_status('2');" value="2"> <?php echo lang('strRefundReject')?>
                        </td>
                    </tr>
                </table>
                <!-- 환불지급액 테이블 -->
                <table class="aut_tb wid_33p float_l hei_300" id="cash_table1">
                    <colgroup>
                        <col width="35%">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th colspan="2"><?php echo lang('strRefundAmount')?></th>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strMountainCashBalance')?></td>
                        <td>
                            <?php echo number_format($request_view['mem_cash']);?>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundAmount')?></td>
                        <td>
                            <input type="text" name="refund_amount" id="refund_amount" onkeyup="auto_cal()">
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundGap')?></td>
                        <td>
                            <span id="gap_amount"></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                </table>
                <table class="aut_tb wid_33p float_l hei_300" id="cash_table2">
                    <colgroup>
                        <col width="35%">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th colspan="2"><?php echo lang('strRefundAmount')?></th>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strMountainCashBalance')?></td>
                        <td>
                            <?php echo number_format($request_view['mem_cash']);?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                    </tr>
                </table>
                <!-- 환불여부 테이블 -->
                <table class="aut_tb wid_33p float_l hei_300" id="bank_table1">
                    <colgroup>
                        <col width="35%">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th colspan="2"><?php echo lang('strRefundAmount')?></th>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strPayWay')?></td>
                        <td>
                            <?php echo lang('strVirtualBankAccount');?>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundWay')?></td>
                        <td>
                            <?php echo lang('strVirtualBankAccount');?>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundBank')?></td>
                        <td>
                            <input type="hidden" name="refund_bank" id="refund_bank">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    &nbsp;<span id="refund_bank_view"><?php echo lang('strSelect')?></span>&nbsp; 
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php foreach ($sel_bank_code as $sel){?>
                                    <li>
                                        <a href="#" onclick="change_refund_bank('<?php echo $sel['code_key']?>','<?php echo $sel['code_desc']?>')">
                                            <?php echo $sel['code_desc'];?>
                                        </a>
                                    </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundBankNumber')?></td>
                        <td>
                            <input type="text" name="refund_bank_no">
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strAccountHolder')?></td>
                        <td>
                            <input type="text" name="refund_bank_no_nm">
                        </td>
                    </tr>
                </table>
                <table class="aut_tb wid_33p float_l hei_300" id="bank_table2">
                    <colgroup>
                        <col width="35%">
                        <col width="*">
                    </colgroup>
                    <tr>
                        <th colspan="2"><?php echo lang('strRefundAmount')?></th>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('strRefundRejectReason')?></td>
                        <td>
                            <input type="hidden" name="refund_reject_type" id="refund_reject_type">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    &nbsp;
                                    <span id="refund_reject_type_view">
                                        <?php 
                                            if ($request_view['refund_reject_type'] == "1"){
                                                echo lang('strRefundRejectReason1');
                                            }else{
                                                echo lang('strRefundRejectReason2');
                                            }
                                        ?>
                                        <?php echo lang('strSelect')?>
                                    </span>
                                    &nbsp; 
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <li>
                                        <a href="#" onclick="change_refund_reject_reason('1', '<?php echo lang('strRefundRejectReason1')?>')">
                                            <?php echo lang('strRefundRejectReason1')?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="change_refund_reject_reason('2', '<?php echo lang('strRefundRejectReason2')?>')">
                                            <?php echo lang('strRefundRejectReason2')?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>- <?php echo lang('')?>상세내용</td>
                        <td rowspan="4">
                            <textarea rows="9" cols="30" name="refund_reject_cont"><?php echo $request_view['refund_reject_cont']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
         	</div>
            </form>
            <div class=" ma_b3" id="all_btn_gp">
                <a href="/cash/refund_request_list"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strCancel')?></span></a>
                <a href="javascript:refund_done('3');"><span class="btn btn-primary ma_l5 float_r"><?php echo lang('strRefundDone')?></span></a>
                <a href="javascript:refund_done('1');"><span class="btn btn-warning ma_l5 float_r"><?php echo lang('strTmpSave')?></span></a>
            </div>
            
		</div>
    </div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.notice_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.notice_list;
        frm.action="/board/notice_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    function auto_cal(){
        var refund_amount = uncomma($("#refund_amount").val());
        var mem_cash = Math.round(<?php echo $request_view['mem_cash']?>);
        if (Number(mem_cash) > Number(refund_amount)){
            var gap_money = Number(mem_cash) - Number(refund_amount);
            $("#gap_amount").html(comma(gap_money));
            $("#refund_amount").val(comma(refund_amount));
        }else{
            alert("환불금액이 잔액보다 많습니다. 확인해 주세요");
            $("#gap_amount").html('');
            $("#refund_amount").val('');
        }
    }
    
    
    function comma(str) {
        str = String(str);
        return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
    
    function uncomma(str) {
        str = String(str);
        return str.replace(/[^\d]+/g, '');
    }

    function sel_question_type(no,type){
        $("#question_fl").val(no);
        $("#question_fl_view").html(type);
    }

    function sel_refund_status(status){
    	if (status == "1"){
        	$("#cash_table1").show('');
        	$("#cash_table2").hide('');
        	$("#bank_table1").show('');
        	$("#bank_table2").hide('');
        }else{
        	$("#cash_table1").hide('');
        	$("#cash_table2").show('');
        	$("#bank_table1").hide('');
        	$("#bank_table2").show('');
        }
    }

    function change_refund_reason(view, no){
        $("#refund_cont_view").html(view);
        $("#refund_cont").val(no);
    }

    function change_refund_bank(no, view){
        $("#refund_bank_view").html(view);
        $("#refund_bank").val(no);
    }
    
    function refund_done(type){
        var frm = document.refund_request_view;
        frm.action = '/cash/refund_request_save';
        frm.type.value = type;
        frm.refund_amount.value = uncomma(frm.refund_amount.value); 
        frm.submit();
    }

    function change_refund_reject_reason(type, view){
        $("#refund_reject_type").val(type);
        $("#refund_reject_type_view").html(view);
    }
        
</script>
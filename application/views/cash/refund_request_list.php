<?php 
    
?>
<script type="text/javascript">
$(document).ready(function() {
});
	function search(){
		var frm = document.question_list;
		frm.action="/board/question_list";
		frm.submit();
	}
</script>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
    </div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strQuestionList')?> </h3>
    </div>
    <div class="panel-body">
    	<div class="camp_rept">
    		<div class="camp2 cursor2">
    			<div class="camp_div2b txt_center" onclick="select_status('all');">
    				<p class="camp_tit"><?php echo lang('strAll')?></p> 
    				<span class="camp_con"><?php echo $request_summary['all_cnt'];?></span>
    			</div>
    			<div class="camp_div2b txt_center" onclick="select_status('today');">
    				<p class="camp_tit"><?php echo lang('strTodayRefundRequest')?></p>
    				<span class="camp_con"><?php echo $request_summary['today_cnt'];?></span>
    			</div>
    			<div class="camp_div2b txt_center" onclick="select_status('soon');">
    				<p class="camp_tit"><?php echo lang('strRefundSoon')?></p>
    				<span class="camp_con"><?php echo $request_summary['soon_cnt'];?></span>
    			</div>		    				
    			<div class="camp_div2b txt_center" onclick="select_status('reject');">
    				<p class="camp_tit"><?php echo lang('strRefundReject')?></p>
    				<span class="camp_con"><?php echo $request_summary['reject_cnt'];?></span>
    			</div>		    				
    			<div class="camp_div2b txt_center" style="border-right:0px" onclick="select_status('done');">
    				<p class="camp_tit"><?php echo lang('strRefundDone')?></p>
    				<span class="camp_con"><?php echo $request_summary['done_cnt'];?></span>
    			</div>		    					
    		</div>	
    	</div>
	</div>
    <div class="panel-body">
        <form name="refund_request_list" id="refund_request_list" method="post">
            <input type="hidden" name="request_no" id="request_no">
            <input type="hidden" name="type" id="type">
            <input type="hidden" name="cond_refund_st" id="cond_refund_st">
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="15%">
                        <col width="35%">
                        <col width="15%">
                        <col width="35%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strAuthority')?></th>
                        <td>
                            <input type="checkbox" name="master_yn" <?php if ($cond['master_yn'] == "on"){echo "checked";}?>> <?php echo lang('strMaster')?>&nbsp;&nbsp;
                            <input type="checkbox" name="manager_yn" <?php if ($cond['manager_yn'] == "on"){echo "checked";}?>> <?php echo lang('strManager')?>&nbsp;&nbsp;
                        </td>
                        <th><?php echo lang('strCompanyName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_com_nm" value="<?php echo $cond['mem_nm'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strID')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_id" value="<?php echo $cond['mem_id'];?>">
                        </td>
                        <th><?php echo lang('strRefundRequestDate')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="fromto_date" id="daterange" value="<?php echo $cond['fromto_date']?>" />
            				<span class="iput-group-addon">
            					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
            				</span>
                        </td>
                    </tr>
                </table>
                <div class="float_r ma_b3" id="all_btn_gp">
                    <span class="btn btn-primary float_r" onClick ="search();"><?php echo lang('strSearch')?></span>
                </div>
         	</div>
            <div class="pa_b20">
                <input type="hidden" name="notice_no" >
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
    			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th><?php echo lang('strRefundRequestDate')?></th>
                            <th><?php echo lang('strID')?></th>
                            <th><?php echo lang('strAuthority')?></th>
                            <th><?php echo lang('strCompanyName')?></th>
                            <th><?php echo lang('strType')?></th>
                            <th><?php echo lang('strRefundStatus')?></th>
                            <th><?php echo lang('strRefundDate')?></th>
                            <th><?php echo lang('strRefundAmount')?></th>
                            <th><?php echo lang('strRefundWay')?></th>
                            <th><?php echo lang('strAnswerName')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($request_list)){ ?>
                            <?php
                                $no = 1;
                                foreach ($request_list as $row){
                            ?>
                                <tr>
                                    <td class="txt_center"><?php echo $no;?></td>
                                    <td>
                                        <?php echo $row['date_ymd']?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['mem_id']?>
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['mem_type'] == 'manager'){
                                                echo lang('strManager');
                                            }elseif($row['mem_type'] == 'master'){
                                                echo lang('strMaster');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['mem_com_nm']?>
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['role'] == "adver"){
                                                echo lang('strAdvertiser');
                                            }elseif ($row['role'] == "agency"){
                                                echo lang('strAgency');
                                            }elseif ($row['role'] == "ind"){
                                                echo lang('strIndividual');
                                            }elseif ($row['role'] == "rep"){
                                                echo lang('strLab');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php if ($row['refund_st'] == "1"){?>
                                        <div class="btn-group">
                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                                &nbsp;
                                                    <?php 
                                                        if ($row['refund_st'] == "1"){
                                                            echo lang('strRefundSoon');
                                                        }elseif($row['refund_st'] == "2"){
                                                            echo lang('strRefundReject');
                                                        }elseif($row['refund_st'] == "3"){
                                                            echo lang('strRefundDone');
                                                        }
                                                    ?>  
                                                &nbsp; 
                                                <i class="dropdown-caret fa fa-caret-down"></i>
                                            </button>
                                            <ul class="dropdown-menu ul_sel_box">
                                                <li>
                                                    <a href="#" onclick="change_refund_status('<?php echo $row['request_no']?>','1')">
                                                        <?php echo lang('strRefundSoon');?>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="change_refund_status('<?php echo $row['request_no']?>','2')">
                                                        <?php echo lang('strRefundReject');?>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" onclick="change_refund_status('<?php echo $row['request_no']?>','3')">
                                                        <?php echo lang('strRefundDone');?>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php }else{?>
                                        <?php 
                                            if ($row['refund_st'] == "2"){
                                                echo lang('strRefundReject');
                                            }elseif($row['refund_st'] == "3"){
                                                echo lang('strRefundDone');
                                            }}
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['refund_ymd']?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo number_format($row['refund_amount'])?>
                                    </td>
                                    <td class="txt_center">
                                        <?php 
                                            if ($row['refund_type'] == '1'){
                                                echo lang('strVirtualBankAccount');
                                            }
                                        ?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['contact_nm']?>
                                    </td>
                               </tr>
                           <?php $no++;}?> 
                       <?php }?>
                   </tbody>
                </table>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
		</div>
    </div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.notice_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.notice_list;
        frm.action="/board/notice_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    
    function search(){
    	var frm = document.refund_request_list;
    	frm.action = '/cash/refund_request_list';
    	frm.submit();
        	
        	
    }
    function sel_question_type(no,type){
        $("#question_fl").val(no);
        $("#question_fl_view").html(type);
    }

    function question_detail_view(question_no){
    	var frm = document.question_list;
    	frm.question_no.value=question_no;
        frm.action="/board/question_detail_view/";
        frm.submit();
    }   

    function change_refund_status(request_no, type){
        var frm = document.refund_request_list;
        frm.request_no.value = request_no;
        frm.type.value =type;
        frm.action = "/cash/refund_request_view";
        frm.submit();        
    }

    function select_status(type){
        var frm = document.refund_request_list;
        frm.mem_id.value = "";
        frm.mem_com_nm.value = "";
        frm.fromto_date.value = "";
        $("#manager_yn").prop("checked", false);
        $("#master_yn").prop("checked", false);
        $("#answer_done").prop("checked", false);
        
        if (type == "all"){
            location.replace('/cash/refund_request_list');
        }else if (type == "today"){
            $("#daterange").val('<?php echo date('Y-m-d') ?>'+ ' ~ ' + '<?php echo date('Y-m-d')?>');
        }else if (type == "soon"){
            $("#cond_refund_st").val("1");
        }else if (type == "reject"){
        	$("#cond_refund_st").val("2");
        }else if (type == "done"){
        	$("#cond_refund_st").val("3");
        }
        search();
    }
            
</script>
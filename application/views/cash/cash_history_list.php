<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strPaymentMgr')?></a></li>
			<li class="active"><?php echo lang('strCash'). lang('strManagement')?></li>
		</ol>
	</div>
	<div class="panel-body ma_t20">
		<h4 class="ma_l10 ma_b10">
		</h4>
		<div class="cash_balance">
			<div class="tit">
				<span class="float_l"><?php echo lang('strCurrentBalance')?> :&nbsp;</span> 
				<span class="color_p f-size-14 float_l"><?php echo number_format($member_info['mem_cash'] + $member_info['mem_event_cash']);?></span>
				<span class="f-size-14 float_l">원</span>
			</div>
			<div class="cash_b_mid">
				<p>
    				<?php if ($member_info['mem_event_cash'] != "0"){?>
    					(<?php echo lang('strAtomCash')?> : <span class="color_t"><?php echo number_format($member_info['mem_cash'])?></span>원 +
    					 <?php echo lang('strEvent')?> <?php echo lang('strCash')?> : <span class="color_t"><?php echo number_format($member_info['mem_event_cash'])?></span>원)
					<?php }?>
				</p>
			</div>
			<div class="cash_b_r">
				<span class="btn btn-primary" onclick="cash_charge_step1();"><?php echo lang('strMakeAPayment')?></span>
			</div>
		</div>
		<div class="cash_balance_view">
			<div class="left_cont">
				<table class="left_cont_tb">
					<colgroup>
						<col width="30%">
						<col width="*">
					</colgroup>
					<tr>
						<th><?php echo lang('strRunningOutDate')?></th>
						<td>
                            <?php
                                if($report_info[1]['use_cash'] > 0){
                                    $day = round(($member_info['mem_cash'] + $member_info['mem_event_cash'] )/($report_info[1]['use_cash']),0);
                                }else{
                                    $day = 0;
                                }
                                echo $day;
                            ?> 일
						</td>
					</tr>
					<?php foreach ($report_info as $row){?>
    					<tr>
    						<th><?php echo lang($row['kind']);?></th>
    						<td><?php echo number_format($row['use_cash']);?> 원</td>
    					</tr>
					<?php }?>
				</table>
				<div class="bar"></div>
			</div>
			<div class="right_cont">
				<div style="float: left;" id="cash_info_chart" >
					
				</div>
			</div>
		</div>
	</div>
	<div class="panel-body ma_t10">
		<div class="tab-base">
			<!--Nav Tabs-->
			<ul class="nav nav-tabs">
				<li><a href="/cash/cash_charge_list"><?php echo lang('strPaymentHistory')?></a></li>
				<li class="active"><a href="/cash/cash_history_list"><?php echo lang('strUsedHistory')?></a></li>
				<li><a href="/cash/cash_refund_list"><?php echo lang('strRefundHistory')?></a></li>
			</ul>
		</div>
		<form name="cash_history_list" method="post">
			<input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
			<input type="hidden" name="kind" value="<?php echo $kind;?>">
			<input type="hidden" name="range" value="<?php echo $range;?>">
			<div class="clear hei_35" >
	    		<div class="wid_40p float_r">
	    		    <div class="float_r ma_l10">
	        		    <a href="javascript:document.cash_history_list.submit()" class="btn btn-primary"><?php echo lang('strSearch')?></a>&nbsp;
	        		    <a href="javascript:excel_down()" class="btn btn-primary"><?php echo lang('strDownload')?></a>&nbsp;
	    		    </div>
	    		    
	    		    <div id="demo_dp_component">
	    				<input type="text" name="fromto_date" id="daterange" class="range wid_55p" value="<?php echo $fromto_date?>" /> 
	    				<span class="iput-group-addon">
	    					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
	    				</span>
	    			</div>
	    		</div>
    	        <div class="charge_days">
        			<div class="btn-group">
        				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                            <span id="cash_history_kind">
                                &nbsp;
                                <?php 
                                    if ($kind == ''){
                                        echo lang('strAll');
                                    }elseif ($kind == 'C'){
                                        echo lang('strAtomCash');
                                    }else{
                                        echo lang('strEvent')." ".lang('strCash');
                                    }
                                ?>  
                                &nbsp;
                            </span> 
                            <i class="dropdown-caret fa fa-caret-down"></i>
        				</button>
        				<ul class="dropdown-menu ul_sel_box">
        				    <li>
                                <a href="javascript:void(0);" onclick="cash_history_list_kind('all')">
                                    <?php echo lang('strAll');?>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="cash_history_list_kind('C')">
                                    <?php echo lang('strAtomCash');?>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="cash_history_list_kind('E')">
                                    <?php echo lang('strEvent');?> <?php echo lang('strCash');?>
                                </a>
                            </li>
                        </ul>
        			</div>
        			<ul class="days_btn">
    	    			<li><a href="javascript:sel_fromto_date('7')" class="btn btn-default"><?php echo lang('strLast7')?></a></li>
    	    			<li><a href="javascript:sel_fromto_date('30')" class="btn btn-default"><?php echo lang('strLast30')?></a></li>
    	    			<li><a href="javascript:sel_fromto_date('60')" class="btn btn-default"><?php echo lang('strLast60')?></a></li>
    	    			<li><a href="javascript:sel_fromto_date('90')" class="btn btn-default"><?php echo lang('strLast90')?></a></li>
        			</ul>
    			</div>
    		</div>
			<table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
				data-horizontal-width="100%" data-display-length="100"
				cellpadding="0" cellspacing="0" border="0">
				<colgroup>
					<col width="5%">
					<col width="20%">
					<col width="20%">
					<col width="20%">
					<col width="20%">
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th><?php echo lang('strCashDate')?></th>
						<th><?php echo lang('strUsedMountainCash')?></th>
						<th><?php echo lang('strUsedEventCash')?></th>
						<th><?php echo lang('strTotalUsedCash')?></th>
					</tr>
				</thead>
				<tbody>
				    <?php
				        if(isset($cash_history_list)){
                            $sum_mo_cash=0;
                            $sum_event_cash=0;
                            $sum_all_cash=0;
                            foreach ($cash_history_list as $row){
                                $sum_mo_cash+=$row['mo_cash'];
                                $sum_event_cash+=$row['event_cash'];
                            }
                            $sum_all_cash=$sum_mo_cash+$sum_event_cash;
    				        $no=0; 
    				        foreach ($cash_history_list as $row){
                                $no++;
                    ?>
				        <tr>
				            <td class="txt_center pa_l0"><?php echo $no;?></td>
				            <td class="txt_center pa_l0"><?php echo $row['cash_dt'];?></td>
				            <td class="txt-right">￦ <?php echo number_format($row['mo_cash']);?></td>
				            <td class="txt-right">￦ <?php echo number_format($row['event_cash']);?></td>
				            <td class="txt-right">￦ <?php echo number_format($row['mo_cash']+$row['event_cash']);?></td>
			            </tr>
		            <?php }?>
				</tbody>
				<tfoot>
				<tr>
	               <td class="txt_center pa_l0" colspan="2"><?php echo lang('strSumWithinDateRange')?></td>
	               <td class="txt-right">￦ <?php echo number_format($sum_mo_cash);?></td>
	               <td class="txt-right">￦ <?php echo number_format($sum_event_cash);?></td>
	               <td class="txt-right">￦ <?php echo number_format($sum_all_cash);?></td>
                </tr>
                <?php }?>
                </tfoot>
			</table>
			<div class="center" id="all_btn_gp">
				<div class="center hei_35">
	                <div class="btn-group float_r">
	                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
	                    <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
	                        <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
	                        <i class="dropdown-caret fa fa-caret-down"></i>
	                    </button>
	                    <ul class="dropdown-menu ul_sel_box_pa" >
	                        <li><a href="javascript:page_change(10)">10</a></li>
	                        <li><a href="javascript:page_change(25)">25</a></li>
	                        <li><a href="javascript:page_change(50)">50</a></li>
	                        <li><a href="javascript:page_change(100)">100</a></li>
	                    </ul>
	                </div>
	                <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
	                <?php
	                /*페이징처리*/
	                    echo $page_links;
	                /*페이징처리*/
	                ?>
	            </div>
            </div>
		</form>
	</div>
</div>



<script type="text/javascript">
//페이징 스크립트 시작
function page_change(row){
	frm=document.cash_history_list;
	frm.per_page.value=row;
	frm.submit();
}

function paging(number){
    var frm = document.cash_history_list;
    frm.action="/cash/cash_history_list/" + number;
    frm.submit();
}
//페이징 스크립트 끝

function cash_history_list_kind(kind){
	var frm = document.cash_history_list;
	frm.action="/cash/cash_history_list";
	if (kind == "all"){
	   frm.kind.value="";
	}else{
	   frm.kind.value=kind;
	}
	frm.submit();
}

function sel_fromto_date(range){
	var frm = document.cash_history_list;
	frm.action="/cash/cash_history_list";
    frm.range.value=range;
	frm.submit();
}

function get_chart(){
    $.ajax({
        url : "/cash/cash_info_chart/",
        dataType : "html",
        type : "post",  // post 또는 get
        data : { mem_no : <?php echo $mem_no;?> },   // 호출할 url 에 있는 페이지로 넘길 파라메터
        success : function(result){
            $("#cash_info_chart").html(result);
        }
    });
}

function excel_down(){
	var cnt = '<?php echo count($cash_history_list);?>';
	if (cnt > 0){
    	var frm = document.cash_history_list;
        frm.action = '/excel/cash_history_excel_down';
        frm.submit();
	}else{
		alert("다운로드 데이터가 없습니다.");
	}
}
$(document).ready(function() {
	get_chart();
	
} );
</script>
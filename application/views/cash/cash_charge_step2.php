<div class="modal-header">
    <h4 class="h4_blit ma_b5 ma_t5"">
    	<?php 
    	if($charge_way == 5){
            echo "후불요청";
    	}else if($charge_way == 6){
            echo "이벤트 캐쉬 지급";
    	}else{
            echo lang('strMakeAPayment');
    	}
    	?>
	</h4>
</div>

	<?php 
	   if($charge_way == 5 || $charge_way == 6){
	?>
	<?php 
	   }else{
	?>
	<div class="pay_cont">
    	<div class="float_l">
    		<ul>
    			<li>
    				<p class="ma_b10">
    					<span class="pay_span"><?php echo lang('strPayment')?> : <span class="color_p"><?php echo number_format($charge_money)?></span> <span
    						class="point_dgray f-size-12">원</span>
    					</span>
    				</p>
    			</li>
    			<li><span class="point_dgray"> <?php echo lang('strCurrentBalance')?> : <?php echo number_format($origin_cash)?>원</span> +
    				<span class="point_dgray"><?php echo lang('strPayment')?> : <?php echo number_format($charge_money)?>원</span> =
    				<span class="point_dgray"><?php echo lang('strBalanceAfterPay')?> : <span class="color_p"><?php echo number_format($after_charge)?> </span>원</span>
    			</li>
    		</ul>
    	</div>
	</div>
	<?php 
	   }
	?>

	<?php 
	   if($charge_way == 6){
	?>
    <div class="clear pa_t20">
		<div class="hei_35 ma_t0 ma_l13">
			<h3 class="line_20 f-size-12 ma_l8">이벤트 캐쉬 지급 내용</h3>
		</div>
    	<div class="panel panel-bordered panel-success wid_95p m_center cash_dv" style="background-color:#f5f5f5;">
    		<div class="">
    			<table class="cash_tb2">
    				<colgroup>
    					<col width="25%">
    					<col width="75%">
    				</colgroup>
    				<tr>
    					<th class="center">이벤트 캐쉬</th>
    					<td><span class="point_blue"><?php echo number_format($charge_cash)?></span></td>
    				</tr>
    			</table>
    		</div>
    	</div>
        <p class=" modal-footer txt-right ma_t10 ma_r10">
    		<span class="btn btn-dark close" data-dismiss="modal" ><?php echo lang('strCancel')?>&nbsp;</span> 
    		<span class="btn btn-primary ma_r7" onclick="cash_charge_save()">&nbsp;<?php echo lang('strNext')?></span>
    	</p>
    </div>
	<?php 
	   }else{
	?>
    <div class="clear pa_t20">
		<div class="hei_35 ma_t0 ma_l13">
			<h3 class="line_20 f-size-12 ma_l8"><?php echo lang('strAccountInfomation')?></h3>
		</div>
    	<div class="panel panel-bordered panel-success wid_95p m_center cash_dv" style="background-color:#f5f5f5;">
    		<div class="">
    			<table class="cash_tb2">
    				<colgroup>
    					<col width="25%">
    					<col width="75%">
    				</colgroup>
    				<tr>
    					<th class="center"><?php echo lang('strDepositBank')?></th>
    					<td><span class="point_blue">신한은행</span></td>
    				</tr>
    				<tr>
    					<th class="center"><?php echo lang('strDepositAccount')?></th>
    					<td><span class="point_blue">140 &ndash; 011 &ndash; 105213</span></td>
    				</tr>
    				<tr>
    					<th class="center"><?php echo lang('strAccountHolder')?></th>
    					<td>주식회사애드오피 이원섭</td>
    				</tr>
    				<?php 
    				    if($charge_way == 1){
    				?>
    				<tr>
    					<th class="center"><?php echo lang('strDeadline')?></th>
    					<td><?php echo $charge_limit_dt?> 00:00:00 까지 입금 요망</td>
    				</tr>
    				<?php 
    				    }
    				?>
    				<tr>
    					<th class="center"><?php echo lang('strRemitter')?></th>
    					<td><input type="text" name="charge_nm" id="charge_nm"></td>
    				</tr>
    			</table>
    		</div>
    	</div>
        <p class=" modal-footer txt-right ma_t10 ma_r10">
    		<span class="btn btn-dark close" data-dismiss="modal" ><?php echo lang('strCancel')?>&nbsp;</span> 
    		<span class="btn btn-primary ma_r7" id="cash_charge_save_btn" onclick="cash_charge_save()">&nbsp;<?php echo lang('strNext')?></span>
    	</p>
    </div>

	<?php 
	   }
	?>
<script type="text/javascript">
function cash_charge_save(){
	$("#cash_charge_save_btn").hide();
	if ($("#charge_nm").val()==""){
		alert("송금자명을 입력해 주세요.");
		$("#cash_charge_save_btn").show();
	}else{
    	url = "/cash/cash_charge_save";
    	$.post(url,
            {
                mem_no : '<?php echo $mem_no; ?>',
                charge_money : '<?php echo $charge_money; ?>',
                charge_cash : '<?php echo $charge_cash; ?>',
                charge_way : '<?php echo $charge_way; ?>',
                cash_type : '<?php echo $cash_type; ?>',
                charge_st : '<?php echo $charge_st; ?>',
                charge_req_dt : '<?php echo $charge_req_dt; ?>',
                charge_limit_dt : '<?php echo $charge_limit_dt; ?>',
                charge_nm : $("#charge_nm").val()
            },
            function(data){
                if(data.trim()=="false"){
                    alert("실패");
                }else{
                    $('#modal_content').empty();
                    $('#modal_content').append(data);
                }
            }
        );
	}
}
</script>

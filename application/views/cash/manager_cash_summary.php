<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strPaymentMgr')?></a></li>
			<li class="active"><?php echo lang('strManager'). lang('strCash'). lang('strManagement')?></li>
		</ol>
	</div>
	<div class="panel-body ma_t20">
		<div class="cash_balance">
			<div class="tit">
				<span class="float_l"><?php echo lang('strManager')?> <?php echo lang('strCurrentBalance')?> :&nbsp;</span> 
				<span class="color_p f-size-14 float_l"><?php echo number_format($manager_cash_info['mo_cash'] + $manager_cash_info['event_cash']);?></span>
				<span class="f-size-14 float_l">원</span>
			</div>
			<div class="cash_b_mid">
				<p>
    				<?php if ($manager_cash_info['event_cash'] != "0" && $manager_cash_info['event_cash'] != ""){?>
    					(<?php echo lang('strMountain')?> <?php echo lang('strCash')?> : <span class="color_t"><?php echo number_format($manager_cash_info['mo_cash'])?></span>원 +
    					 <?php echo lang('strEvent')?> <?php echo lang('strCash')?> : <span class="color_t"> <?php echo number_format($manager_cash_info['event_cash'])?></span>원)
					<?php }?>
				</p>
			</div>
		</div>
		<div class="cash_balance_view">
			<div class="left_cont">
				<table class="left_cont_tb">
					<colgroup>
						<col width="30%">
						<col width="*">
					</colgroup>
					<tr>
						<th><?php echo lang('strRunningOutDate')?></th>
						<td>
						  <?php if ($manager_cash_info['mo_cash'] != 0){?>
						  <?php echo number_format(($manager_cash_info['mo_cash'] + $manager_cash_info['event_cash']) / $report_info[0]['use_cash'])?> 일
						  <?php }else{?>
						      -
						  <?php }?>
						</td>
					</tr>
					<?php foreach ($report_info as $row){?>
           				<tr> 
    						<th><?php echo lang($row['kind']);?></th>
    						<td><?php echo number_format($row['use_cash']);?> 원</td>
            			</tr> 
					<?php }?>
				</table>
				<div class="bar"></div>
			</div>
			<div class="right_cont">
				<div style="float: right;" id="cash_info_chart" >
					
				</div>
			</div>
		</div>
	</div>
	<div class="panel-heading ma_t10">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('Manager’sCashHistory')?></h3>
	</div>
	<div class="panel-body">
		<table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
			<colgroup>
				<col width="5%">
				<col width="15%">
				<col width="15%">
				<col width="15%">
				<col width="10%">
				<col width="15%">
				<col width="10%">
			</colgroup>
			<thead>
				<tr>
					<th>No</th>
					<th><?php echo lang('strAuthority')?></th>
					<th><?php echo lang('strID')?></th>
					<th><?php echo lang('strMountain')?> <?php echo lang('strCash')?></th>
					<th><?php echo lang('strEvent')?> <?php echo lang('strCash')?></th>
					<th><?php echo lang('strSum')?></th>
				</tr>
			</thead>
			<tbody>
			    <?php
			        if(isset($manager_cash_list)){
                        $sum_mo_cash=0;
                        $sum_event_cash=0;
                        $sum_all_cash=0;
                        foreach ($manager_cash_list as $row){
                            $sum_mo_cash+=$row['mem_cash'];
                            $sum_event_cash+=$row['mem_event_cash'];
                        }
                        $sum_all_cash=$sum_mo_cash+$sum_event_cash;
				        $no=0; 
				        foreach ($manager_cash_list as $row){
                            $no++;
                ?>
			        <tr>
			            <td class="txt_center pa_l0"><?php echo $no;?></td>
			            <td class="txt_center pa_l0">매니저</td>
			            <td class="txt_center pa_l0"><a href="javascript:manager_cash_deatail('<?php echo $row['mem_no']?>');"><?php echo $row['mem_id']?></a></td>
			            <td class="txt-right">￦ <?php echo number_format($row['mem_cash']);?></td>
			            <td class="txt-right">￦ <?php echo number_format($row['mem_event_cash']);?></td>
			            <td class="txt-right">￦ <?php echo number_format($row['mem_event_cash']+$row['mem_cash']);?></td>
		            </tr>
	            <?php }?>
			</tbody>
			<tr>
               <td class="txt_center pa_l0" colspan="3"><?php echo lang('strSumWithinDateRange')?></td>
               <td class="txt-right">￦ <?php echo number_format($sum_mo_cash);?></td>
               <td class="txt-right">￦ <?php echo number_format($sum_event_cash);?></td>
               <td class="txt-right">￦ <?php echo number_format($sum_all_cash);?></td>
            </tr>
            <?php }?>
		</table>
		<form name="manager_cash_detail" method="post">
			<input type="hidden" name="master_no" value="<?php echo $master_no;?>">
			<input type="hidden" name="manager_no">
		</form>
	</div>
</div>



<script type="text/javascript">
function manager_cash_deatail(manager_no){
	var frm = document.manager_cash_detail;
	frm.action="/cash/manager_cash_charge_list";
	frm.manager_no.value = manager_no;
	frm.submit();
}

function get_chart(){
    $.ajax({
        url : "/cash/manager_cash_info_chart/",
        dataType : "html",
        type : "post",  // post 또는 get
        data : { mem_no : <?php echo $master_no;?> },   // 호출할 url 에 있는 페이지로 넘길 파라메터
        success : function(result){
            $("#cash_info_chart").html(result);
        }
    });
}

$(document).ready(function() {
	get_chart();
	$('#list').dataTable( {
		"dom": '<"toolbar">t'
    });
	$("div.toolbar").append($("#all_btn_gp").html());
} );
</script>
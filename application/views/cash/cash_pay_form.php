<div class="modal-header">
    <h4 class="h4_blit">캐쉬지급</h4>
</div>
<div class="modal-body">
    <h5 class="sub_tit_blit ma_l10" ></h5>
    <input type="hidden" id="charge_no" name="charge_no" value="<?php echo $charge_no?>"/>
    <input type="hidden" id="charge_st" name="charge_st" value="<?php echo $charge_st?>"/>
    <input type="hidden" id="admin_no" name="admin_no" value="<?php echo $admin_no?>"/>
    <input type="hidden" id="charge_way" name="charge_way" value="<?php echo $charge_way?>"/>    
    <input type="hidden" id="mem_no" name="mem_no" value="<?php echo $cash_info[0]['mem_no']?>"/>
    <input type="hidden" id="charge_cash" name="charge_cash" value="<?php echo $cash_info[0]['charge_cash']?>"/>
    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb bd-t2-gray ma_b30">
        <tr>
            <th class="point_blue" width="20%">ID</th>
            <td colspan="2" width="30%"><?php echo $cash_info[0]['mem_id']?></td>
            <th class="point_blue bd_l" width="20%">입금자명</th>
            <td colspan="2" width="30%"><?php echo $cash_info[0]['charge_nm']?></td>
        </tr>
        <tr>
            <th class="point_blue" width="20%">입금금액</th>
            <td colspan="5" width="80%">
                <?php echo number_format($cash_info[0]['charge_money'])?>원 <span style="color:red;">(TAX 포함)</span>
            </td>
        </tr>
        <tr>
            <th class="point_blue" width="20%">적용캐쉬</th>
            <td colspan="5" width="80%">
                <?php echo number_format($cash_info[0]['charge_cash'])?>원 <span style="color:red;">(TAX 제외)</span>
            </td>
        </tr>
        <tr>
            <th class="point_blue" width="20%">담당자</th>
            <td colspan="5" width="30%"><?php echo $cash_info[0]['admin_nm']?></td>
        </tr>
        <?php 
            if($charge_st == "C"){
        ?>
        <tr>
            <th class="point_blue" width="20%">무통장<br/>입금일자</th>
            <td colspan="5" width="30%"><input type="text" class="date" id="charge_confirm_dt" name="charge_confirm_dt" type="hidden"/></td>
        </tr>
        <?php 
            }
        ?>
    </table>
</div>
<div class="modal-footer ma_t10">
    <span onclick="cash_pay();" id="done" class="btn btn-primary"><?php echo lang('strDone')?></span>
    <span data-dismiss="modal" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>
<script type="text/javascript">
    $(function() {
        jQuery('.date').datetimepicker({
            format:'Y-m-d H:i'
        });
    });

    function cash_pay(){

    	$("#done").hide();
    	
        if($("#cash_confirm_dt").val() == ""){
            alert("무통장 입금 날짜를 입력해주세요~");
            $("#cash_confirm_dt").focus();
            return;
        }

        var url = '/cash/cash_pay';
        $.post(url,
            {
                charge_no: $("#charge_no").val(),
                admin_no: $("#admin_no").val(),
                charge_cash: $("#charge_cash").val(),
                mem_no: $("#mem_no").val(),
                charge_confirm_dt : $("#charge_confirm_dt").val(),
                charge_st : $("#charge_st").val(),
                charge_way : $("#charge_way").val()
            },
            function(data){
                if(data.trim() == "ok"){
                    alert("지급 완료");
                    $('#modal_content').html("");
                    $(this).data('bs.modal', null);
                    location.reload();
                }else{
                    alert("지급 실패");
                }
            }
        );
    }

</script>
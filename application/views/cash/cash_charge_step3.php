<div class="modal-header">
	<h4 class="h4_blit ma_b5 ma_t5"">
		<?php
		  if($charge_way == '5'){
		      echo "후불요청";       
		  }else if($charge_way == '6'){
		      echo "이벤트 캐쉬 지급";
		  }else{
		      echo lang('strMakeAPayment');
		  }
		?>
	</h4>
</div>
<div class="clear pa_t20">
	<div class="panel panel-bordered panel-success wid_95p m_center ma_b20  cash_dv">
	<!-- 
		<div class="panel-heading hei_35 ma_t0">
			<h3 class="panel-title line_20 f-size-12">결제완료</h3>
		</div>
	 -->
	 <?php 
	   //이벤트 캐쉬이면
        if($charge_way == '6'){
	 ?>
 		<div class="panel-body">
			<h3 class="ma_t20 ma_b10 center color_p">이벤트 캐쉬 지급 완료</h3>
		</div>
	 <?php 
        }else{
	 ?>
		<div class="panel-body">
			<h3 class="ma_t20 ma_b10 center color_p"><?php echo lang('strResisterSuccess')?></h3>
			<ul class="cash_frm">
			<li><strong><?php echo lang('strAmountOfPay')?> :</strong> <?php echo number_format($charge_money);?>원</li>
			<li><strong><?php echo lang('strDepositBank')?> :</strong> 신한은행</li>
			<li><strong><?php echo lang('strDepositAccount')?> :</strong> 140 &ndash; 011 &ndash; 105213 <br />(<?php echo lang('strAccountHolder')?> : 주식회사애드오피 이원섭)</li>
			<li><strong><?php echo lang('strDeadline')?> :</strong> <?php echo substr($charge_limit_dt,0,10)?> 이내</li>
			</ul>
			
			<!-- 
			<table class="cash_tb_com2 m_center ma_t0">
				<colgroup>
					<col width="40%">
					<col width="60%">
				</colgroup>
				<?php 
				    if($charge_way == 1){
				?>
				<tr>
					<th><?php echo lang('strAmountOfPay')?> :</th>
					<td><span><?php echo number_format($charge_money);?>원</span></td>
				</tr>
                <?php 
                    }
				?>
				<tr>
					<th><?php echo lang('strDepositBank')?> :</th>
					<td><span>기업은행</span></td>
				</tr>
				<tr>
					<th><?php echo lang('strDepositAccount')?> :</th>
					<td>123-12-1234567 <br />(<?php echo lang('strAccountHolder')?> : 주식회사 애드오피)</td>
				</tr>
				<?php 
				    if($charge_way == 1){
				?>
				<tr>
					<th><?php echo lang('strDeadline')?> : </th>
					<td><?php echo substr($charge_limit_dt,0,10)?> 이내</td>
				</tr>
				<?php 
                    }
				?>
			</table>
			
			 -->
		</div>
		<?php 
	       }
		?>
	</div>
    <p class=" modal-footer txt-right ma_t10 ma_r10">
		<span class="btn btn-primary" onclick="$('#cash_modal').modal('hide'); modal_clear();"><?php echo lang('strOkay')?></span>
	</p>
	
</div>

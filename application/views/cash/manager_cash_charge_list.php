<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strPaymentMgr')?></a></li>
			<li><?php echo lang('strManager'). lang('strCash'). lang('strManagement')?></li>
			<li class="active"><a href="#"><?php echo lang('strDetail')?></a></li>
		</ol>
	</div>
	<div class="panel-body ma_t20">
	   <table class="aut_tb">
	       <tr>
	           <th><?php echo lang('strID')?></th>
	           <td><?php echo $manager_info['mem_id']?></td>
	           <th><?php echo lang('strAuthority')?></th>
	           <td><?php echo lang('strManager')?></td>
	       </tr>
	   </table>
	</div>
	<div class="panel-body ma_t10">
		<div class="tab-base">
			<!--Nav Tabs-->
			<ul class="nav nav-tabs">
				<li class="active"><a href="javascript:tab_move('charge')"><?php echo lang('strPaymentHistory')?></a>
				</li>
				<li><a href="javascript:tab_move('history')"><?php echo lang('strUsedHistory')?></a></li>
				<li><a href="javascript:tab_move('refund')"><?php echo lang('strRefundHistory')?></a></li>
			</ul>
		</div>
		<form name="manager_cash_charge_list" method="post">
			<input type="hidden" name="master_no" value="<?php echo $master_no;?>">
			<input type="hidden" name="manager_no" value="<?php echo $manager_no;?>">
			<input type="hidden" name="mem_no" value="<?php echo $manager_no;?>">
			<input type="hidden" name="kind" value="<?php echo $kind;?>">
			<input type="hidden" name="range" value="<?php echo $range;?>">
    		<div class="clear hei_35">
    		    <div class="float_r wid_37p">
    		        <div class="float_r ma_l10">
            		    <a href="javascript:document.cash_charge_list.submit()" class="btn btn-primary"><?php echo lang('strSearch')?></a>&nbsp;
            		    <a href="javascript:excel_down()" class="btn btn-primary"><?php echo lang('strDownload')?></a>&nbsp;
    		        </div>
        		    <input type="text" name="fromto_date" id="daterange" class="range wid_55p" value="<?php echo $fromto_date?>" /> 
    				<span class="iput-group-addon">
    					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
    				</span>
				</div>
    			<div class="btn-group">
    				<button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                        <span id="cash_charge_kind">
                            &nbsp;  
                            <?php 
                                if ($kind == ''){
                                    echo lang('strAll');
                                }elseif ($kind == 'C'){
                                    echo lang('strAtomCash');
                                }elseif ($kind == 'E'){
                                    echo lang('strEvent')." ".lang('strCash');
                                }elseif ($kind == '1'){
                                    echo lang('strVirtualBankAccount');
                                }else{
                                    echo lang('strCreditCard');
                                }
                            ?>  
                            &nbsp;
                        </span> 
                        <i class="dropdown-caret fa fa-caret-down"></i>
    				</button>
    				<ul class="dropdown-menu ul_sel_box">
    				    <li>
                            <a href="#" onclick="cash_charge_list_kind('all')">
                                <?php echo lang('strAll');?>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="cash_charge_list_kind('C')">
                                ATOM <?php echo lang('strCash')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="cash_charge_list_kind('E')">
                                <?php echo lang('strEvent')?> <?php echo lang('strCash')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="cash_charge_list_kind('1')">
                                <?php echo lang('strVirtualBankAccount')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" onclick="cash_charge_list_kind('2')">
                                <?php echo lang('strCreditCard')?>
                            </a>
                        </li>
                    </ul>
    			</div>&nbsp;
    			<a href="javascript:sel_fromto_date('7')" class="btn btn-default"><?php echo lang('strLast7')?></a>&nbsp;
    			<a href="javascript:sel_fromto_date('30')" class="btn btn-default"><?php echo lang('strLast30')?></a>&nbsp;
    			<a href="javascript:sel_fromto_date('60')" class="btn btn-default"><?php echo lang('strLast60')?></a>&nbsp;
    			<a href="javascript:sel_fromto_date('90')" class="btn btn-default"><?php echo lang('strLast90')?></a>&nbsp;
    		</div>
			<table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb" data-horizontal-width="100%" cellpadding="0" cellspacing="0" border="0">
				<colgroup>
					<col width="5%">
					<col width="15%">
					<col width="15%">
					<col width="15%">
					<col width="10%">
					<col width="15%">
					<col width="10%">
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th><?php echo lang('strCashRegisterDate')?></th>
						<th><?php echo lang('strCashCompleteDate')?></th>
						<th><?php echo lang('strChargedAmount')?></th>
						<th><?php echo lang('strCashType')?></th>
						<th><?php echo lang('strPaymentMethod')?></th>
						<th><?php echo lang('strCashActions')?></th>
					</tr>
				</thead>
				<tbody>
				    <?php
				        if(isset($manager_cash_charge_list)){
                            $sum_charge_cash=0;
                            foreach ($manager_cash_charge_list as $row){
                                $sum_charge_cash+=$row['charge_cash'];
                            }
    				        $no=0; 
    				        foreach ($manager_cash_charge_list as $row){
                                $no++;
                    ?>
				        <tr>
				            <td class="txt_center pa_l0"><?php echo $no;?></td>
				            <td class="txt_center pa_l0"><?php echo substr($row['charge_req_dt'],0,10);?></td>
				            <td class="txt_center pa_l0">
				                <?php 
				                    if($row['charge_dt']==""){
				                        echo "-";
			                        }else{
				                        echo substr($row['charge_dt'],0,10);
			                        }
		                        ?>
			                </td>
				            <td class="txt-right">￦ <?php echo number_format($row['charge_cash']);?></td>
				            <td class="txt_center pa_l0">
				                <?php 
				                    if($row['cash_type']=="C"){
				                        echo "ATOM ".lang('strCash');
                                    }else{
                                        echo lang('strEvent')." ".lang('strCash');
				                    }
			                    ?>
			                </td>
				            <td class="txt_center pa_l0"><?php echo lang($row['charge_way_desc']);?></td>
				            <td class="txt_center pa_l0">
				                <?php 
				                    if($row['charge_st']=="R"){
				                        echo lang('strNeedToDeposit');
				                    }else{
				                        echo lang('strCashComplete');
				                    }
			                    ?>
			                </td>
			            </tr>
		            <?php }?>
				</tbody>
				<tr>
	               <td class="txt_center pa_l0" colspan="2"><?php echo lang('strSumWithinDateRange')?></td>
	               <td class="txt_center pa_l0">-</td>
	               <td class="txt-right">￦ <?php echo number_format($sum_charge_cash);?></td>
	               <td class="txt_center pa_l0">-</td>
	               <td class="txt_center pa_l0">-</td>
	               <td class="txt_center pa_l0">-</td>
                </tr>
                <?php }?>
			</table>
			<div class="center" id="all_btn_gp">
				<div class="center hei_35">
	                <div class="btn-group float_r">
	                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
	                    <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
	                        <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
	                        <i class="dropdown-caret fa fa-caret-down"></i>
	                    </button>
	                    <ul class="dropdown-menu ul_sel_box_pa" >
	                        <li><a href="javascript:page_change(10)">10</a></li>
	                        <li><a href="javascript:page_change(25)">25</a></li>
	                        <li><a href="javascript:page_change(50)">50</a></li>
	                        <li><a href="javascript:page_change(100)">100</a></li>
	                    </ul>
	                </div>
	                <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
	                <?php
	                /*페이징처리*/
	                    echo $page_links;
	                /*페이징처리*/
	                ?>
	            </div>
            </div>
		</form>
	</div>
</div>



<script type="text/javascript">
//페이징 스크립트 시작
function page_change(row){
	frm=document.manager_cash_charge_list;
	frm.per_page.value=row;
	frm.submit();
}

function paging(number){
    var frm = document.manager_cash_charge_list;
    frm.action="/cash/manager_cash_charge_list/" + number;
    frm.submit();
}
//페이징 스크립트 끝

function cash_charge_list_kind(kind){
	var frm = document.manager_cash_charge_list;
	frm.action="/cash/manager_cash_charge_list";
	if(kind=="all"){
	   frm.kind.value="";
	}else{
	   frm.kind.value=kind;
	}
	frm.submit();
}

function sel_fromto_date(range){
	var frm = document.manager_cash_charge_list;
	frm.action="/cash/manager_cash_charge_list";
    frm.range.value=range;
	frm.submit();
}

function tab_move(page){
	var frm = document.manager_cash_charge_list;
	frm.kind.value="";
	frm.action="/cash/manager_cash_"+page+"_list";
	frm.submit();
}

function excel_down(){
	var cnt = '<?php echo count($manager_cash_charge_list); ?>';
	if (cnt > 0){
    	var frm = document.manager_cash_charge_list;
        frm.action = '/excel/cash_charge_excel_down';
        frm.submit();
    }else{
    	alert("다운로드 데이터가 없습니다.");
    }
}

$(document).ready(function() {
} );
</script>
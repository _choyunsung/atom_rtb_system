<script>
$(function () {
    $('#cash_info_chart').highcharts({
        chart: {
        	height:161,
        	width:530
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: [{
        	tickInterval: 2,
            categories: [
                         <?php if(isset($use_daily_cash)){foreach ($use_daily_cash as $row){echo "'".$row['cash_dt']."',";}}?>
                         ],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}원',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: '',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: '캐쉬 소진추이',
            type: 'column',
            data: [
                    <?php if(isset($use_daily_cash)){foreach ($use_daily_cash as $row){echo round($row['used_cash']).",";}}?>
                   ],
            tooltip: {
                valueSuffix: ' 원'
            }

        }]
    });
});
</script>
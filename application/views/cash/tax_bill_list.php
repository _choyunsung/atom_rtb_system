<!-- 세금계산서 상세정보 modal -->
<div class="modal" id="master_tax_bill_detail" role="dialog"tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true"  data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-xs">
        <div class="modal-content" id="modal_tax_bill_detail">
            
        </div>
    </div>
</div>

<!-- 세금계산서 상세정보 modal -->
<div class="modal" id="manager_tax_bill_detail" role="dialog"tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true"  data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="h4_blit"><?php echo lang('strTaxInvoice')?></h4>
            </div>
       		<div class="pa_l20 pa_r20 wid_100p">
       			<div class="pa_20">
       				<h4 class="ma_t8"><?php echo lang('strReceiver')?></h4>
       			    <table class="bill_tb wid_100p">
           				<tr>
           					<th><?php echo lang('strCorporateName')?></th>
           					<td id="mem_com_nm"><?php echo $member_info['mem_com_nm'];?></td>
           				</tr>
           				<tr>
           					<th><?php echo lang('strManagerName')?></th>
           					<td id="tax_mem_nm"><?php echo $member_info['tax_mem_nm'];?></td>
           				</tr>
           				<tr>
           					<th><?php echo lang('strEmail')?></th>
           					<td id="tax_mem_email"><?php echo $member_info['tax_mem_email']?></td>
           				</tr>   				
           			</table>
       			</div>
   			    <div class="pa_20">
       				<h4 class="ma_t15"><?php echo lang('strTaxInvoice')?></h4>
    	   			<table class="wid_100p bill_tb">
    	   				<tr>
    	   					<th colspan="2" id="tax_month_info"></th>
    	   				</tr>
    	   				<tr>
    	   					<th><?php echo lang('strPeriod')?></th>
    	   					<td id="tax_month"></td>
    	   				</tr>
    	   				<tr>
    	   					<th><?php echo lang('strSurtaxIsExcluded')?></th>
    	   					<td id="money"></td>
    	   				</tr>
    	   				<tr>
    	   					<th><?php echo lang('strSurtax')?></th>
    	   					<td id="extra_money"></td>
    	   				</tr>
    	   				<tr>
    	   					<th><?php echo lang('strSum')?></th>
    	   					<td id="total_money"></td>
    	   				</tr>   				
    	   			</table>
           			<table style="width:100%" class="bill_tb">
           				<tr>
           					<th><?php echo lang("strIssuingDateOfTaxInvoice")?></th>
           					<td id="tax_dt"></td>
           				</tr>
           			</table>
   			  </div>
   		   </div>
            <!--Modal footer-->
            <div class="modal-footer ma_t8 ma">
                <span class="btn btn-primary">정산서 재발행 요청</span>
                <span onclick="$('#manager_tax_bill_detail').modal('hide'); modal_clear();" class="btn btn-dark">닫기</span>
            </div>
        </div>
    </div>
</div>

<div class="panel float_r wid_970 min_height">
	<div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strPaymentMgr')?></a></li>
			<li class="active"><?php echo lang('strReferencesMgr')?></li>
		</ol>
	</div>
	<div class="panel-body">
    	<div class="camp ma_t10">
    		<div class="cash_div2">
    			<h4><?php echo lang('strExpectedAmountOfThisMonth')?></h4>
    			<p class="f-size-16 ma_t8 f_bold">
    			     <?php echo number_format($forecast_amount[0]['forecast_amount']);?>원
			     </p>
    			<span class="wid_80p ma_t8 point_dgray">(<?php echo lang('strSurtaxIsExcluded')?>)
    			     <?php 
    			         if ($forecast_amount[0]['forecast_dt'] == $this_month){
    			             echo date('Y-m-d');
    			         }else{
    			             echo $forecast_amount[0]['forecast_dt'];
    			         } 
			         ?>
    			     기준 
			    </span>
    		</div>
    	    <div class="cash_div">
    			<h4><?php echo lang('strTypeOfReference')?></h4>
    			<span class="f-size-16 ma_t8 point_blue f_bold"><?php echo lang('strETaxInvoice')?></span>
    		</div>
     	    <div class="cash_div">
    			<h4><?php echo lang('strTypeOfCharging')?></h4>
    			<span class="f-size-16 ma_t8 point_blue f_bold">
    			     <?php 
    			         if ($member_info['mem_pay_later'] == 'Y'){
    			             echo lang('strPayLater');
    			         }else{
    			             echo lang('strPrePay');
    			         }
    			     ?>
			    </span>
    		</div>   
     	    <div class="cash_div bd_r_0">
    			<h4><?php echo lang('strReceiver')?></h4>
    			<span class=" f-size-16 ma_t8 point_blue f_bold"><?php echo $member_info['mem_com_nm'];?></span><br />
    			<span class=" point_dgray"><?php echo $member_info['tax_mem_email'];?></span>
    			
    			<a href="/account/account_master_modify">
    			<p class="text-right ma_t5 point_dgray pa_b5"> <?php echo lang('strEditReceiver')?>
    			 <i class="fa fa-gear"></i></p></a>
    		</div>    		
    	</div>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit20"><?php echo lang('strTaxInvoiceHistory')?></h3>
	</div>
	<div class="panel-body ma_t8">
		<form name="tax_bill_list" method="post">
			<input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
			<input type="hidden" name="range" value="<?php echo $range;?>">
			<input type="hidden" name="order_col" id="order_col" value="<?php echo $order_col;?>">
            <div class="clear over_hidden" >
                <div class="clear wid_200p">
                    <ul class="report_tg_ul float_l">
                        <li class="ma_l0"><a href="javascript:sel_fromto_date('30')" class="btn btn-default"><?php echo lang('strLast30')?></a></li>
                        <li class="ma_l5"><a href="javascript:sel_fromto_date('60')" class=" btn btn-default"><?php echo lang('strLast60')?></a></li>
                        <li class="ma_l5"><a href="javascript:sel_fromto_date('90')" class=" btn btn-default"><?php echo lang('strLast90')?></a></li>
                    </ul>    
                    <ul class="report_tg_ul_l ma_l5 ma_b10">
                        <li class="float_l">
                            <input type="text" name="fromto_date" id="daterange" class="range wid_200" value="<?php echo $fromto_date?>" /> 
                            <span class="iput-group-addon">
                                <i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
                            </span>
                        </li>
                        <li class=" float_l ma_l5"><a href="javascript:document.tax_bill_list.submit()" class="btn btn-primary"><?php echo lang('strSearch')?></a></li>
                    </ul>
                    <ul class="report_tg_ul_r">
                        <li>
                            <a href="javascript:sel_orderby('publish_dt')" id="order_tax_dt" class="wid_80 btn btn-default">&nbsp;<?php echo lang('strIssuingDate')?></a>
                        </li>
                        <li class="ma_l5">
                            <a href="javascript:sel_orderby('result_price')" id="order_total_money" class="wid_80 btn btn-default"><?php echo lang('strHighAmount')?></a>
                        </li>   		  
                    </ul>
                </div>
            </div>
			<table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
				data-horizontal-width="100%" data-display-length="100"
				cellpadding="0" cellspacing="0" border="0">
				<colgroup>
					<col width="7%">
					<col width="15%">
					<col width="15%">
					<col width="15%">
					<col width="10%">
					<col width="15%">
					<col width="15%">
				</colgroup>
				<thead>
					<tr>
					    <th>No</th>
						<th><?php echo lang('strChargedMonth')?></th>
						<th><?php echo lang('strSupplyPrice')?></th>
						<th><?php echo lang('strSurtax')?></th>
						<th><?php echo lang('strTotal')?></th>
						<th><?php echo lang('strIssuer')?></th>
						<th><?php echo lang('strDetail')?><?php echo lang('strView') ?></th>
					</tr>
				</thead>
				<tbody>
				    <?php
                        $sum_money=0;
                        $sum_extra_money=0;
                        $sum_all_cash=0;
                        $sum_all_money=0;
                        $no=0;
                        if(isset($tax_bill_list)){
                            foreach ($tax_bill_list as $row){
                                $sum_money += round($row['loc_price'],-2);
                                $sum_extra_money += $sum_money * 0.1;
                            }
                            $sum_all_money = $sum_money+$sum_extra_money;

    				        foreach ($tax_bill_list as $row){
                                $loc_price = round($row['loc_price'], -2);
                                $loc_tax = $loc_price * 0.1;
                                $loc_result = $loc_price + $loc_tax;
                                $no++;
                    ?>
				        <tr>
				            <td class="txt_center pa_l0"><?php echo $no;?></td>
				            <td class="txt_center pa_l0"><?php echo $row['date_ym']?></td>
				            <td class="txt-right">￦ <?php echo number_format($loc_price);?></td>
				            <td class="txt-right">￦ <?php echo number_format($loc_tax);?></td>
				            <td class="txt-right">￦ <?php echo number_format($loc_result);?></td>
				            <td class="txt_center pa_l0">ADOP</td>
				            <td class="txt_center pa_l0">
				            <?php if ($member_info['mem_type'] == 'manager'){?>
				                <a href="javascript:manager_tax_bill_detail_view('<?php echo $row['publish_dt'];?>','<?php echo $loc_price;?>','<?php echo $loc_tax;?>','<?php echo $row['publish_dt'];?>')">보기</a>
				            <?php }else{?>
				                <a href="javascript:master_tax_bill_detail_view('<?php echo $row['mem_no']?>', '<?php echo $row['date_ym']?>')">보기</a>
			                <?php }?>
			                </td>
			            </tr>
		            <?php }?>
                <?php }?>
				</tbody>
				<tfoot>
				<tr>
	               <td class="txt_center pa_l0"><?php echo lang('strSumWithinDateRange')?></td>
	               <td class="txt_center pa_l0">-</td>
	               <td class="txt-right">￦ <?php echo number_format($sum_money);?></td>
	               <td class="txt-right">￦ <?php echo number_format($sum_extra_money);?></td>
	               <td class="txt-right">￦ <?php echo number_format($sum_all_money);?></td>
	               <td class="txt_center pa_l0">-</td>
	               <td class="txt_center pa_l0">-</td>
                </tr>
             </tfoot>
			</table>
			<div class="center">
				<div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box_pa" >
                        <li><a href="javascript:page_change(10)">10</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(100)">100</a></li>
                    </ul>
                </div>
                <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                <?php
                /*페이징처리*/
                echo $page_links;
                /*페이징처리*/
                ?>
            </div>
		</form>
	</div>

</div>


<script type="text/javascript">
//페이징 스크립트 시작
function page_change(row){
	frm = document.tax_bill_list;
	frm.per_page.value = row;
	frm.submit();
}

function paging(number){
    var frm = document.tax_bill_list;
    frm.action = "/cash/tax_bill_list/" + number;
    frm.submit();
}
//페이징 스크립트 끝

function request_tax_bill(mem_no){
	$("#request_bill_btn").hide();
	var url = '/cash/request_republish_tax_bill';
	var date_ym = $("#request_bill_ym").val();
	$.post(url,
    {
        mem_no : mem_no,
        date_ym : date_ym
    },
    function(data){
        if(data.trim()=="ok"){
            alert("정산서 재발행 요청이 완료되었습니다.");
        }else{
        	$("#request_bill_btn").show();
            alert("정산서 재발행 요청을 할 수 없습니다.");
        }
    }
    );
	
}

function manager_tax_bill_detail_view(tax_month, money, extra_money, tax_dt){
    var year = tax_month.substring(0,4);
    var month = tax_month.substring(5,7);
    var total_money = Number(money)+Number(extra_money);
    $("#tax_month_info").html("<?php echo $member_info['mem_com_nm'];?>" + " " + month + "월 정산");
    $("#tax_month").html(year + "년 " + month + "월");
    $("#request_bill_ym").val(year + "-" + month);
    $("#money").html(comma(money) + " 원");
    $("#extra_money").html(comma(extra_money) + " 원");
    $("#total_money").html(comma(total_money) + " 원");
    $("#tax_dt").html(tax_dt.substring(0,10));
    $("#manager_tax_bill_detail").modal('show');
}

function master_tax_bill_detail_view(mem_no, date_ym){
	$("#request_bill_btn").hide();
	var url = '/cash/detail_tax_bill_view';
	$.post(url,
    {
        mem_no : mem_no,
        date_ym : date_ym
    },
    function(data){
        $("#modal_tax_bill_detail").html(data);
        $("#master_tax_bill_detail").modal('show');
    }
    );
}

function comma(str) {
    str = String(str);
    return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function sel_orderby(col){
	var frm = document.tax_bill_list;
	frm.action = "/cash/tax_bill_list";
    frm.order_col.value=col;
    frm.submit();
}

function sel_fromto_date(range){
	var frm = document.tax_bill_list;
	frm.action = "/cash/tax_bill_list";
    frm.range.value=range;
	frm.submit();
}



$(document).ready(function() {
	$("#order_"+$("#order_col").val()).addClass('btn-primary');
// 	$("#order_"+$("#order_col").val()).css('background-color','#bdbdbd');
//	$('#list').dataTable( {
//		"ordering": false,
//		"dom": '<"toolbar">t'
//    });
} );
</script>
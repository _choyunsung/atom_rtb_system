<div class="modal-header">
    <h4 class="h4_blit"><?php echo lang('strTaxInvoice')?></h4>
</div>
<div class="pa_l20 pa_r20 wid_100p">
	<div class="pa_20">
		<h4 class="ma_t8"><?php echo lang('strReceiver')?></h4>
	    <table class="bill_tb wid_100p">
	    <input type="hidden" id="request_bill_ym">
			<tr>
				<th><?php echo lang('strCorporateName')?></th>
				<td id="mem_com_nm"><?php echo $member_info['mem_com_nm'];?></td>
			</tr>
			<tr>
				<th><?php echo lang('strManagerName')?></th>
				<td id="tax_mem_nm"><?php echo $member_info['tax_mem_nm'];?></td>
			</tr>
			<tr>
				<th><?php echo lang('strEmail')?></th>
				<td id="tax_mem_email"><?php echo $member_info['tax_mem_email']?></td>
			</tr>   				
		</table>
	</div>
    <div class="pa_20">
		<h4 class="ma_t15"><?php echo lang('strTaxInvoice')?></h4>
		<table class="wid_100p bill_tb">
		    <thead>
                <tr>
                <th colspan="4"><?php echo $date_ym;?> 정산내역</th>
                </tr>
                <tr>
                    <th><?php echo lang('strDetailCont')?></th>
                    <th><?php echo lang('strPublishPrice')?></th>
                    <th><?php echo lang('strTaxPrice')?></th>
                    <th><?php echo lang('strSum')?></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    foreach($taxbill_list as $row){
                        $loc_price = round($row['loc_price'], -2);
                        $loc_tax = $loc_price * 0.1;
                        $loc_result = $loc_price + $loc_tax;
                ?>
                   <?php 
                        foreach($this->calculate_db->select_tax_bill_detail_manager_list($row['mem_no'], $row['date_ym']) as $key_=>$row_){
                    ?>
                    	<?php 
                            $loc_price = round($row_['loc_price'], -2);
                            $loc_tax = $loc_price * 0.1;
                            $loc_result = $loc_price + $loc_tax;
                        ?>
                        <?php $price += $loc_price;?>
                        <?php $tax_price += $loc_tax;?>
                        <?php $result_price += $loc_result;?>
                    <tr>    
                        <td class="txt_center"><?php echo $row_['mem_id']?>(<?php echo $row_['mem_type']?>)</td>
                        <td class="txt_right"><?php echo number_format($loc_price)?></td>
                        <td class="txt_right"><?php echo number_format($loc_tax)?></td>
                        <td class="txt_right"><?php echo number_format($loc_result)?></td>
                    </tr>
                    <?php 
                        }
                    ?>
                <?php }?>
            </tbody>
            <tbody>
                <tr id="prepend_standard">
                    <td class="txt_center"><?php echo lang('strSum')?></td>
                    <td class="txt_right"><?php echo number_format($price)?></td>
                    <td class="txt_right"><?php echo number_format($tax_price)?></td>
                    <td class="txt_right"><?php echo number_format($result_price)?></td>
                </tr>
            </tbody>
		</table>
		<table style="width:100%" class="bill_tb">
			<tr>
				<th><?php echo lang("strIssuingDateOfTaxInvoice")?></th>
				<td id="tax_dt"></td>
			</tr>
		</table>
  </div>
</div>
<!--Modal footer-->
<div class="modal-footer ma_t8 ma">
    <span class="btn btn-primary" id="request_bill_btn" onclick="request_tax_bill(<?php echo $member_info['mem_no'];?>)">정산서 재발행 요청</span>
    <span onclick="$('#master_tax_bill_detail').modal('hide'); modal_clear();" class="btn btn-dark">닫기</span>
</div>

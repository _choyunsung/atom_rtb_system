<!-- 유저이름, 그룹이름 영역 -->
<form name="cate_form">
    <div class="pad-top">
        <h4>
            <?php
                if (isset($_POST['mem_no'])) {
                    $group_nm = "";
                    $mem_no = $_POST['mem_no'];
                    $group_no = 1;
            ?>
                <input type="text" disabled value="<?php echo $mem_nm?>"style="width:100%; background-color:#ececec;"/><span id="mem_nm"></span>
                <?php
                    if($fee_fl == "Y"){
                ?>
                <span class="mar-lft">
                수수료율 <input type="text" id="mem_com_fee" name="mem_com_fee" class="wid_10p" value="<?php echo $mem_com_fee?>"/>%
                </span>
                <?php
                    }
                ?>
            <?php
                }else{
                    if(!isset($group_nm)){
                        $group_nm = "";
                    }
                    $group_no = $_POST['group_no'];
                    //$group_no = 0;
                    $mem_no = 0;
                    if ($group_no == 0) {
                        $cate_rows = $all_cate_rows;
                    }
                ?>
                <input type="text" id="temp_group_nm" name="temp_group_nm" value="<?php echo $temp_group_nm?>" />
                <div style="font-size:12px;">
                
                <span class="mar-lft">
                애드오피 관리자 <input type="checkbox" id="admin_fl" name="admin_fl" value="Y" <?php if($admin_fl == "Y"){ echo "checked";}?>/>
                </span>
                <span class="mar-lft">
                수수료 여부 <input type="checkbox" id="fee_fl" name="fee_fl" value="Y" <?php if($fee_fl == "Y"){ echo "checked";}?>/>
                </span>
                <br/>
                <span class="mar-lft">
                영업자 <input type="radio" id="sales" name="role" value="sales" <?php if($role == "sales"){ echo "checked";}?>/>
                </span>
                <span class="mar-lft">
                대행사 <input type="radio" id="agency" name="role" value="agency" <?php if($role == "agency"){ echo "checked";}?>/>
                </span>
                <span class="mar-lft">
                랩사 <input type="radio" id="lab" name="role" value="lab" <?php if($role == "lab"){ echo "checked";}?>/>
                </span>
                <span class="mar-lft">
                광고주 <input type="radio" id="adver" name="role" value="adver" <?php if($role == "adver"){ echo "checked";}?>/>
                </span>
                <span class="mar-lft">
                개인 <input type="radio" id="ind" name="role" value="ind" <?php if($role == "ind"){ echo "checked";}?>/>
                </span>
                </div>
            <?php
                }
            ?>
        </h4>
    </div>
<!-- 유저이름, 그룹이름 영역 -->

<!-- 카테고리 리스트및 기능영역 -->
<?php
    if (isset($cate_rows) and count($cate_rows) > 0) {
    	$cete_cnt = count($cate_rows);
    	$no = 0;
?>
<!-- 카테고리 리스트 -->
<div class="pad-top">

        <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>">
        <input type="hidden" name="group_no" value="<?php echo $group_no;?>">
        <input type="hidden" name="group_nm" value="<?php echo $group_nm;?>">

        <input type="hidden" name="read_fl_list" value="">
        <input type="hidden" name="write_fl_list" value="">
        <table cellpadding="0" cellspacing="0" border="0" class="aut_tb">
        <?php
            foreach($cate_rows as $cr){
        ?>
            <?php
                if ($cr['cate_parent_no']== "0") {
            ?>
            <tr>
                <th colspan="2" class="txt-left pa_l10"><?php echo lang($cr['cate_nm']);?> <span>*</span></th>
            </tr>

            <?php
                }else{
            ?>
            <tr>
                <td class="pa_l20"><label><?php echo lang($cr['cate_nm']);?> </label></td>
                <?php
                    if ($group_no == 0) {
                ?>
                    <td>
                        <input type="checkbox" value="<?php echo $cr['cate_no'];?>" name="read_fl">
                        read
                        <input type="checkbox" value="<?php echo $cr['cate_no'];?>" name="write_fl">
                        write
                    </td>
                <?php
                    }else{
                ?>
                    <td>
                        <input type="checkbox" value="<?php echo $cr['cate_no'];?>" name="read_fl" <?php if($cr['read_fl'] == 'Y'){ echo 'checked';}?>>
                        read
                        <input type="checkbox" value="<?php echo $cr['cate_no'];?>" name="write_fl" <?php if($cr['write_fl'] == 'Y'){ echo 'checked';}?>>
                        write
                    </td>
                <?php
                    }
                ?>
            </tr>
            <?php
                }
            ?>

        <?php
            }
        ?>

       </table>
</div>

<!-- 카테고리 리스트 -->
<?php
    //if ($_POST['write_fl'] == 'Y') {
?>
<div class="clear">
    <div class="float_left ma_l10 wid_45p">
        <input type="button" class="btn btn-default" onclick="check_all_cate();" value="<?php echo "전체선택"?>"/>
        <?php
            if($mem_no > 0){
        ?>
        <input type="button"class="btn btn-primary"  onclick="save_temp_cate('<?php echo $mem_no?>');" value="저장" />
        <?php
            }else{
                if($group_no == 0){
        ?>
        <input type="button"class="btn btn-primary"  onclick="add_temp_cate();" value="추가" />
        <?php
                } else {
        ?>
        <input type="button" class="btn btn-primary" onclick="modify_temp_cate('<?php echo $group_no?>');" value="수정" />
        <?php
                }
            }
        ?>
    </div>
<?php
    //}
}
?>
</form>
<?php
    if ($mem_no > 0) {
?>
<div>
    <?php
       // if ($_POST['writeyn'] == 'Y') {
    ?>
        <div class="float_right">
            <form name="cate_temp_form" action="#">
                <input type="hidden" name="mem_no" value="<?php echo $mem_no;?>"/>
                <select name="group_no" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                <?php
                    foreach ($select_category_template as $sct) {
                ?>
                    <option value="<?php echo $sct['group_no']?>" <?php if($group_no == $sct['group_no']){ echo "selected";}?>>
                        <?php echo $sct['group_nm'];?>
                    </option>
                <?php
                    }
                ?>
                </select>
                <input type="button" class="btn btn-primary" onclick="apply_temp_cate();" value="적용"/>
            </form>
        </div>
  </div>   
    <?php
        //}
    ?>
</div>
<?php
    }
?>
<!-- 카테고리 리스트및 기능영역 -->

<script type="text/javascript">

    var chkstate = false;

    function check_all_cate(){

        if(!chkstate){
            chk = true;
            chkstate = true;
        }else{
            chk = false;
            chkstate = false;
        }

        var chk_readArr = $("input[name=read_fl]");
        for (var i=0; i < chk_readArr.length; i++) {
            chk_readArr[i].checked = chk;
        }
        var chk_writeArr = $("input[name=write_fl]");
        for (var i=0; i < chk_writeArr.length; i++) {
            chk_writeArr[i].checked = chk;
        }
    }

    function save_temp_cate(mem_no){
        var read_y_idx = '';
        var write_y_idx = '';
        var chk_readArr = $("input:checked[name=read_fl]");
        for (var i=0; i < chk_readArr.length; i++) {
            if(chk_readArr[i].checked){
                if(i == chk_readArr.length-1){
                    read_y_idx += chk_readArr[i].value;
                }else{
                    read_y_idx += chk_readArr[i].value+",";
                }
            }
        }
        var chk_writeArr = $("input:checked[name=write_fl]");
        for (var i=0; i < chk_writeArr.length; i++) {
            if(chk_writeArr[i].checked ){
                if(i == chk_writeArr.length-1){
                    write_y_idx += chk_writeArr[i].value;
                }else{
                    write_y_idx += chk_writeArr[i].value+",";
                }
            }
        }

        var mem_com_fee = $('#mem_com_fee').val();
        var frm = document.cate_form;
        var url = '/admin/authority_modify_temp';
        frm.mem_no.value = mem_no;
        frm.mem_com_fee = mem_com_fee;
        frm.read_fl_list.value = read_y_idx;
        frm.write_fl_list.value = write_y_idx;
        frm.action = url;
        frm.method = 'post';
        frm.submit();
    }


    function add_temp_cate(){
        if($('input[name=temp_group_nm]').val() == ''){
            alert('Please Input Template Name!');
            return;
        }
        var read_y_idx = '';
        var write_y_idx = '';
        var chk_readArr = $("input[name=read_fl]");
        for (var i=0; i < chk_readArr.length; i++) {
            if(chk_readArr[i].checked){
                read_y_idx += chk_readArr[i].value+',';
            }
        }
        var last_index=read_y_idx.lastIndexOf(',');
        read_y_idx=read_y_idx.substring(0,last_index);
        var chk_writeArr = $("input[name=write_fl]");
        for (var i=0; i < chk_writeArr.length; i++) {
            if(chk_writeArr[i].checked ){
                write_y_idx += chk_writeArr[i].value+',';
            }
        }
        var last_index2=write_y_idx.lastIndexOf(',');
        write_y_idx=write_y_idx.substring(0,last_index2);
        var frm = document.cate_form;
        var url = '/admin/authority_add_temp';
        frm.group_no.value = 0;
        frm.mem_no.value = 0;
        frm.group_nm.value = $('input[name=temp_group_nm]').val();
        frm.read_fl_list.value = read_y_idx;
        frm.write_fl_list.value = write_y_idx;
        frm.action = url;
        frm.method = 'post';
        frm.submit();
    }

    function modify_temp_cate(group_no){
        var read_y_idx = '';
        var write_y_idx = '';
        var chk_readArr = $("input:checked[name=read_fl]");
        for (var i=0; i < chk_readArr.length; i++) {
            if(chk_readArr[i].checked){
                if(i == chk_readArr.length-1){
                    read_y_idx += chk_readArr[i].value;
                }else{
                    read_y_idx += chk_readArr[i].value+",";
                }
            }
        }
        var chk_writeArr = $("input:checked[name=write_fl]");
        for (var i=0; i < chk_writeArr.length; i++) {
            if(chk_writeArr[i].checked ){
                if(i == chk_writeArr.length-1){
                    write_y_idx += chk_writeArr[i].value;
                }else{
                    write_y_idx += chk_writeArr[i].value+",";
                }
            }
        }


        if($("input:checked[name=admin_fl]").length > 0){
            chk_admin = $("input:checked[name=admin_fl]")[0].value;
        }else{
            chk_admin = "N";
        }

        if($("input:checked[name=fee_fl]").length > 0){
            chk_fee = $("input:checked[name=fee_fl]")[0].value;
        }else{
            chk_fee = "N";
        }

        if($("input:radio[name=role]").length > 0){
            chk_role = $('input[name="role"]:radio:checked').val();
        }

        var frm = document.cate_form;
        var url = '/admin/authority_modify_temp';
        frm.group_no.value = group_no;
        frm.mem_no.value = 0;
        frm.group_nm.value = $('input[name=temp_group_nm]').val();
        frm.read_fl_list.value = read_y_idx;
        frm.write_fl_list.value = write_y_idx;

        frm.admin_fl.value = chk_admin;
        frm.fee_fl.value = chk_fee;
        frm.role.value = chk_role;

        //alert(frm.admin_fl.value);
        //alert(frm.fee_fl.value);
        frm.action = url;
        frm.method = 'post';
        frm.submit();
    }

    function apply_temp_cate(){
        var frm = document.cate_temp_form;
        var url = '/admin/authority_apply_cate_temp';
        frm.action = url;
        frm.method = 'post';
        frm.submit();
    }

</script>
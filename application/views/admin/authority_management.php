<div class="panel float_r wid_970 min_height">
    <div class="history_box">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAdmin')?></a></li>
            <li class="active"><?php echo lang('strAuthorityMgr')?></li>
        </ol>
    </div>
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strAuthorityMgr')?></h3>
    </div>
    <div class="panel-body">
        <div style="width:45%;float:left;">
        <form id="authority" name="authority" action="/admin/authority_management" method="post">
            <input type="hidden" name="group_nm" id="group_nm" value="<?php echo $group_nm?>">
            <table class="inspection_tb">
           <colgroup>
	    		<col width="30%">
	    		<col width="70%">
	    	
   	 		</colgroup>
                <tr>
                    <th>
                    	<?php echo lang('strName')?> / <?php echo lang('strID')?>
                    </th>
                    <td>
                        <input type="text" id="mem_nm" name="mem_nm" class="float_left" value="<?php echo $condition['mem_nm'];?>" />
                    </td>
                </tr>
                <tr>
                    <th>
                        구분
                    </th>
                    <td>
                         <div class="btn-group float_l">
                            <input type="hidden" name="group_no" id="group_no_search">
			                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
			                    <span id="group_no_sel_search">
			                     <?php if ($group_nm == ""){?>
			                         &nbsp; 전체 &nbsp;
		                         <?php }else{?>
		                             <?php echo $group_nm;?>
	                             <?php }?>
			                    </span> 
			                    <i class="dropdown-caret fa fa-caret-down"></i>
			                </button>
			                <ul class="dropdown-menu ul_sel_box_pa">
			                    <li>
		                            <a href="javascript:sel_group('', '', 'search')">
	                                    <?php echo lang('strAll')?>
		                            </a>
		                        </li>
			                    <?php foreach($select_category_template as $sel){?>
			                        <li>
			                            <a href="javascript:sel_group('<?php echo $sel['group_no'];?>', '<?php echo $sel['group_nm'];?>', 'search')">
		                                    <?php echo $sel['group_nm'];?>
			                            </a>
			                        </li>
			                    <?php }?>
		                    </ul>
                        </div>
                       <button class="btn btn-primary float_r ma_r5" onclick="search();">검색</button>
                    </td>
                </tr>
            </table>

            <!-- 권한 적용 영역 -->
            <!-- 회원 리스트 영역 -->
            <div class="clear">
                <div class="pad_top hei_35">
                    <input class="btn btn-default float_l" type="button"  value="<?php echo lang('strAllCheck')?>" onclick="all_check();">
                    <div class="float_r txt-right">
                    <div class="btn-group float_l">
                        <input type="hidden" name="apply_group" id="group_no_apply">
			                <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" >
			                    <span id="group_no_sel_apply"> 전체 </span> 
			                    <i class="dropdown-caret fa fa-caret-down"></i>
			                </button>
			                <ul class="dropdown-menu ul_sel_box_pa">

			                    <?php 
	                               foreach($select_category_template as $sel){
	                            ?>
			                        <li>
			                            <a href="javascript:sel_group('<?php echo $sel['group_no'];?>', '<?php echo $sel['group_nm'];?>', 'apply')">
		                                    <?php echo $sel['group_nm'];?>
			                            </a>
			                        </li>
			                    <?php 
                                   }
                                ?>
		                    </ul>
                    </div>
                    <input class="btn btn-primary float_l ma_l5"  type="button"  value="<?php echo lang('strApply');?>" onclick="apply();">
                  
      
                    
                </div>
                </div>
                <div class="ma_t10">
                    <table name="member_list" class="inspection_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                        <colgroup>
                            <col width="10%">
                            <col width="10%">
                            <col width="20%">
                            <col width="20%">
                            <col width="40%">
                        </colgroup>
                        <thead>
                            <tr>
                                <th class='center ma_l0'><?php echo lang('strSelect')?></th>
                                <th class='center'>No</th>
                                <th  class="center"><?php echo lang('strName')?></th>
                                <th  class="center"><?php echo lang('strID')?></th>
                                <th  class="center"><?php echo lang('strAuthority')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if(count($members_list) > 0) {
                                foreach ($members_list as $row) {
                            ?>
                                <tr>
                                    <td style='text-align:center;padding-left:0; vertical-align:middle;'>
                                        <input type='checkbox' id='select_<?php echo $no_count?>' name='select_id' value="<?php echo $row['mem_no']?>">
                                    </td>
                                    <td style='text-align:center;padding-left:0;vertical-align:middle;'>
                                        <?php echo $row['mem_no']?>
                                    </td>
                                    <td style='padding-left:0;' class='center'>
                                        <a class="btn-link" style='cursor:pointer;' onclick="get_mem_category('<?php echo $row['mem_no']?>', '<?php echo $write_fl?>')">
                                            <?php echo $row['mem_nm']?>
                                        </a>
                                    </td>
                                    <td style='padding-left:0;' class='center'><?php echo $row['mem_id']?></td>
                                    <td style='padding-left:0;' class='center'><?php echo $row['group_nm']?></td>
                                </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="center">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box" >
                            <li><a href="javascript:page_change(100)">100</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(10)">10</a></li>
                        </ul>
                    </div>
                    <?php
                    /*페이징처리*/
                    echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </div>
        </form>
        <!-- 회원 리스트 영역 -->
        <!-- 회원권한 리스트 영역 -->
        <!-- 권한탬플릿선택영역 -->
        </div>
        <div style="width:55%;float:left;">
            <!-- 권한그룹선택영역 -->
            <!-- ajax 카테고리 리스트 불러오는 영역 -->
            <!-- 검색 영역 -->
            <div class="panel-body" style="padding-bottom:20px;">
            	
                <div class="camp_info pa_l10 hei_45 wid_96p"><!--전체관리  -->
                   <div class="btn-group float_l ma_l10">
                        <input type="hidden" name="category_template" id="group_no_temp">
		                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
		                    <span id="group_no_sel_temp">&nbsp; <?php echo $select_category_template[0]['group_nm']?> &nbsp;</span> 
		                    <i class="dropdown-caret fa fa-caret-down"></i>
		                </button>
		                <ul class="dropdown-menu ul_sel_box_pa">
		                    <?php foreach($select_category_template as $sel){?>
		                        <li>
		                            <a href="javascript:sel_group('<?php echo $sel['group_no'];?>', '<?php echo $sel['group_nm'];?>', 'temp')">
	                                    <?php echo $sel['group_nm'];?>
		                            </a>
		                        </li>
		                    <?php }?>
	                    </ul>
                    </div>
                    <div class="wid_37p float_r">
	                    <input type="button" class="btn btn-primary" onclick='get_temp_category($("#group_no_temp").val());' value="<?php echo lang('strModify')?>"/>
	                    <input type="button" class="btn btn-info" onclick="get_temp_category(0);" value="<?php echo lang('strAdd')?>"/>
	                    <input type="button" class="btn btn-dark font_bold" onclick='delete_temp_category($("#group_no_temp").val());' value="<?php echo lang('strDelete')?>"/>
                    </div>
                </div>
                <div id="ajax_category">
                    <h4><span id="mem_nm"></span>&nbsp;</h4>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

	$(document).ready(function() {
    	sel_group("<?php echo $select_category_template[0]['group_no'];?>", "<?php echo $select_category_template[0]['group_nm'];?>", 'apply');
    	sel_group("<?php echo $select_category_template[0]['group_no'];?>", "<?php echo $select_category_template[0]['group_nm'];?>", 'temp');
	})
    
    var chkstate = false;
    function all_check(){
        var objSel = document.getElementsByName("select_id");
        var chk = false;

        if(!chkstate){
            chk = true;
            chkstate = true;
        }else{
            chk = false;
            chkstate = false;
        }
        for(var i=0; i < objSel.length; i++){
            objSel[i].checked = chk;
        }
    }

    function sel_group(group_no, group_nm, kind){

        if (group_no == ""){
        	group_nm = "<?php echo lang('strAll')?>"
        }
        $("#group_no_"+kind).val(group_no);
        $("#group_no_sel_"+kind).html(group_nm);
        if (kind == "search"){
            $("#group_nm").val(group_nm);
        }
        
    } 
    
    function apply(){
        var objSel = document.getElementsByName("select_id");
        var group_no = $("#group_no_apply").val();

        var chkSel = false;
        for(var i=0; i < objSel.length; i++){
            if(objSel[i].checked == true){
                chkSel = true;
                break;
            }
        }
        if(chkSel == false){
            alert("Check Choice!");
            return false;
        }
        if(confirm('Apply OK!?')){
            var mem_no = "";
            for(var i=0; i < objSel.length; i++){
                if(objSel[i].checked == true){
                    mem_no += ","+objSel[i].value;
                }
            }
            mem_no = mem_no.substring(1, mem_no.length);

            $.post("/admin/authority_apply",{
                    mem_no:mem_no, group_no:group_no
                },
                function(data){
                    if(data.trim() == '1'){
                        var frm = document.authority;
                        frm.submit();
                    }else{
                        alert('Error!!');
                        var frm = document.authority;
                        frm.submit();
                    }
                })
        }
    }

    function view(opt){
        var url = "/admin/authority_management";
        var frm = document.authority;
        frm.start.value = 0;
        frm.count.value = opt.options[opt.selectedIndex].value;
        frm.action = url;
        frm.submit();
    }

    function search(){
        var url = "/admin/authority_management";
        var frm = document.authority;
        frm.action = url;
        frm.submit();
    }

    function get_temp_category(group_no){
        $.ajax({
            url : "/admin/authority_template/"+group_no,
            dataType : "html",
            type : "post",
            data : { group_no:group_no },
            success : function(result){
                $("#ajax_category").html(result);
            }
        });
    }

    function get_mem_category(mem_no, write_fl ){
        $.ajax({
            url : "/admin/authority_member/"+mem_no,
            dataType : "html",
            type : "post",  // post 또는 get
            data : { mem_no:mem_no, write_fl:write_fl },   // 호출할 url 에 있는 페이지로 넘길 파라메터
            success : function(result){
                $("#ajax_category").html(result);
            }
        });
    }

    function delete_temp_category(group_no){
        //var group_no = $("#category_template option:selected").val();
        if(confirm('Delete OK!?')){
            $.post("/admin/authority_delete_temp",{
                    group_no:group_no
                },
                function(data){
                    if(data.trim() == '1'){
                        alert('Success');
                        var frm = document.authority;
                        frm.submit();
                    }else{
                        alert('Error!!');
                        var frm = document.authority;
                        frm.submit();
                    }
                }
            )
        }
    }

    //페이징 스크립트 시작
    function page_change(row){
        frm = document.authority;
        frm.per_page.value = row;
        frm.submit();
    }

    function paging(number){
        var frm = document.authority;
        frm.action = "/admin/authority_management/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝

</script>
<!-- 카테고리 항목 -->


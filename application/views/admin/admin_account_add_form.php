<div class="modal-header">
    <h4 class="h4_blit ma_b5 ma_t5">계정추가</h4>
</div>
<div class="modal-body">
    <h5 class="sub_tit_blit ma_l10" ></h5>
    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb ma_b30">
    	<colgroup>
    		<col width="15%">
    		<col width="35%">
    		<col width="15%">
    		<col width="35%">
    	</colgroup>
        <tr>
            <th class="point_blue"">관리자명</th>
            <td ><input type="text" id="mem_nm" name="mem_nm"/></td>
            <th class="point_blue bd_l">닉네임</th>
            <td ><input type="text" id="mem_nick_nm" name="mem_nick_nm"/></td>
        </tr>
        <tr>
            <th class="point_blue">구분</th>
            <td >
                <select id="group_no" name="group_no" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                    <?php
                        foreach($admin_group_list as $group){
                    ?>
                    <option value="<?php echo $group['group_no']?>"><?php echo $group['group_nm']?></option>
                    <?php
                        }
                    ?>
                </select>
            </td>
            <th class="point_blue bd_l">연락처</th>
            <td >
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_tel_name">
                        &nbsp;선택&nbsp;
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <input type="hidden" id="sel_mem_tel">
                    <ul class="dropdown-menu ul_sel_box">
                    <?php
                        foreach($tel_no_list as $row){
                            $code_desc = $row['code_desc'];
                    ?>
                        <li>
                            <a href="javascript:sel_mem_tel('<?php echo $code_desc;?>')">
                                <?php echo $code_desc;?>
                            </a>
                        </li>
                    <?php
                        }
                    ?>
                    </ul>
                </div>
                <span class="float_l ma_t8">&nbsp;&ndash;&nbsp; </span>
                <input type="text" class="wid_30p float_l" id="mem_tel1" name="mem_tel1" maxlength="4">
                <span class="float_l ma_t8"> &nbsp;&ndash;&nbsp;  </span>
                <input type="text" class="wid_30p float_l" id="mem_tel2" name="mem_tel2" maxlength="4">
            </td>
        </tr>
        <tr>
            <th class="point_blue">아이디</th>
            <td  ><input type="text" id="mem_id" name="mem_id"/></td>
            <th class="point_blue bd_l">패스워드</th>
            <td ><input type="password" id="mem_pwd" name="mem_pwd"/></td>
        </tr>
    </table>
</div>
<div class="modal-footer ma_t8">
    <span onclick="admin_account_add();" id="admin_account_add_btn" class="btn btn-primary"><?php echo lang('strDone')?></span>
    <span data-dismiss="modal" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>
<script type="text/javascript">
    function sel_mem_tel(code){
        $("#sel_mem_tel_name").empty();
        $("#sel_mem_tel_name").append("&nbsp;"+code+"&nbsp;<i class='dropdown-caret fa fa-caret-down'></i>");
        $("#sel_mem_tel").attr("value", code);
    }

    function admin_account_add(){
        $("#admin_account_add_btn").hide();
        if($("#mem_nm").val() == ""){
            alert("관리자명을 입력해주세요.");
            /*
           bootbox.dialog({
                message: "관리자명을 입력해주세요.",
                //title: "Custom title",
                onEscape: function() {$("#mem_nm").focus();},
                show: true,
                backdrop: true,
                closeButton: false,
                animate: true,
                className: "my-modal",
                buttons: {
                    "확인": {
                        className: "btn-danger"
                        //callback: function() {}
                    }
                }
            });
            */
            $("#mem_nm").focus();

            return;
        }

        if($("#mem_nick_nm").val() == ""){
            alert("닉네임을 입력해주세요.");
            $("#mem_nick_nm").focus();
            return;
        }

        if($("#sel_mem_tel").val() == ""){
            alert("연락처를 입력해주세요.");
            $("#sel_mem_tel").focus();
            return;
        }

        if($("#mem_tel1").val() == ""){
            alert("연락처를 입력해주세요.");
            $("#mem_tel1").focus();
            return;
        }

        if($("#mem_tel2").val() == ""){
            alert("연락처를 입력해주세요.");
            $("#mem_tel2").focus();
            return;
        }

        if($("#mem_id").val() == ""){
            alert("아이디를 입력해주세요.");
            $("#mem_id").focus();
            return;
        }

        if($("#mem_pwd").val() == ""){
            alert("패스워드를 입력해주세요.");
            $("#mem_pwd").focus();
            return;
        }

        var url = '/admin/admin_account_add';
        $.post(url,
            {
                group_no: $("#group_no").val(),
                mem_nm: $("#mem_nm").val(),
                mem_nick_nm: $("#mem_nick_nm").val(),
                sel_mem_tel: $("#sel_mem_tel").val(),
                mem_tel1 : $("#mem_tel1").val(),
                mem_tel2 : $("#mem_tel2").val(),
                mem_id : $("#mem_id").val(),
                mem_pwd : $("#mem_pwd").val()
            },
            function(data){
                if(data.trim() == "ok"){
                    alert("등록 완료");
                    $('#modal_content').html("");
                    $(this).data('bs.modal', null);
                    location.reload();
                }else{
                    alert("등록 실패");
                }
            }
        );
    }
</script>

<div class="modal" id="modal_account" role="dialog" tabindex="-1" aria-labelledby="demo-default-modal" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog bd-t7 modal-lg">
        <div class="modal-content" id="modal_account_content"></div>
    </div>
</div>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
		<ol class="breadcrumb">
			<li><a href="#">Home</a></li>
			<li><a href="#"><?php echo lang('strAdmin')?></a></li>
			<li class="active"><?php echo lang('strAdminAccount')?></li>
		</ol>
	</div>
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strAdminAccount')?></h3>
	</div>
	<div class="panel-body">
        <!--
	    <div class="clear" id="all_btn_gp" style="margin-bottom:5px;">
	       <a href="javascript:$('#modal_account').modal('show');" class="btn btn-primary">계정생성</a>&nbsp;
        </div>
        -->
        <form name="admin_account_list"  method="post">
            <div>
                <table class="table table-bordered aut_tb">
                	<colgroup>
                		<col width="20%">
                		<col width="80%">
                	</colgroup>
                    <tr>
                        <th>
                            관리자명
                        </th>
                        <td>
                            <input type="text" id="cond_admin_nm" name="cond_admin_nm"/>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            구분
                        </th>
                        <td>
                             <!--    <div class="btn-group float_l">
			                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="cam_status_sel">
			                    &nbsp; 전체 &nbsp; 
			                    <i class="dropdown-caret fa fa-caret-down"></i>
			                </button>
				                <ul class="dropdown-menu ul_sel_box_pa">
				                    <?php foreach($sel_status as $sel){?>
				                        <li>
				                            <a href="#" onclick="campaign_status_change('<?php echo $sel['code_key'];?>','all')">
				                                <font color="<?php if($sel['code_key']=="1"){echo "#ffa200";}elseif($sel['code_key']=="2"){echo "6ae6ce";}elseif($sel['code_key']=="3"){echo "#5177bc";}else{echo "#e2918f";}?>">
				                                    <?php echo lang($sel['code_desc']);?>
				                                </font>
				                            </a>
				                        </li>
				                    <?php }?>
				                    <li>
				                        <a href="#" onclick="campaign_status_change('del','all')">
				                            <?php echo lang('strDelete');?>
				                        </a>
				                    </li>
				                    <li>
				                        <a href="#" onclick="campaign_status_change('copy','all')">
				                            <?php echo lang('strCopy');?>
				                        </a>
				                    </li>
				                    </ul>
				                    <span class="color_r"> 셀렉트박스 적용해야해요 !!</span>
                    </div>
                        
               -->
                        
                        
                            <select id="cond_group_no" name="cond_group_no" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                                <option value="">전체</option>
                                <?php
                                    foreach($admin_group_list as $group){
                                ?>
                                <option value="<?php echo $group['group_no']?>"><?php echo $group['group_nm']?></option>
                                <?php
                                    }
                                ?>
                            </select>
			                            
			                 <div class="float_r ma_r5">
			                    <button class="btn btn-primary" onclick="this.form.submit();">검색</button>
			                </div>
                
                        </td>
                    </tr>
                </table>
                 <div class="float_r ma_l5 ma_b10">
                    <a class="btn btn-default" href="javascript:admin_account_add_form();">계정추가</a>
                </div>
     
                
                
           
            </div>
            <input type="hidden" name="cre_no" id="cre_no" value="">
            <input type="hidden" name="admin_no" id="admin_no" value="<?php echo $mem_no;?>">
            <input type="hidden" name="mem_no" id="mem_no" value="">
            <input type="hidden" name="cre_evaluation" id="cre_evaluation" value="">
            <table id="list" name="list" class="inspection_tb" data-horizontal-width="100%"  cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="8%">
                    <col width="22%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                </colgroup>
                <thead>
                    <tr>                                                                                                                                    
                        <th>번호</th>
                        <th>관리자명</th>
                        <th>구분</th>
                        <th>가입일</th>
                        <th>상태</th>
                   </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($exchange_list as $key=>$row){
                            $mem_fl = $row['mem_fl'];
                            $mem_no = $row['mem_no'];
                            $mem_nm = $row['mem_nm'];
                            $mem_nick_nm = $row['mem_nick_nm'];
                            $group_no = $row['group_no'];
                            $mem_tel = $row['mem_tel'];
                            $mem_id = $row['mem_id'];
                    ?>
                    <tr>
                        <td class="txt_center pa_l0"><?php echo $row['mem_no']?></td>
                        <td><a class="btn-link" href="javascript:admin_account_modify_form('<?php echo $mem_fl?>', '<?php echo $mem_no?>', '<?php echo $mem_nm?>', '<?php echo $mem_nick_nm?>', '<?php echo $group_no?>', '<?php echo $mem_tel?>', '<?php echo $mem_id?>');"><?php echo $row['mem_nm']?></a></td>
                        <td class="txt_center pa_l0">
                            <?php echo $row['group_nm']?>
                        </td>
                        <td class="txt_center pa_l0">
                            <?php echo substr($row['mem_ymd'], 0, 10)?>
                        </td>
                        <td class="txt_center pa_l0">
                            <?php
                            if($row['mem_fl'] == "N"){
                                echo "진행";
                            }elseif($row['mem_fl'] == "Y"){
                                echo "탈퇴";
                            }
                            ?>
                        </td>
                    </tr>
                   <?php
                        }
                    ?>
                </tbody>
            </table>

            <div class="center">
                <div class="btn-group float_r">
                    <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                        <span id="bank_sel_view"> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu ul_sel_box" >
                        <li><a href="javascript:page_change(100)">100</a></li>
                        <li><a href="javascript:page_change(50)">50</a></li>
                        <li><a href="javascript:page_change(25)">25</a></li>
                        <li><a href="javascript:page_change(10)">10</a></li>
                    </ul>
                </div>
                <?php
                /*페이징처리*/
                    echo $page_links;
                /*페이징처리*/
                ?>
            </div> 
        </form>
   </div>
</div>
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm = document.admin_account_list;
        frm.per_page.value = row;
        frm.submit();
    }

    function paging(number){
        var frm = document.admin_account_list;
        frm.action = "/admin/admin_account/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝

    $("#modal_account").on('hidden.bs.modal', function () {
        $('#modal_account_content').html("");
        $(this).data('bs.modal', null);
    });

    function admin_account_add_form(){
        $.ajax({
            type:"POST",
            url:"/admin/admin_account_add_form",
            success: function (data){
                $('#modal_account_content').append(data);
                $("#modal_account").modal("show");
            }
        });
    }


    function admin_account_modify_form(mem_fl, mem_no, mem_nm, mem_nick_nm, group_no, mem_tel, mem_id){
        $.ajax({
            type:"POST",
            url:"/admin/admin_account_modify_form",
            data : {mem_fl : mem_fl, mem_no : mem_no, mem_nm : mem_nm, mem_nick_nm : mem_nick_nm, group_no : group_no, mem_tel : mem_tel, mem_id : mem_id},
            success: function (data){
                $('#modal_account_content').append(data);
                $("#modal_account").modal("show");
            }
        });
    }

</script>
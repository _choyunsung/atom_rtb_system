<div class="panel float_r wid_970 min_height">
    <div class="history_box">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li><a href="#"><?php echo lang('strAdmin')?></a></li>
            <li class="active"><?php echo lang('strMenuMgr')?></li>
        </ol>
    </div>
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strMenuMgr')?></h3>
    </div>
    <!-- 검색 영역 -->
    <div class="panel-body">
        <!-- 카테고리 항목 -->
        <table name="category_list" class= "inspection_tb" cellpadding="0" border="0" cellspacing="0" width="100%">
            <colgroup>
                <col width="8%">
                <col width="17%">
                <col width="15%">
                <col width="20%">
                <col width="15%">
                <col width="10%">
                <col width="7%">
                <col width="8%">
            </colgroup>
            <thead>
                <tr>
                    <th>cate_no</th>
                    <th>cate_explan</th>
                    <th>cate_nm</th>
                    <th>cate_url</th>
                    <th>cate_parent_no</th>
                    <th>cate_seq</th>
                    <th>cate_fl</th>
                    <th>action</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if(count($cate_rows > 0)) {
                    foreach($cate_rows as $key=>$row){
            ?>
                <tr id="cate_<?php echo $key?>" style="<?php if($row['cate_parent_no'] == 0){ echo "color:#5696ca;font-weight:bold;";}?>">
                    <td class="center pa_l0"><?php echo $row['cate_no']?></td>
                    <td><?php echo $row['cate_explan']?></td>
                    <td><?php echo $row['cate_nm']?></td>
                    <td><?php echo $row['cate_url']?></td>
                    <td class="center pa_l0" ><?php echo $row['cate_parent_no']?></td>
                    <td class="center pa_l0"><?php echo $row['cate_seq']?></td>
                    <td class="center pa_l0" style='<?php if($row['cate_fl'] == "Y"){ echo "color:#d23c3c;";} ?>'><?php echo $row['cate_fl']?></td>
                    <td class="center pa_l0"><a href="javascript:cate_modify('<?php echo $key?>');">Modify</a></td>
                </tr>
                <tr id="cate_form_<?php echo $key?>" style="display:none;">
                    <form id="cate_modify_<?php echo $key?>" name="cate_modify_<?php echo $key?>" action="/admin/category_modify" method="post">
                        <input type="hidden" name="cate_form_type" value="modify"/>
                        <td>
                            <input type="hidden" name="cate_no" value="<?php echo $row['cate_no']?>"/>
                            <?php echo $row['cate_no']?>
                        </td>
                        <td>
                            <input type="text" class="wid_100p" name="cate_explan" value="<?php echo $row['cate_explan']?>"/>
                        </td>
                        <td>
                            <input type="text" class="wid_100p" name="cate_nm" value="<?php echo $row['cate_nm']?>"/>
                        </td>
                        <td>
                            <input type="text" class="wid_100p" name="cate_url" value="<?php echo $row['cate_url']?>"/>
                        </td>
                        <td>
                            <input type="text" class="wid_100p" name="cate_parent_no" value="<?php echo $row['cate_parent_no']?>"/>
                        </td>
                        <td>
                            <input type="text" class="wid_100p" name="cate_seq" value="<?php echo $row['cate_seq']?>"/>
                        </td>
                        <td>
                            <input type="text" class="wid_100p" name="cate_fl" value="<?php echo $row['cate_fl']?>"/>
                        </td>
                        <td class="center pa_l0"><input type="button" onclick="this.form.submit();" value="Save">&nbsp;<a href="javascript:cate_cancel('<?php echo $key?>');"><?php echo lang('strCancel')?></a></td>
                    </form>
                </tr>
            <?php
                    }
                }
            ?>
            </tbody>
        </table>
        <form id="cate_add" name="cate_add" action="/admin/category_add" method="post">
            <table name="category_add_form" class="aut_tb"  width="100%" cellpadding="0" cellspacing="0" border="0">
                <colgroup>
                    <col width="8%">
                    <col width="17%">
                    <col width="15%">
                    <col width="20%">
                    <col width="15%">
                    <col width="10%">
                    <col width="7%">
                    <col width="8%">
                </colgroup>
                <thead>
                    <tr>
                        <th>cate_no</th>
                        <th>cate_explan</th>
                        <th>cate_nm</th>
                        <th>cate_url</th>
                        <th>cate_parent_no</th>
                        <th>cate_seq</th>
                        <th>cate_fl</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <input type="hidden"name="cate_form_type" value="add"/>
                        <td class="center pa_l0">
                            자동생성
                        </td>
                        <td>
                            <input type="text"  class="wid_100p" name="cate_explan" value=""/>
                        </td>
                        <td>
                            <input type="text"  class="wid_100p" name="cate_nm" value=""/>
                        </td>
                        <td>
                            <input type="text"  class="wid_100p" name="cate_url" value=""/>
                        </td>
                        <td>
                            <input type="text"  class="wid_100p" name="cate_parent_no" value=""/>
                        </td>
                        <td>
                            <input type="text"  class="wid_100p" name="cate_seq" value=""/>
                        </td>
                        <td>
                            <input type="text"  class="wid_100p" name="cate_fl" value=""/>
                        </td>
                        <td class="center pa_l0">
                            <input type="submit" class="btn-primary" value="Add">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <script type="text/javascript">
        function cate_modify(key){
            var cate_obj = document.getElementById("cate_"+key);
            var cate_form_obj = document.getElementById("cate_form_"+key);
            cate_obj.style.display = "none";
            cate_form_obj.style.display = "";
        }

        function cate_cancel(key){
            var cate_obj = document.getElementById("cate_"+key);
            var cate_form_obj = document.getElementById("cate_form_"+key);
            cate_obj.style.display = "";
            cate_form_obj.style.display = "none";
        }
    </script>
</div>


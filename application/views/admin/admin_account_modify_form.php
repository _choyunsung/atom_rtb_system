<div class="modal-header">
    <h4 class="h4_blit ma_b5 ma_t5">계정수정</h4>
</div>  
<div class="modal-body">
    <h5 class="sub_tit_blit ma_l10" ></h5>
    <input type="hidden" id="mem_no" name="mem_no" value="<?php echo $mem_no?>"/>
    <table width="100%" border="0" cellspacign="0" cellpadding="0" class="join_tb ma_b30">
        	<colgroup>
	    		<col width="15%">
	    		<col width="35%">
	    		<col width="15%">
	    		<col width="35%">
   	 		</colgroup>
        <tr>
            <th class="point_blue">관리자명</th>
            <td><input type="text" id="mem_nm" name="mem_nm" value="<?php echo $mem_nm?>"/></td>
            <th class="point_blue bd_l">닉네임</th>
            <td><input type="text" id="mem_nick_nm" name="mem_nick_nm" value="<?php echo $mem_nick_nm?>"/></td>
        </tr>
        <tr>
            <th class="point_blue">구분</th>
            <td>
               <!--     <div class="btn-group float_l">
			                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="cam_status_sel">
			                    &nbsp; 운영자 &nbsp; 
			                    <i class="dropdown-caret fa fa-caret-down"></i>
			                </button>
				                <ul class="dropdown-menu ul_sel_box_pa">
				                    <?php foreach($sel_status as $sel){?>
				                        <li>
				                            <a href="#" onclick="campaign_status_change('<?php echo $sel['code_key'];?>','all')">
				                                <font color="<?php if($sel['code_key']=="1"){echo "#ffa200";}elseif($sel['code_key']=="2"){echo "6ae6ce";}elseif($sel['code_key']=="3"){echo "#5177bc";}else{echo "#e2918f";}?>">
				                                    <?php echo lang($sel['code_desc']);?>
				                                </font>
				                            </a>
				                        </li>
				                    <?php }?>
				                    <li>
				                        <a href="#" onclick="campaign_status_change('del','all')">
				                            <?php echo lang('strDelete');?>
				                        </a>
				                    </li>
				                    <li>
				                        <a href="#" onclick="campaign_status_change('copy','all')">
				                            <?php echo lang('strCopy');?>
				                        </a>
				                    </li>
				                    </ul>
				                    <span class="color_r"> 셀렉트박스 적용해야해요 !!</span>
                    </div>-->
            
       
                <select id="group_no" name="group_no" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                    <?php
                        foreach($admin_group_list as $group){
                    ?>
                    <option value="<?php echo $group['group_no']?>" <?if($group['group_no'] == $group_no){ echo "selected"; }?>><?php echo $group['group_nm']?></option>
                    <?php
                        }
                    ?>
                </select>
            </td>
            <th class="point_blue bd_l">연락처</th>
            <td>
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_mem_tel_name">
                        &nbsp;선택&nbsp;
                        <i class="dropdown-caret fa fa-caret-down"></i>
                    </button>
                    <input type="hidden" id="sel_mem_tel">
                    <ul class="dropdown-menu ul_sel_box">
                    <?php
                        foreach($tel_no_list as $row){
                            $code_desc = $row['code_desc'];
                    ?>
                        <li>
                            <a href="javascript:sel_mem_tel('<?php echo $code_desc;?>')">
                                <?php echo $code_desc;?>
                            </a>
                        </li>
                    <?php
                        }
                    ?>
                    </ul>
                </div>
                <span class="float_l ma_t8">&nbsp;-&nbsp;</span>
                <input type="text" class="wid_30p float_l" id="mem_tel1" name="mem_tel1" maxlength="4" value="<?php echo $mem_tel_1;?>">
                <span class="float_l ma_t8">&nbsp;-&nbsp;</span>
                <input type="text" class="wid_30p float_l" id="mem_tel2" name="mem_tel2" maxlength="4" value="<?php echo $mem_tel_2;?>">
            </td>
        </tr>
        <tr>
            <th class="point_blue">아이디</th>
            <td><?php echo $mem_id;?></td>
            <th class="point_blue bd_l">패스워드</th>
            <td><input type="text" id="mem_pwd" name="mem_pwd"/></td>
        </tr>
        <tr>
            <th class="point_blue">상태</th>
            <td colspan="5">
               <div class="btn-group float_l">
			                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="cam_status_sel">
			                    &nbsp; 진행 &nbsp; 
			                    <i class="dropdown-caret fa fa-caret-down"></i>
			                </button>
				                <ul class="dropdown-menu ul_sel_box_pa">
				                    <?php foreach($sel_status as $sel){?>
				                        <li>
				                            <a href="#" onclick="campaign_status_change('<?php echo $sel['code_key'];?>','all')">
				                                <font color="<?php if($sel['code_key']=="1"){echo "#ffa200";}elseif($sel['code_key']=="2"){echo "6ae6ce";}elseif($sel['code_key']=="3"){echo "#5177bc";}else{echo "#e2918f";}?>">
				                                    <?php echo lang($sel['code_desc']);?>
				                                </font>
				                            </a>
				                        </li>
				                    <?php }?>
				                    <li>
				                        <a href="#" onclick="campaign_status_change('del','all')">
				                            <?php echo lang('strDelete');?>
				                        </a>
				                    </li>
				                    <li>
				                        <a href="#" onclick="campaign_status_change('copy','all')">
				                            <?php echo lang('strCopy');?>
				                        </a>
				                    </li>
				                    </ul>
                    </div>
            <!-- 
                <select id="mem_fl" name="mem_fl" class="form-control-static input-sm" style="background-color:#FFFFFF;">
                    <option value="N" <?if($mem_fl == "N"){ echo "selected"; }?>>진행</option>
                    <option value="Y" <?if($mem_fl == "Y"){ echo "selected"; }?>>탈퇴</option>
                </select>-->
            </td>
        </tr>
    </table>
</div>
<div class="modal-footer ma_t8">
    <span onclick="admin_account_modify();" class="btn btn-primary"><?php echo lang('strDone')?></span>
    <span data-dismiss="modal" class="btn btn-dark"><?php echo lang('strClose')?></span>
</div>
<script type="text/javascript">

    sel_mem_tel('<?php echo $mem_tel_0?>');

    function sel_mem_tel(code){
        $("#sel_mem_tel_name").empty();
        $("#sel_mem_tel_name").append("&nbsp;"+code+"&nbsp;<i class='dropdown-caret fa fa-caret-down'></i>");
        $("#sel_mem_tel").attr("value", code);
    }

    function admin_account_modify(){
        if(confirm("수정하시겠습니까?")){
            if($("#mem_nm").val() == ""){
                alert("관리자명을 입력해주세요.");
                $("#mem_nm").focus();
                return;
            }

            if($("#mem_nick_nm").val() == ""){
                alert("닉네임을 입력해주세요.");
                $("#mem_nick_nm").focus();
                return;
            }

            if($("#sel_mem_tel").val() == ""){
                alert("연락처를 입력해주세요.");
                $("#sel_mem_tel").focus();
                return;
            }

            if($("#mem_tel1").val() == ""){
                alert("연락처를 입력해주세요.");
                $("#mem_tel1").focus();
                return;
            }

            if($("#mem_tel2").val() == ""){
                alert("연락처를 입력해주세요.");
                $("#mem_tel2").focus();
                return;
            }

            var url = '/admin/admin_account_modify';
            $.post(url,
                {
                    mem_fl: $("#mem_fl").val(),
                    mem_no: $("#mem_no").val(),
                    group_no: $("#group_no").val(),
                    mem_nm: $("#mem_nm").val(),
                    mem_nick_nm: $("#mem_nick_nm").val(),
                    sel_mem_tel: $("#sel_mem_tel").val(),
                    mem_tel1 : $("#mem_tel1").val(),
                    mem_tel2 : $("#mem_tel2").val(),
                    mem_pwd : $("#mem_pwd").val()
                },
                function(data){
                    if(data.trim() == "ok"){
                        alert("수정 완료");
                        $('#modal_content').html("");
                        $(this).data('bs.modal', null);
                        location.reload();
                    }else{
                        alert("수정 실패");
                    }
                }
            );
        }
    }
</script>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard | Nifty - Responsive admin template.</title>


	<!--STYLESHEET-->
	<!--=================================================-->

	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


	<!--Bootstrap Stylesheet [ REQUIRED ]-->  
	<link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">


	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="/template/bootstrap/css/nifty.css" rel="stylesheet">

	 
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">


	<!--Demo [ DEMONSTRATION ]-->
	<link href="/template/bootstrap/css/demo/nifty-demo.css" rel="stylesheet">

    <!--jQuery [ REQUIRED ]-->
	<script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>

	<!-- Highcharts -->
	<script src="/template/plugins/Highcharts-4.1.5/js/highcharts.js"></script>

	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="/template/plugins/pace/pace.css" rel="stylesheet">
	<script src="/template/plugins/pace/pace.js"></script>
		

</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
	<div id="container" class="effect mainnav-lg">
		
		<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">
			
			
			<div class="nav_mid">
				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">
					<a href="/member/test_page" class="navbar-brand">
						
						<div class="brand-title">
							<img src="/img/m_logo.png">
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->


			<div class="navbar-content clearfix">
				<div class="navi_02">
					<ul>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						
					</ul>
				</div>
				<div class="navi_03">
						<ul class="nav navbar-top-links pull-left">
							<li class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_04.png">
									<span class="badge badge-header badge-warning">9</span>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_03.png">
									<span class="badge badge-header badge-warning">9</span>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_02.png">
								</a>
							</li>
			
							<li class="dropdown wid_30p">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_01.png">	<span class="user_name">정주환</span>
								</a>
							</li>
						</ul>
					</div>
			</div>
				<!--================================-->
				<!--End Navbar Dropdown-->
				</div>
			</div>
		</header>
		<!--===================================================-->
		<!--END NAVBAR-->

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				


				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="row">
						<div class="col-lg-12">
							<!--Chat Widget-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="wrap">
							
														
									<!--MAIN NAVIGATION-->
									
									<!--===================================================-->
									
									<nav id="mainnav-container">
									
										<div id="mainnav">
									
									<!--Menu-->
									
									<!--================================-->
									
											<div id="mainnav-menu-wrap">
											
									
												<div class="nano">
												
													<div class="nano-content">
													
														<ul id="mainnav-menu" class="list-group">
													
															<!--Category name-->
																<li style="background:url('/img/left_nav_bg.png');font-weight:bold; text-align:center; padding-top:25px; height:68px; color:#fff; font-size:2em;">광고관리</li>
																												
															<!--Menu list item-->
															
																<li class="active-link bd_b_li">
															
																	<a href="index.html">
															
																		<i class="fa fa-dashboard"></i>
																	
																			<span class="menu-title">
																	
																				<strong>캠페인</strong>
																		
																				<span class="label label-success pull-right">Top</span>
																		
																			</span>
																	
																	</a>
															
																</li>
															
															<!--Menu list item-->
															
																<li>
																	<a href="#">
																	<i class="fa fa-th"></i>
																	<span class="menu-title">
																		<strong>광고그룹</strong>
																	</span>
															
																	<i class="arrow"></i>
															
																	</a>
															
																</li>
															
																<li>
																	<a href="#">
																	<i class="fa fa-th"></i>
																	<span class="menu-title">
																		<strong>광고</strong>
																	</span>
															
																	<i class="arrow"></i>
															
																	</a>
															
																</li>
																
																													
																<li>
																	<a href="#">
																	<i class="fa fa-th"></i>
																	<span class="menu-title">
																		<strong>광고관리</strong>
																	</span>
															
																	<i class="arrow"></i>
															
																	</a>
															
																</li>													
															<!--Menu list item-->
															
															
												
														
									
									</ul>
									
									</div>
									
									</div>
									
									</div>
									
									<!--================================-->
									
									<!--End menu-->
									
									</div>
									
									</nav>
									
									<!--===================================================-->
									
									<!--END MAIN NAVIGATION-->			
									
						
							
								<div class="panel" style="width:920px; float:right;">
										<div class="history_box">
											<ol class="breadcrumb">
												<li><a href="#">Home</a></li>
												<li><a href="#">Library</a></li>
												<li class="active">Data</li>
											</ol>
										</div>	

						        		<div class="panel-body">
						        		
						        			<div class="tab-base">
					
											<!--Nav Tabs-->
											<ul class="nav nav-tabs">
												<li class="active">
													<a data-toggle="tab" href="#demo-lft-tab-1" aria-expanded="true">마스터 정보 </a>
												</li>
												<li class="">
													<a data-toggle="tab" href="#demo-lft-tab-2" aria-expanded="false">매니저 리스트</a>
												</li>
												<li>
													<a data-toggle="tab" href="#demo-lft-tab-3">로그인 이력</a>
												</li>
											</ul>
								
											<!--Tabs Content-->
											<div class="tab-content">
												<div id="demo-lft-tab-1" class="tab-pane fade active in">
													<p class="txt-right">
														<span class="btn btn-primary">마스터 정보 수정</span>
													</p>
											
													<div class="panel-heading">
									    				<h3 class="page-header text-overflow tit_blit0">업체정보</h3>
									    			</div>
												
													<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
														<colgroup>
															<col width="20%">
															<col width="30%">
															<col width="20%">
															<col width="30%">
														</colgroup>
														<tr>
															<th>아이디</th>
															<td>adop</td>
															<th>비밀번호</th>
															<td>****</td>
														</tr>
														<tr>
															<th>법인명</th>
															<td>애드오피</td>
															<th>회사명</th>
															<td>애드오피</td>
														</tr>														
														<tr>
															<th>사업자 등록번호</th>
															<td colspan="3">123-12-1234</td>
														</tr>													
														<tr>
															<th>대표자명</th>
															<td colspan="3">홍길동</td>
														</tr>
														<tr>
															<th>업태 / 종목</th>
															<td colspan="3">서비스 / 전자상거래</td>
														</tr>																										
														<tr>
															<th>주소</th>
															<td colspan="3">(135-848) 서울 강남구 테헤란로 86길 14 (대치동, 윤천빌딩) 5층</td>
														</tr>													
													</table>
												
												
													<div class="panel-heading">
									    				<h3 class="page-header text-overflow tit_blit0">환불계좌 정보</h3>
									    			</div>															
													<table class="aut_tb" cellpaddig="0"  cellspacing="0">
														<colgroup>
															<col width="20%">
															<col width="*">
														
														</colgroup>
														<tr>
															<th>환불계좌 정보</th>
															<td class="join_info">수정 버튼을 클릭하여, 환불계좌 정보를 입력해 주세요.</td>
														</tr>
																				
													</table>									
												
												
												
													<div class="panel-heading">
									    				<h3 class="page-header text-overflow tit_blit0">세금계산서 담당자 정보</h3>
									    			</div>
												
													<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
														<colgroup>
															<col width="20%">
															<col width="*">

														</colgroup>
														<tr>
															<th>담당자 이름</th>
															<td>adop</td>

														</tr>
														<tr>
															<th>이메일</th>
															<td>애드오피</td>

														</tr>														
														<tr>
															<th>사무실 번호</th>
															<td colspan="3">123-12-1234</td>
														</tr>													
														<tr>
															<th>휴대폰 번호</th>
															<td colspan="3">홍길동</td>
														</tr>
									
													</table>												
												
												
													<div class="panel-heading">
									    				<h3 class="page-header text-overflow tit_blit0">알림 설정</h3>
									    			</div>
												
													<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
														<colgroup>
															<col width="20%">
															<col width="*">

														</colgroup>
														<tr>
															<th>정보성 SMS</th>
															<td class="point_dgray">결제알림 <i class="fa fa-bell"></i></td>
															<td>
																<input type="checkbox"> 충전금액 10% 미만 시
																<input type="checkbox" class="ma_l20"> 일 허용 금액 10% 미만 시
																<input type="checkbox" class="ma_l20"> 충전금액 모두 소진 시
															</td>
														</tr>
														<tr>
															<th>정보성 이메일</th>
															<td class="point_dgray">결제알림 <i class="fa fa-bell"></i></td>															
															<td>
																<input type="checkbox"> 충전금액 10% 미만 시
																<input type="checkbox" class="ma_l20"> 일 허용 금액 10% 미만 시
																<input type="checkbox" class="ma_l20"> 충전금액 모두 소진 시
															</td>

														</tr>														

									
													</table>													
												
												
												<p class="pa_b20 ma_l10">
													해당 계정의 해지를 원하시면, 문의하기를 통해 문의해주시기 바랍니다. <span class="point_blue bold">문의하기 페이지로 이동</span> 
												</p>
												
												
												
												
												
												
												</div>
												<div id="demo-lft-tab-2" class="tab-pane fade">
													<h4 class="text-thin">Second Tab Content</h4>
													<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
												</div>
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												
												<div id="demo-lft-tab-3" class="tab-pane fade">
													<h4 class="text-thin">Third Tab Content</h4>
													<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
												</div>
											</div>
										</div>
																	
														
								
								
							</div>
						</div>
					</div>
					
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


			
		
			
			
			
			
		</div>
		<!-- FOOTER -->
		<!--===================================================-->
		<footer id="footer">

			<!-- Visible when footer positions are fixed -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<div class="show-fixed pull-right">
				<ul class="footer-list list-inline">
					<li>
						<p class="text-sm">SEO Proggres</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-danger"></div>
						</div>
					</li>

					<li>
						<p class="text-sm">Online Tutorial</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-primary"></div>
						</div>
					</li>
					<li>
						<button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
					</li>
				</ul>
			</div>



		</footer>
		<!--===================================================-->
		<!-- END FOOTER -->


		<!-- SCROLL TOP BUTTON -->
		<!--===================================================-->
		<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
		<!--===================================================-->



	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->



	
	<!--JAVASCRIPT--> 
	<!--=================================================-->

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/bootstrap.min.js"></script>


	<!--Fast Click [ OPTIONAL ]-->
	<script src="/template/plugins/fast-click/fastclick.min.js"></script>

	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/nifty.min.js"></script>


	<!--Background Image [ DEMONSTRATION ]-->
	<script src="/template/bootstrap/js/demo/bg-images.js"></script>

	<!--Modals [ SAMPLE ]-->
	<script src="/template/bootstrap/js/demo/ui-modals.js"></script>

</body>
</html>

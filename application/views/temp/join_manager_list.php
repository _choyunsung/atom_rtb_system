		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Page Title-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div id="page-title">
					<h1 class="page-header text-overflow tit_blit">운영자 관리</h1>

		
				</div>
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<!--End page title-->
					<div class="panel">
							<div class="panel-body">
													
	
								<div class="row">
									<div class="col-sm-6 table-toolbar-left" style="float:right; text-align:right;">
										<button id="demo-btn-addrow" class="btn btn-purple btn-labeled fa fa-plus">등록</button>
									</div>
									
								</div>
				
						</div>
							<table id="demo-dt-basic" class="table table-striped table-bordered" cellspacing="0">
								<colgroup>
									<col width="3%">
									
								</colgroup>
								<thead>
									<tr>
										<th>No</th>
										<th>ID</th>
										<th class="min-tablet">소속</th>
										<th class="min-tablet">이름</th>
										<th class="min-desktop">전화번호</th>
										<th class="min-desktop">e-mail</th>
										<th class="min-desktop">상태</th>
										<th class="min-desktop">등록일</th>		
								        <th class="min-desktop">수정/삭제</th>																													
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.cc</td>
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td>버튼/버튼</td>
										
									</tr>
									<tr>
										<td>2</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.cc</td>
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td>버튼/버튼</td>
										
									</tr>
								
									<tr>
										<td>2</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.cc</td>
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td>버튼/버튼</td>
										
									</tr>									
									
									<tr>
										<td>2</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.cc</td>
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td>버튼/버튼</td>
										
									</tr>									
									
									<tr>
										<td>2</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.cc</td>
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td>버튼/버튼</td>
										
									</tr>								
								</tbody>
							</table>
						</div>
						
					</div>
					<!--===================================================-->
					<!-- End Striped Table -->
					
							

						<!--===================================================-->
						<!--End Data Table-->
					
					</div>
					
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


			

		</div>

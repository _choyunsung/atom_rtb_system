<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard | Nifty - Responsive admin template.</title>


	<!--STYLESHEET-->
	<!--=================================================-->

	<!--Open Sans Font [ OPTIONAL ] -->
 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&amp;subset=latin" rel="stylesheet">


	<!--Bootstrap Stylesheet [ REQUIRED ]-->  
	<link href="/template/bootstrap/css/bootstrap.css" rel="stylesheet">


	<!--Nifty Stylesheet [ REQUIRED ]-->
	<link href="/template/bootstrap/css/nifty.css" rel="stylesheet">

	 
	<!--Font Awesome [ OPTIONAL ]-->
	<link href="/template/plugins/font-awesome/css/font-awesome.css" rel="stylesheet">


	<!--Demo [ DEMONSTRATION ]-->
	<link href="/template/bootstrap/css/demo/nifty-demo.css" rel="stylesheet">

    <!--jQuery [ REQUIRED ]-->
	<script src="/template/bootstrap/js/jquery-2.1.1.min.js"></script>

	<!-- Highcharts -->
	<script src="/template/plugins/Highcharts-4.1.5/js/highcharts.js"></script>

	<!--Page Load Progress Bar [ OPTIONAL ]-->
	<link href="/template/plugins/pace/pace.css" rel="stylesheet">
	<script src="/template/plugins/pace/pace.js"></script>
		

</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
	<div id="container" class="effect mainnav-lg">
		
		<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">
			
			
			<div class="nav_mid">
				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">
					<a href="/member/test_page" class="navbar-brand">
						
						<div class="brand-title">
							<img src="/img/m_logo.png">
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->


			<div class="navbar-content clearfix">
				<div class="navi_02">
					<ul>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						<li><a>홈</a></li>
						
					</ul>
				</div>
				<div class="navi_03">
						<ul class="nav navbar-top-links pull-left">
							<li class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_04.png">
									<span class="badge badge-header badge-warning">9</span>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_03.png">
									<span class="badge badge-header badge-warning">9</span>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_02.png">
								</a>
							</li>
			
							<li class="dropdown wid_30p">
								<a href="#" data-toggle="dropdown" class="dropdown-toggle">
									<img src="/img/top_nav_01.png">	<span class="user_name">정주환</span>
								</a>
							</li>
						</ul>
					</div>
			</div>
				<!--================================-->
				<!--End Navbar Dropdown-->
				</div>
			</div>
		</header>
		<!--===================================================-->
		<!--END NAVBAR-->

		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				


				<!--Page content-->
				<!--===================================================-->
				<div id="page-content">
					<div class="row">
						<div class="col-lg-12">
							<!--Chat Widget-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="wrap">
							
														
									<!--MAIN NAVIGATION-->
									
									<!--===================================================-->
									
									<nav id="mainnav-container">
									
										<div id="mainnav">
									
									<!--Menu-->
									
									<!--================================-->
									
											<div id="mainnav-menu-wrap">
											
									
												<div class="nano">
												
													<div class="nano-content">
													
														<ul id="mainnav-menu" class="list-group">
													
															<!--Category name-->
																<li style="background:url('/img/left_nav_bg.png');font-weight:bold; text-align:center; padding-top:25px; height:68px; color:#fff; font-size:2em;">광고관리</li>
																												
															<!--Menu list item-->
															
																<li class="active-link bd_b_li">
															
																	<a href="index.html">
															
																		<i class="fa fa-dashboard"></i>
																	
																			<span class="menu-title">
																	
																				<strong>캠페인</strong>
																		
																				<span class="label label-success pull-right">Top</span>
																		
																			</span>
																	
																	</a>
															
																</li>
															
															<!--Menu list item-->
															
																<li>
																	<a href="#">
																	<i class="fa fa-th"></i>
																	<span class="menu-title">
																		<strong>광고그룹</strong>
																	</span>
															
																	<i class="arrow"></i>
															
																	</a>
															
																</li>
															
																<li>
																	<a href="#">
																	<i class="fa fa-th"></i>
																	<span class="menu-title">
																		<strong>광고</strong>
																	</span>
															
																	<i class="arrow"></i>
															
																	</a>
															
																</li>
																
																													
																<li>
																	<a href="#">
																	<i class="fa fa-th"></i>
																	<span class="menu-title">
																		<strong>광고관리</strong>
																	</span>
															
																	<i class="arrow"></i>
															
																	</a>
															
																</li>													
															<!--Menu list item-->
															
															
												
														
									
									</ul>
									
									</div>
									
									</div>
									
									</div>
									
									<!--================================-->
									
									<!--End menu-->
									
									</div>
									
									</nav>
									
									<!--===================================================-->
									
									<!--END MAIN NAVIGATION-->			
									
						
							
								<div class="panel" style="width:920px; float:right;">
										<div class="history_box">
											<ol class="breadcrumb">
												<li><a href="#">Home</a></li>
												<li><a href="#">Library</a></li>
												<li class="active">Data</li>
											</ol>
										</div>	

						        		<div class="panel-body">
						        							
							    					<div class="panel-heading">
									    				<h3 class="page-header tit_blit0 wid_40p float_l">환경설정</h3>
									    																									
														<span class="btn btn-default float_r ma_t10">환경설정 수정</span>
													</div>
													                    
										<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
														<colgroup>
															<col width="20%">
															<col width="*">
														
														</colgroup>
														<tr>
															<th>사용자 ID</th>
															<td> M-12345678</td>
													    </tr>
														<tr>
															<th>표시 언어</th>
															<td>한국어</td>
														</tr>
														<tr>
															<th>표시 통화</th>
															<td>₩ (KRW)</td>
														</tr>														
														<tr>
															<th>표준 시간대</th>
															<td>GMT</td>
														</tr>														
																												
													
													</table>												
						
						
						
										                    
										<table class="aut_98_tb" cellpaddig="0" cellspacing="0">
														<colgroup>
															<col width="20%">
															<col width="*">
														
														</colgroup>
														<tr>
															<th>사용자 ID</th>
															<td> M-12345678</td>
													    </tr>
													    <tr>
															<th>표시 언어</th>
															<td>
																<select>
																	<option>
																		한국어
																	</option>
																</select>
															</td>
														</tr>
														<tr>
															<th>표시 통화</th>
															<td>
																<select>
																	<option>
																		₩ (KRW)
																	</option>
																</select>
															</td>
														</tr>														
														<tr>
															<th>표준 시간대</th>
															<td>
																<select>
																	<option>
																	GMT
																	</option>
																</select>
															</td>
														</tr>										
																												
													
													</table>												
						
											<p class="pa_b20 ma_l10 txt-right">
												<span class="btn btn-primary" onclick="master_info_modify_save()">저장</span> 
												<span class="btn btn-default">취소</span>
											</p>									
												
												
													
						                    									
														
											
										</div>
							</div>
						</div>
						
						
					
				</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


			
		
			
			
			
			
		</div>
		<!-- FOOTER -->
		<!--===================================================-->
		<footer id="footer">

			<!-- Visible when footer positions are fixed -->
			<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
			<div class="show-fixed pull-right">
				<ul class="footer-list list-inline">
					<li>
						<p class="text-sm">SEO Proggres</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-danger"></div>
						</div>
					</li>

					<li>
						<p class="text-sm">Online Tutorial</p>
						<div class="progress progress-sm progress-light-base">
							<div style="width: 80%" class="progress-bar progress-bar-primary"></div>
						</div>
					</li>
					<li>
						<button class="btn btn-sm btn-dark btn-active-success">Checkout</button>
					</li>
				</ul>
			</div>



		</footer>
		<!--===================================================-->
		<!-- END FOOTER -->


		<!-- SCROLL TOP BUTTON -->
		<!--===================================================-->
		<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
		<!--===================================================-->



	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->



	
	<!--JAVASCRIPT--> 
	<!--=================================================-->

	<!--BootstrapJS [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/bootstrap.min.js"></script>


	<!--Fast Click [ OPTIONAL ]-->
	<script src="/template/plugins/fast-click/fastclick.min.js"></script>

	
	<!--Nifty Admin [ RECOMMENDED ]-->
	<script src="/template/bootstrap/js/nifty.min.js"></script>


	<!--Background Image [ DEMONSTRATION ]-->
	<script src="/template/bootstrap/js/demo/0bg-images.js"></script>

	<!--Modals [ SAMPLE ]-->
	<script src="/template/bootstrap/js/demo/ui-modals.js"></script>

</body>
</html>

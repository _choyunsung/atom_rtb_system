		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
							<!--Chat Widget-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
					
								<!--Chat widget header-->
								<div class="panel-heading">
						
								
									<h1 class="page-header text-overflow tit_blit">운영자 정보</h1>
								</div>
					
								<!--Chat widget body-->
								
								<div id="demo-chat-body" class="collapse in">
								
								
									<table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb ">
										<tr>
											<th>ID</th>
											<td colspan="3">karin</td>
										</tr>
																	<tr>
											<th>이름</th>
											<td colspan="3">정주영</td>
										</tr>	
						
										<tr>
											<th>소속/회사명</th>
											<td colspan="3">adop / R&D</td>
										</tr>	
										
										<tr>
											<th>e-mail</th>
											<td>karin@adop.co.kr</td>
											<th class="bd_l">전화번호</th>
											<td>010-6252-2747</td>
										</tr>										
										
										<tr>
											<th>등록일</th>
											<td>2015/03/02</td>
											<th class="bd_l">상태</th>
											<td>정상</td>
										</tr>		
										<tr>
											<th>주소</th>
											<td colspan="3">서울시 강북구 미아동</td>
										</tr>																																						
										</table>
								
								
								<div class="bottom_bt">
									<button id="demo-state-btn" class="btn btn-lg btn-info" data-loading-text="Loading..." type="button">
										수정
										
									</button>
					
								</div>
								
								</div>
							</div>

							<!--Chat Widget-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
					
								<!--Chat widget header-->
								<div class="panel-heading">
							
								<h1 class="page-header text-overflow tit_blit">운영자 리스트
								</h1>
								</div>
					
								<!--Chat widget body-->
								
								<div id="demo-chat-body-2" class="collapse in">

							<table id="demo-dt-basic" class="table table-striped table-bordered bd-t4" cellspacing="0">
								<colgroup>
									<col width="3%">
									<col width="11%">
									<col width="11%">
									<col width="11%">
									<col width="11%">
									<col width="15%">
									<col width="5%">
									<col width="5%">
									<col width="11%">									
									<col width="7%">
								</colgroup>
								<thead>
									<tr>
										<th>No</th>
										<th>국가</th>
										<th class="min-tablet">대행사명</th>
										<th class="min-tablet">담당자</th>
										<th class="min-desktop">전화번호</th>
										<th class="min-desktop">e-mail</th>
										<th class="min-desktop">운영퍼블리셔</th>
										<th class="min-desktop">상태</th>		
								        <th class="min-desktop">등록일/종료일</th>
		                                <th class="min-desktop">보고서보기</th>																														
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>korea</td>									
										<td>대행사adop</td>
										<td>대행사 담당자이름.</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td class="txt_center">몇명?</td>
										<td class="txt_center"> 진행</td>
									    <td>2015 / 03 / 02 ~ 2016/ 03/ 02</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">보기</button></td>
										
									</tr>
									<tr>
										<td>2</td>
										<td>korea</td>									
										<td>대행사adop</td>
										<td>대행사 담당자이름.</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td class="txt_center">몇명?</td>
										<td class="txt_center"> 진행</td>
									    <td>2015 / 03 / 02 ~ 2016/ 03/ 02</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">보기</button></td>
										
									</tr>
									<tr>
										<td>3</td>
										<td>korea</td>									
										<td>대행사adop</td>
										<td>대행사 담당자이름.</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td class="txt_center">몇명?</td>
										<td class="txt_center"> 진행</td>
									    <td>2015 / 03 / 02 ~ 2016/ 03/ 02</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">보기</button></td>
										
									</tr>

										
								</tbody>
							</table>
						</div>
						
					</div>
								
								
				</div>
			</div>


					
		</div>
		
		
	</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


			

		</div>

		


		<!-- SCROLL TOP BUTTON -->
		<!--===================================================-->
		<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
		<!--===================================================-->



	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->




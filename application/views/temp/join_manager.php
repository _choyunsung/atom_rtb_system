		<div class="boxed">

			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
							<!--Chat Widget-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
					
								<!--Chat widget header-->
								<div class="panel-heading">
									<div class="panel-control">
										<div class="btn-group">
											<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body"><i class="fa fa-chevron-down"></i></button>

										</div>
									</div>
									<h1 class="page-header text-overflow tit_blit">운영자 등록 / 수정</h1>
								</div>
					
								<!--Chat widget body-->
								
								<div id="demo-chat-body" class="collapse in">
								
								
									<table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb ">
										<colgroup>
											<col width="10%">
											<col width="40%">
											<col width="10%">
											<col width="40%">
											
										</colgroup>									
										<tr>
											<th class="point_blue">ID</th>
											<td colspan="3"><input type="text"class="wid_400" ></td>
										</tr>
										<tr>
											<th class="point_blue">* 비밀번호</th>
											<td><input type="text"class="wid_400"></td>
											<th class="bd_l point_blue">* 비밀번호확인</th>
											<td><input type="text"class="wid_400"></td>						
										</tr>			
										<tr>
											<th>이름</th>
											<td colspan="3"><input type="text"class="wid_400"></td>
										</tr>	
										<tr>
											<th class="point_blue">* e-mail</th>
											<td><input type="text"class="wid_400"></td>
											<th class="bd_l">전화번호</th>
											<td>				
											
											<div class="btn-group">
													<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
														010          
														<i class="dropdown-caret fa fa-caret-down"></i>
													</button>
													<ul class="dropdown-menu">
														<li><a href="#">011</a>
														</li>
														<li><a href="#">017</a>
														</li>
														<li><a href="#">016</a>
														</li>
										
													</ul>
											</div> - 
											
											<input type="text" class="wid_100"> - <input type="text" class="wid_100"></td>
										</tr>							
										<tr>
											<th>소속/회사명</th>
											<td colspan="3"><input type="text"class="wid_400"></td>
										</tr>	
										<tr>
											<th>등록일</th>
											<td><input type="text"class="wid_400"></td>
											<th class="bd_l">상태</th>
											<td>	
											
											<div class="btn-group">
													<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
														&nbsp;&nbsp; 진행 &nbsp;&nbsp;       
														<i class="dropdown-caret fa fa-caret-down"></i>
													</button>
													<ul class="dropdown-menu">
														<li><a href="#">중지</a>
														</li>
														<li><a href="#">준비</a>
														</li>
																			
													</ul>
											</div> 
											</td>
										</tr>		
										<tr>
											<th>주소 (우편번호)</th>
											<td colspan="3"><input type="text"class="wid_100">
												<button class="btn btn-default btn-icon icon-lg fa ">우편번호찾기</button>
												<input type="text"class="wid_400 ma_l10">
											</td>
										</tr>																																						
										</table>
								
								
								<div class="bottom_bt">
									<button id="demo-state-btn" class="btn btn-lg btn-info" data-loading-text="Loading..." type="button">
										추가
									</button>
					
								</div>
								
								</div>
							</div>

							<!--Chat Widget-->
							<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
							<div class="panel">
					
								<!--Chat widget header-->
								<div class="panel-heading">
									<div class="panel-control">
										<div class="btn-group">
											<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body-2"><i class="fa fa-chevron-down"></i></button>

										</div>
									</div>
								<h1 class="page-header text-overflow tit_blit">운영자 리스트
								</h1>
								</div>
					
								<!--Chat widget body-->
								
								<div id="demo-chat-body-2" class="collapse in">

							<table id="demo-dt-basic" class="table table-striped table-bordered bd-t4" cellspacing="0">
								<colgroup>
									<col width="3%">
									<col width="10%">
									<col width="10%">
									<col width="10%">
									<col width="17%">
									<col width="10%">
									<col width="10%">
									<col width="10%">									
									<col width="10%">
								</colgroup>
								<thead>
									<tr>
										<th>No</th>
										<th>ID</th>
										<th class="min-tablet">소속</th>
										<th class="min-tablet">이름</th>
										<th class="min-desktop">전화번호</th>
										<th class="min-desktop">e-mail</th>
										<th class="min-desktop">관리대행사</th>										
										<th class="min-desktop">상태</th>
										<th class="min-desktop txt_center">등록일</th>		
								        <th class="min-desktop">수정/삭제</th>																													
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td><a href="manager_list_click.php">Karin</a></td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td>대행사갯수</td>										
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">수정</button>
										<button class="btn btn-default btn-icon icon-lg fa ">삭제</button></td>
										
									</tr>
									<tr>
										<td>1</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td>대행사갯수</td>										
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">수정</button>
										<button class="btn btn-default btn-icon icon-lg fa ">삭제</button></td>
										
									</tr>
									<tr>
										<td>1</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td>대행사갯수</td>										
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">수정</button>
										<button class="btn btn-default btn-icon icon-lg fa ">삭제</button></td>
										
									</tr>									
									
										<tr>
										<td>1</td>
										<td>Karin</td>									
										<td>R&D</td>
										<td>정주영</td>
										<td>010-6252-****</td>
										<td>karin@adop.co.kr</td>
										<td>대행사갯수</td>										
										<td>진행 icon</td>
										<td>2015/03/23</td>
										<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">수정</button>
										<button class="btn btn-default btn-icon icon-lg fa ">삭제</button></td>
										
									</tr>								
																
								</tbody>
							</table>
						</div>
						
					</div>
								
								
				</div>
			</div>


					
		</div>
		
		
	</div>
				<!--===================================================-->
				<!--End page content-->


			</div>
			<!--===================================================-->
			<!--END CONTENT CONTAINER-->


			

		</div>

		


		<!-- SCROLL TOP BUTTON -->
		<!--===================================================-->
		<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
		<!--===================================================-->



	</div>
	<!--===================================================-->
	<!-- END OF CONTAINER -->


	

		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
			    <div class="web_fix">
				<!--Chat Widget-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div class="panel">
		 
					<!--Chat widget header-->
					<div class="panel-heading">
						<div class="panel-control">
							<div class="btn-group">
								<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body"><i class="fa fa-chevron-down"></i></button>

							</div>
						</div>
						<h1 class="page-header text-overflow tit_blit">대행사 등록 / 수정</h1>
					</div>
					<!--Chat widget body-->
					<div id="demo-chat-body" class="collapse in">
						<table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb ma_b20">
							<colgroup>
								<col width="10%">
								<col width="40%">
								<col width="10%">
								<col width="40%">
							</colgroup>
							<tr>
								<th class="point_blue">ID</th>
								<td colspan="3"><input type="text" class="wid_400"></td>
							</tr>
							<tr>
								<th class="point_blue">* 비밀번호</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l point_blue">* 비밀번호확인</th>
								<td><input type="text" class="wid_400"></td>						
							</tr>			
							<tr>
								<th>e-mail</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l">전화번호</th>
								<td>				
								    <div class="btn-group">
										<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											010          
											<i class="dropdown-caret fa fa-caret-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">011</a>
											</li>
											<li><a href="#">017</a>
											</li>
											<li><a href="#">016</a>
											</li>
										</ul>
    								</div> - 
    								<input type="text" class="wid_100"> - <input type="text" class="wid_100">
								</td>
							</tr>							
						</table>
						<table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb bd-t2-gray">
						<tr>
							<th class="point_blue">* 대행사명</th>
							<td><input type="text" class="wid_400"></td>
							<th class="bd_l">국가</th>
							<td>				
							     <div class="btn-group">
									<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;국가선택&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           
										<i class="dropdown-caret fa fa-caret-down"></i>
									</button>
									<ul class="dropdown-menu">
										<li><a href="#">Korea</a>
										</li>
										<li><a href="#">Indonesia</a>
										</li>
										<li><a href="#">Singapore</a>
										</li>
										<li><a href="#">Malaysia</a>
										</li>													
										<li><a href="#">Malaysia</a>
										</li>													
										<li><a href="#">Malaysia</a>
										</li>													
										<li><a href="#">Malaysia</a>
										</li>													
										<li><a href="#">Malaysia</a>
										</li>													
										<li><a href="#">Malaysia</a>
										</li>													
									</ul>
							     </div>
							</td>
						</tr>
						<tr>
							<th class="point_blue">* 대행사 사이트 (URL)</th>
							<td colspan="3"><input type="text" class="wid_400"></td>					
						</tr>			
						<tr>
							<th class="point_blue">* 담당자명</th>
							<td><input type="text" class="wid_400"></td>
							<th class="bd_l point_blue">* 담당자 전화번호</th>
							<td><input type="text" class="wid_400"></td>
						</tr>							
						<tr>
							<th>팩스번호</th>
							<td><input type="text" class="wid_400"></td>
						</tr>
						<tr>
							<th>주소</th>
							<td colspan="3"><input type="text"class="wid_100">
								<button class="btn btn-default btn-icon icon-lg fa ">우편번호찾기</button>
								<input type="text"class="wid_400 ma_l10">
							</td>
						</tr>			
						<tr>
							<th><span class="point_blue">* 사업자번호</span>+<br />로고이미지등록</th>
							<td><input type="text" class="wid_400"></td>
							<th class="bd_l point_blue">계좌정보<br />(은행명+계좌번호)</th>
							<td><input type="text" class="wid_400"></td>
						</tr>							
					</table>					
					<div class="bottom_bt">
						<button id="demo-state-btn" class="btn btn-lg btn-info" data-loading-text="Loading..." type="button">
							추가
						</button>
					</div>
				</div>
			</div>
			<!--Chat Widget-->
			<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
			<div class="panel">
				<!--Chat widget header-->
				<div class="panel-heading">
					<div class="panel-control">
						<div class="btn-group">
							<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body-2"><i class="fa fa-chevron-down"></i></button>
						</div>
					</div>
				    <h1 class="page-header text-overflow tit_blit">대행사 리스트</h1>
				</div>
				<!--Chat widget body-->
				<div id="demo-chat-body-2" class="collapse in">
    				<table id="demo-dt-basic" class="table table-striped table-bordered bd-t4" cellspacing="0">
    					<colgroup>
    						<col width="3%">
    						<col width="10%">
    						<col width="10%">
    						<col width="13%">
    						<col width="10%">
    						<col width="10%">
    						<col width="10%">
    						<col width="10%">									
    						<col width="10%">
    					</colgroup>
    					<thead>
    						<tr>
    							<th>소속</th>
    							<th>국가</th>
    							<th class="min-tablet">대행사명</th>
    							<th class="min-tablet">ID</th>
    							<th class="min-desktop">담당자</th>
    							<th class="min-desktop">전화번호</th>
    							<th class="min-desktop">e-mail</th>										
    							<th class="min-desktop">운영 퍼블리셔</th>
    							<th class="min-desktop txt_center">상태</th>		
    					        <th class="min-desktop">등록일/종료일</th>																													
    						</tr>
    					</thead>
    					<tbody>
    						<tr>
    							<td>1</td>
    							<td>Korea</td>									
    							<td><a href="agency_list_click.php">ADOP대행사</a></td>
    							<td>Karin</td>
    							<td>정주영</td>
    							<td>010-6252-2747</td>
    							<td>karin@adop.co.kr</td>										
    							<td>몇</td>
    							<td>진행</td>
    							<td class="txt_center">2015-03-02 ~ 2015-05-01</td>
    						</tr>
    						<tr>
    							<td>1</td>
    							<td><a href="manager_list_click.php">Korea</a></td>									
    							<td>ADOP대행사</td>
    							<td>Karin</td>
    							<td>정주영</td>
    							<td>010-6252-2747</td>
    							<td>karin@adop.co.kr</td>										
    							<td>몇</td>
    							<td>진행</td>
    							<td class="txt_center">2015-03-02 ~ 2015-05-01</td>
    						</tr>	
							<tr>
    							<td>1</td>
    							<td><a href="manager_list_click.php">Korea</a></td>									
    							<td>ADOP대행사</td>
    							<td>Karin</td>
    							<td>정주영</td>
    							<td>010-6252-2747</td>
    							<td>karin@adop.co.kr</td>										
    							<td>몇</td>
    							<td>진행</td>
    							<td class="txt_center">2015-03-02 ~ 2015-05-01</td>
    						</tr>									
    						<tr>
    							<td>1</td>
    							<td><a href="manager_list_click.php">Korea</a></td>									
    							<td>ADOP대행사</td>
    							<td>Karin</td>
    							<td>정주영</td>
    							<td>010-6252-2747</td>
    							<td>karin@adop.co.kr</td>										
    							<td>몇</td>
    							<td>진행</td>
    							<td class="txt_center">2015-03-02 ~ 2015-05-01</td>
    						</tr>												
    					</tbody>
    				</table>
			     </div>
			</div>
		




	<!--NAVBAR-->
		<!--===================================================-->
		<header id="navbar">
			<div id="navbar-container" class="boxed">

				<!--Brand logo & name-->
				<!--================================-->
				<div class="navbar-header">
					<a href="index.html" class="navbar-brand">
						<img src="/img/logo.png" alt="Nifty Logo" class="brand-icon">
						<div class="brand-title">
							<span class="brand-text">Ocean</span>
						</div>
					</a>
				</div>
				<!--================================-->
				<!--End brand logo & name-->


				<!--Navbar Dropdown-->
				<!--================================-->
				<div class="navbar-content clearfix">
					<ul class="nav navbar-top-links pull-left">

						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End Navigation toogle button-->


						<!--Messages Dropdown-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								운영자 관리
							
							</a>

						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End message dropdown-->

						<!--Notification dropdown-->
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								대행사 관리
								
							</a>

	
						</li>
						<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
						<!--End notifications dropdown-->

						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								퍼블리셔 관리
							
								
							</a>

		
						</li>
						
						
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								광고 관리
											
							</a>

							<div class="dropdown-menu dropdown-menu-md with-arrow">
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-1</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
							</div>
						</li>
						
						
						
						
						
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								지면 관리
								
							</a>

							<div class="dropdown-menu dropdown-menu-md with-arrow">
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-1</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
							</div>
						</li>
						
						
						
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								보고서
								
							</a>

							<div class="dropdown-menu dropdown-menu-md with-arrow">
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-1</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
							</div>
						</li>
						
						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								계정관리
								
								
							</a>

							<div class="dropdown-menu dropdown-menu-md with-arrow">
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-1</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">메뉴2-2</p>
								</div>
							</div>
						</li>
						<li>
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								내정보
								
								
							</a>

							<div class="dropdown-menu dropdown-menu-md with-arrow">
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">내정보수정</p>
								</div>
								<div class="pad-all bord-btm">
									<p class="text-lg text-muted text-thin mar-no">logout</p>
								</div>
							</div>
						</li>
						
						
						
					</ul>
	

						

			</div>
		</header>
		<!--===================================================-->
		<!--END NAVBAR-->

<body>
	<div id="container" class="effect mainnav-lg">
		<div class="boxed">
			<!--CONTENT CONTAINER-->
			<!--===================================================-->
			<div id="content-container">
				
				<!--Chat Widget-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div class="panel">
					<!--Chat widget header-->
					<div class="panel-heading">
						<div class="panel-control">
							<div class="btn-group">
								<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body"><i class="fa fa-chevron-down"></i></button>

							</div>
						</div>
						<h1 class="page-header text-overflow tit_blit">퍼블리셔 등록 / 수정</h1>
					</div>
					<!--Chat widget body-->
					<div id="demo-chat-body" class="collapse in">
						<table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb ma_b20">
							<colgroup>
								<col width="10%">
								<col width="40%">
								<col width="10%">
								<col width="40%">
							</colgroup>
							<tr>
								<th class="point_blue">* ID</th>
								<td colspan="3"><input type="text" class="wid_400"></td>
							</tr>
							<tr>
								<th class="point_blue">* 비밀번호</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l point_blue">* 비밀번호확인</th>
								<td><input type="text" class="wid_400"></td>						
							</tr>			
							<tr>
								<th class="point_blue">* e-mail</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l point_blue">* 범주</th>
								<td>								
								    <div class="btn-group">
										<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;범주선택&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           
											<i class="dropdown-caret fa fa-caret-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">뉴스</a></li>
											<li><a href="#">구인구직</a></li>
											<li><a href="#">게임</a></li>
											<li><a href="#">커뮤니티</a></li>
											<li><a href="#">기타</a></li>										
										</ul>
								    </div>
								</td>
							</tr>							
					   </table>
					   <table width="100%" border="0" cellspacign="0" cellpadding="0"class="join_tb bd-t2-gray">
							<colgroup>
								<col width="10%">
								<col width="40%">
								<col width="10%">
								<col width="40%">
							</colgroup>							
							<tr>
								<th class="point_blue">* 퍼블리셔명</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l">국가</th>
								<td>
								    <div class="btn-group">
										<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;국가&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;           
											<i class="dropdown-caret fa fa-caret-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Comunity</a>
											</li>
											<li><a href="#">News</a>
											</li>
											<li><a href="#">Entertaiment</a>
											</li>
							
										</ul>
								    </div>
								</td>
							</tr>
							<tr>
								<th class="point_blue">* 퍼블리 사이트 (URL)</th>
								<td colspan="3"><input type="text" class="wid_400"></td>					
							</tr>			
							<tr>
								<th class="point_blue">* 담당자명</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l point_blue">* 담당자 전화번호</th>
								<td><input type="text" class="wid_400"></td>
							</tr>							
							<tr>
								<th>팩스번호</th>
								<td><input type="text" class="wid_400"></td>
								<th class="bd_l">주소(우편번호)</th>
								<td><input type="text" class="wid_400"></td>
							</tr>				
							
							<tr>
								<th><span>사업자번호</span>+로고이미지등록</th>
								<td><input type="text" class="wid_350"> <button class="btn btn-sm btn-default ma_l10">첨부</button><br></td>
								<th class="bd_l point_blue">* 계좌정보(은행명+계좌번호)</th>
								<td><input type="text" class="wid_400"></td>
							</tr>							
						</table>					
					    <div class="bottom_bt">
    						<button id="demo-state-btn" class="btn btn-lg btn-info" data-loading-text="Loading..." type="button">
    							추가
    						</button>
					    </div>
					</div>
				</div>
				<!--Chat Widget-->
				<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
				<div class="panel">
					<!--Chat widget header-->
					<div class="panel-heading">
						<div class="panel-control">
							<div class="btn-group">
								<button class="btn btn-default" type="button" data-toggle="collapse" data-target="#demo-chat-body-2"><i class="fa fa-chevron-down"></i></button>

							</div>
						</div>
					    <h1 class="page-header text-overflow tit_blit">퍼블리셔 리스트</h1>
					</div>
						<!--Chat widget body-->
					<div id="demo-chat-body-2" class="collapse in">
						<table id="demo-dt-basic" class="table table-striped table-bordered bd-t4" cellspacing="0">
							<colgroup>
								<col width="3%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
								<col width="10%">
								<col width="6%">									
								<col width="12%">
								<col width="10%">
								<col width="10%">									
							</colgroup>
							<thead>
								<tr>
									<th>No</th>
									<th>국가</th>
									<th class="min-tablet">퍼블리셔명</th>
									<th class="min-tablet">ID</th>
									<th class="min-desktop">담당자</th>
									<th class="min-desktop">전화번호</th>
									<th class="min-desktop">e-mail</th>										
									<th class="min-desktop">상태</th>
									<th class="min-desktop txt_center">등록일/종료일</th>		
							        <th class="min-desktop">범주</th>			
							        <th class="min-desktop">수정/삭제</th>									        																										
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Korea</td>									
									<td><a href="/member/agency_list_click.php">ADOP퍼블리셔</a></td>
									<td>Karin</td>
									<td>정주영</td>
									<td>010-6252-2747</td>
									<td>karin@adop.co.kr</td>										
									<td>진행</td>
									<td class="txt_center">2015-03-02 ~ 2015-05-01</td>
									<td>Comunity</td>
									<td class="txt_center"><button class="btn btn-default btn-icon icon-lg fa ">수정</button>&nbsp;<button class="btn btn-default btn-icon icon-lg fa ">삭제</button></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<div>
	<!-- SCROLL TOP BUTTON -->
	<!--===================================================-->
	<button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
	<!--===================================================-->
</div>
<!--===================================================-->
<!-- END OF CONTAINER -->


<?php 
    
?>
<script type="text/javascript">
$(document).ready(function() {
});
	function search(){
		var frm = document.notice_list;
		if (frm.search_type.value == ""){ 
			  frm.search_type.value = "sub";
		}
		frm.action="/board/admin_notice_list";
		frm.submit();
	}
</script>
<div class="panel float_r wid_970 min_height">
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strAdminNotice')?></h3>
    </div>
    <div class="panel-body">
        <form name="notice_list" method="post">
            <div class="float_l wid_100p">
                <div class="float_l pa_t20 ma_b3" id="all_btn_gp">
                    <?php if ($this->session->userdata('admin_fl') == "Y" ){?>
                        <a href="/board/notice_add_form" class="btn btn-primary float_l ma_r5"><?php echo lang('strNewNotice')?></a>
                    <?php }?>
                    <div class="btn-group float_l">
                        <span class="btn btn-primary float_r" onClick ="search();"><?php echo lang('strSearch')?></span>
                        <input type="text" name="search_txt" value="<?php echo $search_txt;?>">
                        <input type="hidden" name="search_type">
                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="sel_search_type">
                            <span id="search_type_view">
                                &nbsp;
                                <?php if ($search_type == "cont"){
                                        echo lang('strContents');
                                    }elseif ($search_type == "all"){
                                        echo lang('strSubject')."+".lang('strContents');
                                    }else{
                                        echo lang('strSubject');
                                    }       
                                ?>
                                &nbsp;
                            </span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box">
                            <li>
                                <a href="#" onclick="sel_search_type('all')">
                                    <?php echo lang('strSubject')."+".lang('strContents');?>
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="sel_search_type('sub')">
                                    <?php echo lang('strSubject');?>
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="sel_search_type('cont')">
                                    <?php echo lang('strContents');?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
               	<span class="btn ma_t20 float_r" onClick =""><?php echo lang('strUnOpenedNotice')?></span>
         	</div>
            <div class="pa_b20">
                <input type="hidden" name="notice_no" >
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
    			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                    <colgroup>
                        <col width="5%">
                        <col width="*">
                        <col width="11%">
                        <col width="10%">
                        <col width="7%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th><?php echo lang('strSubject')?></th>
                            <th><?php echo lang('strRegisteredDate')?></th>
                            <th><?php echo lang('strWriter')?></th>
                            <th><?php echo lang('strReadCount')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($notice_list)){ ?>
                            <?php
                                $no = 1;
                                foreach ($notice_list as $row){
                            ?>
                                <tr>
                                    <td class="txt_center"><?php echo $no;?></td>
                                    <td>
                                        <a href="javascript:notice_detail_view('<?php echo $row['notice_no']?>')"><?php echo $row['notice_nm']?></a>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['date_ymd']?> 
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['writer_nm']?> 
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['read_no']?> 
                                    </td>
                               </tr>
                           <?php $no++;}?> 
                       <?php }?>
                   </tbody>
                </table>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
		</div>
    </div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.notice_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.notice_list;
        frm.action="/board/notice_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    
    function sel_search_type(type){
        if (type == "sub"){
            type_tmp = '<?php echo lang('strSubject');?>';
        }else if (type == "cont"){
        	type_tmp = '<?php echo lang('strContents');?>';
        }else{
        	type_tmp = '<?php echo lang('strSubject')."+".lang('strContents');?>';
        }
        
    	var frm = document.notice_list;
    	frm.search_type.value=type;
    	$("#search_type_view").html(type_tmp);
    }

    function notice_detail_view(notice_no){
    	var frm = document.notice_list;
    	frm.notice_no.value=notice_no;
        frm.action="/board/admin_notice_detail_view/";
        frm.submit();
    }   
</script>
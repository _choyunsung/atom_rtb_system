<?php 
    
?>
<script type="text/javascript">
$(document).ready(function() {
});
	function search(){
		var frm = document.question_list;
		frm.action="/board/question_list";
		frm.submit();
	}
</script>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
    </div>	
    <div class="panel-heading">
        <h3 class="page-header text-overflow tit_blit"><?php echo lang('strQuestionList')?> </h3>
    </div>
    <div class="panel-body">
    	<div class="camp_rept">
    		<div class="camp cursor">
    			<div class="camp_div txt_center" onclick="select_status('all');">
    				<p class="camp_tit"><?php echo lang('strAll')?> <?php echo lang('strQuestion')?></p> 
    				<span class="camp_con"><?php echo $question_summary['all_cnt'];?><?php echo lang('strEa');?></span>
    			</div>
    			<div class="camp_div txt_center" onclick="select_status('today');">
    				<p class="camp_tit"><?php echo lang('strToday')?> <?php echo lang('strRegister')?></p>
    				<span class="camp_con"><?php echo $question_summary['today_cnt'];?><?php echo lang('strEa');?></span>
    			</div>
    			<div class="camp_div txt_center" onclick="select_status('noanswer');">
    				<p class="camp_tit"><?php echo lang('strNoAnswer')?></p>
    				<span class="camp_con"><?php echo $question_summary['noanswer_cnt'];?><?php echo lang('strEa');?></span>
    			</div>		    				
    			<div class="camp_div txt_center" onclick="select_status('answering');">
    				<p class="camp_tit"><?php echo lang('strAnswering')?></p>
    				<span class="camp_con"><?php echo $question_summary['answering_cnt'];?><?php echo lang('strEa');?></span>
    			</div>		    				
    			<div class="camp_div txt_center" style="border-right:0px" onclick="select_status('done');">
    				<p class="camp_tit"><?php echo lang('strAnswerDone')?></p>
    				<span class="camp_con"><?php echo $question_summary['done_cnt'];?><?php echo lang('strEa');?></span>
    			</div>		    					
    		</div>	
    	</div>
	</div>
    <div class="panel-body">
        <form name="question_list" id="question_list" method="post">
            <input type="hidden" name="question_no">
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="15%">
                        <col width="35%">
                        <col width="15%">
                        <col width="35%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strID')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_id" value="<?php echo $cond['mem_id'];?>">
                        </td>
                        <th><?php echo lang('strName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_nm" value="<?php echo $cond['mem_nm'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strQuestionType')?></th>
                        <td>
                            <div class="btn-group float_l">
                                <input type="hidden" name="question_fl" id="question_fl">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    <span id="question_fl_view">
                                        &nbsp;
                                        <?php 
                                            if ($cond['question_fl'] == "1"){
                                                echo lang('strPay');
                                            }elseif ($cond['question_fl'] == "2"){
                                                echo lang('strError');
                                            }elseif ($cond['question_fl'] == "3"){
                                                echo lang('strReport');
                                            }elseif ($cond['question_fl'] == "4"){
                                                echo lang('strAdMgr');
                                            }elseif ($cond['question_fl'] == "5"){
                                                echo lang('strEtc');
                                            }elseif ($cond['question_fl'] == "6"){
                                                echo lang('strRefund');
                                            }elseif ($cond['question_fl'] == "7"){
                                                echo lang('strSecession');
                                            }else{
                                                echo lang('strSelect');
                                            }
                                        ?>
                                        &nbsp;
                                    </span>
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php foreach ($sel_question_fl as $row){?>
                                        <li>
                                            <a href="#" onclick="sel_question_type('<?php echo $row['code_key']?>', '<?php echo lang($row['code_desc'])?>')">
                                                <?php echo lang($row['code_desc'])?>
                                            </a>
                                    </li>
                                    <?php }?>
                                </ul>
                            </div>
                        </td>
                        <th><?php echo lang('strManagerName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="manager_nm" value="<?php echo $cond['manager_nm'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strAnswerStatus')?></th>
                        <td>
                            <input type="checkbox" name="no_answer" id="no_answer" <?php if ($cond['no_answer'] == "on"){echo "checked";}?>> <?php echo lang('strNoAnswer')?>&nbsp;&nbsp;
                            <input type="checkbox" name="tmp_save" id="tmp_save" <?php if ($cond['tmp_save'] == "on"){echo "checked";}?>> <?php echo lang('strTmpSave')?>&nbsp;&nbsp;
                            <input type="checkbox" name="answer_done" id="answer_done" <?php if ($cond['answer_done'] == "on"){echo "checked";}?>> <?php echo lang('strAnswerDone')?>&nbsp;&nbsp;
                        </td>
                        <th><?php echo lang('strRegisteredDate')?></th>
                        <td>
            				<input type="text" class="wid_65p" name="fromto_date" id="daterange" value="<?php echo $cond['fromto_date']?>" />
            				<span class="iput-group-addon">
            					<i class="fa fa-calendar fa-lg range" onclick="$('#daterange').focus();"></i>
            				</span>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strSubject')?></th>
                        <td colspan="3">
                            <input type="text" class="wid_26p" name="question_nm" value="<?php echo $cond['question_nm'];?>">
                        </td>
                    </tr>
                </table>
                <div class="float_r ma_b3" id="all_btn_gp">
                    <span class="btn btn-primary float_r" onClick ="search();"><?php echo lang('strSearch')?></span>
                </div>
         	</div>
            <div class="pa_b20">
                <input type="hidden" name="notice_no" >
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
    			data-horizontal-width="100%" data-display-length="100" cellpadding="0" cellspacing="0" border="0">
                    <colgroup>
                        <col width="5%">
                        <col width="*">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th><?php echo lang('strSubject')?></th>
                            <th><?php echo lang('strQuestionType')?></th>
                            <th><?php echo lang('strName')?></th>
                            <th><?php echo lang('strID')?></th>
                            <th><?php echo lang('strRegisteredDate')?></th>
                            <th><?php echo lang('strAnswerStatus')?></th>
                            <th><?php echo lang('strAnswerName')?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($question_list)){ ?>
                            <?php
                                $no = 1;
                                foreach ($question_list as $row){
                            ?>
                                <tr>
                                    <td class="txt_center"><?php echo $no;?></td>
                                    <td>
                                        <a href="javascript:question_detail_view('<?php echo $row['question_no']?>')"><?php echo $row['question_nm']?></a>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo lang($row['question_fl_desc'])?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['mem_nm']?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['mem_id']?>(<?php echo $row['mem_com_nm']?>)
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['date_ymd']?> 
                                    </td>
                                    <td class="txt_center">
                                        <?php echo lang($row['question_st_desc'])?>
                                    </td>
                                    <td class="txt_center">
                                        <?php echo $row['contact_nm']?>
                                    </td>
                               </tr>
                           <?php $no++;}?> 
                       <?php }?>
                   </tbody>
                </table>
                <div class="center hei_35">
                    <div class="btn-group float_r">
                        <input type="hidden" name="per_page" id="per_page" value="<?php echo $per_page?>">
                        <button class="btn btn-default ma_l5 dropdown-toggle" data-toggle="dropdown" id="per_page_sel" >
                            <span> &nbsp;  <?php echo $per_page?>  &nbsp;</span>
                            <i class="dropdown-caret fa fa-caret-down"></i>
                        </button>
                        <ul class="dropdown-menu ul_sel_box_pa" >
                            <li><a href="javascript:page_change(10)">10</a></li>
                            <li><a href="javascript:page_change(25)">25</a></li>
                            <li><a href="javascript:page_change(50)">50</a></li>
                            <li><a href="javascript:page_change(100)">100</a></li>
                        </ul>
                    </div>
                    <span class="float_r ma_t7"><?php echo lang('strShowRows')?></span>
                    <?php
                    /*페이징처리*/
                        echo $page_links;
                    /*페이징처리*/
                    ?>
                </div>
            </form>
		</div>
    </div>
</div>
        
<script type="text/javascript">
    //페이징 스크립트 시작
    function page_change(row){
        frm=document.notice_list;
        frm.per_page.value=row;
        frm.submit();
    }

    function paging(number){
        var frm = document.notice_list;
        frm.action="/board/notice_list/" + number;
        frm.submit();
    }
    //페이징 스크립트 끝
    
    function sel_question_type(no,type){
        $("#question_fl").val(no);
        $("#question_fl_view").html(type);
    }

    function question_detail_view(question_no){
    	var frm = document.question_list;
    	frm.question_no.value=question_no;
        frm.action="/board/question_detail_view/";
        frm.submit();
    }   

    function select_status(type){
        var frm = document.question_list;
        frm.mem_id.value = "";
        frm.mem_nm.value = "";
        frm.question_fl.value = "";
        frm.manager_nm.value = "";
        frm.fromto_date.value = "";
        frm.question_nm.value = "";
        $("#no_answer").prop("checked", false);
        $("#tmp_save").prop("checked", false);
        $("#answer_done").prop("checked", false);
        
        if (type == "all"){
            location.replace('/board/question_list');
        }else if (type == "today"){
            $("#daterange").val('<?php echo date('Y-m-d') ?>'+ ' ~ ' + '<?php echo date('Y-m-d')?>');
        }else if (type == "noanswer"){
            $("#no_answer").prop("checked",true);
        }else if (type == "answering"){
        	$("#tmp_save").prop("checked",true);
        }else if (type == "done"){
        	$("#answer_done").prop("checked",true);
        }
        search();
    }
            
</script>
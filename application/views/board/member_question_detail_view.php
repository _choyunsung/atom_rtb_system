<?php 
    $mem_no = $this->session->userdata('mem_no');
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strQuestionView')?></h3>
	</div>
	<div class="panel-body">
        <div class="clear">
            <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="10%">
                	<col width="*">
                	<col width="7%">
                	<col width="9%">
                	<col width="7%">
                	<col width=10%">
                </colgroup>
                <tr>
                    <th><?php echo lang('strSubject')?></th>
                    <td>
                        <?php echo $question_detail['question_nm']?>
                    </td>
                    <th><?php echo lang('strQuestionType')?></th>
                    <td>
                        <?php echo lang($question_detail['question_fl_desc'])?>
                    </td>
                    <th><?php echo lang('strRegisteredDate')?></th>
                    <td>
                        <?php echo $question_detail['question_ymd']?>
                    </td>
                </tr>
            </table>
            <table class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="10%">
                	<col width="*">
                </colgroup>
                <tr class="hei_300 ver_top">
                    <th><?php echo lang('strContents')?></th>
                    <td colspan="2">
                        <?php echo $question_detail['question_cont']?>
                    </td>
                </tr>
                <?php foreach ($question_attach as $row){?>
                    <tr>
					    <th><?php echo lang('strAttachFile')?></th>
					    <td>
					       <a href="<?php echo BOARD_IMAGE_BASE_URL.$row['attach_file_nm']?>" target="new"><?php echo $row['attach_file_nm']?></a>
						</td>
						<td>
						</td>
					</tr>
				<?php }?>
            </table>
            <?php if ($question_detail['answer_cond'] != ""){?>
                <table class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                    <colgroup>
                    	<col width="10%">
                    	<col width="*">
                    </colgroup>
                    <tr class="hei_300 ver_top">
                        <th><?php echo lang('strAnswer')?></th>
                        <td colspan="2">
                            <?php echo $question_detail['answer_cont']?>
                        </td>
                    </tr>
                </table>
            <?php }?>
            <div class=" ma_b3" id="all_btn_gp">
                <a href="/board/question_list"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strClose')?></span></a>
                <?php if ($question_detail['writer_no'] == $mem_no){?>
                    <a href="javascript:question_modify();" ><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strModify')?></span></a>
                    <a href="javascript:question_delete();" ><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strDelete')?></span></a>
                <?php }?>
            </div>
            <form name="question_modify" method="post" >
                <input type="hidden" name="question_no" value="<?php echo $question_detail['question_no']?>">
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

function notice_move(no){
	var frm = document.notice_move;
	frm.action = "/board/notice_detail_view"
	frm.notice_no.value = no;
	frm.submit();
}

function answer_save(st){
    frm = document.answer_save;
    $("#question_st").val(st);
    frm.action = "/board/answer_save";
    frm.submit();
}

function question_modify(){
	var question_st = '<?php echo $question_detail['question_st']?>';

	if (question_st != "1"){
		alert("답변이 완료된 문의는 수정할 수 없습니다.");
	}else{
		document.question_modify.action="/board/question_modify_form";
		document.question_modify.submit();  
	}
}

function question_delete(){
	var question_st = '<?php echo $question_detail['question_st']?>';

	if (question_st != "1"){
		alert("답변이 완료된 문의는 삭제할 수 없습니다.");
	}else{
		if(confirm("해당 문의를 삭제하겠습니까?")){
    		document.question_modify.action="/board/question_delete";
    		document.question_modify.submit();
		}  
	}
}
</script>
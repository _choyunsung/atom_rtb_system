<?php 
    $mem_no = $this->session->userdata('mem_no');
?>
<div class="panel float_r wid_970 min_height">
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strNotice')?></h3>
	</div>
	<div class="panel-body">
        <div class="clear">
            <form name="notice_modify_save" method="post" enctype="multipart/form-data">
                <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
				<input type="hidden" name="notice_no" id="notice_no" value="<?php echo $notice_detail['notice_no'];?>">
                <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                    <colgroup>
                    	<col width="10%">
                    	<col width="*">
                    	<col width="10%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strUserType')?></th>
                        <td colspan="2">
                            <input type="radio" name="notice_fl" value="A" <?php if ($notice_detail['notice_fl'] == "A"){echo "checked";}?>> 운영자 공지&nbsp;&nbsp;
                            <input type="radio" name="notice_fl" value="M" <?php if ($notice_detail['notice_fl'] == "M"){echo "checked";}?>> 회원 공지
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strSubject')?></th>
                        <td colspan="2">
                            <input type="text" name="notice_nm" id="notice_nm" class="wid_100p" value="<?php echo $notice_detail['notice_nm'];?>"/>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strContents')?></th>
                        <td colspan="2">
                            <textarea rows="15" cols="120" name="notice_cont" id="editor"><?php echo $notice_detail['notice_cont'];?></textarea>
                        </td>
                    </tr>
					<?php if (isset ($notice_attach)){?>
						<?php $no = 1;?>
						<?php foreach ($notice_attach as $row) {?>
							<tr>
								<th><?php echo lang('strAttachFile').$no?></th>
								<td>
									<input type="file" name="attach_file_nm<?php echo $no;?> " size="40" onchange="$('#upload1_yn').val('Y');"/>
									<input type="hidden" name="upload<?php echo $no;?>_yn"  id="upload<?php echo $no;?>_yn" value="N"  onchange="$('#upload1_yn').val('Y');">
									<a href="<?php echo BOARD_IMAGE_BASE_URL.$row['attach_file_nm']?>" target="new">
										<?php echo $row['attach_file_nm'];?>
									</a>
								</td>
								<td>
								   <input type='button' id="btn_add_upload<?php echo $no;?>" class='btn btn-sm btn-primary' value="<?php echo lang('strAddAttach')?>" onclick="add_upload_form('<?php echo $no?>');" />
								</td>
							</tr>
							<?php $no++;?>
						<?php }?>
					<?php }else{?>
						<tr>
							<th><?php echo lang('strAttachFile')?>1</th>
							<td>
								<input type="file" name="attach_file_nm1" size="40" onchange="$('#upload1_yn').val('Y');"/>
								<input type="hidden" name="upload1_yn" id="upload1_yn" value="N">
							</td>
							<td>
								<input type='button' id="btn_add_upload1" class='btn btn-sm btn-primary' value="<?php echo lang('strAddAttach')?>" onclick="add_upload_form('1');" />
							</td>
						</tr>
						<?php $no = 2;?>
					<?php }?>
					<?php for($i = $no; $i <= 10; $i++){?>
						<tr id="upload_form<?php echo $i;?>" style="display: none;">
						    <th><?php echo lang('strAttachFile').$i?></th>
						    <td>
						        <input type="file" name="attach_file_nm<?php echo $i;?>" size="40"/>
						        <input type="hidden" id="upload<?php echo $i;?>_yn" name="upload<?php echo $i;?>_yn" value="">
							</td>
							<td style="width:90px; text-align:center; vertical-align: middle;">
							   <?php if ($i!=10){?>
							       <input type='button' id="btn_add_upload<?php echo $i;?>" class='btn btn-sm btn-primary' value="<?php echo lang('strAddAttach')?>" onclick="add_upload_form('<?php echo $i;?>');" />
						        <?php }?>
						        <input type='button' id="btn_add_upload<?php echo $i;?>" class='btn btn-sm btn-danger' value="<?php echo lang('strCancel')?>" onclick="del_upload_form('<?php echo $i;?>');" />
							</td>
							
						</tr>
					<?php }?>
                </table>
            </form>
        </div>
        <p class="txt-right">
            <a href="#" onclick="history.back();"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strCancel')?></span></a>
            
            <span onclick="save();" class="btn btn-primary float_r"><?php echo lang('strSave');?></span>
        </p>
    </div>
</div>
<script type="text/javascript">
CKEDITOR.replace('editor',{
	filebrowserImageUploadUrl:"/template/plugins/ckeditor/upload.php?type=Images"
});

function add_upload_form(idx){
	btn=document.getElementById('btn_add_upload'+idx);
	tr=document.getElementById("upload_form"+(Number(idx)+1));
	yn=document.getElementById("upload"+(Number(idx)+1)+"_yn");
    tr.style.display = "";
    btn.style.display = "none";
    yn.value = "Y";
}

function del_upload_form(idx){
	btn=document.getElementById('btn_add_upload'+(Number(idx)-1));
	tr=document.getElementById("upload_form"+idx);
	yn=document.getElementById("upload"+idx+"_yn");
	btn.style.display = "";
	tr.style.display = "none";
    yn.value = "N";
}

function save(){
    frm = document.notice_modify_save;
    var title = frm.notice_nm;
    var content = frm.notice_cont;

	    if(title.value.trim()==""){
	        alert("title Needed!");
	        title.focus();
	        return false;
	    }
	    if(CKEDITOR.instances.editor.getData().trim()==""){
	        alert("content Needed!");
	        content.focus();
	        return false;
	    }   

	    var url = '/board/notice_modify_save/';
        frm.action = url;
	    frm.submit();
}
</script>
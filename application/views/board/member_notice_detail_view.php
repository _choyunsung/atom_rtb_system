<?php 
    $mem_no = $this->session->userdata('mem_no');
?>
<div class="panel float_r wid_970 min_height">
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strNotice')?></h3>
	</div>
	<div class="panel-body">
        <div class="clear">
            <div class="float_r pa_t10 ma_b3" id="all_btn_gp">
                <a href="/board/member_notice_list"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strList')?></span></a>
                <?php if ($mem_no == $notice_detail['mem_no']){ ?>
                    <a href="javascript:notice_modify('<?php echo $notice_detail['notice_no']?>');"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strModify')?></span></a>
                <?php }?>
            </div>
            <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
            <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="10%">
                	<col width="*">
                	<col width="15%">
                	<col width="20%">
                </colgroup>
                <tr>
                    <th><?php echo lang('strSubject')?></th>
                    <td>
                        <?php echo $notice_detail['notice_nm']?>
                        <?php echo $notice_detail['mem_no']?>
                    </td>
                    <th><?php echo lang('strRegisteredDate')?></th>
                    <td>
                        <?php echo $notice_detail['date_ymd']?>
                    </td>
                </tr>
            </table>
            <table class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="10%">
                	<col width="*">
                </colgroup>
                <tr class="hei_300 ver_top">
                    <th><?php echo lang('strContents')?></th>
                    <td colspan="3">
                        <?php echo $notice_detail['notice_cont']?>
                    </td>
                </tr>
                <?php foreach ($notice_attach as $row){?>
                    <tr>
					    <th><?php echo lang('strAttachFile')?></th>
					    <td colspan="2">
					       <a href="<?php echo BOARD_IMAGE_BASE_URL.$row['attach_file_nm']?>" target="new"><?php echo $row['attach_file_nm']?></a>
						</td>
					</tr>
				<?php }?>
            </table>
            <div class="center hei_35">
                <a href="javascript:notice_move('next', '<?php echo $next_notice_no['notice_no']?>')"><span class="btn btn-dark float_r"><?php echo lang('strNextNotice');?> ▶</span></a>
                <a href="javascript:notice_move('pre', '<?php echo $pre_notice_no['notice_no']?>')"><span class="btn btn-dark float_l">◀ <?php echo lang('strPreNotice');?></span></a>
           </div>
        </div>
        
        <form name="notice_move" method="post">
            <input type="hidden" name="notice_no">
            <input type="hidden" name="mem_no" value="<?php echo $mem_no?>">
        </form>
    </div>
</div>
<script type="text/javascript">
    function img_download(file_nm){
            url = '/board/img_download';
            $.post(url,
                {
                file_name : file_nm,
                },
                function(data){
                    if(data.trim()=="ok"){
                    }
                }
            );
    }

    function notice_move(type, no){
        if (no == ""){
            if (type == "pre"){
                alert("이전글이 없습니다.");
            }else{
                alert("다음글이 없습니다.");
            }
        }else{
            var frm = document.notice_move;
            frm.action = "/board/notice_detail_view"
            frm.notice_no.value = no;
            frm.submit();
        }
    }
    function notice_modify(no){
        var frm = document.notice_move;
        frm.notice_no.value = no;
        frm.action = "/board/notice_modify_form";
        frm.submit();
    }
</script>
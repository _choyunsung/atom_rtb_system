<div class="panel float_r wid_970 min_height">
    <div class="history_box">
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strAddQuestion')?></h3>
	</div>
	<div class="panel-body">
        <form name="question_add" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
            <div class="float_l wid_100p">
                <table class="aut_tb">
                    <colgroup>
                        <col width="10%">
                    	<col width="40%">
                    	<col width="10%">
                    	<col width="40%">
                    </colgroup>
                    <tr>
                        <th><?php echo lang('strName')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_nm" value="<?php echo $mem_info['mem_nm'];?>">
                        </td>
                        <th><?php echo lang('strContactNo')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_tel" value="<?php echo $mem_info['mem_tel'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strQuestionType')?></th>
                        <td>
                            <div class="btn-group float_l">
                                <input type="hidden" name="question_fl" id="question_fl">
                                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
                                    <span id="question_fl_view">
                                        &nbsp;<?php echo lang('strSelect')?>&nbsp;
                                    </span>
                                    <i class="dropdown-caret fa fa-caret-down"></i>
                                </button>
                                <ul class="dropdown-menu ul_sel_box">
                                    <?php 
                                    foreach ($sel_question_fl as $row){
                                    
                                        if($mem_pay_later == "Y" && $row['code_key'] == '6'){
                                            
                                        }else{
                                    ?>
                                    <li>
                                        <a href="javascript:void(0);" onclick="sel_question_type('<?php echo $row['code_key']?>', '<?php echo lang($row['code_desc'])?>')">
                                            <?php echo lang($row['code_desc'])?>
                                        </a>
                                    </li>
                                    <?php 
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </td>
                        <th><?php echo lang('strEmail')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_email" value="<?php echo $mem_info['mem_email'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strAdvertiser')?> <?php echo lang('strID')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="adver_id" value="<?php echo $mem_info['mem_id']?>">
                        </td>
                        <th><?php echo lang('strHomepage')?></th>
                        <td>
                            <input type="text" class="wid_65p" name="mem_com_url" value="<?php echo $mem_info['mem_com_url'];?>">
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strSubject')?></th>
                        <td colspan="3">
                            <input type="text" name="question_nm" id="question_nm" class="wid_100p" />
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo lang('strContents')?></th>
                        <td colspan="3">
                            <textarea rows="15" cols="120" name="question_cont" id="editor"></textarea>
                        </td>
                    </tr>
                    <tr>
        			    <th><?php echo lang('strAttachFile')?>1</th>
        			    <td colspan="3">
        			        <input type="file" class="float_l" name="attach_file_nm1"/>
        			        <input type="hidden" name="upload1_yn" value="Y">
        				    <a id="btn_add_upload1" class='btn btn-sm btn-primary float_r' onclick="add_upload_form('1');" ><?php echo lang('strAddAttach')?></a>
        				</td>
        			</tr>
        			<?php for($i=2;$i<=10;$i++){?>
        				<tr id="upload_form<?php echo $i;?>" style="display: none;">
        				    <th><?php echo lang('strAttachFile').$i?></th>
        				    <td colspan="3">
        				        <input type="file" name="attach_file_nm<?php echo $i;?>" class="float_l"/>
        				        <input type="hidden" id="upload<?php echo $i;?>_yn" name="upload<?php echo $i;?>_yn" value="">
        				        <a id="btn_add_upload<?php echo $i;?>" class='btn btn-sm btn-danger float_r' onclick="del_upload_form('<?php echo $i;?>');" >
        				            <?php echo lang('strCancel')?>
    				            </a>
        				        <?php if ($i!=10){?>
        					       <a id="btn_add_upload<?php echo $i;?>" class='btn btn-sm btn-primary float_r' onclick="add_upload_form('<?php echo $i;?>');" >
        					           <?php echo lang('strAddAttach')?>
    					           </a>
        				        <?php }?>
        					</td>
        				</tr>
        			<?php }?>
                </table>
            </form>
        </div>
        <p class="txt-right">
            <a href="/board/question_list"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strCancel')?></span></a>
            <span onclick="save();" class="btn btn-primary float_r"><?php echo lang('strSave')?></span>
        </p>
    </div>
<script type="text/javascript">
CKEDITOR.replace('editor',{
	filebrowserImageUploadUrl:"/template/plugins/ckeditor/upload.php?type=Images"
});

function add_upload_form(idx){
	btn=document.getElementById('btn_add_upload'+idx);
	tr=document.getElementById("upload_form"+(Number(idx)+1));
	yn=document.getElementById("upload"+(Number(idx)+1)+"_yn");
    tr.style.display = "";
    btn.style.display = "none";
    yn.value = "Y";
}

function del_upload_form(idx){
	btn=document.getElementById('btn_add_upload'+(Number(idx)-1));
	tr=document.getElementById("upload_form"+idx);
	yn=document.getElementById("upload"+idx+"_yn");
	btn.style.display = "";
	tr.style.display = "none";
    yn.value = "N";
}

function sel_question_type(no,type){
    $("#question_fl").val(no);
    $("#question_fl_view").html(type);
}

function save(){
    frm = document.question_add;

	    if(frm.question_nm.value.trim()==""){
	        alert("title Needed!");
	        frm.title.focus();
	        return false;
	    }
	    if(CKEDITOR.instances.editor.getData().trim()==""){
	        alert("content Needed!");
	        frm.question_cont.focus();
	        return false;
	    }   

	    var url = '/board/question_add/';
        frm.action = url;
	    frm.submit();
}
</script>
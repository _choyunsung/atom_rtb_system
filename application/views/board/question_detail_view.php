<?php 
    $mem_no = $this->session->userdata('mem_no');
?>
<div class="panel float_r wid_970 min_height">
    <div class="history_box">
	</div>	
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strQuestionView')?></h3>
	</div>
	<div class="panel-body">
        <div class="clear">
            <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
            <table  class="aut_tb" width="100%" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="7%">
                	<col width="*">
                	<col width="7%">
                	<col width="9%">
                	<col width="7%">
                	<col width=10%">
                	<col width="7%">
                	<col width="10%">
                	<col width="7%">
                	<col width="9%">
                </colgroup>
                <tr>
                    <th><?php echo lang('strSubject')?></th>
                    <td>
                        <?php echo $question_detail['question_nm']?>
                    </td>
                    <th><?php echo lang('strQuestionType')?></th>
                    <td>
                        <?php echo lang($question_detail['question_fl_desc'])?>
                    </td>
                    <th><?php echo lang('strRegisteredDate')?></th>
                    <td>
                        <?php echo $question_detail['question_ymd']?>
                    </td>
                    <th><?php echo lang('strAnswerDate')?></th>
                    <td>
                        <?php echo $question_detail['answer_ymd']?>
                    </td>
                    <th><?php echo lang('strAnswerName')?></th>
                    <td>
                        <?php echo $question_detail['contact_nm']?>
                    </td>
                </tr>
            </table>
            <table  class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="20%">
                	<col width="30%">
                	<col width="20%">
                	<col width="30%">
                </colgroup>
                <tr>
                    <th colspan="4"><?php echo lang('strWriterInfo')?></th>
                </tr>
                <tr>
                    <th><?php echo lang('strName')?></th>
                    <td>
                        <?php echo $question_detail['mem_nm']?>
                    </td>
                    <th><?php echo lang('strID')?></th>
                    <td>
                        <?php echo $question_detail['mem_id']?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo lang('strRegisteredDate')?></th>
                    <td>
                        <?php echo $question_detail['mem_cell']?>
                    </td>
                    <th><?php echo lang('strAnswerDate')?></th>
                    <td>
                        <?php echo $question_detail['date_ymd']?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo lang('strEmail')?></th>
                    <td colspan="3">
                        <?php echo $question_detail['mem_email']?>
                    </td>
                </tr>
            </table>
            <table class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="10%">
                	<col width="*">
                </colgroup>
                <tr class="hei_300 ver_top">
                    <th><?php echo lang('strContents')?></th>
                    <td colspan="2">
                        <?php echo $question_detail['question_cont']?>
                    </td>
                </tr>
                <?php foreach ($question_attach as $row){?>
                    <tr>
					    <th><?php echo lang('strAttachFile')?></th>
					    <td>
					       <a href="<?php echo BOARD_IMAGE_BASE_URL.$row['attach_file_nm']?>" target="new"><?php echo $row['attach_file_nm']?></a>
						</td>
						<td>
						</td>
					</tr>
				<?php }?>
            </table>
            <form name="answer_save" method="post">
                <input type="hidden" name="question_no" value="<?php echo $question_detail['origin_qeustion_no']?>">
                <input type="hidden" name="answer_no" value="<?php echo $question_detail['answer_no']?>">
                <input type="hidden" name="question_st" id="question_st">
                <input type="hidden" name="mem_no" value="<?php echo $this->session->userdata('mem_no')?>">
                <table class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                    <colgroup>
                    	<col width="10%">
                    	<col width="*">
                    </colgroup>
                    <tr class="hei_300 ver_top">
                        <th><?php echo lang('strAnswer')?></th>
                        <td colspan="2">
                            <?php if ($question_detail['question_st'] == "3"){?>
                                <?php echo $question_detail['answer_cont']?>
                            <?php }else{?>
                                <textarea rows="15" cols="120" name="answer_cont" id="editor">
                                    <?php echo $question_detail['answer_cont']?>
                                </textarea>
                            <?php }?>
                        </td>
                    </tr>
                </table>
            </form>
            <div class=" ma_b3" id="all_btn_gp">
                <span class = "floar_l"><?php echo lang('strAnswerName')?>&nbsp;&nbsp;<input class="wid_10p" type="text" value="<?php echo $this->session->userdata('mem_nm')?>" readonly></span>
                <a href="/board/question_list"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strClose')?></span></a>
                <?php if ($question_detail['question_st'] != "3"){?>
                    <a href="javascript:answer_save('3')"><span class="btn btn-primary ma_l5 float_r"><?php echo lang('strAnswerDone')?></span></a>
                    <a href="javascript:answer_save('2')"><span class="btn btn-warning ma_l5 float_r"><?php echo lang('strTmpSave')?></span></a>
                <?php }?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

function notice_move(no){
	var frm = document.notice_move;
	frm.action = "/board/notice_detail_view"
	frm.notice_no.value = no;
	frm.submit();
}

function answer_save(st){
    frm = document.answer_save;
    $("#question_st").val(st);
    frm.action = "/board/answer_save";
    frm.submit();
}

CKEDITOR.replace('editor',{
	filebrowserImageUploadUrl:"/template/plugins/ckeditor/upload.php?type=Images"
});

</script>
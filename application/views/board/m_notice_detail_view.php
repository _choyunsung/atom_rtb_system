<?php 
    $mem_no = $this->session->userdata('mem_no');
?>
<div class="m_wrap panel float_r min_height">
	<div class="panel-heading">
		<h3 class="page-header text-overflow tit_blit"><?php echo lang('strNotice')?></h3>
	</div>
	<div class="panel-body">
        <div class="clear">
            <div class="float_r ma_b3" id="all_btn_gp">
                <a href="/"><span class="btn btn-dark ma_l5 float_r"><?php echo lang('strBack')?></span></a>
            </div>
            <input type="hidden" name="mem_no" id="mem_no" value="<?php echo $mem_no;?>">
            <table  class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="20%">
                	<col width="*">
                	<col width="15%">
                	<col width="*">
                </colgroup>
                <tr>
                    <th><?php echo lang('strSubject')?></th>
                    <td colspan="3">
                        <?php echo $notice_detail['notice_nm']?>
                    </td>
                </tr>
                <tr>
                    <th><?php echo lang('strRegisteredDate')?></th>
                    <td class="txt_center">
                        <?php echo $notice_detail['date_ymd']?>
                    </td>
                    <th><?php echo lang('strWriter')?></th>
                    <td class="txt_center">
                        <?php echo $notice_detail['writer_nm']?>
                    </td>
                </tr>
            </table>
            <table class="aut_tb" cellpadding="0" cellspacing="0" border="0" >
                <colgroup>
                	<col width="20%">
                	<col width="*">
                	<col width="10%">
                </colgroup>
                <tbody>
                    <tr class="hei_300 ver_top">
                        <th><?php echo lang('strContents')?></th>
                        <td colspan="2">
                            <?php echo $notice_detail['notice_cont']?>
                        </td>
                    </tr>
                    <?php foreach ($notice_attach as $row){?>
                        <tr>
                            <th><?php echo lang('strAttachFile')?></th>
                            <td colspan="2">
                               <a href="<?php echo BOARD_IMAGE_BASE_URL.$row['attach_file_nm']?>" target="new"><?php echo substr($row['attach_file_nm'], 0, 10)?>....</a>
                            </td>
                        </tr>
                    <?php }?>
                    <?php if ($notice_fl == "A"){?>
                        <form name="reply1_form" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="notice_no" value="<?php echo $notice_detail['notice_no'];?>">
                            <input type="hidden" name="reply_depth" value="1">
                            <input type="hidden" name="mem_no" value="<?php echo $mem_no; ?>">
                            <tr>
                                <th><?php echo $this->session->userdata('mem_nm');?></th>
                                <td>
                                    <textarea name="reply_cont" class="form-control" rows="3" ></textarea>
                                 </td>
                                 <td class="txt_center">
                                     <input type='button' class='btn btn-sm btn-primary' value="<?php echo lang('strSave')?>" onclick='reply1_save();' />&nbsp;
                                </td>
                            </tr>
                        </form>
                    <?php }?>
                </tbody>
            </table>
            <?php if ($notice_fl == "A"){?>
                <table id="list" name="list" class="table table-striped table-bordered table-hover datatable table-tabletools scroll aut_tb"
                    data-horizontal-width="100%" cellpadding="0" cellspacing="0" border="0">
                        <colgroup>
                            <col width="10%">
                            <col width="*">
                            <col width="15%">
                        </colgroup>
                        <tbody>
                            <?php if (isset($reply_list)){ ?>
                                <?php $reply_idx=0;?>
                                <?php foreach ($reply_list as $row){ ?>
                                    <?if ($row['reply_depth'] == '1'){?>
                                        <tr id="original_reply_<?php echo $reply_idx;?>">
                                            <td class="txt_center"><?php echo $row['mem_nm'];?></td>
                                            <td>
                                                <?php echo $row['reply_cont']?>
                                                <a id="reply_btn_<?php echo $reply_idx;?>" class="cursor" style="display: ;" onclick="reply_view('<?php echo $reply_idx;?>');">댓글</a>
                                                <a id="cancel_btn_<?php echo $reply_idx;?>" style="display: none;" onclick="reply_view_close('<?php echo $reply_idx;?>');">취소</a>
                                                <?php if($emp_idx==$rep['write_idx']){?>
                                                    <a style="cursor:pointer;" onclick="reply_modify('<?php echo $reply_idx;?>');">수정</a>
                                                    <a style="cursor:pointer;" onclick="reply_del('original_modify_reply_form<?php echo $reply_idx;?>');">삭제</a>
                                                <?php }?>
                                            </td>
                                            <td class="txt_center">
                                                <?php echo $row['date_ymd']?>
                                            </td>
                                                 </tr>
                                        <form id="original_modify_reply_form<?php echo $reply_idx;?>" action="/board/notice_reply1_modify_save/" method="post" >
                                            <input type="hidden" name="reply_no" value="<?php echo $row['reply_no'];?>">
                                            <input type="hidden" name="notice_no" value="<?php echo $notice_detail['notice_no'];?>">
                                            <tr id="original_modify_reply_<?php echo $reply_idx;?>"  style="display:none;">
                                                <th><?php echo $row['mem_nm'];?></th>
                                                <td>
                                                    <textarea name="reply_cont" class="form-control" rows="3" ><?php echo str_replace("<br/>","\n",$row['reply_cont']);?></textarea>
                                                </td>
                                                <td class="txt_center">
                                                    <input type='button' class='btn btn-sm btn-primary' value="<?php echo lang('strSave')?>" onclick="original_modify_save('<?php echo $reply_idx;?>');" />&nbsp;
                                                    <input type='button' class='btn btn-sm btn-danger' value="<?php echo lang('strCancel')?>" onclick="original_modify_cancel('<?php echo $reply_idx;?>');" />&nbsp;
                                                </td>
                                            </tr>
                                        </form>
                                        <form id="reply_form_<?php echo $reply_idx;?>" name="reply_form_<?php echo $reply_idx;?>" method="post" action = "/board/notice_reply1_save" >
                                            <input type="hidden" name="parent_reply_no" value="<?php echo $row['reply_no']?>">
                                            <input type="hidden" name="notice_no" value="<?php echo $notice_detail['notice_no'];?>">
                                            <input type="hidden" name="reply_depth" value="2">
                                            <input type="hidden" name="mem_no" value="<?php echo $mem_no; ?>">
                                            <tr id="reply_tr<?php echo $reply_idx;?>" style="display: none;">
                                                <th><?php echo $this->session->userdata('mem_nm');?></th>
                                                <td >
                                                    <textarea name="reply_cont" class="form-control" rows="3" ></textarea>
                                                 </td>
                                                 <td>
                                                     <input type="button" class="btn btn-sm btn-primary" value="<?php echo lang('strSave')?>" onclick="reply2_save('<?php echo $reply_idx;?>');" />&nbsp;
                                                </td>
                                            </tr>
                                        </form>
                                    <?php }else{?>
                                        <tr id="original_reply_reply_<?php echo $reply_idx;?>">
                                            <th class="txt_right pa_r5">
                                                ↳ <?php echo $row['mem_nm']; ?>
                                            </th>
                                            <td>
                                                <?php echo $row['reply_cont']; ?>
                                                <?php if ($mem_no == $row['mem_no']){?>
                                                    <a class="cursor" onclick="reply_reply_modify('<?php echo $reply_idx;?>');">수정</a>
                                                    <a class="cursor" onclick="reply_del('original_modify_reply_reply_form<?php echo $reply_idx;?>');">삭제</a>
                                                <?php }?>
                                            </td>
                                            <td><?php echo $row['date_ymd']; ?></td>
                                        </tr>
                                        <form id="original_modify_reply_reply_form<?php echo $reply_idx;?>" action="" method="post" >
                                            <input type="hidden" name="reply_no" value="<?php echo $row['reply_no'];?>">
                                            <input type="hidden" name="notice_no" value="<?php echo $notice_detail['notice_no'];?>">
                                            <tr id="original_modify_reply_reply_<?php echo $reply_idx;?>"  style="display:none;">
                                                <th><?php echo $this->session->userdata('mem_nm');?></th>
                                                <td>
                                                    <textarea name="reply_cont" class="form-control" rows="3" ><?php echo str_replace("<br/>","\n",$row['reply_cont']);?></textarea>
                                                </td>
                                                <td>
                                                    <input type='button' class='btn btn-sm btn-primary' value="<?php echo lang('strSave')?>" onclick="original_reply_modify_save('<?php echo $reply_idx;?>');" />&nbsp;
                                                    <input type='button' class='btn btn-sm btn-danger' value="<?php echo lang('strCancel')?>" onclick="original_reply_modify_cancel('<?php echo $reply_idx;?>');" />&nbsp;
                                                </td>
                                            </tr>
                                        </form>
                                    <?php }?>
                               <?php $reply_idx++;}?>
                           <?php }?>
                       </tbody>
                    </table>
            <?php }?>
            <div class="center">
                <?php if ($next_notice_no['notice_no'] != ""){?>
                    <a href="javascript:notice_move('<?php echo $next_notice_no['notice_no']?>')"><span class="btn btn-dark float_r">︎︎<?php echo lang('strNextNotice')?> ▶</span></a>
                <?php }?>
                <?php if ($pre_notice_no['notice_no'] != ""){?>
                    <a href="javascript:notice_move('<?php echo $pre_notice_no['notice_no']?>')"><span class="btn btn-dark float_l">◀ <?php echo lang('strPreNotice')?></span></a>
                <?php }?>
            </div>
        </div>
        <form name="notice_move" method="post">
            <input type="hidden" name="notice_no">
        </form>
    </div>
</div>
<script type="text/javascript">

    function notice_move(no){
        var frm = document.notice_move;
        frm.action = "/board/m_notice_detail_view"
        frm.notice_no.value = no;
        frm.submit();
    }

    function reply1_save(){
        frm = document.reply1_form;
        frm.action = "/board/notice_reply1_save";
        frm.submit();
    }

    //댓글에 대한 수정
    function reply_modify(idx){
        original_tr=document.getElementById('original_reply_'+idx);
        original_modify_tr=document.getElementById('original_modify_reply_'+idx);
        original_tr.style.display="none";
        original_modify_tr.style.display="";
    }

    function original_modify_save(idx){
        document.getElementById('original_modify_reply_form'+idx).submit();
    }

    function original_modify_cancel(idx){
        original_tr=document.getElementById('original_reply_'+idx);
        original_modify_tr=document.getElementById('original_modify_reply_'+idx);
        original_tr.style.display="";
        original_modify_tr.style.display="none";
    }
    //댓글 댓글에 대한 수정
    function reply_reply_modify(idx){
        original_tr=document.getElementById('original_reply_reply_'+idx);
        original_modify_tr=document.getElementById('original_modify_reply_reply_'+idx);
        original_tr.style.display="none";
        original_modify_tr.style.display="";
    }

    function original_reply_modify_save(idx){
        document.getElementById('original_modify_reply_reply_form'+idx).action="/board/notice_reply1_modify_save";
        document.getElementById('original_modify_reply_reply_form'+idx).submit();
    }

    function original_reply_modify_cancel(idx){
        original_tr=document.getElementById('original_reply_reply_'+idx);
        original_modify_tr=document.getElementById('original_modify_reply_reply_'+idx);
        original_tr.style.display="";
        original_modify_tr.style.display="none";
    }

    function reply_del(frmname){
        if(confirm("댓글을 삭제하시겠습니까?")){
            document.getElementById(frmname).action="/board/notice_reply1_delete";
            document.getElementById(frmname).submit();
        }
    }

    //대댓글 작성
    function reply_view(idx){
        tr=document.getElementById('reply_tr'+idx);
        cancel_btn=document.getElementById('cancel_btn_'+idx);
        reply_btn=document.getElementById('reply_btn_'+idx);
        tr.style.display = "";
        cancel_btn.style.display = "";
        reply_btn.style.display = "none";
    }
    function reply_view_close(idx){
        tr=document.getElementById('reply_tr'+idx);
        cancel_btn=document.getElementById('cancel_btn_'+idx);
        reply_btn=document.getElementById('reply_btn_'+idx);
        tr.style.display = "none";
        cancel_btn.style.display = "none";
        reply_btn.style.display = "";
    }
    function reply2_save(idx){
        document.getElementById('reply_form_'+idx).submit();
    }

</script>